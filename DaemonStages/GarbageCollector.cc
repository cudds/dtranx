/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "GarbageCollector.h"
#include "DTranx/Util/Log.h"
#include "Util/FileUtil.h"

#include "Stages/Log/TranxLog.h"
#include "Stages/TranxRPCHelper.h"
#include "Repartition.h"
#include "Stages/TranxStates.h"

namespace DTranx {
namespace DaemonStages {

GarbageCollector::GarbageCollector(SharedResources::TranxServerInfo *tranxServerInfo,
		SharedResources::TranxServiceSharedData *tranxServiceSharedData)
		: DaemonStages(tranxServerInfo, tranxServiceSharedData, "GarbageCollect") {
	std::string logFullName = Util::FileUtil::CombineName(
			tranxServerInfo->GetConfig().read("PersistLogDir"),
			tranxServerInfo->GetConfig().read("GCLogFileName"));
	gcLog = std::unique_ptr<Stages::Log::TranxLog>(
			new Stages::Log::TranxLog(logFullName, tranxServerInfo));
}

GarbageCollector::~GarbageCollector() {
}

void GarbageCollector::DaemonThread() {
	NOTICE("GCThread started");
	uint64 gcTimeout = tranxServerInfo->GetConfig().read<uint64>("GCPollingPeriod");
	std::vector<std::string> ips = tranxServerInfo->GetAllNodes();
	int nodeID = tranxServerInfo->GetNodeID();
	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	while (true) {
		if (terminateThread.load()) {
			NOTICE("GCThread is reclaimed");
			break;
		}
		std::vector<Stages::TranxServiceQueueElement *> tempElements;
		Stages::TranxServiceQueueElement *element;
		while (daemonProcessQueue.try_dequeue(element)) {
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(0);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(0);
				std::vector<int> taskLongLatencySamples = GetLongLatency(0);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName);
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				delete element;
				continue;
			}
			assert(tempElements.size() <= 1);
			if (tempElements.size() != 0) {
				AddToFreeList(*tempElements.begin());
				tempElements.clear();
			}
			tempElements.push_back(element);
		}

		if (!tempElements.empty()) {
			/*
			 * simply use the last response from tranxStates is sufficient
			 */
			assert(tempElements.size() == 1);
			element = *tempElements.begin();
			//element->goThroughStage("GC");
			Stages::GCFactory tmpGCFactory = element->getStateGcFactory();
			bool gcLogWritten = false;
			if (element->isStateToBroadcast()) {
				uint64 selfEarliest = tmpGCFactory.GetEarliest();
				std::unordered_map<int, uint64> allNodeGC = tmpGCFactory.GetAllNodeGC();
				VERBOSE("gc broadcast(earliest): %lu", tmpGCFactory.GetEarliest());
				gcLogWritten = true;
				gcLog->GCEntry(allNodeGC);
				for (auto it = ips.begin(); it != ips.end(); ++it) {
					if (*it == selfAddress) {
						continue;
					}
					RPC::TranxRPC* tranxRPC = new RPC::TranxRPC(Service::GOpCode::TRANX_GC,
							RPC::GServiceID::TranxService, 0, false);
					tranxRPC->GetTranxRequest()->mutable_gc()->set_nodeid(nodeID);
					tranxRPC->GetTranxRequest()->mutable_gc()->set_tranxid(selfEarliest);
					tranxRPCHelper->SendNoResponse(*it, tranxRPC);
				}
			}
			std::set<std::string> files = DTranx::Util::FileUtil::LsPrefix(
					tranxServerInfo->GetConfig().read("PersistLogDir"),
					tranxServerInfo->GetConfig().read("LogFileName"));
			if (files.size() > 1) {
				if (!gcLogWritten) {
					gcLog->GCEntry(tmpGCFactory.GetAllNodeGC());
				}
				for (auto it = files.begin(); *it != *(files.rbegin()); ++it) {
					/*
					 * check if this file can be deleted
					 */
					VERBOSE("checking for log files %s if it can be deleted", it->c_str());
					Stages::Log::LogOption logOption =
							(tranxServerInfo->GetConfig().read("LogOption") == "Pmem") ?
									Stages::Log::PMEM : Stages::Log::DISK;
					Stages::Log::TranxLog tmpTranxLog(*it, tranxServerInfo);
					bool toRemove = true;
					while (true) {
						Stages::Log::GTranxLogRecord entry;
						try {
							entry = tmpTranxLog.OneReadNext();
							if (!tmpGCFactory.IsOutdated(entry.tranxid().nodeid(),
									entry.tranxid().tranxid())) {
								VERBOSE("tranx %d#%lu is not ready to be garbage collected",
										entry.tranxid().nodeid(), entry.tranxid().tranxid());
								toRemove = false;
								break;
							}
						} catch (Util::NotFoundException &e) {
							break;
						}
					}
					if (toRemove) {
						VERBOSE("log file %s is garbage collected", it->c_str());
						std::string dirName, fileName;
						Util::FileUtil::SplitName(*it, dirName, fileName);
						std::string newFilename = Util::FileUtil::AdjustDirSuffix(dirName) + "."
								+ fileName;
						assert(Util::FileUtil::RenameFile(*it, newFilename));
						repartition->SendToLogRepartition(newFilename);
					}
				}
			}
			AddToFreeList(element);
		} else {
			/*
			 * transform from ack to gc, send task to tranxStates
			 */
			Stages::TranxServiceQueueElement *element;
			if (freeList.size() == 0) {
				element = new Stages::TranxServiceQueueElement();
			} else {
				element = freeList.front();
				freeList.pop();
			}
			//element->goThroughStage("GC");
			element->setStageId(Stages::TranxServiceQueueElement::StageID::GCTHREAD);
			element->setStateTask(Stages::TranxStatesStageTask::_GarbageCollectAck);
			tranxStates->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID, ptok_tranxStates);
		}

		usleep(gcTimeout);
	}
}

} /* namespace DaemonStages */
} /* namespace DTranx */

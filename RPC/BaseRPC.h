/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * BaseRPC provides basic RPC header and serialization/deserialization functions
 * 	besides, messageID is also included to distinguish RPC's because it's asynchronous
 */

#ifndef DTRANX_RPC_BASERPC_H_
#define DTRANX_RPC_BASERPC_H_

#include <memory>
#include "DTranx/Util/Buffer.h"
#include <google/protobuf/message.h>
#include "zmsg.hpp"
#include "DTranx/Util/Log.h"
#include "DTranx/RPC/RPC.pb.h"

namespace DTranx {
namespace RPC {
using Util::Buffer;

class BaseRPC {
public:
	BaseRPC();
	explicit BaseRPC(Buffer reqBuffer, Buffer repBuffer);
	explicit BaseRPC(GRPCRequestMessage *requestMessage, GRPCResponseMessage *responseMessage);
	BaseRPC(BaseRPC&& other);
	BaseRPC& operator=(BaseRPC&& other);
	virtual ~BaseRPC() noexcept(false) {
		if (requestMessage != NULL) {
			delete requestMessage;
		}
		if (responseMessage != NULL) {
			delete responseMessage;
		}
	}

	/*
	 * message ID access methods
	 */
	uint64 GetReqMessageID() {
		if (requestMessage == NULL) {
			assert(false);
			return 0;
		}
		return requestMessage->messageid();
	}

	void SetReqMessageID(uint64 messageID) {
		if (requestMessage == NULL) {
			return;
		}
		requestMessage->set_messageid(messageID);
	}

	uint64 GetRepMessageID() {
		if (responseMessage == NULL) {
			assert(false);
			return 0;
		}
		return responseMessage->messageid();
	}

	void SetRepMessageID(uint64 messageID) {
		if (responseMessage == NULL) {
			return;
		}
		responseMessage->set_messageid(messageID);
	}

	/*
	 * request ip/port access methods
	 */
	std::string GetReqIP();
	void SetReqIP(uint32_t* ip);
	void SetReqIP(std::string ipstr);

	uint32_t GetReqPort() {
		if (requestMessage == NULL) {
			assert(false);
			return 0;
		}
		return requestMessage->port();
	}

	void SetReqPort(uint32_t port) {
		if (requestMessage == NULL) {
			return;
		}
		return requestMessage->set_port(port);
	}

	/*
	 * service ID access methods
	 */
	GServiceID GetReqService() {
		if (requestMessage == NULL) {
			return GServiceID::InvalidService;
		}
		return requestMessage->serviceid();
	}

	void SetReqService(GServiceID serviceID) {
		if (requestMessage == NULL) {
			return;
		}
		requestMessage->set_serviceid(serviceID);
	}

	/*
	 * status access methods
	 */

	GStatus GetRepStatus() {
		if (responseMessage == NULL) {
			return GStatus::INVALID_STATUS;
		}
		return responseMessage->status();
	}

	void SetRepStatus(GStatus status) {
		if (responseMessage == NULL) {
			return;
		}
		responseMessage->set_status(status);
	}

	/*
	 * Access methods directly to request and response
	 */
	GRPCRequestMessage* GetRequest() {
		return requestMessage;
	}

	GRPCResponseMessage* GetResponse() {
		return responseMessage;
	}

	void SetRequest(GRPCRequestMessage* requestM) {
		if (requestMessage != NULL) {
			delete requestMessage;
		}
		requestMessage = requestM;
	}

	void SetResponse(GRPCResponseMessage* responseM) {
		if (responseMessage != NULL) {
			delete responseMessage;
		}
		responseMessage = responseM;
	}

	/*
	 * static methods
	 * 	parsing message: return whole message
	 * 	protobuf serialize/parse
	 */
	static GMultiRPCResponseMessage *GetMessages(void *message, int size) {
		if (message != NULL) {
			Buffer repBuffer(message, size);
			GMultiRPCResponseMessage *responseMessageTmp = new GMultiRPCResponseMessage();
			assert(parse(repBuffer, *responseMessageTmp, 0));
			return responseMessageTmp;
		}
		return NULL;
	}

	static GRPCResponseMessage *GetMessage(void *message, int size) {
		if (message != NULL) {
			Buffer repBuffer(message, size);
			GRPCResponseMessage *responseMessageTmp = new GRPCResponseMessage();
			assert(parse(repBuffer, *responseMessageTmp, 0));
			return responseMessageTmp;
		}
		return NULL;
	}

	static bool parse(Buffer& from, google::protobuf::Message& to, uint32 skipBytes) {
		if (!to.ParseFromArray(static_cast<const char *>(from.GetData()) + skipBytes,
				from.GetLength() - skipBytes)) {
			WARNING("Missing fields in protocol buffer of type %s: %s", to.GetTypeName().c_str(),
					to.InitializationErrorString().c_str());
			return false;
		}
		return true;
	}

	static void serialize(const google::protobuf::Message& from, Buffer& to, uint32 skipBytes) {
		uint32 length = from.ByteSize();
		uint8* data = new uint8[skipBytes + length];
		from.SerializeToArray(data + skipBytes, length);
		to.SetData(data, skipBytes + length);
	}

	static void serializeWithMem(const google::protobuf::Message& from, Buffer& to,
			uint32_t bufferSize) {
		uint32 length = from.ByteSize();
		assert(length <= bufferSize);
		void* data = to.GetData();
		from.SerializeToArray(data, length);
		to.SetData(data, length);
	}
	void GetReqCombinedEndpoint(char *endpoint, char *ip);

protected:
	GRPCRequestMessage *requestMessage;
	GRPCResponseMessage *responseMessage;

	/*
	 * BaseRPC is non-copyable.
	 */
	BaseRPC(const BaseRPC&) = delete;
	BaseRPC& operator=(const BaseRPC&) = delete;
};

} /* namespace RPC */
} /* namespace DTranx */

#endif /* DTRANX_RPC_BASERPC_H_ */

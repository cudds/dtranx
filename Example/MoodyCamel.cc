/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <getopt.h>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include "DTranx/Util/ConcurrentQueue.h"
#include "DTranx/Util/Log.h"

/*
 * Test moodycamel enqueue/dequeue separately
 */

/**
 * Parses argv for the main function.
 */
class OptionParser {
public:
	OptionParser(int& argc, char**& argv)
			: argc(argc), argv(argv), numOfThreads(0) {
		while (true) {
			static struct option longOptions[] = { { "thread",
			required_argument, NULL, 't' }, { "help",
			no_argument, NULL, 'h' }, { 0, 0, 0, 0 } };
			int c = getopt_long(argc, argv, "t:e:h", longOptions, NULL);

			// Detect the end of the options.
			if (c == -1)
				break;

			switch (c) {
				case 't':
					numOfThreads = std::stoi(optarg, nullptr, 10);
					break;
				case 'e':
					numOfElements = std::stoi(optarg, nullptr, 10);
					break;
				case 'h':
					usage();
					exit(0);
				case '?':
				default:
					// getopt_long already printed an error message.
					usage();
					exit(1);
			}
		}
	}

	void usage() {
		std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
		std::cout << "Options: " << std::endl;
		std::cout << "  -t, --thread"
				<< "thread number in total including both producer and consumers" << std::endl;
		std::cout << "  -e, --element" << "total number of elements to enqueue/dequeue"
				<< std::endl;
		std::cout << "  -h, --help              " << "Print this usage information" << std::endl;
	}

	int& argc;
	char**& argv;
	uint32_t numOfThreads;
	uint32_t numOfElements;
};

moodycamel::ConcurrentQueue<int> queue(1000);

void Producer(int numOfElementToEnqueue) {
	VERBOSE("Producer started");
	moodycamel::ProducerToken ptok(queue);
	int count = 0;
	while (count < numOfElementToEnqueue) {
		count += (queue.try_enqueue(ptok, 0)) ? 1 : 0;
	}
	VERBOSE("Producer exited");
}

void Consumer(int numOfElementToDequeue) {
	VERBOSE("Consumer started");
	int count = 0;
	int item;
	int tmpelements[20];
	while (count < numOfElementToDequeue) {
		count += queue.try_dequeue_bulk(tmpelements, 20);
	}
	VERBOSE("Consumer exited");
}

int main(int argc, char** argv) {
	DTranx::Util::Log::setLogPolicy( { { "Example", "VERBOSE" } });
	OptionParser options(argc, argv);
	if (options.numOfThreads < 2) {
		exit(0);
	}
	int numOfProducerThreads = options.numOfThreads - 1;
	int numOfConsumerThreads = 1;
	int totalElements = options.numOfElements;
	totalElements = totalElements / numOfProducerThreads * numOfProducerThreads;

	boost::chrono::steady_clock::time_point start = boost::chrono::steady_clock::now();

	std::vector<boost::thread> producers;
	boost::thread consumer(Consumer, totalElements);
	// Create a cpu_set_t object representing a set of CPUs. Clear it and mark
	// only CPU i as set.

	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(0, &cpuset);
	int rc = pthread_setaffinity_np(consumer.native_handle(), sizeof(cpu_set_t), &cpuset);
	if (rc != 0) {
		std::cerr << "Error calling pthread_setaffinity_np: " << rc << "\n";
	}

	for (int i = 1; i < options.numOfThreads; ++i) {
		producers.push_back(boost::thread(Producer, totalElements / numOfProducerThreads));

		cpu_set_t cpuset;
		CPU_ZERO(&cpuset);
		CPU_SET(i, &cpuset);
		int rc = pthread_setaffinity_np(producers[i - 1].native_handle(), sizeof(cpu_set_t),
				&cpuset);
		if (rc != 0) {
			std::cerr << "Error calling pthread_setaffinity_np: " << rc << "\n";
		}

	}

	consumer.join();
	for (int i = 1; i < options.numOfThreads; ++i) {
		producers[i - 1].join();
	}

	boost::chrono::steady_clock::time_point end = boost::chrono::steady_clock::now();
	uint64_t timeElapsed =
			boost::chrono::duration_cast<boost::chrono::nanoseconds>(end - start).count();
	double throughput = totalElements * 1.0 / timeElapsed * 1000000;
	VERBOSE("threads: %d, time(nanoseconds): %lu; total number of elements: %d, throughput %fk/sec",
			options.numOfThreads, timeElapsed, totalElements, throughput);

}

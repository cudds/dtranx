#!/usr/bin/env python
import sys
sys.path.insert(0, '/home/neal/Documents/dev/DTranx/Scripts')

from BasicLogHelper import *
from Utility.SSHHelper import *
from PerfPlot import *
from Utility.BashHelper import *
import subprocess
import argparse
import os
import numpy as np

def CompileProto(srcDir, srcFile, destDir):
	command = "protoc -I=" + srcDir + " --python_out=" + destDir + " " + srcDir + "/" + srcFile
	print command
	runBash(command)

def CleaningProto(dirName, protoName):
	command = "rm " + dirName + protoName + "_pb2.py*"
	print command
	runBash(command)

# compile python protobuf
scriptPath = GetScriptDir(__file__)
CompileProto(scriptPath + "/../../Util", "Profile.proto", scriptPath + "/")
from Profile_pb2 import *

# argument parsing
parser = argparse.ArgumentParser()
parser.add_argument("-m","--mode",dest="mode",help="perf: show the queue length of stages and the average time spent in stages;\
	partition: how data in different servers are accessed in transactions",
	choices = ["partition", "perf"], required =True)
parser.add_argument("-l", "--log", dest="log",help="log filenames, it could be a regular expression", required =True)
args = parser.parse_args()

if args.mode == 'partition':
	logNames = GetLogNamesFromRegex(args.log);
	logs = []
	for logName in logNames:
		logs.extend(GetLogFromFile(LogKind.PROFILE, logName))

	#metric analysis
	numServerInvolved = {}
	serverInTranx = {}
	for log in logs:
		profileLog = ProfileLog()
		profileLog.ParseFromString(log)
		if profileLog.type == SCALE:
			assert profileLog.HasField("scalelog")
			numOfServer = len(profileLog.scalelog.ips)
			if numServerInvolved.has_key(numOfServer):
				numServerInvolved[numOfServer] += 1
			else:
				numServerInvolved[numOfServer] = 1
			for ip in profileLog.scalelog.ips:
				if serverInTranx.has_key(ip):
					serverInTranx[ip] += 1
				else:
					serverInTranx[ip] = 1

	#metric output
	print "----------------------------------"
	print "Metric: How many servers are involved in each transaction"
	print "# of servers, # of transactions" 
	for numServer in numServerInvolved.keys():
		print str(numServer) + "," + str(numServerInvolved[numServer])

	print "----------------------------------"
	print "Metric: how many transactions are each server involved in"
	print "serverIP, # of transactions"
	for serverIP in serverInTranx.keys():
		print str(serverIP) + "," + str(serverInTranx[serverIP])

	print "----------------------------------"
	CleaningProto(scriptPath + "/", "Profile")
elif args.mode == "perf":
	logNames = GetLogNamesFromRegex(args.log);
	logs = []
	for logName in logNames:
		logs.extend(GetLogFromFile(LogKind.PROFILE, logName, -1))
	responses = {}
	queueLength = {}
	longLatencyTasks = {}

	logNum = 1
	for log in logs:
		logNum = logNum + 1
		profileLog = ProfileLog()
		profileLog.ParseFromString(log)
		if profileLog.type == PERF and profileLog.HasField("perflog"):
			if not responses.has_key(profileLog.perflog.stagename):
				responses[profileLog.perflog.stagename] = []
			if not queueLength.has_key(profileLog.perflog.stagename):
				queueLength[profileLog.perflog.stagename] = []
			if not longLatencyTasks.has_key(profileLog.perflog.stagename):
				longLatencyTasks[profileLog.perflog.stagename] = []
			for responseTimeSample in profileLog.perflog.timensec:
				responses[profileLog.perflog.stagename].append(responseTimeSample)
			for queueLengthSample in profileLog.perflog.queuelength:
				queueLength[profileLog.perflog.stagename].append(queueLengthSample)
			for longLatencySample in profileLog.perflog.longlatencytask:
				longLatencyTasks[profileLog.perflog.stagename].append(longLatencySample)
	for stageName in responses.keys():
		print stageName,"(response): total prints ",len(responses[stageName])

		if len(responses[stageName]) == 0:
			continue
		print "max: ", np.max(responses[stageName])
		print "min: ", np.min(responses[stageName])
		print "avg: ", np.average(responses[stageName])
		print "median: ", np.median(responses[stageName])
		'''
		responseStr = ""
		for response in responses[stageName]:
			responseStr += str(response) + ","
		print responseStr
		'''
	for stageName in queueLength.keys():
		print stageName,"(queue): total prints ",len(queueLength[stageName])
		if len(queueLength[stageName]) == 0:
			continue
		print "max: ", np.max(queueLength[stageName])
		print "min: ", np.min(queueLength[stageName])
		print "avg: ", np.average(queueLength[stageName])
		print "median: ", np.median(queueLength[stageName])
		
		queueStr = ""
		for queue in queueLength[stageName]:
			queueStr += str(queue) + ","
		print queueStr
		
	for stageName in longLatencyTasks.keys():
		print stageName,"(long latency task): total prints ",len(longLatencyTasks[stageName])
		if len(longLatencyTasks[stageName]) == 0:
			continue
		taskStr = ""
		for task in longLatencyTasks[stageName]:
			taskStr += str(task) + ","
		print taskStr
		
	CleaningProto(scriptPath + "/", "Profile")

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "BaseRPC.h"
using namespace DTranx;

TEST(BaseRPC, header) {
	RPC::BaseRPC baseRPC;
	uint32_t selfAddress[4];
	selfAddress[0] = 192;
	selfAddress[1] = 168;
	selfAddress[2] = 0;
	selfAddress[3] = 1;

	baseRPC.responseMessage = new RPC::GRPCResponseMessage();
	baseRPC.SetReqMessageID(15);
	EXPECT_EQ(15, baseRPC.GetReqMessageID());
	baseRPC.SetRepMessageID(17);
	EXPECT_EQ(17, baseRPC.GetRepMessageID());
	baseRPC.SetReqIP(selfAddress);
	EXPECT_EQ("192.168.0.1", baseRPC.GetReqIP());
	baseRPC.SetReqPort(30030);
	EXPECT_EQ(30030, baseRPC.GetReqPort());
	baseRPC.SetReqService(RPC::GServiceID::StorageService);
	EXPECT_EQ(RPC::GServiceID::StorageService, baseRPC.GetReqService());
	baseRPC.SetRepStatus(RPC::GStatus::OK);
	EXPECT_EQ(RPC::GStatus::OK, baseRPC.GetRepStatus());
}

TEST(BaseRPC, serialization) {
	RPC::BaseRPC baseRPC;
	uint32_t selfAddress[4];
	selfAddress[0] = 192;
	selfAddress[1] = 168;
	selfAddress[2] = 0;
	selfAddress[3] = 1;
	baseRPC.SetReqService(RPC::GServiceID::StorageService);
	baseRPC.SetReqIP(selfAddress);
	baseRPC.SetReqPort(30030);
	baseRPC.SetReqMessageID(15);
	Util::Buffer buffer;
	RPC::BaseRPC::serialize(*baseRPC.GetRequest(), buffer, 0);
}

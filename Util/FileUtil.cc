/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "FileUtil.h"
#include <dirent.h>
#include <stdio.h>
#include <cassert>
#include <sys/stat.h>
#include "DTranx/Util/Log.h"

namespace DTranx {
namespace Util {

FileUtil::FileUtil() {
}

FileUtil::~FileUtil() noexcept(false) {
}

void FileUtil::RemoveFile(std::string fileName) {
	remove(fileName.c_str());
}

void FileUtil::RemoveDir(std::string dirName) {
	std::string command = "rm -rf " + dirName;
	system(command.c_str());
}

void FileUtil::RemovePrefix(std::string dirName, std::string prefix) {
	std::set<std::string> files = LsPrefix(dirName, prefix);
	for (auto it = files.begin(); it != files.end(); ++it) {
		if (remove(it->c_str()) != 0) {
			assert(false);
		}
	}
}

std::set<std::string> FileUtil::LsPrefix(std::string fullPrefix) {
	std::string dirName, fileName;
	SplitName(fullPrefix, dirName, fileName);
	return LsPrefix(dirName, fileName);
}

std::set<std::string> FileUtil::LsPrefix(std::string dirName, std::string prefix) {
	dirName = AdjustDirSuffix(dirName);
	std::set<std::string> allFiles = LsFiles(dirName);
	std::set<std::string> result;
	std::string dirPrefix = dirName + prefix;
	for (auto it = allFiles.begin(); it != allFiles.end(); ++it) {
		if (it->find(dirPrefix) == 0) {
			result.insert(*it);
		}
	}
	return result;
}

std::set<std::string> FileUtil::LsPrefixDir(std::string dirName, std::string prefix) {
	dirName = AdjustDirSuffix(dirName);
	std::set<std::string> allDirs = LsDirectory(dirName);
	std::set<std::string> result;
	std::string dirPrefix = dirName + prefix;
	for (auto it = allDirs.begin(); it != allDirs.end(); ++it) {
		if (it->find(dirPrefix) == 0) {
			result.insert(*it);
		}
	}
	return result;
}

std::set<std::string> FileUtil::LsFiles(std::string dirName) {
	dirName = AdjustDirSuffix(dirName);
	DIR *dp;
	struct dirent *dirp;
	std::set<std::string> files;
	if ((dp = opendir(dirName.c_str())) == NULL) {
		VERBOSE("cannot open %s", dirName.c_str());
		return std::set<std::string>();
	}

	while ((dirp = readdir(dp)) != NULL) {
		if (dirp->d_type == DT_REG) {
			files.insert(dirName + std::string(dirp->d_name));
		}
	}
	closedir(dp);
	return files;
}

std::set<std::string> FileUtil::LsDirectory(std::string dirName) {
	dirName = AdjustDirSuffix(dirName);
	DIR *dp;
	struct dirent *dirp;
	std::set<std::string> dirs;
	if ((dp = opendir(dirName.c_str())) == NULL) {
		VERBOSE("cannot open %s", dirName.c_str());
		return std::set<std::string>();
	}

	while ((dirp = readdir(dp)) != NULL) {
		if (dirp->d_type == DT_DIR) {
			dirs.insert(dirName + std::string(dirp->d_name));
		}
	}
	closedir(dp);
	return dirs;
}

bool FileUtil::RenameFile(std::string src, std::string des) {
	if (-1 == rename(src.c_str(), des.c_str())) {
		VERBOSE("rename fails with src %s and des %s", src.c_str(), des.c_str());
		return false;
	}
	return true;
}

std::string FileUtil::AdjustDirSuffix(std::string dirName) {
	if (*dirName.rbegin() != '/') {
		dirName += "/";
	}
	return dirName;
}

std::string FileUtil::CombineName(std::string prefix, std::string fileName) {
	return AdjustDirSuffix(prefix) + fileName;
}

void FileUtil::SplitName(std::string fullPathName, std::string& dirName, std::string& fileName) {
	const size_t last_slash_idx = fullPathName.rfind('/');
	if (std::string::npos != last_slash_idx) {
		dirName = fullPathName.substr(0, last_slash_idx);
		fileName = fullPathName.substr(last_slash_idx + 1);
	} else {
		dirName = "./";
		fileName = fullPathName;
	}
}

} /* namespace Util */
} /* namespace DTranx */

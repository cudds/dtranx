/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "PerfStage.h"
#include "DTranx/Util/Log.h"

namespace DTranx {
namespace Stages {
PerfStage::PerfStage(std::string stageName)
		: stageName(stageName), queuePerfFreq(10), responsePerfFreq(1), responseEnabled(false),
				queueEnabled(false) {

}

void PerfStage::AddQueueLengthSample(uint32_t index, uint32_t queueLength) {
	assert(index < 10);
	if (queueLength > 20) {
		queueLengthSamples[index].push_back(queueLength);
	}
}

std::vector<uint32_t> PerfStage::GetQueueLengthSamplesAndClear(uint32_t index) {
	assert(index < 10);
	std::vector<uint32_t> queueLengthSamplesCopy = queueLengthSamples[index];
	queueLengthSamples[index].clear();
	return queueLengthSamplesCopy;
}

} /* namespace Stages */
} /* namespace DTranx */

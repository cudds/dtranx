/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "DTranx/RPC/ZeromqReceiver.h"

namespace DTranx {
namespace RPC {

ZeromqReceiver::ZeromqReceiver(std::shared_ptr<zmq::context_t> context)
		: ZeromqConnection(context, ZMQ_ROUTER), zm() {
	int hwm = 0;
	size_t len = sizeof(hwm);
	socket.setsockopt(ZMQ_SNDHWM, &hwm, len);
	socket.setsockopt(ZMQ_RCVHWM, &hwm, len);
}

ZeromqReceiver::~ZeromqReceiver() noexcept(false) {
}

void *ZeromqReceiver::Wait(int &size, int pollInMilliSecond) {
	zm.clear();
	if (!zm.recv(socket, pollInMilliSecond)) {
		return NULL;
	}
	zmsg::ustring request = zm.get_part(1);
	assert(!request.empty());

	size = request.size();
	uint8_t *requestArray = new uint8_t[size];
	memcpy((void*) requestArray, request.data(), size);
	return requestArray;
}

void *ZeromqReceiver::Wait(int &size) {
	zm.clear();
	if (!zm.recv_non_block(socket)) {
		return NULL;
	}
	zmsg::ustring request = zm.get_part(1);
	assert(!request.empty());

	size = request.size();
	uint8_t *requestArray = new uint8_t[size];
	memcpy((void*) requestArray, request.data(), size);
	return requestArray;
}

void *ZeromqReceiver::Wait(int &size, zmsg::ustring& clientID) {
	zm.clear();
	if (!zm.recv_non_block(socket)) {
		return NULL;
	}
	clientID = zm.get_part(0);
	zmsg::ustring request = zm.get_part(1);
	assert(!request.empty());

	size = request.size();
	uint8_t *requestArray = new uint8_t[size];
	memcpy((void*) requestArray, request.data(), size);
	return requestArray;
}

void *ZeromqReceiver::Wait(int &size, zmsg::ustring& clientID, void *requestBuffer,
		uint32_t requestBufferSize, bool& inBuffer) {
	inBuffer = false;
	zm.clear();
	if (!zm.recv_non_block(socket)) {
		return NULL;
	}
	clientID = zm.get_part(0);
	zmsg::ustring request = zm.get_part(1);
	assert(!request.empty());

	size = request.size();
	if (requestBufferSize >= size) {
		memcpy(requestBuffer, request.data(), size);
		inBuffer = true;
		return NULL;
	} else {
		uint8_t *requestArray = new uint8_t[size];
		memcpy((void*) requestArray, request.data(), size);
		return requestArray;
	}

}

} /* namespace RPC */
} /* namespace DTranx */

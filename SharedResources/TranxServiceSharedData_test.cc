/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "TranxServiceSharedData.h"
#include "Util/FileUtil.h"

using namespace DTranx;

TEST(TranxServiceSharedData, GenerateTranxID) {
	std::vector<std::string> allNodes;
	allNodes.push_back("192.168.0.1");
	allNodes.push_back("192.168.0.2");
	allNodes.push_back("192.168.0.3");
	allNodes.push_back("192.168.0.4");
	allNodes.push_back("192.168.0.5");
	DTranx::Util::ConfigHelper configHelper;
	configHelper.readFile("DTranx.conf");
	SharedResources::TranxServerInfo *tranxServerInfo = new SharedResources::TranxServerInfo();
	tranxServerInfo->SetAllNodes(allNodes);
	tranxServerInfo->SetConfig(configHelper);
	SharedResources::TranxServiceSharedData *tranxServiceSharedData =
			new SharedResources::TranxServiceSharedData(2, tranxServerInfo);
	EXPECT_EQ(Util::TranxID(2, 1), tranxServiceSharedData->GenerateTranxID());
	EXPECT_EQ(Util::TranxID(2, 2), tranxServiceSharedData->GenerateTranxID());
	Util::FileUtil::RemoveDir("dtranx.mapdb");
	delete tranxServerInfo;
	delete tranxServiceSharedData;
}

TEST(TranxServiceSharedData, TranxHistoryBroadcast) {
	std::vector<std::string> allNodes;
	allNodes.push_back("192.168.0.1");
	allNodes.push_back("192.168.0.2");
	allNodes.push_back("192.168.0.3");
	allNodes.push_back("192.168.0.4");
	allNodes.push_back("192.168.0.5");
	DTranx::Util::ConfigHelper configHelper;
	configHelper.readFile("DTranx.conf");
	SharedResources::TranxServerInfo *tranxServerInfo = new SharedResources::TranxServerInfo();
	tranxServerInfo->SetAllNodes(allNodes);
	tranxServerInfo->SetConfig(configHelper);
	SharedResources::TranxServiceSharedData *tranxServiceSharedData =
			new SharedResources::TranxServiceSharedData(2, tranxServerInfo);
	delete tranxServiceSharedData->tranxHistory;
	tranxServiceSharedData->tranxHistory = new SharedResources::TranxHistory(10, 1000);
	RPC::TranxRPC* tranxRPC = tranxServiceSharedData->TranxHistoryBroadcast();
	EXPECT_EQ(NULL, tranxRPC);
	Service::GTranxPartition tranxPartition;
	for (int i = 0; i < 10; ++i) {
		Service::GTranxKeys *tranxKeys = tranxPartition.add_tranxs();
		tranxKeys->add_readset("key1");
		tranxKeys->add_readset("key2");
		tranxKeys->add_writeset("key3");
		tranxKeys->add_writeset("key4");
		tranxKeys->add_writeset("key5");
	}
	tranxServiceSharedData->tranxHistory->ExtractFromMessage(&tranxPartition);
	EXPECT_EQ(10, tranxServiceSharedData->tranxHistory->tranxList.size());
	tranxRPC = tranxServiceSharedData->TranxHistoryBroadcast();
	EXPECT_TRUE(tranxRPC != NULL);
	EXPECT_EQ(RPC::GServiceID::TranxService, tranxRPC->GetReqService());
	EXPECT_TRUE(tranxRPC->GetTranxRequest()->has_tranxpartition());
	EXPECT_EQ(10, tranxRPC->GetTranxRequest()->tranxpartition().tranxs_size());
	EXPECT_EQ(3, tranxRPC->GetTranxRequest()->tranxpartition().tranxs(5).writeset_size());
	delete tranxRPC;
	Util::FileUtil::RemoveDir("dtranx.mapdb");
	delete tranxServiceSharedData;
	delete tranxServerInfo;
}

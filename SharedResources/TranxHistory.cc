/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <iostream>
#include "TranxHistory.h"
#include "Util/StaticConfig.h"
#include "DTranx/Util/Log.h"

namespace DTranx {
namespace SharedResources {

TranxHistory::TranxHistory(uint64 partitionBroadcastThreshold,
		uint64 repartitionThreshold,
		uint64 inMemLimit)
		: partitionBroadcastThreshold(partitionBroadcastThreshold),
				repartitionThreshold(repartitionThreshold), inMemLimit(inMemLimit) {
}

TranxHistory::TranxHistory(const TranxHistory& other)
		: mutex() {
	partitionBroadcastThreshold = other.partitionBroadcastThreshold;
	repartitionThreshold = other.repartitionThreshold;
	tranxList = other.tranxList;
	inMemLimit = other.inMemLimit;
}

TranxHistory::~TranxHistory() noexcept(false) {
}

void TranxHistory::AddTranx(std::unordered_set<std::string>& readset,
		std::unordered_set<std::string>& writeset) {
	if (mutex.try_lock()) {
		tranxList.push_back(std::make_pair(readset, writeset));
		int excess = tranxList.size() - inMemLimit;
		if (excess > 0) {
			tranxList.erase(tranxList.begin(), tranxList.begin() + excess);
		}
		mutex.unlock();
	}
}

void TranxHistory::EncodeIntoMessage(Service::GTranxPartition* partitionRequest) {
	for (auto tranx = tranxList.begin(); tranx != tranxList.end(); ++tranx) {
		Service::GTranxKeys* tranxKeys = partitionRequest->add_tranxs();
		for (auto tranx_readkey = tranx->first.begin(); tranx_readkey != tranx->first.end();
				++tranx_readkey) {
			tranxKeys->add_readset(*tranx_readkey);
		}
		for (auto tranx_writekey = tranx->second.begin(); tranx_writekey != tranx->second.end();
				++tranx_writekey) {
			tranxKeys->add_writeset(*tranx_writekey);
		}
	}
}

void TranxHistory::ExtractFromMessage(Service::GTranxPartition* partitionRequest) {
	if (mutex.try_lock()) {
		for (auto it = partitionRequest->tranxs().begin(); it != partitionRequest->tranxs().end();
				++it) {
			tranxList.push_back(
					std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>());
			std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>& data =
					tranxList.back();
			data.first = std::unordered_set<std::string>();
			data.second = std::unordered_set<std::string>();
			for (auto key = it->readset().begin(); key != it->readset().end(); ++key) {
				data.first.insert(*key);
			}
			for (auto key = it->writeset().begin(); key != it->writeset().end(); ++key) {
				data.second.insert(*key);
			}
		}
		int excess = tranxList.size() - inMemLimit;
		if (excess > 0) {
			tranxList.erase(tranxList.begin(), tranxList.begin() + excess);
		}
		mutex.unlock();
		return;
	}
}

void TranxHistory::PrintTranxHistory() {
	std::cout << "PrintTranxHistory starts" << std::endl;
	for (auto tranx = tranxList.begin(); tranx != tranxList.end(); ++tranx) {
		std::cout << "readset: ";
		for (auto tranx_readkey = tranx->first.begin(); tranx_readkey != tranx->first.end();
				++tranx_readkey) {
			std::cout << *tranx_readkey << ", ";
		}
		std::cout << "; writeset: ";
		for (auto tranx_writekey = tranx->second.begin(); tranx_writekey != tranx->second.end();
				++tranx_writekey) {
			std::cout << *tranx_writekey << ", ";
		}
		std::cout << std::endl;
	}
	std::cout << "PrintTranxHistory ends" << std::endl;
}

bool TranxHistory::ToBroadcast(Service::GTranxPartition* partitionRequest) {
	if (mutex.try_lock()) {
		if (tranxList.size() >= partitionBroadcastThreshold) {
			VERBOSE("broadcasting tranxhistory %zu", tranxList.size());
			EncodeIntoMessage(partitionRequest);
			tranxList.clear();
			mutex.unlock();
			return true;
		}
		mutex.unlock();
	}
	return false;
}

std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>>TranxHistory::ToRePartition() {
	std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>>tmpTranxList;
	if (mutex.try_lock()) {
		if(tranxList.size() >= repartitionThreshold) {
			for (auto it = tranxList.begin(); it != tranxList.end(); ++it) {
				tmpTranxList.push_back(*it);
			}
			tranxList.clear();
		}
		mutex.unlock();
	}
	return tmpTranxList;
}

std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>>TranxHistory::ForceRePartition() {
	std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>>tmpTranxList;
	while(!mutex.try_lock());
	for (auto it = tranxList.begin(); it != tranxList.end(); ++it) {
		tmpTranxList.push_back(*it);
	}
	tranxList.clear();
	mutex.unlock();
	return tmpTranxList;
}

}
/* namespace SharedResources */
} /* namespace DTranx */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "DTranx/Util/Log.h"
#include "DTranx/RPC/ZeromqSender.h"

namespace DTranx {
namespace RPC {

ZeromqSender::ZeromqSender(std::shared_ptr<zmq::context_t> context)
		: ZeromqConnection(context, ZMQ_DEALER), zm() {
	int hwm = 0;
	size_t len = sizeof(hwm);
	socket.setsockopt(ZMQ_SNDHWM, &hwm, len);
	socket.setsockopt(ZMQ_RCVHWM, &hwm, len);
}

ZeromqSender::~ZeromqSender() noexcept(false) {
}

void ZeromqSender::Send(void *message, int size, zmq_free_fn* freeFunc) {
	/*
	 zm.clear();
	 zm.push_back((unsigned char *) message, size);
	 while (!zm.send_non_block(socket)) {
	 }
	 */
	if (freeFunc != NULL) {
		while (!zm.send_non_block_zerocopy_reclaim(socket, message, size, freeFunc)) {
		}
	} else {
		while (!zm.send_non_block_zerocopy(socket, message, size)) {
		}
	}

}

void ZeromqSender::Send(const void *message, int size, std::string ip,
		std::vector<uint64> messageIDs) {
	zm.clear();
	zm.push_back((unsigned char *) message, size);
	while (!zm.send_non_block(socket)) {
	}

	//VERBOSE("sending starts at %s", ip.c_str());
	for (auto it = messageIDs.begin(); it != messageIDs.end(); ++it) {
		VERBOSE("sending message id %lu to peer %s", *it, ip.c_str());
	}
	//VERBOSE("sending ends at %s", ip.c_str());
}

} /* namespace RPC */
} /* namespace DTranx */

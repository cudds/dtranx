/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "AssignParticipantsStrategy.h"
#include "Util/FileUtil.h"

using namespace DTranx;

TEST(AssignParticipantsStrategy, InnerJoin) {
	std::string selfAddress = "192.168.0.1";

	std::vector<std::string> allNodes;
	allNodes.push_back("192.168.0.1");
	allNodes.push_back("192.168.0.2");
	allNodes.push_back("192.168.0.3");
	allNodes.push_back("192.168.0.4");
	allNodes.push_back("192.168.0.5");
	Util::ConfigHelper configHelper;
	configHelper.readFile("DTranx.conf");
	SharedResources::TranxServerInfo* tranxServerInfo = new SharedResources::TranxServerInfo();
	tranxServerInfo->SetAllNodes(allNodes);
	tranxServerInfo->SetConfig(configHelper);
	SharedResources::TranxServiceSharedData* tranxServiceSharedData =
			new SharedResources::TranxServiceSharedData(1, tranxServerInfo);
	Service::AssignParticipantsStrategy strategy(selfAddress, tranxServiceSharedData);
	std::unordered_set<std::string> set1, set2;
	set1.insert("192.168.0.1");
	set2.insert("192.168.0.1");
	strategy.InnerJoin(set1, set2);
	EXPECT_EQ(1, set1.size());
	delete tranxServerInfo;
	delete tranxServiceSharedData;
	Util::FileUtil::RemoveDir("dtranx.mapdb");
}

TEST(AssignParticipantsStrategy, MapItem) {
	std::string selfAddress = "192.168.0.1";

	std::vector<std::string> allNodes;
	allNodes.push_back("192.168.0.1");
	allNodes.push_back("192.168.0.2");
	allNodes.push_back("192.168.0.3");
	allNodes.push_back("192.168.0.4");
	allNodes.push_back("192.168.0.5");
	Util::ConfigHelper configHelper;
	configHelper.readFile("DTranx.conf");
	SharedResources::TranxServerInfo* tranxServerInfo = new SharedResources::TranxServerInfo();
	tranxServerInfo->SetAllNodes(allNodes);
	tranxServerInfo->SetConfig(configHelper);
	SharedResources::TranxServiceSharedData* tranxServiceSharedData =
			new SharedResources::TranxServiceSharedData(1, tranxServerInfo);

	std::unordered_set<std::string> mapping;
	mapping.insert("192.168.0.1");
	mapping.insert("192.168.0.2");
	mapping.insert("192.168.0.3");
	tranxServiceSharedData->getMappingStorage()->UpdateLookupTable("key1", mapping);
	mapping.clear();
	mapping.insert("192.168.0.3");
	mapping.insert("192.168.0.4");
	mapping.insert("192.168.0.5");
	tranxServiceSharedData->getMappingStorage()->UpdateLookupTable("key2", mapping);

	Service::GClientCommit::Request request;
	Service::GItem *item = request.add_read_set();
	item->set_key("key1");
	item = request.add_write_set();
	item->set_key("key2");
	Service::AssignParticipantsStrategy strategy(selfAddress, tranxServiceSharedData);

	/*
	 * both these read/write items share the same server: "192.168.0.3"
	 */
	std::unordered_map<std::string,
			std::pair<std::vector<Service::GItem>, std::vector<Service::GItem>> > mappedItems =
			strategy.MapItem(request.read_set(), request.write_set());
	EXPECT_EQ(1, mappedItems.size());
	EXPECT_EQ("192.168.0.3", mappedItems.begin()->first);
	delete tranxServerInfo;
	delete tranxServiceSharedData;
	Util::FileUtil::RemoveDir("dtranx.mapdb");
}

TEST(AssignParticipantsStrategy, MapItem2) {
	std::string selfAddress = "192.1.1.7";

	std::vector<std::string> allNodes;
	allNodes.push_back("192.1.1.1");
	allNodes.push_back("192.1.1.2");
	allNodes.push_back("192.1.1.3");
	allNodes.push_back("192.1.1.4");
	allNodes.push_back("192.1.1.5");
	allNodes.push_back("192.1.1.6");
	allNodes.push_back("192.1.1.7");

	Util::ConfigHelper configHelper;
	configHelper.readFile("DTranx.conf");
	SharedResources::TranxServerInfo *tranxServerInfo = new SharedResources::TranxServerInfo();
	tranxServerInfo->SetAllNodes(allNodes);
	tranxServerInfo->SetConfig(configHelper);
	SharedResources::TranxServiceSharedData *tranxServiceSharedData =
			new SharedResources::TranxServiceSharedData(1, tranxServerInfo);

	std::unordered_set<std::string> mapping;

	mapping.insert(selfAddress);
	mapping.insert("192.1.1.1");
	mapping.insert("192.1.1.2");
	tranxServiceSharedData->getMappingStorage()->UpdateLookupTable("key2", mapping);

	mapping.clear();
	mapping.insert("192.1.1.2");
	mapping.insert("192.1.1.3");
	mapping.insert("192.1.1.4");
	tranxServiceSharedData->getMappingStorage()->UpdateLookupTable("key1", mapping);

	mapping.clear();
	mapping.insert("192.1.1.4");
	mapping.insert("192.1.1.5");
	mapping.insert("192.1.1.6");
	tranxServiceSharedData->getMappingStorage()->UpdateLookupTable("key3", mapping);

	Service::GClientCommit::Request request;
	Service::GItem *item = request.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item = request.add_read_set();
	item->set_key("key2");
	item->set_value("value2_2");
	item = request.add_read_set();
	item->set_key("key3");
	item->set_value("value3_3");
	EXPECT_EQ(3, request.read_set_size());
	EXPECT_EQ(0, request.write_set_size());

	Service::AssignParticipantsStrategy strategy(selfAddress, tranxServiceSharedData);
	std::unordered_map<std::string,
			std::pair<std::vector<Service::GItem>, std::vector<Service::GItem>> > result =
			strategy.MapItem(request.read_set(), request.write_set());
	EXPECT_EQ(2, result.size());
	EXPECT_TRUE(result.find(selfAddress) != result.end());
	EXPECT_EQ(1, result[selfAddress].first.size());
	EXPECT_EQ("key2", result[selfAddress].first[0].key());
	EXPECT_TRUE(result.find("192.1.1.4") != result.end());
	EXPECT_EQ(2, result["192.1.1.4"].first.size());
	delete tranxServerInfo;
	delete tranxServiceSharedData;
	Util::FileUtil::RemoveDir("dtranx.mapdb");
}

TEST(AssignParticipantsStrategy, MapItem3) {
	std::string selfAddress = "192.1.1.7";

	std::vector<std::string> allNodes;
	allNodes.push_back("192.1.1.1");
	allNodes.push_back("192.1.1.2");
	allNodes.push_back("192.1.1.3");
	allNodes.push_back("192.1.1.4");
	allNodes.push_back("192.1.1.5");
	allNodes.push_back("192.1.1.6");
	allNodes.push_back("192.1.1.7");

	Util::ConfigHelper configHelper;
	configHelper.readFile("DTranx.conf");
	SharedResources::TranxServerInfo *tranxServerInfo = new SharedResources::TranxServerInfo();
	tranxServerInfo->SetAllNodes(allNodes);
	tranxServerInfo->SetConfig(configHelper);
	SharedResources::TranxServiceSharedData *tranxServiceSharedData =
			new SharedResources::TranxServiceSharedData(1, tranxServerInfo);

	std::unordered_set<std::string> mapping;

	mapping.insert(selfAddress);
	mapping.insert("192.1.1.1");
	mapping.insert("192.1.1.2");
	tranxServiceSharedData->getMappingStorage()->UpdateLookupTable("key2", mapping);

	mapping.clear();
	mapping.insert("192.1.1.2");
	mapping.insert("192.1.1.3");
	mapping.insert("192.1.1.4");
	tranxServiceSharedData->getMappingStorage()->UpdateLookupTable("key1", mapping);

	mapping.clear();
	mapping.insert("192.1.1.4");
	mapping.insert("192.1.1.5");
	mapping.insert("192.1.1.6");
	tranxServiceSharedData->getMappingStorage()->UpdateLookupTable("key3", mapping);

	Service::GClientCommit::Request request;
	Service::GItem *item = request.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item = request.add_read_set();
	item->set_key("key3");
	item->set_value("value3_3");
	item = request.add_write_set();
	item->set_key("key2");
	item->set_value("value2_2");
	EXPECT_EQ(2, request.read_set_size());
	EXPECT_EQ(1, request.write_set_size());

	Service::AssignParticipantsStrategy strategy(selfAddress, tranxServiceSharedData);
	std::unordered_map<std::string, std::unordered_set<std::string>> readItemMapping;
	std::unordered_map<std::string, std::unordered_set<std::string>> writeItemMapping;
	std::unordered_map<std::string,
			std::pair<std::vector<Service::GItem>, std::vector<Service::GItem>> > result =
			strategy.MapItem(request.read_set(), request.write_set(), &readItemMapping,
					&writeItemMapping);
	EXPECT_EQ(4, result.size());
	EXPECT_TRUE(result.find(selfAddress) != result.end());
	EXPECT_EQ(1, result[selfAddress].second.size());
	EXPECT_EQ("key2", result[selfAddress].second[0].key());
	EXPECT_TRUE(result.find("192.1.1.6") != result.end());
	EXPECT_EQ(1, result["192.1.1.6"].first.size());
	EXPECT_EQ(1, writeItemMapping.size());
	EXPECT_TRUE(writeItemMapping.find("key2") != writeItemMapping.end());
	EXPECT_EQ(3, writeItemMapping["key2"].size());
	EXPECT_TRUE(writeItemMapping["key2"].find("192.1.1.2") != writeItemMapping["key2"].end());
	delete tranxServerInfo;
	delete tranxServiceSharedData;
	Util::FileUtil::RemoveDir("dtranx.mapdb");
}

#!/usr/bin/env python
import glob
import re
import string
SPACE = ' '
class LogKind:
	SILENT = 0
	ERROR = 1
	WARNING = 2
	NOTICE = 3
	VERBOSE = 4
	PROFILE = 5
	INVALID = 6
	@staticmethod
	def Convert(str):
		if str == "SILENT":
			return LogKind.SILENT
		elif str == "ERROR":
			return LogKind.ERROR
		elif str == "WARNING":
			return LogKind.WARNING
		elif str == "NOTICE":
			return LogKind.NOTICE
		elif str == "VERBOSE":
			return LogKind.VERBOSE
		elif str == "PROFILE":
			return LogKind.PROFILE
		return LogKind.INVALID
		
'''
	GetLineLogKind returns the log kind
	the field is the string(split from the line) that contains the log kind information
	e.g. VERBOSE[10744:12027]:
'''
def GetLineLogKind(field):
	end = field.find('[')
	if end == -1:
		print line+": format error, no thread number"
		return LogKind.INVALID
	logKindStr = field[0:end]
	return LogKind.Convert(logKindStr)

'''
	Read the next log, which might span multiple lines
	GetNextLogLine uses the timestamp format to check whether log ends or not
	note that it doesn't skip empty logs

	it tries to match 1466541665.906142 at the start of the line

	return value:
		whole line with newlines
'''
def GetNextLogLine(logF, binaryPrint):
	log = ""
	start  = True
	#lastRead used to rewind
	lastRead = logF.tell()
	line = logF.readline()
	while line:
		fields = line.split(SPACE)
		if len(fields) == 0:
			log += line
		else:
			matchObj = re.match(r'\d{10}\.\d{6}',fields[0], re.I)
			if matchObj:
				if start:
					log = line
					start = False
				else:
					logF.seek(lastRead)
					break
			else:
				log += line
		lastRead = logF.tell()
		line = logF.readline()
	if binaryPrint:
		print ':'.join(x.encode('hex') for x in log)
	return log

'''
	GetLogFromFile reads the whole log and returns all logs that belong to logKind
	printLogNum denotes the log to be printed, if it equals -1, none is printed, the logNum only increments when the one is the logKind
'''
def GetLogFromFile(logKind, logName, printLine):
	logs = []
	logNum = 1
	try:
		logF = open(logName,'r')
		if logNum == printLine:
			log = GetNextLogLine(logF, True)
		else:
			log = GetNextLogLine(logF, False)
		logNum = logNum + 1
		while log:
			log = log[:-1]
			if log == "":
				continue
			fields = log.split(SPACE)
			if len(fields) <= 5:
				print log+": format error, fields less than 5"
				return
			if GetLineLogKind(fields[4]) != logKind:
				log = GetNextLogLine(logF, False)
				continue
			pureLog = " ".join(fields[5:])
			logs.append(pureLog)
			if logNum == printLine:
				print "printing log ", logNum
				log = GetNextLogLine(logF, True)
			else:
				log = GetNextLogLine(logF, False)
			logNum = logNum + 1
	except IOError:
		print logName + ": file not exist"
	return logs

def GetLogNamesFromRegex(regex):
	return glob.glob(regex)
/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * RecoverLog will read through the log, recover the volatile states, then start a transient
 * stage thread for post recovery.
 *
 * Note: Recover has the privilege to access resources directly without using stage queues
 * because during initialization, there's only this thread that are accessing the resources.
 *
 */

#ifndef DTRANX_SERVER_RECOVERLOG_H_
#define DTRANX_SERVER_RECOVERLOG_H_

#include "Service/AssignParticipantsStrategy.h"
#include "DaemonStages/DaemonStages.h"

namespace DTranx {
namespace DaemonStages {
class TranxAck;
class Repartition;
}
namespace Service {
class TranxService;
}
namespace Server {

class RecoverLog: public DaemonStages::DaemonStages {
public:
	RecoverLog(SharedResources::TranxServerInfo *tranxServerInfo,
			SharedResources::TranxServiceSharedData *tranxServiceSharedData);
	virtual ~RecoverLog();

	/*
	 * Recover log has the privilege to call stage class functions without locks
	 * main function of Recover is to relock, reapply committed/aborted transactions,
	 * 	initialize shared data items like curTranx.
	 */
	void Recover();

	void InitStages(Stages::TranxRPCHelper *tranxRPCHelper,
			Stages::TranxStates *tranxStates,
			Stages::StringStorage *localStorage,
			Stages::Log::TranxLog *tranxLog,
			Stages::Lock::WriteBlockLockTable *lockTable,
			DTranx::DaemonStages::TranxAck *tranxAck,
			Service::TranxService *tranxService,
			DTranx::DaemonStages::Repartition *repartition) {
		DaemonStages::InitStages(tranxRPCHelper, tranxStates, localStorage, tranxLog, lockTable);
		this->tranxAck = tranxAck;
		this->tranxService = tranxService;
		this->repartition = repartition;
	}
	virtual void StartService(std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) {
		bool enableCoreBinding = false;
		uint32_t coreIDIndex = 0;
		enableCoreBinding = (coreIDs.size() == 1);
		daemonThread = boost::thread(&RecoverLog::DaemonThread, this);
		if (enableCoreBinding) {
			Util::ThreadHelper::PinToCPUCore(daemonThread, coreIDs[coreIDIndex++]);
		}
	}

private:
	/**************************************************************************************
	 * Shared Data
	 *
	 * tranxServerInfo is both initialized and used
	 * 	nextTranxID is initialized
	 * 	others are used
	 *
	 * tranxServiceSharedData is both initialized and used
	 * 	mappingStorage is used
	 * 	curTranx, snapshotTranx are initialized
	 */

	/**************************************************************************************
	 * tranxAck, repartition are used by the PostRecovery to send ack and complete repartition transaction
	 * tranxService is used to initialize the newMappings for participants.
	 */
	DTranx::DaemonStages::TranxAck *tranxAck;
	DTranx::DaemonStages::Repartition *repartition;
	Service::TranxService *tranxService;

	/**************************************************************************************
	 * local data
	 *
	 * toAckCommit/toAckAbort are filled by Recover and executed by PostRecovery
	 * toAckTranxType: for post recovery usage, it only stores transactions with this node as the coordinator
	 * 	it is needed because tranxcommit request requires for Tranx Type information
	 *
	 * tranxInDoubt: transactions that is either prepared or ready
	 *
	 * toRecover: used in the PostRecovery thread check whether all pending transactions are recovered.
	 */
	std::unordered_map<Util::TranxID, std::vector<std::string>, Util::KeyHasher> toAckCommit;
	std::unordered_map<Util::TranxID, std::vector<std::string>, Util::KeyHasher> toAckAbort;
	std::unordered_map<Util::TranxID, Service::GTranxType, Util::KeyHasher> toAckTranxType;
	std::unordered_map<Util::TranxID, std::vector<Stages::Log::GTranxLogRecord>, Util::KeyHasher> tranxInDoubt;
	std::unordered_set<Util::TranxID, Util::KeyHasher> toRecover;
	Service::AssignParticipantsStrategy assignParticipantsStrategy;

	/**************************************************************************************
	 * Log related functions
	 * HoldLockFromLog/ReleaseLockFromLog is used to rehold/release locks for prepare/ready logs
	 *
	 * note: abort log might happen before ready log
	 *
	 * Only Recover function calls the following utility functions
	 */
	void HoldLockFromLog(Stages::Log::GTranxLogRecord &entry);
	void ReleaseLockFromLog(Stages::Log::GTranxLogRecord &entry);
	void ProcessLog(Stages::Log::GTranxLogRecord entry);
	void ApplyLog(Stages::Log::GTranxLogRecord &logEntry);
	void ApplyLogs(std::vector<Stages::Log::GTranxLogRecord> &logVec);

	virtual void DaemonThread();

	/**************************************************************************************
	 * Process functions(called by PostRecovery thread)
	 * 	PostRecoveryProcess: deal with internal stages
	 * 	ProcessTranxInDoubt: start to process tranxInDoubt
	 * 	ProcessToAck: start to process transaction that needs to be ack'ed
	 */
	void PostRecoveryProcess(Stages::TranxServiceQueueElement *element);
	void ProcessTranxInDoubt();
	void ProcessToAck();
};

} /* namespace Server */
} /* namespace DTranx */

#endif /* DTRANX_SERVER_RECOVERLOG_H_ */

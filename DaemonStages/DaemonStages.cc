/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "DaemonStages.h"
#include "Stages/TranxRPCHelper.h"
#include "Stages/TranxStates.h"
#include "Stages/Log/TranxLog.h"
#include "Stages/Lock/WriteBlockLockTable.h"
#include "Stages/Storage.h"

namespace DTranx {
namespace DaemonStages {

void DaemonStages::InitStages(Stages::TranxRPCHelper *tranxRPCHelper,
		Stages::TranxStates *tranxStates,
		Stages::StringStorage *localStorage,
		Stages::Log::TranxLog *tranxLog,
		Stages::Lock::WriteBlockLockTable *lockTable) {
	this->tranxRPCHelper = tranxRPCHelper;
	this->tranxStates = tranxStates;
	this->localStorage = localStorage;
	this->tranxLog = tranxLog;
	this->lockTable = lockTable;

	//ptok_tranxStates = this->tranxStates->CreateDispatchQueueToken();
}
}
}


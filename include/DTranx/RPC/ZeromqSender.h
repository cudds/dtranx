/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 * ZeromqSender is a wrapper around zeromq interfaces so that Server class doesn't have to deal with context, socket mode etc.
 * it's the sender end of RPC calls
 */

#ifndef DTRANX_RPC_ZEROMQSENDER_H_
#define DTRANX_RPC_ZEROMQSENDER_H_

#include "DTranx/RPC/ZeromqConnection.h"
#include "DTranx/Util/types.h"

namespace DTranx {
namespace RPC {

class ZeromqSender: public ZeromqConnection {
public:
	/*
	 * call Connect before use
	 */
	explicit ZeromqSender(std::shared_ptr<zmq::context_t> context);
	virtual ~ZeromqSender() noexcept(false);
	void Send(void *message, int size, zmq_free_fn* freeFunc = NULL);
	/*
	 * For performance monitoring: message latency
	 */
	void Send(const void *message, int size, std::string ip, std::vector<uint64> messageIDs);

private:
	zmsg zm;
};

} /* namespace RPC */
} /* namespace DTranx */

#endif /* DTRANX_RPC_ZEROMQSENDER_H_ */

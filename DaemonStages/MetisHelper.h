/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * MetisHelper is the interface to metis algorithm. It supports input/output from memory and disk files
 *
 * graph interpretation:
 * 	vertex is key
 * 	edge weight between vertices means # of transaction access these two keys together
 *
 * Single threaded
 * Note: metis only deals with undirected graph, however, both edge should be in adjncy list
 *
 */

#ifndef DTRANX_DAEMONSTAGES_METISHELPER_H_
#define DTRANX_DAEMONSTAGES_METISHELPER_H_

#include <unordered_set>
#include <set>
#include <vector>
#include <map>
#include <unordered_map>
#include <list>
#include <string>
#include <metis.h>

namespace DTranx {
namespace DaemonStages {

class MetisHelper {
public:
	static const int NO_PARTITION_FOUND;
	static const int INVALID_VERTEXID;
	static const int VOID_VERTEXID;

	MetisHelper(bool replicationEnabled = false);
	virtual ~MetisHelper() noexcept(false);

	/*
	 * AddTranx assumes no overlap between readSet and writeSet
	 */
	void AddTranx(std::unordered_set<std::string> readSet,
					std::unordered_set<std::string> writeSet);
	bool RunMetis(int numOfPartition);
	/*
	 * the returned partitionID starts from 0;
	 */
	int GetPartitions(std::string key);
	std::unordered_set<int> GetPartitionsR(std::string key);
	void GetNewMappingsR(std::unordered_map<std::string, std::unordered_set<int> > &newMapping);
private:
	/*
	 * mapping to internal graph representation
	 */
	int ConvertKeyToVertexID(std::string key);
	std::string ConvertVertexIDToKey(int vertexID);
	int nextVertexID;
	std::unordered_map<std::string, int> keyToVertexID;
	std::vector<std::string> vertexIDToKey;

	/*
	 * graph data structure for non replicated
	 * 	index(vertex ID): vertext2->weight
	 * partitionResult structure
	 * 	index(vertex ID): partition ID
	 */
	std::vector<std::unordered_map<int, int> > graph;
	std::vector<int> partitionResult;
	/*
	 * graph data structure for replicated
	 * 	stage1:(value: list of tranx)
	 * 	index(vertex ID): vertex2, vertex3	count index-in-map(starts from 0)
	 * 					vertext2, vertex4	count index-in-map
	 * 					...
	 * 	stage2:(key: new replicated vertexID; value: list of neighbors)
	 * 	all the vertices ID's are replicated vertices ID
	 * 	index(vertex ID): vertex2, vertex3	count/freq
	 * 					vertex2, vertex4	count/freq
	 * 					...
	 * 	vertexMappingR: mapping data to replication
	 * 		vertexID: start -> end, which means vertexID is mapped to start, start+1 ,..., end-1, end
	 * partitionResultR structure
	 * 	index(vertex ID): partition ID list
	 */
	struct Vertex {
		int numOfUpdate;
		/*
		 * tranxs is a list of transactions, each transaction is a set of vertex ID.
		 * all data items including this vertex itself is included in tranxs for ordering purposes.
		 *
		 * value of tranxs is a pair(frequency, index)
		 * 	frequency means how many times the same transactions have existed before
		 * 	index is used in Stage2 graph as the index for a transaction for each vertex
		 */
		std::map<std::set<int>, std::pair<int, int>> tranxs;
		Vertex()
				: numOfUpdate(1) {
		}
	};
	std::vector<struct Vertex> graphR_S1;
	std::vector<std::pair<std::list<int>, int>> graphR_S2;
	std::vector<std::pair<int, int> > vertexMappingR;
	std::vector<std::unordered_set<int> > partitionResultR;
	/*
	 * FillOldTranxRepVertex is a helper function to retrieve the replicated vertex ID that is already assigned when
	 * 	Vertex with the smallest vertexID in that transaction was first assigned
	 * _ConvertToStage2 converts stage1 graph data to stage2 graph data where all the vertices are replicated and mapped
	 */
	std::unordered_map<int, int> FillOldTranxRepVertex(
			const std::map<std::set<int>, std::pair<int, int>>::iterator& it, int prevVertexID);
	void _ConvertToStage2();
	void _AddTranxR(std::unordered_set<std::string> readSet,
					std::unordered_set<std::string> writeSet);
	void _PostProcessResult();
	bool _RunMetisR(int numOfPartition);
	bool _CheckGraph(idx_t nvtxs, idx_t *xadj, idx_t *adjncy, idx_t *adjwgt);

	bool replicationEnabled;

};

} /* namespace DaemonStages */
} /* namespace DTranx */

#endif /* DTRANX_DAEMONSTAGES_METISHELPER_H_ */

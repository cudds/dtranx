/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * ServerReply provides the functionalities of replying to clients/peer servers
 * no synchronization is needed because only one thread is accessing the socket
 * and moodycamel queue is thread safe
 */

#ifndef DTRANX_STAGES_SERVERREPLY_H_
#define DTRANX_STAGES_SERVERREPLY_H_

#include <unordered_map>
#include <unordered_set>
#include <boost/thread.hpp>
#include <condition_variable>
#include "DTranx/Util/ConfigHelper.h"
#include "RPC/ServerRPC.h"
#include "DTranx/RPC/ZeromqSender.h"
#include "SharedResources/TranxServerInfo.h"
#include "DTranx/Util/ConcurrentQueue.h"
#include "Util/ThreadHelper.h"
#include "PerfStage.h"

namespace DTranx {
namespace Server {
class Server;
}
namespace Stages {

class ServerReply: public PerfStage {
public:
	explicit ServerReply(SharedResources::TranxServerInfo *tranxServerInfo);
	virtual ~ServerReply() noexcept(false);

	static std::unordered_map<std::string, std::unordered_set<uint32_t> > InitializePeerPorts(
			std::vector<std::string> allNodes, std::vector<uint32_t> allPorts,
			std::string selfAddress);

	void SendReplyPeer(RPC::ServerRPC *serverRPC);
	void SendReplyClient(RPC::ServerRPC *serverRPC);

	void StartStage(std::vector<uint32_t> coreIDs = std::vector<uint32_t>(), bool splitThread =
	false) {
		bool enableCoreBinding = false;
		uint32_t coreIDIndex = 0;
		if (splitThread) {
			enableCoreBinding = (coreIDs.size() == 2);
			peerReplyThread = boost::thread(&ServerReply::PeerReplyThread, this);
			if (enableCoreBinding) {
				Util::ThreadHelper::PinToCPUCore(peerReplyThread, coreIDs[coreIDIndex++]);
			}
			clientReplyThread = boost::thread(&ServerReply::ClientReplyThread, this);
			if (enableCoreBinding) {
				Util::ThreadHelper::PinToCPUCore(clientReplyThread, coreIDs[coreIDIndex++]);
			}
		} else {
			enableCoreBinding = (coreIDs.size() == 1);
			replyThread = boost::thread(&ServerReply::ReplyThread, this);
			if (enableCoreBinding) {
				Util::ThreadHelper::PinToCPUCore(replyThread, coreIDs[coreIDIndex++]);
			}
		}

	}

	void SetServer(Server::Server *server) {
		this->server = server;
	}

private:
	/*
	 * PeerReplyThread is responsible for sending replies to peer servers
	 * ServerRPC should be deleted in this thread
	 * for each peer, there might be multiple sockets available, right now use only one
	 */
	std::atomic_bool terminateServerRPCReplyThread;
	void PeerReplyThread();
	boost::thread peerReplyThread;
	std::unordered_map<std::string, RPC::ZeromqSender *> peerSenders;
	moodycamel::ConcurrentQueue<RPC::ServerRPC*> peerReplyQueue;

	/*
	 * separate reply for client requests
	 * clientAllMutex protects both clientSenders and clientMutexes when inserting
	 */
	void ClientReplyThread();
	boost::thread clientReplyThread;
	std::unordered_map<std::string, RPC::ZeromqSender *> clientSenders;
	moodycamel::ConcurrentQueue<RPC::ServerRPC *> clientReplyQueue;

	void ReplyThread();
	boost::thread replyThread;

	/*
	 * reuse serverRPC
	 */
	Server::Server *server;

	Util::ConfigHelper config;
	std::shared_ptr<zmq::context_t> context;
	std::unordered_map<std::string, std::unordered_set<uint32_t> > remotePeerPorts;
	SharedResources::TranxServerInfo::Mode mode;

	/*
	 * buffer space to reduce memory allocation/deletion
	 */
	Util::Buffer localBufferPeer;
	Util::Buffer localBufferClient;
	Util::Buffer localBufferClientForEndpoint;
	Util::Buffer localBufferClientForIP;
	uint32_t bufferSize;
};

extern uint32_t clientResponseBufferSize;
extern moodycamel::ConcurrentQueue<void*> responseBufferForClients;
void ReclaimClientResponsesFromZeromq(void *data, void *hint);

} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_SERVERREPLY_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * IMPORTANT: set log policy at start to avoid concurrent RW access to policy because
 * the mutex are too time consuming and has been removed to protect policy and
 * cache, which has also been removed.
 */

#ifndef DTRANX_UTIL_LOG_H_
#define DTRANX_UTIL_LOG_H_

#include <cstdlib>
#include <sys/types.h>
#include <unistd.h>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include <vector>
#include <sys/syscall.h>

namespace DTranx {
namespace Util {
namespace Log {

//all the codes can call syscall(__NR_gettid)
#define gettid() syscall(__NR_gettid);

/**
 * The levels of verbosity for log messages. Higher values are noisier.
 */
enum class LogLevel {
	// Keep this in sync with logLevelToString.
	/**
	 * This log level is just used for disabling all log messages, which is
	 * really only useful in unit tests.
	 */
	SILENT = 0,
	/**
	 * Bad stuff that shouldn't happen. The system broke its contract to users
	 * in some way or some major assumption was violated.
	 * See the ERROR macro below.
	 */
	ERROR = 1,
	/**
	 * Messages at the WARNING level indicate that, although something went
	 * wrong or something unexpected happened, it was transient and
	 * recoverable.
	 * See WARNING macro below.
	 */
	WARNING = 2,
	/**
	 * A system message that might be useful for administrators and developers.
	 * See NOTICE macro below.
	 */
	NOTICE = 3,
	/**
	 * Messages at the VERBOSE level don't necessarily indicate that anything
	 * went wrong, but they could be useful in diagnosing problems.
	 * See VERBOSE macro below.
	 */
	VERBOSE = 4,

	PROFILE = 5,
};

/**
 * Return whether the current logging configuration includes messages of
 * the given level for the given filename.
 * This is normally called by LOG().
 * \warning
 *      fileName must be a string literal!
 * \param level
 *      The log level to query.
 * \param fileName
 *      This should be a string literal, probably __FILE__, since the result of
 *      this call will be cached based on the memory address pointed to by
 *      'fileName'.
 */
bool
isLogging(LogLevel level, const char* fileName);

/**
 * Unconditionally log the given message to stderr.
 * This is normally called by LOG().
 * \param level
 *      The level of importance of the message.
 * \param fileName
 *      The output of __FILE__.
 * \param lineNum
 *      The output of __LINE__.
 * \param functionName
 *      The output of __FUNCTION__.
 * \param format
 *      A printf-style format string for the message. It should include a line
 *      break at the end.
 * \param ...
 *      The arguments to the format string, as in printf.
 */
void
log(LogLevel level, const char* fileName, uint32_t lineNum,
		const char* functionName, const char* format, ...)
				__attribute__((format(printf, 5, 6)));

void
profile(const char* fileName, uint32_t lineNum,
		const char* functionName, std::string profile);

/**
 * A short name to be used in log messages to identify this process.
 * This defaults to the UNIX process ID.
 */
extern std::string processName;

void setLogPolicy(
		const std::vector<std::pair<std::string, std::string>>& newPolicy);

} /* namespace Log */
} /* namespace Util */

/**
 * Unconditionally log the given message to stderr.
 * This is normally called by ERROR(), WARNING(), NOTICE(), or VERBOSE().
 * \param level
 *      The level of importance of the message.
 * \param format
 *      A printf-style format string for the message. It should not include a
 *      line break at the end, as LOG will add one.
 * \param ...
 *      The arguments to the format string, as in printf.
 */
#define LOG(level, format, ...) do { \
    if (::DTranx::Util::Log::isLogging(level, __FILE__)) { \
    	::DTranx::Util::Log::log(level, \
            __FILE__, __LINE__, __FUNCTION__, \
            format "\n", ##__VA_ARGS__); \
    } \
} while (0)

/**
 * Log an ERROR message and abort the process.
 * \copydetails ERROR
 */
#define PANIC(format, ...) do { \
    ERROR(format " Exiting...", ##__VA_ARGS__); \
    ::abort(); \
} while (0)

/**
 * Log an ERROR message.
 * \param format
 *      A printf-style format string for the message. It should not include a
 *      line break at the end, as LOG will add one.
 * \param ...
 *      The arguments to the format string, as in printf.
 */
#define ERROR(format, ...) \
    LOG((::DTranx::Util::Log::LogLevel::ERROR), format, ##__VA_ARGS__)

/**
 * Log a WARNING message.
 * \copydetails ERROR
 */
#define WARNING(format, ...) \
    LOG((::DTranx::Util::Log::LogLevel::WARNING), format, ##__VA_ARGS__)

/**
 * Log a NOTICE message.
 * \copydetails ERROR
 */
#define NOTICE(format, ...) \
    LOG((::DTranx::Util::Log::LogLevel::NOTICE), format, ##__VA_ARGS__)

/**
 * Log a VERBOSE message.
 * \copydetails ERROR
 */
#define VERBOSE(format, ...) \
    LOG((::DTranx::Util::Log::LogLevel::VERBOSE), format, ##__VA_ARGS__)

/**
 * Log a PROFILE message.
 * \copydetails ERROR
 */
#define PROFILE(str) do { \
    	::DTranx::Util::Log::profile(\
            __FILE__, __LINE__, __FUNCTION__, \
            str); \
} while (0)

} /* namespace DTranx */


#endif /* DTRANX_UTIL_LOG_H_ */

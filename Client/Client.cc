/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <memory>
#include "DTranx/Client/Client.h"
#include "Util/StaticConfig.h"
#include "DTranx/Util/Log.h"
#include "RPC/ClientRPC.h"
#include "Util/StringUtil.h"

namespace DTranx {
namespace Client {

Client::Client(const std::string& ip, uint32_t port, std::shared_ptr<zmq::context_t> context)
		: context(context), responses(), terminateSocketThreads(false), selfPort() {
	NOTICE("Creating Clients");
	sender = std::unique_ptr<RPC::ZeromqSender>(new RPC::ZeromqSender(context));
	sender->ConnectTcp(ip, std::to_string(port));
	nextMessageId = std::strtoull(Util::StaticConfig::GetInstance()->Read("FirstID").c_str(), NULL,
			10);
	pollWaitTime = std::stoull(Util::StaticConfig::GetInstance()->Read("ClientPollPeriod"));
	rpcTimeout = std::stoll(Util::StaticConfig::GetInstance()->Read("RPCTimeout"));
}

Client::~Client() noexcept(false) {
	terminateSocketThreads.store(true);
	if (recvThread.joinable()) {
		recvThread.join();
	}
	if (receiver.get() != NULL) {
		assert(recvMutexes.size() == NUM_OF_RECV_QUEUE);
		assert(recvCVs.size() == NUM_OF_RECV_QUEUE);
		for (int i = 0; i < NUM_OF_RECV_QUEUE; ++i) {
			delete recvMutexes[i];
			delete recvCVs[i];
		}
	}
}

bool Client::Bind(const std::string& selfAddressStr, uint32_t selfPort) {
	try {
		for (int i = 0; i < NUM_OF_RECV_QUEUE; ++i) {
			recvMutexes[i] = new boost::mutex();
			recvCVs[i] = new boost::condition_variable();
		}
	} catch (std::bad_alloc &e) {
		std::cout << e.what() << std::endl;
		assert(false);
	}
	receiver = std::unique_ptr<RPC::ZeromqReceiver>(new RPC::ZeromqReceiver(context));
	this->selfAddressStr = selfAddressStr;
	this->selfPort = selfPort;
	bool result = receiver->Bind(selfAddressStr, std::to_string(selfPort));
	if (!result) {
		delete receiver.release();
		return result;
	}
	recvThread = boost::thread(&Client::RecvThread, this);
	return result;
}

void Client::CallRPC(RPC::ClientRPC* clientRPC) {
	Util::Buffer localBuffer;
	RPC::BaseRPC::serialize(*clientRPC->GetRequest(), localBuffer, 0);
	sendMutex.lock();
	sender->Send(localBuffer.GetData(), localBuffer.GetLength());
	sendMutex.unlock();
}

void Client::Retry(RPC::ClientRPC* clientRPC) {
	Util::Buffer localBuffer;
	RPC::BaseRPC::serialize(*clientRPC->GetRequest(), localBuffer, 0);
	sendMutex.lock();
	sender->Send(localBuffer.GetData(), localBuffer.GetLength());
	sendMutex.unlock();
}

void Client::Cancel(RPC::ClientRPC* clientRPC) {
}

bool Client::Poll(RPC::ClientRPC *clientRPC) {
	uint64 messageID = clientRPC->GetReqMessageID();
	int indexQueue = messageID % NUM_OF_RECV_QUEUE;
	if (responses[indexQueue].find(messageID) != responses[indexQueue].end()) {
		clientRPC->SetResponse(responses[indexQueue][messageID]);
		responses[indexQueue].erase(messageID);
		return true;
	}
	return false;
}

bool Client::BlockPoll(RPC::ClientRPC *clientRPC) {
	bool received;
	uint64 messageID = clientRPC->GetReqMessageID();
	int indexQueue = messageID % NUM_OF_RECV_QUEUE;
	boost::unique_lock<boost::mutex> lockGuard(*recvMutexes[indexQueue]);
	while (true) {
		boost::chrono::steady_clock::time_point now = boost::chrono::steady_clock::now();
		uint64 timePassed = 0;
		while (timePassed < rpcTimeout) {
			received = Poll(clientRPC);
			if (!received) {
				boost::chrono::steady_clock::time_point end = boost::chrono::steady_clock::now();
				timePassed =
						boost::chrono::duration_cast<boost::chrono::milliseconds>(end - now).count();
			} else {
				break;
			}
			recvCVs[indexQueue]->wait_for(lockGuard, boost::chrono::milliseconds(pollWaitTime));
		}
		if (received) {
			break;
		}
		VERBOSE("clientRPC retrying");
		Retry(clientRPC);
	}
	return true;
}

uint64 Client::GetNextMessageId() {
	return nextMessageId++;
}

void Client::RecvThread() {
	NOTICE("Client receiver thread started");
	assert(receiver);
	while (!terminateSocketThreads.load()) {
		int size;
		void *responseArray;
		RPC::GRPCResponseMessage* curResponse;
		responseArray = receiver->Wait(size, 1000);
		if (responseArray != NULL) {
			curResponse = RPC::BaseRPC::GetMessage(responseArray, size);
			uint64 receivedMID = curResponse->messageid();
			int indexQueue = receivedMID % NUM_OF_RECV_QUEUE;
			recvMutexes[indexQueue]->lock();
			responses[indexQueue][receivedMID] = curResponse;
			recvCVs[indexQueue]->notify_all();
			recvMutexes[indexQueue]->unlock();
		}
	}
	NOTICE("Client receiver thread exited");
}

} /* namespace Client */
} /* namespace DTranx */

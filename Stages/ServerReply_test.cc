/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "ServerReply.h"

TEST(ServerReply, InitializePeerPorts) {
	DTranx::Util::Log::setLogPolicy( { { "Server", "PROFILE" } });
	std::vector<std::string> allNodes;
	std::vector<uint32_t> allPorts;
	std::string selfAddress;
	allNodes.push_back("172.0.0.1");
	allNodes.push_back("172.0.0.2");
	allNodes.push_back("172.0.0.3");
	allPorts.push_back(30002);
	allPorts.push_back(30003);
	allPorts.push_back(30004);
	allPorts.push_back(30005);
	allPorts.push_back(30006);
	allPorts.push_back(30007);
	selfAddress = "172.0.0.2";
	std::unordered_map<std::string, std::unordered_set<uint32_t> > remotePeerPorts =
			DTranx::Stages::ServerReply::InitializePeerPorts(allNodes, allPorts, selfAddress);
	EXPECT_TRUE(remotePeerPorts.find("172.0.0.1") != remotePeerPorts.end());
	EXPECT_TRUE(remotePeerPorts["172.0.0.1"].find(30002) != remotePeerPorts["172.0.0.1"].end());
	EXPECT_TRUE(remotePeerPorts["172.0.0.1"].find(30003) != remotePeerPorts["172.0.0.1"].end());
	EXPECT_TRUE(remotePeerPorts["172.0.0.1"].find(30004) != remotePeerPorts["172.0.0.1"].end());
	EXPECT_TRUE(remotePeerPorts["172.0.0.3"].find(30005) != remotePeerPorts["172.0.0.3"].end());
}

TEST(ServerReply, InitializePeerPorts2) {
	DTranx::Util::Log::setLogPolicy( { { "Server", "PROFILE" } });
	std::vector<std::string> allNodes;
	std::vector<uint32_t> allPorts;
	std::string selfAddress;
	allNodes.push_back("172.0.0.1");
	allNodes.push_back("172.0.0.2");
	allNodes.push_back("172.0.0.3");
	allPorts.push_back(30002);
	allPorts.push_back(30003);
	selfAddress = "172.0.0.1";
	std::unordered_map<std::string, std::unordered_set<uint32_t> > remotePeerPorts =
			DTranx::Stages::ServerReply::InitializePeerPorts(allNodes, allPorts, selfAddress);
	EXPECT_TRUE(remotePeerPorts.find("172.0.0.2") != remotePeerPorts.end());
	EXPECT_TRUE(remotePeerPorts.find("172.0.0.3") != remotePeerPorts.end());
	EXPECT_TRUE(remotePeerPorts["172.0.0.2"].find(30002) != remotePeerPorts["172.0.0.2"].end());
	EXPECT_TRUE(remotePeerPorts["172.0.0.3"].find(30002) != remotePeerPorts["172.0.0.3"].end());
}


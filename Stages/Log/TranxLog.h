/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * TranxLog persists logs for 2PC as well as garbage collection
 */

#ifndef DTRANX_STAGES_LOG_TRANXLOG_H_
#define DTRANX_STAGES_LOG_TRANXLOG_H_

#include <unordered_map>
#include <memory>
#include <mutex>
#include <unordered_set>
#include <libpmemlog.h>
#include "DTranx/Stages/Log/Log.pb.h"
#include "LogReader.h"
#include "LogWriter.h"
#include "DTranx/Util/types.h"
#include "DTranx/Util/ConfigHelper.h"
#include "Stages/MidStage.h"

namespace DTranx {
namespace Stages {
namespace Log {

class TranxLog: public MidStage {
public:
	TranxLog(std::string fname, SharedResources::TranxServerInfo *tranxServerInfo);
	virtual ~TranxLog();

	/*
	 * prepare log adds ips for inquiry only because read items were chosen randomly.
	 * prepare with newMapping is used to record newmappings for repartitioning
	 */
	void Prepare(const Util::TranxID& tranxID,
			const google::protobuf::RepeatedPtrField<Service::GItem>& readSet,
			const google::protobuf::RepeatedPtrField<Service::GItem>& writeSet,
			const std::vector<std::string>& ips,
			bool snapshot = false);
	void Prepare(const Util::TranxID& tranxID,
			const std::unordered_map<std::string, std::unordered_set<std::string> >& newMapping,
			std::vector<std::string> allNodes);
	void ParticipantAbort(const Util::TranxID& tranxID);
	void CoordinatorAbort(const Util::TranxID& tranxID);
	void Ready(const Util::TranxID& tranxID,
			const google::protobuf::RepeatedPtrField<Service::GItem>& readSet,
			const google::protobuf::RepeatedPtrField<Service::GItem>& writeSet,
			const google::protobuf::RepeatedPtrField<std::string>& ips,
			bool snapshot = false);
	void Ready(const Util::TranxID& tranxID,
			const google::protobuf::RepeatedPtrField<Service::GMapItem>& mappingItems,
			std::vector<std::string> allNodes);
	void Commit(const Util::TranxID& tranxID,
			Service::GTranxType tranType = Service::GTranxType::NORMAL,
			GLogType logType = GLogType::COMMIT);
	void Commit_1PC(const Util::TranxID& tranxID,
			const google::protobuf::RepeatedPtrField<Service::GItem>& readSet,
			const google::protobuf::RepeatedPtrField<Service::GItem>& writeSet);
	void CoordinatorAck(const Util::TranxID& tranxID, Service::GTranxType tranType =
			Service::GTranxType::NORMAL);

	/*
	 * catch the exception when calling ReadNext
	 * the first ReadNext traverse all log files
	 * the second OneReadNext only reads one file
	 * important, don't call them interchangeably
	 */
	GTranxLogRecord ReadNext();
	GTranxLogRecord OneReadNext();

	/*
	 * the following functions are mainly used by GarbageCollector thread/RecoverLog
	 * so not in stage threads
	 */
	void GCEntry(std::unordered_map<int, uint64> tranxIDs);
	std::unordered_map<int, uint64> GetGCEntry();

	virtual void StartStage(std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) {
		bool enableCoreBinding = false;
		uint32_t coreIDIndex = 0;
		assert(numOfStageThreads >= 1);
		if (numOfStageThreads == 1) {
			enableCoreBinding = (coreIDs.size() == 1);
		} else {
			enableCoreBinding = (coreIDs.size() == numOfStageThreads + 1);
		}
		stageThread.push_back(boost::thread(&TranxLog::StageThread, this));
		if (enableCoreBinding) {
			Util::ThreadHelper::PinToCPUCore(stageThread[0], coreIDs[coreIDIndex++]);
		}
	}

private:
	/*
	 * file initialize: truncate if needed for write, read next file for read
	 * 	nextWriteSize is only used for pmem logs
	 */
	void FileInitWrite(int nextWriteSize = 0);
	void FileInitRead();
	void CloseWrite();
	void CloseRead();
	std::unique_ptr<LogWriter> logWriter;
	std::unique_ptr<LogReader> logReader;
	/*
	 * curReadName: the file name currently is reading
	 * fname: the prefix to all the log files, or it's the gc file name, or it's the one single file that needs to be read
	 * truncateThreshold: only used for file writing(wal), not applied to gc log;
	 * 		for disk writes, it means the maximum number of log entries
	 * 		for pmem writes, it means the maximum bytes
	 * totalAvailBytes: only used for pmem to record the max bytes after subtracting the metadata size from truncateThreshold
	 */
	std::string curReadName;
	std::string fname;
	uint64 truncateThreshold;
	uint64 totalAvailBytes;
	/*
	 * counter: record how many records are written to the current file
	 * 		for disk writes, it means how many log entries are stored
	 * 		for pmem writes, it means how many bytes are used
	 */
	uint64 counter;
	LogOption logOption;

	/*
	 * buffer for serialized string to avoid too much memory new/delete
	 */
	std::string bufferString;
	GTranxLogRecord logRecord;

	virtual void StageThread();
};

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_LOG_TRANXLOG_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */
#include "DTranx/Util/Log.h"
#include "Util/StringUtil.h"
#include "DaemonStages/MetisHelper.h"
#include <fstream>
#include <unordered_set>
using namespace DTranx;

int main(int argc, char **argv) {
	Util::Log::setLogPolicy( { { "Util", "VERBOSE" }, { "Example", "VERBOSE" } });
	if (argc < 3) {
		std::cout << "arguments required: arg1(tranhistory filename), arg2(num of partition)"
				<< std::endl;
		exit(0);
	}
	std::string fileName = std::string(argv[1]);
	int numPartition = atoi(argv[2]);
	DaemonStages::MetisHelper metisHelper(true);
	std::unordered_map<std::string, std::unordered_set<int> > newMappings;
	int totalTranx = 0;
	if (!fileName.empty()) {
		std::ifstream ipFile(fileName);
		if (!ipFile.is_open()) {
			VERBOSE("cannot open %s", fileName.c_str());
			exit(0);
		}
		VERBOSE("reading tranxHistory from file");
		std::string line;
		std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>> tranxHistory;
		while (std::getline(ipFile, line)) {
			totalTranx++;
			std::unordered_set<std::string> readSet, writeSet;
			std::vector<std::string> readAndWrite = Util::StringUtil::Split(line, ';');
			assert(readAndWrite.size() == 2);
			std::string readStr = readAndWrite[0];
			assert(readStr.substr(0, 8) == "readset:");
			std::vector<std::string> readSetVec = Util::StringUtil::Split(
					readStr.substr(8, readStr.size() - 9), ',');
			for (auto it = readSetVec.begin(); it != readSetVec.end(); ++it) {
				readSet.insert(it->substr(1));
			}
			std::string writeStr = readAndWrite[1];
			assert(writeStr.substr(1, 9) == "writeset:");
			std::vector<std::string> writeSetVec = Util::StringUtil::Split(
					writeStr.substr(10, writeStr.size() - 10), ',');
			for (auto it = writeSetVec.begin(); it != writeSetVec.end(); ++it) {
				if (it->size() > 1) {
					writeSet.insert(it->substr(1));
				}
			}
			tranxHistory.push_back(std::make_pair(readSet, writeSet));
		}
		VERBOSE("adding tranxHistory to metis");
		for(auto it=tranxHistory.begin(); it!= tranxHistory.end(); ++it){
			metisHelper.AddTranx(it->first, it->second);
		}
		VERBOSE("TranxHistory has %d transactions", totalTranx);
		if (totalTranx > 0) {
			VERBOSE("Start running METIS");
			metisHelper.RunMetis(numPartition);
			VERBOSE("Get new mappings from METIS");
			metisHelper.GetNewMappingsR(newMappings);
			for (auto it = newMappings.begin(); it != newMappings.end(); ++it) {
				std::cout << "key: " << it->first << std::endl;
				for (auto ip = it->second.begin(); ip != it->second.end(); ++ip) {
					std::cout << *ip << " ";
				}
				std::cout << std::endl;
			}
		}
	}
}


/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * Service interface to provide HandleRPC method and basic stage functions
 * Classes that inherit Service: ClientService, StorageService, TranxService
 *
 * Initialization consists of InitStages and StartService
 */

#ifndef DTRANX_SERVICE_SERVICE_H_
#define DTRANX_SERVICE_SERVICE_H_

#include <boost/thread.hpp>
#include "RPC/ServerRPC.h"
#include "Stages/ServerReply.h"
#include "Stages/TranxServiceQueueElement.h"
#include "SharedResources/TranxServiceSharedData.h"
#include "SharedResources/TranxServerInfo.h"
#include "Build/Util/Profile.pb.h"
#include "Stages/PerfStage.h"
#include "Util/ThreadHelper.h"

namespace DTranx {

namespace Server {
class Server;
}

namespace Stages {
class TranxRPCHelper;
class TranxStates;
namespace Lock {
class WriteBlockLockTable;
}
namespace Log {
class TranxLog;
}
class ServerReply;
class StringStorage;
}

namespace DaemonStages {
class TranxAck;
class Repartition;
}

namespace Service {

class Service: public Stages::PerfStage {
public:
	Service(SharedResources::TranxServerInfo *tranxServerInfo,
			SharedResources::TranxServiceSharedData *tranxServiceSharedData, std::string stageName =
					"ServiceDefault")
			: Stages::PerfStage(stageName), tranxServerInfo(tranxServerInfo), tranxServiceSharedData(
					tranxServiceSharedData), terminateThread(false), terminateInternalThread(
			false), ptok_tranxStates(), serviceProcessQueue(1000), serviceInternalProcessQueue(
					1000), freeList(1000) {
	}
	virtual ~Service() noexcept(false) {

	}

	void InitStages(Stages::TranxRPCHelper *tranxRPCHelper, Stages::TranxStates *tranxStates,
			Stages::StringStorage *localStorage, DaemonStages::TranxAck *tranxAck,
			DaemonStages::Repartition *repartition, Stages::Log::TranxLog *tranxLog,
			Stages::Lock::WriteBlockLockTable *lockTable, Stages::ServerReply *replyThread);

	virtual void HandleRPC(RPC::ServerRPC *serverRPC) {

	}

	virtual void StartService(int numOfServiceThreads,
			std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) = 0;

	void ShutService() {
		terminateThread.store(true);
		for (auto thread = serviceThread.begin(); thread != serviceThread.end(); ++thread) {
			if (thread->joinable()) {
				thread->join();
			}
		}
		terminateInternalThread.store(true);
		for (auto thread = serviceInternalThread.begin(); thread != serviceInternalThread.end();
				++thread) {
			if (thread->joinable()) {
				thread->join();
			}
		}
	}

	/*
	 * interfaces to add to queue
	 */
	void SendToService(RPC::ServerRPC *element) {
		//assert(serviceProcessQueue.enqueue(element));
		while (!serviceProcessQueue.try_enqueue(element)) {
			if (serviceProcessQueue.enqueue(element)) {
				break;
			}
		}
	}
	void SendToServiceInternal(Stages::TranxServiceQueueElement *element) {
		//assert(serviceInternalProcessQueue.enqueue(element));
		while (!serviceInternalProcessQueue.try_enqueue(element)) {
			if (serviceInternalProcessQueue.enqueue(element)) {
				break;
			}
		}
	}

	void AddToFreeList(Stages::TranxServiceQueueElement *element) {
		element->Clear();
		freeList.try_enqueue(element);
	}

	void SetServer(Server::Server *server);

protected:
	/*
	 * Daemon Stages
	 */
	DaemonStages::TranxAck *tranxAck;
	DaemonStages::Repartition *repartition;
	/*
	 * MidStages
	 */
	Stages::TranxRPCHelper *tranxRPCHelper;
	Stages::TranxStates *tranxStates;
	Stages::StringStorage *localStorage;
	Stages::Log::TranxLog *tranxLog;
	Stages::Lock::WriteBlockLockTable *lockTable;

	/*
	 * Be careful, only use one token in one thread, here in service class, use it in Internal thread only for now
	 */
	moodycamel::ProducerToken * ptok_tranxStates;
	moodycamel::ProducerToken * ptok_tranxStates_external;

	/*
	 * Final Stages
	 */
	Stages::ServerReply *replyThread;

	/*
	 * shared/non-shared data
	 */
	SharedResources::TranxServerInfo *tranxServerInfo;
	SharedResources::TranxServiceSharedData *tranxServiceSharedData;

	/*
	 * freeList is used to reuse the TranxServiceQueueElement to avoid new/delete
	 */
	moodycamel::ConcurrentQueue<Stages::TranxServiceQueueElement *> freeList;

	/*
	 * perf use: server is used to contact every stage to output its perf data
	 * 	like response time, queue length
	 */
	Server::Server *server;

	/*
	 * performance metric counter that's thread safe
	 */
	std::atomic<uint64_t> metrics[10];

	/*
	 * stage pipeline threads
	 * ServiceThread: connects with Server
	 * ServiceInternalThread: connects with stages other than Server
	 */
	virtual void ServiceThread(int index) = 0;
	std::vector<boost::thread> serviceThread;
	std::atomic_bool terminateThread;
	moodycamel::ConcurrentQueue<RPC::ServerRPC *> serviceProcessQueue;
	virtual void ServiceInternalThread(int index) = 0;
	std::vector<boost::thread> serviceInternalThread;
	std::atomic_bool terminateInternalThread;
	moodycamel::ConcurrentQueue<Stages::TranxServiceQueueElement *> serviceInternalProcessQueue;
};

} /* namespace Service */
} /* namespace DTranx */

#endif /* DTRANX_SERVICE_SERVICE_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * RPC handler used by TranxService
 */

#ifndef DTRANX_RPC_TRANXRPC_H_
#define DTRANX_RPC_TRANXRPC_H_

#include "BaseRPC.h"
#include "DTranx/RPC/ZeromqSender.h"
#include "Stages/StageTasks.h"
#include "Util/TranxID.h"
namespace DTranx {
namespace RPC {

class TranxRPC: public BaseRPC {
public:
	explicit TranxRPC(Service::GOpCode opcode,
			GServiceID serviceType,
			uint64 messageID,
			bool ResponsesNeeded = true);
	TranxRPC(TranxRPC&& other);
	TranxRPC& operator=(TranxRPC&& other);
	virtual ~TranxRPC() noexcept(false);

	enum RPCWaitingStatus {
		/**
		 * Waiting for a reply from the server.
		 */
		WAITING,
		/**
		 * Received a reply (find it in #reply).
		 */
		HAS_REPLY,
		/**
		 * The RPC has been canceled by another thread.
		 */
		CANCELED,
	} waitingStatus;

	void Clear();

	void Assign(Service::GOpCode opcode,
			GServiceID serviceType,
			uint64 messageID,
			bool ResponsesNeeded = true);
	/*
	 * for those RPC's, which doesn't require responses
	 */
	bool IsResponsesNeeded() {
		return ResponsesNeeded;
	}

	/*
	 * opcode access methods
	 */

	void SetTranxOpcode(Service::GOpCode opcode) {
		requestMessage->mutable_tranxservicerpc()->set_opcode(opcode);
	}

	Service::GStatus GetTranxRepStatus() {
		assert(responseMessage != NULL);
		assert(responseMessage->has_tranxservicerpc());
		assert(requestMessage->has_tranxservicerpc());
		Service::GOpCode opcode = requestMessage->tranxservicerpc().opcode();
		if (opcode == Service::GOpCode::TRANX_ABORT) {
			return responseMessage->tranxservicerpc().tranxabort().response().status();
		} else if (opcode == Service::GOpCode::TRANX_COMMIT) {
			return responseMessage->tranxservicerpc().tranxcommit().response().status();
		} else if (opcode == Service::GOpCode::TRANX_GC) {
			assert(false);
		} else if (opcode == Service::GOpCode::TRANX_INQUIRE) {
			return responseMessage->tranxservicerpc().tranxinquire().response().status();
		} else if (opcode == Service::GOpCode::TRANX_MIGRATION) {
			return responseMessage->tranxservicerpc().tranxmigration().response().status();
		} else if (opcode == Service::GOpCode::TRANX_PARTITION) {
			assert(false);
		} else if (opcode == Service::GOpCode::TRANX_PREP) {
			return responseMessage->tranxservicerpc().tranxprep().response().status();
		} else {
			assert(false);
		}
		return Service::GStatus::FAIL;
	}

	Util::TranxID GetRepTranxID() {
		Util::TranxID tranxID;
		assert(requestMessage != NULL);
		assert(requestMessage->has_tranxservicerpc());
		Service::GOpCode opcode = requestMessage->tranxservicerpc().opcode();
		if (opcode == Service::GOpCode::TRANX_ABORT) {
			tranxID.SetNodeID(
					requestMessage->tranxservicerpc().tranxabort().request().tranxid().nodeid());
			tranxID.SetTranxID(
					requestMessage->tranxservicerpc().tranxabort().request().tranxid().tranxid());
			return tranxID;
		} else if (opcode == Service::GOpCode::TRANX_COMMIT) {
			tranxID.SetNodeID(
					requestMessage->tranxservicerpc().tranxcommit().request().tranxid().nodeid());
			tranxID.SetTranxID(
					requestMessage->tranxservicerpc().tranxcommit().request().tranxid().tranxid());
			return tranxID;
		} else if (opcode == Service::GOpCode::TRANX_GC) {
			assert(false);
		} else if (opcode == Service::GOpCode::TRANX_INQUIRE) {
			tranxID.SetNodeID(
					requestMessage->tranxservicerpc().tranxinquire().request().tranxid().nodeid());
			tranxID.SetTranxID(
					requestMessage->tranxservicerpc().tranxinquire().request().tranxid().tranxid());
			return tranxID;
		} else if (opcode == Service::GOpCode::TRANX_MIGRATION) {
			tranxID.SetNodeID(
					requestMessage->tranxservicerpc().tranxmigration().request().tranxid().nodeid());
			tranxID.SetTranxID(
					requestMessage->tranxservicerpc().tranxmigration().request().tranxid().tranxid());
			return tranxID;
		} else if (opcode == Service::GOpCode::TRANX_PARTITION) {
			assert(false);
		} else if (opcode == Service::GOpCode::TRANX_PREP) {
			tranxID.SetNodeID(
					requestMessage->tranxservicerpc().tranxprep().request().tranxid().nodeid());
			tranxID.SetTranxID(
					requestMessage->tranxservicerpc().tranxprep().request().tranxid().tranxid());
			return tranxID;
		} else {
			assert(false);
		}
		return tranxID;
	}

	/*
	 * specific service request/response access methods
	 */

	Service::GTranxServiceRPC* GetTranxRequest() {
		assert(requestMessage != NULL);
		assert(requestMessage->has_tranxservicerpc());
		return requestMessage->mutable_tranxservicerpc();
	}

	Service::GTranxServiceRPC* GetTranxResponse() {
		assert(requestMessage != NULL);
		assert(responseMessage->has_tranxservicerpc());
		return responseMessage->mutable_tranxservicerpc();
	}

	void SetMonitor() {
		toMonitor = true;
	}

	bool GetMonitor() {
		return toMonitor;
	}

	Stages::TranxPeerStageTask getStageTask() const {
		return stageTask;
	}

	void setStageTask(Stages::TranxPeerStageTask stageTask) {
		this->stageTask = stageTask;
	}

	const std::string& getRemoteIp() const {
		return remoteIP;
	}

	void setRemoteIp(const std::string& remoteIp) {
		remoteIP = remoteIp;
	}

private:

	/*
	 * ResponsesNeeded
	 * only used for rpc's that don't require responses
	 */
	bool ResponsesNeeded;

	/*
	 * used for metric system
	 */
	bool toMonitor;

	/*
	 * used for stage processing
	 *  special peer task should tell TranxPeer sender to actually send after the
	 *  receiver gets the queue element
	 */
	Stages::TranxPeerStageTask stageTask;

	std::string remoteIP;

	/*
	 * TranxRPC is non-copyable.
	 */
	TranxRPC(const TranxRPC&) = delete;
	TranxRPC& operator=(const TranxRPC&) = delete;
};

} /* namespace RPC */
} /* namespace DTranx */

#endif /* DTRANX_RPC_TRANXRPC_H_ */

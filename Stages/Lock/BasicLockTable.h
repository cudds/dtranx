/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * BasicLockTable is a lock table that manages the locks to the database elements. It automatically checks with the storage whether the data element exist or not
 * different modes of lock: read/shared, write/exclusive
 * user can call _RequestLock function multiple times for the same lock mode.
 * however, user can request for another lock mode when it's granted a previous lock, BasicLockTable will upgrade it or grant it accordingly.
 *
 * BasicLock is not used directly and that's why functions are protected. Inherited classes like WriteBlockLockTable are used by the TranxService
 * whether waitIfDenied is true or false, the functions are nonblocking.
 *
 * Not Threadsafe, should be protected by mutex in child classes
 * do not request for another lock mode when the previous one is not granted.
 *
 * 		map					point
 * key-------> *LockElement------->lockMode
 * 									wait
 * 									list of *TranxInfo		point
 * 									t1-------------------------------> tranxID
 * 									t2									lockMode
 * 									... 								isWait
 *
 *
 *
 * for epoch lock, to avoid time spent on traversing the list, it's assumed that reads will be nonblocking and not put in wait list, write will be blocking and wait in list, only one write can wait
 */

#ifndef DTRANX_STAGES_BASICLOCKTABLE_H_
#define DTRANX_STAGES_BASICLOCKTABLE_H_
#include <iostream>
#include <list>
#include <unordered_map>
#include <memory>
#include <map>
#include <condition_variable>
#include "Stages/MidStage.h"

namespace DTranx {
namespace Stages {
namespace Lock {

class BasicLockTable: public MidStage {
public:

	struct TranxInfo {
		Util::TranxID tranxID;
		BasicLockTableLockMode lockMode;
		TranxInfo(const Util::TranxID& tranxID, BasicLockTableLockMode lockMode)
				: tranxID(tranxID), lockMode(lockMode) {
		}
	};

	struct LockElement {
		BasicLockTableLockMode mode;bool wait;  // wait is true when there's at least one transaction waiting for the lock
		std::list<TranxInfo*> tranx;
		LockElement(BasicLockTableLockMode mode, bool wait)
				: mode(mode), wait(wait) {
		}
	};

	struct EpochLockElement {
		BasicLockTableLockMode mode;

		bool wait;
		std::unordered_set<Util::TranxID, Util::KeyHasher> tranxID;  // all the reading tranxs
		Util::TranxID writeTranx;  // whether still waiting or holding, it should be empty when released
		EpochLockElement(BasicLockTableLockMode mode = BasicLockTableLockMode::Shared, bool wait =
		false)
				: wait(wait), mode(mode) {
		}
	};

	BasicLockTable(SharedResources::TranxServerInfo *tranxServerInfo,
			int numOfStageThreads = 1,
			std::string stageName = "BasicLockTable");
	virtual ~BasicLockTable() noexcept(false);
	virtual void StartStage(std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) {

	}

	/*
	 * to be implemented in child classes
	 */
	virtual bool RequestLock(const std::string& key,
			const Util::TranxID& tranxID,
			BasicLockTableLockMode lockMode) {
		return true;
	}
	virtual bool RequestLocks(std::vector<std::string>& keys,
			const Util::TranxID& tranxID,
			BasicLockTableLockMode lockMode) {
		return true;
	}
	virtual bool RequestLocks(std::map<std::string, BasicLockTableLockMode>& keys_mode,
			const Util::TranxID& tranxID) {
		return true;
	}

	virtual bool ReleaseLock(const std::string& key, const Util::TranxID& tranxID) {
		return true;
	}
	virtual bool ReleaseLocks(std::vector<std::string>& keys, const Util::TranxID& tranxID) {
		return true;
	}
	virtual bool ReleaseLocks(std::map<std::string, BasicLockTableLockMode>& keys_mode,
			const Util::TranxID& tranxID) {
		return true;
	}
	std::string PrintLockStatus(int stageThreadIndex);
protected:

	bool _RequestLock(const std::string& key,
			const Util::TranxID& tranxID,
			BasicLockTableLockMode lockMode,
			bool waitIfDenied);

	bool _RequestLockPerKey(LockElement *currentLock,
			const Util::TranxID& tranxID,
			BasicLockTableLockMode lockMode,
			bool waitIfDenied);

	bool _RequestEpochLock(const Util::TranxID& tranxID, BasicLockTableLockMode lockMode);

	bool _CancelRequest(const std::string& key, const Util::TranxID& tranxID);

	bool _ReleaseEpochLock(const Util::TranxID& tranxID);

	bool _ReleaseLockPerKey(LockElement *currentLock, const Util::TranxID& tranxID);

	bool _ReleaseLock(const std::string& key, const Util::TranxID& tranxID);

	/*
	 * GetThreadByKey get the thread that stores the key
	 * 	this is the method to split the shared data(by hash)
	 */
	int GetThreadByKey(const std::string& key) const {
		uint64 hashedKey = Util::StringUtil::SDBMHash(key.c_str());
		return hashedKey % numOfStageThreads;
	}

private:
	/*
	 * lockTable is partitioned into key hash spaces to reduce contention
	 * _hashSpace can be adjusted for less contention if needed.
	 */
	std::vector<std::unordered_map<std::string, LockElement*>> lockTable;
	std::unique_ptr<EpochLockElement> epochLock;

	virtual void StageThread(int stageThreadIndex = 0) {

	}
	virtual void DispatchThread() {

	}
};

} /* namespace Lock */
} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_BASICLOCKTABLE_H_ */

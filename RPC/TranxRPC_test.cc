/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "TranxRPC.h"

using namespace DTranx;
TEST(TranxRPC, tranxid) {
	RPC::TranxRPC tranxRPC(Service::GOpCode::TRANX_PREP, RPC::GServiceID::TranxService, 12);
	tranxRPC.requestMessage->mutable_tranxservicerpc()->mutable_tranxprep()->mutable_request()->mutable_tranxid()->set_nodeid(
			1);
	tranxRPC.requestMessage->mutable_tranxservicerpc()->mutable_tranxprep()->mutable_request()->mutable_tranxid()->set_tranxid(
			2);
	EXPECT_EQ(Util::TranxID(1, 2), tranxRPC.GetRepTranxID());
	tranxRPC.responseMessage = new RPC::GRPCResponseMessage();
	tranxRPC.responseMessage->mutable_tranxservicerpc()->mutable_tranxprep()->mutable_response()->set_status(
			Service::GStatus::OK);
	EXPECT_EQ(RPC::GStatus::OK, tranxRPC.GetRepStatus());
}

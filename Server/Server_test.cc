/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */
#include "gtest/gtest.h"
#include "Server.h"
#include "DTranx/Util/Log.h"
#include "Util/FileUtil.h"

using namespace DTranx;

TEST(Server, InitDBForPerfTest) {
	DTranx::Util::Log::setLogPolicy( { { "Server", "NOTICE" } });
	Util::ConfigHelper configHelper;
	configHelper.readFile("DTranx.conf");
	Server::Server *server = new Server::Server(configHelper);
	server->InitDBForPerfTest(false, 100);
	EXPECT_EQ("value_init", server->localStorage->Read("user12161962213042174405"));
	delete server;
	Util::FileUtil::RemoveDir("dtranx.db*");
	Util::FileUtil::RemoveDir("dtranx.mapdb");
}

void ShutServer(Server::Server *server) {
	VERBOSE("sleep 5 sec to wait for server to start up");
	sleep(5);
	server->ShutDown();
}

TEST(Server, MainThread) {
	DTranx::Util::Log::setLogPolicy( { { "Server", "VERBOSE" } });
	Util::ConfigHelper configHelper;
	configHelper.readFile("DTranx.conf");
	configHelper.set("SelfAddress", "127.0.0.1");
	Server::Server server(configHelper, NULL);
	boost::thread tmpThread(ShutServer, &server);
	server.ServerMainThread();
	EXPECT_TRUE(tmpThread.joinable());
	tmpThread.join();
	DTranx::Util::Log::setLogPolicy( { { "Server", "NOTICE" } });
	Util::FileUtil::RemoveDir("dtranx.db*");
	Util::FileUtil::RemoveDir("dtranx.mapdb");
}

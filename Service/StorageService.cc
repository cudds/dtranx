/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "StorageService.h"
#include "DTranx/Service/Storage.pb.h"
#include "RPC/ClientRPC.h"
#include "DTranx/Client/Client.h"
#include "DTranx/Util/Log.h"
#include "Util/Exceptions/NullPointerException.h"
#include "Stages/Storage.h"
#include "Server/Server.h"

namespace DTranx {
namespace Service {
StorageService::StorageService(SharedResources::TranxServerInfo *tranxServerInfo,
		SharedResources::TranxServiceSharedData *tranxServiceSharedData)
		: Service(tranxServerInfo, tranxServiceSharedData, "storageservice") {
	selfAddress = tranxServerInfo->GetSelfAddress();
}

void StorageService::HandleRPC(RPC::ServerRPC *serverRPC) {
	GOpCode opcode = GOpCode(serverRPC->GetStorageOpcode());
	switch (opcode) {
	case GOpCode::READ_DATA:
		ReadData(std::move(serverRPC));
		break;
	case GOpCode::WRITE_DATA:
		WriteData(std::move(serverRPC));
		break;
	}
}

void StorageService::ServiceThread(int index) {
	NOTICE("StorageServiceThread started");
	uint64_t randSeed = 0;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateThread.load()) {
			NOTICE("StorageServiceThread is reclaimed");
			break;
		}
		RPC::ServerRPC *tmpRPCs[Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = serviceProcessQueue.try_dequeue_bulk(tmpRPCs,
				Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1 && index == 0) {
				/*
				 * use index here to avoid race condition on queue length samples
				 */
				AddQueueLengthSample(6, serviceProcessQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			RPC::ServerRPC *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(6);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(6);
				std::vector<int> taskLongLatencySamples = GetLongLatency(6);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName);
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				server->AddToFreeList(element);
				Stages::TranxServiceQueueElement *tmpElement =
						new Stages::TranxServiceQueueElement();
				tmpElement->setIsPerfOutPutTask(true);
				SendToServiceInternal(tmpElement);
				continue;
			}
			HandleRPC(element);
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void StorageService::ServiceInternalThread(int index) {
	NOTICE("StorageServiceInternalThread started");
	uint64_t randSeed = 0;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateInternalThread.load()) {
			NOTICE("StorageServiceInternalThread is reclaimed");
			break;
		}
		Stages::TranxServiceQueueElement *tmpRPCs[Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = serviceInternalProcessQueue.try_dequeue_bulk(tmpRPCs,
				Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1 && index == 0) {
				/*
				 * use index here to avoid race condition on queue length samples
				 */
				AddQueueLengthSample(7, serviceInternalProcessQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			Stages::TranxServiceQueueElement *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(7);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(7);
				std::vector<int> taskLongLatencySamples = GetLongLatency(7);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_Internal");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				AddToFreeList(element);
				continue;
			}
			//element->goThroughStage("StorageService");
			Process(element);
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void StorageService::ReadData(RPC::ServerRPC *serverRPC) {
	/*
	 metrics[0].operator ++();
	 if (metrics[0].load() % 10000 == 1) {
	 VERBOSE("storageservice read requests %lu", metrics[0].load());
	 }
	 */
	Stages::TranxServiceQueueElement *element;
	if (!freeList.try_dequeue(element)) {
		element = new Stages::TranxServiceQueueElement();
	}
	//element->goThroughStage("StorageService");
	element->SetServerRPC(serverRPC);
	element->setStageId(Stages::TranxServiceQueueElement::STORAGESERVICE);

	GReadData::Request *request =
			serverRPC->GetStorageRequest()->mutable_readdata()->mutable_request();
	GReadData::Response *response =
			serverRPC->GetStorageResponse()->mutable_readdata()->mutable_response();
	response->set_status(GStatus::OK);
	std::string key = request->key();
	bool isSnapshot = request->snapshot();
	//VERBOSE("storageService request for key %s", key.c_str());
	if (isSnapshot) {
		element->setStageNum(1);
		element->setStorageTask(Stages::StorageStageTask::_ReadSnapshot);
		localStorage->SendToStage(element,
				Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else {
		element->setStageNum(2);
		element->setStorageTask(Stages::StorageStageTask::_ReadWithVersion);
		localStorage->SendToStage(element,
				Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	}
}

void StorageService::Process(Stages::TranxServiceQueueElement *element) {
	int stageNum = element->getStageNum();
	GReadData::Response *response =
			element->GetServerRPC()->GetStorageResponse()->mutable_readdata()->mutable_response();
	if (stageNum == 1) {
		/*
		 * read snapshot check snapshot existence first then check if key exist in db
		 */
		if (element->isStorageSnapshotExist()) {
			if (element->isStorageKeyExist()) {
				response->set_value(element->getStorageValue());
				response->set_epoch(element->getStorageEpoch());
				std::string key =
						element->GetServerRPC()->GetStorageRequest()->readdata().request().key();
				std::unordered_set<std::string> nodeIPs;
				tranxServiceSharedData->getMappingStorage()->GetMapping(key, nodeIPs);
				for (auto ip = nodeIPs.begin(); ip != nodeIPs.end(); ++ip) {
					response->add_newips(*ip);
				}
				replyThread->SendReplyClient(element->GetServerRPC());
				AddToFreeList(element);
				return;
			} else {
				std::unordered_set<std::string> ips;
				tranxServiceSharedData->getMappingStorage()->GetMapping(
						element->GetServerRPC()->GetStorageRequest()->readdata().request().key(),
						ips);
				if (std::find(ips.begin(), ips.end(), selfAddress) != ips.end() || ips.empty()) {
					response->set_status(GStatus::KEY_NOT_FOUND);
				} else {
					//VERBOSE("forwarding storage read request to %s", ips.begin()->c_str());
					response->set_status(GStatus::FORWARD_SERVER);
					for (auto ip = ips.begin(); ip != ips.end(); ++ip) {
						response->add_newips(*ip);
					}
				}
				replyThread->SendReplyClient(element->GetServerRPC());
				AddToFreeList(element);
				return;
			}
		} else {
			response->set_status(GStatus::SNAPSHOT_NOT_CREATED);
			replyThread->SendReplyClient(element->GetServerRPC());
			AddToFreeList(element);
			return;
		}
	} else if (stageNum == 2) {
		if (element->isStorageKeyExist()) {
			response->set_value(element->getStorageValue());
			response->set_version(element->getStorageVersion());
			replyThread->SendReplyClient(element->GetServerRPC());
			AddToFreeList(element);
			return;
		} else {
			std::unordered_set<std::string> ips;
			tranxServiceSharedData->getMappingStorage()->GetMapping(
					element->GetServerRPC()->GetStorageRequest()->readdata().request().key(), ips);
			if (ips.find(selfAddress) != ips.end() || ips.empty()) {
				response->set_status(GStatus::KEY_NOT_FOUND);
			} else {
				//VERBOSE("forwarding storage read request to %s", ips.begin()->c_str());
				response->set_status(GStatus::FORWARD_SERVER);
				for (auto ip = ips.begin(); ip != ips.end(); ++ip) {
					response->add_newips(*ip);
				}
			}
			replyThread->SendReplyClient(element->GetServerRPC());
			AddToFreeList(element);
			return;
		}
	} else if (stageNum == 3) {
		response->set_status(GStatus::OK);
		replyThread->SendReplyClient(element->GetServerRPC());
		AddToFreeList(element);
		return;
	} else {
		assert(false);
	}
}

void StorageService::WriteData(RPC::ServerRPC *serverRPC) {
	Stages::TranxServiceQueueElement *element;
	if (!freeList.try_dequeue(element)) {
		element = new Stages::TranxServiceQueueElement();
	}
	//element->goThroughStage("StorageService");
	element->SetServerRPC(serverRPC);
	element->setStageId(Stages::TranxServiceQueueElement::STORAGESERVICE);

	GWriteData::Request *request =
			serverRPC->GetStorageRequest()->mutable_writedata()->mutable_request();
	GWriteData::Response *response =
			serverRPC->GetStorageResponse()->mutable_writedata()->mutable_response();
	response->set_status(GStatus::OK);
	//VERBOSE("storageService writedata request");
	std::unordered_map<std::string, std::string> writeset;

	for (auto it = request->items().begin(); it != request->items().end(); ++it) {
		writeset[it->key()] = it->value();
		VERBOSE("storageService write key %s, %s", it->key().c_str(), it->value().c_str());
	}
	element->setWriteSet(writeset);
	element->setStorageTask(Stages::StorageStageTask::_WritePlain);
	element->setStageNum(3);
	localStorage->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
	return;
}

} /* namespace Service */
} /* namespace DTranx */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * FileUtil is a list of functions helps with file listing, removing
 */

#ifndef DTRANX_UTIL_FILEUTIL_H_
#define DTRANX_UTIL_FILEUTIL_H_

#include <string>
#include <vector>
#include <set>

namespace DTranx {
namespace Util {

class FileUtil {
public:
	FileUtil();
	virtual ~FileUtil() noexcept(false);

	/*
	 * LsFiles: list all regular filenames under dirName
	 * 	not recursive
	 *
	 * LSPrefix: list all file under dirName which has filename prefixed as prefix
	 */
	static void RemovePrefix(std::string dirName, std::string prefix);
	static void RemoveFile(std::string fileName);
	static void RemoveDir(std::string dirName);

	static std::set<std::string> LsPrefix(std::string fullPrefix);
	static std::set<std::string> LsPrefix(std::string dirName, std::string prefix);
	static std::set<std::string> LsPrefixDir(std::string dirName, std::string prefix);
	static std::set<std::string> LsFiles(std::string dirName);
	static std::set<std::string> LsDirectory(std::string dirName);

	static bool RenameFile(std::string src, std::string des);
	static std::string AdjustDirSuffix(std::string dirName);

	static std::string CombineName(std::string prefix, std::string fileName);
	static void SplitName(std::string fullPathName, std::string& dirName, std::string& fileName);
};

} /* namespace Util */
} /* namespace DTranx */

#endif /* DTRANX_UTIL_FILEUTIL_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <fstream>
#include "Server.h"
#include "DTranx/Util/Buffer.h"
#include "DTranx/Util/Log.h"
#include "Util/ExpBackoff.h"
#include "Util/StringUtil.h"
#include "Util/FileUtil.h"
#include "RPC/ServerRPC.h"

namespace DTranx {
namespace Server {
/*
 * Start Stage threads after Recover and Init
 */

Server::Server(Util::ConfigHelper& config, RSMHelper* rsmHelper)
		: config(config), rsmHelper(rsmHelper), terminate(false), messagesFromClients(true), messagesFromPeers(
		true), freeList(1000) {
	uint32 serverID;
	SharedResources::TranxServerInfo::Mode mode;
	if (config.read("ServerMode") == "docker") {
		NOTICE("Server Mode: docker");
		mode = SharedResources::TranxServerInfo::Mode::INTEGTEST;
	} else if (config.read("ServerMode") == "vm") {
		NOTICE("Server Mode: normal");
		mode = SharedResources::TranxServerInfo::Mode::NORMAL;
	} else {
		assert(false);
	}
	if (rsmHelper == NULL) {
		mode = SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER;
	}
	/*
	 * initialize allNodes, context etc.
	 */
	std::string selfAddress = config.read("SelfAddress");
	if (mode == SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER) {
		NOTICE("Server Mode: change to unit test");
		serverID = 1;
		allNodes.push_back(selfAddress);
	} else {
		std::map<uint64, std::string> orderedNodes;
		LogCabin::Client::Configuration config = rsmHelper->GetAllServers();
		for (auto it = config.begin(); it != config.end(); ++it) {
			std::string ip = Util::StringUtil::Split(it->second, ':')[0];
			orderedNodes[it->first] = ip;
			if (ip == selfAddress) {
				serverID = it->first;
			}
		}
		allNodes.clear();
		for (auto it = orderedNodes.begin(); it != orderedNodes.end(); ++it) {
			allNodes.push_back(it->second);
		}
	}
	VERBOSE("serverID: %d", serverID);

	/*
	 * Initialize TranxServerInfo
	 */
	tranxServerInfo = new DTranx::SharedResources::TranxServerInfo();
	tranxServerInfo->SetAllNodes(allNodes);
	tranxServerInfo->SetConfig(config);
	tranxServerInfo->SetMode(mode);
	tranxServerInfo->SetNodeID(serverID);
	/*
	 * Initialize TranxServiceSharedData
	 */
	tranxServiceSharedData = new DTranx::SharedResources::TranxServiceSharedData(serverID,
			tranxServerInfo);

	/*******************************************************************************************
	 * Create all Stages: InitStages, MidStages and FinalStages
	 */
	/*
	 * Init Stages: Service
	 */
	clientService = new Service::ClientService(tranxServerInfo, tranxServiceSharedData);
	clientService->SetServer(this);
	tranxService = new Service::TranxService(tranxServerInfo, tranxServiceSharedData);
	tranxService->SetServer(this);
	storageService = new Service::StorageService(tranxServerInfo, tranxServiceSharedData);
	storageService->SetServer(this);
	//storageService->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);

	/*
	 * Init Stages: Daemon Stages
	 */
	tranxAck = new DaemonStages::TranxAck(tranxServerInfo, tranxServiceSharedData);
	gc = new DaemonStages::GarbageCollector(tranxServerInfo, tranxServiceSharedData);
	repartition = new DaemonStages::Repartition(tranxServerInfo, tranxServiceSharedData);

	/*
	 * Init Stages: Server
	 */
	recoverLog = new RecoverLog(tranxServerInfo, tranxServiceSharedData);

	/*
	 * Mid Stages
	 */
	std::vector<uint32_t> coreIDs;
	std::vector<std::string> coreIDsStr;
	coreIDsStr = Util::StringUtil::Split(config.read("BindPeer"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}

	tranxRPCHelper = new Stages::TranxRPCHelper(tranxServerInfo, coreIDs);
	//tranxRPCHelper->Enable(Stages::PerfStage::PerfKind::RESPONSE_TIME);
	tranxStates = new Stages::TranxStates(tranxServerInfo, 1);
	//tranxStates->Enable(Stages::PerfStage::PerfKind::RESPONSE_TIME);
	localStorage = new Stages::StringStorage(tranxServerInfo, 4);
	//localStorage->Enable(Stages::PerfStage::PerfKind::RESPONSE_TIME);
	//localStorage->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
	std::string logFullName = Util::FileUtil::CombineName(
			tranxServerInfo->GetConfig().read("PersistLogDir"),
			tranxServerInfo->GetConfig().read("LogFileName"));
	tranxLog = new Stages::Log::TranxLog(logFullName, tranxServerInfo);
	//tranxLog->Enable(Stages::PerfStage::PerfKind::RESPONSE_TIME);
	lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo, 1);
	//lockTable->Enable(Stages::PerfStage::PerfKind::RESPONSE_TIME);

	/*
	 * Final Stages
	 */
	replyThread = new Stages::ServerReply(tranxServerInfo);
	replyThread->SetServer(this);
	//replyThread->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);

	/*
	 * monitoring
	 */
	if (tranxServerInfo->isEnablePerfLog()) {
		clientService->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
		tranxService->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
		storageService->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
		tranxAck->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
		gc->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
		repartition->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
		recoverLog->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
		tranxRPCHelper->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
		tranxRPCHelper->EnableForPeer(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
		tranxStates->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
		localStorage->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
		tranxLog->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
		lockTable->Enable(Stages::PerfStage::PerfKind::QUEUE_LENGTH);
	}
	/*
	 * initstages and then start recover
	 */
	clientService->InitStages(tranxRPCHelper, tranxStates, localStorage, tranxAck, repartition,
			tranxLog, lockTable, replyThread);
	tranxService->InitStages(tranxRPCHelper, tranxStates, localStorage, tranxAck, repartition,
			tranxLog, lockTable, replyThread);
	storageService->InitStages(tranxRPCHelper, tranxStates, localStorage, tranxAck, repartition,
			tranxLog, lockTable, replyThread);
	tranxAck->InitStages(tranxRPCHelper, tranxStates, localStorage, tranxLog, lockTable);
	gc->InitStages(tranxRPCHelper, tranxStates, localStorage, tranxLog, lockTable, repartition);
	repartition->InitStages(tranxRPCHelper, tranxStates, localStorage, tranxLog, lockTable,
			tranxAck, tranxService);
	recoverLog->InitStages(tranxRPCHelper, tranxStates, localStorage, tranxLog, lockTable, tranxAck,
			tranxService, repartition);
	tranxRPCHelper->InitStage(tranxService, clientService, storageService, tranxAck, repartition,
			recoverLog);
	tranxStates->InitStages(tranxService, clientService, storageService, tranxAck, repartition,
			recoverLog, gc);
	localStorage->InitStage(tranxService, clientService, storageService, tranxAck, repartition,
			recoverLog);
	tranxLog->InitStage(tranxService, clientService, storageService, tranxAck, repartition,
			recoverLog);
	lockTable->InitStage(tranxService, clientService, storageService, tranxAck, repartition,
			recoverLog);

	/*
	 * Recover Log
	 */
	recoverLog->Recover();

}

void Server::Start() {
	serverMainThread = boost::thread(&Server::Server::ServerMainThread, this);
}

void Server::ShutDown() {
	terminate.store(true);
	if (serverMainThread.joinable()) {
		serverMainThread.join();
	}
}

void Server::Init() {
	uint64 DBSize = config.read<uint64>("DBSize");
	bool IsRead = !(config.read<uint64>("IsRead") == 0);
	InitDBForPerfTest(IsRead, DBSize);
	std::vector<uint32_t> coreIDs;
	std::vector<std::string> coreIDsStr;
	coreIDsStr = Util::StringUtil::Split(config.read("BindServiceClient"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	clientService->StartService(2, coreIDs);
	coreIDsStr.clear();
	coreIDs.clear();
	coreIDsStr = Util::StringUtil::Split(config.read("BindServiceTranx"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	tranxService->StartService(1, coreIDs);
	coreIDsStr.clear();
	coreIDs.clear();
	coreIDsStr = Util::StringUtil::Split(config.read("BindServiceStorage"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	storageService->StartService(1, coreIDs);

	coreIDsStr.clear();
	coreIDs.clear();
	coreIDsStr = Util::StringUtil::Split(config.read("BindTranxAck"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	tranxAck->StartService(coreIDs);
	coreIDsStr.clear();
	coreIDs.clear();
	coreIDsStr = Util::StringUtil::Split(config.read("BindGC"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	gc->StartService(coreIDs);
	coreIDsStr.clear();
	coreIDs.clear();
	coreIDsStr = Util::StringUtil::Split(config.read("BindRepartition"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	repartition->StartService(coreIDs);
	coreIDsStr.clear();
	coreIDs.clear();
	coreIDsStr = Util::StringUtil::Split(config.read("BindRecoverLog"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	recoverLog->StartService(coreIDs);

	coreIDsStr.clear();
	coreIDs.clear();
	coreIDsStr = Util::StringUtil::Split(config.read("BindTranxRPCHelper"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	tranxRPCHelper->StartStage(coreIDs);
	coreIDsStr.clear();
	coreIDs.clear();
	coreIDsStr = Util::StringUtil::Split(config.read("BindTranxStates"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	tranxStates->StartStage(coreIDs);
	coreIDsStr.clear();
	coreIDs.clear();
	coreIDsStr = Util::StringUtil::Split(config.read("BindStorage"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	localStorage->StartStage(coreIDs);
	coreIDsStr.clear();
	coreIDs.clear();
	coreIDsStr = Util::StringUtil::Split(config.read("BindTranxLog"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	tranxLog->StartStage(coreIDs);
	coreIDsStr.clear();
	coreIDs.clear();
	coreIDsStr = Util::StringUtil::Split(config.read("BindLockTable"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	lockTable->StartStage(coreIDs);
	coreIDsStr.clear();
	coreIDs.clear();
	coreIDsStr = Util::StringUtil::Split(config.read("BindReplyThread"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	replyThread->StartStage(coreIDs, true);
}

void Server::InitForTest(std::string fileName) {
	std::ifstream ipFile(fileName);
	if (!ipFile.is_open()) {
		VERBOSE("cannot open %s", fileName.c_str());
		exit(0);
	}
	std::string line;
	std::string selfAddress = config.read("SelfAddress");
	SharedResources::MappingDS *mappingDS = tranxServiceSharedData->getMappingStorage();
	while (std::getline(ipFile, line)) {
		std::vector<std::string> fields = Util::StringUtil::Split(line, ',');
		if (fields.size() >= 3) {
			std::unordered_set<std::string> nodeIPs;
			for (int i = 2; i < fields.size(); ++i) {
				nodeIPs.insert(fields[i]);
			}
			mappingDS->UpdateLookupTable(fields[0], nodeIPs);
			VERBOSE("mapping: key %s-> ip %s...", fields[0].c_str(), fields[2].c_str());
			if (nodeIPs.find(selfAddress) != nodeIPs.end()) {
				uint64 which = Util::StringUtil::SDBMHash(fields[0].c_str());
				localStorage->Write(fields[0], fields[1]);
				VERBOSE("localdata: key %s, value %s", fields[0].c_str(), fields[1].c_str());
			}
		} else if (fields.size() == 1) {
			std::string value = "value_init";
			uint64 which = Util::StringUtil::SDBMHash(fields[0].c_str());
			if (allNodes[which % allNodes.size()] == selfAddress) {
				localStorage->Write(fields[0], value);
				VERBOSE("localdata: key %s, value %s", fields[0].c_str(), value.c_str());
			}
		} else {
			VERBOSE("data format wrong");
			exit(0);
		}
	}
}

void Server::InitDBForPerfTest(bool isRead, uint64 intializedDB) {
	std::string selfAddress = config.read("SelfAddress");
	std::string value = "value_init";
	for (uint64 i = 0; i < intializedDB; i++) {
		std::string key = Util::StringUtil::BuildKeyName(i);
		uint64 which = Util::StringUtil::SDBMHash(key.c_str());
		if (allNodes[which % allNodes.size()] == selfAddress) {
			if(isRead){
				localStorage->Read(key);
				VERBOSE("read data: key %s", key.c_str());
			}else{
				localStorage->Write(key, value);
				VERBOSE("write data: key %s, value %s", key.c_str(), value.c_str());
			}
		}
	}
}

void Server::DumpPerfLog() {
	VERBOSE("dumping performance log");
	RPC::ServerRPC *serverRPC = new RPC::ServerRPC();
	serverRPC->setIsPerfOutPutTask(true);
	clientService->SendToService(serverRPC);
	serverRPC = new RPC::ServerRPC();
	serverRPC->setIsPerfOutPutTask(true);
	tranxService->SendToService(serverRPC);
	serverRPC = new RPC::ServerRPC();
	serverRPC->setIsPerfOutPutTask(true);
	storageService->SendToService(serverRPC);

	Stages::TranxServiceQueueElement *element = new Stages::TranxServiceQueueElement();
	element->setIsPerfOutPutTask(true);
	tranxAck->SendToDaemon(element);
	element = new Stages::TranxServiceQueueElement();
	element->setIsPerfOutPutTask(true);
	gc->SendToDaemon(element);
	element = new Stages::TranxServiceQueueElement();
	element->setIsPerfOutPutTask(true);
	repartition->SendToDaemon(element);

	element = new Stages::TranxServiceQueueElement();
	element->setIsPerfOutPutTask(true);
	tranxRPCHelper->SendToStage(element);
	element = new Stages::TranxServiceQueueElement();
	element->setIsPerfOutPutTask(true);
	tranxStates->SendToStage(element);
	element = new Stages::TranxServiceQueueElement();
	element->setIsPerfOutPutTask(true);
	localStorage->SendToStage(element);
	element = new Stages::TranxServiceQueueElement();
	element->setIsPerfOutPutTask(true);
	tranxLog->SendToStage(element);
	element = new Stages::TranxServiceQueueElement();
	element->setIsPerfOutPutTask(true);
	lockTable->SendToStage(element);
}

void Server::ServerMainThread() {
	NOTICE("ServerMainThread started");
	Init();
	std::vector<std::string> allPorts = Util::StringUtil::Split(config.read("routerFrontPort"),
			',');
	uint32_t serverIndex = 0;
	std::vector<uint32_t> coreIDs;
	std::vector<std::string> coreIDsStr;
	coreIDsStr = Util::StringUtil::Split(config.read("BindServer"), '_');
	for (auto it = coreIDsStr.begin(); it != coreIDsStr.end(); ++it) {
		coreIDs.push_back(std::stoul(it->c_str(), nullptr, 10));
	}
	for (auto it = allPorts.begin(); it != allPorts.end(); ++it) {
		serverThreads.push_back(boost::thread(&Server::ServerThread, this, *it));
		Util::ThreadHelper::PinToCPUCores(serverThreads[serverIndex], coreIDs);
		serverIndex++;
	}
	while (true) {
		if (terminate.load()) {
			NOTICE("ServerMainThread exited");
			for (auto it = serverThreads.begin(); it != serverThreads.end(); ++it) {
				if (it->joinable()) {
					it->join();
				}
			}
			break;
		}
		sleep(1);
	}
	clientService->ShutService();
	tranxService->ShutService();
	storageService->ShutService();

	tranxAck->ShutService();
	repartition->ShutService();
	recoverLog->ShutService();
	gc->ShutService();

	tranxRPCHelper->ShutStage();
	tranxStates->ShutStage();
	localStorage->ShutStage();
	tranxLog->ShutStage();
	lockTable->ShutStage();
}

void Server::ServerThread(std::string port) {
	NOTICE("ServerThread binding to port %s started", port.c_str());
	std::string selfAddress = config.read("SelfAddress");
	RPC::ZeromqReceiver *receiver = new RPC::ZeromqReceiver(tranxServerInfo->GetContext());
	assert(receiver->Bind(selfAddress, port));
	int count = 0;
	Util::Buffer localBuffer;
	uint32 bufferSize = 500;
	localBuffer.SetData(new uint8[bufferSize], bufferSize);
	zmsg::ustring clientID;
	clientID.reserve(100);
	uint64_t sampleRateForTranx = 0;
	uint64_t sampleRateForClient = 0;
	uint64_t sampleRateForStorage = 0;
	while (true) {
		if (terminate.load()) {
			NOTICE("ServerThread binding to port %s exited", port.c_str());;
			delete receiver;
			break;
		}
		int size;
		void *message = NULL;
		bool inBuffer = false;
		try {
			message = receiver->Wait(size, clientID, localBuffer.GetData(), bufferSize, inBuffer);
		} catch (zmq::error_t& e) {
			std::cout << e.what() << std::endl;
		}
		if (inBuffer == false && message == NULL) {
			continue;
		}
		Util::Buffer buffer(message, size);
		RPC::ServerRPC *serverRPC;
		if (!freeList.try_dequeue(serverRPC)) {
			serverRPC = new RPC::ServerRPC();
		}
		if (inBuffer) {
			localBuffer.SetData(localBuffer.GetData(), size);
			assert(RPC::BaseRPC::parse(localBuffer, *serverRPC->GetRequest(), 0));
		} else {
			assert(RPC::BaseRPC::parse(buffer, *serverRPC->GetRequest(), 0));
		}
		//serverRPC->SetMonitor();
		serverRPC->SetClientID(clientID);
		//serverRPC->SetStartTime(startTime);
		RPC::GServiceID serviceID = serverRPC->GetReqService();
		if (serviceID == RPC::GServiceID::ClientService) {
			if (messagesFromClients.IsProcessed(serverRPC->GetClientID(),
					serverRPC->GetReqMessageID())) {
				AddToFreeList(serverRPC);
				continue;
			}
			sampleRateForClient++;
			if (sampleRateForClient % 10000 == 0) {
				VERBOSE("ClientService getting %lu messages", sampleRateForClient);
			}
			clientService->SendToService(serverRPC);
		} else if (serviceID == RPC::GServiceID::TranxService) {
			if (messagesFromPeers.IsProcessed(serverRPC->GetClientID(),
					serverRPC->GetReqMessageID())) {
				AddToFreeList(serverRPC);
				continue;
			}
			sampleRateForTranx++;
			if (sampleRateForTranx % 10000 == 0) {
				VERBOSE("TranxService getting %lu messages", sampleRateForTranx);
			}
			tranxService->SendToService(serverRPC);
		} else if (serviceID == RPC::GServiceID::StorageService) {
			sampleRateForStorage++;
			if (sampleRateForStorage % 10000 == 0) {
				VERBOSE("StorageService getting %lu messages", sampleRateForStorage);
			}
			storageService->SendToService(serverRPC);
		} else {
			delete serverRPC;
		}
	}
}

} /* namespace Server */
} /* namespace DTranx */

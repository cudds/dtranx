syntax = "proto2";
package DTranx.RPC;

import "Service/Transaction.proto";
import "Service/Storage.proto";
import "Service/Client.proto";
 
/**
 * Identifies which RPC service it is.
 * 	StorageService: access to the database
 * 	TranxService: requests for 2PC
 * 	ClientService: commit requests from the client, namely ClientCommit
 */
enum GServiceID {
	InvalidService = 0;
	StorageService = 1;
	TranxService = 2;
	ClientService = 3;
};

enum GStatus {
	/**
	 * The service processed the request and returned a valid protocol buffer
	 * with the results.
	 */
	OK = 0;

	/**
	 * An error specific to the particular service. The format of the remainder
	 * of the message is specific to the particular service.
	 */
	SERVICE_SPECIFIC_ERROR = 1;

	/**
	 * The server does not have the requested service.
	 */
	INVALID_SERVICE = 2;

	/**
	 * The server did not like the RPC request. Either it specified an opCode
	 * the server didn't understand or a request protocol buffer the server
	 * couldn't accept.
	 */
	INVALID_REQUEST = 3;

	/**
	 * currently used as an invalid state, e.g. in the BaseRPC GetRepStatus.
	 */
	INVALID_STATUS = 4;
};

message GIP{
	required uint32 fieldone = 1;
	required uint32 fieldtwo = 2;
	required uint32 fieldthree = 3;
	required uint32 fieldfour = 4;
}

message GRPCRequestMessage {
	required GServiceID serviceid = 1;
	required GIP ipint = 2;
	required uint32 port = 3;
	required uint64 messageid = 4;
	optional Service.GTranxServiceRPC tranxservicerpc = 5;
	optional Service.GStorageServiceRPC storageservicerpc = 6;
	optional Service.GClientServiceRPC clientservicerpc = 7;
};

message GMultiRPCRequestMessage{
	repeated GRPCRequestMessage messages = 1;
};

message GRPCResponseMessage {
	required GStatus status = 1;
	required uint64 messageid = 2;
	optional Service.GTranxServiceRPC tranxservicerpc = 3;
	optional Service.GStorageServiceRPC storageservicerpc = 4;
	optional Service.GClientServiceRPC clientservicerpc = 5;
};

message GMultiRPCResponseMessage{
	repeated GRPCResponseMessage messages = 1;
};

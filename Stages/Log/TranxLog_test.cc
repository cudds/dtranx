/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <unordered_map>
#include <boost/chrono.hpp>
#include "gtest/gtest.h"
#include "TranxLog.h"
#include "DTranx/Util/types.h"
#include "Util/FileUtil.h"
#include "DTranx/Service/Transaction.pb.h"
#include "DTranx/Util/types.h"
#include "DaemonStages/DaemonStagesMock.h"

using namespace DTranx;

class TranxLogTest: public ::testing::Test {
public:
	TranxLogTest() {
		Util::ConfigHelper configHelper;
		configHelper.readFile("DTranx.conf");
		tranxServerInfo = new SharedResources::TranxServerInfo();
		tranxServerInfo->SetConfig(configHelper);
		tranxLog = new Stages::Log::TranxLog("./dtranx.log", tranxServerInfo);
	}
	~TranxLogTest() {
		delete tranxLog;
		delete tranxServerInfo;
		Util::FileUtil::RemovePrefix("./", "dtranx.log");
	}
private:
	Stages::Log::TranxLog *tranxLog;
	SharedResources::TranxServerInfo *tranxServerInfo;

};

TEST_F(TranxLogTest, Prepare) {
	Stages::Log::GTranxLogRecord logRecord;
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	Service::GItem *item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2");
	std::vector<std::string> ips;
	ips.push_back("192.1.1.1");
	ips.push_back("192.1.1.2");
	Util::TranxID tranxID(1, 1);
	tranxLog->Prepare(tranxID, logRecord.read_set(), logRecord.write_set(), ips);
	tranxLog->CloseWrite();
	Stages::Log::GTranxLogRecord returnedLog = tranxLog->ReadNext();
	EXPECT_EQ(tranxID.GetNodeID(), returnedLog.tranxid().nodeid());
	EXPECT_EQ(tranxID.GetTranxID(), returnedLog.tranxid().tranxid());
	EXPECT_EQ(2, returnedLog.ips_size());
	EXPECT_TRUE(
			returnedLog.has_tranxtype() && returnedLog.tranxtype() == Service::GTranxType::NORMAL);
}

TEST_F(TranxLogTest, Prepare_repartition) {
	std::unordered_map<std::string, std::unordered_set<std::string> > newMapping;
	std::vector<std::string> allNodes;
	allNodes.push_back("192.1.1.1");
	allNodes.push_back("192.1.1.2");
	newMapping["key1"].insert("192.1.1.1");
	newMapping["key1"].insert("192.1.1.2");
	Util::TranxID tranxID(1, 1);
	tranxLog->Prepare(tranxID, newMapping, allNodes);
	tranxLog->CloseWrite();
	Stages::Log::GTranxLogRecord returnedLog = tranxLog->ReadNext();
	EXPECT_EQ(tranxID.GetNodeID(), returnedLog.tranxid().nodeid());
	EXPECT_EQ(tranxID.GetTranxID(), returnedLog.tranxid().tranxid());
	EXPECT_EQ(1, returnedLog.newmapping_size());
	EXPECT_EQ(2, returnedLog.newmapping(0).ips_size());
	EXPECT_TRUE(
			returnedLog.has_tranxtype()
					&& returnedLog.tranxtype() == Service::GTranxType::REPARTITION);
}

TEST_F(TranxLogTest, Ready_repartition) {
	Service::GTranxPrep::Request req;
	Service::GMapItem *item = req.add_newmappings();
	std::vector<std::string> allNodes;
	allNodes.push_back("192.1.1.1");
	allNodes.push_back("192.1.1.2");
	item->set_key("key1");
	item->add_ips("192.1.1.1");
	item->add_ips("192.1.1.2");
	item = req.add_newmappings();
	item->set_key("key2");
	item->add_ips("192.1.1.2");
	item->add_ips("192.1.1.5");

	Util::TranxID tranxID(1, 1);
	tranxLog->Ready(tranxID, req.newmappings(), allNodes);
	tranxLog->CloseWrite();
	Stages::Log::GTranxLogRecord returnedLog = tranxLog->ReadNext();
	EXPECT_EQ(tranxID.GetNodeID(), returnedLog.tranxid().nodeid());
	EXPECT_EQ(tranxID.GetTranxID(), returnedLog.tranxid().tranxid());
	EXPECT_EQ(2, returnedLog.newmapping_size());
	EXPECT_EQ("key1", returnedLog.newmapping(0).key());
	EXPECT_TRUE(
			returnedLog.has_tranxtype()
					&& returnedLog.tranxtype() == Service::GTranxType::REPARTITION);
}

TEST_F(TranxLogTest, Commit) {
	Util::TranxID tranxID(1, 1);
	tranxLog->Commit(tranxID, Service::GTranxType::NORMAL, Stages::Log::GLogType::COMMIT_1PC);
	tranxLog->CloseWrite();
	Stages::Log::GTranxLogRecord returnedLog = tranxLog->ReadNext();
	EXPECT_EQ(tranxID.GetNodeID(), returnedLog.tranxid().nodeid());
	EXPECT_EQ(tranxID.GetTranxID(), returnedLog.tranxid().tranxid());
	EXPECT_TRUE(returnedLog.has_tranxtype());
	EXPECT_EQ(Service::GTranxType::NORMAL, returnedLog.tranxtype());
	EXPECT_EQ(Stages::Log::GLogType::COMMIT_1PC, returnedLog.logtype());
}

TEST_F(TranxLogTest, Abort) {
	Util::TranxID tranxID1(1, 3);
	Util::TranxID tranxID2(2, 3);
	tranxLog->ParticipantAbort(tranxID1);
	tranxLog->CoordinatorAbort(tranxID2);
	tranxLog->CloseWrite();
	Stages::Log::GTranxLogRecord returnedLog = tranxLog->ReadNext();
	EXPECT_EQ(tranxID1.GetNodeID(), returnedLog.tranxid().nodeid());
	EXPECT_EQ(tranxID1.GetTranxID(), returnedLog.tranxid().tranxid());
	EXPECT_EQ(Service::GTranxType::NORMAL, returnedLog.tranxtype());
	EXPECT_EQ(Stages::Log::GLogType::PART_ABORT, returnedLog.logtype());
	returnedLog = tranxLog->ReadNext();
	EXPECT_EQ(tranxID2.GetNodeID(), returnedLog.tranxid().nodeid());
	EXPECT_EQ(tranxID2.GetTranxID(), returnedLog.tranxid().tranxid());
	EXPECT_EQ(Service::GTranxType::NORMAL, returnedLog.tranxtype());
	EXPECT_EQ(Stages::Log::GLogType::COORD_ABORT, returnedLog.logtype());
}

TEST_F(TranxLogTest, CoordAck) {
	Util::TranxID tranxID(4, 1);
	tranxLog->CoordinatorAck(tranxID, Service::GTranxType::REPARTITION);
	tranxLog->CloseWrite();
	Stages::Log::GTranxLogRecord returnedLog = tranxLog->ReadNext();
	EXPECT_EQ(tranxID.GetNodeID(), returnedLog.tranxid().nodeid());
	EXPECT_EQ(tranxID.GetTranxID(), returnedLog.tranxid().tranxid());
	EXPECT_TRUE(returnedLog.has_tranxtype());
	EXPECT_EQ(Service::GTranxType::REPARTITION, returnedLog.tranxtype());
	EXPECT_EQ(Stages::Log::GLogType::ACK, returnedLog.logtype());
}

TEST_F(TranxLogTest, Snapshot) {
	Stages::Log::GTranxLogRecord logRecord;
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	logRecord.set_tranxtype(Service::GTranxType::SNAPSHOT);
	std::vector<std::string> ips;
	ips.push_back("192.1.1.1");
	Util::TranxID tranxID(1, 1);
	tranxLog->Prepare(tranxID, logRecord.read_set(), logRecord.write_set(), ips, true);
	tranxLog->CloseWrite();
	Stages::Log::GTranxLogRecord returnedLog = tranxLog->ReadNext();
	EXPECT_EQ(tranxID.GetNodeID(), returnedLog.tranxid().nodeid());
	EXPECT_EQ(tranxID.GetTranxID(), returnedLog.tranxid().tranxid());
	EXPECT_TRUE(
			returnedLog.has_tranxtype()
					&& returnedLog.tranxtype() == Service::GTranxType::SNAPSHOT);
}

TEST_F(TranxLogTest, PerfTest) {
	boost::chrono::steady_clock::time_point start = boost::chrono::steady_clock::now();
	for (int i = 0; i < 500; i++) {
		Stages::Log::GTranxLogRecord logRecord;
		logRecord.mutable_tranxid()->set_nodeid(1);
		logRecord.mutable_tranxid()->set_tranxid(1);
		logRecord.set_logtype(Stages::Log::GLogType::PREP);
		Service::GItem *item = logRecord.add_read_set();
		item->set_key("key1");
		item->set_value("value1");
		item->set_version(1);
		item = logRecord.add_write_set();
		item->set_key("key2");
		item->set_value("value2");
		std::vector<std::string> ips;
		ips.push_back("192.1.1.1");
		Util::TranxID tranxID(1, 1);
		tranxLog->Prepare(tranxID, logRecord.read_set(), logRecord.write_set(), ips);
	}
	boost::chrono::steady_clock::time_point end = boost::chrono::steady_clock::now();
	int milliseconds =
			boost::chrono::duration_cast<boost::chrono::milliseconds>(end - start).count();
	tranxLog->CloseWrite();
	std::cout << "it takes " << milliseconds << " milliseconds to process 5000 log writes"
			<< std::endl;
}

TEST_F(TranxLogTest, gc) {
	std::unordered_map<int, uint64> tranxIDs;
	tranxIDs[1] = 5;
	tranxIDs[3] = 6;
	tranxLog->GCEntry(tranxIDs);
	tranxIDs.clear();
	tranxIDs = tranxLog->GetGCEntry();
	delete tranxLog;
	EXPECT_TRUE(tranxIDs.find(3) != tranxIDs.end());
	EXPECT_EQ(6, tranxIDs[3]);

	tranxLog = new Stages::Log::TranxLog("./dtranx.log", tranxServerInfo);

	tranxIDs[1] = 9;
	tranxIDs[3] = 6;
	tranxLog->GCEntry(tranxIDs);
	tranxIDs.clear();
	tranxIDs = tranxLog->GetGCEntry();
	EXPECT_TRUE(tranxIDs.find(1) != tranxIDs.end());
	EXPECT_EQ(9, tranxIDs[1]);

	Util::FileUtil::RemovePrefix("./", "gcdtranx.log");
}

class TranxLogTest_Stage: public ::testing::Test {
public:
	TranxLogTest_Stage() {
		Util::ConfigHelper configHelper;
		configHelper.readFile("DTranx.conf");
		tranxServerInfo = new SharedResources::TranxServerInfo();
		tranxServerInfo->SetConfig(configHelper);
		std::vector<std::string> ips;
		ips.push_back("192.168.0.1");
		ips.push_back("192.168.0.2");
		ips.push_back("192.168.0.3");
		tranxServerInfo->SetAllNodes(ips);
		tranxServerInfo->SetNodeID(2);
		tranxServerInfo->SetMode(SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER);
		tranxServiceSharedData = new SharedResources::TranxServiceSharedData(2, tranxServerInfo);
		tranxLog = new Stages::Log::TranxLog("./dtranx.log", tranxServerInfo);
		daemonStages = new DaemonStages::DaemonStagesMock(tranxServerInfo, tranxServiceSharedData);
		tranxLog->daemonStagesMock = daemonStages;
		tranxLog->StartStage();
	}
	~TranxLogTest_Stage() {
		tranxLog->ShutStage();
		delete tranxLog;
		delete tranxServerInfo;
		delete tranxServiceSharedData;
		delete daemonStages;
		Util::FileUtil::RemovePrefix("./", "dtranx.log");
		Util::FileUtil::RemoveDir("dtranx.mapdb");
	}

	Stages::Log::TranxLog *tranxLog;
	DaemonStages::DaemonStagesMock *daemonStages;
	SharedResources::TranxServiceSharedData *tranxServiceSharedData;
	SharedResources::TranxServerInfo *tranxServerInfo;
private:
	TranxLogTest_Stage(const TranxLogTest_Stage&) = delete;
	TranxLogTest_Stage& operator=(const TranxLogTest_Stage&) = delete;
};

TEST_F(TranxLogTest_Stage, _ReadyRepartition) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setLogTask(Stages::Log::TranxLogStageTask::_ReadyRepartition);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	RPC::ServerRPC serverRPC;
	Service::GMapItem *mapItem =
			serverRPC.requestMessage->mutable_tranxservicerpc()->mutable_tranxprep()->mutable_request()->add_newmappings();
	mapItem->set_key("key1");
	mapItem->add_ips("192.168.0.1");
	mapItem->add_ips("192.168.0.2");
	element.SetServerRPC(&serverRPC);
	tranxLog->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());

	tranxLog->CloseWrite();
	Stages::Log::GTranxLogRecord returnedLog = tranxLog->ReadNext();
	EXPECT_EQ(tranxID.GetNodeID(), returnedLog.tranxid().nodeid());
	EXPECT_EQ(tranxID.GetTranxID(), returnedLog.tranxid().tranxid());
	EXPECT_TRUE(returnedLog.has_logtype() && returnedLog.logtype() == Stages::Log::GLogType::READY);
	EXPECT_TRUE(
			returnedLog.has_tranxtype()
					&& returnedLog.tranxtype() == Service::GTranxType::REPARTITION);
}

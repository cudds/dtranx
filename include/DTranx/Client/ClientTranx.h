/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * ClientTranx is the interface class on the client side that enables
 * other applications to initiate transactions in DTranx project.
 *
 * API:
 * ClientTranx has read, write, commit interfaces for optimistic concurrency control
 * it calls RPC modules(Keyvalue class) to communicate with servers and cache modules
 *
 * Caching policy:
 * Caching will be consulted in Read, updated when commit fails, abort and read returned from the server
 */

#ifndef DTRANX_CLIENT_CLIENTTRANX_H_
#define DTRANX_CLIENT_CLIENTTRANX_H_

#include <unordered_map>
#include <iostream>
#include <memory>
#include "DTranx/Util/ConfigHelper.h"
#include "DTranx/Util/types.h"
#include "DTranx/Client/ClientCache.h"
#include "DTranx/Client/Client.h"
#include "DTranx/Service/Storage.pb.h"
#include "zmq.hpp"

namespace DTranx {
namespace Client {

class ClientTranx {
public:

	/*
	 * Strategy as to how to choose the coordinator,
	 * default is random.
	 * note that if ONEPHASECOMMIT is chosen, when no one server can handle the
	 * transaction, RANDOM is adopted
	 */
	enum CoordinatorStrategy {
		RANDOM, ONEPHASECOMMIT
	};

	/*
	 * cache can be initialized for sharing among clients
	 */
	ClientTranx(uint32_t routerFrontPort, std::shared_ptr<zmq::context_t> context,
			std::vector<std::string> ips, std::string selfAddressStr, uint32_t selfPort,
			bool cacheEnabled = true, CoordinatorStrategy coordinatorStrategy =
					CoordinatorStrategy::RANDOM);
	ClientTranx(uint32_t routerFrontPort, std::shared_ptr<zmq::context_t> context,
			std::vector<std::string> ips, std::shared_ptr<ClientCache> cache,
			std::string selfAddressStr, uint32_t selfPort,
			bool cacheEnabled = true, CoordinatorStrategy coordinatorStrategy =
					CoordinatorStrategy::RANDOM);
	void InitClients(std::string ip, Client* client);
	virtual ~ClientTranx() noexcept(false);
	Service::GStatus Read(std::string& key, std::string& value, bool snapshot = false);
	void Write(std::string& key, std::string& value);

	bool Commit();
	/*
	 * abort when some inconsistent data are read.
	 */
	void Abort();
	void Clear();

	/*
	 * thread safe
	 */
	void SetThreadsafe() {
		if (cacheEnabled) {
			assert(cache != NULL);
			cache->SetThreadSafety(true);
		}
	}
	/*
	 * the following *Debug functions are for debug usage only
	 * 	ReadDebug send read request to the server without waiting for any replies(pressure tests)
	 */
	void ReadDebug(std::string& key);

	std::string PrintTranx();
private:
	/*
	 * client cache
	 */
	bool cacheEnabled;
	std::shared_ptr<ClientCache> cache;

	/*
	 * clients related
	 *
	 * clients stores client interfaces to servers, it might be shared among transactions
	 * clientsPorts is a map structure that records ip to port mapping
	 * ports usage starts from selfClientPort
	 *
	 * reclaimClients: whether to reclaim clients in the destructor depends on
	 * 	whether clients are shared among different ClientTranx
	 *
	 * cluster: used to randomize coordinator choosing
	 */
	std::shared_ptr<zmq::context_t> context;
	uint32_t routerFrontPort;
	std::string selfAddressStr;
	uint32_t *selfAddress;
	uint32_t selfClientPort;
	std::unordered_map<std::string, Client*> clients;
	std::unordered_map<std::string, uint32_t> clientPorts;

	bool reclaimClients;
	std::vector<std::string> cluster;

	/*
	 * transaction data
	 * cluster is only used to find a coordinator to send the request
	 */
	std::unordered_map<std::string, std::pair<uint64, std::string> > readSet;
	std::unordered_map<std::string, std::pair<uint64, std::string> > snapshotReadSet;
	std::unordered_map<std::string, std::string> writeSet;

	/*
	 * Strategy for coordinator choosing
	 */
	CoordinatorStrategy coordStrategy;

	/*
	 * Update functions to local client cache
	 *
	 * UpdateDataCacheFromCommittedTranx is called when a commit succeeds.
	 * write items are updated in the local cache to advance the version
	 * number but only when the item is also a read item.
	 */
	void UpdateMappingCacheFromStorageService(std::string& key,
			Service::GReadData::Response& response);
	void UpdateMappingCacheFromClientService(Service::GClientCommit::Response& response);
	void UpdateDataCacheFromStorageService(std::string& key,
			Service::GReadData::Response& response);
	void UpdateDataCacheFromCommittedTranx();
	std::string GetMapping(const std::string& key);
	void CreateClientIfNotExist(std::string ip);

	/*
	 * Utility function
	 */
	std::unordered_set<std::string> GetOverlappedIPs(std::unordered_set<std::string> v1, std::unordered_set<std::string> v2);
};

} /* namespace Client */
} /* namespace DTranx */

#endif /* CLIENT_CLIENTTRANX_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_UTIL_THREADHELPER_H_
#define DTRANX_UTIL_THREADHELPER_H_

#include <sys/prctl.h>
#include <string>

namespace DTranx {
namespace Util {
namespace ThreadHelper {

inline void SetThreadID(std::string id) {
	prctl(PR_SET_NAME, id.c_str(), 0, 0, 0);
}

inline void PinToCPUCore(boost::thread &T, uint32_t coreID) {
	/*
	 * Create a cpu_set_t object representing a set of CPUs. Clear it and mark only CPU i as set.
	 */
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(coreID, &cpuset);
	int rc = pthread_setaffinity_np(T.native_handle(), sizeof(cpu_set_t), &cpuset);
	if (rc != 0) {
		std::cerr << "Error calling pthread_setaffinity_np: " << rc << "\n";
	} else {
		VERBOSE("successfully pin a thread to core %u", coreID);
	}
}

inline void PinToCPUCores(boost::thread &T, std::vector<uint32_t> coreIDs) {
	/*
	 * Create a cpu_set_t object representing a set of CPUs. Clear it and mark only CPU i as set.
	 */
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	std::string coresStr;
	for (auto it = coreIDs.begin(); it != coreIDs.end(); ++it) {
		CPU_SET(*it, &cpuset);
		coresStr += std::to_string(*it) + ",";
	}
	int rc = pthread_setaffinity_np(T.native_handle(), sizeof(cpu_set_t), &cpuset);
	if (rc != 0) {
		std::cerr << "Error calling pthread_setaffinity_np: " << rc << "\n";
	} else {
		VERBOSE("successfully pin a thread to core %s", coresStr.c_str());
	}
}

}
}
}

#endif /* DTRANX_UTIL_THREADHELPER_H_ */

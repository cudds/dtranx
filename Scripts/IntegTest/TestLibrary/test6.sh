#!/bin/bash
# test6 is a snapshot test
function test6(){
	# this first tranx will only fail the first time that the test6 is called. Otherwise, the snapshot is already created
	sudo docker exec dtranx1 bash -c '/dtranx/SnapshotRead -p Workloads/readall &> test6_snapshotfail'
	sudo docker cp dtranx1:/dtranx/test6_snapshotfail ./
	sudo docker exec dtranx1 bash -c 'rm /dtranx/test6_snapshotfail'	
	
	sudo docker exec dtranx1 bash -c '/dtranx/CreateSnapshot 172.17.0.2 172.17.0.2 &> test6_snapshotcreate'
	sudo docker cp dtranx1:/dtranx/test6_snapshotcreate ./
	sudo docker exec dtranx1 bash -c 'rm /dtranx/test6_snapshotcreate'	
	
	sudo docker exec dtranx1 bash -c '/dtranx/SnapshotRead -p Workloads/readall &> test6_snapshotsucc'
	sudo docker cp dtranx1:/dtranx/test6_snapshotsucc ./
	sudo docker exec dtranx1 bash -c 'rm /dtranx/test6_snapshotsucc'	
}
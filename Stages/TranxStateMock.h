/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_STAGES_TRANXSTATEMOCK_H_
#define DTRANX_STAGES_TRANXSTATEMOCK_H_

#include "TranxStates.h"

namespace DTranx {
namespace Stages {

class TranxStateMock: public TranxStates {
public:
	TranxStateMock(SharedResources::TranxServerInfo *tranxServerInfo)
			: TranxStates(tranxServerInfo), ProcessFunc() {
	}
	~TranxStateMock() {

	}

	void SetProcessFunc(void (*f)(TranxServiceQueueElement *)) {
		ProcessFunc = f;
	}

	virtual void SendToStage(TranxServiceQueueElement *element, int stageThread =
			TranxServiceQueueElement::DISPATCH_STAGE_THREADID, moodycamel::ProducerToken *ptok =
	NULL, uint64_t randSeed = 1) {
		/*
		 * process the request
		 */
		ProcessFunc(element);

		/*
		 * return to the previous stage
		 */
		TranxServiceQueueElement::StageID stageID = element->getStageId();
		if (stageID == TranxServiceQueueElement::StageID::TRANXSERVICE) {
			tranxService->SendToServiceInternal(element);
		} else if (stageID == TranxServiceQueueElement::StageID::CLIENTSERVICE) {
			clientService->SendToServiceInternal(element);
		} else if (stageID == TranxServiceQueueElement::StageID::STORAGESERVICE) {
			storageService->SendToServiceInternal(element);
		} else if (stageID == TranxServiceQueueElement::StageID::TRANXACK) {
			tranxAck->SendToDaemon(element);
		} else if (stageID == TranxServiceQueueElement::StageID::GCTHREAD) {
			gc->SendToDaemon(element);
		} else if (stageID == TranxServiceQueueElement::StageID::REPARTITION) {
			repartition->SendToDaemon(element);
		} else if (stageID == TranxServiceQueueElement::StageID::RECOVERY) {
			postRecovery->SendToDaemon(element);
		} else {
			assert(false);
		}
	}

	void (*ProcessFunc)(TranxServiceQueueElement *);
};
}
}

#endif /* DTRANX_STAGES_TRANXSTATEMOCK_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "PmemWritableFile.h"
#include "DTranx/Util/Log.h"

namespace DTranx {
namespace Stages {
namespace Log {

LogStatus PmemWritableFile::Append(const Slice& data) {
	if (plp == NULL) {
		return LogStatus::IOError(filename_, strerror(errno));
	}
	if(pmemlog_append(plp, data.data(), data.size()) == 0) {
		return LogStatus::OK();
	}
	return LogStatus::IOError(filename_, strerror(errno));
}

LogStatus PmemWritableFile::Close() {
	if (plp != NULL) {
		//VERBOSE("pmem closing %s", filename_.c_str());
		pmemlog_close(plp);
		plp = NULL;
	}
	return LogStatus::OK();
}

LogStatus PmemWritableFile::Flush() {
	return LogStatus::OK();
}

LogStatus PmemWritableFile::Sync() {
	return LogStatus::OK();
}

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

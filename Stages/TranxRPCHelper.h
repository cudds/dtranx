/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * TranxRPCHelper provides all sorts of utility functions to send RPC to participants/coordinators
 * it's used by TranxService, TranxRepartition and TranxAck
 * Peers are instantiated during initialization to avoid lock contention
 *
 * TranxRPCHelper has two stages that wraps TranxPeer around where the external
 * world send TranxServiceQueueElement to TranxRPCHelper and TranxRPCHelper send/receive
 * TranxRPC to/from TranxPeer
 */

#ifndef DTRANX_STAGES_TRANXRPCHELPER_H_
#define DTRANX_STAGES_TRANXRPCHELPER_H_

#include <unordered_map>
#include <unordered_set>
#include "TranxPeer.h"
#include "MidStage.h"

namespace DTranx {
namespace Stages {

class TranxRPCHelper: public MidStage {
public:
	/*
	 * Mode indicates how TranxRPCHelper initiates the peers.
	 * NORMAL: deterministically create peers from configuration
	 * UNITTEST_SERVER: not creating any peers
	 * UNITTEST_CLIENT: creating peers to localhost
	 */
	TranxRPCHelper(SharedResources::TranxServerInfo *tranxServerInfo,
			std::vector<uint32_t> coreIDs = std::vector<uint32_t>());
	virtual ~TranxRPCHelper() noexcept(false);

	/*
	 * low level interface to send to a peer a message that requires no response
	 */
	void SendNoResponse(std::string peerIP, RPC::TranxRPC *rpc);

	void SendPrepAndPoll(const Util::TranxID& tranxID, TranxServiceQueueElement *element);
	void SendAbortAndPoll(const Util::TranxID& tranxID, TranxServiceQueueElement *element);
	void SendCommitAndPoll(const Util::TranxID& tranxID, TranxServiceQueueElement *element);
	void SendInquiryAndPoll(const Util::TranxID& tranxID, TranxServiceQueueElement *element);

	/*
	 * special send rpc calls for repartitioning
	 */
	void SendPrepAndPollRepartition(const Util::TranxID& tranxID,
			TranxServiceQueueElement *element);
	void SendMigrateAndPoll(const Util::TranxID& tranxID, TranxServiceQueueElement *element);

	/*
	 * SendToTranxRPCInternal:
	 * 	first: used by TranxPeer to return the rpc call
	 * 	second: used by TranxRPC stage to inform the Internal thread of the incoming rpc call
	 */
	void SendToTranxRPCInternal(RPC::TranxRPC *rpc) {
		while (!rpcInternalProcessQueue.try_enqueue(rpc)) {
			if (rpcInternalProcessQueue.enqueue(rpc)) {
				break;
			}
		}
	}

	void SendToTranxRPCInternal(TranxServiceQueueElement *element, uint64_t randSeed = 1) {
		element->setStageEnteringTime(TranxServiceQueueElement::EPOCH_TIMEPOINT);
		if (IsEnabled(Stages::PerfStage::PerfKind::RESPONSE_TIME)) {
			if (randSeed % responsePerfFreq == 0) {
				element->setStageEnteringTime(boost::chrono::steady_clock::now());
			}
		}
		while (!rpcInternalElementProcessQueue.try_enqueue(element)) {
			if (rpcInternalElementProcessQueue.enqueue(element)) {
				break;
			}
		}
	}

	void LeaveStageInternal(TranxServiceQueueElement *element) {
		if (IsEnabled(Stages::PerfStage::PerfKind::RESPONSE_TIME)) {
			if (element->getStageEnteringTime() != TranxServiceQueueElement::EPOCH_TIMEPOINT) {
				boost::chrono::steady_clock::time_point now = boost::chrono::steady_clock::now();
				uint32 timeElapsed = boost::chrono::duration_cast<boost::chrono::nanoseconds>(
						now - element->getStageEnteringTime()).count();
				if (timeElapsed > 10000000) {
					AddLongLatency(7, element->getTranxRpcTask());
				}
				AddResponseTimeSample(7, timeElapsed);
			}
		}
	}

	void ShutStage() {
		terminateInternalThread.store(true);
		if (tranxRPCHelperInternalThread.joinable()) {
			tranxRPCHelperInternalThread.join();
		}
		MidStage::ShutStage();
	}

	virtual void StartStage(std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) {
		bool enableCoreBinding = false;
		uint32_t coreIDIndex = 0;
		assert(numOfStageThreads >= 1);
		enableCoreBinding = (coreIDs.size() == 2);
		stageThread.push_back(boost::thread(&TranxRPCHelper::StageThread, this));
		if (enableCoreBinding) {
			Util::ThreadHelper::PinToCPUCore(stageThread[0], coreIDs[coreIDIndex++]);
		}
		tranxRPCHelperInternalThread = boost::thread(&TranxRPCHelper::TranxRPCHelperInternalThread,
				this);
		if (enableCoreBinding) {
			Util::ThreadHelper::PinToCPUCore(tranxRPCHelperInternalThread, coreIDs[coreIDIndex++]);
		}
	}

	/*
	 * EnableForPeer is used in Server to enable tranxpeer
	 * all the performance enabler are in Server for clearance.
	 */
	void EnableForPeer(PerfStage::PerfKind perfKind) {
		peers->Enable(perfKind);
	}

	void AddToFreeList(RPC::TranxRPC *element) {
		element->Clear();
		freeList.try_enqueue(element);
	}

private:
	/***************************************************************************************************************
	 * Send RPC calls
	 */
	void PreparePrepReq(const Util::TranxID& tranxID,
			std::pair<std::vector<Service::GItem>, std::vector<Service::GItem> > &items,
			RPC::TranxRPC* rpc, std::vector<std::string> &ips);
	void SendRequest(const std::unordered_set<RPC::TranxRPC*>& RPCs);

private:
	std::unique_ptr<TranxPeer> peers;

	/*
	 * internal stage pipeline communicating with TranxPeer
	 */
	virtual void StageThread();
	void TranxRPCHelperInternalThread();
	boost::thread tranxRPCHelperInternalThread;
	std::atomic_bool terminateInternalThread;
	moodycamel::ConcurrentQueue<RPC::TranxRPC *> rpcInternalProcessQueue;
	moodycamel::ConcurrentQueue<TranxServiceQueueElement *> rpcInternalElementProcessQueue;

	/*
	 * TranxRPCHelperInternalThread will be waiting for tranxRPC to be returned
	 *
	 * internalProcessWaiting is used to cache the TranxServiceQueueElement;
	 * 	only accessed by TranxRPCHelperInternalThread
	 */
	std::unordered_map<Util::TranxID, TranxServiceQueueElement*, Util::KeyHasher> internalProcessWaitingElements;

	/*
	 * freeList is used to reuse the TranxRPC to avoid new/delete
	 */
	moodycamel::ConcurrentQueue<RPC::TranxRPC *> freeList;

	SharedResources::TranxServerInfo::Mode mode;

	/*
	 * Util function called by stage threads
	 */
	uint64 RPCTimeout;
	void SendBackToPreviousStage(const Util::TranxID& tranxID);
	void ProcessPeerReply(RPC::TranxRPC *rpc);

};

} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_TRANXRPCHELPER_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <cassert>
#include "TranxID.h"

namespace DTranx {
namespace Util {

TranxID::TranxID(int nodeID, uint64_t tranxID)
		: nodeID(nodeID), tranxID(tranxID) {

}
TranxID::TranxID()
		: nodeID(-1), tranxID(0) {

}

TranxID::~TranxID() {
}

TranxID::TranxID(const TranxID& other) {
	nodeID = other.nodeID;
	tranxID = other.tranxID;
}

void TranxID::operator=(const TranxID& other) {
	nodeID = other.nodeID;
	tranxID = other.tranxID;
}

bool TranxID::operator==(const TranxID& other) const {
	if (nodeID != other.nodeID) {
		return false;
	}
	if (tranxID != other.tranxID) {
		return false;
	}
	return true;
}

bool TranxID::operator!=(const TranxID& other) const {
	if (nodeID != other.nodeID) {
		return true;
	}
	if (tranxID != other.tranxID) {
		return true;
	}
	return false;
}

bool TranxID::operator<(const TranxID& other) const{
	if(nodeID < other.nodeID){
		return true;
	}
	else if(nodeID > other.nodeID){
		return false;
	}
	assert(nodeID == other.nodeID);
	return (tranxID < other.tranxID);
}

} /* namespace Util */
} /* namespace DTranx */

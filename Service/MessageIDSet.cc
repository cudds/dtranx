/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "MessageIDSet.h"
#include "Util/StringUtil.h"

namespace DTranx {
namespace Service {

MessageIDSet::MessageIDSet(bool threadsafe)
		: threadsafe(threadsafe) {
}

MessageIDSet::~MessageIDSet() noexcept(false) {
	for (auto it = messages.begin(); it != messages.end(); ++it) {
		delete it->second;
	}
}

bool MessageIDSet::IsProcessed(std::string client, uint64 messageID) {
	if (!threadsafe) {
		mutex.lock();
	}
	if (_IsProcessed(client, messageID)) {
		if (!threadsafe) {
			mutex.unlock();
		}
		return true;
	}
	_Insert(client, messageID);
	_GCProcessed(client);
	if (!threadsafe) {
		mutex.unlock();
	}
	return false;
}

uint64_t MessageIDSet::AggregateAllMessage() {
	if (!threadsafe) {
		mutex.lock();
	}
	uint64_t totalMessageNum = 0;
	for(auto it = messages.begin(); it != messages.end(); ++it){
		totalMessageNum += it->second->earliest;
		totalMessageNum += it->second->discreteSet.size();
	}
	if (!threadsafe) {
		mutex.unlock();
	}
	return totalMessageNum;
}

bool MessageIDSet::_IsProcessed(std::string clientID, uint64 messageID) {
	if (messages.find(clientID) == messages.end()) {
		return false;
	}
	if (messageID <= messages[clientID]->earliest) {
		return true;
	}
	if (messages[clientID]->discreteSet.count(messageID) != 0) {
		return true;
	}
	return false;
}

void MessageIDSet::_Insert(std::string clientID, uint64 messageID) {
	if (messages.find(clientID) == messages.end()) {
		messages[clientID] = new MessageInfo();
	}
	if (messageID <= messages[clientID]->earliest) {
		return;
	}
	messages[clientID]->discreteSet.insert(messageID);
}

void MessageIDSet::_GCProcessed(std::string clientID) {
	MessageInfo *messageInfo = messages[clientID];
	uint64 prevEarliest = messageInfo->earliest;
	for (auto it = messageInfo->discreteSet.begin(); it != messageInfo->discreteSet.end(); ++it) {
		if (*it - 1 == messageInfo->earliest) {
			messageInfo->earliest++;
		} else {
			break;
		}
	}
	for (int i = prevEarliest + 1; i <= messageInfo->earliest; ++i) {
		messageInfo->discreteSet.erase(i);
	}
}

} /* namespace Service */
} /* namespace DTranx */

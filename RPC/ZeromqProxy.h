/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * ZeromqProxy class set up the proxy and run it in a separate thread
 * in order to shut the proxy, first terminate the context
 */

#ifndef DTRANX_RPC_ZEROMQPROXY_H_
#define DTRANX_RPC_ZEROMQPROXY_H_

#include <boost/thread.hpp>
#include "zmsg.hpp"
#include "Util/StaticConfig.h"
#include "DTranx/Util/Log.h"

namespace DTranx {
namespace RPC {

class ZeromqProxy {

private:
	std::vector<boost::thread> zeromqProxyThreads;
	std::shared_ptr<zmq::context_t> context;
	std::vector<std::string> ports;
	std::string selfAddress;
	void StartProxyThread(std::string port, std::shared_ptr<zmq::context_t> context) {
		NOTICE("proxy thread starts at port %s", port.c_str());
		zmq::socket_t frontend(*context, ZMQ_ROUTER);
		zmq::socket_t backendReceiver(*context, ZMQ_DEALER);

		std::string localAddress = "tcp://" + selfAddress;
		std::string frontAddress = localAddress + ":" + port;
		try {
			frontend.bind(frontAddress.c_str());
			backendReceiver.bind(
					(std::string("inproc://")
							+ Util::StaticConfig::GetInstance()->Read("backendReceiver") + port).c_str());
		} catch (zmq::error_t &e) {
			std::cout << e.what() << std::endl;
			assert(false);
		}

		int lingtime = 0;
		frontend.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
		backendReceiver.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));

		int hwm = 0;
		size_t len = sizeof(hwm);
		frontend.setsockopt(ZMQ_SNDHWM, &hwm, len);
		backendReceiver.setsockopt(ZMQ_SNDHWM, &hwm, len);
		frontend.setsockopt(ZMQ_RCVHWM, &hwm, len);
		backendReceiver.setsockopt(ZMQ_RCVHWM, &hwm, len);
		int mandatory = 1;
		len = sizeof(mandatory);
		frontend.setsockopt(ZMQ_ROUTER_MANDATORY, &mandatory, len);
		try {
			zmq::proxy(frontend, backendReceiver, NULL);
		} catch (std::exception &e) {

		}
		frontend.close();
		backendReceiver.close();
		NOTICE("proxy thread exits at port %s", port.c_str());
	}
public:
	explicit ZeromqProxy(std::vector<std::string> ports, std::shared_ptr<zmq::context_t> context,
							std::string selfAddress = "*")
			: ports(ports), context(context), selfAddress(selfAddress) {
	}

	~ZeromqProxy() {
		ShutProxy();
	}

	void StartProxy() {
		for (auto it = ports.begin(); it != ports.end(); ++it) {
			zeromqProxyThreads.push_back(
					boost::thread(&ZeromqProxy::StartProxyThread, this, *it, context));
		}
	}

	void ShutProxy() {
		for (auto it = zeromqProxyThreads.begin(); it != zeromqProxyThreads.end(); ++it) {
			if (it->joinable()) {
				it->join();
			}
		}
	}
}
;

} /* namespace RPC */
} /* namespace DTranx */

#endif

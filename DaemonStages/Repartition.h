/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * Repartition stage is triggered by gcthread after gcthread garbage collects a WAL
 * then Repartition will broadcast tranxhistory or initiate a new repartition process.
 */

#ifndef DTRANX_DAEMONSTAGES_REPARTITION_H_
#define DTRANX_DAEMONSTAGES_REPARTITION_H_

#include <cassert>
#include <unordered_map>
#include <unordered_set>
#include "DaemonStages.h"
#include "DTranx/Client/Client.h"

namespace DTranx {
namespace Service {
class TranxService;
}
namespace DaemonStages {

class TranxAck;
class Repartition: public DaemonStages {
public:
	Repartition(SharedResources::TranxServerInfo *tranxServerInfo,
			SharedResources::TranxServiceSharedData *tranxServiceSharedData);
	virtual ~Repartition();

	/*
	 * TriggerRepartition is called by ClientService to
	 * manually trigger a repartition process even if there are
	 * not enough tranx history yet.
	 */
	void TriggerRepartition() {
		ManualTriggerRepartition.store(true);
	}

	/*
	 * interfaces to add to queue
	 */
	void SendToLogRepartition(std::string logName);
	void SendToCommitHistory(const Util::TranxID& tranxID);
	/*
	 * This function is only called by RecoverLog::recover
	 */
	void SetRepartitionTranxIDRecover(Util::TranxID& repartitionTranxID) {
		this->repartitionTranxID = repartitionTranxID;
	}
	virtual void DaemonThread();

	void InitStages(Stages::TranxRPCHelper *tranxRPCHelper,
			Stages::TranxStates *tranxStates,
			Stages::StringStorage *localStorage,
			Stages::Log::TranxLog *tranxLog,
			Stages::Lock::WriteBlockLockTable *lockTable,
			TranxAck *tranxAck,
			Service::TranxService *tranxService) {
		DaemonStages::InitStages(tranxRPCHelper, tranxStates, localStorage, tranxLog, lockTable);
		this->tranxAck = tranxAck;
		this->tranxService = tranxService;
	}

	virtual void StartService(std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) {
		bool enableCoreBinding = false;
		uint32_t coreIDIndex = 0;
		enableCoreBinding = (coreIDs.size() == 1);
		daemonThread = boost::thread(&Repartition::DaemonThread, this);
		if (enableCoreBinding) {
			Util::ThreadHelper::PinToCPUCore(daemonThread, coreIDs[coreIDIndex++]);
		}
	}

private:
	/*
	 * shared data, stages etc.
	 */
	TranxAck *tranxAck;
	Service::TranxService *tranxService;
	std::atomic_bool ManualTriggerRepartition;

	/*
	 * non shared data
	 */
	Util::TranxID repartitionTranxID;
	std::unordered_map<std::string, Client::Client*> clients;
	std::unordered_set<std::string> allClientsIps;
	std::unordered_set<std::string>::const_iterator clientItr;
	uint32_t committedTranxFilter;

	/*
	 * stage pipeline thread
	 *
	 * it broadcasts transaction history for partitioning purpose and initiate repartitioning in the master node
	 *
	 * localCommitHistorySet stores commit tranxID's because partitionThread only considers the committed ones
	 * it's from TranxStates
	 *
	 */
	moodycamel::ConcurrentQueue<std::string> repartitionLProcessQueue;
	moodycamel::ConcurrentQueue<Util::TranxID> repartitionCommitHistoryQueue;
	std::unordered_set<Util::TranxID, Util::KeyHasher> localCommitHistorySet;

	/*
	 * process log queue from GarbageCollector
	 * process repartition queue from other stages
	 */
	void ProcessLog(std::string logName);
	void ProcessRepartition(Stages::TranxServiceQueueElement *element);
	void RepartitionPrepare(std::vector<
			std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>>&tranxList);
	void MappingTransition(std::unordered_map<std::string, std::unordered_set<int> >& newMappings,
	std::unordered_map<std::string, int>& keysFrequency, int totalTranx);
	/*
	 * Generate the IP mapping from the partition ID, which is returned from Metis
	 */
	std::unordered_map<int, int> OptimizeForDataMigration(
	std::unordered_map<uint64, std::unordered_set<int> >& oldMappings,
	std::unordered_map<uint64, std::unordered_set<int> >& newMappings);
	void MigrateDBInitial(Stages::TranxServiceQueueElement *element);
	void MigrateDBClientSend(Stages::TranxServiceQueueElement *element);
	void MigrateDBLocalDelete(Stages::TranxServiceQueueElement *element);
};

}
/* namespace DaemonStages */
} /* namespace DTranx */

#endif /* DTRANX_DAEMONSTAGES_REPARTITION_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * TranxHistory stores transaction history records as input to the metis partitioning algorithm
 *
 * MaybeLater: store in disk when exceeding threshold
 *
 */

#ifndef DTRANX_SHAREDRESOURCES_TRANXHISTORY_H_
#define DTRANX_SHAREDRESOURCES_TRANXHISTORY_H_

#include <vector>
#include <unordered_set>
#include <atomic>
#include <memory>
#include <boost/thread.hpp>
#include "DTranx/Service/Transaction.pb.h"
#include "DTranx/Util/types.h"

namespace DTranx {
namespace SharedResources {

using DTranx::uint64;
class TranxHistory {
public:
	TranxHistory(uint64 partitionBroadcastThreshold,
			uint64 repartitionThreshold,
			uint64 inMemLimit = 100);
	TranxHistory(const TranxHistory& other);
	virtual ~TranxHistory() noexcept(false);
	/*
	 * AddTranx: adds a transaction into tranxList
	 * ExtractFromMessage: extract the tranx history from protobuf message and add to the local tranxList
	 * 	usually used in the RPC receiver handler
	 * ToBroadcast: whether to broadcast local tranx or not
	 * ToRePartition: whether to run metis again or not
	 * ForceRePartition: forcefully return the current tranx history
	 */

	void AddTranx(std::unordered_set<std::string> &readset,
			std::unordered_set<std::string> &writeset);
	void ExtractFromMessage(Service::GTranxPartition *partitionRequest);
	std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>>ToRePartition();
	std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>>ForceRePartition();
	bool ToBroadcast(Service::GTranxPartition* partitionRequest);

	void PrintTranxHistory();
private:
	/*
	 * EncodeIntoMessage: aggregate the local tranxList into the protobuf message
	 *	usually used for broadcasting local tranx history
	 *	used by ToBroadcast
	 *
	 *	Only this function is not protected by mutex because lock
	 *	are held when calling ToBroadcast
	 */
	void EncodeIntoMessage(Service::GTranxPartition *partitionRequest);
	/*
	 * class memebers:
	 * tranxList stores all transaction history for repartitioning purposes
	 * partitionBroadcastThreshold sets the limit for tranxhistory broadcast
	 * repartitionThreshold sets the limit for running repartitioning process
	 * inMemLimit sets the limit for tranxList size to avoid too much memory usage
	 */
	std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>> tranxList;
	uint64 partitionBroadcastThreshold;
	uint64 repartitionThreshold;
	uint64 inMemLimit;

	/*
	 * mutex guanratees thread safety 
	 * Repartition and tranxservice might be concurrently accessing/modifying tranxList
	 * protects concurrent access to tranxList
	 */
	boost::mutex mutex;
};

}
/* namespace SharedResources */
} /* namespace DTranx */

#endif /* DTRANX_SHAREDRESOURCES_TRANXHISTORY_H_ */

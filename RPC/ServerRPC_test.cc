/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "ServerRPC.h"

using namespace DTranx;

TEST(ServerRPC, time) {
	Util::Log::setLogPolicy( { { "RPC", "VERBOSE" } });
	RPC::ServerRPC serverRPC;
	serverRPC.SetReqMessageID(130);
	serverRPC.SetMonitor();
	serverRPC.PrintTime();
	Util::Log::setLogPolicy( { { "RPC", "NOTICE" } });
	/*
	 * manually check if time is output
	 */
}

TEST(ServerRPC, opcode) {
	RPC::ServerRPC serverRPC;
	serverRPC.requestMessage->mutable_clientservicerpc()->set_opcode(Service::GOpCode::CLIENT_COMMIT);
	EXPECT_EQ(Service::GOpCode::CLIENT_COMMIT, serverRPC.GetClientOpcode());
	serverRPC.requestMessage->mutable_storageservicerpc()->set_opcode(Service::GOpCode::WRITE_DATA);
		EXPECT_EQ(Service::GOpCode::WRITE_DATA, serverRPC.GetStorageOpcode());
		serverRPC.requestMessage->mutable_tranxservicerpc()->set_opcode(Service::GOpCode::TRANX_PREP);
		EXPECT_EQ(Service::GOpCode::TRANX_PREP, serverRPC.GetTranxOpcode());
}


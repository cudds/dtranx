/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "TranxAck.h"
#include "DTranx/Util/Log.h"
#include "DTranx/Util/ConfigHelper.h"
#include "Util/ThreadHelper.h"
#include "Util/StaticConfig.h"

#include "Stages/TranxRPCHelper.h"
#include "Stages/TranxStates.h"
#include "Stages/Log/TranxLog.h"

namespace DTranx {
namespace DaemonStages {

TranxAck::TranxAck(SharedResources::TranxServerInfo *tranxServerInfo,
		SharedResources::TranxServiceSharedData *tranxServiceSharedData)
		: DaemonStages(tranxServerInfo, tranxServiceSharedData, "tranxack") {
	nodeID = tranxServerInfo->GetNodeID();
}

TranxAck::~TranxAck() noexcept(false) {
}

void TranxAck::DaemonThread() {
	NOTICE("AckThread started");
	uint64_t randSeed = 0;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateThread.load()) {
			NOTICE("AckThread is reclaimed");
			break;
		}
		Stages::TranxServiceQueueElement *tmpRPCs[Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = daemonProcessQueue.try_dequeue_bulk(tmpRPCs,
				Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE);

		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1) {
				AddQueueLengthSample(0, daemonProcessQueue.size_approx());
			}
		}

		for (int i = 0; i < count; ++i) {
			Stages::TranxServiceQueueElement *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(0);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(0);
				std::vector<int> taskLongLatencySamples = GetLongLatency(0);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName);
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				delete element;
				continue;
			}
			//element->goThroughStage("TranxAck");
			ProcessAck(element);
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void TranxAck::ProcessAck(Stages::TranxServiceQueueElement *element) {
	int stageNum = element->getStageNum();
	if (stageNum == 1) {
		/*
		 * for 1PC transactions, no need to go through TranxRPCHelper stage
		 */
		if (element->getIps().size() == 1
				&& element->getIps()[0] == tranxServerInfo->GetSelfAddress()) {
			/*
			 element->setStageNum(3);
			 element->setLogTask(Stages::Log::TranxLogStageTask::_CoordinatorAck);
			 tranxLog->SendToStage(element,
			 Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			 return;
			 */
			element->setStageNum(4);
			element->setStateTask(Stages::TranxStatesStageTask::_SetToAck);
			element->setTerminateInThisStage(true);
			tranxStates->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID, ptok_tranxStates);
			return;
		}
		element->setStageNum(2);
		if (element->isAckCommitAbort()) {
			element->setTranxRpcTask(Stages::TranxRPCHelperStageTask::_SENDCOMMITANDPOLL);
		} else {
			element->setTranxRpcTask(Stages::TranxRPCHelperStageTask::_SENDABORTANDPOLL);
		}
		//VERBOSE("TranxAck ack for tranx %d#%lu(%d)", element->GetTranxID().GetNodeID(),
		//					element->GetTranxID().GetTranxID(), element->isAckCommitAbort());
		tranxRPCHelper->SendToStage(element,
				Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
	} else if (stageNum == 2) {
		Stages::TranxRPCHelperPollingResult peerResult = element->getTranxRpcResult();
		if (peerResult == Stages::TranxRPCHelperPollingResult::AnyFail) {
			ERROR("really? ack failed for %d#%lu", element->GetTranxID().GetNodeID(),
					element->GetTranxID().GetTranxID());
		}
		assert(peerResult != Stages::TranxRPCHelperPollingResult::AnyFail);
		if (peerResult == Stages::TranxRPCHelperPollingResult::AllOK) {
			element->setStageNum(3);
			element->setLogTask(Stages::Log::TranxLogStageTask::_CoordinatorAck);
			tranxLog->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		} else {
			VERBOSE("TranxAck retrying for tranx %d#%lu(%d)", element->GetTranxID().GetNodeID(),
					element->GetTranxID().GetTranxID(), element->isAckCommitAbort());
			element->setStageNum(1);
			SendToDaemon(element);
			return;
		}
	} else if (stageNum == 3) {
		element->setStageNum(4);
		element->setStateTask(Stages::TranxStatesStageTask::_SetToAck);
		element->setTerminateInThisStage(true);
		tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
				ptok_tranxStates);
	} else {
		assert(false);
	}
}

} /* namespace DaemonStages */
} /* namespace DTranx */

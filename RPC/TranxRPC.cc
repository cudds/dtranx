/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "TranxRPC.h"
#include "DTranx/Util/Log.h"

namespace DTranx {
namespace RPC {

TranxRPC::TranxRPC(Service::GOpCode opcode,
		GServiceID serviceType,
		uint64 messageID,
		bool ResponsesNeeded)
		: BaseRPC(), ResponsesNeeded(ResponsesNeeded) {
	SetReqService(serviceType);
	SetReqMessageID(messageID);
	SetTranxOpcode(opcode);
	waitingStatus = RPCWaitingStatus::WAITING;
	toMonitor = false;
	stageTask = Stages::TranxPeerStageTask::_INFORMPEERRECV;
}

TranxRPC::~TranxRPC() noexcept(false) {
}

TranxRPC::TranxRPC(TranxRPC&& other)
		: BaseRPC(std::forward<BaseRPC>(other)), waitingStatus(other.waitingStatus),
				ResponsesNeeded(other.ResponsesNeeded), toMonitor(other.toMonitor) {
}

TranxRPC& TranxRPC::operator=(TranxRPC&& other) {
	BaseRPC(std::forward<BaseRPC>(other));
	waitingStatus = other.waitingStatus;
	ResponsesNeeded = other.ResponsesNeeded;
	toMonitor = other.toMonitor;
	return *this;
}

void TranxRPC::Clear() {
	if (requestMessage != NULL) {
		//delete requestMessage->release_ip();
		//delete requestMessage->release_port();
		requestMessage->clear_tranxservicerpc();
	}

	if (responseMessage != NULL) {
		delete responseMessage;
		responseMessage = NULL;
	}
}

void TranxRPC::Assign(Service::GOpCode opcode,
		GServiceID serviceType,
		uint64 messageID,
		bool ResponsesNeeded) {
	this->ResponsesNeeded = ResponsesNeeded;
	SetReqService(serviceType);
	//SetReqMessageID(messageID);
	SetTranxOpcode(opcode);
	waitingStatus = RPCWaitingStatus::WAITING;
	toMonitor = false;
	stageTask = Stages::TranxPeerStageTask::_INFORMPEERRECV;
}

} /* namespace RPC */
} /* namespace DTranx */

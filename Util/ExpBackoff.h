/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * 	ExpBackOff: exponentially backoff algorithm
 * 	currently not used yet.
 */

#ifndef DTRANX_UTIL_EXPBACKOFF_H_
#define DTRANX_UTIL_EXPBACKOFF_H_

#include "DTranx/Util/types.h"

namespace DTranx {
namespace Util {

/*
 * ExpBackoff backoff exponentially for optimistic transactions
 */
class ExpBackoff {
public:
	ExpBackoff();
	ExpBackoff(uint32 initialBackoff, uint32 maxBackoff, uint32 maxRetries);
	virtual ~ExpBackoff() noexcept(false);
	bool wait();
private:
	uint32 initialBackoff;
	uint32 maxBackoff;
	uint32 maxRetries;
	uint32 retries;
	uint32 backoff;
};

} /* namespace Util */
} /* namespace DTranx */

#endif /* DTRANX_UTIL_EXPBACKOFF_H_ */

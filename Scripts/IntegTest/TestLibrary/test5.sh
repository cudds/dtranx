#!/bin/bash
# test5 kills and then restart one server on the run.
function test5(){
	sudo docker exec dtranx1 bash -c '/dtranx/SimpleTranx -p Workloads/writedistributed -r 300 &> /dtranx/test5_write' &
	sleep 5
	RestartDtranx dtranx1
	wait
	sudo docker cp dtranx1:/dtranx/DTranx.out ./test5_recoverylogscheck
	sudo docker exec dtranx1 bash -c 'rm /dtranx/test5_write'
}

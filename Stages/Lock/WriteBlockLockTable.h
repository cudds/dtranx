/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * WriteBlockLockTable is a lock table that has write request blocking and read request non-blocking.
 * Write block will timeout to avoid deadlock.
 *
 * Epoch lock are implicitly requested/released in RequestLocks/ReleaseLocks
 */

#ifndef DTRANX_STAGES_WRITEBLOCKLOCKTABLE_H_
#define DTRANX_STAGES_WRITEBLOCKLOCKTABLE_H_

#include "BasicLockTable.h"

namespace DTranx {
namespace Stages {
namespace Lock {

class WriteBlockLockTable: public BasicLockTable {
public:
	WriteBlockLockTable(SharedResources::TranxServerInfo *tranxServerInfo,
			int numOfStageThreads = 1);
	virtual ~WriteBlockLockTable() noexcept(false);

	/*
	 * element being NULL used by RecoverLog where no queue element is created
	 */

	bool RequestEpochLock(const Util::TranxID& tranxID,
			TranxServiceQueueElement *element = NULL,
			BasicLockTableLockMode lockMode = BasicLockTableLockMode::Exclusive);

	bool RequestLocks(std::unordered_map<std::string, BasicLockTableLockMode>& keys_mode,
			const Util::TranxID& tranxID,
			TranxServiceQueueElement *element = NULL);
	/*
	 * RequestLocksBlocking is used for repartitioning, called by MappingTransition in TranxService or Repartition thread
	 */
	bool RequestLocksBlocking(std::unordered_map<std::string, BasicLockTableLockMode>& keys_mode,
			const Util::TranxID& tranxID,
			TranxServiceQueueElement *element = NULL);

	bool ReleaseEpochLock(const Util::TranxID& tranxID);

	bool ReleaseLocks(std::unordered_set<std::string>& keys, const Util::TranxID& tranxID);

	bool ReleaseLocks(std::vector<std::string>& keys, const Util::TranxID& tranxID);

	bool ReleaseLocks(std::unordered_map<std::string, BasicLockTableLockMode>& keys_mode, const Util::TranxID& tranxID);

	/*
	 * epoch request goes to one specific thread
	 * basically there are dispatch thread + stage threads + epoch thread
	 */
	virtual void StartStage(std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) {
		bool enableCoreBinding = false;
		uint32_t coreIDIndex = 0;
		assert(numOfStageThreads >= 1);
		if (numOfStageThreads == 1) {
			enableCoreBinding = (coreIDs.size() == 1);
		} else {
			enableCoreBinding = (coreIDs.size() == numOfStageThreads + 2);
		}
		for (int i = 0; i < numOfStageThreads; ++i) {
			processQueues.push_back(moodycamel::ConcurrentQueue<TranxServiceQueueElement *>(1000));
		}
		if (numOfStageThreads > 1) {
			processQueues.push_back(moodycamel::ConcurrentQueue<TranxServiceQueueElement *>(1000));
		}
		for (int i = 0; i < numOfStageThreads; ++i) {
			stageThread.push_back(boost::thread(&WriteBlockLockTable::StageThread, this, i));
			if (enableCoreBinding) {
				Util::ThreadHelper::PinToCPUCore(stageThread[i], coreIDs[coreIDIndex++]);
			}
		}
		if (numOfStageThreads > 1) {
			stageThread.push_back(
					boost::thread(&WriteBlockLockTable::StageThread, this, numOfStageThreads));
			if (enableCoreBinding) {
				Util::ThreadHelper::PinToCPUCore(stageThread[numOfStageThreads],
						coreIDs[coreIDIndex++]);
			}
		}
		if (numOfStageThreads > 1) {
			dispatchThread = boost::thread(&WriteBlockLockTable::DispatchThread, this);
			if (enableCoreBinding) {
				Util::ThreadHelper::PinToCPUCore(dispatchThread, coreIDs[coreIDIndex++]);
			}
		}
	}

private:
	bool RequestLock(const std::string& key, const Util::TranxID& tranxID, BasicLockTableLockMode lockMode);

	bool ReleaseLock(const std::string& key, const Util::TranxID& tranxID);

	/*
	 * LockFailureRelease release the locks when it fails or times out
	 * 	return true means it's done, false means it's sent to other stagethreads to further process
	 */
	bool LockFailureRelease(TranxServiceQueueElement *element, std::unordered_map<std::string, Lock::BasicLockTableLockMode>& chosenKeysMode, int stageThreadIndex);

	static uint64 _lockTimeout;
	/*
	 * TODO: separate data for stage threads
	 */
	std::unordered_set<Util::TranxID, Util::KeyHasher> abortBeforePrepareTranx;

	virtual void StageThread(int stageThreadIndex);
	virtual void DispatchThread();
};

}
/* namespace Lock */
} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_WRITEBLOCKLOCKTABLE_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "TranxRPCHelper.h"
#include "Util/StaticConfig.h"
#include "Service/TranxService.h"
#include "Service/StorageService.h"
#include "Service/ClientService.h"
#include "DaemonStages/Repartition.h"
#include "DaemonStages/TranxAck.h"
#include "Server/RecoverLog.h"

namespace DTranx {
namespace Stages {

TranxRPCHelper::TranxRPCHelper(SharedResources::TranxServerInfo *tranxServerInfo,
		std::vector<uint32_t> coreIDs)
		: MidStage(tranxServerInfo, 1, "TranxRPCHelper"), peers(), terminateInternalThread(
		false), rpcInternalProcessQueue(1000), rpcInternalElementProcessQueue(1000), freeList(1000) {
	RPCTimeout = std::stoll(Util::StaticConfig::GetInstance()->Read("RPCTimeout"));
	mode = tranxServerInfo->GetMode();
	peers = std::unique_ptr<Stages::TranxPeer>(
			new Stages::TranxPeer(this, tranxServerInfo, coreIDs));
}

TranxRPCHelper::~TranxRPCHelper() noexcept(false) {
}

void TranxRPCHelper::SendNoResponse(std::string peerIP, RPC::TranxRPC *rpc) {
	rpc->setRemoteIp(peerIP);
	peers->SendToPeerSend(rpc);
}

void TranxRPCHelper::SendPrepAndPoll(const Util::TranxID& tranxID,
		TranxServiceQueueElement *element) {
	std::unordered_map<std::string,
			std::pair<std::vector<Service::GItem>, std::vector<Service::GItem>> > &items =
			element->getItems();
	RPC::TranxRPC* rpc;
	Service::GTranxType tranxType = element->getTranxType();
	std::vector<std::string> ips;
	if (mode == SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER) {
		element->setTranxRpcResult(TranxRPCHelperPollingResult::AllOK);
		SendBackToPreviousStage(tranxID);
		return;
	}
	for (auto cur = items.begin(); cur != items.end(); ++cur) {
		ips.push_back(cur->first);
	}
	int internalProcessNumPeerStillWaited = 0;
	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	if (tranxType == Service::GTranxType::SNAPSHOT) {
		std::vector<std::string> allNodes = tranxServerInfo->GetAllNodes();
		for (auto it = allNodes.begin(); it != allNodes.end(); ++it) {
			if (*it == selfAddress) {
				continue;
			}
			internalProcessNumPeerStillWaited++;
			if (!freeList.try_dequeue(rpc)) {
				rpc = new RPC::TranxRPC(Service::GOpCode::TRANX_PREP, RPC::GServiceID::TranxService,
						0,
						true);
			} else {
				rpc->Assign(Service::GOpCode::TRANX_PREP, RPC::GServiceID::TranxService, 0,
				true);
			}

			rpc->GetTranxRequest()->mutable_tranxprep()->mutable_request()->set_tranxtype(
					tranxType);
			rpc->GetTranxRequest()->mutable_tranxprep()->mutable_request()->mutable_tranxid()->set_nodeid(
					tranxID.GetNodeID());
			rpc->GetTranxRequest()->mutable_tranxprep()->mutable_request()->mutable_tranxid()->set_tranxid(
					tranxID.GetTranxID());
			rpc->waitingStatus = RPC::TranxRPC::RPCWaitingStatus::WAITING;
			rpc->setRemoteIp(*it);
			element->addTranxRPCs(rpc);
		}
	} else {
		for (auto cur = items.begin(); cur != items.end(); ++cur) {
			std::string ip = cur->first;
			if (ip == selfAddress) {
				continue;
			}
			internalProcessNumPeerStillWaited++;
			if (!freeList.try_dequeue(rpc)) {
				rpc = new RPC::TranxRPC(Service::GOpCode::TRANX_PREP, RPC::GServiceID::TranxService,
						0,
						true);
			} else {
				rpc->Assign(Service::GOpCode::TRANX_PREP, RPC::GServiceID::TranxService, 0,
				true);
			}
			rpc->waitingStatus = RPC::TranxRPC::RPCWaitingStatus::WAITING;
			PreparePrepReq(tranxID, cur->second, rpc, ips);
			rpc->GetTranxRequest()->mutable_tranxprep()->mutable_request()->set_tranxtype(
					tranxType);
			rpc->setRemoteIp(ip);
			element->addTranxRPCs(rpc);
		}
	}
	element->setTranxRpcNumOfPeerStillWaiting(internalProcessNumPeerStillWaited);
	assert(internalProcessNumPeerStillWaited == element->getTranxRpCs().size());
	LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
			element->getTranxRpcTask());
	SendToTranxRPCInternal(element);
}

void TranxRPCHelper::SendAbortAndPoll(const Util::TranxID& tranxID,
		TranxServiceQueueElement *element) {
	RPC::TranxRPC* rpc;
	std::vector<std::string> ips = element->getIps();
	std::set<std::string> ipsSet;
	if (mode == SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER) {
		element->setTranxRpcResult(TranxRPCHelperPollingResult::AllOK);
		SendBackToPreviousStage(tranxID);
		return;
	}
	for (auto it = ips.begin(); it != ips.end(); ++it) {
		ipsSet.insert(*it);
	}
	int internalProcessNumPeerStillWaited = 0;
	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	for (auto cur = ipsSet.begin(); cur != ipsSet.end(); ++cur) {
		std::string ip = *cur;
		if (ip == selfAddress) {
			continue;
		}
		internalProcessNumPeerStillWaited++;
		if (!freeList.try_dequeue(rpc)) {
			rpc = new RPC::TranxRPC(Service::GOpCode::TRANX_ABORT, RPC::GServiceID::TranxService, 0,
			true);
		} else {
			rpc->Assign(Service::GOpCode::TRANX_ABORT, RPC::GServiceID::TranxService, 0,
			true);
		}
		rpc->GetTranxRequest()->mutable_tranxabort()->mutable_request()->mutable_tranxid()->set_nodeid(
				tranxID.GetNodeID());
		rpc->GetTranxRequest()->mutable_tranxabort()->mutable_request()->mutable_tranxid()->set_tranxid(
				tranxID.GetTranxID());
		rpc->waitingStatus = RPC::TranxRPC::RPCWaitingStatus::WAITING;
		rpc->setRemoteIp(ip);
		element->addTranxRPCs(rpc);
	}
	//VERBOSE("send abort for tranxid %d#%lu", tranxID.GetNodeID(), tranxID.GetTranxID());
	element->setTranxRpcNumOfPeerStillWaiting(internalProcessNumPeerStillWaited);
	assert(internalProcessNumPeerStillWaited == element->getTranxRpCs().size());
	LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
			element->getTranxRpcTask());
	SendToTranxRPCInternal(element);
}

void TranxRPCHelper::SendCommitAndPoll(const Util::TranxID& tranxID,
		TranxServiceQueueElement *element) {
	RPC::TranxRPC* rpc;
	std::vector<std::string> ips = element->getIps();
	std::set<std::string> ipsSet;
	Service::GTranxType tranxType = element->getTranxType();
	if (mode == SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER) {
		element->setTranxRpcResult(TranxRPCHelperPollingResult::AllOK);
		SendBackToPreviousStage(tranxID);
		return;
	}
	for (auto it = ips.begin(); it != ips.end(); ++it) {
		ipsSet.insert(*it);
	}
	int internalProcessNumPeerStillWaited = 0;
	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	for (auto cur = ipsSet.begin(); cur != ipsSet.end(); ++cur) {
		std::string ip = *cur;
		if (ip == selfAddress) {
			continue;
		}
		internalProcessNumPeerStillWaited++;
		if (!freeList.try_dequeue(rpc)) {
			rpc = new RPC::TranxRPC(Service::GOpCode::TRANX_COMMIT, RPC::GServiceID::TranxService,
					0,
					true);
		} else {
			rpc->Assign(Service::GOpCode::TRANX_COMMIT, RPC::GServiceID::TranxService, 0,
			true);
		}
		rpc->GetTranxRequest()->mutable_tranxcommit()->mutable_request()->set_tranxtype(tranxType);
		rpc->GetTranxRequest()->mutable_tranxcommit()->mutable_request()->mutable_tranxid()->set_nodeid(
				tranxID.GetNodeID());
		rpc->GetTranxRequest()->mutable_tranxcommit()->mutable_request()->mutable_tranxid()->set_tranxid(
				tranxID.GetTranxID());
		rpc->waitingStatus = RPC::TranxRPC::RPCWaitingStatus::WAITING;
		rpc->setRemoteIp(ip);
		element->addTranxRPCs(rpc);
	}
	element->setTranxRpcNumOfPeerStillWaiting(internalProcessNumPeerStillWaited);
	LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
			element->getTranxRpcTask());
	SendToTranxRPCInternal(element);
}

void TranxRPCHelper::SendInquiryAndPoll(const Util::TranxID& tranxID,
		TranxServiceQueueElement *element) {
	RPC::TranxRPC* rpc;
	std::vector<std::string> ips = element->getIps();
	std::set<std::string> ipsSet;
	if (mode == SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER) {
		element->setTranxRpcInquiryResult(Service::GInquiryTranxStatus::COMMITTED);
		SendBackToPreviousStage(tranxID);
		return;
	}
	for (auto it = ips.begin(); it != ips.end(); ++it) {
		ipsSet.insert(*it);
	}
	int internalProcessNumPeerStillWaited = 0;
	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	for (auto cur = ipsSet.begin(); cur != ipsSet.end(); ++cur) {
		std::string ip = *cur;
		if (ip == selfAddress) {
			continue;
		}
		internalProcessNumPeerStillWaited++;
		if (!freeList.try_dequeue(rpc)) {
			rpc = new RPC::TranxRPC(Service::GOpCode::TRANX_INQUIRE, RPC::GServiceID::TranxService,
					0,
					true);
		} else {
			rpc->Assign(Service::GOpCode::TRANX_INQUIRE, RPC::GServiceID::TranxService, 0,
			true);
		}
		rpc->GetTranxRequest()->mutable_tranxinquire()->mutable_request()->mutable_tranxid()->set_nodeid(
				tranxID.GetNodeID());
		rpc->GetTranxRequest()->mutable_tranxinquire()->mutable_request()->mutable_tranxid()->set_tranxid(
				tranxID.GetTranxID());
		rpc->waitingStatus = RPC::TranxRPC::RPCWaitingStatus::WAITING;
		rpc->setRemoteIp(ip);
		element->addTranxRPCs(rpc);
	}
	element->setTranxRpcNumOfPeerStillWaiting(internalProcessNumPeerStillWaited);
	LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
			element->getTranxRpcTask());
	SendToTranxRPCInternal(element);
}

void TranxRPCHelper::SendPrepAndPollRepartition(const Util::TranxID& tranxID,
		TranxServiceQueueElement *element) {
	RPC::TranxRPC* rpc;
	std::vector<std::string> allNodes = tranxServerInfo->GetAllNodes();
	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	std::unordered_map<std::string, std::unordered_set<std::string> > newMappings =
			element->getNewMapping();
	if (mode == SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER) {
		element->setTranxRpcResult(TranxRPCHelperPollingResult::AllOK);
		SendBackToPreviousStage(tranxID);
	}
	int internalProcessNumPeerStillWaited = 0;
	for (auto it = allNodes.begin(); it != allNodes.end(); ++it) {
		if (*it == selfAddress) {
			continue;
		}
		internalProcessNumPeerStillWaited++;
		if (!freeList.try_dequeue(rpc)) {
			rpc = new RPC::TranxRPC(Service::GOpCode::TRANX_PREP, RPC::GServiceID::TranxService, 0,
			true);
		} else {
			rpc->Assign(Service::GOpCode::TRANX_PREP, RPC::GServiceID::TranxService, 0,
			true);
		}
		rpc->GetTranxRequest()->mutable_tranxprep()->mutable_request()->mutable_tranxid()->set_nodeid(
				tranxID.GetNodeID());
		rpc->GetTranxRequest()->mutable_tranxprep()->mutable_request()->mutable_tranxid()->set_tranxid(
				tranxID.GetTranxID());
		rpc->GetTranxRequest()->mutable_tranxprep()->mutable_request()->set_tranxtype(
				Service::GTranxType::REPARTITION);
		rpc->GetTranxRequest()->mutable_tranxprep()->mutable_request()->add_ips(*it);
		rpc->waitingStatus = RPC::TranxRPC::RPCWaitingStatus::WAITING;
		rpc->setRemoteIp(*it);
		element->addTranxRPCs(rpc);
		for (auto it_b = allNodes.begin(); it_b != allNodes.end(); ++it_b) {
			rpc->GetTranxRequest()->mutable_tranxprep()->mutable_request()->add_ips(*it_b);
		}

		for (auto it_b = newMappings.begin(); it_b != newMappings.end(); ++it_b) {
			Service::GMapItem *item =
					rpc->GetTranxRequest()->mutable_tranxprep()->mutable_request()->add_newmappings();
			item->set_key(it_b->first);
			for (auto ip = it_b->second.begin(); ip != it_b->second.end(); ++ip) {
				item->add_ips(*ip);
			}
		}
	}
	element->setTranxRpcNumOfPeerStillWaiting(internalProcessNumPeerStillWaited);
	LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
			element->getTranxRpcTask());
	SendToTranxRPCInternal(element);
}

void TranxRPCHelper::SendMigrateAndPoll(const Util::TranxID& tranxID,
		TranxServiceQueueElement *element) {
	RPC::TranxRPC* rpc;
	std::vector<std::string> allNodes = tranxServerInfo->GetAllNodes();
	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	if (mode == SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER) {
		element->setTranxRpcResult(TranxRPCHelperPollingResult::AllOK);
		SendBackToPreviousStage(tranxID);
	}
	int internalProcessNumPeerStillWaited = 0;
	for (auto it = allNodes.begin(); it != allNodes.end(); ++it) {
		if (*it == selfAddress) {
			continue;
		}
		internalProcessNumPeerStillWaited++;
		if (!freeList.try_dequeue(rpc)) {
			rpc = new RPC::TranxRPC(Service::GOpCode::TRANX_MIGRATION,
					RPC::GServiceID::TranxService, 0,
					true);
		} else {
			rpc->Assign(Service::GOpCode::TRANX_MIGRATION, RPC::GServiceID::TranxService, 0,
			true);
		}
		rpc->GetTranxRequest()->mutable_tranxmigration()->mutable_request()->mutable_tranxid()->set_nodeid(
				tranxID.GetNodeID());
		rpc->GetTranxRequest()->mutable_tranxmigration()->mutable_request()->mutable_tranxid()->set_tranxid(
				tranxID.GetTranxID());
		rpc->waitingStatus = RPC::TranxRPC::RPCWaitingStatus::WAITING;
		rpc->setRemoteIp(*it);
		element->addTranxRPCs(rpc);
	}
	element->setTranxRpcNumOfPeerStillWaiting(internalProcessNumPeerStillWaited);
	LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
			element->getTranxRpcTask());
	SendToTranxRPCInternal(element);
}

void TranxRPCHelper::PreparePrepReq(const Util::TranxID& tranxID,
		std::pair<std::vector<Service::GItem>, std::vector<Service::GItem> > &items,
		RPC::TranxRPC* rpc, std::vector<std::string> &ips) {
	Service::GTranxPrep::Request* request =
			rpc->GetTranxRequest()->mutable_tranxprep()->mutable_request();
	for (auto it = items.first.begin(); it != items.first.end(); ++it) {
		Service::GItem& item = *request->add_read_set();
		item.set_key(it->key());
		if (it->has_version()) {
			item.set_version(it->version());
		}
	}
	for (auto it = items.second.begin(); it != items.second.end(); ++it) {
		Service::GItem& item = *request->add_write_set();
		item.set_key(it->key());
		item.set_value(it->value());
		if (it->has_version()) {
			item.set_version(it->version());
		}
	}
	for (auto it = ips.begin(); it != ips.end(); ++it) {
		request->add_ips(*it);
	}
	request->mutable_tranxid()->set_nodeid(tranxID.GetNodeID());
	request->mutable_tranxid()->set_tranxid(tranxID.GetTranxID());
}

void TranxRPCHelper::SendRequest(const std::unordered_set<RPC::TranxRPC*>& RPCs) {
	/*
	 * copying here is to avoid race contention on element when
	 * it's sent back to previous stage but there are still tranxRPC's to send
	 */
	std::unordered_set<RPC::TranxRPC*> RPCsCopy(RPCs);
	for (auto it = RPCsCopy.begin(); it != RPCsCopy.end(); ++it) {
		peers->SendToPeerSend(*it);
	}
}

void TranxRPCHelper::StageThread() {
	NOTICE("TranxRPCHelperThread started");
	uint64_t randSeed = 0;
	while (true) {
		if (terminateThread.load()) {
			NOTICE("TranxRPCHelperThread is reclaimed");
			break;
		}
		TranxServiceQueueElement *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = processQueue.try_dequeue_bulk(tmpRPCs,
				TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1) {
				AddQueueLengthSample(0, processQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			TranxServiceQueueElement *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(0);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(0);
				std::vector<int> taskLongLatencySamples = GetLongLatency(0);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName);
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				SendToTranxRPCInternal(element);
				RPC::TranxRPC *tranxRPC = new RPC::TranxRPC(Service::GOpCode::TRANX_ABORT,
						RPC::GServiceID::TranxService, 0,
						true);
				tranxRPC->setStageTask(TranxPeerStageTask::_OUTPUTPERFPEER);
				peers->SendToPeerSend(tranxRPC);
				RPC::TranxRPC *tranxRPC2 = new RPC::TranxRPC(Service::GOpCode::TRANX_ABORT,
						RPC::GServiceID::TranxService, 0,
						true);
				tranxRPC2->setStageTask(TranxPeerStageTask::_OUTPUTPERFPEER);
				SendToTranxRPCInternal(tranxRPC2);
				continue;
			}
			//element->goThroughStage("tranxrpchelper");
			TranxRPCHelperStageTask task = element->getTranxRpcTask();
			const Util::TranxID& tranxID = element->GetTranxID();

			if (task == TranxRPCHelperStageTask::_SENDNOWTRANXRPCHELPER) {
				SendRequest(element->getTranxRpCs());
			} else {
				element->setTranxRpcStartTime(boost::chrono::steady_clock::now());
				if (task == TranxRPCHelperStageTask::_SENDPREPANDPOLL) {
					SendPrepAndPoll(tranxID, element);
				} else if (task == TranxRPCHelperStageTask::_SENDABORTANDPOLL) {
					SendAbortAndPoll(tranxID, element);
				} else if (task == TranxRPCHelperStageTask::_SENDCOMMITANDPOLL) {
					SendCommitAndPoll(tranxID, element);
				} else if (task == TranxRPCHelperStageTask::_SENDINQUIRYANDPOLL) {
					SendInquiryAndPoll(tranxID, element);
				} else if (task == TranxRPCHelperStageTask::_SENDMIGRATEANDPOLL) {
					SendMigrateAndPoll(tranxID, element);
				} else if (task == TranxRPCHelperStageTask::_SENDPREPANDPOLLREPARTITION) {
					SendPrepAndPollRepartition(tranxID, element);
				} else {
					assert(false);
				}
			}
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void TranxRPCHelper::TranxRPCHelperInternalThread() {
	NOTICE("TranxRPCHelperInternalThread started");
	uint64_t randSeed = 0;
	while (true) {
		if (terminateInternalThread.load()) {
			NOTICE("TranxRPCHelperInternalThread is reclaimed");
			break;
		}
		/*
		 * process requests coming from TranxPeers
		 */
		RPC::TranxRPC *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = rpcInternalProcessQueue.try_dequeue_bulk(tmpRPCs,
				TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1) {
				AddQueueLengthSample(8, rpcInternalProcessQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			if (tmpRPCs[i]->getStageTask() == TranxPeerStageTask::_OUTPUTPERFPEER) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(8);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(8);
				std::vector<int> taskLongLatencySamples = GetLongLatency(8);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_internal_tranxrpc");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				AddToFreeList(tmpRPCs[i]);
				continue;
			}
			ProcessPeerReply(tmpRPCs[i]);
		}

		/*
		 * process requests coming from TranxRPCHelper: initial stage
		 */
		TranxServiceQueueElement *tmpElements[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		int count_b = rpcInternalElementProcessQueue.try_dequeue_bulk(tmpElements,
				TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1) {
				AddQueueLengthSample(7, rpcInternalElementProcessQueue.size_approx());
			}
		}

		for (int i = 0; i < count_b; ++i) {
			TranxServiceQueueElement *element = tmpElements[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(7);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(7);
				std::vector<int> taskLongLatencySamples = GetLongLatency(7);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_internal");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				delete element;
				continue;
			}
			const Util::TranxID& tranxID = element->GetTranxID();
			internalProcessWaitingElements[tranxID] = element;
			element->setTranxRpcTask(TranxRPCHelperStageTask::_SENDNOWTRANXRPCHELPER);
			SendToStage(element);
		}

		/*
		 * MaybeLater: this might affect the performance every once in a while
		 */
		if (count == 0 && count_b == 0) {
			/*
			 * inquire request will not be timeout and retried continuously
			 */
			boost::chrono::steady_clock::time_point now = boost::chrono::steady_clock::now();
			for (auto it = internalProcessWaitingElements.begin();
					it != internalProcessWaitingElements.end(); ++it) {
				const Util::TranxID& tranxID = it->first;

				if (it->second->getTranxRpcTask() == TranxRPCHelperStageTask::_SENDMIGRATEANDPOLL) {
					/*
					 * timeout does not apply to migrate since it might take long time
					 */
					continue;
				}
				uint64 timePassed = boost::chrono::duration_cast<boost::chrono::milliseconds>(
						now - it->second->getTranxRpcStartTime()).count();

				if (timePassed > RPCTimeout) {
					if (it->second->getTranxRpcTask()
							== TranxRPCHelperStageTask::_SENDINQUIRYANDPOLL) {
						/*
						 * this rarely happens because not a single machine in the cluster has commit/abort info
						 */
						assert(false);
					} else {
						it->second->setTranxRpcResult(TranxRPCHelperPollingResult::Timeout);
						VERBOSE("tranx %d#%lu timeout in tranxrpchelper", tranxID.GetNodeID(),
								tranxID.GetTranxID());
						SendBackToPreviousStage(tranxID);
						break;
						/*
						 * if tranxRPC's are not reclaimed here, memory might be lost if not responded.
						 */
					}
				}
			}
		}

		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void TranxRPCHelper::SendBackToPreviousStage(const Util::TranxID& tranxID) {
	//VERBOSE("tranxrpcHelper sending back %p with tranxID %s",
	//		internalProcessWaitingElements[tranxID], tranxID.c_str());
	assert(internalProcessWaitingElements.find(tranxID) != internalProcessWaitingElements.end());
	LeaveStageInternal(internalProcessWaitingElements[tranxID]);
	internalProcessWaitingElements[tranxID]->clearTranxRPCs();
	TranxServiceQueueElement::StageID stageID =
			internalProcessWaitingElements[tranxID]->getStageId();
	if (stageID == Stages::TranxServiceQueueElement::StageID::TRANXSERVICE) {
		tranxService->SendToServiceInternal(internalProcessWaitingElements[tranxID]);
	} else if (stageID == Stages::TranxServiceQueueElement::StageID::CLIENTSERVICE) {
		clientService->SendToServiceInternal(internalProcessWaitingElements[tranxID]);
	} else if (stageID == Stages::TranxServiceQueueElement::StageID::STORAGESERVICE) {
		storageService->SendToServiceInternal(internalProcessWaitingElements[tranxID]);
	} else if (stageID == Stages::TranxServiceQueueElement::StageID::TRANXACK) {
		tranxAck->SendToDaemon(internalProcessWaitingElements[tranxID]);
	} else if (stageID == Stages::TranxServiceQueueElement::StageID::REPARTITION) {
		repartition->SendToDaemon(internalProcessWaitingElements[tranxID]);
	} else if (stageID == Stages::TranxServiceQueueElement::StageID::RECOVERY) {
		postRecovery->SendToDaemon(internalProcessWaitingElements[tranxID]);
	} else {
		assert(false);
	}
	internalProcessWaitingElements.erase(tranxID);
}

void TranxRPCHelper::ProcessPeerReply(RPC::TranxRPC *rpc) {
	Util::TranxID tranxID = rpc->GetRepTranxID();
	if (internalProcessWaitingElements.find(tranxID) == internalProcessWaitingElements.end()) {
		//VERBOSE(
		//		"TranxRPCHelper Internal getting a message for tranxID %d#%lu that's already completed",
		//		tranxID.GetNodeID(), tranxID.GetTranxID());
		AddToFreeList(rpc);
	} else {
		/*
		 * To map TranxRPC to the Queue Element, tranxRPC pointer is stored in queue element
		 *
		 */
		if (!internalProcessWaitingElements[tranxID]->checkInTranxRPCs(rpc)) {
			//VERBOSE(
			//		"TranxRPCHelper Internal getting a message for tranxID %d#%lu that's not in waiting tranxrpc list",
			//		tranxID.GetNodeID(), tranxID.GetTranxID());
			AddToFreeList(rpc);
			return;
		}
		assert(rpc->waitingStatus == RPC::TranxRPC::RPCWaitingStatus::HAS_REPLY);

		if (internalProcessWaitingElements[tranxID]->getTranxRpcTask()
				== TranxRPCHelperStageTask::_SENDINQUIRYANDPOLL) {
			Service::GInquiryTranxStatus result =
					rpc->GetTranxResponse()->tranxinquire().response().tranxstatus();
			if (result == Service::GInquiryTranxStatus::COMMITTED) {
				internalProcessWaitingElements[tranxID]->setTranxRpcInquiryResult(
						Service::GInquiryTranxStatus::COMMITTED);
			} else if (result == Service::GInquiryTranxStatus::ABORTED) {
				internalProcessWaitingElements[tranxID]->setTranxRpcInquiryResult(
						Service::GInquiryTranxStatus::ABORTED);
			} else {
				if (internalProcessWaitingElements[tranxID]->decAndGetTranxRpcNumOfPeerStillWaiting()
						== 0) {
					internalProcessWaitingElements[tranxID]->setTranxRpcInquiryResult(
							Service::GInquiryTranxStatus::WAITING);
				} else {
					AddToFreeList(rpc);
					return;
				}
			}
			SendBackToPreviousStage(tranxID);
			AddToFreeList(rpc);
			return;
		} else {
			int numOfTranxRPCLeft =
					internalProcessWaitingElements[tranxID]->decAndGetTranxRpcNumOfPeerStillWaiting();
			if (rpc->GetTranxRepStatus() == Service::GStatus::OK) {
				if (numOfTranxRPCLeft == 0) {
					internalProcessWaitingElements[tranxID]->setTranxRpcResult(
							TranxRPCHelperPollingResult::AllOK);
					SendBackToPreviousStage(tranxID);
				}
				AddToFreeList(rpc);
				return;
			} else {
				internalProcessWaitingElements[tranxID]->setTranxRpcResult(
						TranxRPCHelperPollingResult::AnyFail);
				SendBackToPreviousStage(tranxID);
				AddToFreeList(rpc);
				return;
			}
		}
	}
}

} /* namespace Stages */
} /* namespace DTranx */

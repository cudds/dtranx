/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * 	RSMHelper interacts with the logcabin service to launch replicated state machines,
 * 	reconfigure it and shut it down.
 */

#include <utility>
#include <boost/thread.hpp>
#include <cassert>
#include <logcabin/RPC/DaemonSendHelper.h>
#include "RSMHelper.h"
#include "DTranx/Util/Log.h"
#include "Util/StringUtil.h"

namespace DTranx {
namespace Server {

RSMHelper::RSMHelper(Util::ConfigHelper& config) {
	smID = "sm1";
	RSMPort = config.read("RSMPort");
	RSMDaemonPort = config.read("RSMDaemonPort");
	selfAddr = config.read("SelfAddress");
}

RSMHelper::~RSMHelper() noexcept(false) {
}

void RSMHelper::PrintConfiguration(
		const std::pair<uint64_t, LogCabin::Client::Configuration>& configuration) {
	std::string result;
	result += "Configuration " + std::to_string(configuration.first) + ":";
	for (auto it = configuration.second.begin(); it != configuration.second.end(); ++it) {
		result += "- " + std::to_string(it->first) + ": " + it->second;
	}
	VERBOSE("%s", result.c_str());
}

bool RSMHelper::Reconfig(std::vector<std::string> servers, std::string leader, std::string smID) {
	LogCabin::Client::Cluster cluster(leader, smID);
	std::pair<uint64_t, LogCabin::Client::Configuration> configuration = cluster.getConfiguration();
	PrintConfiguration(configuration);
	uint64_t id = configuration.first;
	LogCabin::Client::Configuration serversConfig;
	for (uint64_t i = 0; i < servers.size(); ++i)
		serversConfig.emplace_back(i + 1, servers.at(i));
	LogCabin::Client::ConfigurationResult result = cluster.setConfiguration(id, serversConfig);

	std::string printTmp;
	printTmp += "Reconfiguration ";
	if (result.status == LogCabin::Client::ConfigurationResult::OK) {
		printTmp += "OK";
	} else if (result.status == LogCabin::Client::ConfigurationResult::CHANGED) {
		printTmp += "CHANGED";
	} else if (result.status == LogCabin::Client::ConfigurationResult::BAD) {
		printTmp += "BAD SERVERS:";
		for (auto it = result.badServers.begin(); it != result.badServers.end(); ++it) {
			printTmp += "- " + std::to_string(it->first) + ": " + it->second;
		}
	}
	VERBOSE("%s", printTmp.c_str());
	PrintConfiguration(cluster.getConfiguration());

	if (result.status == LogCabin::Client::ConfigurationResult::OK)
		return true;
	else
		return false;
}

void RSMHelper::Init(std::vector<std::string> ips) {
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(1);
	LogCabin::RPC::DaemonSendHelper sendHelper = LogCabin::RPC::DaemonSendHelper(context);
	this->ips = ips;
	std::string ipString;
	std::vector<std::string> ipVector;
	int selfRank = 0;
	for (auto it = ips.begin(); it != ips.end(); ++it) {
		if (*it == selfAddr) {
			selfRank = 1 + std::distance(ips.begin(), it);
		}
		ipString += *it + ";";
		ipVector.push_back(*it + ":" + RSMPort);
	}
	assert(selfRank != 0);
	sendHelper.send(LogCabin::RPC::DaemonSendHelper::Kind::CREATE,
			ipString.substr(0, ipString.size() - 1), smID, selfRank, RSMDaemonPort);
	/*
	 * sleep here to avoid too many connections from connectrandom(LeaderRPC)
	 * because create rsm took some time to launch
	 */
	boost::this_thread::sleep_for(boost::chrono::milliseconds(2000));
	if (selfRank == 1) {
		while (!Reconfig(ipVector, ips[0], smID)) {
			boost::this_thread::sleep_for(boost::chrono::milliseconds(1000));
		}
	}
}

void RSMHelper::Shutdown() {
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(1);
	LogCabin::RPC::DaemonSendHelper sendHelper = LogCabin::RPC::DaemonSendHelper(context);
	std::string ipString;
	std::vector<std::string> ipVector;
	int selfRank = 0;
	if (ips.empty()) {
		return;
	}
	for (auto it = ips.begin(); it != ips.end(); ++it) {
		if (*it == selfAddr) {
			selfRank = 1 + std::distance(ips.begin(), it);
		}
		ipString += *it + ";";
		ipVector.push_back(*it + ":" + RSMPort);
	}
	assert(selfRank != 0);
	sendHelper.send(LogCabin::RPC::DaemonSendHelper::Kind::DELETE,
			ipString.substr(0, ipString.size() - 1), smID, selfRank, RSMDaemonPort);
}

LogCabin::Client::Configuration RSMHelper::GetAllServers() {
	VERBOSE("get all servers");
	LogCabin::Client::Cluster cluster(selfAddr, smID);
	std::pair<uint64_t, LogCabin::Client::Configuration> configuration = cluster.getConfiguration();
	return configuration.second;
}

uint64_t RSMHelper::GetServerID() {
	LogCabin::Client::Configuration config = GetAllServers();
	while (config.size() == 0) {
		config = GetAllServers();
	}
	for (auto it = config.begin(); it != config.end(); ++it) {
		std::string ip = Util::StringUtil::Split(it->second, ':')[0];
		if (ip == selfAddr) {
			return it->first;
		}
	}
	assert(false);
	return -1;
}

} /* namespace Server */
} /* namespace DTranx */

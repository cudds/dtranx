/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "BasicLockTable.h"
#include "gtest/gtest.h"
#include "Util/FileUtil.h"

using namespace DTranx;

class BasicLockTest: public ::testing::Test {
public:
	BasicLockTest() {
		Util::ConfigHelper configHelper;
		configHelper.readFile("DTranx.conf");
		tranxServerInfo = new SharedResources::TranxServerInfo();
		tranxServerInfo->SetConfig(configHelper);
		lockTable = new Stages::Lock::BasicLockTable(tranxServerInfo);
	}
	~BasicLockTest() {
		delete tranxServerInfo;
		delete lockTable;
	}
private:
	Stages::Lock::BasicLockTable *lockTable;
	DTranx::SharedResources::TranxServerInfo *tranxServerInfo;

};

TEST_F(BasicLockTest, NonBlockBasic) {
	bool reqReturn, repReturn;
	Util::TranxID tranxID1(1, 1);
	Util::TranxID tranxID2(1, 2);
	Util::TranxID tranxID3(1, 3);
	//lock list empty
	reqReturn = lockTable->_RequestLock("key1", tranxID1,
			Stages::Lock::BasicLockTableLockMode::Shared, false);
	EXPECT_TRUE(reqReturn);
	//k1: shared t1

	//test:sharing lock
	reqReturn = lockTable->_RequestLock("key1", tranxID2,
			Stages::Lock::BasicLockTableLockMode::Shared, false);
	EXPECT_TRUE(reqReturn);
	//k1: shared t1, shared t2

	//test:exclusive lock conflict with sharing lock
	reqReturn = lockTable->_RequestLock("key1", tranxID3,
			Stages::Lock::BasicLockTableLockMode::Exclusive, false);
	EXPECT_FALSE(reqReturn);
	//k1: shared t1, shared t2

	//test:release lock
	reqReturn = lockTable->_ReleaseLock("key1", tranxID1);
	EXPECT_TRUE(reqReturn);
	//k1: shared t2

	reqReturn = lockTable->_ReleaseLock("key1", tranxID2);
	EXPECT_TRUE(reqReturn);
	//k1: empty

	//test: exclusive lock
	reqReturn = lockTable->_RequestLock("key1", tranxID3,
			Stages::Lock::BasicLockTableLockMode::Exclusive, false);
	EXPECT_TRUE(reqReturn);
	//k1: ex t3

	//test: sharing lock conflict with exclusive lock
	reqReturn = lockTable->_RequestLock("key1", tranxID2,
			Stages::Lock::BasicLockTableLockMode::Shared, false);
	EXPECT_FALSE(reqReturn);
	//k1: ex t3

	//test: shared lock on top on exclusive lock
	reqReturn = lockTable->_RequestLock("key1", tranxID3,
			Stages::Lock::BasicLockTableLockMode::Shared, false);
	EXPECT_TRUE(reqReturn);
	//k1: ex t3

	//test: multiple key lock
	reqReturn = lockTable->_RequestLock("key2", tranxID2,
			Stages::Lock::BasicLockTableLockMode::Shared, false);
	EXPECT_TRUE(reqReturn);
	//k1: ex t3
	//k2: shared t2
}

TEST_F(BasicLockTest, NonBlockBasic2) {
	bool reqReturn, repReturn;
	Util::TranxID tranxID1(1, 1);
	Util::TranxID tranxID2(1, 2);
	reqReturn = lockTable->_RequestLock("key1", tranxID1,
			Stages::Lock::BasicLockTableLockMode::Shared, false);
	EXPECT_TRUE(reqReturn);
	//k1: shared t1

	reqReturn = lockTable->_RequestLock("key1", tranxID2,
			Stages::Lock::BasicLockTableLockMode::Shared, false);
	EXPECT_TRUE(reqReturn);
	//k1: shared t1, shared t2

	//test: upgrade to exclusive lock when other holds shared too
	reqReturn = lockTable->_RequestLock("key1", tranxID1,
			Stages::Lock::BasicLockTableLockMode::Exclusive, false);
	EXPECT_FALSE(reqReturn);
	//k1: shared t1, shared t2
}

TEST_F(BasicLockTest, NonBlockRelease) {
	bool reqReturn, repReturn;
	Util::TranxID tranxID1(1, 1);
	//lock list empty
	reqReturn = lockTable->_RequestLock("key1", tranxID1,
			Stages::Lock::BasicLockTableLockMode::Shared, false);
	EXPECT_TRUE(reqReturn);
	//k1: shared t1

	repReturn = lockTable->_ReleaseLock("key1", tranxID1);
	EXPECT_TRUE(repReturn);
	//k1: empty

	//test: repetitive release
	repReturn = lockTable->_ReleaseLock("key1", tranxID1);
	EXPECT_FALSE(repReturn);
	//k1: empty
}

TEST_F(BasicLockTest, BlockRequest) {
	bool reqReturn, repReturn;
	Util::TranxID tranxID1(1, 1);
	Util::TranxID tranxID2(1, 2);
	Util::TranxID tranxID3(1, 3);
	//lock list empty
	reqReturn = lockTable->_RequestLock("key1", tranxID2,
			Stages::Lock::BasicLockTableLockMode::Shared, true);
	EXPECT_TRUE(reqReturn);
	//k1: shared t2

	reqReturn = lockTable->_RequestLock("key1", tranxID1,
			Stages::Lock::BasicLockTableLockMode::Exclusive, true);
	EXPECT_FALSE(reqReturn);
	//k1: shared t2, ex t1 wait

	//test: repetitive exclusive lock
	reqReturn = lockTable->_RequestLock("key1", tranxID1,
			Stages::Lock::BasicLockTableLockMode::Exclusive, true);
	EXPECT_FALSE(reqReturn);
	//k1: shared t2, ex t1 wait

	//test: waiting exclusive lock blocks other shared lock request
	reqReturn = lockTable->_RequestLock("key1", tranxID3,
			Stages::Lock::BasicLockTableLockMode::Shared, true);
	EXPECT_FALSE(reqReturn);
	//k1: shared t2, ex t1 wait, shared t3 wait

	reqReturn = lockTable->_ReleaseLock("key1", tranxID2);
	EXPECT_TRUE(reqReturn);
	//k1: ex t1, shared t3 wait

	reqReturn = lockTable->_RequestLock("key1", tranxID1,
			Stages::Lock::BasicLockTableLockMode::Exclusive, true);
	EXPECT_TRUE(reqReturn);
	//k1: ex t1

	reqReturn = lockTable->_ReleaseLock("key1", tranxID1);
	EXPECT_TRUE(reqReturn);
	//shared t3 wait

	reqReturn = lockTable->_RequestLock("key1", tranxID3,
			Stages::Lock::BasicLockTableLockMode::Shared, true);
	EXPECT_TRUE(reqReturn);
	//shared t3
}

TEST_F(BasicLockTest, CancelRequest) {
	bool reqReturn, repReturn;
	Util::TranxID tranxID1(1, 1);
	Util::TranxID tranxID2(1, 2);
	Util::TranxID tranxID3(1, 3);
	//lock list empty
	reqReturn = lockTable->_RequestLock("key1", tranxID2,
			Stages::Lock::BasicLockTableLockMode::Shared, true);
	EXPECT_TRUE(reqReturn);
	//k1: shared t2

	reqReturn = lockTable->_RequestLock("key1", tranxID1,
			Stages::Lock::BasicLockTableLockMode::Exclusive, true);
	EXPECT_FALSE(reqReturn);
	//k1: shared t2, ex t1 wait

	//test: cancel a waiting req
	reqReturn = lockTable->_CancelRequest("key1", tranxID1);
	EXPECT_TRUE(reqReturn);
	//k1: shared t2

	//test: cancel a non waiting req
	reqReturn = lockTable->_CancelRequest("key1", tranxID3);
	EXPECT_FALSE(reqReturn);
	//k1: shared t2

	reqReturn = lockTable->_RequestLock("key1", tranxID3,
			Stages::Lock::BasicLockTableLockMode::Shared, true);
	EXPECT_TRUE(reqReturn);
	//k1: shared t2, shared t3
}

TEST_F(BasicLockTest, EpochLock) {
	bool reqReturn, repReturn;
	Util::TranxID tranxID1(1, 1);
	Util::TranxID tranxID2(1, 2);
	//lock list empty
	reqReturn = lockTable->_RequestEpochLock(tranxID1,
			Stages::Lock::BasicLockTableLockMode::Shared);
	EXPECT_TRUE(reqReturn);
	//shared t1
	reqReturn = lockTable->_RequestEpochLock(tranxID2,
			Stages::Lock::BasicLockTableLockMode::Exclusive);
	EXPECT_FALSE(reqReturn);
	//shared t1
	reqReturn = lockTable->_ReleaseEpochLock(tranxID1);
	EXPECT_TRUE(reqReturn);
	//nothing
	reqReturn = lockTable->_RequestEpochLock(tranxID2,
			Stages::Lock::BasicLockTableLockMode::Exclusive);
	EXPECT_TRUE(reqReturn);
	//ex t2
	reqReturn = lockTable->_ReleaseEpochLock(tranxID2);
	EXPECT_TRUE(reqReturn);
	//nothing
	EXPECT_FALSE(lockTable->epochLock->wait);
}

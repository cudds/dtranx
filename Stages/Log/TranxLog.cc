/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <cassert>
#include "TranxLog.h"
#include "Util/Exceptions/NotFoundException.h"
#include "Util/StaticConfig.h"
#include "Util/FileUtil.h"
#include "Service/TranxService.h"
#include "Service/StorageService.h"
#include "Service/ClientService.h"
#include "DaemonStages/Repartition.h"
#include "DaemonStages/TranxAck.h"
#include "DaemonStages/DaemonStagesMock.h"
#include "Server/RecoverLog.h"

namespace DTranx {
namespace Stages {
namespace Log {

TranxLog::TranxLog(std::string fname,
		SharedResources::TranxServerInfo *tranxServerInfo) :
		MidStage(tranxServerInfo, 1, "TranxLog"), fname(fname), curReadName(), bufferString() {
	logOption =
			(tranxServerInfo->GetConfig().read("LogOption") == "Pmem") ?
					PMEM : DISK;
	truncateThreshold = tranxServerInfo->GetConfig().read<uint64>(
			"FileTruncate");
	logWriter = nullptr;
	logReader = nullptr;
	counter = 0;
	totalAvailBytes = 0;
}

TranxLog::~TranxLog() {
	CloseWrite();
	CloseRead();
}

void TranxLog::FileInitWrite(int nextWriteSize) {
	if (logOption == DISK) {
		if (counter >= truncateThreshold) {
			CloseWrite();
		}
	}
	if (logOption == PMEM) {
		if (counter + nextWriteSize > totalAvailBytes) {
			CloseWrite();
		} else {
			counter += nextWriteSize;
		}
	}
	if (logWriter == nullptr) {
		struct timespec now;
		clock_gettime(CLOCK_MONOTONIC_RAW, &now);
		/*
		 * to make sure log names are generated in monotonically increasing order
		 */
		std::string seconds = std::to_string(now.tv_sec);
		uint32_t size = seconds.size();
		for (uint32_t i = size; i < 7; ++i) {
			seconds = "0" + seconds;
		}
		std::string nanoseconds = std::to_string(now.tv_nsec);
		size = nanoseconds.size();
		for (uint32_t i = size; i < 9; ++i) {
			nanoseconds = "0" + nanoseconds;
		}
		std::string newLogName = fname + seconds + nanoseconds;
		logWriter = std::unique_ptr<LogWriter>(
				new LogWriter(newLogName, true, logOption, truncateThreshold));
		counter = 0;
		totalAvailBytes = logWriter->GetTotalAvailBytes();
	}
}

void TranxLog::Prepare(const Util::TranxID& tranxID,
		const google::protobuf::RepeatedPtrField<Service::GItem>& readSet,
		const google::protobuf::RepeatedPtrField<Service::GItem>& writeSet,
		const std::vector<std::string>& ips,
		bool snapshot) {
	logRecord.Clear();
	Service::GTranxType tranxType =
			(snapshot) ?
					Service::GTranxType::SNAPSHOT : Service::GTranxType::NORMAL;
	logRecord.set_tranxtype(tranxType);
	logRecord.set_logtype(GLogType::PREP);
	logRecord.mutable_tranxid()->set_nodeid(tranxID.GetNodeID());
	logRecord.mutable_tranxid()->set_tranxid(tranxID.GetTranxID());
	if (!snapshot) {
		for (auto it = readSet.begin(); it != readSet.end(); ++it) {
			Service::GItem *item = logRecord.add_read_set();
			item->set_key(it->key());
			if (it->has_version()) {
				item->set_version(it->version());
			}
		}

		for (auto it = writeSet.begin(); it != writeSet.end(); ++it) {
			Service::GItem *item = logRecord.add_write_set();
			item->set_key(it->key());
			item->set_value(it->value());
			if (it->has_version()) {
				item->set_version(it->version());
			}
		}

		for (auto it = ips.begin(); it != ips.end(); ++it) {
			logRecord.add_ips(*it);
		}
	}
	logRecord.SerializeToString(&bufferString);
	FileInitWrite(bufferString.size());

	LogStatus status = logWriter->AddRecord(Slice(bufferString));
	assert(status.ok());
	if (logOption == DISK) {
		counter++;
	} else if (logOption == PMEM) {
		counter += bufferString.size();
	}
}

void TranxLog::Prepare(const Util::TranxID& tranxID,
		const std::unordered_map<std::string, std::unordered_set<std::string> >& newMapping,
		std::vector<std::string> allNodes) {
	logRecord.Clear();
	logRecord.set_tranxtype(Service::GTranxType::REPARTITION);
	logRecord.set_logtype(GLogType::PREP);
	logRecord.mutable_tranxid()->set_nodeid(tranxID.GetNodeID());
	logRecord.mutable_tranxid()->set_tranxid(tranxID.GetTranxID());
	for (auto it = newMapping.begin(); it != newMapping.end(); ++it) {
		Service::GMapItem *item = logRecord.add_newmapping();
		item->set_key(it->first);
		for (auto ip = it->second.begin(); ip != it->second.end(); ++ip) {
			item->add_ips(*ip);
		}
	}
	for (auto it = allNodes.begin(); it != allNodes.end(); ++it) {
		logRecord.add_ips(*it);
	}
	logRecord.SerializeToString(&bufferString);
	FileInitWrite(bufferString.size());

	LogStatus status = logWriter->AddRecord(Slice(bufferString));
	assert(status.ok());
	if (logOption == DISK) {
		counter++;
	} else if (logOption == PMEM) {
		counter += bufferString.size();
	}
}

void TranxLog::ParticipantAbort(const Util::TranxID& tranxID) {
	logRecord.Clear();
	logRecord.set_logtype(GLogType::PART_ABORT);
	logRecord.mutable_tranxid()->set_nodeid(tranxID.GetNodeID());
	logRecord.mutable_tranxid()->set_tranxid(tranxID.GetTranxID());
	logRecord.SerializeToString(&bufferString);
	FileInitWrite(bufferString.size());
	LogStatus status = logWriter->AddRecord(Slice(bufferString));
	assert(status.ok());
	if (logOption == DISK) {
		counter++;
	} else if (logOption == PMEM) {
		counter += bufferString.size();
	}
}

void TranxLog::CoordinatorAbort(const Util::TranxID& tranxID) {
	logRecord.Clear();
	logRecord.set_logtype(GLogType::COORD_ABORT);
	logRecord.mutable_tranxid()->set_nodeid(tranxID.GetNodeID());
	logRecord.mutable_tranxid()->set_tranxid(tranxID.GetTranxID());
	logRecord.SerializeToString(&bufferString);
	FileInitWrite(bufferString.size());
	LogStatus status = logWriter->AddRecord(Slice(bufferString));
	assert(status.ok());
	if (logOption == DISK) {
		counter++;
	} else if (logOption == PMEM) {
		counter += bufferString.size();
	}
}

void TranxLog::Ready(const Util::TranxID& tranxID,
		const google::protobuf::RepeatedPtrField<Service::GItem>& readSet,
		const google::protobuf::RepeatedPtrField<Service::GItem>& writeSet,
		const google::protobuf::RepeatedPtrField<std::string>& ips,
		bool snapshot) {
	logRecord.Clear();
	Service::GTranxType tranxType =
			(snapshot) ?
					Service::GTranxType::SNAPSHOT : Service::GTranxType::NORMAL;
	logRecord.set_tranxtype(tranxType);
	logRecord.set_logtype(GLogType::READY);
	logRecord.mutable_tranxid()->set_nodeid(tranxID.GetNodeID());
	logRecord.mutable_tranxid()->set_tranxid(tranxID.GetTranxID());

	for (auto it = readSet.begin(); it != readSet.end(); ++it) {
		Service::GItem *item = logRecord.add_read_set();
		item->set_key(it->key());
		if (it->has_version()) {
			item->set_version(it->version());
		}
	}

	for (auto it = writeSet.begin(); it != writeSet.end(); ++it) {
		Service::GItem *item = logRecord.add_write_set();
		item->set_key(it->key());
		item->set_value(it->value());
		if (it->has_version()) {
			item->set_version(it->version());
		}
	}

	for (auto it = ips.begin(); it != ips.end(); ++it) {
		logRecord.add_ips(*it);
	}
	logRecord.SerializeToString(&bufferString);
	FileInitWrite(bufferString.size());
	LogStatus status = logWriter->AddRecord(Slice(bufferString));
	assert(status.ok());
	if (logOption == DISK) {
		counter++;
	} else if (logOption == PMEM) {
		counter += bufferString.size();
	}
}

void TranxLog::Ready(const Util::TranxID& tranxID,
		const google::protobuf::RepeatedPtrField<Service::GMapItem>& mappingItems,
		std::vector<std::string> allNodes) {
	logRecord.Clear();
	logRecord.set_tranxtype(Service::GTranxType::REPARTITION);
	logRecord.set_logtype(GLogType::READY);
	logRecord.mutable_tranxid()->set_nodeid(tranxID.GetNodeID());
	logRecord.mutable_tranxid()->set_tranxid(tranxID.GetTranxID());
	for (auto it = mappingItems.begin(); it != mappingItems.end(); ++it) {
		Service::GMapItem *item = logRecord.add_newmapping();
		item->set_key(it->key());
		for (auto ip = it->ips().begin(); ip != it->ips().end(); ++ip) {
			item->add_ips(*ip);
		}
	}
	for (auto it = allNodes.begin(); it != allNodes.end(); ++it) {
		logRecord.add_ips(*it);
	}
	logRecord.SerializeToString(&bufferString);
	FileInitWrite(bufferString.size());
	LogStatus status = logWriter->AddRecord(Slice(bufferString));
	assert(status.ok());
	if (logOption == DISK) {
		counter++;
	} else if (logOption == PMEM) {
		counter += bufferString.size();
	}
}

void TranxLog::Commit(const Util::TranxID& tranxID,
		Service::GTranxType tranType, GLogType logType) {
	logRecord.Clear();
	logRecord.set_tranxtype(tranType);
	logRecord.set_logtype(logType);
	logRecord.mutable_tranxid()->set_nodeid(tranxID.GetNodeID());
	logRecord.mutable_tranxid()->set_tranxid(tranxID.GetTranxID());
	logRecord.SerializeToString(&bufferString);
	FileInitWrite(bufferString.size());
	LogStatus status = logWriter->AddRecord(Slice(bufferString));
	assert(status.ok());
	if (logOption == DISK) {
		counter++;
	} else if (logOption == PMEM) {
		counter += bufferString.size();
	}
}

void TranxLog::Commit_1PC(const Util::TranxID& tranxID,
		const google::protobuf::RepeatedPtrField<Service::GItem>& readSet,
		const google::protobuf::RepeatedPtrField<Service::GItem>& writeSet) {
	logRecord.Clear();
	logRecord.set_tranxtype(Service::GTranxType::NORMAL);
	logRecord.set_logtype(GLogType::COMMIT_1PC);
	logRecord.mutable_tranxid()->set_nodeid(tranxID.GetNodeID());
	logRecord.mutable_tranxid()->set_tranxid(tranxID.GetTranxID());

	for (auto it = readSet.begin(); it != readSet.end(); ++it) {
		Service::GItem *item = logRecord.add_read_set();
		item->set_key(it->key());
		if (it->has_version()) {
			item->set_version(it->version());
		}
	}

	for (auto it = writeSet.begin(); it != writeSet.end(); ++it) {
		Service::GItem *item = logRecord.add_write_set();
		item->set_key(it->key());
		item->set_value(it->value());
		if (it->has_version()) {
			item->set_version(it->version());
		}
	}

	logRecord.SerializeToString(&bufferString);
	FileInitWrite(bufferString.size());
	LogStatus status = logWriter->AddRecord(Slice(bufferString));
	assert(status.ok());
	if (logOption == DISK) {
		counter++;
	} else if (logOption == PMEM) {
		counter += bufferString.size();
	}
}

void TranxLog::CoordinatorAck(const Util::TranxID& tranxID,
		Service::GTranxType tranType) {
	logRecord.Clear();
	logRecord.set_tranxtype(tranType);
	logRecord.set_logtype(GLogType::ACK);
	logRecord.mutable_tranxid()->set_nodeid(tranxID.GetNodeID());
	logRecord.mutable_tranxid()->set_tranxid(tranxID.GetTranxID());
	logRecord.SerializeToString(&bufferString);
	FileInitWrite(bufferString.size());
	LogStatus status = logWriter->AddRecord(Slice(bufferString));
	assert(status.ok());
	if (logOption == DISK) {
		counter++;
	} else if (logOption == PMEM) {
		counter += bufferString.size();
	}
}

void TranxLog::FileInitRead() {
	std::set<std::string> files = Util::FileUtil::LsPrefix(fname);
	if (files.empty()) {
		throw Util::NotFoundException("TranxLog::FileInitRead returns false.");
	}
	std::string nextFileName;
	if (curReadName.empty()) {
		nextFileName = *files.begin();
		curReadName = nextFileName;
	} else {
		auto it = files.find(curReadName);
		if (it == files.end() || ++it == files.end()) {
			throw Util::NotFoundException(
					"TranxLog::FileInitRead returns false.");
		}
		nextFileName = *it;
		curReadName = nextFileName;
	}
	logReader = std::unique_ptr<LogReader>(
			new LogReader(nextFileName, true, 0, logOption));
}

GTranxLogRecord TranxLog::ReadNext() {
	if (logReader == nullptr) {
		FileInitRead();
	}
	std::string scratch;
	Slice record;
	if (logReader->ReadRecord(&record, &scratch)) {
		logRecord.Clear();
		logRecord.ParseFromString(record.ToString());
		return logRecord;
	}
	CloseRead();
	FileInitRead();
	if (logReader->ReadRecord(&record, &scratch)) {
		logRecord.Clear();
		logRecord.ParseFromString(record.ToString());
		return logRecord;
	}
	throw Util::NotFoundException("TranxLog::FileInitRead returns false.");
}

GTranxLogRecord TranxLog::OneReadNext() {
	if (logReader == nullptr) {
		logReader = std::unique_ptr<LogReader>(
				new LogReader(fname, true, 0, logOption));
	}
	std::string scratch;
	Slice record;
	if (logReader->ReadRecord(&record, &scratch)) {
		logRecord.Clear();
		logRecord.ParseFromArray(record.data(), record.size());
		return logRecord;
	}
	CloseRead();
	throw Util::NotFoundException("TranxLog::OneReadNext returns false.");
}

void TranxLog::CloseWrite() {
	if (logWriter != nullptr) {
		LogWriter *tmpLogWriter = logWriter.release();
		delete tmpLogWriter;
	}
}

void TranxLog::CloseRead() {
	if (logReader != nullptr) {
		LogReader *tmpLogReader = logReader.release();
		delete tmpLogReader;
	}
}

void TranxLog::GCEntry(std::unordered_map<int, uint64> tranxIDs) {
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC_RAW, &now);
	int adjustTimeZeroes = 9 - std::to_string(now.tv_nsec).size();
	std::string newGCLogName = fname + std::to_string(now.tv_sec);
	while (adjustTimeZeroes != 0) {
		newGCLogName += '0';
		adjustTimeZeroes--;
	}
	newGCLogName += std::to_string(now.tv_nsec);

	LogWriter *logWriterLocal = new LogWriter(newGCLogName, false, logOption,
			truncateThreshold);
	for (auto it = tranxIDs.begin(); it != tranxIDs.end(); ++it) {
		GGCLogRecord logRecord;
		logRecord.set_nodeid(it->first);
		logRecord.set_tranxid(it->second);
		logRecord.SerializeToString(&bufferString);
		LogStatus status = logWriterLocal->AddRecord(Slice(bufferString));
		assert(status.ok());
	}
	delete logWriterLocal;

	std::set<std::string> files = DTranx::Util::FileUtil::LsPrefix(fname);
	if (files.size() > 1) {
		for (auto it = files.begin(); *it != *(files.rbegin()); ++it) {
			//VERBOSE("cleaning gc file  %s", it->c_str());
			Util::FileUtil::RemoveFile(*it);
		}
	}
}

std::unordered_map<int, uint64> TranxLog::GetGCEntry() {
	std::unordered_map<int, uint64> tranxIDs;
	std::set<std::string> files = DTranx::Util::FileUtil::LsPrefix(fname);
	assert(files.size() <= 2);
	if (files.size() == 2) {
		Util::FileUtil::RemoveFile(*files.rbegin());
	} else if (files.size() == 0) {
		return tranxIDs;
	}
	std::string gcLogName = *files.begin();
	LogReader *logReaderLocal = new LogReader(gcLogName, true, 0, logOption);
	std::string scratch;
	Slice record;
	while (true) {
		if (logReaderLocal->ReadRecord(&record, &scratch)) {
			GGCLogRecord logRecord;
			logRecord.ParseFromString(record.ToString());
			tranxIDs[logRecord.nodeid()] = logRecord.tranxid();
		} else {
			break;
		}
	}
	delete logReaderLocal;
	return tranxIDs;
}

void TranxLog::StageThread() {
	NOTICE("TranxLogThread started");
	uint64_t randSeed = 0;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	TranxServiceQueueElement *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
	size_t count = 0;
	while (true) {
		if (terminateThread.load()) {
			NOTICE("TranxLogThread is reclaimed");
			break;
		}
		count = processQueue.try_dequeue_bulk(tmpRPCs,
				TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1) {
				AddQueueLengthSample(0, processQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			TranxServiceQueueElement *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples =
						GetResponseTimeSamplesAndClear(0);
				std::vector<uint32_t> queueLengthSamples =
						GetQueueLengthSamplesAndClear(0);
				std::vector<int> taskLongLatencySamples = GetLongLatency(0);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName);
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end();
						++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(
							*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end();
						++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(
							*queueLengthSample);
				}
				for (auto taskLongLatencySample =
						taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(
							*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				delete element;
				continue;
			}
			//element->goThroughStage("tranxlog");
			TranxLogStageTask task = element->getLogTask();
			const Util::TranxID& tranxID = element->GetTranxID();
			if (task == _ReadyRepartition) {
				const google::protobuf::RepeatedPtrField<Service::GMapItem>& mappingItems =
						element->GetServerRPC()->GetTranxRequest()->tranxprep().request().newmappings();
				Ready(tranxID, mappingItems, tranxServerInfo->GetAllNodes());
			} else if (task == _ParticipantAbort) {
				ParticipantAbort(tranxID);
			} else if (task == _Ready) {
				const Service::GTranxPrep::Request& request =
						element->GetServerRPC()->GetTranxRequest()->tranxprep().request();
				const google::protobuf::RepeatedPtrField<Service::GItem>& readSet =
						request.read_set();
				const google::protobuf::RepeatedPtrField<Service::GItem>& writeSet =
						request.write_set();
				const google::protobuf::RepeatedPtrField<std::string>& ips =
						request.ips();
				Ready(tranxID, readSet, writeSet, ips,
						element->getTranxType()
								== Service::GTranxType::SNAPSHOT);
			} else if (task == _Commit) {
				Commit(tranxID, element->getTranxType(), element->getLogType());
			} else if (task == _Commit1PC) {
				const Service::GClientCommit::Request& request =
						element->GetServerRPC()->GetClientRequest()->clientcommit().request();
				const google::protobuf::RepeatedPtrField<Service::GItem>& readSet =
						request.read_set();
				const google::protobuf::RepeatedPtrField<Service::GItem>& writeSet =
						request.write_set();
				Commit_1PC(tranxID, readSet, writeSet);
			} else if (task == _Prepare) {
				const Service::GClientCommit::Request& request =
						element->GetServerRPC()->GetClientRequest()->clientcommit().request();
				Prepare(tranxID, request.read_set(), request.write_set(),
						element->getIps(),
						element->getTranxType()
								== Service::GTranxType::SNAPSHOT);
			} else if (task == _PrepareRepartition) {
				const std::unordered_map<std::string,
						std::unordered_set<std::string> >& newMapping =
						element->getNewMapping();
				std::vector<std::string> ips = element->getIps();
				Prepare(tranxID, newMapping, ips);
			} else if (task == _CoordinatorAbort) {
				CoordinatorAbort(tranxID);
			} else if (task == _CoordinatorAck) {
				CoordinatorAck(tranxID, element->getTranxType());
			} else {
				assert(false);
			}
			LeaveStage(element, 0, task);
			TranxServiceQueueElement::StageID stageID = element->getStageId();
			if (stageID
					== Stages::TranxServiceQueueElement::StageID::TRANXSERVICE) {
				tranxService->SendToServiceInternal(element);
			} else if (stageID
					== Stages::TranxServiceQueueElement::StageID::CLIENTSERVICE) {
				clientService->SendToServiceInternal(element);
			} else if (stageID
					== Stages::TranxServiceQueueElement::StageID::STORAGESERVICE) {
				storageService->SendToServiceInternal(element);
			} else if (stageID
					== Stages::TranxServiceQueueElement::StageID::TRANXACK) {
				tranxAck->SendToDaemon(element);
			} else if (stageID
					== Stages::TranxServiceQueueElement::StageID::REPARTITION) {
				repartition->SendToDaemon(element);
			} else if (stageID
					== Stages::TranxServiceQueueElement::StageID::RECOVERY) {
				postRecovery->SendToDaemon(element);
			} else if (stageID
					== Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON) {
				daemonStagesMock->SendToDaemon(element);
			} else {
				assert(false);
			}
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_STAGES_LOG_PMEMWRITABLEFILE_H_
#define DTRANX_STAGES_LOG_PMEMWRITABLEFILE_H_

#include <libpmemlog.h>
#include "WritableFile.h"
#include "DTranx/Util/Log.h"

namespace DTranx {
namespace Stages {
namespace Log {

class PmemWritableFile: public WritableFile {
public:
	PmemWritableFile(const std::string& fname, PMEMlogpool *plp)
			: filename_(fname), plp(plp) {

	}
	virtual ~PmemWritableFile() {
		if (plp != NULL) {
			//VERBOSE("pmem closing %s", filename_.c_str());
			pmemlog_close(plp);
		}
	}

	virtual LogStatus Append(const Slice& data);

	virtual LogStatus Close();

	virtual LogStatus Flush();

	virtual LogStatus Sync();

private:
	std::string filename_;
	PMEMlogpool *plp;
};

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_LOG_PMEMWRITABLEFILE_H_ */

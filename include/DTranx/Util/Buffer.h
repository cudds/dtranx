/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary
 * Buffer takes care of pointers and length and provides zero copy or content copy
 */

#ifndef DTRANX_UTIL_BUFFER_H_
#define DTRANX_UTIL_BUFFER_H_

#include "DTranx/Util/types.h"
namespace DTranx {
namespace Util {

class Buffer {
public:
	Buffer();
	Buffer(void *data, uint32 length);
	Buffer(Buffer&& other);
	Buffer& operator=(Buffer&& other);
	virtual ~Buffer() noexcept(false);

	uint32 GetLength() {
		return length;
	}

	void* GetData() {
		return data;
	}

	const void* GetData() const {
		return data;
	}

	void SetData(void *data, uint32 length) {
		this->length = length;
		this->data = data;
	}

private:
	void *data;
	uint32 length;

	/*
	 * Buffer is non-copyable.
	 */
	Buffer(const Buffer&) = delete;
	Buffer& operator=(const Buffer&) = delete;
};

} /* namespace Util */
} /* namespace DTranx */

#endif /* DTRANX_UTIL_BUFFER_H_ */

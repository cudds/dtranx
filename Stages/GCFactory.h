/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * GCFactory collects ack'ed transaction ids and provide earliest tranxID to tranxservice.
 * It's currently used in TranxStates stage and returned to GarbageCollector for gc.
 */

#ifndef DTRANX_STAGES_GCFACTORY_H_
#define DTRANX_STAGES_GCFACTORY_H_

#include <cstdlib>
#include <set>
#include <mutex>
#include <unordered_map>
#include <boost/chrono.hpp>
#include "DTranx/Util/types.h"

namespace DTranx {
namespace Stages {

class GCFactory {
public:
	GCFactory(int nodeID, uint64 tranxBroadcastThreshold, uint64 timeBroadcastThreshold);
	GCFactory(const GCFactory &other);
	virtual ~GCFactory() noexcept(false);

	/* gc tranx for itself
	 * AddTranxID: add a new tranxID to GCFactory
	 * ToBroadcast: check if the tranx's for the local coordinator needs to be broadcasted; automatically update prevBroadcast
	 * GetEarliest: get the earliest for the local coordinator
	 * 		used by recoverLog to initialize nextTranxID
	 * GetEarliest: get the earliest for nodeID
	 * IsOutdated: check if it's garbage collected
	 * UpdateEarliest: update the earliest for the local coordinator
	 * 		used by recoverLog to initialize the earliest from log
	 */
	void AddTranxID(uint64 tranxID);
	bool ToBroadcast();
	uint64 GetEarliest();
	uint64 GetEarliest(int nodeID);
	bool IsOutdated(uint64 tranxID);
	void UpdateEarliest(uint64 tranxID);

	/* gc for both
	 *	IsOutdated: check if it's garbage collected by either other nodes or the local node
	 *	UpdateEarliest: update the earliest for both either other nodes and the local node
	 *		used by recoverLog to initialize GCFactory
	 *		used by TranxGC RPC calls
	 *	GetAllNodeGC: return allNodeGC earliest
	 *		used by GarbageCollector to write the log
	 */
	bool IsOutdated(int nodeID, uint64 tranxID);
	void UpdateEarliest(int nodeID, uint64 tranxID);
	std::unordered_map<int, uint64> GetAllNodeGC();

private:
	uint64 earliest;
	std::set<uint64> discreteSet;
	uint64 prevBroadcast;
	int nodeID;
	/*
	 * record the time the last broadcast happens, to enable broadcast when idle and num of ack tranx not reaching threshold
	 */
	boost::chrono::steady_clock::time_point lastBroadcast;

	/*
	 * gc'ed tranxID in other nodes
	 * key: nodeID, value: tranxID
	 */
	std::unordered_map<int, uint64> otherNodeGC;

	/*
	 * constants
	 */
	uint64 GCTranxThreshold;
	uint64 GCTimeThreshold;
};

} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_GCFACTORY_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * TranxServiceQueueElement managed data being exchanged across stages
 * of tranx service processing. It does not manage the memory de/allocation
 * of the items that it contains.
 */

#ifndef DTRANX_STAGES_TRANXSERVICEQUEUEELEMENT_H_
#define DTRANX_STAGES_TRANXSERVICEQUEUEELEMENT_H_

#include <unordered_set>
#include "RPC/ServerRPC.h"
#include "DTranx/Service/Transaction.pb.h"
#include "DTranx/Stages/Log/Log.pb.h"
#include "GCFactory.h"
#include "StageTasks.h"
#include "RPC/TranxRPC.h"
#include "Util/TranxID.h"

namespace DTranx {
namespace Stages {

class TranxServiceQueueElement {
public:
	TranxServiceQueueElement();
	virtual ~TranxServiceQueueElement();

	/*
	 * Clear is used to clear all members to reuse the memory
	 */
	void Clear();

	enum StageID {
		CLIENTSERVICE = 0,
		TRANXSERVICE = 1,
		STORAGESERVICE = 2,
		TRANXACK = 3,
		GCTHREAD = 4,
		REPARTITION = 5,
		RECOVERY = 6,
		INVALID = 7,
		UNITTESTDAEMON = 8,
	};

	/*
	 * DISPATCH_STAGE_THREADID
	 * INVALID_STAGE_THREADID:
	 * 	1. used for recoverlog
	 * 	2. used for stage itself to return from popSplittedNextInvolvedStageThreads that there's no threads to go through
	 */
	static int INVALID_STAGE_THREADID;
	static int DISPATCH_STAGE_THREADID;
	static boost::chrono::steady_clock::time_point EPOCH_TIMEPOINT;
	static int DEQUEUE_BULK_SIZE;

	RPC::ServerRPC* GetServerRPC() const {
		return serverRPC_;
	}

	void SetServerRPC(RPC::ServerRPC *serverRPC) {
		serverRPC_ = serverRPC;
	}

	Service::GOpCode GetOpcode() const {
		return opcode_;
	}

	void SetOpcode(Service::GOpCode opcode) {
		opcode_ = opcode;
	}

	const Util::TranxID& GetTranxID() const {
		return tranxID_;
	}

	Util::TranxID& GetMutableTranxID() {
		return tranxID_;
	}

	void SetTranxID(Util::TranxID& tranxID) {
		tranxID_ = tranxID;
	}

	Service::GTranxType getTranxType() const {
		return tranxType_;
	}

	void setTranxType(Service::GTranxType tranxType) {
		tranxType_ = tranxType;
	}

	bool isIsPostRecovery() const {
		return isPostRecovery_;
	}

	void setIsPostRecovery(bool isPostRecovery) {
		isPostRecovery_ = isPostRecovery;
	}

	Service::GInquiryTranxStatus getInquiryStatus() const {
		return inquiryStatus_;
	}

	void setInquiryStatus(Service::GInquiryTranxStatus inquiryStatus) {
		inquiryStatus_ = inquiryStatus;
	}

	const std::vector<Stages::Log::GTranxLogRecord>& getTranxLogRecord() const {
		return tranxLogRecord_;
	}

	void setTranxLogRecord(const std::vector<Stages::Log::GTranxLogRecord>& tranxLogRecord) {
		tranxLogRecord_ = tranxLogRecord;
	}

	const State& getState() const {
		return state_;
	}

	void setState(const State& state) {
		state_ = state;
	}

	TranxStatesStageTask getStateTask() const {
		return stateTask_;
	}

	void setStateTask(TranxStatesStageTask stateTask) {
		stateTask_ = stateTask;
	}

	int getStageNum() const {
		return stageNum_;
	}

	void setStageNum(int stageNum) {
		stageNum_ = stageNum;
	}

	const std::unordered_map<std::string, Lock::BasicLockTableLockMode>& getKeysMode() const {
		return keysMode_;
	}

	void setKeysMode(std::unordered_map<std::string, Lock::BasicLockTableLockMode>& keysMode) {
		keysMode_.swap(keysMode);
	}

	bool isLockObtained() const {
		return lockObtained_;
	}

	void setLockObtained(bool lockObtained) {
		lockObtained_ = lockObtained;
	}

	bool isStorageCheckSucc() const {
		return storageCheckSucc_;
	}

	void setStorageCheckSucc(bool storageCheckSucc) {
		storageCheckSucc_ = storageCheckSucc;
	}

	const std::vector<std::string>& getKeys() const {
		return keys_;
	}

	void setKeys(std::vector<std::string>& keys) {
		keys_.swap(keys);
	}

	const std::unordered_map<std::string, std::string>& getWriteSet() const {
		return writeSet_;
	}

	void setWriteSet(std::unordered_map<std::string, std::string>& writeSet) {
		writeSet_.swap(writeSet);
	}

	Lock::WriteBlockLockTableStageTask getLockTask() const {
		return lockTask_;
	}

	void setLockTask(Lock::WriteBlockLockTableStageTask lockTask) {
		lockTask_ = lockTask;
	}

	Log::TranxLogStageTask getLogTask() const {
		return logTask_;
	}

	void setLogTask(Log::TranxLogStageTask logTask) {
		logTask_ = logTask;
	}

	const std::unordered_map<std::string, uint64>& getReadSet() const {
		return readSet_;
	}

	void setReadSet(std::unordered_map<std::string, uint64>& readSet) {
		readSet_.swap(readSet);
	}

	StorageStageTask getStorageTask() const {
		return storageTask_;
	}

	void setStorageTask(StorageStageTask storageTask) {
		storageTask_ = storageTask;
	}

	std::unordered_map<std::string,
			std::pair<std::vector<Service::GItem>, std::vector<Service::GItem> > >& getItems() {
		return items_;
	}

	void setItems(const std::unordered_map<std::string,
			std::pair<std::vector<Service::GItem>, std::vector<Service::GItem> > >& items) {
		items_ = items;
	}

	const std::unordered_map<std::string, std::unordered_set<std::string> >& getWriteItemMapping() const {
		return writeItemMapping;
	}

	void setWriteItemMapping(const std::unordered_map<std::string, std::unordered_set<std::string> >& writeItemMapping) {
		this->writeItemMapping = writeItemMapping;
	}

	const std::vector<std::string>& getIps() const {
		return ips_;
	}

	void setIps(const std::vector<std::string>& ips) {
		ips_ = ips;
	}

	Stages::Log::GLogType getLogType() const {
		return logType_;
	}

	void setLogType(Stages::Log::GLogType logType) {
		logType_ = logType;
	}

	bool isAckCommitAbort() const {
		return ackCommitAbort_;
	}

	void setAckCommitAbort(bool ackCommitAbort) {
		ackCommitAbort_ = ackCommitAbort;
	}

	bool isStateAckCommitAbort() const {
		return stateAckCommitAbort_;
	}

	void setStateAckCommitAbort(bool stateAckCommitAbort) {
		stateAckCommitAbort_ = stateAckCommitAbort;
	}

	uint64 getStorageEpoch() const {
		return storageEpoch_;
	}

	void setStorageEpoch(uint64 storageEpoch) {
		storageEpoch_ = storageEpoch;
	}

	bool isStorageSnapshotExist() const {
		return storageSnapshotExist_;
	}

	void setStorageSnapshotExist(bool storageSnapshotExist) {
		storageSnapshotExist_ = storageSnapshotExist;
	}

	const std::string& getStorageValue() const {
		return storageValue_;
	}

	void setStorageValue(const std::string& storageValue) {
		storageValue_ = storageValue;
	}

	bool isStorageKeyExist() const {
		return storageKeyExist_;
	}

	void setStorageKeyExist(bool storageKeyExist) {
		storageKeyExist_ = storageKeyExist;
	}

	StageID getStageId() const {
		return stageID_;
	}

	void setStageId(StageID stageId) {
		stageID_ = stageId;
	}

	TranxRPCHelperStageTask getTranxRpcTask() const {
		return tranxRPCTask_;
	}

	void setTranxRpcTask(TranxRPCHelperStageTask tranxRpcTask) {
		tranxRPCTask_ = tranxRpcTask;
	}

	Service::GInquiryTranxStatus getTranxRpcInquiryResult() const {
		return tranxRPCInquiryResult_;
	}

	void setTranxRpcInquiryResult(Service::GInquiryTranxStatus tranxRpcInquiryResult) {
		tranxRPCInquiryResult_ = tranxRpcInquiryResult;
	}

	TranxRPCHelperPollingResult getTranxRpcResult() const {
		return tranxRPCResult_;
	}

	void setTranxRpcResult(TranxRPCHelperPollingResult tranxRpcResult) {
		tranxRPCResult_ = tranxRpcResult;
	}

	const Stages::GCFactory& getStateGcFactory() const {
		return stateGCFactory_;
	}

	void setStateGcFactory(const Stages::GCFactory& stateGcFactory) {
		stateGCFactory_ = stateGcFactory;
	}

	bool isStateToBroadcast() const {
		return stateToBroadcast_;
	}

	void setStateToBroadcast(bool stateToBroadcast) {
		stateToBroadcast_ = stateToBroadcast;
	}

	const std::unordered_map<std::string, std::unordered_set<std::string> >& getNewMapping() const {
		return newMapping_;
	}

	void setNewMapping(const std::unordered_map<std::string, std::unordered_set<std::string> >& newMapping) {
		newMapping_ = newMapping;
	}

	const std::vector<std::string>& getStorageKeys() const {
		return storageKeys_;
	}

	void setStorageKeys(const std::vector<std::string>& storageKeys) {
		storageKeys_ = storageKeys;
	}

	const std::vector<std::vector<std::pair<std::string, std::string> > >& getStorageSearchAllResults() const {
		return storageSearchAllResults_;
	}

	void setStorageSearchAllResults(const std::vector<
			std::vector<std::pair<std::string, std::string> > >& storageSearchAllResults) {
		storageSearchAllResults_ = storageSearchAllResults;
	}

	void addStorageSearchAllResults(const std::vector<
			std::vector<std::pair<std::string, std::string> > >& storageSearchAllResults) {
		storageSearchAllResults_.insert(storageSearchAllResults_.end(),
				storageSearchAllResults.begin(), storageSearchAllResults.end());
	}

	void clearStorageSearchAllResults() {
		storageSearchAllResults_.clear();
	}

	StageID getOldStageId() const {
		return oldStageID_;
	}

	void setOldStageId(StageID oldStageId) {
		oldStageID_ = oldStageId;
	}

	int getOldStageNum() const {
		return oldStageNum_;
	}

	void setOldStageNum(int oldStageNum) {
		oldStageNum_ = oldStageNum;
	}

	const std::string& getLockStatus() const {
		return lockStatus_;
	}

	void setLockStatus(const std::string& lockStatus) {
		lockStatus_ = lockStatus;
	}

	void addLockStatus(const std::string& lockStatus) {
		lockStatus_ += lockStatus;
	}

	bool isTerminateInThisStage() const {
		return terminateInThisStage_;
	}

	void setTerminateInThisStage(bool terminateInThisStage) {
		terminateInThisStage_ = terminateInThisStage;
	}

	uint64 getStorageVersion() const {
		return storageVersion_;
	}

	void setStorageVersion(uint64 storageVersion) {
		storageVersion_ = storageVersion;
	}

	int getTranxRpcNumOfPeerStillWaiting() const {
		return tranxRPCNumOfPeerStillWaiting_;
	}

	int decAndGetTranxRpcNumOfPeerStillWaiting() {
		return --tranxRPCNumOfPeerStillWaiting_;
	}

	void setTranxRpcNumOfPeerStillWaiting(int tranxRPCNumOfPeerStillWaiting) {
		tranxRPCNumOfPeerStillWaiting_ = tranxRPCNumOfPeerStillWaiting;
	}

	boost::chrono::steady_clock::time_point getTranxRpcStartTime() const {
		return tranxRPCStartTime_;
	}

	void setTranxRpcStartTime(boost::chrono::steady_clock::time_point tranxRpcStartTime) {
		tranxRPCStartTime_ = tranxRpcStartTime;
	}

	void addTranxRPCs(RPC::TranxRPC* tranxRPC) {
		tranxRPCs_.insert(tranxRPC);
	}

	bool checkInTranxRPCs(RPC::TranxRPC* tranxRPC) {
		return tranxRPCs_.find(tranxRPC) != tranxRPCs_.end();
	}

	void clearTranxRPCs() {
		tranxRPCs_.clear();
	}

	const std::unordered_set<RPC::TranxRPC*>& getTranxRpCs() const {
		return tranxRPCs_;
	}

	void goThroughStage(std::string stageName) {
		passedStages_.push_back(stageName);
	}

	std::vector<std::string> getPassedStages() {
		return passedStages_;
	}

	void PrintElement();

	const std::set<int>& getSplittedInvolvedStageThreads() const {
		return splittedInvolvedStageThreads_;
	}

	void setSplittedInvolvedStageThreads(const std::set<int>& storageInvolvedStageThreads) {
		splittedInvolvedStageThreads_ = storageInvolvedStageThreads;
	}

	void clearSplittedInvolvedStageThreads() {
		splittedInvolvedStageThreads_.clear();
	}

	int popSplittedNextInvolvedStageThreads() {
		if (splittedInvolvedStageThreads_.empty()) {
			return INVALID_STAGE_THREADID;
		}
		int nextStageThread = *splittedInvolvedStageThreads_.begin();
		splittedInvolvedStageThreads_.erase(splittedInvolvedStageThreads_.begin());
		return nextStageThread;
	}

	const std::unordered_set<std::string>& getLockAlreadyLockedKeys() const {
		return lockAlreadyLockedKeys_;
	}

	void setLockAlreadyLockedKeys(const std::unordered_set<std::string>& lockAlreadyLockedKeys) {
		lockAlreadyLockedKeys_ = lockAlreadyLockedKeys;
	}

	void addLockAlreadyLockedKeys(const std::string& key) {
		lockAlreadyLockedKeys_.insert(key);
	}

	bool isLockAlreadyLockedKeys(const std::string& key) {
		return lockAlreadyLockedKeys_.find(key) != lockAlreadyLockedKeys_.end();
	}

	bool isLockEpochAlreadyLocked() const {
		return lockEpochAlreadyLocked_;
	}

	void setLockEpochAlreadyLocked(bool lockEpochAlreadyLocked) {
		lockEpochAlreadyLocked_ = lockEpochAlreadyLocked;
	}

	boost::chrono::steady_clock::time_point getLockStartTimePoint() const {
		return lockStartTimePoint_;
	}

	void setLockStartTimePoint(boost::chrono::steady_clock::time_point lockStartTimePoint) {
		lockStartTimePoint_ = lockStartTimePoint;
	}

	void clearLockInternalStates() {
		lockStartTimePoint_ = EPOCH_TIMEPOINT;
		lockEpochAlreadyLocked_ = false;
		lockAlreadyLockedKeys_.clear();
	}

	boost::chrono::steady_clock::time_point getStageEnteringTime() const {
		return stageEnteringTime_;
	}

	void setStageEnteringTime(boost::chrono::steady_clock::time_point stageEnteringTime) {
		stageEnteringTime_ = stageEnteringTime;
	}

	bool isIsPerfOutPutTask() const {
		return isPerfOutPutTask_;
	}

	void setIsPerfOutPutTask(bool isPerfOutPutTask) {
		this->isPerfOutPutTask_ = isPerfOutPutTask;
	}

private:
	RPC::ServerRPC *serverRPC_;
	/*
	 * tranxType_ only exist for ClientCommit, TranxPrepare and TranxCommit requests
	 *
	 * stageID remembers which service/stage pushes the request to the next stage so that
	 * 	it's able to push back to the previous stage queue
	 * stageNum_ remembers the current stage at the TranxService, which is understood by Tranx Internal Service
	 *
	 * oldStageID_/oldStageNum_ are used when later staged pipeline needs to be returned to an intermediate
	 * stage and at a certain point, it returns to the original stage originator. Stage originator are Service
	 * stages, TranxAck, GC, Repartition.
	 * It's used by the Tranx Service to assign migrate request to Repartition thread.
	 *
	 * terminateInThisStage_ is used to forcefully terminate a stage at some midstage
	 */
	Service::GOpCode opcode_;
	Util::TranxID tranxID_;
	Service::GTranxType tranxType_;
	StageID stageID_;
	int stageNum_;

	StageID oldStageID_;
	int oldStageNum_;

	bool terminateInThisStage_;

	/*
	 * items/ips/writeItemMapping only exist for ClientCommit; newMapping_ only exist for Repartition, ips exist in Repartition as allNodes
	 * ips_ contains all node ips that are involved for new transactions that starts here
	 * writeItemMapping contains the mapping data for write items in order to do read check after lock acquisition
	 */
	std::unordered_map<std::string,
			std::pair<std::vector<Service::GItem>, std::vector<Service::GItem>> > items_;
	std::unordered_map<std::string, std::unordered_set<std::string>> writeItemMapping;
	std::vector<std::string> ips_;
	std::unordered_map<std::string, std::unordered_set<std::string> > newMapping_;

	/***********************************************************************************************
	 * Tasks and return data
	 * TranxState, LockTable, Storage, TranxLog
	 * TranxPeer, TranxAck, TranxRPCHelper
	 */

	/*
	 * stateGCFactor_ and stateToBroadcast_ are returned for GarbageCollector stage
	 */
	State state_;
	bool stateAckCommitAbort_;
	GCFactory stateGCFactory_;
	bool stateToBroadcast_;
	TranxStatesStageTask stateTask_;

	/*
	 * LockTable related
	 * 	request params: lockTask_, keysMode_, keys_(in participants where curTranxs stores the keys)
	 * 	returned: lockObtained_, lockStatus_(used for special tranx ShowLock)
	 * 	others:lockStartTimePoint_, lockAlreadyLockedKeys_, lockEpochAlreadyLocked_ used for internal
	 * 		blocked timeout to avoid relock cost
	 */
	std::unordered_map<std::string, Lock::BasicLockTableLockMode> keysMode_;
	std::vector<std::string> keys_;
	bool lockObtained_;
	std::string lockStatus_;
	Lock::WriteBlockLockTableStageTask lockTask_;

	boost::chrono::steady_clock::time_point lockStartTimePoint_;
	std::unordered_set<std::string> lockAlreadyLockedKeys_;
	bool lockEpochAlreadyLocked_;

	/*
	 * storageKeys_: searchall task, delete task(repartition stage)
	 *
	 * readSet_: tranxprep check read
	 *
	 * (readwithversion uses serverRPC from storage read request)
	 *
	 * storageSnapshotExist_, storageKeyExist_, storageEpoch_, storageValue_, storageVersion_ are used
	 * 	by readwithversion and readsnapshot tasks
	 *
	 * writeset_: used by the writeplain, write tasks
	 *
	 * storageInvolvedStageThreads is filled by Storage Dispatch thread to show which threads will be involved
	 * 	for the current storage task
	 */
	std::unordered_map<std::string, std::string> writeSet_;
	std::unordered_map<std::string, uint64> readSet_;
	std::vector<std::string> storageKeys_;
	bool storageCheckSucc_;
	bool storageSnapshotExist_;
	bool storageKeyExist_;
	uint64 storageEpoch_;
	std::string storageValue_;
	uint64 storageVersion_;
	std::vector<std::vector<std::pair<std::string, std::string>>>storageSearchAllResults_;

	StorageStageTask storageTask_;

	/*
	 * TranxLog params
	 */
	Log::GLogType logType_;
	Log::TranxLogStageTask logTask_;

	/*
	 * tranxRPCResult_/tranxRPCInquiryResult_ is the result from rpc calling
	 *
	 * tranxRPCStartTime_/numOfPeerStillWaiting_ are used in TranxRPCInternalThread
	 *
	 * tranxRPCs_ is used to map the tranxRPC from TranxPeer back to the queue element
	 */
	TranxRPCHelperPollingResult tranxRPCResult_;
	Service::GInquiryTranxStatus tranxRPCInquiryResult_;
	boost::chrono::steady_clock::time_point tranxRPCStartTime_;
	int tranxRPCNumOfPeerStillWaiting_;
	std::unordered_set<RPC::TranxRPC *> tranxRPCs_;
	TranxRPCHelperStageTask tranxRPCTask_;

	bool ackCommitAbort_;

	/*
	 * isPostRecovery denotes whether it's recovering from previous failure
	 * there are two situations:
	 * 	(1) when it's ready in the tranxlog, tranxinquire will lead to whether commit/abort
	 * 	(2) recovery might lead to some transactions to be ack'ed
	 */
	bool isPostRecovery_;
	Service::GInquiryTranxStatus inquiryStatus_;
	std::vector<Stages::Log::GTranxLogRecord> tranxLogRecord_;

	/*
	 * splittedInvolvedStageThreads_ is used by the splitted stages to navigate through different
	 * 	stage threads, basically it stores a list of index that points to the relevant threads
	 */
	std::set<int> splittedInvolvedStageThreads_;

	/*
	 * Debug: storing intermediate stage names
	 * tranxstates, tranxrpchelper, tranxlog, storage, tranxlock
	 * tranxack, repartition, GC
	 * clientservice, storageservice, tranxservice
	 *
	 */
	std::vector<std::string> passedStages_;

	/*
	 * Performance test
	 */
	boost::chrono::steady_clock::time_point stageEnteringTime_;
	bool isPerfOutPutTask_;

	/*
	 * TranxServiceQueueElement is non copyable
	 */
	TranxServiceQueueElement(const TranxServiceQueueElement&) = delete;
	TranxServiceQueueElement& operator=(const TranxServiceQueueElement&) = delete;

};
}
/* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_TRANXSERVICEQUEUEELEMENT_H_ */

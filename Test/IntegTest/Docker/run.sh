#!/bin/bash 
# run this script to generate docker images
cp /usr/local/lib/libprotobuf.so.10 /usr/local/lib/libprotobuf.so.10.0.0 \
 /usr/local/lib/libzmq.so.4  /usr/local/lib/libzmq.so.4.0.0 \
 /usr/local/lib/libpmem.so.1 /usr/local/lib/libpmem.so.1.0.0 \
 /usr/local/lib/libpmemlog.so /usr/local/lib/libpmemlog.so.1 /usr/local/lib/libpmemlog.so.1.0.0 \
 /usr/local/lib/libtcmalloc.so.4  /usr/local/lib/libtcmalloc.so.4.3.0 \
 /usr/local/lib/libboost_thread.so /usr/local/lib/libboost_thread.so.1.53.0 \
 /usr/local/lib/libboost_chrono.so /usr/local/lib/libboost_chrono.so.1.53.0 \
 /usr/local/lib/libboost_system.so /usr/local/lib/libboost_system.so.1.53.0 \
 /usr/local/lib/libmetis.so /usr/local/lib/liblogcabin.so /usr/local/lib/libleveldb.so.1 \
 /usr/local/lib/libprofiler.so /usr/local/lib/libprofiler.so.0.4.8 ./;
sudo docker build -t dtranx:4.0 .;
rm libprotobuf.so.10 libprotobuf.so.10.0.0 \
 libzmq.so.4  libzmq.so.4.0.0 \
 libpmem.so.1 libpmem.so.1.0.0 \
 libpmemlog.so libpmemlog.so.1 libpmemlog.so.1.0.0 \
 libtcmalloc.so.4  libtcmalloc.so.4.3.0 \
 libboost_thread.so libboost_thread.so.1.53.0 \
 libboost_chrono.so libboost_chrono.so.1.53.0 \
 libboost_system.so libboost_system.so.1.53.0 \
 libmetis.so liblogcabin.so libleveldb.so.1 \
 libprofiler.so libprofiler.so.0.4.8;
#!/bin/bash
# tests for retry RPC calls, prepare will fail because after timeout, it assumes remote node failure
# while commit/clientcommit won't affect the commit results and TranxAck and Client will retry
# after timeout

# if some tests still fails, check out the sleep time, increase it maybe
function test2(){
	# test single client with hang time
	RestartDtranx dtranx1 hang prepare
	RestartDtranx dtranx2 hang prepare
	RestartDtranx dtranx3 hang prepare
	sudo docker exec dtranx1 bash -c '/dtranx/SimpleTranx -p Workloads/readall &> /dtranx/test2_prepare'
	sudo docker cp dtranx1:/dtranx/test2_prepare ./
	sudo docker exec dtranx1 bash -c 'rm /dtranx/test2_prepare'
	
	sleep 5
	RestartDtranx dtranx1 hang commit
	RestartDtranx dtranx2 hang commit
	RestartDtranx dtranx3 hang commit
	sudo docker exec dtranx1 bash -c '/dtranx/SimpleTranx -p Workloads/writedistributed &> /dtranx/test2_commit'
	sudo docker cp dtranx1:/dtranx/test2_commit ./
	sudo docker exec dtranx1 bash -c 'rm /dtranx/test2_commit'

	sleep 5
	RestartDtranx dtranx1 hang clientcommit
	RestartDtranx dtranx2 hang clientcommit
	RestartDtranx dtranx3 hang clientcommit
	# waiting for the previous ack to complete or else the next transaction will fail 
	# because locks are not released yet
	sleep 10
	sudo docker exec dtranx1 bash -c '/dtranx/SimpleTranx -p Workloads/writedistributed &> /dtranx/test2_clientcommit'
	sudo docker cp dtranx1:/dtranx/test2_clientcommit ./
	sudo docker exec dtranx1 bash -c 'rm /dtranx/test2_clientcommit'

	RestartDtranx dtranx1
	RestartDtranx dtranx2
	RestartDtranx dtranx3
	
	# wait so that dtranx2 will not get the commit request in the following tests
	sleep 15
}
/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * currently used in integration test to create snapshot intentionally
 */

#include <memory>
#include "DTranx/Client/Client.h"
#include "DTranx/Util/ConfigHelper.h"
#include "RPC/ClientRPC.h"
#include "DTranx/Service/Transaction.pb.h"
#include "Util/StringUtil.h"
using namespace DTranx;

int main(int argc, char **argv) {
	Util::Log::setLogPolicy( { { "Client", "VERBOSE" }, { "RPC", "VERBOSE" } });
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(10);

	if (argc < 3) {
		std::cout
				<< "please input any server address as the first parameter, self address as the second parameter"
				<< std::endl;
		return 0;
	}

	Client::Client* client = new Client::Client(argv[1], 30000, context);
	std::string selfAddressStr(argv[2]);
	uint32_t selfAddress[4];
	std::vector<std::string> tmpFields = Util::StringUtil::Split(selfAddressStr, '.');
	assert(tmpFields.size() == 4);
	for (int i = 0; i < 4; ++i) {
		selfAddress[i] = std::stoul(tmpFields[i], nullptr, 10);
	}
	assert(client->Bind(selfAddressStr, 30030));
	RPC::ClientRPC *clientRPC = new RPC::ClientRPC(RPC::GServiceID::ClientService, client);
	clientRPC->SetClientOpcode(Service::GOpCode::CLIENT_COMMIT);
	clientRPC->SetReqIP(selfAddress);
	clientRPC->SetReqPort(30030);
	DTranx::Service::GClientCommit::Request *request =
			clientRPC->GetClientRequest()->mutable_clientcommit()->mutable_request();
	request->set_tranxtype(Service::GTranxType::SNAPSHOT);
	client->CallRPC(clientRPC);
	assert(client->BlockPoll(clientRPC));
	DTranx::Service::GClientCommit::Response *response =
			clientRPC->GetClientResponse()->mutable_clientcommit()->mutable_response();
	if (DTranx::Service::GStatus::OK == response->status()) {
		std::cout << "creation successful" << std::endl;
	} else {
		std::cout << "creation failure" << std::endl;
	}
	delete client;
	delete clientRPC;
}


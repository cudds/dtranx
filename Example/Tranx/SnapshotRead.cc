/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <getopt.h>
#include <fstream>
#include "DTranx/Client/ClientTranx.h"
#include "DTranx/Util/ConfigHelper.h"
#include "DTranx/Util/Log.h"
#include "Util/StringUtil.h"
using namespace DTranx;

/**
 * Parses argv for the main function.
 */
class OptionParser {
public:
	OptionParser(int& argc, char**& argv)
			: argc(argc), argv(argv), clusterFile() {
		profile = "";
		while (true) {
			static struct option longOptions[] = { { "profile",
			required_argument, NULL, 'p' }, { "cluster",
			required_argument, NULL, 'c' }, { "help", no_argument, NULL, 'h' }, { 0, 0, 0, 0 } };
			int c = getopt_long(argc, argv, "p:c:h", longOptions, NULL);

			// Detect the end of the options.
			if (c == -1)
				break;

			switch (c) {
				case 'p':
					profile = optarg;
					break;
				case 'c':
					clusterFile = optarg;
					break;
				case 'h':
					usage();
					exit(0);
				case '?':
				default:
					// getopt_long already printed an error message.
					usage();
					exit(1);
			}
		}
	}

	void usage() {
		std::cout << "Usage: " << argv[0] << " [options] <servers>" << std::endl;
		std::cout << "Options: " << std::endl;
		std::cout << "  -p, --profile (filename) " << "dtranx profile, line format r/w,key,value"
				<< "(default: one node transaction)" << std::endl;
		std::cout << "  -c, --cluster (filename) " << "dtranx cluster, line format ip"
				<< "(optional argument, default localhost)" << std::endl;
		std::cout << "  -h, --help              " << "Print this usage information" << std::endl;
	}

	int& argc;
	char**& argv;
	std::string profile;
	std::string clusterFile;
};

int main(int argc, char** argv) {
	OptionParser options(argc, argv);
	Util::ConfigHelper configHelper;
	Util::Log::setLogPolicy( { { "Client", "VERBOSE" }, { "RPC", "VERBOSE" } });
	configHelper.readFile("DTranx.conf");
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(
			configHelper.read<uint32>("maxIOThreads"));
	std::vector<std::string> ips;
	if (options.clusterFile.empty()) {
		ips.push_back(configHelper.read("SelfAddress"));
	} else {
		std::ifstream ipFile(options.clusterFile);
		if (!ipFile.is_open()) {
			VERBOSE("cannot open %s", argv[1]);
			exit(0);
		}
		std::string ip;
		while (std::getline(ipFile, ip)) {
			ips.push_back(ip);
		}
	}
	Client::ClientTranx *clientTranx = new DTranx::Client::ClientTranx(
			std::stoul(configHelper.read("routerFrontPort").c_str(), nullptr, 10), context, ips,
			configHelper.read("SelfAddress"),
			std::stoul(configHelper.read("clientReceivePort").c_str(), nullptr, 10));

	if (options.profile.empty()) {
		std::string key, value;
		key = "key4";
		Service::GStatus status = clientTranx->Read(key, value);
		if (status == Service::GStatus::OK) {
			std::cout << "read key: " << key << " and the value is " << value << std::endl;
		}
		clientTranx->Clear();
	} else {
		std::ifstream ipFile(options.profile);
		if (!ipFile.is_open()) {
			VERBOSE("cannot open %s", options.profile.c_str());
			exit(0);
		}
		std::string line;
		bool toCommit = true;
		while (std::getline(ipFile, line)) {
			std::vector<std::string> fields = Util::StringUtil::Split(line, ',');
			assert(fields.size() >= 2);
			if (fields[0] == "r") {
				std::string key, value;
				key = fields[1];
				Service::GStatus status = clientTranx->Read(key, value, true);
				if (status == Service::GStatus::OK) {
					std::cout << "read key: " << key << " and the value is " << value << std::endl;
				} else if (status == Service::GStatus::KEY_NOT_FOUND) {
					std::cout << "read failure, key not found..." << std::endl;
					toCommit = false;
				} else {
					std::cout << "read failure, snapshot not created yet..." << std::endl;
					toCommit = false;
				}
			} else if (fields[0] == "w") {
				std::cout << "no writes for snapshot reading, exiting" << std::endl;
				return 0;
			} else {
				assert(false);
			}
		}
		if (toCommit) {
			bool result = clientTranx->Commit();
			if (result) {
				std::cout << "commit success" << std::endl;
			} else {
				std::cout << "commit failure" << std::endl;
			}
		}
		clientTranx->Clear();
	}
	delete clientTranx;
	return 0;
}

#include <boost/thread.hpp>
#include <mutex>
#include <memory>
#include <queue>

#include "zmsg.hpp"

#define reqPerThread 2500000
#define numOfThreads 2
void sender(zmq::socket_t* socket) {
	std::string data;
	for (int i = 0; i < 120; i++) {
		data += 'a';
	}
	std::cout << data.size() << " bytes per message" << std::endl;
	int count = 0;
	while (count < reqPerThread) {
		count++;
		zmsg zm;
		zm.push_back(data.c_str());
		zm.send(*socket);
		if (count % 10000 == 0) {
			std::cout << "sender " << count << std::endl;
		}

	}
}

int main() {
	s_version();
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(200);
	std::vector<zmq::socket_t> sockets;

	for (int i = 0; i < numOfThreads; ++i) {
		sockets.push_back(zmq::socket_t(*context, ZMQ_DEALER));
		int lingtime = 0;
		sockets.back().setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
		int hwm = 0;
		size_t len = sizeof(hwm);
		sockets.back().setsockopt(ZMQ_SNDHWM, &hwm, len);
		if (i % 2 == 0) {
			sockets.back().connect(std::string("tcp://128.104.222.74:2222").c_str());
		} else {
			sockets.back().connect(std::string("tcp://128.104.222.74:2223").c_str());
		}

	}

	std::vector<boost::thread> sendingThreads;
	std::vector<boost::thread> receivingThreads;

	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();

	for (int i = 0; i < numOfThreads; ++i) {
		sendingThreads.push_back(boost::thread(sender, &sockets[i]));
	}

	for (int i = 0; i < numOfThreads; ++i) {
		sendingThreads[i].join();
		std::cout << "sender joined" << std::endl;
	}
	sleep(30);

	for (int i = 0; i < numOfThreads; ++i) {
		sockets[i].close();
	}
	context->close();

	std::chrono::system_clock::time_point end = std::chrono::system_clock::now();

	int timePassed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

	std::cout << "throughout: " << 1.0 * reqPerThread * numOfThreads * 1000 / timePassed
			<< std::endl;
	return 0;
}

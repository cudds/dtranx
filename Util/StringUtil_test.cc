/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "StringUtil.h"

TEST(StringUtil, StartsWith) {
	EXPECT_TRUE(DTranx::Util::StringUtil::StartsWith("hello", "hell"));
	EXPECT_FALSE(DTranx::Util::StringUtil::StartsWith("hello", "world"));
}

TEST(StringUtil, EndsWith) {
	EXPECT_TRUE(DTranx::Util::StringUtil::EndsWith("hello", "llo"));
	EXPECT_FALSE(DTranx::Util::StringUtil::EndsWith("hello", "world"));
}

TEST(StringUtil, Format) {
	int num = 100;
	std::string str = "percent";
	EXPECT_EQ("100percent", DTranx::Util::StringUtil::format("%d%s", num, str.c_str()));
}

TEST(StringUtil, Split) {
	std::string srcStr = "1#12#123";
	char delimiter = '#';
	std::vector<std::string> desStrs = DTranx::Util::StringUtil::Split(srcStr, delimiter);
	EXPECT_EQ(3, desStrs.size());
	EXPECT_EQ("12", desStrs[1]);
	srcStr = "1#12#123#";
	desStrs = DTranx::Util::StringUtil::Split(srcStr, delimiter);
	EXPECT_EQ(3, desStrs.size());
	EXPECT_EQ("123", desStrs[2]);
	srcStr = "";
	desStrs = DTranx::Util::StringUtil::Split(srcStr, delimiter);
	EXPECT_EQ(0, desStrs.size());
	srcStr = "123#";
	desStrs = DTranx::Util::StringUtil::Split(srcStr, delimiter);
	EXPECT_EQ(1, desStrs.size());
	srcStr = "123";
	desStrs = DTranx::Util::StringUtil::Split(srcStr, delimiter);
	EXPECT_EQ(1, desStrs.size());
}

TEST(StringUtil, Concat) {
	std::vector<std::string> strs;
	strs.push_back("2");
	strs.push_back("1");
	strs.push_back("0");
	EXPECT_EQ("2#1#0", DTranx::Util::StringUtil::Concat(strs, '#'));
	strs.clear();
	strs.push_back("1");
	EXPECT_EQ("1", DTranx::Util::StringUtil::Concat(strs, '#'));
}

TEST(StringUtil, UstringToString) {
	zmsg::ustring srcStr = (const unsigned char*) "123";
	srcStr[0] = '\0';
	std::string destStr = DTranx::Util::StringUtil::UstringToString(srcStr);
	EXPECT_EQ(3, destStr.size());
}

TEST(StringUtil, SDBMHash) {
	EXPECT_EQ(3480279571628963890, DTranx::Util::StringUtil::SDBMHash("adweqr"));
}

TEST(StringUtil, GetTranxID) {
	EXPECT_EQ(23, DTranx::Util::StringUtil::GetTranxID("1#23"));
}

TEST(StringUtil, GetNodeID) {
	EXPECT_EQ("1", DTranx::Util::StringUtil::GetNodeID("1#23"));
}

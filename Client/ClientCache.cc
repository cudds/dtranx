/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <cstdlib>
#include <cassert>
#include "DTranx/Client/ClientCache.h"
#include "Util/Exceptions/NotFoundException.h"
#include "Util/StaticConfig.h"
#include "DTranx/Util/Log.h"
#include "Util/StringUtil.h"
namespace DTranx {
namespace Client {

const uint64 ClientCache::NO_SUCH_DATA = -1;
const std::string ClientCache::NO_SUCH_MAP = "empty";

ClientCache::ClientCache()
		: threadSafe(false), randSeed(0) {
}

ClientCache::~ClientCache() noexcept(false) {
}

std::string ClientCache::GetMapping(const std::string& key) {
	if (!threadSafe) {
		mutex.lock();
	}
	if (mapping.find(key) == mapping.end()) {
		if (!threadSafe) {
			mutex.unlock();
		}
		return NO_SUCH_MAP;
	}
	std::unordered_set<std::string> ips = mapping[key];
	int index = randSeed % ips.size();
	randSeed++;
	if (!threadSafe) {
		mutex.unlock();
	}
	auto it = ips.begin();
	std::advance(it, index);
	return *it;
}

std::unordered_set<std::string> ClientCache::GetAllMapping(const std::string& key) {
	std::unordered_set<std::string> ipList;
	if (!threadSafe) {
		mutex.lock();
	}
	if (mapping.find(key) == mapping.end()) {
		if (!threadSafe) {
			mutex.unlock();
		}
		return ipList;
	}
	ipList = mapping[key];
	if (!threadSafe) {
		mutex.unlock();
	}
	return ipList;
}

void ClientCache::UpdateMapping(const std::string& key,
		const std::unordered_set<std::string>& newips) {
	if (!threadSafe) {
		mutex.lock();
	}
	//VERBOSE("update local mapping cache for %s", key.c_str());
	mapping[key] = newips;
	if (!threadSafe) {
		mutex.unlock();
	}
}

std::string ClientCache::ReadLocal(const std::string& key, uint64& version) {
	if (!threadSafe) {
		mutex.lock();
	}
	if (data.find(key) == data.end()) {
		version = NO_SUCH_DATA;
		if (!threadSafe) {
			mutex.unlock();
		}
		return "";
	}
	version = data[key].first;
	std::string value = data[key].second;
	if (!threadSafe) {
		mutex.unlock();
	}
	return value;
}

void ClientCache::UpdateLocal(const std::string& key, const std::string& value,
		const uint64& version) {
	if (!threadSafe) {
		mutex.lock();
	}
	//VERBOSE("update local data cache for %s", key.c_str());
	if (data.find(key) == data.end()) {
		data[key] = std::make_pair(NO_SUCH_DATA, "");
	}
	data[key].first = version;
	data[key].second = value;
	if (!threadSafe) {
		mutex.unlock();
	}
}

void ClientCache::Invalidate(const std::string& key) {
	//VERBOSE("invalidate local data cache for %s", key.c_str());
	if (!threadSafe) {
		mutex.lock();
	}
	if (data.find(key) != data.end()) {
		data.erase(key);
	}
	if (!threadSafe) {
		mutex.unlock();
	}
}

} /* namespace Client */
} /* namespace DTranx */

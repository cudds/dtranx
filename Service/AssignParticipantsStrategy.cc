/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <cassert>
#include "AssignParticipantsStrategy.h"

namespace DTranx {
namespace Service {

AssignParticipantsStrategy::AssignParticipantsStrategy(std::string selfAddress,
		SharedResources::TranxServiceSharedData* tranxServiceSharedData)
		: selfAddress(selfAddress), tranxServiceSharedData(tranxServiceSharedData) {

}

AssignParticipantsStrategy::~AssignParticipantsStrategy() {
}

void AssignParticipantsStrategy::InnerJoin(std::unordered_set<std::string>& set1,
		std::unordered_set<std::string>& set2) {
	/*
	 std::unordered_set<std::string> result;
	 for (auto it = set1.begin(); it != set1.end(); ++it) {
	 if (set2.find(*it) != set2.end()) {
	 result.insert(*it);
	 }
	 }
	 return result;
	 */
	for (auto it = set1.begin(); it != set1.end();) {
		if (set2.find(*it) == set2.end()) {
			it = set1.erase(it);
		} else {
			++it;
		}
	}
}

std::unordered_map<std::string, std::pair<std::vector<GItem>, std::vector<GItem>> > AssignParticipantsStrategy::MapItem(
		google::protobuf::RepeatedPtrField<GItem> readItems,
		google::protobuf::RepeatedPtrField<GItem> writeItems,
		std::unordered_map<std::string, std::unordered_set<std::string>>* readItemMapping,
		std::unordered_map<std::string, std::unordered_set<std::string>>* writeItemMapping) {
	std::unordered_map<std::string, std::pair<std::vector<GItem>, std::vector<GItem>> > result;

	/*
	 * tmpKeyMappings stores key to a list of server ips
	 * keyToItem stores key to Item
	 * commonIPs: check the overlapped server ips for all items(both readItems and writeItems)
	 * 	it's used to check if 1PC is possible
	 * firstItem: used to assign initial commonIPs
	 */
	std::unordered_map<std::string, std::unordered_set<std::string> > tmpKeyMappings;
	std::unordered_map<std::string, GItem> keyToItem;
	std::unordered_set<std::string> commonIPs;
	bool firstItem = true;
	std::unordered_set<std::string> nodeIPs;
	Service::GItem item;
	for (auto it = readItems.begin(); it != readItems.end(); ++it) {
		nodeIPs.clear();
		tranxServiceSharedData->getMappingStorage()->GetMapping(it->key(), nodeIPs);
		if (readItemMapping != NULL) {
			(*readItemMapping)[it->key()] = nodeIPs;
		}
		if (firstItem) {
			firstItem = false;
			commonIPs = nodeIPs;
		}
		if (!commonIPs.empty()) {
			InnerJoin(commonIPs, nodeIPs);
		}
		/*
		 * prefer the local server
		 */
		assert(nodeIPs.size() != 0);
		std::string nodeIP;
		if (nodeIPs.find(selfAddress) != nodeIPs.end()) {
			nodeIP = selfAddress;
		} else {
			tmpKeyMappings[it->key()] = nodeIPs;
			keyToItem[it->key()] = *it;
			continue;
		}
		std::pair<std::vector<GItem>, std::vector<GItem> >* items;
		std::unordered_map<std::string, std::pair<std::vector<GItem>, std::vector<GItem>> >::iterator nodeItr =
				result.find(nodeIP);
		if (nodeItr != result.end()) {
			items = &(nodeItr->second);
		} else {
			result[nodeIP] = std::pair<std::vector<Service::GItem>, std::vector<Service::GItem> >();
			items = &result[nodeIP];
			items->first.reserve(10);
			items->second.reserve(10);
		}
		item.set_key(it->key());
		if (it->has_version()) {
			item.set_version(it->version());
		}
		items->first.push_back(item);
	}
	for (auto it = writeItems.begin(); it != writeItems.end(); ++it) {
		nodeIPs.clear();
		tranxServiceSharedData->getMappingStorage()->GetMapping(it->key(), nodeIPs);
		if (writeItemMapping != NULL) {
			(*writeItemMapping)[it->key()] = nodeIPs;
		}
		if (firstItem) {
			firstItem = false;
			commonIPs = nodeIPs;
		}
		if (!commonIPs.empty()) {
			InnerJoin(commonIPs, nodeIPs);
		}
		assert(nodeIPs.size() != 0);
		std::pair<std::vector<Service::GItem>, std::vector<Service::GItem> > *items;
		/*
		 * write set requires all
		 */
		for (auto nodeip = nodeIPs.begin(); nodeip != nodeIPs.end(); ++nodeip) {
			std::unordered_map<std::string,
					std::pair<std::vector<Service::GItem>, std::vector<Service::GItem>> >::iterator nodeItr =
					result.find(*nodeip);
			if (nodeItr != result.end()) {
				items = &(nodeItr->second);
			} else {
				result[*nodeip] =
						std::pair<std::vector<Service::GItem>, std::vector<Service::GItem> >();
				items = &result[*nodeip];
				items->first.reserve(10);
				items->second.reserve(10);
			}
			item.set_key(it->key());
			item.set_value(it->value());
			if (it->has_version()) {
				item.set_version(it->version());
			}
			items->second.push_back(item);
		}
	}
	if (commonIPs.size() == 1 && *commonIPs.begin() != selfAddress) {
		result.clear();
		result[*commonIPs.begin()] = std::pair<std::vector<Service::GItem>,
				std::vector<Service::GItem>>();
		return result;
	}

	/*
	 * fill the readset with ip already involved
	 */
	std::unordered_set<std::string> toRemove;
	for (auto it = tmpKeyMappings.begin(); it != tmpKeyMappings.end(); ++it) {
		for (auto ip = it->second.begin(); ip != it->second.end(); ++ip) {
			if (result.find(*ip) != result.end()) {
				Service::GItem item;
				item.set_key(it->first);
				if (keyToItem[it->first].has_version()) {
					item.set_version(keyToItem[it->first].version());
				}
				result[*ip].first.push_back(item);
				toRemove.insert(it->first);
				break;
			}
		}
	}
	for (auto it = toRemove.begin(); it != toRemove.end(); ++it) {
		tmpKeyMappings.erase(*it);
	}

	/*
	 * fill the rest of the readset by reducing the number of servers involved
	 * find the server that is most frequent among the keys
	 */
	std::unordered_map<std::string, int> serverFreq;
	for (auto it = tmpKeyMappings.begin(); it != tmpKeyMappings.end(); ++it) {
		for (auto ip = it->second.begin(); ip != it->second.end(); ++ip) {
			if (serverFreq.find(*ip) == serverFreq.end()) {
				serverFreq[*ip] = 1;
			} else {
				serverFreq[*ip]++;
			}
		}
	}
	std::set<std::pair<int, std::string>> serverFreqSorted;
	for (auto it = serverFreq.begin(); it != serverFreq.end(); ++it) {
		serverFreqSorted.insert(std::make_pair(it->second, it->first));
	}
	/*
	 for (auto it = tmpKeyMappings.begin(); it != tmpKeyMappings.end(); ++it) {
	 std::cout << "key: " << it->first << " needs to be resolved" << std::endl;
	 for (auto ip = it->second.begin(); ip != it->second.end(); ++ip) {
	 std::cout << *ip << " ";
	 }
	 std::cout << std::endl;
	 }
	 for (auto it = serverFreq.begin(); it != serverFreq.end(); ++it) {
	 std::cout << it->first << ":" << it->second << " ";
	 }
	 std::cout << std::endl;
	 for (auto it = serverFreqSorted.begin(); it != serverFreqSorted.end(); ++it) {
	 std::cout << it->first << ":" << it->second << " ";
	 }
	 std::cout << std::endl;
	 */
	while (!tmpKeyMappings.empty()) {
		toRemove.clear();
		assert(!serverFreqSorted.empty());
		std::string ip = serverFreqSorted.rbegin()->second;
		serverFreqSorted.erase(*serverFreqSorted.rbegin());
		for (auto it = tmpKeyMappings.begin(); it != tmpKeyMappings.end(); ++it) {
			if (it->second.find(ip) != it->second.end()) {
				Service::GItem item;
				item.set_key(it->first);
				if (keyToItem[it->first].has_version()) {
					item.set_version(keyToItem[it->first].version());
				}
				result[ip].first.push_back(item);
				toRemove.insert(it->first);
			}
		}
		for (auto it = toRemove.begin(); it != toRemove.end(); ++it) {
			tmpKeyMappings.erase(*it);
		}
	}
	return result;
}

} /* namespace Service */
} /* namespace DTranx */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 */

#include "gtest/gtest.h"
#include "TranxStates.h"
#include "DaemonStages/DaemonStagesMock.h"
#include "Util/FileUtil.h"
using namespace DTranx;

class TranxStates_Basic: public ::testing::Test {
public:
	TranxStates_Basic() {
		Util::ConfigHelper configHelper;
		configHelper.readFile("DTranx.conf");
		tranxServerInfo = new SharedResources::TranxServerInfo();
		tranxServerInfo->SetConfig(configHelper);
		std::vector<std::string> ips;
		ips.push_back("192.168.0.1");
		ips.push_back("192.168.0.2");
		ips.push_back("192.168.0.3");
		tranxServerInfo->SetAllNodes(ips);
		tranxServerInfo->SetNodeID(2);
		tranxStates = new Stages::TranxStates(tranxServerInfo);
	}
	~TranxStates_Basic() {
		delete tranxStates;
		delete tranxServerInfo;
	}

	Stages::TranxStates *tranxStates;
	SharedResources::TranxServerInfo *tranxServerInfo;
private:
	TranxStates_Basic(const TranxStates_Basic&) = delete;
	TranxStates_Basic& operator=(const TranxStates_Basic&) = delete;

};

TEST_F(TranxStates_Basic, CheckAndSetReadyState) {
	tranxStates->gcFactory.UpdateEarliest(11);
	tranxStates->gcFactory.UpdateEarliest(1, 12);
	tranxStates->gcFactory.UpdateEarliest(3, 13);
	Util::TranxID tranxID1(1, 16);
	Util::TranxID tranxID3(3, 10);

	EXPECT_EQ(tranxStates->CheckAndSetReadyState(tranxID1), DTranx::Stages::State::NOTEXIST);
	EXPECT_EQ(tranxStates->CheckAndSetReadyState(tranxID1), DTranx::Stages::State::READY);
	EXPECT_EQ(tranxStates->CheckAndSetReadyState(tranxID3), DTranx::Stages::State::GC);
}

TEST_F(TranxStates_Basic, CheckAndSetCommitState) {
	Util::TranxID tranxID2(2, 16);
	EXPECT_EQ(tranxStates->CheckAndSetReadyState(tranxID2), DTranx::Stages::State::NOTEXIST);
	EXPECT_EQ(tranxStates->CheckAndSetCommitState(tranxID2), DTranx::Stages::State::READY);
	EXPECT_EQ(tranxStates->CheckAndSetCommitState(tranxID2), DTranx::Stages::State::COMMIT);
}

TEST_F(TranxStates_Basic, SetToCommitState) {
	Util::TranxID tranxID1(1, 12);
	EXPECT_EQ(DTranx::Stages::State::NOTEXIST, tranxStates->CheckState(tranxID1));
	tranxStates->SetToCommitState(tranxID1);
	EXPECT_EQ(DTranx::Stages::State::COMMIT, tranxStates->CheckState(tranxID1));
}

TEST_F(TranxStates_Basic, CheckAndSetAbortState) {
	tranxStates->gcFactory.UpdateEarliest(1, 12);
	tranxStates->gcFactory.UpdateEarliest(3, 13);
	Util::TranxID tranxID2_1(2, 16);
	Util::TranxID tranxID2_2(2, 17);

	EXPECT_EQ(tranxStates->CheckAndSetAbortState(tranxID2_1), DTranx::Stages::State::NOTEXIST);
	EXPECT_EQ(tranxStates->CheckAndSetAbortState(tranxID2_1), DTranx::Stages::State::ABORT);
	EXPECT_EQ(tranxStates->CheckAndSetReadyState(tranxID2_2), DTranx::Stages::State::NOTEXIST);
	EXPECT_EQ(tranxStates->CheckState(tranxID2_2), DTranx::Stages::State::READY);
	EXPECT_EQ(tranxStates->CheckAndSetAbortState(tranxID2_2), DTranx::Stages::State::READY);
}

TEST_F(TranxStates_Basic, SetToAbortState) {
	Util::TranxID tranxID1(1, 12);
	EXPECT_EQ(DTranx::Stages::State::NOTEXIST, tranxStates->CheckState(tranxID1));
	tranxStates->SetToAbortState(tranxID1);
	EXPECT_EQ(DTranx::Stages::State::ABORT, tranxStates->CheckState(tranxID1));
}

TEST_F(TranxStates_Basic, CheckAndSetMigrateState) {
	Util::TranxID tranxID2_1(2, 16);
	Util::TranxID tranxID2_2(2, 17);
	EXPECT_EQ(tranxStates->CheckAndSetMigrateState(tranxID2_1), DTranx::Stages::State::NOTEXIST);
	EXPECT_EQ(tranxStates->CheckAndSetMigrateState(tranxID2_1), DTranx::Stages::State::MIGRATION);
	EXPECT_EQ(tranxStates->CheckAndSetReadyState(tranxID2_2), DTranx::Stages::State::NOTEXIST);
	EXPECT_EQ(tranxStates->CheckAndSetMigrateState(tranxID2_2), DTranx::Stages::State::READY);
}

TEST_F(TranxStates_Basic, SetToAck) {
	Util::TranxID tranxID2_1(2, 16);
	Util::TranxID tranxID2_2(2, 17);
	tranxStates->SetToAck(tranxID2_1, true);
	EXPECT_EQ(DTranx::Stages::State::GC, tranxStates->CheckAndSetReadyState(tranxID2_1));
	tranxStates->SetToAck(tranxID2_2, false);
	EXPECT_EQ(DTranx::Stages::State::GC, tranxStates->CheckAndSetReadyState(tranxID2_2));
}

TEST_F(TranxStates_Basic, MoveFromAckToGC) {
	tranxStates->gcFactory.UpdateEarliest(12);
	tranxStates->gcFactory.UpdateEarliest(1, 12);
	tranxStates->gcFactory.UpdateEarliest(3, 13);

	Util::TranxID tranxID2_1(2, 13);
	Util::TranxID tranxID2_2(2, 14);
	Util::TranxID tranxID2_3(2, 15);
	Util::TranxID tranxID2_4(2, 16);

	tranxStates->SetToAck(tranxID2_1, true);
	tranxStates->SetToAck(tranxID2_2, true);
	tranxStates->SetToAck(tranxID2_3, false);
	tranxStates->SetToAck(tranxID2_4, true);
	EXPECT_EQ(DTranx::Stages::State::GC, tranxStates->CheckAndSetCommitState(tranxID2_1));
}

TEST_F(TranxStates_Basic, UpdateGC) {
	tranxStates->UpdateGC(3, 13);
	Stages::GCFactory gcFactory = tranxStates->GetGCFactory();
	EXPECT_EQ(13, gcFactory.GetEarliest(3));
	tranxStates->UpdateGCEarliest(22);
	gcFactory = tranxStates->GetGCFactory();
	EXPECT_EQ(22, gcFactory.GetEarliest());
	EXPECT_EQ(22, tranxStates->GetGCEarliest());
}

class TranxStates_Stage: public ::testing::Test {
public:
	TranxStates_Stage() {
		Util::ConfigHelper configHelper;
		configHelper.readFile("DTranx.conf");
		tranxServerInfo = new SharedResources::TranxServerInfo();
		tranxServerInfo->SetConfig(configHelper);
		std::vector<std::string> ips;
		ips.push_back("192.168.0.1");
		ips.push_back("192.168.0.2");
		ips.push_back("192.168.0.3");
		tranxServerInfo->SetAllNodes(ips);
		tranxServerInfo->SetNodeID(2);
		tranxServerInfo->SetMode(SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER);
		tranxServiceSharedData = new DTranx::SharedResources::TranxServiceSharedData(2,
				tranxServerInfo);
		tranxStates = new Stages::TranxStates(tranxServerInfo);
		daemonStages = new DaemonStages::DaemonStagesMock(tranxServerInfo, tranxServiceSharedData);
		tranxStates->daemonStagesMock = daemonStages;
		tranxStates->StartStage();
	}
	~TranxStates_Stage() {
		tranxStates->ShutStage();
		delete tranxStates;
		delete daemonStages;
		delete tranxServerInfo;
		delete tranxServiceSharedData;
		Util::FileUtil::RemoveDir("dtranx.mapdb");
	}

	Stages::TranxStates *tranxStates;
	DaemonStages::DaemonStagesMock *daemonStages;
	SharedResources::TranxServerInfo *tranxServerInfo;
	DTranx::SharedResources::TranxServiceSharedData *tranxServiceSharedData;
private:
	TranxStates_Stage(const TranxStates_Stage&) = delete;
	TranxStates_Stage& operator=(const TranxStates_Stage&) = delete;
};

TEST_F(TranxStates_Stage, _CheckState) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setStateTask(Stages::TranxStatesStageTask::_CheckState);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::NOTEXIST, element.getState());
}

TEST_F(TranxStates_Stage, _CheckAndSetReadyState) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setStateTask(Stages::TranxStatesStageTask::_CheckAndSetReadyState);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::NOTEXIST, element.getState());
	element.setStateTask(Stages::TranxStatesStageTask::_CheckState);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::READY, element.getState());
}

TEST_F(TranxStates_Stage, _CheckAndSetCommitState) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setStateTask(Stages::TranxStatesStageTask::_CheckAndSetReadyState);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::NOTEXIST, element.getState());
	element.setStateTask(Stages::TranxStatesStageTask::_CheckAndSetCommitState);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::READY, element.getState());
	element.setStateTask(Stages::TranxStatesStageTask::_CheckState);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::COMMIT, element.getState());
}

TEST_F(TranxStates_Stage, _CheckAndSetAbortState) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setStateTask(Stages::TranxStatesStageTask::_CheckAndSetAbortState);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::NOTEXIST, element.getState());
	element.setStateTask(Stages::TranxStatesStageTask::_CheckState);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::ABORT, element.getState());
}

TEST_F(TranxStates_Stage, _CheckAndSetMigrateState) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setStateTask(Stages::TranxStatesStageTask::_CheckAndSetReadyState);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::NOTEXIST, element.getState());
	element.setStateTask(Stages::TranxStatesStageTask::_CheckAndSetMigrateState);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::READY, element.getState());
	element.setStateTask(Stages::TranxStatesStageTask::_CheckState);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::MIGRATION, element.getState());
}

TEST_F(TranxStates_Stage, _UpdateGC) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	Util::TranxID tranxID(3, 17);
	element.setStateTask(Stages::TranxStatesStageTask::_UpdateGC);
	element.SetTranxID(tranxID);
	RPC::ServerRPC serverRPC;
	serverRPC.requestMessage->mutable_tranxservicerpc()->mutable_gc()->set_nodeid(3);
	serverRPC.requestMessage->mutable_tranxservicerpc()->mutable_gc()->set_tranxid(50);
	element.SetServerRPC(&serverRPC);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	element.setStateTask(Stages::TranxStatesStageTask::_CheckState);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::GC, element.getState());
}

TEST_F(TranxStates_Stage, _SetToCommitState) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setStateTask(Stages::TranxStatesStageTask::_SetToCommitState);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	element.setStateTask(Stages::TranxStatesStageTask::_CheckState);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::COMMIT, element.getState());
}

TEST_F(TranxStates_Stage, _SetToAbortState) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setStateTask(Stages::TranxStatesStageTask::_SetToAbortState);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	element.setStateTask(Stages::TranxStatesStageTask::_CheckState);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::ABORT, element.getState());
}

TEST_F(TranxStates_Stage, _SetToAck) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setStateTask(Stages::TranxStatesStageTask::_SetToAck);
	Util::TranxID tranxID(2, 1);
	element.SetTranxID(tranxID);
	element.setAckCommitAbort(true);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	element.setStateTask(Stages::TranxStatesStageTask::_CheckState);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(Stages::State::GC, element.getState());
}

TEST_F(TranxStates_Stage, _GarbageCollectAck) {
	tranxStates->gcFactory = Stages::GCFactory(2, 10, 100000);
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setStateTask(Stages::TranxStatesStageTask::_SetToAck);
	element.setAckCommitAbort(true);
	for (int i = 1; i <= 10; ++i) {
		Util::TranxID tranxID(2, i);
		element.SetTranxID(tranxID);
		tranxStates->SendToStage(&element);
		EXPECT_EQ(&element, daemonStages->WaitForResult());
	}
	element.setStateTask(Stages::TranxStatesStageTask::_GarbageCollectAck);
	tranxStates->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_EQ(true, element.stateToBroadcast_);
}

TEST_F(TranxStates_Stage, _TerminateInThisStage) {
	DTranx::Util::Log::setLogPolicy( { { "Stages", "PROFILE" } });
	Stages::TranxServiceQueueElement *element = new Stages::TranxServiceQueueElement();
	element->setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element->setTerminateInThisStage(true);
	element->setStateTask(Stages::TranxStatesStageTask::_SetToAck);
	Util::TranxID tranxID(2, 1);
	element->SetTranxID(tranxID);
	element->setAckCommitAbort(true);
	tranxStates->SendToStage(element);
	sleep(2);
	VERBOSE("Is there a delete log before me? Check");
	DTranx::Util::Log::setLogPolicy( { { "Stages", "NOTICE" } });
}

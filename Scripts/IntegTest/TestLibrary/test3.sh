#!/bin/bash
function test3(){
	# test single client fail exit
	# this transaction should succeed but the coord will continue send commit request to participants
	RestartDtranx dtranx2 fail commit
	sudo docker exec dtranx1 bash -c '/dtranx/SimpleTranx -p Workloads/readall &> /dtranx/test3_successtranx'
	sudo docker cp dtranx1:/dtranx/test3_successtranx ./
	sudo docker exec dtranx1 bash -c 'rm /dtranx/test3_successtranx'

	# transaction should fail because lock is still granted to the previous not committed tranx.
	RestartDtranx dtranx2 hang postrecovery commit
	sudo docker exec dtranx1 bash -c '/dtranx/SimpleTranx -p Workloads/writedistributed &> /dtranx/test3_shouldfailwrite'
	sudo docker cp dtranx1:/dtranx/test3_shouldfailwrite ./
	sudo docker exec dtranx1 bash -c 'rm /dtranx/test3_shouldfailwrite'
	
	RestartDtranx dtranx2
	# sleep so that transaction in test4 won't fail because of late start of the dtranx2
	sleep 15
}

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <memory>
#include <fstream>
#include "DTranx/Client/Client.h"
#include "DTranx/Util/ConfigHelper.h"
#include "RPC/ClientRPC.h"
#include "DTranx/Service/Transaction.pb.h"
#include "Util/StringUtil.h"
using namespace DTranx;

int main(int argc, char **argv) {
	Util::Log::setLogPolicy( { { "Client", "VERBOSE" }, { "RPC", "VERBOSE" } });
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(10);
	if (argc < 4) {
		std::cout
				<< "please input any server address as the first parameter, self address as the second parameter" << std::endl
				<< "self binding port as the third parameter" << std::endl
				<< "note: use the node1 server address "
				<< std::endl;
		return 0;
	}
	std::string ip = argv[1];
	std::string selfAddressStr(argv[2]);
	uint32_t selfAddress[4];
	int selfPort = std::stoi(argv[3]);
	std::vector<std::string> tmpFields = Util::StringUtil::Split(selfAddressStr, '.');
	assert(tmpFields.size() == 4);
	for (int i = 0; i < 4; ++i) {
		selfAddress[i] = std::stoul(tmpFields[i], nullptr, 10);
	}
	Client::Client *client = new Client::Client(ip, 30000, context);
	assert(client->Bind(selfAddressStr, selfPort));
	RPC::ClientRPC* clientRPC;
	try {
		clientRPC = new RPC::ClientRPC(RPC::GServiceID::ClientService, client);
		clientRPC->SetClientOpcode(Service::GOpCode::CLIENT_COMMIT);
		clientRPC->SetReqIP(selfAddress);
		clientRPC->SetReqPort(selfPort);
	} catch (std::exception &e) {
		std::cout << "ning bad alloc 4" << std::endl;
	}
	DTranx::Service::GClientCommit::Request *request =
			clientRPC->GetClientRequest()->mutable_clientcommit()->mutable_request();
	request->set_tranxtype(Service::GTranxType::RUNMETIS);
	client->CallRPC(clientRPC);
	assert(client->BlockPoll(clientRPC));
	DTranx::Service::GClientCommit::Response *response =
			clientRPC->GetClientResponse()->mutable_clientcommit()->mutable_response();
	if (response->status() == Service::GStatus::OK) {
		std::cout << "success" << std::endl;
	} else {
		assert(false);
	}
	delete clientRPC;
	delete client;
	return 0;
}


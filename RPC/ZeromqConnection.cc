/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "DTranx/RPC/ZeromqConnection.h"
#include "DTranx/Util/Log.h"

namespace DTranx {
namespace RPC {

ZeromqConnection::ZeromqConnection(std::shared_ptr<zmq::context_t> context, int socketType)
		: socket(*context, socketType), context(context) {
	int lingtime = 0;
	socket.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
}

ZeromqConnection::~ZeromqConnection() noexcept(false){
	VERBOSE("Close Socket");
	socket.close();
}

bool ZeromqConnection::ConnectInproc(const std::string &address) {
	std::string endpoint = "inproc://";
	endpoint += address;
	return Connect(endpoint);
}

bool ZeromqConnection::ConnectTcp(const std::string &address, const std::string &port) {
	std::string endpoint = "tcp://";
	endpoint += address;
	endpoint += ":" + port;
	return Connect(endpoint);
}

bool ZeromqConnection::Connect(const std::string& endpoint) {
	try {
		socket.connect(endpoint.c_str());
	} catch (const error_t& error) {
		return false;
	}
	return true;
}

bool ZeromqConnection::Bind(const std::string &address, const std::string &port) {
	try {
		std::string endpoint = "tcp://" + address + ":" + port;
		VERBOSE("binding to %s", endpoint.c_str());
		socket.bind(endpoint.c_str());
	} catch (const error_t& error) {
		VERBOSE("ZeromqConnection::Bind failure");
		return false;
	} catch (const std::exception& e) {
		VERBOSE("ZeromqConnection::Bind failure");
		return false;
	}
	return true;
}

} /* namespace RPC */
} /* namespace DTranx */

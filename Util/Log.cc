/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * thread safe logging system supporting multiple levels of logging
 */

#include <unordered_map>
#include <cassert>
#include <cstdarg>
#include <cstring>
#include <boost/thread.hpp>
#include "DTranx/Util/Log.h"
#include "StringUtil.h"

namespace DTranx {
namespace Util {
namespace Log {

/*
 * IMPORTANT: Change the log policy when the program starts
 * 	because the policy is not thread safe
 */

std::string processName = StringUtil::format("%u", getpid());

/**
 * Specifies the log messages that should be displayed for each filename.
 * This first component is a pattern; the second is a log level.
 * A filename is matched against each pattern in order: if the filename starts
 * with or ends with the pattern, the corresponding log level defines the most
 * verbose messages that are to be displayed for the file. If a filename
 * matches no pattern, its log level will default to NOTICE.
 *
 * Protected by #mutex.
 */
std::vector<std::pair<std::string, std::string>> policy;

/**
 * Used to convert LogLevels into strings that will be printed.
 * This array must match the LogLevel enum in Core/Debug.h.
 */
const char* logLevelToString[] = { "SILENT", "ERROR", "WARNING", "NOTICE", "VERBOSE", "PROFILE",
NULL  // must be the last element in the array
		};

/**
 * Convert a string to a log level.
 * PANICs if the string is not a valid log level (case insensitive).
 */
LogLevel logLevelFromString(const std::string& level) {
	for (uint32_t i = 0; logLevelToString[i] != NULL; ++i) {
		if (strcasecmp(logLevelToString[i], level.c_str()) == 0)
			return LogLevel(i);
	}
	log((LogLevel::ERROR), __FILE__, __LINE__, __FUNCTION__, "'%s' is not a valid log level.\n",
			level.c_str());
	abort();
}
/**
 * Return the number of characters of __FILE__ that make up the path prefix.
 * That is, __FILE__ plus this value will be the relative path from the top
 * directory of the code tree.
 */
int calculateLengthFilePrefix() {
	const char* start = __FILE__;
	const char* match = strstr(__FILE__, "Util/Log.cc");
	assert(match != NULL);
	return int(match - start);
}

/// Stores result of calculateLengthFilePrefix().
const int lengthFilePrefix = calculateLengthFilePrefix();

/**
 * Strip out the common prefix of a filename to get a path from the project's
 * root directory.
 * \param fileName
 *      An absolute or relative filename, usually the value of __FILE__.
 * \return
 *      A nicer way to show display 'fileName' to the user.
 *      For example, this file would yield "Core/Debug.cc".
 */
const char*
relativeFileName(const char* fileName) {
	// Remove the prefix only if it matches that of __FILE__. This check is
	// needed in case someone compiles different files using different paths.
	if (strncmp(fileName, __FILE__, lengthFilePrefix) == 0)
		return fileName + lengthFilePrefix;
	else
		return fileName;
}

/**
 * A cache of the results of getLogLevel(), since that function is slow.
 * This needs to be cleared when the policy changes.
 * The key to the map is a pointer to the absolute filename, which should be a
 * string literal.
 *
 * Protected by #mutex.
 */
std::unordered_map<const char*, LogLevel> isLoggingCache;

/**
 * From the policy, calculate the most verbose log level that should be
 * displayed for this file.
 * This is slow, so isLogging() caches the results in isLoggingCache.
 *
 * Must be called with #mutex held.
 *
 * \param fileName
 *      Relative filename.
 */
LogLevel getLogLevel(const char* fileName) {
	for (auto it = policy.begin(); it != policy.end(); ++it) {
		const std::string& pattern = it->first;
		const std::string& headerPattern = "./" + pattern;
		const std::string& logLevel = it->second;
		if (StringUtil::StartsWith(fileName, pattern) || StringUtil::EndsWith(fileName, pattern)
				|| StringUtil::StartsWith(fileName, headerPattern)) {
			return logLevelFromString(logLevel);
		}
	}
	return LogLevel::NOTICE;
}

bool isLogging(LogLevel level, const char* fileName) {
	LogLevel verbosity = getLogLevel(relativeFileName(fileName));
	return uint32_t(level) <= uint32_t(verbosity);
}

/**
 * Where log messages go.
 */
FILE* stream = stderr;

void log(LogLevel level, const char* fileName, uint32_t lineNum, const char* functionName,
			const char* format, ...) {
	va_list ap;
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC_RAW, &now);

	// This ensures that output on stderr won't be interspersed with other
	// output. This normally happens automatically for a single call to
	// fprintf, but must be explicit since we're using two calls here.
	flockfile(stream);
	fprintf(stream, "%010lu.%06lu %s:%d in %s() %s[%s:%lu]: ", now.tv_sec, now.tv_nsec / 1000,
			relativeFileName(fileName), lineNum, functionName, logLevelToString[uint32_t(level)],
			processName.c_str(), syscall(__NR_gettid));

	va_start(ap, format);
	vfprintf(stream, format, ap);
	va_end(ap);

	funlockfile(stream);

	fflush(stream);
}

void profile(const char* fileName, uint32_t lineNum, const char* functionName,
				std::string profile) {
	va_list ap;
	struct timespec now;
	clock_gettime(CLOCK_MONOTONIC_RAW, &now);

	// This ensures that output on stderr won't be interspersed with other
	// output. This normally happens automatically for a single call to
	// fprintf, but must be explicit since we're using two calls here.
	flockfile(stream);
	fprintf(stream, "%010lu.%06lu %s:%d in %s() %s[%s:%lu]: ", now.tv_sec, now.tv_nsec / 1000,
			relativeFileName(fileName), lineNum, functionName,
			logLevelToString[uint32_t(LogLevel::PROFILE)], processName.c_str(),
			syscall(__NR_gettid));

	std::cerr << profile;
	fprintf(stream, "\n");

	funlockfile(stream);

	fflush(stream);
}

void setLogPolicy(const std::vector<std::pair<std::string, std::string>>& newPolicy) {
	policy = newPolicy;
	isLoggingCache.clear();
}

} /* namespace Log */
} /* namespace Util */
} /* namespace DTranx */


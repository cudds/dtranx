/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <cassert>
#include <cstdlib>
#include <leveldb/cache.h>
#include "MappingDS.h"
#include "DTranx/Util/Log.h"
#include "Util/StringUtil.h"
#include "Util/FileUtil.h"
#include "DTranx/Util/Log.h"

namespace DTranx {
namespace SharedResources {

//TODO: unit test for using string storage option

MappingDS::MappingDS(std::vector<std::string> cluster, std::string dbName,
		StorageOption storageOption)
		: mutex_cache(), mutex_db(), storageOption(storageOption) {
	for (int i = 0; i < cluster.size(); ++i) {
		clusterID[i + 1] = cluster[i];
		clusterIP[cluster[i]] = i + 1;
	}
	options.create_if_missing = true;
	options.block_cache = leveldb::NewLRUCache(50 * 1024 * 1024);
	leveldb::Status status = leveldb::DB::Open(options, dbName, &db);

	if (false == status.ok()) {
		ERROR("Unable to open/create database %s: %s, try to remove and LOCK and then reopen",
				dbName.c_str(), status.ToString().c_str());
		std::string lockFile = dbName + "/LOCK";
		Util::FileUtil::RemoveFile(lockFile);
		status = leveldb::DB::Open(options, dbName, &db);
		assert(true == status.ok());
	}
	leveldb::Iterator* it = db->NewIterator(leveldb::ReadOptions());

	for (it->SeekToFirst(); it->Valid(); it->Next()) {
		uint64 hashedKey = Util::StringUtil::SDBMHash(it->key().ToString().c_str());
		std::vector<std::string> ips = Util::StringUtil::Split(it->value().ToString(), DELIMITER);
		if (storageOption == StorageOption::BIT) {
			lookupTable[hashedKey] = GetBitArrayFromMapping(
					std::unordered_set<std::string>(ips.begin(), ips.end()));
		} else {
			lookupTableString[hashedKey] = std::unordered_set<std::string>(ips.begin(), ips.end());
		}
	}

	if (false == it->status().ok()) {
		VERBOSE("An error was found during the scan: %s", it->status().ToString().c_str());
		assert(false);
	}
	delete it;
}

MappingDS::~MappingDS() noexcept(false) {
	if (db != NULL) {
		delete db;
	}
	delete options.block_cache;
}

bool MappingDS::UpdateLookupTable(std::string key, std::unordered_set<std::string> mapping) {
	//VERBOSE("updating lookup table in mapping storage for key %s", key.c_str());
	uint64 hashedKey = Util::StringUtil::SDBMHash(key.c_str());
	mutex_cache.lock();
	if (storageOption == StorageOption::BIT) {
		lookupTable[hashedKey] = GetBitArrayFromMapping(mapping);
	} else {
		lookupTableString[hashedKey] = mapping;
	}
	mutex_cache.unlock();
	std::string ips;
	for (auto it = mapping.begin(); it != mapping.end(); ++it) {
		ips += *it + DELIMITER;
	}
	ips = ips.substr(0, ips.size() - 1);
	mutex_db.lock();
	leveldb::WriteOptions writeOptions;
	writeOptions.sync = true;
	db->Put(writeOptions, key, ips);
	mutex_db.unlock();
	return true;
}

bool MappingDS::DeleteFromLookupTable(std::string key) {
	uint64 hashedKey = Util::StringUtil::SDBMHash(key.c_str());
	mutex_cache.lock();
	if (storageOption == StorageOption::BIT) {
		if (lookupTable.find(hashedKey) != lookupTable.end()) {
			lookupTable.erase(hashedKey);
			mutex_cache.unlock();
			mutex_db.lock();
			leveldb::WriteOptions writeOptions;
			writeOptions.sync = true;
			db->Delete(writeOptions, key);
			mutex_db.unlock();
			return true;
		}
		mutex_cache.unlock();
		return false;
	} else {
		if (lookupTableString.find(hashedKey) != lookupTableString.end()) {
			lookupTableString.erase(hashedKey);
			mutex_cache.unlock();
			mutex_db.lock();
			leveldb::WriteOptions writeOptions;
			writeOptions.sync = true;
			db->Delete(writeOptions, key);
			mutex_db.unlock();
			return true;
		}
		mutex_cache.unlock();
		return false;
	}
}

void MappingDS::GetMapping(std::string key, std::unordered_set<std::string>& mapping) {
	uint64 hashedKey = Util::StringUtil::SDBMHash(key.c_str());
	boost::unique_lock<boost::mutex> lockGuard(mutex_cache);
	if (storageOption == StorageOption::BIT) {
		if (lookupTable.find(hashedKey) != lookupTable.end()) {
			uint64_t bitArray = lookupTable[hashedKey];
			lockGuard.unlock();
			GetMappingFromBitArray(bitArray, mapping);
			return;
		}
	} else {
		std::unordered_map<uint64, std::unordered_set<std::string>>::iterator it = lookupTableString.find(hashedKey);
		if (it != lookupTableString.end()) {
			mapping = it->second;
			return;
		}
	}
	lockGuard.unlock();
	mapping.insert(clusterID[(hashedKey % clusterID.size()) + 1]);
	return;
}

void MappingDS::GetMappingFromBitArray(uint64 bitArray, std::unordered_set<std::string>& mapping) {
	int index = 1;
	while (bitArray) {
		bool isExist = bitArray % 2;
		if (isExist) {
			assert(clusterID.find(index) != clusterID.end());
			mapping.insert(clusterID[index]);
		}
		index++;
		bitArray >>= 1;
	}
	return;
}

uint64 MappingDS::GetBitArrayFromMapping(std::unordered_set<std::string> mapping) {
	uint64 bitResult = 0;
	for (auto it = mapping.begin(); it != mapping.end(); ++it) {
		assert(clusterIP.find(*it) != clusterIP.end());
		uint64 serverID = clusterIP[*it];
		bitResult |= 1 << (serverID - 1);
	}
	return bitResult;
}

} /* namespace SharedResources */
} /* namespace DTranx */

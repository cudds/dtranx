/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_DAEMONSTAGES_DAEMONSTAGES_H_
#define DTRANX_DAEMONSTAGES_DAEMONSTAGES_H_

#include <boost/thread.hpp>
#include "RPC/ServerRPC.h"
#include "Stages/ServerReply.h"
#include "SharedResources/TranxServiceSharedData.h"
#include "SharedResources/TranxServerInfo.h"
#include "Stages/TranxServiceQueueElement.h"
#include "Build/Util/Profile.pb.h"
#include "Stages/PerfStage.h"
#include "Util/ThreadHelper.h"

namespace DTranx {
namespace Stages {
class TranxStates;
class TranxRPCHelper;
class StringStorage;
namespace Log {
class TranxLog;
}
namespace Lock {
class WriteBlockLockTable;
}
}

namespace DaemonStages {

class DaemonStages: public Stages::PerfStage {
public:
	DaemonStages(SharedResources::TranxServerInfo *tranxServerInfo,
			SharedResources::TranxServiceSharedData *tranxServiceSharedData,
			std::string stageName = "DaemonStagesDefault")
			: Stages::PerfStage(stageName), tranxServerInfo(tranxServerInfo),
					tranxServiceSharedData(tranxServiceSharedData), terminateThread(false),
					daemonProcessQueue(1000) {
	}
	virtual ~DaemonStages() noexcept(false) {

	}

	void InitStages(Stages::TranxRPCHelper *tranxRPCHelper,
			Stages::TranxStates *tranxStates,
			Stages::StringStorage *localStorage,
			Stages::Log::TranxLog *tranxLog,
			Stages::Lock::WriteBlockLockTable *lockTable);

	virtual void StartService(std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) = 0;

	void ShutService() {
		terminateThread.store(true);
		if (daemonThread.joinable()) {
			daemonThread.join();
		}
	}

	/*
	 * interfaces to add to queue
	 */
	virtual void SendToDaemon(Stages::TranxServiceQueueElement *element) {
		//assert(daemonProcessQueue.enqueue(element));
		while (!daemonProcessQueue.try_enqueue(element)){
			if(daemonProcessQueue.enqueue(element)){
				break;
			}
		}
	}

protected:
	/*
	 * MidStages
	 */
	Stages::TranxRPCHelper *tranxRPCHelper;
	Stages::TranxStates *tranxStates;
	Stages::StringStorage *localStorage;
	Stages::Log::TranxLog *tranxLog;
	Stages::Lock::WriteBlockLockTable *lockTable;

	moodycamel::ProducerToken * ptok_tranxStates;

	/*
	 * shared/non-shared data
	 */
	SharedResources::TranxServerInfo *tranxServerInfo;
	SharedResources::TranxServiceSharedData *tranxServiceSharedData;

	/*
	 * stage pipeline threads
	 */
	virtual void DaemonThread() = 0;
	boost::thread daemonThread;
	std::atomic_bool terminateThread;
	moodycamel::ConcurrentQueue<Stages::TranxServiceQueueElement *> daemonProcessQueue;
};

} /* namespace DaemonStages */
} /* namespace DTranx */

#endif /* DTRANX_DAEMONSTAGES_DAEMONSTAGES_H_ */

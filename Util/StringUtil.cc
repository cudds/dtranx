/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "StringUtil.h"
#include <cstdarg>
#include <cassert>
#include <sstream>
#include "DTranx/Util/Log.h"

namespace DTranx {
namespace Util {
const uint64 StringUtil::kFNVOffsetBasis64 = 0xCBF29CE484222325;
const uint64 StringUtil::kFNVPrime64 = 1099511628211;
const char StringUtil::DELIMITER = '#';

StringUtil::StringUtil() {

}

StringUtil::~StringUtil() noexcept(false) {
}

bool StringUtil::StartsWith(const std::string& haystack, const std::string& needle) {
	return (haystack.compare(0, needle.length(), needle) == 0);
}

bool StringUtil::EndsWith(const std::string& haystack, const std::string& needle) {
	if (haystack.length() < needle.length()) {
		return false;
	}
	return (haystack.compare(haystack.length() - needle.length(), needle.length(), needle) == 0);
}

/*
 *  This comes from the RAMCloud project.
 */
std::string StringUtil::format(const char* format, ...) {
	std::string s;
	va_list ap;
	va_start(ap, format);

	// We're not really sure how big of a buffer will be necessary.
	// Try 1K, if not the return value will tell us how much is necessary.
	int bufSize = 1024;
	while (true) {
		char buf[bufSize];
		// vsnprintf trashes the va_list, so copy it first
		va_list aq;
		__va_copy(aq, ap);
		int r = vsnprintf(buf, bufSize, format, aq);
		assert(r >= 0);  // old glibc versions returned -1
		if (r < bufSize) {
			s = buf;
			break;
		}
		bufSize = r + 1;
	}

	va_end(ap);
	return s;
}

std::vector<std::string> StringUtil::Split(std::string str, char delimiter) {
	size_t start = 0;
	size_t end = str.find_first_of(delimiter);
	std::vector<std::string> result;
	size_t size = str.size();
	if (size == 0) {
		return result;
	}
	if (end == std::string::npos) {
		result.push_back(str);
		return result;
	}
	while (end <= std::string::npos) {
		result.emplace_back(str.substr(start, end - start));
		if (end == std::string::npos || end == size - 1) {
			break;
		}
		start = end + 1;
		end = str.find_first_of(delimiter, start);
	}
	return result;
}

void StringUtil::Split(std::string str, char delimiter, std::vector<std::string>& result) {
	size_t start = 0;
	size_t end = str.find_first_of(delimiter);
	size_t size = str.size();
	if (size == 0) {
		return;
	}
	if (end == std::string::npos) {
		result.push_back(str);
		return;
	}
	while (end <= std::string::npos) {
		result.emplace_back(str.substr(start, end - start));
		if (end == std::string::npos || end == size - 1) {
			break;
		}
		start = end + 1;
		end = str.find_first_of(delimiter, start);
	}
	return;
}

std::string StringUtil::Concat(std::vector<std::string> strs, char delimiter) {
	std::stringstream ss;
	for (auto it = strs.begin(); it != strs.end(); ++it) {
		ss << *it << delimiter;
	}
	return ss.str().substr(0, ss.str().size() - 1);
}

std::string StringUtil::UstringToString(zmsg::ustring str) {
	std::string result;
	for (auto it = str.begin(); it != str.end(); ++it) {
		result += *it;
	}
	return result;
}

std::string StringUtil::BuildKeyName(uint64 key) {
	uint64_t hash = kFNVOffsetBasis64;

	for (int i = 0; i < 8; i++) {
		uint64_t octet = key & 0x00ff;
		key = key >> 8;

		hash = hash ^ octet;
		hash = hash * kFNVPrime64;
	}
	return std::string("user").append(std::to_string(hash));
}

uint64 StringUtil::SDBMHash(const char *cstr) {
	uint64 hash = 0;
	uint64 c;
	while ((c = *cstr++) != '\0') {
		hash = c + (hash << 6) + (hash << 16) - hash;
	}
	return hash;
}

std::string StringUtil::GetNodeID(std::string tranxID) {
	/*
	std::vector<std::string> fields = StringUtil::Split(tranxID, DELIMITER);
	if (fields.size() != 2) {
		ERROR("tranxID format error: tranxID is %s", tranxID.c_str());
		assert(false);
	}
	return fields[0];
	*/
	size_t end = tranxID.find_first_of(DELIMITER);
	return tranxID.substr(0, end);

}

uint64 StringUtil::GetTranxID(std::string tranxID) {
	std::vector<std::string> fields;
	Util::StringUtil::Split(tranxID, DELIMITER, fields);
	if (fields.size() != 2) {
		ERROR("tranxID format error: tranxID is %s", tranxID.c_str());
		assert(false);
	}
	return std::strtoull(fields[1].c_str(), NULL, 10);
}

} /* namespace Util */
} /* namespace DTranx */

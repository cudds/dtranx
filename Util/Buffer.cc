/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Buffer takes care of memory free for the data
 */

#include <iostream>
#include "DTranx/Util/Buffer.h"

namespace DTranx {
namespace Util {

Buffer::Buffer()
		: data(NULL), length(0) {
}

Buffer::Buffer(void *data, uint32 length)
		: data(data), length(length) {
}

Buffer::Buffer(Buffer&& other) {
	length = other.length;
	data = other.data;
	other.length = 0;
	other.data = NULL;
}

Buffer& Buffer::operator=(Buffer&& other) {
	length = other.length;
	data = other.data;
	other.length = 0;
	other.data = NULL;
	return *this;
}

Buffer::~Buffer() noexcept(false) {
	if (data != NULL) {
		delete[] static_cast<uint8*>(data);
	}
}

} /* namespace Util */
} /* namespace DTranx */

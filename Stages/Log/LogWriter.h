/*
 * Copyright (c) 2011 The LevelDB Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file. See the AUTHORS file for names of contributors.
 *
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * LogWriter helps transaction service to persist logs to disk
 * it's not thread safe.
 *
 * format of the log
 *
 * Each block consists of a sequence of records:
 * 	block := record* trailer?
 * 	record :=
 * 	checksum: uint32	// crc32c of type and data[] ; little-endian
 * 	length: uint16		// little-endian
 * 	type: uint8		// One of FULL, FIRST, MIDDLE, LAST
 * 	data: uint8[length]
 * A record never starts within the last six bytes of a block
 */
#ifndef DTRANX_STAGES_LOG_DISKLOGWRITER_H_
#define DTRANX_STAGES_LOG_DISKLOGWRITER_H_

#include "PosixWritableFile.h"
#include "PmemWritableFile.h"
#include "DTranx/Util/types.h"

namespace DTranx {
namespace Stages {
namespace Log {

class LogWriter {
public:
	explicit LogWriter(std::string& fname, bool append, LogOption logOption, uint64 truncateThreshold);
	virtual ~LogWriter();
	LogStatus AddRecord(const Slice& slice);

	uint64 GetTotalAvailBytes(){
		return totalAvailBytes;
	}

private:
	/*
	 * totalAvailBytes is only used for pmem log
	 */
	WritableFile* dest_;
	uint64 totalAvailBytes;
	int block_offset_;  // Current offset in block

	// crc32c values for all supported record types.  These are
	// pre-computed to reduce the overhead of computing the crc of the
	// record type stored in the header.
	uint32_t type_crc_[kMaxRecordType + 1];

	LogStatus EmitPhysicalRecord(RecordType type, const char* ptr, size_t length);

	//Noncopyable
	LogWriter(const LogWriter&) = delete;
	LogWriter& operator=(const LogWriter&) = delete;
};

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_LOG_DISKLOGWRITER_H_ */

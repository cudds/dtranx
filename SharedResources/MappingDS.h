/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * MappingDS is used for data partitioning. It consists of hashing and a lookup table.
 * 	Hashing: using the basic hashing algorithm in c++ standard library.
 * 	LookupTable: stored in hashmap data structure where key is hashed to the key of the hashmap and value is uint64 that stores the partition id, encoded on bit level
 *
 * 	BitArray: bit0(LSB) corresponds to cluster[0]
 * 	ServerID's stored in clusterID/clusterIP starts from 1
 *
 * 	In database, it's stored as key->ip#ip#ip
 * 	in-mem cache is always consistent with the disk and during initialization, it reads all database into the memory
 *
 */

#ifndef DTRANX_SHAREDRESOURCES_MAPPINGDS_H_
#define DTRANX_SHAREDRESOURCES_MAPPINGDS_H_

#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <string>
#include <boost/thread.hpp>
#include <leveldb/db.h>
#include "DTranx/Util/types.h"
#include "Util/StaticConfig.h"

namespace DTranx {
namespace SharedResources {

/*
 * DELIMITER is used in mappingDS to separate IP's
 * note that in Stage/Storage, there's also a DELIMITER for storage usage but these
 * two DELIMITER are not related.
 */
const char DELIMITER = '#';

class MappingDS {
public:
	enum StorageOption {
		BIT, STRING
	};
	/*
	 * constructor, cluster should be an ordered ip vector for the whole cluster.
	 * prefer the cluster stored in database
	 */
	MappingDS(std::vector<std::string> cluster, std::string dbName, StorageOption storageOption =
			StorageOption::BIT);
	virtual ~MappingDS() noexcept(false);

	/*
	 * UpdateLookupTable/DeleteFromLookupTable: update/delete a mapping entry to the lookup table
	 * UpdateLookupTable will insert new if not exist
	 */
	bool UpdateLookupTable(std::string key, std::unordered_set<std::string> mapping);
	bool DeleteFromLookupTable(std::string key);
	void GetMapping(std::string key, std::unordered_set<std::string>& mapping);

private:
	/*
	 * helper functions to transform from bit to mapping set and vice versa.
	 */
	void GetMappingFromBitArray(uint64 bitArray, std::unordered_set<std::string>& mapping);
	uint64 GetBitArrayFromMapping(std::unordered_set<std::string> mapping);

	/*
	 * clusterID/clusterIP stores two directional mapping from ID to IP and vice versa
	 * lookupTable stores a mapping from hashkey of the key to the partition bit array
	 */
	std::unordered_map<uint64, std::string> clusterID;
	std::unordered_map<std::string, uint64> clusterIP;
	std::unordered_map<uint64, uint64> lookupTable;
	std::unordered_map<uint64, std::unordered_set<std::string>> lookupTableString;

	/*
	 * persist to disk
	 */
	leveldb::DB* db;
	leveldb::Options options;

	/*
	 * In memory storage options: bitarray, plain ip
	 */
	StorageOption storageOption;

	/*
	 * mutex_cache protects concurrent access.
	 * protect lookupTable specifically
	 *
	 * mutex_db protects db access
	 */
	mutable boost::mutex mutex_cache;
	mutable boost::mutex mutex_db;
};

} /* namespace SharedResources */
} /* namespace DTranx */

#endif /* DTRANX_SHAREDRESOURCES_MAPPINGDS_H_ */

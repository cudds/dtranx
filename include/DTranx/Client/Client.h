/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Client takes care of constructing the ClientRPC and then sending the request via Zeromq interfaces
 */

#ifndef DTRANX_CLIENT_CLIENT_H_
#define DTRANX_CLIENT_CLIENT_H_

#include <unordered_map>
#include <set>
#include <boost/thread.hpp>
#include <atomic>
#include <boost/thread.hpp>
#include <condition_variable>
#include "DTranx/RPC/ZeromqSender.h"
#include "DTranx/RPC/ZeromqReceiver.h"
#include "DTranx/Util/Buffer.h"
#include "DTranx/RPC/RPC.pb.h"
#include "DTranx/Util/ConcurrentQueue.h"

namespace DTranx {
namespace RPC {
class ClientRPC;
}
;
namespace Client {
#define NUM_OF_RECV_QUEUE 50
class Client {
public:
	/*
	 * Initialize the client
	 */
	Client(const std::string& ip, uint32_t port, std::shared_ptr<zmq::context_t> context);
	virtual ~Client() noexcept(false);

	bool Bind(const std::string& selfAddress, uint32_t selfPort);

	/*
	 * client calls to send/recv messages
	 */
	void CallRPC(RPC::ClientRPC *clientRPC);
	void Retry(RPC::ClientRPC* clientRPC);
	void Cancel(RPC::ClientRPC* clientRPC);

	bool Poll(RPC::ClientRPC *clientRPC);

	bool BlockPoll(RPC::ClientRPC *clientRPC);

	/*
	 * messageID access
	 */
	uint64 GetNextMessageId();

	/*
	 * used in ClientTranx
	 */
	std::string selfAddressStr;
	uint32_t selfPort;

private:
	/*
	 * sockets related
	 * 	no locks needed because only one thread is accessing it.
	 */
	std::unique_ptr<RPC::ZeromqSender> sender;
	std::unique_ptr<RPC::ZeromqReceiver> receiver;
	std::shared_ptr<zmq::context_t> context;
	boost::thread recvThread;
	void RecvThread();
	std::atomic_bool terminateSocketThreads;

	/*
	 * atomic assignment of message ID
	 */
	std::atomic<uint64> nextMessageId;
	/*
	 * shared queues
	 */
	std::unordered_map<int, boost::mutex *> recvMutexes;
	std::unordered_map<int, boost::condition_variable *> recvCVs;

	boost::mutex sendMutex;

	std::unordered_map<uint64, RPC::GRPCResponseMessage*> responses[NUM_OF_RECV_QUEUE];

	/*
	 * time out config
	 * 	pollWaitTime: how long to wait before poll again when not notified
	 * 	rpcTimeout: drop the rpc after this timeout
	 */
	uint64 pollWaitTime;
	uint64 rpcTimeout;
};

} /* namespace Client */
} /* namespace DTranx */

#endif /* DTRANX_CLIENT_CLIENT_H_ */

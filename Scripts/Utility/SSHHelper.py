import subprocess
import paramiko
import os
import time
from ConfigHelper import *
from BashHelper import *
#
# SSHHelper provides copying to/from and running in the remote machines
#

#
# important: running commands with too much output will lead to hang because
# of exit_status_ready call. Transport window size is limited.
#
def runSSHCommand(server, username, command):
	client = paramiko.client.SSHClient()
	client.load_system_host_keys()
	client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
	client.connect(server, username=username)
	stdin, stdout, stderr = client.exec_command(command)
	while not stdout.channel.exit_status_ready():
		time.sleep(1)
	client.close()
	stdoutAsString = []
	stderrAsString = []
	for line in stdout:
		stdoutAsString.append(line)
	for line in stderr:
		stderrAsString.append(line)
	return stdoutAsString, stderrAsString

def runSSHCommandAsBackground(server, username, command, outputFile):
	return runSSHCommand(server, username, command+" &>"+outputFile+" &")

def runSSHCommandWithOutput(server, username, command, outputFile):
	return runSSHCommand(server, username, command+" &>"+outputFile)

def scp(direction, localFile, user, server, path):
	if direction == True:
		os.system("scp -r " + localFile + " " + user + "@" + server + ":" + path)
	else:
		os.system("scp -r " + user + "@" + server + ":" + path + " " + localFile)

def KillRemote(server, username, appName):
	curScriptDir = GetScriptDir(__file__)
	bashCmd = "\
		" + curScriptDir + "/KillRemote.sh " + appName + " " + server + " " + username
	runBash(bashCmd) 
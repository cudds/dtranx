/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "ServerRPC.h"
#include "Util/StaticConfig.h"

namespace DTranx {
namespace RPC {

ServerRPC::ServerRPC()
		: BaseRPC(), isPerfOutPutTask(false) {
	//clock_gettime(CLOCK_MONOTONIC_RAW, &startTime);
	responseMessage = new GRPCResponseMessage();
	toMonitor = false;
}

ServerRPC::ServerRPC(ServerRPC&& other)
		: BaseRPC(std::move(other)), startTime(std::move(other.startTime)),
				clientID(other.clientID), toMonitor(other.toMonitor),
				isPerfOutPutTask(other.isPerfOutPutTask) {
}

ServerRPC& ServerRPC::operator=(ServerRPC&& other) {
	startTime = std::move(other.startTime);
	clientID = std::move(other.clientID);
	toMonitor = std::move(other.toMonitor);
	isPerfOutPutTask = other.isPerfOutPutTask;
	BaseRPC::operator =(std::move(other));
	return *this;
}

ServerRPC::~ServerRPC() noexcept(false) {
}

void ServerRPC::Clear() {
	if (requestMessage != NULL) {
		requestMessage->clear_clientservicerpc();
	}
	if (responseMessage != NULL) {
		responseMessage->Clear();
	}
	isPerfOutPutTask = false;
	toMonitor = false;

}

} /* namespace RPC */
} /* namespace DTranx */

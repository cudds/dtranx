/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_CLIENTSERVICE_H_
#define DTRANX_CLIENTSERVICE_H_

#include <cassert>
#include <unordered_set>
#include "Service.h"
#include "MessageIDSet.h"
#include "AssignParticipantsStrategy.h"

namespace DTranx {
namespace Service {

class ClientService: public Service {
public:
	ClientService(SharedResources::TranxServerInfo *tranxServerInfo,
			SharedResources::TranxServiceSharedData *tranxServiceSharedData);
	virtual ~ClientService() noexcept(false);

	virtual void HandleRPC(RPC::ServerRPC *serverRPC);

	/*
	 * coreIDs for external threads
	 * one coreID for internal thread
	 */
	virtual void StartService(int numOfExServiceThreads, std::vector<uint32_t> coreIDs =
			std::vector<uint32_t>()) {
		/*
		 * external service threads will share physical cores while there's only one internal thread
		 */
		bool enableCoreBinding = false;
		uint32_t coreIDIndex = 0;
		assert(numOfExServiceThreads >= 1);
		enableCoreBinding = (coreIDs.size() - 1 == numOfExServiceThreads);

		for (int i = 0; i < numOfExServiceThreads; ++i) {
			serviceThread.push_back(boost::thread(&ClientService::ServiceThread, this, i));
			if (enableCoreBinding) {
				Util::ThreadHelper::PinToCPUCore(serviceThread[i], coreIDs[coreIDIndex++]);
			}
		}
		serviceInternalThread.push_back(boost::thread(&ClientService::ServiceInternalThread, this, 0));
		if (enableCoreBinding) {
			Util::ThreadHelper::PinToCPUCore(serviceInternalThread[0], coreIDs[coreIDIndex++]);
		}
	}

private:
	/*
	 * non shared data
	 */
	AssignParticipantsStrategy assignParticipantsStrategy;

	virtual void ServiceThread(int index);
	virtual void ServiceInternalThread(int index);
	/**************************************************************************************************************
	 * client service handlers from other stages
	 */
	void ClientCommit(Stages::TranxServiceQueueElement *element);

	// ClientService is non-copyable.
	ClientService(const ClientService&) = delete;
	ClientService& operator=(const ClientService&) = delete;
};

} /* namespace Service */
} /* namespace DTranx */

#endif /* DTRANX_CLIENTSERVICE_H_ */

#!/usr/bin/env python
'''
	The building/running environment is assumed to be ready and the nodes are accessible.
		It means the following software are installed: 
		OS: Ubuntu16.04.2(build), Ubuntu14.04.2(runtime)
		Software: python paramiko(build), g++-4.9(build), git(build, runtime), dh-autoreconf(build, runtime), build-essential(build, runtime), curl(build, runtime), scons 2.1.0(build)
		Library: 
			Statis: libboost(1.53.0), leveldb(77948e7), metis(5.1.0), logcabin
			Dynamic: zeromq 4.0.5(build), protobuf commit f473bb9(build), libpmem

	note: 
		1. assumption: logcabin project is cloned in the same directory as DTranx
		2. if running in openstack, virtual machines should be already running;
			if running in docker containers, this script automatically launch them up.
		3. run the EnvSetup script to format, mount, install dynamic library
'''

import argparse
import subprocess
import os
import sys
import shutil
import time
import paramiko
from Utility.SSHHelper import *
from Utility.BashHelper import *

#CONSTANTS
initializeEnvironmentCommand = "export LD_LIBRARY_PATH=/usr/local/lib/:$LD_LIBRARY_PATH"
	
def CreateIps(ips, filename):
	f = open(filename, 'w')
	for ip in ips:
		f.write(ip+"\n")
	f.close()

def GetIpsFromFile(filename):
	# handle relative/absolute path
	if len(filename) > 0 and filename.startswith('/'):
		ipsRealPath = filename
	else:
		workingPath = GetWorkingDir()
		ipsRealPath = workingPath + filename
	print "ip path is: " + ipsRealPath
	f = open(ipsRealPath,'r')
	ips = []
	for line in f:
		ips.append(line.strip())
	return ips, ipsRealPath

def AdjustConfigForDocker():
	# DTranx.conf is for normal test
	# DTranx.conf.repartition is for repartition test
	#
	runBash("cp DTranx/DTranx.conf.sample DTranx/DTranx.conf")
	runBash("cp DTranx/DTranx.conf.sample DTranx/DTranx.conf.repartition")
	AdjustConfig("maxIOThreads", "10", "DTranx/DTranx.conf")
	AdjustConfig("routerFrontPort", "30000", "DTranx/DTranx.conf")
	AdjustConfig("peerReceiverPort", "30002,30003", "DTranx/DTranx.conf")
	AdjustConfig("clientReceivePort", "30012,30013,30014", "DTranx/DTranx.conf")
	AdjustConfig("maxSockets", "200", "DTranx/DTranx.conf")
	AdjustConfig("PersistLogDir", "./", "DTranx/DTranx.conf")
	AdjustConfig("ServerMode", "docker", "DTranx/DTranx.conf")
	AdjustConfig("GCPollingPeriod", "500000", "DTranx/DTranx.conf")
	AdjustConfig("maxIOThreads", "10", "DTranx/DTranx.conf.repartition")
	AdjustConfig("maxSockets", "200", "DTranx/DTranx.conf.repartition")
	AdjustConfig("PersistLogDir", "./", "DTranx/DTranx.conf.repartition")
	AdjustConfig("ServerMode", "docker", "DTranx/DTranx.conf.repartition")
	AdjustConfig("RePartitionThreshold", "130", "DTranx/DTranx.conf.repartition")
	AdjustConfig("FileTruncate", "10", "DTranx/DTranx.conf.repartition")
	AdjustConfig("PartitionBroadcastThreshold", "50", "DTranx/DTranx.conf.repartition")
	AdjustConfig("LogOption", "Disk", "DTranx/DTranx.conf.repartition")
	AdjustConfig("RepartitionTranxFilter", "1", "DTranx/DTranx.conf.repartition")
	AdjustConfig("GCPollingPeriod", "500000", "DTranx/DTranx.conf.repartition")

'''
	peerReceiverPort and clientReceiverPort should be adjusted for different number of servers
'''
def AdjustConfigPort(numOfNodes):
	if numOfNodes == 3:
		AdjustConfig("peerReceiverPort", "30002,30003", "DTranx/DTranx.conf")
		AdjustConfig("clientReceivePort", "30012,30013,30014,30015,30016", "DTranx/DTranx.conf")
	elif numOfNodes == 9:
		AdjustConfig("peerReceiverPort", "30002,30003,30004,30005,30006,30007,30008,30009", "DTranx/DTranx.conf")
		AdjustConfig("clientReceivePort", "30012,30013,30014,30015,30016", "DTranx/DTranx.conf")
	elif numOfNodes == 18:
		AdjustConfig("peerReceiverPort", "30002,30003,30004,30005,30006,30007,30008,30009,30010,30011,30012,30013,\
			30014,30015,30016,30017,30018", "DTranx/DTranx.conf")
		AdjustConfig("clientReceivePort", "30024,30025,30026,30027,30028,30029", "DTranx/DTranx.conf")
	elif numOfNodes == 25:
		AdjustConfig("peerReceiverPort", "30002,30003,30004,30005,30006,30007,30008,30009,30010,30011,30012,30013,\
			30014,30015,30016,30017,30018,30019,30020,30021,30022,30023,30024,30025", "DTranx/DTranx.conf")
		AdjustConfig("clientReceivePort", "30026,30027,30028,30029,30030,30031", "DTranx/DTranx.conf")
	elif numOfNodes == 27:
		AdjustConfig("peerReceiverPort", "30002,30003,30004,30005,30006,30007,30008,30009,30010,30011,30012,30013,\
			30014,30015,30016,30017,30018,30019,30020,30021,30022,30023,30024,30025,30026,30027", "DTranx/DTranx.conf")
		AdjustConfig("clientReceivePort", "30028,30029,30030,30031,30032,30033", "DTranx/DTranx.conf")
	elif numOfNodes == 36:
		AdjustConfig("peerReceiverPort", "30002,30003,30004,30005,30006,30007,30008,30009,30010,30011,30012,30013,\
			30014,30015,30016,30017,30018,30019,30020,30021,30022,30023,30024,30025,30026,30027,30028,30029,30030,\
			30031,30032,30033,30034,30035,30036", "DTranx/DTranx.conf")
		AdjustConfig("clientReceivePort", "30037,30038,30039,30040,30041,30042", "DTranx/DTranx.conf")
	else:
		print "Deploy script should provide client/peer receiver port for ",numOfNodes," nodes of clusters."
		exit()

def AdjustConfigForVM(enablePerf, initDB, numOfNodes):
	if enablePerf == True:
		AdjustConfig("PerfLog", "1", "DTranx/DTranx.conf")
	else:
		AdjustConfig("PerfLog", "0", "DTranx/DTranx.conf")
	AdjustConfig("routerFrontPort", "30000", "DTranx/DTranx.conf")
	AdjustConfig("maxIOThreads", "1", "DTranx/DTranx.conf")
	AdjustConfig("maxSockets", "200", "DTranx/DTranx.conf")
	AdjustConfig("LogOption", "Pmem", "DTranx/DTranx.conf")
	AdjustConfig("PersistLogDir", "/mnt/mem/", "DTranx/DTranx.conf")
	AdjustConfig("FileTruncate", "100000000", "DTranx/DTranx.conf")	
	#the following used for non-pmem log test
	#AdjustConfig("LogOption", "Disk", "DTranx/DTranx.conf")
	#AdjustConfig("PersistLogDir", "/users/nigo9731/Test/", "DTranx/DTranx.conf")
	#AdjustConfig("FileTruncate", "500000", "DTranx/DTranx.conf")
	AdjustConfig("PartitionBroadcastThreshold", "100", "DTranx/DTranx.conf")
	AdjustConfig("ServerMode", "vm", "DTranx/DTranx.conf")
	AdjustConfig("GCPollingPeriod", "1000000", "DTranx/DTranx.conf")
	AdjustConfig("RepartitionTranxFilter", "50", "DTranx/DTranx.conf")
	#
	# these configurations need to be changed sometimes
	# for perf test initialization
	# for different cluster sizes
	AdjustConfig("InitializeDB", initDB, "DTranx/DTranx.conf")
	AdjustConfigPort(numOfNodes)
parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()
runtime_parser = subparsers.add_parser('run', help='prepare run environment')
runtime_parser.add_argument("-m","--mode",dest="mode",help="docker: launch docker machines; vm: run openstack/cloudlab machines", choices = ["docker", "vm"], required =True)
runtime_parser.add_argument("-c","--cluster",dest="cluster",help="cluster config file required for vm mode: ip list", nargs =1)
runtime_parser.add_argument("-p","--perflog", dest="perf", help="whether enable perf metric profiling/analysis or not for vm mode, default 0", choices = ["0", "1"])
runtime_parser.add_argument("-d","--initdb", dest="initDB", help="configure initial db, corresponding to InitializeDB, default 0")
runtime_parser.add_argument("-i","--image", dest="image", help="name of docker image to launch on", nargs=1)
runtime_parser.set_defaults(subcommand = 'runtime')

clean_parser = subparsers.add_parser('clean', help='clean running program')
clean_parser.add_argument("-m","--mode",dest="mode",help="docker: launch docker machines; vm: run openstack/cloudlab machines", choices = ["docker", "vm"], required =True)
clean_parser.add_argument("-c","--cluster",dest="cluster",help="cluster config file required for vm mode: ip list", nargs =1)
clean_parser.add_argument("-d","--cleandb", dest="cleanDocker", action="store_true", help="Whether to clean the database or not")
clean_parser.set_defaults(subcommand = 'clean')

args = parser.parse_args()
	
if args.subcommand == 'clean':
	if args.mode == 'vm':
		print "cleaning vm"
		# add /bin/bash, or else there's a bug in running the bash script
		bashcmd = "/bin/bash ./Scripts/Clean.sh -i " + args.cluster[0]
		if args.cleanDocker:
			bashcmd = bashcmd + " -d"
		print bashcmd
		print runBash(bashcmd)
	elif args.mode == 'docker':
		print "cleaning docker"
		cleanDockerBash = "\
		sudo docker kill dtranx1;\
		sudo docker kill dtranx2;\
		sudo docker kill dtranx3;\
		sudo docker rm dtranx1;\
		sudo docker rm dtranx2;\
		sudo docker rm dtranx3;"
		runBash(cleanDockerBash)
else:
	if args.mode == 'vm':
		print "vm mode"
		ips, ipsRealPath = GetIpsFromFile(args.cluster[0])
		os.chdir('../')
		AdjustConfig("storagePath", RemoteProjDir + "Logcabinlog", "logcabin/LogCabin.conf")
		######################
		# run logcabin service
		logcabinBash = "\
		cd logcabin;\
		./Deploy.py run -m prepare -c " + ipsRealPath +";\
		./Deploy.py run -m start -c " + ipsRealPath +";"
		runBash(logcabinBash)
		print "logcabin is running"
		########################
		# adjust configuration
		enablePerf = False
		if args.perf is not None:
			enablePerf = (args.perf == "1")
		initDB = "0"
		if args.initDB is not None:
			initDB = args.initDB
		AdjustConfigForVM(enablePerf, initDB, len(ips))
		#######################
		# generating server side ip address in LAN 
		baseIP = "192.168.0."
		index = 0
		localips = []
		for ip in ips:
			index = index + 1
			localips.append(baseIP + str(index))
		CreateIps(localips, "localips")
		#######################
		# run DDSBrick
		index = 0
		for ip in ips:
			index = index + 1
			print "processing "+ ip
			AdjustConfig("SelfAddress", baseIP + str(index), "DTranx/DTranx.conf")
			scp(True, "DTranx/DTranx.conf", RemoteUser, ip, RemoteProjDir)
			scp(True, "DTranx/Build/DTranx", RemoteUser, ip, RemoteProjDir)
			scp(True, "localips", RemoteUser, ip, RemoteIps)
			dtranxBash="cd " + RemoteProjDir + ";ulimit -c unlimited; export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib; ./"+ DTranx+" -i " + RemoteIps+ " -c " + RemoteDTranxConf
			runSSHCommandAsBackground(ip, RemoteUser, dtranxBash, RemoteDTranxOutput);
		runBash("rm localips");
		pass
	######################
	# run docker machines
	# TODO: replace directory with constants
	elif args.mode == 'docker':
		print "docker mode"	
		os.chdir('../')
		numOfNodes = 3
		AdjustConfig("storagePath", "/mnt", "logcabin/LogCabin.conf")
		for i in range(1,numOfNodes+1):
			dockerName = "dtranx" + str(i)
			os.system("sudo docker run --privileged --name="+dockerName+ " " +args.image[0] + " &")
			print "docker container "+dockerName+" is running"
			time.sleep(3)
			os.system("sudo docker cp logcabin/LogCabin.conf "+dockerName+":/dtranx/")
			os.system("sudo docker cp logcabin/build/LogCabin "+dockerName+":/dtranx/")
			os.system("sudo docker exec "+dockerName+" bash -c \'/dtranx/LogCabin &> /dtranx/LogCabin.out &\'")
		print "all logcabin's are running"
		ips = []
		AdjustConfigForDocker()

		for i in range(1,numOfNodes+1):
			dockerName = "dtranx" + str(i)
			dtranxBash="sudo docker inspect --format=\'{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}\' "+dockerName
			ip = runBash(dtranxBash)
			ip = ip.strip()
			ips.append(ip)
			AdjustConfig("SelfAddress", ip, "DTranx/DTranx.conf")
			AdjustConfig("SelfAddress", ip, "DTranx/DTranx.conf.repartition")
			dtranxBash="\
				sudo docker cp DTranx/DTranx.conf "+dockerName+":/dtranx/;\
				sudo docker cp DTranx/DTranx.conf.repartition "+dockerName+":/dtranx/;\
				sudo docker cp DTranx/Test/IntegTest/Data/test.data "+dockerName+":/dtranx/;\
				sudo docker cp DTranx/Build/DTranx "+dockerName+":/dtranx/;\
				sudo docker cp DTranx/Test/IntegTest/appfunc.cpp "+dockerName+":/dtranx/pin/source/tools/ManualExamples/;\
				sudo docker cp DTranx/Test/IntegTest/makefile.rules "+dockerName+":/dtranx/pin/source/tools/ManualExamples/;\
				sudo docker exec "+dockerName+" bash -c \'cd /dtranx/pin/source/tools/ManualExamples;make\';"
			runBash(dtranxBash)
		CreateIps(ips, "DTranx/ips_integ")
		for i in range(1,numOfNodes+1):
			dockerName = "dtranx" + str(i)
			dtranxBash="\
			sudo docker cp DTranx/ips_integ "+dockerName+":/dtranx/ips;\
			sudo docker exec "+dockerName+" bash -c \'/dtranx/DTranx -c /dtranx/DTranx.conf -i /dtranx/ips -t /dtranx/test.data &> /dtranx/DTranx.out &\'"
			runBash(dtranxBash)	
			print "dtranx in "+ dockerName+" is running"
		runBash("rm DTranx/ips_integ DTranx/DTranx.conf.repartition");
		print "all dtranx's are running"

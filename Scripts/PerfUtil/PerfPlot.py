import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def Plot(data, filename):
	plt.plot(data)
	plt.savefig(filename)
	plt.clf()
	data.sort()
	plt.plot(data)
	plt.savefig(filename+"_sorted")
	plt.clf()

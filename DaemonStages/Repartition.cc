/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "Repartition.h"
#include "DTranx/Util/Log.h"
#include "Util/FileUtil.h"
#include "MetisHelper.h"
#include "RPC/ClientRPC.h"

#include "Stages/Log/TranxLog.h"
#include "Stages/TranxRPCHelper.h"
#include "Stages/Lock/WriteBlockLockTable.h"
#include "Stages/Storage.h"
#include "Stages/TranxStates.h"
#include "TranxAck.h"
#include "Service/TranxService.h"

namespace DTranx {
namespace DaemonStages {

/*
 * Concurrency
 *	N: normal transaction
 *	R: repartition transaction
 *	D: database
 *	M: mapping database
 *	N-N contention do not write M, so lock and read check
 *	suffices to provide isolation.
 *	R-R contention won't happen, because repartition
 *	transaction is only initiated at the Node indexed at 0
 *	N-R contention requires that the mapping data to be checked
 *	just like data items. It's special that R-tranx requests for
 *	exclusive locks but doesn't check mapping data because N-tranx
 *	never changes. For N-tranx, read items and write items can be 
 *	checked in different ways
 * 		read items: If the mapping for the read data items changes,
 * 		it doesn't necessarily affect the commit result. If the new 
 *		mapping still contains this node, it doesn't matter because 
 *		reading only locks one copy and it did already. If the new 
 *		mapping doesn't contain this node any more, this transaction
 *		might abort because of lock contention with R-tranx, thus 
 *		totally fine. If this transaction requests
 *		for locks any time after the R-tranx releases, the read check
 *		in storage stage will return failure, thus aborting the N-tranx.
 *		Whichever case it is when the new mapping doesn't contain the node
 *		any more, the N-Tranx will abort in case of R-Tranx.
 *			
 * 		write items: no matter whether the new mapping contains this
 *		node or not, N-Tranx needs to check the mapping data for the 
 *		write data item to avoid inconsistency among replications.
 *		write item mapping data should be checked in coordinator when
 *		all locks in participants are also acquired and
 *		participants do not need to check because mapping data is a read
 *		item in coordinator. If the mapping data doesn't change in coordinator,
 *		it will not in participants either since changing mapping data requires
 *		write locks on all servers.
 */

Repartition::Repartition(SharedResources::TranxServerInfo *tranxServerInfo,
		SharedResources::TranxServiceSharedData *tranxServiceSharedData)
		: DaemonStages(tranxServerInfo, tranxServiceSharedData, "repartition"), repartitionLProcessQueue(
				1000), repartitionCommitHistoryQueue(1000), ManualTriggerRepartition(
		false) {
	committedTranxFilter = tranxServerInfo->GetConfig().read<uint32_t>("RepartitionTranxFilter");
}

Repartition::~Repartition() {
}

void Repartition::SendToLogRepartition(std::string logName) {
	while (!repartitionLProcessQueue.try_enqueue(logName)) {
		if (repartitionLProcessQueue.enqueue(logName)) {
			break;
		}
	}
}

void Repartition::SendToCommitHistory(const Util::TranxID& tranxID) {
	assert(repartitionCommitHistoryQueue.enqueue(tranxID));
}

void Repartition::DaemonThread() {
	NOTICE("RepartitionThread started");
	uint64_t randSeed = 0;
	std::vector<std::string> allNodes = tranxServerInfo->GetAllNodes();
	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	Util::TranxID tmpCommitHistory[Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
	size_t count_commithistory = 0;
	std::string tmpLogs[Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
	size_t count_logs = 0;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateThread.load()) {
			NOTICE("RepartitionThread is reclaimed");
			break;
		}
		Stages::TranxServiceQueueElement *tmpRPCs[Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = daemonProcessQueue.try_dequeue_bulk(tmpRPCs,
				Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE);

		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1) {
				AddQueueLengthSample(0, daemonProcessQueue.size_approx());
			}
		}

		if (count == 0) {
			count_commithistory = repartitionCommitHistoryQueue.try_dequeue_bulk(tmpCommitHistory,
					Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
			for (int i = 0; i < count_commithistory; ++i) {
				localCommitHistorySet.insert(tmpCommitHistory[i]);
			}
			count_logs = repartitionLProcessQueue.try_dequeue_bulk(tmpLogs,
					Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
			for (int i = 0; i < count_logs; ++i) {
				ProcessLog(tmpLogs[i]);
			}
		} else {
			for (int i = 0; i < count; ++i) {
				Stages::TranxServiceQueueElement *element = tmpRPCs[i];
				if (element->isIsPerfOutPutTask()) {
					std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(0);
					std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(0);
					std::vector<int> taskLongLatencySamples = GetLongLatency(0);
					Util::ProfileLog profileLog;
					profileLog.set_type(Util::ProfileType::PERF);
					profileLog.mutable_perflog()->set_stagename(stageName);
					for (auto responseTimeSample = responseTimeSamples.begin();
							responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
						profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
					}
					for (auto queueLengthSample = queueLengthSamples.begin();
							queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
						profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
					}
					for (auto taskLongLatencySample = taskLongLatencySamples.begin();
							taskLongLatencySample != taskLongLatencySamples.end();
							++taskLongLatencySample) {
						profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
					}
					PROFILE(profileLog.SerializeAsString());
					delete element;
					continue;
				}
				//element->goThroughStage("repartition");
				ProcessRepartition(element);
			}
		}
		if (ManualTriggerRepartition.load()) {
			ManualTriggerRepartition.store(false);
			std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>>tranxList=tranxServiceSharedData->TranxHistoryForceRepartition();
			if (!tranxList.empty()) {
				RepartitionPrepare(tranxList);
			} else {
				VERBOSE(
						"manully trigger repartition, however, there's no committed transactions in the past");
			}
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void Repartition::ProcessLog(std::string logName) {
	std::vector<std::string> allNodes = tranxServerInfo->GetAllNodes();
	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	Stages::Log::TranxLog tmpTranxLog(logName, tranxServerInfo);
	std::unordered_set<Util::TranxID, Util::KeyHasher> toRemove;
	uint64_t randSeed = 0;
	while (true) {
		Stages::Log::GTranxLogRecord entry;
		try {
			entry = tmpTranxLog.OneReadNext();
			if (entry.has_tranxtype() && entry.tranxtype() == Service::GTranxType::SNAPSHOT) {
				continue;
			}
			if (entry.has_tranxtype() && entry.tranxtype() == Service::GTranxType::REPARTITION) {
				continue;
			}
			Util::TranxID tranxID;
			tranxID.SetNodeID(entry.tranxid().nodeid());
			tranxID.SetTranxID(entry.tranxid().tranxid());
			toRemove.insert(tranxID);
			if (randSeed++ % committedTranxFilter == 0) {
				/*
				 * sampling the successful transactions to reduce overhead
				 */
				if (entry.logtype() == Stages::Log::GLogType::PREP
						&& localCommitHistorySet.find(tranxID) != localCommitHistorySet.end()) {
					std::unordered_set<std::string> readset, writeset;
					for (auto key = entry.read_set().begin(); key != entry.read_set().end();
							++key) {
						readset.insert(key->key());
					}
					for (auto key = entry.write_set().begin(); key != entry.write_set().end();
							++key) {
						writeset.insert(key->key());
					}
					tranxServiceSharedData->AddTranxHistory(readset, writeset);
				}
			}
		} catch (Util::NotFoundException &e) {
			break;
		}
	}
	Util::FileUtil::RemoveFile(logName);
	for (auto it = toRemove.begin(); it != toRemove.end(); ++it) {
		localCommitHistorySet.erase(*it);
	}
	/*
	 * Broadcast tranxHistory to master node, or run repartition in the master node
	 */
	if (allNodes[0] != selfAddress) {
		RPC::TranxRPC* tranxRPC = tranxServiceSharedData->TranxHistoryBroadcast();
		if (tranxRPC) {
			tranxRPCHelper->SendNoResponse(allNodes[0], tranxRPC);
		}
	} else {
		std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>>tranxList =
		tranxServiceSharedData->TranxHistoryRepartition();
		if (!tranxList.empty()) {
			RepartitionPrepare(tranxList);
		}
	}
}

void Repartition::ProcessRepartition(Stages::TranxServiceQueueElement *element) {
	int stageNum = element->getStageNum();
	if (stageNum == 1) {
		Stages::State state = element->getState();
		if (state != Stages::State::GC) {
			VERBOSE("previous repartitioning tranx is not complete yet");
			delete element;
			return;
		} else {
			Util::TranxID tranxID = tranxServiceSharedData->GenerateTranxID();
			std::unordered_map<std::string, std::unordered_set<std::string> > newMapping =
					element->getNewMapping();
			repartitionTranxID = tranxID;
			VERBOSE("TranxService starts a new transaction with ID for repartitioning: %lu",
					tranxID.GetTranxID());
			std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
			for (auto hashkey = newMapping.begin(); hashkey != newMapping.end(); ++hashkey) {
				std::string key = hashkey->first;
				keysMode[key] = Stages::Lock::BasicLockTableLockMode::Exclusive;
			}
			element->setKeysMode(keysMode);
			element->setStageNum(2);
			element->SetTranxID(tranxID);
			element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSBLOCKING);
			lockTable->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		}
	} else if (stageNum == 2) {
		/*
		 * local lock obtained
		 */
		VERBOSE("writing prepare log");
		element->setLogTask(Stages::Log::TranxLogStageTask::_PrepareRepartition);
		element->setStageNum(3);
		tranxLog->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else if (stageNum == 3) {
		/*
		 * prepare log written
		 */
		VERBOSE("sending prep and poll");
		element->setStageNum(4);
		element->setTranxRpcTask(Stages::TranxRPCHelperStageTask::_SENDPREPANDPOLLREPARTITION);
		tranxRPCHelper->SendToStage(element,
				Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else if (stageNum == 4) {
		/*
		 * prepare request sent, start to migrate
		 */
		assert(element->getTranxRpcResult() == Stages::TranxRPCHelperPollingResult::AllOK);
		VERBOSE("start to migrate for local data");
		MigrateDBInitial(element);
	} else if (stageNum == 5) {
		/*
		 * storage search returned
		 */
		MigrateDBClientSend(element);
	} else if (stageNum == 6) {
		/*
		 * after data are migrated, mapping data should also be updated
		 */
		std::unordered_map<std::string, std::unordered_set<std::string> > newMappings =
				element->getNewMapping();
		SharedResources::MappingDS *mappingStorage = tranxServiceSharedData->getMappingStorage();
		for (auto it = newMappings.begin(); it != newMappings.end(); ++it) {
			mappingStorage->UpdateLookupTable(it->first, it->second);
		}
		/*
		 * local migrateDB complete
		 */
		if (element->getOldStageId() == Stages::TranxServiceQueueElement::StageID::TRANXSERVICE) {
			element->setStageId(Stages::TranxServiceQueueElement::StageID::TRANXSERVICE);
			element->setOldStageId(Stages::TranxServiceQueueElement::StageID::INVALID);
			element->setStageNum(element->getOldStageNum());
			tranxService->SendToServiceInternal(element);
		} else {
			VERBOSE("sending migrate message");
			element->setStageNum(7);
			element->setTranxRpcTask(Stages::TranxRPCHelperStageTask::_SENDMIGRATEANDPOLL);
			tranxRPCHelper->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		}
	} else if (stageNum == 7) {
		/*
		 * migrateDB message sent
		 */
		VERBOSE("writing commit log");
		element->setStageNum(8);
		element->setLogType(Stages::Log::GLogType::COMMIT);
		element->setLogTask(Stages::Log::TranxLogStageTask::_Commit);
		tranxLog->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
	} else if (stageNum == 8) {
		/*
		 * commit log written
		 */
		element->setStageNum(9);
		element->setStateTask(Stages::TranxStatesStageTask::_SetToCommitState);
		tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
				ptok_tranxStates);
	} else if (stageNum == 9) {
		/*
		 * tranxStates updated
		 */
		element->setStageNum(10);
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSMAP);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
	} else if (stageNum == 10) {
		element->setAckCommitAbort(true);
		element->setStageId(Stages::TranxServiceQueueElement::StageID::TRANXACK);
		element->setStageNum(1);
		tranxAck->SendToDaemon(element);
	} else if (stageNum == 11) {
		/*
		 * transaction service migrate request
		 */
		VERBOSE("tranxservice: migrate starts to migrate for local data");
		MigrateDBInitial(element);
	} else {
		assert(false);
	}

}

void Repartition::RepartitionPrepare(
		std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>>& tranxList) {
	std::unordered_map<std::string, std::unordered_set<int> > newMappings;
	MetisHelper metisHelper(true);
	std::unordered_map<std::string, int> keysFrequency;
	int totalTranx = 0;
	for (auto it = tranxList.begin(); it != tranxList.end(); ++it) {
		metisHelper.AddTranx(it->first, it->second);
		totalTranx++;
		for (auto key = it->first.begin(); key != it->first.end(); ++key) {
			if (keysFrequency.find(*key) != keysFrequency.end()) {
				keysFrequency[*key]++;
			} else {
				keysFrequency[*key] = 1;
			}
		}
		for (auto key = it->second.begin(); key != it->second.end(); ++key) {
			if (keysFrequency.find(*key) != keysFrequency.end()) {
				keysFrequency[*key]++;
			} else {
				keysFrequency[*key] = 1;
			}
		}
	}
	VERBOSE("TranxHistory has %d transactions", totalTranx);
	/*
	 * Run metis
	 */
	std::vector<std::string> allNodes = tranxServerInfo->GetAllNodes();
	if (allNodes.size() > 1 && totalTranx > 0) {
		VERBOSE("Start running METIS");
		metisHelper.RunMetis(allNodes.size());
		metisHelper.GetNewMappingsR(newMappings);
		VERBOSE("Start transition to new mapping");
		MappingTransition(newMappings, keysFrequency, totalTranx);
	}
}

void Repartition::MappingTransition(
		std::unordered_map<std::string, std::unordered_set<int> >& _newMappings,
		std::unordered_map<std::string, int>& keysFrequency, int totalTranx) {
	//potential bug: when the old mapping is read here and migration tranx is not initiated, the next repartitioning request might change the mapping already
	//1. filter the data to migrate and generate mapping change
	double partitionKeysFilter = tranxServerInfo->GetConfig().read<double>("PartitionKeysFilter");
	SharedResources::MappingDS *mappingStorage = tranxServiceSharedData->getMappingStorage();
	uint64 adjustedPartitionKeysFilter = uint64(partitionKeysFilter * totalTranx);
	std::vector<std::string> allNodes = tranxServerInfo->GetAllNodes();
	for (auto it = keysFrequency.begin(); it != keysFrequency.end(); ++it) {
		if (it->second <= adjustedPartitionKeysFilter) {
			_newMappings.erase(it->first);
		}
	}
	std::unordered_map<uint64, std::unordered_set<int> > oldMappings, newMappings;

	std::unordered_map<uint64, std::string> hashToKey;
	std::unordered_map<std::string, int> ipToIndex;

	/*
	 * 2. prepare old mapping data
	 * map ip to old index of the node
	 */
	int index = 0;
	for (auto ip = allNodes.begin(); ip != allNodes.end(); ++ip) {
		ipToIndex[*ip] = index++;
	}

	for (auto it = _newMappings.begin(); it != _newMappings.end(); ++it) {
		uint64 hashedKey = Util::StringUtil::SDBMHash(it->first.c_str());
		hashToKey[hashedKey] = it->first;
		std::unordered_set<std::string> nodes;
		mappingStorage->GetMapping(it->first, nodes);
		oldMappings[hashedKey] = std::unordered_set<int>();
		std::unordered_set<int>& oldNodes = oldMappings[hashedKey];
		for (auto ip = nodes.begin(); ip != nodes.end(); ++ip) {
			assert(ipToIndex.find(*ip) != ipToIndex.end());
			oldNodes.insert(ipToIndex[*ip]);
		}
		newMappings[hashedKey] = it->second;
	}
	/*
	 * generate ips for the partition ID's from metis algorithm
	 * partitioningMapping: a mapping from the new partition ID to old partition ID
	 */
	std::unordered_map<int, int> partitioningMapping = OptimizeForDataMigration(oldMappings,
			newMappings);
	/*
	 std::cout<<"partition ID mapping result: "<<std::endl;
	 for(auto it = partitioningMapping.begin(); it != partitioningMapping.end(); ++it){
	 std::cout<<"new server"<<it->first<<" maps to old server"<<it->second<<std::endl;
	 }
	 */
	newMappings.clear();
	for (auto it = _newMappings.begin(); it != _newMappings.end(); ++it) {
		uint64 hashedKey = Util::StringUtil::SDBMHash(it->first.c_str());
		for (auto newServerID = it->second.begin(); newServerID != it->second.end();
				++newServerID) {
			newMappings[hashedKey].insert(partitioningMapping[*newServerID]);
		}
	}
	//filter out the ones that has not changed
	std::unordered_map<std::string, std::unordered_set<std::string> > newMapping_filtered;
	for (auto it = newMappings.begin(); it != newMappings.end(); ++it) {
		if (it->second != oldMappings[it->first]) {
			for (auto serverID_old = it->second.begin(); serverID_old != it->second.end();
					++serverID_old) {
				newMapping_filtered[hashToKey[it->first]].insert(allNodes[*serverID_old]);
			}
		}
	}

	for (auto it = newMapping_filtered.begin(); it != newMapping_filtered.end(); ++it) {
		std::cout << "key: " << it->first << std::endl;
		std::cout << "oldmapping: ";
		std::unordered_set<std::string> nodes;
		mappingStorage->GetMapping(it->first, nodes);
		for (auto ip = nodes.begin(); ip != nodes.end(); ++ip) {
			std::cout << *ip << " ";
		}
		std::cout << std::endl;
		std::cout << "newmapping: ";
		for (auto ip = it->second.begin(); ip != it->second.end(); ++ip) {
			std::cout << *ip << " ";
		}
		std::cout << std::endl;
	}

	//when no new mapping generated, simply return
	if (newMapping_filtered.empty()) {
		return;
	}
	/*
	 * avoid concurrent repartition transaction, the previous one should be at least ack'ed
	 */
	Stages::TranxServiceQueueElement *element = new Stages::TranxServiceQueueElement();
	//element->goThroughStage("repartition");
	element->setTranxType(Service::GTranxType::REPARTITION);
	/*
	 * Even though not every node is involved in the repartitioning
	 * where data needs to be migrated from or to,
	 * Still every node should be included to guarantee atomic
	 * change of mapping data
	 */
	element->setIps(tranxServerInfo->GetAllNodes());
	element->setStageId(Stages::TranxServiceQueueElement::StageID::REPARTITION);
	element->setNewMapping(newMapping_filtered);

	if (!repartitionTranxID.empty()) {
		element->setStageNum(1);
		element->setStateTask(Stages::TranxStatesStageTask::_CheckState);
		element->SetTranxID(repartitionTranxID);
		tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
				ptok_tranxStates);
		return;
	} else {
		Util::TranxID tranxID = tranxServiceSharedData->GenerateTranxID();
		repartitionTranxID = tranxID;
		VERBOSE("TranxService starts a new transaction with ID for repartitioning: %lu",
				tranxID.GetTranxID());
		std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
		for (auto hashkey = newMapping_filtered.begin(); hashkey != newMapping_filtered.end();
				++hashkey) {
			std::string key = hashkey->first;
			keysMode[key] = Stages::Lock::BasicLockTableLockMode::Exclusive;
		}
		element->setKeysMode(keysMode);
		element->setStageNum(2);
		element->SetTranxID(tranxID);
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSBLOCKING);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	}
}

std::unordered_map<int, int> Repartition::OptimizeForDataMigration(
		std::unordered_map<uint64, std::unordered_set<int> >& oldMappings,
		std::unordered_map<uint64, std::unordered_set<int> >& newMappings) {
	//MaybeLater: different iterative function, most frequent in oldMappings/newMappings
	/*
	 * partitioningMapping: newserver-> oldserver
	 */
	std::unordered_map<int, int> partitioningMapping;
	/*
	 * generate a bag of keys for each server
	 * serverBags: serverID-> unordered_set<key>
	 * serverFreq_old: serverID -> frequency
	 */
	std::unordered_map<int, std::unordered_set<uint64> > serverBags;
	std::unordered_map<int, int> serverFreq_old;
	for (auto key = oldMappings.begin(); key != oldMappings.end(); ++key) {
		for (auto server = key->second.begin(); server != key->second.end(); ++server) {
			if (serverBags.find(*server) == serverBags.end()) {
				serverBags[*server] = std::unordered_set<uint64>();
			}
			serverBags[*server].insert(key->first);
			if (serverFreq_old.find(*server) != serverFreq_old.end()) {
				serverFreq_old[*server] = 1;
			} else {
				serverFreq_old[*server] = serverFreq_old[*server] + 1;
			}
		}
	}
	/*
	 * serverFreq_ordered: pair<frequency, serverid>
	 */
	std::set<std::pair<int, int> > serverFreq_old_ordered;
	for (auto it = serverFreq_old.begin(); it != serverFreq_old.end(); ++it) {
		serverFreq_old_ordered.insert(std::make_pair(it->second, it->first));
	}

	/*
	 for (auto it = serverBags.begin(); it != serverBags.end(); ++it) {
	 std::cout << "server: " << it->first << std::endl;
	 for (auto it_b = it->second.begin(); it_b != it->second.end(); ++it_b) {
	 std::cout << *it_b << " ";
	 }
	 std::cout << std::endl;
	 }
	 std::cout << "ordered servers: " << std::endl;
	 for (auto it = serverFreq_old_ordered.rbegin(); it != serverFreq_old_ordered.rend(); ++it) {
	 std::cout << it->second << " ";
	 }
	 std::cout << std::endl;
	 */

	/*
	 * generating partitioning mapping for each server in each iteration
	 */
	for (auto it = serverFreq_old_ordered.rbegin(); it != serverFreq_old_ordered.rend(); ++it) {
		int serverID_old = it->second;
		std::unordered_set<uint64> &allKeys = serverBags[serverID_old];

		/*
		 * find the server that contains most of the keys in the original mapping
		 * serverFreq_new: serverID -> frequency
		 */
		std::unordered_map<int, int> serverFreq_new;
		for (auto key = allKeys.begin(); key != allKeys.end(); ++key) {
			for (auto serverID_new = newMappings[*key].begin();
					serverID_new != newMappings[*key].end(); ++serverID_new) {
				if (partitioningMapping.find(*serverID_new) == partitioningMapping.end()) {
					if (serverFreq_new.find(*serverID_new) == serverFreq_new.end()) {
						serverFreq_new[*serverID_new] = 1;
					} else {
						serverFreq_new[*serverID_new] = serverFreq_new[*serverID_new] + 1;
					}
				}
			}
		}
		/*
		 * serverFreq_new_ordered: pair<frequency, serverid>
		 */
		std::set<std::pair<int, int> > serverFreq_new_ordered;
		for (auto it = serverFreq_new.begin(); it != serverFreq_new.end(); ++it) {
			serverFreq_new_ordered.insert(std::make_pair(it->second, it->first));
		}
		/*
		 std::cout<<"ordered new servers for old server"<<serverID_old<<std::endl;
		 for(auto it = serverFreq_new_ordered.rbegin(); it != serverFreq_new_ordered.rend(); ++it){
		 std::cout<<it->second<<" ";
		 }
		 std::cout << std::endl;
		 std::cout<<"old server"<<serverID_old<<" maps to new server"<<serverFreq_new_ordered.rbegin()->second<<std::endl;
		 */
		/*
		 * it could be that the servers in the new mapping for the keys are already mapped,
		 * randomly pair them at the end
		 */
		if (!serverFreq_new_ordered.empty()) {
			partitioningMapping[serverFreq_new_ordered.rbegin()->second] = serverID_old;
		}
	}
	int oldServerNum = tranxServerInfo->GetAllNodes().size();
	std::unordered_set<int> newServers;
	for (auto key = newMappings.begin(); key != newMappings.end(); ++key) {
		for (auto server = key->second.begin(); server != key->second.end(); ++server) {
			newServers.insert(*server);
		}
	}
	for (auto it = newServers.begin(); it != newServers.end(); ++it) {
		if (partitioningMapping.find(*it) == partitioningMapping.end()) {
			partitioningMapping[*it] = rand() % oldServerNum;
		}
	}
	return partitioningMapping;
}

void Repartition::MigrateDBInitial(Stages::TranxServiceQueueElement *element) {
	std::unordered_map<std::string, std::unordered_set<std::string> > newMappings =
			element->getNewMapping();
	SharedResources::MappingDS *mappingStorage = tranxServiceSharedData->getMappingStorage();

	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	Util::ConfigHelper config = tranxServerInfo->GetConfig();
	std::shared_ptr<zmq::context_t> context = tranxServerInfo->GetContext();
	for (auto it = newMappings.begin(); it != newMappings.end(); ++it) {
		for (auto ip = it->second.begin(); ip != it->second.end(); ++ip) {
			if (*ip != selfAddress) {
				allClientsIps.insert(*ip);
			}
		}
	}
	//TODO: clean allClientsIps after repartition transaction
	clientItr = allClientsIps.begin();
	if (clientItr != allClientsIps.end()) {
		//VERBOSE("in coord, check for %s", clientItr->c_str());
		std::vector<std::string> searchKeys;
		for (auto it = newMappings.begin(); it != newMappings.end(); ++it) {
			std::unordered_set<std::string> oldMapping;
			mappingStorage->GetMapping(it->first, oldMapping);
			std::set<std::string> oldMappingSorted;
			for (auto ip = oldMapping.begin(); ip != oldMapping.end(); ++ip) {
				oldMappingSorted.insert(*ip);
			}
			assert(!oldMappingSorted.empty());
			/*
			 * if the first node in old mapping is this server, the key is new in newmapping, copy to it
			 */
			if (oldMapping.find(*clientItr) == oldMapping.end()
					&& it->second.find(*clientItr) != it->second.end()
					&& selfAddress == *oldMappingSorted.begin()) {
				searchKeys.push_back(it->first);
				//VERBOSE("searchkey %s", it->first.c_str());
			}
		}
		element->setStageNum(5);
		element->setStorageTask(Stages::StorageStageTask::_SearchAll);
		element->setStorageKeys(searchKeys);
		localStorage->SendToStage(element,
				Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
	} else {
		MigrateDBLocalDelete(element);
	}

}

void Repartition::MigrateDBClientSend(Stages::TranxServiceQueueElement *element) {
	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	Util::ConfigHelper config = tranxServerInfo->GetConfig();
	uint32_t bindPort;
	std::shared_ptr<zmq::context_t> context = tranxServerInfo->GetContext();
	std::vector<std::vector<std::pair<std::string, std::string>>>searchAllResults = element->getStorageSearchAllResults();
	if (clients.find(*clientItr) == clients.end()) {
		clients[*clientItr] = new Client::Client(*clientItr,
				std::stoul(config.read("routerFrontPort").c_str(), nullptr, 10), context);
		std::vector<std::string> clientPortsStr = Util::StringUtil::Split(
				config.read<std::string>("clientReceivePort"), ',');
		std::vector<uint32_t> clientPorts;
		for (auto port = clientPortsStr.begin(); port != clientPortsStr.end(); ++port) {
			clientPorts.push_back(std::strtoul(port->c_str(), nullptr, 10));
		}

		std::string selfAddress = config.read<std::string>("SelfAddress");
		int clientPortsNum = clientPorts.size();
		int currentClientPort = 0;
		assert(currentClientPort < clientPortsNum);
		while (!clients[*clientItr]->Bind(selfAddress, clientPorts[currentClientPort])) {
			currentClientPort = (currentClientPort + 1) % clientPortsNum;
		}
		bindPort = clientPorts[currentClientPort];
	}
	RPC::ClientRPC* clientRPC = new RPC::ClientRPC(RPC::GServiceID::StorageService,
			clients[*clientItr]);
	clientRPC->SetStorageOpcode(Service::GOpCode::WRITE_DATA);
	uint32_t selfAddressInt[4];
	std::vector<std::string> tmpFields = Util::StringUtil::Split(selfAddress, '.');
	for (int i = 0; i < 4; ++i) {
		selfAddressInt[i] = std::stoul(tmpFields[i], nullptr, 10);
	}
	clientRPC->SetReqIP(selfAddressInt);
	clientRPC->SetReqPort(bindPort);
	Service::GWriteData::Request* request =
			clientRPC->GetStorageRequest()->mutable_writedata()->mutable_request();
	for (int i = 0; i < searchAllResults.size(); ++i) {
		for (auto data = searchAllResults[i].begin(); data != searchAllResults[i].end(); ++data) {
			Service::GItem *item = request->add_items();
			//VERBOSE("found key %s, value: %s", data->first.c_str(), data->second.c_str());
			item->set_key(data->first);
			item->set_value(data->second);
		}
	}
	//VERBOSE("searchAllResults size %zu", searchAllResults.size());
	assert(searchAllResults.size() == element->getStorageKeys().size());
	if (request->items_size() != 0) {
		clients[*clientItr]->CallRPC(clientRPC);
		assert(clients[*clientItr]->BlockPoll(clientRPC));
		Service::GWriteData::Response* response =
				clientRPC->GetStorageResponse()->mutable_writedata()->mutable_response();
		assert(response->status() == Service::GStatus::OK);
	}
	delete clientRPC;
	delete clients[*clientItr];

	clientItr++;
	if (clientItr != allClientsIps.end()) {
		//VERBOSE("in coord, check for %s", clientItr->c_str());
		std::unordered_map<std::string, std::unordered_set<std::string> > newMappings =
				element->getNewMapping();
		element->clearStorageSearchAllResults();
		SharedResources::MappingDS *mappingStorage = tranxServiceSharedData->getMappingStorage();
		std::vector<std::string> searchKeys;
		for (auto it = newMappings.begin(); it != newMappings.end(); ++it) {
			std::unordered_set<std::string> oldMapping;
			mappingStorage->GetMapping(it->first, oldMapping);
			std::set<std::string> oldMappingSorted;
			for (auto ip = oldMapping.begin(); ip != oldMapping.end(); ++ip) {
				oldMappingSorted.insert(*ip);
			}
			assert(!oldMappingSorted.empty());
			/*
			 * if the first node in old mapping is this server, the key is new in newmapping, copy to it
			 */
			if (oldMapping.find(*clientItr) == oldMapping.end()
					&& it->second.find(*clientItr) != it->second.end()
					&& selfAddress == *oldMappingSorted.begin()) {
				searchKeys.push_back(it->first);
				//VERBOSE("searchkey %s", it->first.c_str());
			}
		}
		element->setStageNum(5);
		element->setStorageTask(Stages::StorageStageTask::_SearchAll);
		element->setStorageKeys(searchKeys);
		localStorage->SendToStage(element,
				Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
	} else {
		MigrateDBLocalDelete(element);
	}
}

void Repartition::MigrateDBLocalDelete(Stages::TranxServiceQueueElement *element) {
	allClientsIps.clear();
	clients.clear();
	std::unordered_map<std::string, std::unordered_set<std::string> > newMappings =
			element->getNewMapping();
	SharedResources::MappingDS *mappingStorage = tranxServiceSharedData->getMappingStorage();
	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	std::vector<std::string> deleteKeys;
	for (auto it = newMappings.begin(); it != newMappings.end(); ++it) {
		std::unordered_set<std::string> oldMapping;
		mappingStorage->GetMapping(it->first, oldMapping);
		//if the data was not in this server for old mapping, simply ignore
		if (oldMapping.find(selfAddress) == oldMapping.end()) {
			continue;
		} //whether this server is the first or not but if it's not in the new mapping, delete it in the storage.
		if (it->second.find(selfAddress) == it->second.end()) {
			VERBOSE("deleting data %s", it->first.c_str());
			deleteKeys.push_back(it->first);
		}
	}
	element->setStageNum(6);
	element->setStorageTask(Stages::StorageStageTask::_Delete);
	element->setStorageKeys(deleteKeys);
	localStorage->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
}

}
/* namespace DaemonStages */
} /* namespace DTranx */

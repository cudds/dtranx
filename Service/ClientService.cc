/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "ClientService.h"
#include "DTranx/Util/Log.h"

#include "Stages/Lock/WriteBlockLockTable.h"
#include "Stages/Log/TranxLog.h"
#include "Stages/TranxStates.h"
#include "Stages/TranxRPCHelper.h"
#include "DaemonStages/TranxAck.h"
#include "Stages/Storage.h"
#include "Server/Server.h"

namespace DTranx {
namespace Service {

ClientService::ClientService(SharedResources::TranxServerInfo *tranxServerInfo,
		SharedResources::TranxServiceSharedData *tranxServiceSharedData)
		: Service(tranxServerInfo, tranxServiceSharedData, "clientservice"), assignParticipantsStrategy(
				tranxServerInfo->GetSelfAddress(), tranxServiceSharedData) {
}

ClientService::~ClientService() noexcept(false) {
}

void ClientService::ServiceThread(int index) {
	NOTICE("ClientServiceThread started");
	uint64_t randSeed = 0;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateThread.load()) {
			NOTICE("clientServiceThread is reclaimed");
			break;
		}
		RPC::ServerRPC *tmpRPCs[Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = serviceProcessQueue.try_dequeue_bulk(tmpRPCs,
				Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1 && index == 0) {
				/*
				 * use index here to avoid race condition on queue length samples
				 */
				AddQueueLengthSample(6, serviceProcessQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			RPC::ServerRPC *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(6);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(6);
				std::vector<int> taskLongLatencySamples = GetLongLatency(6);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName);
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				server->AddToFreeList(element);
				Stages::TranxServiceQueueElement *tmpElement =
						new Stages::TranxServiceQueueElement();
				tmpElement->setIsPerfOutPutTask(true);
				SendToServiceInternal(tmpElement);
				continue;
			}
			HandleRPC(element);
		}

		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void ClientService::ServiceInternalThread(int index) {
	NOTICE("ClientServiceInternalThread started");
	uint64_t randSeed = 0;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateInternalThread.load()) {
			NOTICE("ClientServiceInternalThread is reclaimed");
			break;
		}
		Stages::TranxServiceQueueElement *tmpRPCs[Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = serviceInternalProcessQueue.try_dequeue_bulk(tmpRPCs,
				Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1 && index == 0) {
				/*
				 * use index here to avoid race condition on queue length samples
				 */
				AddQueueLengthSample(7, serviceInternalProcessQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			Stages::TranxServiceQueueElement *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(7);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(7);
				std::vector<int> taskLongLatencySamples = GetLongLatency(7);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_Internal");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				AddToFreeList(element);
				continue;
			}
			//element->goThroughStage("ClientService");
			ClientCommit(element);
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void ClientService::HandleRPC(RPC::ServerRPC *serverRPC) {
	/*
	 * ClientCommit handles all transactions except repartition tranx
	 */
	Stages::TranxServiceQueueElement *element;
	if (!freeList.try_dequeue(element)) {
		element = new Stages::TranxServiceQueueElement();
	}
	//element->goThroughStage("ClientService");
	element->setStageId(Stages::TranxServiceQueueElement::CLIENTSERVICE);
	element->SetServerRPC(serverRPC);

	GClientCommit::Request *request =
			serverRPC->GetClientRequest()->mutable_clientcommit()->mutable_request();
	GClientCommit::Response *response =
			serverRPC->GetClientResponse()->mutable_clientcommit()->mutable_response();

	response->set_status(GStatus::FAIL);
	GTranxType tranxType = request->tranxtype();
	element->setTranxType(tranxType);
	bool snapshotCreation = tranxType == GTranxType::SNAPSHOT;
	std::string selfAddress = tranxServerInfo->GetSelfAddress();

	/*
	 * mapping the readset/writeset to its node
	 * also get the mappings for write items
	 */
	std::unordered_map<std::string, std::unordered_set<std::string>> writeItemMapping;
	std::unordered_map<std::string, std::unordered_set<std::string>> readItemMapping;
	readItemMapping.reserve(10);
	writeItemMapping.reserve(10);
	std::unordered_map<std::string, std::pair<std::vector<GItem>, std::vector<GItem>> > items =
			assignParticipantsStrategy.MapItem(request->read_set(), request->write_set(),
					&readItemMapping, &writeItemMapping);
	element->setItems(items);
	element->setWriteItemMapping(writeItemMapping);

	/*
	metrics[1].operator ++();
	if (metrics[1].load() % 10 == 1) {
		VERBOSE("# of items %d", request->read_set_size() + request->write_set_size());
	}
	*/
	/*
	 * dealing with special transactions
	 */
	if (tranxType == GTranxType::SHOWLOCK) {
		response->set_status(GStatus::OK);
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_PRINTLOCKSTATUS);
		element->setStageNum(17);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	}

	if (tranxType == GTranxType::SHOWMAP) {
		if (request->read_set_size() != 1) {
			replyThread->SendReplyClient(serverRPC);
		} else {
			std::unordered_set<std::string> ips;
			tranxServiceSharedData->getMappingStorage()->GetMapping(request->read_set(0).key(),
					ips);
			for (auto it = ips.begin(); it != ips.end(); ++it) {
				response->add_ips(*it);
			}
			response->set_status(GStatus::OK);
			replyThread->SendReplyClient(serverRPC);
		}
		AddToFreeList(element);
		return;
	}

	if (tranxType == GTranxType::RUNMETIS) {
		/*
		 * TODO: forward to node0 to run the repartition
		 */
		repartition->TriggerRepartition();
		response->set_status(GStatus::OK);
		replyThread->SendReplyClient(serverRPC);
		return;
	}

	if (tranxType == GTranxType::OUTPUTPERFMETRIC) {
		response->set_status(GStatus::OK);
		serverRPC->setIsPerfOutPutTask(true);
		replyThread->SendReplyClient(serverRPC);
		AddToFreeList(element);
		server->DumpPerfLog();
		return;
	}
	/*
	 * dealing normal transaction and snapshot creation transaction
	 *  except repartition transaction is handled by TranxRepartition class
	 */

	if (!snapshotCreation && items.empty()) {
		VERBOSE("empty tranx, weird");
		response->set_status(GStatus::OK);
		replyThread->SendReplyClient(serverRPC);
		AddToFreeList(element);
		return;
	}

	/*
	 * transaction forward for 1PC
	 */
	if (items.size() == 1 && items.begin()->first != selfAddress) {
		//VERBOSE("TranxService gets forwarded to %s", items.begin()->first.c_str());
		for (auto it = readItemMapping.begin(); it != readItemMapping.end(); ++it) {
			GMapItem *mapItem = response->add_mapitem();
			mapItem->set_key(it->first);
			for (auto ip = it->second.begin(); ip != it->second.end(); ++ip) {
				mapItem->add_ips(*ip);
			}
		}
		for (auto it = writeItemMapping.begin(); it != writeItemMapping.end(); ++it) {
			GMapItem *mapItem = response->add_mapitem();
			mapItem->set_key(it->first);
			for (auto ip = it->second.begin(); ip != it->second.end(); ++ip) {
				mapItem->add_ips(*ip);
			}
		}
		response->set_coord(items.begin()->first);
		response->set_status(GStatus::CHANGE_COORD);
		replyThread->SendReplyClient(serverRPC);
		AddToFreeList(element);
		return;
	}

	//Lock local data
	Util::TranxID tranxID = tranxServiceSharedData->GenerateTranxID();
	element->SetTranxID(tranxID);

	//VERBOSE("ClientService gets a new transaction with ID: %d#%lu:%s", tranxID.GetNodeID(),
	//		tranxID.GetTranxID(), (snapshotCreation) ? "snapshot" : "normal");

	std::vector<std::string> ips;
	if (snapshotCreation) {
		tranxServiceSharedData->SetSnapshotTranx(tranxID);
		ips = tranxServerInfo->GetAllNodes();
	} else {
		for (auto it = items.begin(); it != items.end(); ++it) {
			ips.push_back(it->first);
		}
	}
	element->setIps(ips);

	if (ips.size() == 1) {
		metrics[0].operator ++();
		if (metrics[0].load() % 10000 == 1) {
			VERBOSE("1pc metric %lu", metrics[0].load());
		}
	}

	std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
	std::vector<GItem> localReadSet;
	std::vector<GItem> localWriteSet;
	if (!snapshotCreation) {
		localReadSet = items[selfAddress].first;
		localWriteSet = items[selfAddress].second;
		for (auto it = localReadSet.begin(); it != localReadSet.end(); ++it) {
			keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Shared;
		}
		for (auto it = localWriteSet.begin(); it != localWriteSet.end(); ++it) {
			keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Exclusive;
		}
	}
	element->setStageNum(1);
	if (element->getTranxType() == GTranxType::SNAPSHOT) {
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTEPOCHLOCK);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
	} else {
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSMAP);
		element->setKeysMode(keysMode);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
	}
	return;
}

void ClientService::ClientCommit(Stages::TranxServiceQueueElement *element) {
	int stageNum = element->getStageNum();
	if (stageNum == 1) {
		/*
		 * no matter it's normal/snapshot transaction
		 * lock needs to be obtained
		 */
		if (element->isLockObtained()) {
			element->setStageNum(2);
			element->setStorageTask(Stages::StorageStageTask::_CheckReadSet);
			std::unordered_map<std::string, uint64> readSet;
			std::unordered_map<std::string, std::pair<std::vector<GItem>, std::vector<GItem>> >& items =
					element->getItems();
			std::vector<GItem> localReadSet = items[tranxServerInfo->GetSelfAddress()].first;
			for (auto it = localReadSet.begin(); it != localReadSet.end(); ++it) {
				readSet[it->key()] = it->version();
			}
			element->setReadSet(readSet);
			localStorage->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		} else {
			//VERBOSE("ClientService local lock not obtained");
			assert(element->getTranxType() != GTranxType::SNAPSHOT);
			replyThread->SendReplyClient(element->GetServerRPC());
			element->setStageNum(16);
			element->setLogTask(Stages::Log::TranxLogStageTask::_CoordinatorAck);
			tranxLog->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		}
		return;
	} else if (stageNum == 2) {
		/*
		 * readset checked
		 */
		if (!element->isStorageCheckSucc()) {
			//VERBOSE("ClientService local read check failed");
			assert(element->getTranxType() != GTranxType::SNAPSHOT);
			replyThread->SendReplyClient(element->GetServerRPC());
			element->setStageNum(15);
			element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSMAP);
			lockTable->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		}
		if (element->getIps().size() == 1) {
			/*
			 * Optimize for 1PC when only this node is involved
			 */
			const std::unordered_map<std::string, std::unordered_set<std::string>> &writeItemMapping =
					element->getWriteItemMapping();
			SharedResources::MappingDS *mappingDS = tranxServiceSharedData->getMappingStorage();
			bool mappingCheckSucc = true;
			std::unordered_set<std::string> ips;
			for (auto it = writeItemMapping.begin(); it != writeItemMapping.end(); ++it) {
				ips.clear();
				mappingDS->GetMapping(it->first, ips);
				if (it->second != ips) {
					mappingCheckSucc = false;
					break;
				}
			}
			if (mappingCheckSucc) {
				element->setStageNum(3);
				element->setLogTask(Stages::Log::TranxLogStageTask::_Commit1PC);
				tranxLog->SendToStage(element,
						Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
				return;
			} else {
				//VERBOSE("ClientService map data check failed for 1pc tranx");
				assert(element->getTranxType() != GTranxType::SNAPSHOT);
				replyThread->SendReplyClient(element->GetServerRPC());
				element->setStageNum(15);
				element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSMAP);
				lockTable->SendToStage(element,
						Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
				return;
			}
		} else {
			element->setStageNum(7);
			element->setLogTask(Stages::Log::TranxLogStageTask::_Prepare);
			tranxLog->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		}
	} else if (stageNum == 3) {
		/*
		 * commit log is persisted
		 */
		element->GetServerRPC()->GetClientResponse()->mutable_clientcommit()->mutable_response()->set_status(
				GStatus::OK);
		replyThread->SendReplyClient(element->GetServerRPC());
		element->setStageNum(4);
		element->setStateTask(Stages::TranxStatesStageTask::_SetToCommitState);
		tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
				ptok_tranxStates);
		return;
	} else if (stageNum == 4) {
		/*
		 * state set to commit
		 */
		std::unordered_map<std::string, std::string> writeSet;
		std::unordered_map<std::string, std::pair<std::vector<GItem>, std::vector<GItem>> >& items =
				element->getItems();
		std::vector<GItem> localWriteSet = items[tranxServerInfo->GetSelfAddress()].second;
		for (auto it = localWriteSet.begin(); it != localWriteSet.end(); ++it) {
			writeSet[it->key()] = it->value();
		}
		element->setStageNum(5);
		element->setWriteSet(writeSet);
		element->setStorageTask(Stages::StorageStageTask::_Write);
		localStorage->SendToStage(element,
				Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else if (stageNum == 5) {
		/*
		 * write set written
		 */
		element->setStageNum(6);
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSMAP);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else if (stageNum == 6) {
		/*
		 * lock released
		 * initial stageNum for tranxAck
		 */
		element->setAckCommitAbort(true);
		element->setStageNum(1);
		element->setStageId(Stages::TranxServiceQueueElement::StageID::TRANXACK);
		tranxAck->SendToDaemon(element);
		return;
	} else if (stageNum == 7) {
		/*
		 * prepare log is persisted
		 */
		element->setStageNum(8);
		element->setTranxRpcTask(Stages::TranxRPCHelperStageTask::_SENDPREPANDPOLL);
		tranxRPCHelper->SendToStage(element,
				Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else if (stageNum == 8) {
		/*
		 * prepare(first phase) returned
		 */
		Stages::TranxRPCHelperPollingResult peerResult = element->getTranxRpcResult();
		if (peerResult != Stages::TranxRPCHelperPollingResult::AllOK) {
			//VERBOSE("ClientService prepare failure");
			assert(element->getTranxType() != GTranxType::SNAPSHOT);
			element->setStageNum(9);
			element->setLogTask(Stages::Log::TranxLogStageTask::_CoordinatorAbort);
			tranxLog->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		} else {
			const std::unordered_map<std::string, std::unordered_set<std::string>> &writeItemMapping =
					element->getWriteItemMapping();
			SharedResources::MappingDS *mappingDS = tranxServiceSharedData->getMappingStorage();
			bool mappingCheckSucc = true;
			std::unordered_set<std::string> ips;
			for (auto it = writeItemMapping.begin(); it != writeItemMapping.end(); ++it) {
				ips.clear();
				mappingDS->GetMapping(it->first, ips);
				if (it->second != ips) {
					mappingCheckSucc = false;
					break;
				}
			}
			if (mappingCheckSucc) {
				element->setStageNum(12);
				element->setLogTask(Stages::Log::TranxLogStageTask::_Commit);
				element->setLogType(Stages::Log::GLogType::COMMIT);
				tranxLog->SendToStage(element,
						Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
				return;
			} else {
				//VERBOSE("ClientService map check failure after prepare poll");
				assert(element->getTranxType() != GTranxType::SNAPSHOT);
				element->setStageNum(9);
				element->setLogTask(Stages::Log::TranxLogStageTask::_CoordinatorAbort);
				tranxLog->SendToStage(element,
						Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
				return;
			}
		}
	} else if (stageNum == 9) {
		/*
		 * prepare failed and log is persisted
		 */
		element->setStageNum(10);
		element->setStateTask(Stages::TranxStatesStageTask::_SetToAbortState);
		tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
				ptok_tranxStates);
		return;
	} else if (stageNum == 10) {
		/*
		 * During aborting, states are modified
		 */
		element->setStageNum(11);
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSMAP);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else if (stageNum == 11) {
		/*
		 * aborting, lock is released
		 *
		 * initial stageNum for tranxAck
		 */
		replyThread->SendReplyClient(element->GetServerRPC());
		element->setAckCommitAbort(false);
		element->setStageNum(1);
		element->setStageId(Stages::TranxServiceQueueElement::StageID::TRANXACK);
		tranxAck->SendToDaemon(element);
		return;
	} else if (stageNum == 12) {
		/*
		 * prepare succ, log is persisted
		 */
		element->setStageNum(13);
		element->setStateTask(Stages::TranxStatesStageTask::_SetToCommitState);
		tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
				ptok_tranxStates);
		return;
	} else if (stageNum == 13) {
		/*
		 * prepare succ, states updated
		 */
		element->GetServerRPC()->GetClientResponse()->mutable_clientcommit()->mutable_response()->set_status(
				GStatus::OK);
		replyThread->SendReplyClient(element->GetServerRPC());
		if (element->getTranxType() == GTranxType::SNAPSHOT) {
			element->setStageNum(14);
			element->setStorageTask(Stages::StorageStageTask::_CreateSnapshot);
			localStorage->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		} else {
			std::unordered_map<std::string, std::string> writeSet;
			std::unordered_map<std::string, std::pair<std::vector<GItem>, std::vector<GItem>> >& items =
					element->getItems();
			std::vector<GItem> localWriteSet = items[tranxServerInfo->GetSelfAddress()].second;
			for (auto it = localWriteSet.begin(); it != localWriteSet.end(); ++it) {
				writeSet[it->key()] = it->value();
			}
			element->setStageNum(5);
			element->setWriteSet(writeSet);
			element->setStorageTask(Stages::StorageStageTask::_Write);
			localStorage->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		}
		return;
	} else if (stageNum == 14) {
		/*
		 * snapshot tranx write succ
		 */
		element->setStageNum(6);
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASEEPOCHLOCK);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else if (stageNum == 15) {
		/*
		 * aborting, client replied, lock released
		 */
		element->setStageNum(16);
		element->setLogTask(Stages::Log::TranxLogStageTask::_CoordinatorAck);
		tranxLog->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else if (stageNum == 16) {
		/*
		 * aborting last step, set tranxstates
		 */
		element->setStageNum(100);
		element->setStateAckCommitAbort(false);
		element->setStateTask(Stages::TranxStatesStageTask::_SetToAck);
		element->setTerminateInThisStage(true);
		tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
				ptok_tranxStates);
		return;
	} else if (stageNum == 17) {
		/*
		 * print lock status request
		 */
		VERBOSE("in ClientService show lock status");
		element->GetServerRPC()->GetClientResponse()->mutable_clientcommit()->mutable_response()->set_lock_status(
				element->getLockStatus());
		replyThread->SendReplyClient(element->GetServerRPC());
		AddToFreeList(element);
		return;
	} else {
		assert(false);
	}
}

} /* namespace Service */
} /* namespace DTranx */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "MidStage.h"

namespace DTranx {
namespace Stages {

void MidStage::SendToStage(TranxServiceQueueElement *element, int stageThread,
		moodycamel::ProducerToken *ptok, uint64_t randSeed) {
	element->setStageEnteringTime(TranxServiceQueueElement::EPOCH_TIMEPOINT);
	if (IsEnabled(Stages::PerfStage::PerfKind::RESPONSE_TIME)) {
		if (randSeed % responsePerfFreq == 0) {
			element->setStageEnteringTime(boost::chrono::steady_clock::now());
		}
	}
	if (numOfStageThreads > 1) {
		if (stageThread == TranxServiceQueueElement::DISPATCH_STAGE_THREADID) {
			//if (ptok != NULL) {
			//	assert(dispatchQueue.enqueue(*ptok, element));
			//} else {
			//assert(dispatchQueue.enqueue(element));
			while (!dispatchQueue.try_enqueue(element)) {
				if (dispatchQueue.enqueue(element)) {
					break;
				}
			}
			//}
		} else {
			assert(processQueues.size() > stageThread);
			//assert(processQueues[stageThread].enqueue(element));
			while (!processQueues[stageThread].try_enqueue(element)) {
				if (processQueues[stageThread].enqueue(element)) {
					break;
				}
			}
		}
	} else {
		//assert(processQueue.enqueue(element));
		while (!processQueue.try_enqueue(element)) {
			if (processQueue.enqueue(element)) {
				break;
			}
		}
	}
}

void MidStage::LeaveStage(TranxServiceQueueElement *element, int stageThread, int task) {
	/*
	if (IsEnabled(Stages::PerfStage::PerfKind::RESPONSE_TIME)) {
		if (element->getStageEnteringTime() != TranxServiceQueueElement::EPOCH_TIMEPOINT) {
			boost::chrono::steady_clock::time_point now = boost::chrono::steady_clock::now();
			uint32_t timeElapsed = boost::chrono::duration_cast<boost::chrono::nanoseconds>(
					now - element->getStageEnteringTime()).count();
			if (numOfStageThreads > 1) {
				if (stageThread == TranxServiceQueueElement::DISPATCH_STAGE_THREADID) {
					if (timeElapsed > 10000000) {
						AddLongLatency(5, task);
					}
					AddResponseTimeSample(5, timeElapsed);
				} else {
					if (timeElapsed > 10000000) {
						AddLongLatency(stageThread, task);
					}
					AddResponseTimeSample(stageThread, timeElapsed);
				}
			} else {
				if (timeElapsed > 10000000) {
					AddLongLatency(0, task);
				}
				AddResponseTimeSample(0, timeElapsed);
			}
			return;
		}
	}
	*/
	return;
}

} /* namespace Stages */
} /* namespace DTranx */


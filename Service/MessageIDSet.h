/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * MessageIDSet are used to record the processing and processed client/tranx requests in order to avoid re-execution.
 * it's not persisted in disk but it's ok because the second processing will always fail.
 * To garbage collect, for each client we keep track of the existing message IDs and aggregate them by calculating the smallest one.
 *
 * thread safe
 */

#ifndef DTRANX_SERVICE_MESSAGEIDSET_H_
#define DTRANX_SERVICE_MESSAGEIDSET_H_

#include <cstdlib>
#include <unordered_map>
#include <set>
#include <mutex>
#include "DTranx/Util/types.h"
#include "Util/StaticConfig.h"
#include "zmsg.hpp"

namespace DTranx {
namespace Service {

class MessageIDSet {
public:
	MessageIDSet(bool threadsafe = false);
	virtual ~MessageIDSet() noexcept(false);
	bool IsProcessed(std::string client, uint64 messageID);
	uint64_t AggregateAllMessage();

private:
	bool _IsProcessed(std::string client, uint64 messageID);
	void _Insert(std::string client, uint64 messageID);
	void _GCProcessed(std::string clientID);

	mutable std::mutex mutex;
	struct MessageInfo {
		uint64 earliest;
		std::set<uint64> discreteSet;
		MessageInfo() {
			earliest = std::strtoull(Util::StaticConfig::GetInstance()->Read("FirstID").c_str(),
			NULL, 10) - 1;
		}
	};

	std::unordered_map<std::string, MessageInfo *> messages;
	bool threadsafe;
};

} /* namespace Service */
} /* namespace DTranx */

#endif /* DTRANX_SERVICE_CLIENTMESSAGES_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "MetisHelper.h"

using namespace DTranx;

TEST(MetisHelper, AddTranx) {
	DaemonStages::MetisHelper metisHelper;
	std::unordered_set<std::string> readSet, writeSet;
	readSet.insert("key1");
	writeSet.insert("key2");
	metisHelper.AddTranx(readSet, writeSet);
	EXPECT_EQ(1, metisHelper.graph[0].size());
	EXPECT_EQ(1, metisHelper.graph[1].size());
}

TEST(MetisHelper, RunMetis) {
	DaemonStages::MetisHelper metisHelper;
	std::unordered_set<std::string> readSet, writeSet;
	readSet.insert("key1");
	readSet.insert("key2");
	writeSet.insert("key3");
	metisHelper.AddTranx(readSet, writeSet);
	readSet.clear();
	writeSet.clear();
	readSet.insert("key4");
	readSet.insert("key5");
	writeSet.insert("key6");
	metisHelper.AddTranx(readSet, writeSet);
	EXPECT_TRUE(metisHelper.RunMetis(2));
	EXPECT_EQ(metisHelper.GetPartitions("key1"), metisHelper.GetPartitions("key2"));
	EXPECT_EQ(metisHelper.GetPartitions("key4"), metisHelper.GetPartitions("key6"));
	EXPECT_NE(metisHelper.GetPartitions("key3"), metisHelper.GetPartitions("key5"));
}

TEST(MetisHelper, _AddTranxR) {
	DaemonStages::MetisHelper metisHelper(true);
	std::unordered_set<std::string> readSet, writeSet;
	readSet.insert("key1");
	readSet.insert("key2");
	writeSet.insert("key3");
	metisHelper.AddTranx(readSet, writeSet);
	readSet.clear();
	writeSet.clear();
	readSet.insert("key1");
	writeSet.insert("key2");
	metisHelper.AddTranx(readSet, writeSet);

	int vertexID1 = metisHelper.ConvertKeyToVertexID("key1");
	int vertexID2 = metisHelper.ConvertKeyToVertexID("key2");
	int vertexID3 = metisHelper.ConvertKeyToVertexID("key3");

	EXPECT_EQ(1, metisHelper.graphR_S1[vertexID1].numOfUpdate);
	EXPECT_EQ(2, metisHelper.graphR_S1[vertexID1].tranxs.size());
	EXPECT_EQ(2, metisHelper.graphR_S1[vertexID2].numOfUpdate);
	EXPECT_EQ(2, metisHelper.graphR_S1[vertexID2].tranxs.size());
	EXPECT_EQ(2, metisHelper.graphR_S1[vertexID3].numOfUpdate);
	EXPECT_EQ(1, metisHelper.graphR_S1[vertexID3].tranxs.size());
}

TEST(MetisHelper, _ConvertToStage2_1) {
	DaemonStages::MetisHelper metisHelper(true);
	/*
	 * T1: 0,1,2,3
	 * T2: 0,1
	 * T3: 1,2,3
	 * T4: 0,4
	 */
	DaemonStages::MetisHelper::Vertex vertex;
	std::set<int> tranx;
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(0);
	tranx.insert(1);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(0);
	tranx.insert(4);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	metisHelper.graphR_S1.push_back(vertex);

	tranx.clear();
	vertex.tranxs.clear();
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(0);
	tranx.insert(1);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	metisHelper.graphR_S1.push_back(vertex);

	vertex.tranxs.clear();
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	metisHelper.graphR_S1.push_back(vertex);

	vertex.tranxs.clear();
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	metisHelper.graphR_S1.push_back(vertex);

	vertex.tranxs.clear();
	tranx.clear();
	tranx.insert(0);
	tranx.insert(4);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	metisHelper.graphR_S1.push_back(vertex);

	metisHelper._ConvertToStage2();
	EXPECT_EQ(3, metisHelper.graphR_S2[0].first.size());
	EXPECT_EQ(2, metisHelper.graphR_S2[1].first.size());
	EXPECT_EQ(4, metisHelper.graphR_S2[2].first.size());
	EXPECT_EQ(4, metisHelper.graphR_S2[7].first.front());
	EXPECT_EQ(10, metisHelper.graphR_S2[13].first.back());
}

TEST(MetisHelper, _ConvertToStage2_2) {
	DaemonStages::MetisHelper metisHelper(true);
	/*
	 * T1: 0,1,2,3
	 * T2: 0,1
	 * T3: 1,2,3
	 * T4: 0,4
	 * T5: 1,2,3
	 */
	DaemonStages::MetisHelper::Vertex vertex;
	std::set<int> tranx;
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(0);
	tranx.insert(1);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(0);
	tranx.insert(4);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	metisHelper.graphR_S1.push_back(vertex);

	tranx.clear();
	vertex.tranxs.clear();
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(0);
	tranx.insert(1);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(2,0);
	tranx.clear();
	metisHelper.graphR_S1.push_back(vertex);

	vertex.tranxs.clear();
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(2,0);
	tranx.clear();
	metisHelper.graphR_S1.push_back(vertex);

	vertex.tranxs.clear();
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(2,0);
	tranx.clear();
	metisHelper.graphR_S1.push_back(vertex);

	vertex.tranxs.clear();
	tranx.insert(0);
	tranx.insert(4);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	metisHelper.graphR_S1.push_back(vertex);

	metisHelper._ConvertToStage2();
	EXPECT_EQ(15, metisHelper.graphR_S2.size());
	EXPECT_EQ(3, metisHelper.graphR_S2[0].first.size());
	EXPECT_EQ(4, metisHelper.graphR_S2[2].first.size());
	EXPECT_EQ(4, metisHelper.graphR_S2[7].first.front());
	EXPECT_EQ(12, metisHelper.graphR_S2[9].first.back());
	EXPECT_EQ(13, metisHelper.graphR_S2[11].first.back());
}

/*
 * subtle bug check when converting to replicated graph
 * 	assigning replicated vertex ID according to the ordered tranx list might not be right
 */
TEST(MetisHelper, _ConvertToStage2_3) {
	DaemonStages::MetisHelper metisHelper(true);
	/*
	 * T1: 0,1,2,3
	 * T2: 0,1,3
	 */
	DaemonStages::MetisHelper::Vertex vertex;
	std::set<int> tranx;
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	metisHelper.graphR_S1.push_back(vertex);

	vertex.tranxs.clear();
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	metisHelper.graphR_S1.push_back(vertex);

	vertex.tranxs.clear();
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	metisHelper.graphR_S1.push_back(vertex);

	vertex.tranxs.clear();
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	tranx.insert(0);
	tranx.insert(1);
	tranx.insert(2);
	tranx.insert(3);
	vertex.tranxs[tranx] = std::make_pair(1,0);
	tranx.clear();
	metisHelper.graphR_S1.push_back(vertex);

	metisHelper._ConvertToStage2();
	EXPECT_EQ(10, metisHelper.graphR_S2.size());
	EXPECT_EQ(2, metisHelper.graphR_S2[0].first.size());
	EXPECT_EQ(3, metisHelper.graphR_S2[2].first.size());
	EXPECT_EQ(6, metisHelper.graphR_S2[8].first.back());
	EXPECT_EQ(5, metisHelper.graphR_S2[9].first.back());
}

/*
 * eyeball the result
 */
TEST(MetisHelper, _RunMetisR1) {
	DaemonStages::MetisHelper metisHelper(true);
	/*
	 * T1: 0,1,2,3
	 * T2: 0,1
	 * T3: 1,2,3
	 * T4: 0,4
	 */
	/*
	 * expected
	 * key mapping
	 * key1 -> 3; key2 -> 2; key3 -> 0; key4 -> 1; key5 -> 4
	 *
	 * vertexid to replicated vertexid
	 * 0 -> 10 11 12 13
	 * 1 -> 6 7 8 9
	 * 2 -> 0 1 2
	 * 3 -> 3 4 5
	 * 4 -> 14
	 *
	 *
	 * original graph partition result
	 * partition0 -> key1, key2, key5
	 * partition1 -> key2, key3, key4
	 * partition2 -> key1, key2, key3
	 */
	std::unordered_set<std::string> readSet, writeSet;
	readSet.insert("key1");
	readSet.insert("key2");
	writeSet.insert("key3");
	writeSet.insert("key4");
	metisHelper.AddTranx(readSet, writeSet);

	readSet.clear();
	writeSet.clear();
	readSet.insert("key1");
	writeSet.insert("key2");
	metisHelper.AddTranx(readSet, writeSet);

	readSet.clear();
	writeSet.clear();
	readSet.insert("key2");
	writeSet.insert("key3");
	readSet.insert("key4");
	metisHelper.AddTranx(readSet, writeSet);

	readSet.clear();
	writeSet.clear();
	readSet.insert("key1");
	writeSet.insert("key5");
	metisHelper.AddTranx(readSet, writeSet);

	metisHelper.RunMetis(3);
	std::unordered_set<int> result = metisHelper.GetPartitionsR("key1");
	std::cout << "key1: ";
	for (auto it = result.begin(); it != result.end(); ++it) {
		std::cout << *it << " ";
	}
	std::cout << std::endl;

	result = metisHelper.GetPartitionsR("key2");
	std::cout << "key2: ";
	for (auto it = result.begin(); it != result.end(); ++it) {
		std::cout << *it << " ";
	}
	std::cout << std::endl;

	result = metisHelper.GetPartitionsR("key3");
	std::cout << "key3: ";
	for (auto it = result.begin(); it != result.end(); ++it) {
		std::cout << *it << " ";
	}
	std::cout << std::endl;

	result = metisHelper.GetPartitionsR("key4");
	std::cout << "key4: ";
	for (auto it = result.begin(); it != result.end(); ++it) {
		std::cout << *it << " ";
	}
	std::cout << std::endl;

	result = metisHelper.GetPartitionsR("key5");
	std::cout << "key5: ";
	for (auto it = result.begin(); it != result.end(); ++it) {
		std::cout << *it << " ";
	}
	std::cout << std::endl;
}

/*
 * test replicated METIS effectiveness
 */
TEST(MetisHelper, _RunMetisR2) {
	DaemonStages::MetisHelper metisHelper(true);
	std::unordered_set<std::string> readSet, writeSet;
	readSet.insert("key2");
	readSet.insert("key3");
	readSet.insert("key4");
	for (int i = 0; i < 5; i++) {
		metisHelper.AddTranx(readSet, writeSet);
	}
	readSet.clear();
	readSet.insert("key5");
	readSet.insert("key6");
	readSet.insert("key7");
	for (int i = 0; i < 3; i++) {
		metisHelper.AddTranx(readSet, writeSet);
	}
	readSet.clear();
	readSet.insert("key2");
	readSet.insert("key3");
	readSet.insert("key4");
	for (int i = 0; i < 7; i++) {
		metisHelper.AddTranx(readSet, writeSet);
	}

	readSet.clear();
	readSet.insert("key8");
	readSet.insert("key9");
	readSet.insert("key1");
	for (int i = 0; i < 2; i++) {
		metisHelper.AddTranx(readSet, writeSet);
	}
	metisHelper.RunMetis(3);
	std::unordered_map<std::string, std::unordered_set<int> > newMapping;
	metisHelper.GetNewMappingsR(newMapping);
	EXPECT_EQ(newMapping["key2"], newMapping["key3"]);
	EXPECT_EQ(newMapping["key5"], newMapping["key7"]);
	EXPECT_EQ(newMapping["key9"], newMapping["key1"]);
}

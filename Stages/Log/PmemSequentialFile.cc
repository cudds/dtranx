/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "PmemSequentialFile.h"

namespace DTranx {
namespace Stages {
namespace Log {

LogStatus PmemSequentialFile::Read(size_t n, Slice* result, char* scratch) {
	char *addr;
	if(plp == NULL){
		return LogStatus::IOError(filename_);
	}
	size_t actualRead = pmemlog_read(plp, n, &addr);
	memcpy(scratch, addr, actualRead);
	*result = Slice(scratch, actualRead);
	return LogStatus::OK();
}

LogStatus PmemSequentialFile::Skip(uint64_t n) {
	/*
	 * Not implemented yet, used to skip some bytes when reading
	 */
	assert(false);
	return LogStatus::OK();
}

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

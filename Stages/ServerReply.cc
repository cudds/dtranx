/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "ServerReply.h"
#include "DTranx/Util/Log.h"
#include "Util/StringUtil.h"
#include "Build/Util/Profile.pb.h"
#include "TranxServiceQueueElement.h"
#include "Server/Server.h"

namespace DTranx {
namespace Stages {

ServerReply::ServerReply(SharedResources::TranxServerInfo *tranxServerInfo)
		: PerfStage("ServerReply"), terminateServerRPCReplyThread(false), peerReplyQueue(1000), clientReplyQueue(
				1000), bufferSize(300), localBufferPeer(), localBufferClient() {
	config = tranxServerInfo->GetConfig();
	context = tranxServerInfo->GetContext();
	mode = tranxServerInfo->GetMode();
	localBufferPeer.SetData(new uint8[bufferSize], bufferSize);
	localBufferClient.SetData(new uint8[bufferSize], bufferSize);
	localBufferClientForIP.SetData(new uint8[50], 50);
	localBufferClientForEndpoint.SetData(new uint8[50], 50);
	std::vector<std::string> allNodes = tranxServerInfo->GetAllNodes();
	std::string selfAddress = config.read("SelfAddress");
	std::vector<std::string> tempAllPeerPortsStr = Util::StringUtil::Split(
			config.read<std::string>("peerReceiverPort"), ',');
	std::vector<uint32_t> tempAllPeerPorts;
	for (auto port = tempAllPeerPortsStr.begin(); port != tempAllPeerPortsStr.end(); ++port) {
		tempAllPeerPorts.push_back(std::strtoul(port->c_str(), nullptr, 10));
	}

	if (mode == SharedResources::TranxServerInfo::Mode::NORMAL
			|| mode == SharedResources::TranxServerInfo::Mode::INTEGTEST) {
		VERBOSE("server reply normal mode");
		remotePeerPorts = InitializePeerPorts(allNodes, tempAllPeerPorts, selfAddress);
	} else {
		VERBOSE("server reply unittest server mode");
		assert(tempAllPeerPorts.size() > 0);
		remotePeerPorts[selfAddress].insert(tempAllPeerPorts[0]);
	}
}

std::unordered_map<std::string, std::unordered_set<uint32_t> > ServerReply::InitializePeerPorts(
		std::vector<std::string> allNodes, std::vector<uint32_t> allPorts,
		std::string selfAddress) {
	std::unordered_map<std::string, std::unordered_set<uint32_t> > remotePeerPorts;
	int numOfPeer = allNodes.size() - 1;
	int selfIPPosition = -1;
	for (auto it = allNodes.begin(); it != allNodes.end(); ++it) {
		if (selfAddress == *it) {
			selfIPPosition = it - allNodes.begin();
		}
	}
	assert(numOfPeer != 0);
	int numOfPortsForPeer = allPorts.size() / numOfPeer;

	/*
	 * deterministically find out the listening peer port on the remote server
	 */

	for (auto ip = allNodes.begin(); ip != allNodes.end(); ++ip) {
		int positionInRemoteServer = selfIPPosition;
		if (ip - allNodes.begin() < selfIPPosition) {
			positionInRemoteServer -= 1;
		} else if (ip - allNodes.begin() == selfIPPosition) {
			continue;
		}
		int startPort = positionInRemoteServer * numOfPortsForPeer;
		int endPort = startPort + numOfPortsForPeer - 1;
		VERBOSE("initializing peer %s, port range are (%d, %d)", ip->c_str(), startPort, endPort);
		for (int port = startPort; port <= endPort; ++port) {
			remotePeerPorts[*ip].insert(allPorts[port]);
		}
	}
	return remotePeerPorts;
}

ServerReply::~ServerReply() noexcept(false) {
	terminateServerRPCReplyThread.store(true);
	if (peerReplyThread.joinable()) {
		peerReplyThread.join();
	}
	if (clientReplyThread.joinable()) {
		clientReplyThread.join();
	}
}

void ServerReply::PeerReplyThread() {
	NOTICE("PeerReplyThread started");
	uint64_t randSeed = 0;

	for (auto peer = remotePeerPorts.begin(); peer != remotePeerPorts.end(); ++peer) {
		assert(peer->second.size() == 1);
		VERBOSE("PeerReplyThread connects to %s at port %u", peer->first.c_str(),
				*peer->second.begin());
		peerSenders[peer->first] = new RPC::ZeromqSender(context);
		peerSenders[peer->first]->ConnectTcp(peer->first, std::to_string(*peer->second.begin()));

	}
	while (true) {
		if (terminateServerRPCReplyThread.load()) {
			NOTICE("PeerReplyThread is reclaimed");
			for (auto peer = peerSenders.begin(); peer != peerSenders.end(); ++peer) {
				delete peer->second;
			}
			break;
		}
		RPC::ServerRPC *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = peerReplyQueue.try_dequeue_bulk(tmpRPCs,
				TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1) {
				AddQueueLengthSample(8, peerReplyQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			RPC::ServerRPC *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(8);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(8);
				std::vector<int> taskLongLatencySamples = GetLongLatency(8);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_peer");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				server->AddToFreeList(element);
				continue;
			}
			std::string ip = element->GetReqIP();
			uint32_t port = element->GetReqPort();
			element->SetRepStatus(RPC::GStatus::OK);
			element->SetRepMessageID(element->GetReqMessageID());
			//VERBOSE("serverreply to %s with messageID %lu", ip.c_str(), element->GetReqMessageID());
			if (bufferSize >= element->GetResponse()->ByteSize()) {
				RPC::BaseRPC::serializeWithMem(*element->GetResponse(), localBufferPeer,
						bufferSize);
				element->PrintTime();
				server->AddToFreeList(element);
				peerSenders[ip]->Send(localBufferPeer.GetData(), localBufferPeer.GetLength());
			} else {
				Util::Buffer localBuffer;
				RPC::BaseRPC::serialize(*element->GetResponse(), localBuffer, 0);
				element->PrintTime();
				server->AddToFreeList(element);
				peerSenders[ip]->Send(localBuffer.GetData(), localBuffer.GetLength());
			}
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void ServerReply::SendReplyPeer(RPC::ServerRPC *serverRPC) {
	//assert(peerReplyQueue.enqueue(serverRPC));
	while (!peerReplyQueue.try_enqueue(serverRPC)) {
		if (peerReplyQueue.enqueue(serverRPC)) {
			break;
		}
	}
}

void ServerReply::ClientReplyThread() {
	NOTICE("ClientReplyThread started");
	uint64_t randSeed = 0;
	Util::Buffer localBuffer;
	while (true) {
		if (terminateServerRPCReplyThread.load()) {
			NOTICE("ClientReplyThread is reclaimed");
			for (auto client = clientSenders.begin(); client != clientSenders.end(); ++client) {
				delete client->second;
			}
			break;
		}
		RPC::ServerRPC *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = clientReplyQueue.try_dequeue_bulk(tmpRPCs,
				TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1) {
				AddQueueLengthSample(9, clientReplyQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			RPC::ServerRPC *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(9);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(9);
				std::vector<int> taskLongLatencySamples = GetLongLatency(9);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_client");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
			}

			//std::string ip = element->GetReqIP();
			//uint32_t port = element->GetReqPort();
			//std::string combinedEndPoint = ip + std::to_string(port);
			element->GetReqCombinedEndpoint((char *) localBufferClientForEndpoint.GetData(),
					(char *) localBufferClientForIP.GetData());
			std::string combinedEndPoint((char *) localBufferClientForEndpoint.GetData());
			std::string ip((char*) localBufferClientForIP.GetData());
			uint32_t port = element->GetReqPort();
			element->SetRepStatus(RPC::GStatus::OK);
			element->SetRepMessageID(element->GetReqMessageID());
			if (clientSenders.find(combinedEndPoint) == clientSenders.end()) {
				VERBOSE("ClientReplyThread connects to %s at port %u", ip.c_str(), port);
				clientSenders[combinedEndPoint] = new RPC::ZeromqSender(context);
				clientSenders[combinedEndPoint]->ConnectTcp(ip, std::to_string(port));
			}

			if (clientResponseBufferSize >= element->GetResponse()->ByteSize()) {
				void *data;
				if (!responseBufferForClients.try_dequeue(data)) {
					data = new uint8[clientResponseBufferSize];
				}
				localBuffer.SetData(data, clientResponseBufferSize);
				RPC::BaseRPC::serializeWithMem(*element->GetResponse(), localBuffer,
						clientResponseBufferSize);
				element->PrintTime();
				clientSenders[combinedEndPoint]->Send(localBuffer.GetData(),
						localBuffer.GetLength(), ReclaimClientResponsesFromZeromq);
			} else {
				WARNING("ServerReply response size: %d",element->GetResponse()->ByteSize());
				RPC::BaseRPC::serialize(*element->GetResponse(), localBuffer, 0);
				element->PrintTime();
				clientSenders[combinedEndPoint]->Send(localBuffer.GetData(),
						localBuffer.GetLength());
				delete[] static_cast<uint8*>(localBuffer.GetData());
			}
			if (element->isIsPerfOutPutTask()) {
				SendReplyPeer(element);
			} else {
				server->AddToFreeList(element);
			}
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
	localBuffer.SetData(NULL, 0);
}

/*
 * SendReply used by services to reply
 */
void ServerReply::SendReplyClient(RPC::ServerRPC *serverRPC) {
	//assert(clientReplyQueue.enqueue(serverRPC));
	while (!clientReplyQueue.try_enqueue(serverRPC)) {
		if (clientReplyQueue.enqueue(serverRPC)) {
			break;
		}
	}
}

void ServerReply::ReplyThread() {
	NOTICE("ReplyThread started");
	for (auto peer = remotePeerPorts.begin(); peer != remotePeerPorts.end(); ++peer) {
		for (auto port = peer->second.begin(); port != peer->second.end(); ++port) {
			VERBOSE("PeerReplyThread connects to %s at port %u", peer->first.c_str(), *port);
			peerSenders[peer->first] = new RPC::ZeromqSender(context);
			peerSenders[peer->first]->ConnectTcp(peer->first, std::to_string(*port));
		}
	}
	while (true) {
		if (terminateServerRPCReplyThread.load()) {
			NOTICE("ReplyThread is reclaimed");
			for (auto client = clientSenders.begin(); client != clientSenders.end(); ++client) {
				delete client->second;
			}
			for (auto peer = peerSenders.begin(); peer != peerSenders.end(); ++peer) {
				delete peer->second;
			}
			break;
		}
		RPC::ServerRPC *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = clientReplyQueue.try_dequeue_bulk(tmpRPCs,
				TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		for (int i = 0; i < count; ++i) {
			RPC::ServerRPC *element = tmpRPCs[i];
			std::string ip = element->GetReqIP();
			uint32_t port = element->GetReqPort();
			std::string combinedEndPoint = ip + std::to_string(port);
			element->SetRepStatus(RPC::GStatus::OK);
			element->SetRepMessageID(element->GetReqMessageID());
			if (clientSenders.find(combinedEndPoint) == clientSenders.end()) {
				VERBOSE("ClientReplyThread connects to %s at port %u", ip.c_str(), port);
				clientSenders[combinedEndPoint] = new RPC::ZeromqSender(context);
				clientSenders[combinedEndPoint]->ConnectTcp(ip, std::to_string(port));
			}
			if (bufferSize >= element->GetResponse()->ByteSize()) {
				RPC::BaseRPC::serializeWithMem(*element->GetResponse(), localBufferClient,
						bufferSize);
				element->PrintTime();
				clientSenders[combinedEndPoint]->Send(localBufferClient.GetData(),
						localBufferClient.GetLength());
			} else {
				Util::Buffer localBuffer;
				RPC::BaseRPC::serialize(*element->GetResponse(), localBuffer, 0);
				element->PrintTime();
				clientSenders[combinedEndPoint]->Send(localBuffer.GetData(),
						localBuffer.GetLength());
			}
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(9);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(9);
				std::vector<int> taskLongLatencySamples = GetLongLatency(9);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_client");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				SendReplyPeer(element);
			} else {
				server->AddToFreeList(element);
			}
		}
		count = peerReplyQueue.try_dequeue_bulk(tmpRPCs,
				TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		for (int i = 0; i < count; ++i) {
			RPC::ServerRPC *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(8);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(8);
				std::vector<int> taskLongLatencySamples = GetLongLatency(8);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_peer");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				server->AddToFreeList(element);
				continue;
			}
			std::string ip = element->GetReqIP();
			uint32_t port = element->GetReqPort();
			element->SetRepStatus(RPC::GStatus::OK);
			element->SetRepMessageID(element->GetReqMessageID());
			if (bufferSize >= element->GetResponse()->ByteSize()) {
				RPC::BaseRPC::serializeWithMem(*element->GetResponse(), localBufferPeer,
						bufferSize);
				element->PrintTime();
				server->AddToFreeList(element);
				peerSenders[ip]->Send(localBufferPeer.GetData(), localBufferPeer.GetLength());
			} else {
				Util::Buffer localBuffer;
				RPC::BaseRPC::serialize(*element->GetResponse(), localBuffer, 0);
				element->PrintTime();
				clientSenders[ip]->Send(localBuffer.GetData(), localBuffer.GetLength());
			}
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

uint32_t clientResponseBufferSize = 1500;
moodycamel::ConcurrentQueue<void*> responseBufferForClients(1000);

void ReclaimClientResponsesFromZeromq(void *data, void *hint) {
	assert(responseBufferForClients.enqueue(data));
}

}
/* namespace Stages */
} /* namespace DTranx */

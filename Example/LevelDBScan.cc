/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */
#include <iostream>
#include <sstream>
#include <string>
#include <cstdlib>

#include "leveldb/db.h"

using namespace std;

int main(int argc, char** argv)
{
    // Set up database connection information and open database
    leveldb::DB* db;
    leveldb::Options options;
    options.create_if_missing = true;

    std::string dbName = "./dtranx.db";
    if( argc > 1){
    	dbName = std::string(argv[1]);
    }

    leveldb::Status status = leveldb::DB::Open(options, dbName, &db);

    if (false == status.ok())
    {
        cerr << "Unable to open/create test database "<< dbName << endl;
        cerr << status.ToString() << endl;
        return -1;
    }

    // Iterate over each item in the database and print them
    leveldb::Iterator* it = db->NewIterator(leveldb::ReadOptions());

    for (it->SeekToFirst(); it->Valid(); it->Next())
    {
        cout << it->key().ToString() << " : " << it->value().ToString() << endl;
    }

    if (false == it->status().ok())
    {
        cerr << "An error was found during the scan" << endl;
        cerr << it->status().ToString() << endl;
    }

    delete it;

    // Close the database
    delete db;
}



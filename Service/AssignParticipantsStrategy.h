/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * AssignParticipantsStrategy decides which node to forward to as participants.
 * If possible, the transaction could be 1PC
 */

#ifndef DTRANX_ASSIGNPARTICIPANTSSTRATEGY_H_
#define DTRANX_ASSIGNPARTICIPANTSSTRATEGY_H_

#include <unordered_set>
#include "SharedResources/TranxServiceSharedData.h"
namespace DTranx {
namespace Service {

class AssignParticipantsStrategy {
public:
	AssignParticipantsStrategy(std::string selfAddress,
			SharedResources::TranxServiceSharedData* tranxServiceSharedData);
	virtual ~AssignParticipantsStrategy();

	/*
	 * mapping resolve.
	 * important: it randomly choose one server read items, but choose the local server over others
	 * 	so during recovery, only the items in the local server is the same.
	 *
	 * if there exists a server where all data items reside, MapItem returns that ip address with empty pair
	 * return value: ip -> pair<readset, writeset>
	 *
	 * along with the read write items, MapItem also returns write item mapping data for the coordinator
	 * to read check after lock acquisition
	 */
	std::unordered_map<std::string, std::pair<std::vector<GItem>, std::vector<GItem>> > MapItem(
			google::protobuf::RepeatedPtrField<GItem> readItems,
			google::protobuf::RepeatedPtrField<GItem> writeItems,
			std::unordered_map<std::string, std::unordered_set<std::string>>* readItemMapping = NULL,
			std::unordered_map<std::string, std::unordered_set<std::string>>* writeItemMapping =
					NULL);
private:
	/*
	 * InnerJoin: function to obtain the intersection of two unordered_set
	 */
	void InnerJoin(std::unordered_set<std::string>& set1,
			std::unordered_set<std::string>& set2);

	std::string selfAddress;
	SharedResources::TranxServiceSharedData *tranxServiceSharedData;
};

} /* namespace Service */
} /* namespace DTranx */

#endif /* DTRANX_ASSIGNPARTICIPANTSSTRATEGY_H_ */

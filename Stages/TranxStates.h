/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * 	TranxStates records volatile states for transactions
 * 	it mainly consists of transaction-level states and node-level gc information
 *
 * 	functionalities
 * 		1. avoid unnecessary processing, e.g. prepare when already aborted
 * 		2. fast retrieval for inquiries
 * 		3. fast obtain all commit, abort, gc transactions
 */

#ifndef DTRANX_STAGES_TRANXSTATE_H_
#define DTRANX_STAGES_TRANXSTATE_H_

#include <cassert>
#include <unordered_set>
#include <vector>
#include <unordered_map>
#include <mutex>
#include <memory>
#include "DTranx/Util/Log.h"
#include "Util/StringUtil.h"
#include "DTranx/Util/ConfigHelper.h"
#include "MidStage.h"

namespace DTranx {
namespace DaemonStages {
class GarbageCollector;
}
namespace Stages {

class TranxStates: public MidStage {
public:
	TranxStates(SharedResources::TranxServerInfo *tranxServerInfo, int numOfStageThreads = 1);
	virtual ~TranxStates() noexcept(false);

	void InitStages(Service::TranxService *tranxService,
			Service::ClientService *clientService,
			Service::StorageService *storageService,
			DaemonStages::TranxAck *tranxAck,
			DaemonStages::Repartition *repartition,
			Server::RecoverLog *postRecovery,
			DaemonStages::GarbageCollector *gc) {
		MidStage::InitStage(tranxService, clientService, storageService, tranxAck, repartition,
				postRecovery);
		this->gc = gc;
	}

	State CheckState(const Util::TranxID& tranxID);

	/************************************************************************************************
	 * ready states
	 * CheckAndSetReadyState: set only if it doesn't exist before
	 */
	State CheckAndSetReadyState(const Util::TranxID& tranxID);  //participants

	/************************************************************************************************
	 * commit states
	 * CheckAndSetCommitState is called by participants
	 * SetToCommitState is called by coordinator
	 */
	State CheckAndSetCommitState(const Util::TranxID& tranxID);
	void SetToCommitState(const Util::TranxID& tranxID);

	/************************************************************************************************
	 * abort states
	 * CheckAndSetAbortState: set only if it doesn't exist or it was ready, called by participants
	 * SetToAbortState is called by coordinator
	 */
	State CheckAndSetAbortState(const Util::TranxID& tranxID);
	void SetToAbortState(const Util::TranxID& tranxID);

	/************************************************************************************************
	 * migrate states
	 */
	State CheckAndSetMigrateState(const Util::TranxID& tranxID);

	/************************************************************************************************
	 * ack states
	 * ack states are set by clientCommit/TranxAck/RecoverLog for coordinators
	 */
	void SetToAck(const Util::TranxID& tranxID, bool isCommit);

	/************************************************************************************************
	 * gc states
	 * UpdateGC is called when gc broadcast comes
	 * CleanStateFromGCForOtherNodes clean the allStates for other nodes when not a lot of transactions are going on.
	 * GetGCFactory and GCTobroadcast are called when GarbageCollector tries to gc states
	 */
	void UpdateGC(int nodeID, uint64_t tranxID);
	void CleanStateFromGCForOtherNodes(int nodeID);
	GCFactory GetGCFactory();

	bool GCToBroadcast();

	/*
	 * the following three functions are used by RecoverLog
	 */
	void UpdateGCEarliest(uint64 earliest) {
		gcFactory.UpdateEarliest(earliest);
	}

	void UpdateGCEarliest(int nodeID, uint64_t tranxID) {
		gcFactory.UpdateEarliest(nodeID, tranxID);

		std::unordered_set<Util::TranxID, Util::KeyHasher> toRemove;

		for (auto it = allStates[nodeID].begin(); it != allStates[nodeID].end(); ++it) {
			if (it->first.GetTranxID() <= tranxID) {
				toRemove.insert(it->first);
			}
		}
		for (auto it = toRemove.begin(); it != toRemove.end(); ++it) {
			allStates[nodeID].erase(*it);
		}

	}

	uint64 GetGCEarliest() {
		return gcFactory.GetEarliest();
	}

	virtual void StartStage(std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) {
		bool enableCoreBinding = false;
		uint32_t coreIDIndex = 0;
		assert(numOfStageThreads >= 1);
		if (numOfStageThreads == 1) {
			enableCoreBinding = (coreIDs.size() == 1);
		} else {
			enableCoreBinding = (coreIDs.size() == numOfStageThreads + 1);
		}
		for (int i = 0; i < numOfStageThreads; ++i) {
			processQueues.push_back(moodycamel::ConcurrentQueue<TranxServiceQueueElement *>(1000));
		}
		for (int i = 0; i < numOfStageThreads; ++i) {
			stageThread.push_back(boost::thread(&TranxStates::StageThread, this, i));
			if (enableCoreBinding) {
				Util::ThreadHelper::PinToCPUCore(stageThread[i], coreIDs[coreIDIndex++]);
			}
		}
		if (numOfStageThreads > 1) {
			dispatchThread = boost::thread(&TranxStates::DispatchThread, this);
			if (enableCoreBinding) {
				Util::ThreadHelper::PinToCPUCore(dispatchThread, coreIDs[coreIDIndex++]);
			}
		}
	}

	/*
	 * GetThreadByNodeID get the thread that stores the tranx state
	 * 	this is the method to split the shared data(by hash)
	 *
	 * 	selfNode will receive a lot more because of GC stage polling
	 */
	int GetThreadByNodeID(int nodeID) const {
		return nodeID % numOfStageThreads;
	}

private:
	Util::ConfigHelper configHelper;
	int selfNodeID;

	/************************************************************************************************
	 * state information
	 * nodeid index to tranxid->State
	 */
	std::vector<std::unordered_map<Util::TranxID, State, Util::KeyHasher> > allStates;
	GCFactory gcFactory;
	DaemonStages::GarbageCollector *gc;

	virtual void StageThread(int stageThreadIndex);
	/*
	 * TranxStates stage request only has to go through one thread
	 */
	virtual void DispatchThread();
}
;

} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_TRANXSTATE_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "DTranx/Client/ClientTranx.h"
#include "DTranx/Service/Transaction.pb.h"
#include "DTranx/Util/Log.h"
#include "RPC/ClientRPC.h"
#include "Util/StringUtil.h"

namespace DTranx {
namespace Client {

/*
 * Important: make sure port range (selfClientPort, selfClientPort+ numOfServers) are available
 */
ClientTranx::ClientTranx(uint32_t routerFrontPort, std::shared_ptr<zmq::context_t> context,
		std::vector<std::string> ips, std::string selfAddressStr, uint32_t selfClientPort,
		bool cacheEnabled, CoordinatorStrategy coordinatorStrategy)
		: context(context), routerFrontPort(routerFrontPort), cacheEnabled(cacheEnabled), cluster(
				ips), selfClientPort(selfClientPort), selfAddressStr(selfAddressStr), reclaimClients(
		true), coordStrategy(coordinatorStrategy) {
	std::vector<std::string> tmpFields = Util::StringUtil::Split(selfAddressStr, '.');
	assert(tmpFields.size() == 4);
	selfAddress = new uint32_t[4];
	for (int i = 0; i < 4; ++i) {
		selfAddress[i] = std::stoul(tmpFields[i], nullptr, 10);
	}
	if (cacheEnabled) {
		cache = std::make_shared<ClientCache>();
	}
}

ClientTranx::ClientTranx(uint32_t routerFrontPort, std::shared_ptr<zmq::context_t> context,
		std::vector<std::string> ips, std::shared_ptr<ClientCache> cache,
		std::string selfAddressStr, uint32_t selfClientPort,
		bool cacheEnabled, CoordinatorStrategy coordinatorStrategy)
		: context(context), cache(cache), routerFrontPort(routerFrontPort), cacheEnabled(
				cacheEnabled), cluster(ips), selfAddressStr(selfAddressStr), selfClientPort(
				selfClientPort), reclaimClients(true), coordStrategy(coordinatorStrategy) {
	selfAddress = new uint32_t[4];
	std::vector<std::string> tmpFields = Util::StringUtil::Split(selfAddressStr, '.');
	assert(tmpFields.size() == 4);
	for (int i = 0; i < 4; ++i) {
		selfAddress[i] = std::stoul(tmpFields[i], nullptr, 10);
	}
}

ClientTranx::~ClientTranx() noexcept(false) {
	if (reclaimClients) {
		for (auto it = clients.begin(); it != clients.end(); ++it) {
			delete it->second;
		}
	}
	delete[] selfAddress;
}

void ClientTranx::InitClients(std::string ip, Client* client) {
	if (clients.find(ip) != clients.end()) {
		return;
	}
	clients[ip] = client;
	clientPorts[ip] = client->selfPort;
	reclaimClients = false;
}

Service::GStatus ClientTranx::Read(std::string& key, std::string& value, bool snapshot) {
	if (writeSet.find(key) != writeSet.end()) {
		value = writeSet[key];
		return Service::GStatus::OK;
	}
	uint64 version;
	if (!snapshot && cacheEnabled) {
		value = cache->ReadLocal(key, version);
		if (version != ClientCache::NO_SUCH_DATA) {
			readSet[key] = std::make_pair(version, value);
			return Service::GStatus::OK;
		}
	}
	std::string ip;
	if (cacheEnabled) {
		ip = cache->GetMapping(key);
		if (ip == ClientCache::NO_SUCH_MAP) {
			ip = GetMapping(key);
		}
	} else {
		ip = GetMapping(key);
	}
	CreateClientIfNotExist(ip);
	RPC::ClientRPC* clientRPC = new RPC::ClientRPC(RPC::GServiceID::StorageService, clients[ip]);
	Service::GReadData_Request* request =
			clientRPC->GetStorageRequest()->mutable_readdata()->mutable_request();
	request->set_key(key);
	request->set_snapshot(snapshot);
	clientRPC->SetStorageOpcode(Service::GOpCode::READ_DATA);
	clientRPC->SetReqIP(selfAddress);
	clientRPC->SetReqPort(clientPorts[ip]);
	clients[ip]->CallRPC(clientRPC);
	assert(clients[ip]->BlockPoll(clientRPC));
	Service::GReadData_Response *response =
			clientRPC->GetStorageResponse()->mutable_readdata()->mutable_response();
	assert(clientRPC->GetRepStatus() == RPC::GStatus::OK);
	if (response->status() == Service::GStatus::OK) {
		value = response->value();
		version = response->version();
		if (snapshot) {
			assert(response->has_epoch());
			snapshotReadSet[key] = std::make_pair(response->epoch(), value);
			if (cacheEnabled) {
				UpdateMappingCacheFromStorageService(key, *response);
			}
		} else {
			readSet[key] = std::make_pair(version, value);
		}
		if (cacheEnabled) {
			UpdateDataCacheFromStorageService(key, *response);
		}
		delete clientRPC;
		return Service::GStatus::OK;
	} else if (response->status() == Service::GStatus::FORWARD_SERVER) {
		assert(response->newips_size() > 0);
		std::string targetServer = response->newips(0);
		//VERBOSE("Storage read %s being forwarded to %s", key.c_str(), targetServer.c_str());
		if (cacheEnabled) {
			UpdateMappingCacheFromStorageService(key, *response);
		}
		CreateClientIfNotExist(targetServer);
		delete clientRPC;
		uint64 version;
		clientRPC = new RPC::ClientRPC(RPC::GServiceID::StorageService, clients[targetServer]);
		request = clientRPC->GetStorageRequest()->mutable_readdata()->mutable_request();
		request->set_key(key);
		request->set_snapshot(snapshot);
		clientRPC->SetStorageOpcode(Service::GOpCode::READ_DATA);
		clientRPC->SetReqIP(selfAddress);
		clientRPC->SetReqPort(clientPorts[targetServer]);
		clients[targetServer]->CallRPC(clientRPC);
		assert(clients[targetServer]->BlockPoll(clientRPC));
		response = clientRPC->GetStorageResponse()->mutable_readdata()->mutable_response();
		if (response->status() == Service::GStatus::OK) {
			value = response->value();
			version = response->version();
			if (snapshot) {
				assert(response->has_epoch());
				snapshotReadSet[key] = std::make_pair(response->epoch(), value);
			} else {
				readSet[key] = std::make_pair(version, value);
			}
			if (cacheEnabled) {
				UpdateDataCacheFromStorageService(key, *response);
			}
			delete clientRPC;
			return Service::GStatus::OK;
		} else {
			Service::GStatus status = response->status();
			assert(status != Service::GStatus::FORWARD_SERVER);
			delete clientRPC;
			return status;
		}
	} else {
		Service::GStatus status = response->status();
		delete clientRPC;
		return status;
	}
}

void ClientTranx::Write(std::string& key, std::string& value) {
	writeSet[key] = value;
}

bool ClientTranx::Commit() {
	if (!snapshotReadSet.empty()) {
		assert(readSet.empty() && writeSet.empty());
		uint64 epoch = -1;
		for (auto it = snapshotReadSet.begin(); it != snapshotReadSet.end(); ++it) {
			if (epoch == -1) {
				epoch = it->second.first;
			} else {
				if (epoch != it->second.first) {
					VERBOSE("snapshot commit fail");
					return false;
				}
			}
		}
		//VERBOSE("snapshot commit success");
		return true;
	}
	//The following output was used in the key distribution tests for btree and rtree
	//std::string tranxStr = PrintTranx();
	//VERBOSE("transaction string: %s", tranxStr.c_str());
	/*
	 * coordinator choosing
	 * 	random
	 * 	1PC smart
	 */
	assert(!cluster.empty());
	std::string coordIP = "";
	std::unordered_set<std::string> sharedServers(cluster.begin(), cluster.end());
	if (cacheEnabled && coordStrategy == CoordinatorStrategy::ONEPHASECOMMIT) {
		for (auto it = readSet.begin(); it != readSet.end(); ++it) {
			std::unordered_set<std::string> tmpServers = cache->GetAllMapping(it->first);
			if (tmpServers.empty()) {
				tmpServers.insert(GetMapping(it->first));
			}
			sharedServers = GetOverlappedIPs(sharedServers, tmpServers);
			if (sharedServers.empty()) {
				break;
			}
		}
		if (!sharedServers.empty()) {
			for (auto it = writeSet.begin(); it != writeSet.end(); ++it) {
				std::unordered_set<std::string> tmpServers = cache->GetAllMapping(it->first);
				if (tmpServers.empty()) {
					tmpServers.insert(GetMapping(it->first));
				}
				sharedServers = GetOverlappedIPs(sharedServers, tmpServers);
				if (sharedServers.empty()) {
					break;
				}
			}
		}
		if (!sharedServers.empty()) {
			coordIP = *(sharedServers.begin());
		}
	}
	if (coordIP.empty()) {
		coordIP = cluster[rand() % cluster.size()];
	}
	CreateClientIfNotExist(coordIP);
	RPC::ClientRPC* clientRPC = new RPC::ClientRPC(RPC::GServiceID::ClientService,
			clients[coordIP]);
	clientRPC->SetClientOpcode(Service::GOpCode::CLIENT_COMMIT);
	clientRPC->SetReqIP(selfAddress);
	clientRPC->SetReqPort(clientPorts[coordIP]);
	Service::GClientCommit_Request* request =
			clientRPC->GetClientRequest()->mutable_clientcommit()->mutable_request();
	request->set_tranxtype(Service::GTranxType::NORMAL);
	for (auto it = readSet.begin(); it != readSet.end(); ++it) {
		Service::GItem& item = *request->add_read_set();
		item.set_key(it->first);
		item.set_version(it->second.first);
	}
	for (auto it = writeSet.begin(); it != writeSet.end(); ++it) {
		Service::GItem& item = *request->add_write_set();
		item.set_key(it->first);
		item.set_value(it->second);
	}
	clients[coordIP]->CallRPC(clientRPC);
	assert(clients[coordIP]->BlockPoll(clientRPC));
	assert(clientRPC->GetRepStatus() == RPC::GStatus::OK);
	Service::GClientCommit_Response* response =
			clientRPC->GetClientResponse()->mutable_clientcommit()->mutable_response();
	if (response->status() == Service::GStatus::OK) {
		delete clientRPC;
		if (cacheEnabled) {
			UpdateDataCacheFromCommittedTranx();
		}
		return true;
	} else if (response->status() == Service::GStatus::CHANGE_COORD) {
		assert(response->has_coord());
		std::string newCoord = response->coord();
		//VERBOSE("sending commit to %s for 1PC", newCoord.c_str());
		if (cacheEnabled) {
			UpdateMappingCacheFromClientService(*response);
		}
		CreateClientIfNotExist(newCoord);
		delete clientRPC;
		clientRPC = new RPC::ClientRPC(RPC::GServiceID::ClientService, clients[newCoord]);
		clientRPC->SetClientOpcode(Service::GOpCode::CLIENT_COMMIT);
		clientRPC->SetReqIP(selfAddress);
		clientRPC->SetReqPort(clientPorts[newCoord]);
		request = clientRPC->GetClientRequest()->mutable_clientcommit()->mutable_request();
		request->set_tranxtype(Service::GTranxType::NORMAL);
		for (auto it = readSet.begin(); it != readSet.end(); ++it) {
			Service::GItem& item = *request->add_read_set();
			item.set_key(it->first);
			item.set_version(it->second.first);
		}
		for (auto it = writeSet.begin(); it != writeSet.end(); ++it) {
			Service::GItem& item = *request->add_write_set();
			item.set_key(it->first);
			item.set_value(it->second);
		}
		clients[newCoord]->CallRPC(clientRPC);
		assert(clients[newCoord]->BlockPoll(clientRPC));
		response = clientRPC->GetClientResponse()->mutable_clientcommit()->mutable_response();
		if (response->status() == Service::GStatus::OK) {
			delete clientRPC;
			if (cacheEnabled) {
				UpdateDataCacheFromCommittedTranx();
			}
			return true;
		} else {
			assert(response->status() != Service::GStatus::CHANGE_COORD);
			/*
			 * MaybeLater: smart reply with new versions
			 * different choices: update none and invalidate all; update partial failed keys and invalidate unknown,
			 * update partial failed keys and invalidate none, update all failed keys
			 */
			if (cacheEnabled) {
				for (auto it = readSet.begin(); it != readSet.end(); ++it) {
					cache->Invalidate(it->first);
				}
			}
			delete clientRPC;
			return false;
		}

	} else {
		/*
		 * MaybeLater: smart reply with new versions
		 * different choices: update none and invalidate all; update partial failed keys and invalidate unknown,
		 * update partial failed keys and invalidate none, update all failed keys
		 */
		if (cacheEnabled) {
			for (auto it = readSet.begin(); it != readSet.end(); ++it) {
				cache->Invalidate(it->first);
			}
		}
		delete clientRPC;
		return false;
	}
}

void ClientTranx::Abort() {
	if (cacheEnabled) {
		for (auto it = readSet.begin(); it != readSet.end(); ++it) {
			cache->Invalidate(it->first);
		}
	}
	Clear();
}

void ClientTranx::Clear() {
	readSet.clear();
	writeSet.clear();
	snapshotReadSet.clear();
}

void ClientTranx::ReadDebug(std::string& key) {
	std::string ip = GetMapping(key);
	CreateClientIfNotExist(ip);
	RPC::ClientRPC* clientRPC = new RPC::ClientRPC(RPC::GServiceID::StorageService, clients[ip]);
	Service::GReadData_Request* request =
			clientRPC->GetStorageRequest()->mutable_readdata()->mutable_request();
	request->set_key(key);
	request->set_snapshot(false);
	clientRPC->SetStorageOpcode(Service::GOpCode::READ_DATA);
	clientRPC->SetReqIP(selfAddress);
	clientRPC->SetReqPort(clientPorts[ip]);
	clients[ip]->CallRPC(clientRPC);
}

void ClientTranx::UpdateMappingCacheFromStorageService(std::string& key,
		Service::GReadData::Response& response) {
	//VERBOSE("calling UpdateMappingCacheFromStorageService for key %s", key.c_str());
	assert(cacheEnabled);
	const google::protobuf::RepeatedPtrField<std::string>& ipsProtobuf = response.newips();
	std::unordered_set<std::string> newips;
	for (auto it = ipsProtobuf.begin(); it != ipsProtobuf.end(); ++it) {
		newips.insert(*it);
	}
	assert(!newips.empty());
	cache->UpdateMapping(key, newips);
}

void ClientTranx::UpdateMappingCacheFromClientService(Service::GClientCommit::Response& response) {
	//VERBOSE("calling UpdateMappingCacheFromClientService for key %s", key.c_str());
	assert(cacheEnabled);
	for (auto it = response.mapitem().begin(); it != response.mapitem().end(); ++it) {
		std::unordered_set<std::string> newips;
		for (auto ip = it->ips().begin(); ip != it->ips().end(); ++ip) {
			newips.insert(*ip);
		}
		assert(!newips.empty());
		cache->UpdateMapping(it->key(), newips);
	}
}

void ClientTranx::UpdateDataCacheFromStorageService(std::string& key,
		Service::GReadData::Response& response) {
	//VERBOSE("calling UpdateDataCacheFromStorageService for key %s", key.c_str());
	assert(cacheEnabled);
	cache->UpdateLocal(key, response.value(), response.version());
}

void ClientTranx::UpdateDataCacheFromCommittedTranx() {
	for (auto writeItem = writeSet.begin(); writeItem != writeSet.end(); ++writeItem) {
		const std::string &key = writeItem->first;
		if (readSet.find(key) != readSet.end()) {
			uint64_t version = readSet[key].first;
			const std::string &value = writeItem->second;
			cache->UpdateLocal(key, value, version + 1);
		}
	}
}

std::string ClientTranx::GetMapping(const std::string& key) {
	uint64 which = Util::StringUtil::SDBMHash(key.c_str());
	return cluster[which % cluster.size()];
}

void ClientTranx::CreateClientIfNotExist(std::string ip) {
	if (clients.find(ip) == clients.end()) {
		clients[ip] = new Client(ip, routerFrontPort, context);
		while (!clients[ip]->Bind(selfAddressStr, selfClientPort))
			;
		assert(clientPorts.find(ip) == clientPorts.end());
		clientPorts[ip] = selfClientPort++;
	}
}

std::unordered_set<std::string> ClientTranx::GetOverlappedIPs(std::unordered_set<std::string> v1,
		std::unordered_set<std::string> v2) {
	std::unordered_set<std::string> overlapped;
	for (auto it = v1.begin(); it != v1.end(); ++it) {
		if (v2.find(*it) != v2.end()) {
			overlapped.insert(*it);
		}
	}
	return overlapped;
}

std::string ClientTranx::PrintTranx(){
	std::string outputStr = "readset: ";
	for(auto it=readSet.begin(); it!=readSet.end(); ++it){
		outputStr += it->first + ",";
	}
	outputStr += "writeset: ";
	for(auto it=writeSet.begin(); it!=writeSet.end(); ++it){
		outputStr += it->first + ',';
	}
	return outputStr;
}

} /* namespace Client */
} /* namespace DTranx */

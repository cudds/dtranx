/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <vector>
#include <unordered_set>
#include "gtest/gtest.h"
#include "DTranx/Client/ClientCache.h"

using namespace DTranx;
TEST(ClientCache, Mapping) {
	Client::ClientCache clientCache;
	EXPECT_EQ(Client::ClientCache::NO_SUCH_MAP, clientCache.GetMapping("key1"));
	std::unordered_set<std::string> newips;
	newips.insert("192.168.0.1");
	newips.insert("192.168.0.2");
	newips.insert("192.168.0.3");
	clientCache.UpdateMapping("key1", newips);
	std::string ipFromCache = clientCache.GetMapping("key1");
	std::unordered_set<std::string> newipsSet(newips.begin(), newips.end());
	EXPECT_TRUE(newipsSet.find(ipFromCache) != newipsSet.end());
	std::unordered_set<std::string> ipsFromCache = clientCache.GetAllMapping("key1");
	EXPECT_EQ(newips, ipsFromCache);
}

TEST(ClientCache, Data) {
	Client::ClientCache clientCache;
	uint64 version;
	EXPECT_EQ("", clientCache.ReadLocal("key1", version));
	EXPECT_EQ(Client::ClientCache::NO_SUCH_DATA, version);
	clientCache.UpdateLocal("key1", "value1", 23);
	EXPECT_EQ("value1", clientCache.ReadLocal("key1", version));
	EXPECT_EQ(23, version);
	clientCache.Invalidate("key1");
	EXPECT_EQ("", clientCache.ReadLocal("key1", version));
	EXPECT_EQ(Client::ClientCache::NO_SUCH_DATA, version);
	clientCache.Invalidate("key2");
}

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "DTranx/Util/Log.h"
#include "WriteBlockLockTable.h"
#include "Service/TranxService.h"
#include "Service/StorageService.h"
#include "Service/ClientService.h"
#include "DaemonStages/Repartition.h"
#include "DaemonStages/TranxAck.h"
#include "DaemonStages/DaemonStagesMock.h"
#include "Server/RecoverLog.h"

namespace DTranx {
namespace Stages {
namespace Lock {

//TODO: for WriteBlockLockTable, response time might be long for the ones that blocked in the current stage thread
// and it's been pushed to itself over and over again.

uint64 WriteBlockLockTable::_lockTimeout = std::stoll(
		Util::StaticConfig::GetInstance()->Read("LockTimeout"));

WriteBlockLockTable::WriteBlockLockTable(SharedResources::TranxServerInfo *tranxServerInfo,
		int numOfStageThreads)
		: BasicLockTable(tranxServerInfo, numOfStageThreads, "WriteBlockLockTable") {
	abortBeforePrepareTranx.reserve(500);
}

WriteBlockLockTable::~WriteBlockLockTable() noexcept(false) {
}

bool WriteBlockLockTable::RequestEpochLock(const Util::TranxID& tranxID,
		TranxServiceQueueElement *element, BasicLockTableLockMode lockMode) {
	//VERBOSE("tranxID %d#%lu: request epoch", tranxID.GetNodeID(), tranxID.GetTranxID());
	if (element == NULL || !element->isLockEpochAlreadyLocked()) {
		if (!_RequestEpochLock(tranxID, lockMode)) {
			//VERBOSE("tranxID %d#%lu: request epoch failure", tranxID.GetNodeID(),
			//		tranxID.GetTranxID());
			return false;
		} else if (element != NULL) {
			element->setLockEpochAlreadyLocked(true);
		}
	}
	return true;
}

bool WriteBlockLockTable::RequestLock(const std::string& key, const Util::TranxID& tranxID,
		BasicLockTableLockMode lockMode) {
	return _RequestLock(key, tranxID, lockMode, lockMode == BasicLockTableLockMode::Exclusive);
}

bool WriteBlockLockTable::RequestLocks(
		std::unordered_map<std::string, BasicLockTableLockMode>& keys_mode,
		const Util::TranxID& tranxID, TranxServiceQueueElement *element) {
	//VERBOSE("tranxID %d#%lu: request locks", tranxID.GetNodeID(),
	//		tranxID.GetTranxID());
	int curIndex = 0;
	bool allSuccessful = true;

	for (auto it = keys_mode.begin(); it != keys_mode.end(); ++it) {
		if (element != NULL && element->isLockAlreadyLockedKeys(it->first)) {
			continue;
		}
		if (!RequestLock(it->first, tranxID, it->second)) {
			//VERBOSE("Tranx %d#%lu request lock for key %s failed",
			//		element->GetTranxID().GetNodeID(), element->GetTranxID().GetTranxID(),
			//		it->first.c_str());
			return false;
		}
		if (element != NULL) {
			element->addLockAlreadyLockedKeys(it->first);
		}
	}
	return true;
}

bool WriteBlockLockTable::RequestLocksBlocking(
		std::unordered_map<std::string, BasicLockTableLockMode>& keys_mode,
		const Util::TranxID& tranxID, TranxServiceQueueElement *element) {
	//VERBOSE("tranxID %d#%lu: request locks", tranxID.GetNodeID(),
	//		tranxID.GetTranxID());
	int curIndex = 0;
	for (auto it = keys_mode.begin(); it != keys_mode.end(); ++it) {
		if (element != NULL && element->isLockAlreadyLockedKeys(it->first)) {
			continue;
		}
		if (!RequestLock(it->first, tranxID, it->second)) {
			return false;
		}
		if (element != NULL) {
			element->addLockAlreadyLockedKeys(it->first);
		}
	}
	return true;
}

bool WriteBlockLockTable::ReleaseEpochLock(const Util::TranxID& tranxID) {
	//VERBOSE("tranxID %d#%lu: releasing epoch", tranxID.GetNodeID(), tranxID.GetTranxID());
	return _ReleaseEpochLock(tranxID);
}

bool WriteBlockLockTable::ReleaseLock(const std::string& key, const Util::TranxID& tranxID) {
	return _ReleaseLock(key, tranxID);
}

bool WriteBlockLockTable::ReleaseLocks(std::vector<std::string>& keys,
		const Util::TranxID& tranxID) {
	//VERBOSE("tranxID %d#%lu: releasing locks", tranxID.GetNodeID(), tranxID.GetTranxID());
	bool allSuccessful = true;
	int curIndex = 0;
	for (int curIndex = 0; curIndex < keys.size(); curIndex++) {
		if (!ReleaseLock(keys[curIndex], tranxID)) {
			allSuccessful = false;
		}
	}
	if (allSuccessful) {
		return true;
	}
	return false;
}

bool WriteBlockLockTable::ReleaseLocks(std::unordered_set<std::string>& keys,
		const Util::TranxID& tranxID) {
	//VERBOSE("tranxID %d#%lu: releasing locks", tranxID.GetNodeID(), tranxID.GetTranxID());
	bool allSuccessful = true;
	int curIndex = 0;
	for (auto it = keys.begin(); it != keys.end(); ++it) {
		if (!ReleaseLock(*it, tranxID)) {
			allSuccessful = false;
		}
	}
	if (allSuccessful) {
		return true;
	}
	return false;
}

bool WriteBlockLockTable::ReleaseLocks(
		std::unordered_map<std::string, BasicLockTableLockMode>& keys_mode,
		const Util::TranxID& tranxID) {
	std::vector<std::string> keys;
	for (auto it = keys_mode.begin(); it != keys_mode.end(); ++it) {
		keys.push_back(it->first);
	}
	return ReleaseLocks(keys, tranxID);
}

void WriteBlockLockTable::DispatchThread() {
	NOTICE("TranxLockThread Dispatch started");
	uint64_t randSeed = 0;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateThread.load()) {
			NOTICE("TranxLockThread Dispatch is reclaimed");
			break;
		}
		TranxServiceQueueElement *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = dispatchQueue.try_dequeue_bulk(tmpRPCs,
				TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1) {
				AddQueueLengthSample(5, dispatchQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			TranxServiceQueueElement *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {

				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(5);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(5);
				std::vector<int> taskLongLatencySamples = GetLongLatency(5);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_dispatch");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				std::set<int> involvedStageThreads;
				for (int i = 1; i <= numOfStageThreads; ++i) {
					involvedStageThreads.insert(i);
				}
				element->setSplittedInvolvedStageThreads(involvedStageThreads);
				element->setTerminateInThisStage(true);
				SendToStage(element, 0);
				continue;
			}
			//element->goThroughStage("tranxlock dispatch");
			WriteBlockLockTableStageTask task = element->getLockTask();
			if (task == _REQUESTLOCKSBLOCKING) {
				if (element->getLockStartTimePoint() == TranxServiceQueueElement::EPOCH_TIMEPOINT) {
					element->setLockStartTimePoint(boost::chrono::steady_clock::now());
				}
				const std::unordered_map<std::string, Lock::BasicLockTableLockMode>& keysMode =
						element->getKeysMode();
				std::set<int> involvedStageThreads;
				for (auto it = keysMode.begin(); it != keysMode.end(); ++it) {
					involvedStageThreads.insert(GetThreadByKey(it->first));
				}
				element->setSplittedInvolvedStageThreads(involvedStageThreads);
				LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
				SendToStage(element, numOfStageThreads);
			} else if (task == _REQUESTEPOCHLOCK) {
				if (element->getLockStartTimePoint() == TranxServiceQueueElement::EPOCH_TIMEPOINT) {
					element->setLockStartTimePoint(boost::chrono::steady_clock::now());
				}
				LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
				SendToStage(element, numOfStageThreads);
			} else if (task == _REQUESTLOCKSMAP) {
				if (element->getLockStartTimePoint() == TranxServiceQueueElement::EPOCH_TIMEPOINT) {
					element->setLockStartTimePoint(boost::chrono::steady_clock::now());
				}
				std::unordered_map<std::string, Lock::BasicLockTableLockMode> keysMode =
						element->getKeysMode();
				std::set<int> involvedStageThreads;
				for (auto it = keysMode.begin(); it != keysMode.end(); ++it) {
					involvedStageThreads.insert(GetThreadByKey(it->first));
				}
				element->setSplittedInvolvedStageThreads(involvedStageThreads);
				LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
				SendToStage(element, numOfStageThreads);
			} else if (task == _RELEASELOCKSMAP) {
				std::unordered_map<std::string, Lock::BasicLockTableLockMode> keysMode =
						element->getKeysMode();
				std::set<int> involvedStageThreads;
				for (auto it = keysMode.begin(); it != keysMode.end(); ++it) {
					involvedStageThreads.insert(GetThreadByKey(it->first));
				}
				element->setSplittedInvolvedStageThreads(involvedStageThreads);
				LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
				SendToStage(element, numOfStageThreads);
			} else if (task == _RELEASELOCKSVECTOR) {
				std::vector<std::string> keys = element->getKeys();
				std::set<int> involvedStageThreads;
				for (auto it = keys.begin(); it != keys.end(); ++it) {
					involvedStageThreads.insert(GetThreadByKey(*it));
				}
				element->setSplittedInvolvedStageThreads(involvedStageThreads);
				LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
				SendToStage(element, numOfStageThreads);
			} else if (task == _RELEASEEPOCHLOCK) {
				LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
				SendToStage(element, numOfStageThreads);
			} else if (task == _PRINTLOCKSTATUS) {
				std::set<int> involvedStageThreads;
				for (int i = 1; i < numOfStageThreads; ++i) {
					involvedStageThreads.insert(i);
				}
				element->setSplittedInvolvedStageThreads(involvedStageThreads);
				LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
				SendToStage(element, 0);
			} else {
				assert(false);
			}
			/*
			 * TODO: support GC dispatch
			 */
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

bool WriteBlockLockTable::LockFailureRelease(TranxServiceQueueElement *element,
		std::unordered_map<std::string, Lock::BasicLockTableLockMode>& chosenKeysMode,
		int stageThreadIndex) {
	const Util::TranxID& tranxID = element->GetTranxID();
	WriteBlockLockTableStageTask task = element->getLockTask();
	element->setLockObtained(false);
	const std::unordered_set<std::string>& alreadyLockedKeys = element->getLockAlreadyLockedKeys();
	element->clearSplittedInvolvedStageThreads();
	std::unordered_set<std::string> alreadyLockedKeysOfThisThread;
	for (auto lockedKey = alreadyLockedKeys.begin(); lockedKey != alreadyLockedKeys.end();
			++lockedKey) {
		if (chosenKeysMode.find(*lockedKey) != chosenKeysMode.end()) {
			alreadyLockedKeysOfThisThread.insert(*lockedKey);
		}
	}
	ReleaseLocks(alreadyLockedKeysOfThisThread, tranxID);

	for (auto key = chosenKeysMode.begin(); key != chosenKeysMode.end(); ++key) {
		if (alreadyLockedKeysOfThisThread.find(key->first) == alreadyLockedKeysOfThisThread.end()) {
			_CancelRequest(key->first, tranxID);
		}
	}

	std::set<int> involvedStageThreads;
	for (auto lockedKey = alreadyLockedKeys.begin(); lockedKey != alreadyLockedKeys.end();
			++lockedKey) {
		int threadID = GetThreadByKey(*lockedKey);
		if (threadID != stageThreadIndex) {
			involvedStageThreads.insert(threadID);
		}
	}
	if (element->isLockEpochAlreadyLocked()) {
		if (numOfStageThreads == 1) {
			ReleaseEpochLock(tranxID);
		} else if (numOfStageThreads != stageThreadIndex) {
			involvedStageThreads.insert(numOfStageThreads);
		} else {
			ReleaseEpochLock(tranxID);
		}
	}
	if (!involvedStageThreads.empty()) {
		int nextStageThread = *involvedStageThreads.begin();
		involvedStageThreads.erase(involvedStageThreads.begin());
		element->setSplittedInvolvedStageThreads(involvedStageThreads);
		element->setLockTask(_RELEASELOCKSWHENFAIL);
		LeaveStage(element, stageThreadIndex, task);
		SendToStage(element, nextStageThread);
		return false;
	}
	return true;
}

void WriteBlockLockTable::StageThread(int stageThreadIndex) {
	NOTICE("TranxLockThread %d started", stageThreadIndex);
	uint64_t randSeed = 0;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateThread.load()) {
			NOTICE("TranxLockThread %d is reclaimed", stageThreadIndex);
			break;
		}
		TranxServiceQueueElement *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = 0;

		if (numOfStageThreads > 1) {
			count = processQueues[stageThreadIndex].try_dequeue_bulk(tmpRPCs,
					TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
			if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
				if (randSeed++ % queuePerfFreq == 1) {
					AddQueueLengthSample(stageThreadIndex,
							processQueues[stageThreadIndex].size_approx());
				}
			}
		} else {
			count = processQueue.try_dequeue_bulk(tmpRPCs,
					TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
			if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
				if (randSeed++ % queuePerfFreq == 1) {
					AddQueueLengthSample(0, processQueue.size_approx());
				}
			}
		}
		for (int i = 0; i < count; ++i) {
			TranxServiceQueueElement *element = tmpRPCs[i];
			//element->goThroughStage("tranxlock" + std::to_string(stageThreadIndex));
			WriteBlockLockTableStageTask task = element->getLockTask();
			const Util::TranxID& tranxID = element->GetTranxID();
			if (element->isIsPerfOutPutTask()) {
				/*
				 * the setTerminate is necessary because it might've skipped the dispatch because the numOfStageThreads is 1
				 */
				element->setTerminateInThisStage(true);
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(
						stageThreadIndex);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(
						stageThreadIndex);
				std::vector<int> taskLongLatencySamples = GetLongLatency(stageThreadIndex);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(
						stageName + std::to_string(stageThreadIndex));
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
			} else {
				if (task == _REQUESTLOCKSBLOCKING) {
					if (element->getLockStartTimePoint()
							== TranxServiceQueueElement::EPOCH_TIMEPOINT) {
						element->setLockStartTimePoint(boost::chrono::steady_clock::now());
					}
					const std::unordered_map<std::string, Lock::BasicLockTableLockMode>& allKeysMode =
							element->getKeysMode();
					std::unordered_map<std::string, Lock::BasicLockTableLockMode> chosenKeysMode;
					for (auto keyMode = allKeysMode.begin(); keyMode != allKeysMode.end();
							++keyMode) {
						if (GetThreadByKey(keyMode->first) == stageThreadIndex) {
							chosenKeysMode[keyMode->first] = keyMode->second;
						}
					}
					if (RequestLocksBlocking(chosenKeysMode, tranxID, element)) {
						element->setLockObtained(true);
					} else {
						if (numOfStageThreads > 1) {
							//assert(processQueues[stageThreadIndex].enqueue(element));
							while (!processQueues[stageThreadIndex].try_enqueue(element)) {
								if (processQueues[stageThreadIndex].enqueue(element)) {
									break;
								}
							}
						} else {
							//assert(processQueue.enqueue(element));
							while (!processQueue.try_enqueue(element)) {
								if (processQueue.enqueue(element)) {
									break;
								}
							}
						}
						continue;
					}
				} else if (task == _REQUESTEPOCHLOCK) {
					if (numOfStageThreads > 1) {
						assert(stageThreadIndex == numOfStageThreads);
					}
					if (RequestEpochLock(tranxID, element)) {
						element->setLockObtained(true);
					} else {
						if (numOfStageThreads > 1) {
							while (!processQueues[stageThreadIndex].try_enqueue(element)) {
								if (processQueues[stageThreadIndex].enqueue(element)) {
									break;
								}
							}
						} else {
							while (!processQueue.try_enqueue(element)) {
								if (processQueue.enqueue(element)) {
									break;
								}
							}
						}
						continue;
					}
				} else if (task == _REQUESTLOCKSMAP) {
					if (element->getLockStartTimePoint()
							== TranxServiceQueueElement::EPOCH_TIMEPOINT) {
						element->setLockStartTimePoint(boost::chrono::steady_clock::now());
					}
					const std::unordered_map<std::string, Lock::BasicLockTableLockMode>& allKeysMode =
							element->getKeysMode();
					std::unordered_map<std::string, Lock::BasicLockTableLockMode> chosenKeysMode;
					bool hasExclusiveLockRequests = false;
					for (auto keyMode = allKeysMode.begin(); keyMode != allKeysMode.end();
							++keyMode) {
						if (GetThreadByKey(keyMode->first) == stageThreadIndex) {
							chosenKeysMode[keyMode->first] = keyMode->second;
							if (keyMode->second == Lock::BasicLockTableLockMode::Exclusive) {
								hasExclusiveLockRequests = true;
							}
						}
					}

					if (abortBeforePrepareTranx.find(tranxID) != abortBeforePrepareTranx.end()) {
						/*
						 * when this prepare request might come after abort request,
						 * do not lock anything
						 */
						if (!LockFailureRelease(element, chosenKeysMode, stageThreadIndex)) {
							abortBeforePrepareTranx.erase(tranxID);
							continue;
						}
						abortBeforePrepareTranx.erase(tranxID);
					} else {
						bool lockSucc = false;
						if (numOfStageThreads > 1 && stageThreadIndex == numOfStageThreads) {
							/*
							 * epoch thread in multi threads
							 */
							lockSucc = RequestEpochLock(tranxID, element,
									BasicLockTableLockMode::Shared);
						} else if (numOfStageThreads == 1) {
							lockSucc = RequestEpochLock(tranxID, element,
									BasicLockTableLockMode::Shared);
							if (lockSucc) {
								lockSucc = RequestLocks(chosenKeysMode, tranxID, element);
							}
						} else if (numOfStageThreads > 1 && stageThreadIndex != numOfStageThreads) {
							lockSucc = RequestLocks(chosenKeysMode, tranxID, element);
						}

						if (lockSucc) {
							element->setLockObtained(true);
						} else {
							boost::chrono::steady_clock::time_point now =
									boost::chrono::steady_clock::now();

							uint64 timePassed = boost::chrono::duration_cast<
									boost::chrono::nanoseconds>(
									now - element->getLockStartTimePoint()).count();

							if (!hasExclusiveLockRequests || timePassed > _lockTimeout) {
								if (!LockFailureRelease(element, chosenKeysMode,
										stageThreadIndex)) {
									continue;
								}
							} else {
								if (numOfStageThreads > 1) {
									//assert(processQueues[stageThreadIndex].enqueue(element));
									while (!processQueues[stageThreadIndex].try_enqueue(element)) {
										if (processQueues[stageThreadIndex].enqueue(element)) {
											break;
										}
									}
								} else {
									//assert(processQueue.enqueue(element));
									while (!processQueue.try_enqueue(element)) {
										if (processQueue.enqueue(element)) {
											break;
										}
									}
								}
								//VERBOSE("tranxID %d#%lu again try to lock", tranxID.GetNodeID(),
								//		tranxID.GetTranxID());
								continue;
							}
						}
					}
				} else if (task == _RELEASELOCKSMAP) {
					const std::unordered_map<std::string, Lock::BasicLockTableLockMode>& allKeysMode =
							element->getKeysMode();
					std::unordered_map<std::string, Lock::BasicLockTableLockMode> chosenKeysMode;
					for (auto keyMode = allKeysMode.begin(); keyMode != allKeysMode.end();
							++keyMode) {
						if (GetThreadByKey(keyMode->first) == stageThreadIndex) {
							chosenKeysMode[keyMode->first] = keyMode->second;
						}
					}
					ReleaseLocks(chosenKeysMode, tranxID);
					if (numOfStageThreads == 1 || stageThreadIndex == numOfStageThreads) {
						ReleaseEpochLock(tranxID);
					}
				} else if (task == _RELEASELOCKSVECTOR) {
					const std::vector<std::string>& allKeys = element->getKeys();
					std::vector<std::string> chosenKeys;
					for (auto key = allKeys.begin(); key != allKeys.end(); ++key) {
						if (GetThreadByKey(*key) == stageThreadIndex) {
							chosenKeys.push_back(*key);
						}
					}
					if (element->GetOpcode() == Service::GOpCode::TRANX_ABORT) {
						abortBeforePrepareTranx.insert(tranxID);
					}
					ReleaseLocks(chosenKeys, tranxID);
					if (numOfStageThreads == 1 || stageThreadIndex == numOfStageThreads) {
						ReleaseEpochLock(tranxID);
					}
				} else if (task == _RELEASEEPOCHLOCK) {
					if (numOfStageThreads > 1) {
						assert(stageThreadIndex == numOfStageThreads);
					}
					ReleaseEpochLock(tranxID);
				} else if (task == _PRINTLOCKSTATUS) {
					element->addLockStatus(PrintLockStatus(stageThreadIndex));
				} else if (task == _RELEASELOCKSWHENFAIL) {
					if (numOfStageThreads > 1 && numOfStageThreads == stageThreadIndex) {
						ReleaseEpochLock(tranxID);
					} else {
						const std::unordered_set<std::string>& alreadyLockedKeys =
								element->getLockAlreadyLockedKeys();
						std::vector<std::string> chosenKeys;
						for (auto lockedKey = alreadyLockedKeys.begin();
								lockedKey != alreadyLockedKeys.end(); ++lockedKey) {
							if (GetThreadByKey(*lockedKey) == stageThreadIndex) {
								chosenKeys.push_back(*lockedKey);
							}
						}
						ReleaseLocks(chosenKeys, tranxID);
					}
				} else if (task == _CLEANBLACKLIST) {
					int nodeIDGC =
							element->GetServerRPC()->GetTranxRequest()->mutable_gc()->nodeid();
					uint64_t tranxIDGC =
							element->GetServerRPC()->GetTranxRequest()->mutable_gc()->tranxid();

					for (auto it = abortBeforePrepareTranx.begin();
							it != abortBeforePrepareTranx.end();) {
						if (it->GetNodeID() == nodeIDGC && it->GetTranxID() <= tranxIDGC) {
							it = abortBeforePrepareTranx.erase(it);
						} else {
							++it;
						}
					}
					//VERBOSE("blacklist after cleaning is %zu", abortBeforePrepareTranx.size());
				} else {
					assert(false);
				}
			}
			/*
			 * first check if there are still some stage threads that this element should go through
			 * 	if complete, send back to the daemonstage/service
			 */
			int nextStageThread = element->popSplittedNextInvolvedStageThreads();
			if (nextStageThread != TranxServiceQueueElement::INVALID_STAGE_THREADID) {
				LeaveStage(element, stageThreadIndex, task);
				SendToStage(element, nextStageThread);
			} else {
				TranxServiceQueueElement::StageID stageID = element->getStageId();
				if (element->isTerminateInThisStage()) {
					//VERBOSE("terminate in lock");
					if (stageID == Stages::TranxServiceQueueElement::StageID::TRANXSERVICE) {
						tranxService->AddToFreeList(element);
					} else if (stageID
							== Stages::TranxServiceQueueElement::StageID::CLIENTSERVICE) {
						clientService->AddToFreeList(element);
					} else if (stageID
							== Stages::TranxServiceQueueElement::StageID::STORAGESERVICE) {
						storageService->AddToFreeList(element);
					} else if (stageID == Stages::TranxServiceQueueElement::StageID::TRANXACK) {
						/*
						 * tranxAck elements were originally created in ClientService
						 */
						clientService->AddToFreeList(element);
					} else {
						delete element;
					}
					continue;
				}
				LeaveStage(element, stageThreadIndex, task);
				element->clearLockInternalStates();
				if (stageID == Stages::TranxServiceQueueElement::StageID::TRANXSERVICE) {
					tranxService->SendToServiceInternal(element);
				} else if (stageID == Stages::TranxServiceQueueElement::StageID::CLIENTSERVICE) {
					clientService->SendToServiceInternal(element);
				} else if (stageID == Stages::TranxServiceQueueElement::StageID::STORAGESERVICE) {
					storageService->SendToServiceInternal(element);
				} else if (stageID == Stages::TranxServiceQueueElement::StageID::TRANXACK) {
					tranxAck->SendToDaemon(element);
				} else if (stageID == Stages::TranxServiceQueueElement::StageID::REPARTITION) {
					repartition->SendToDaemon(element);
				} else if (stageID == Stages::TranxServiceQueueElement::StageID::RECOVERY) {
					postRecovery->SendToDaemon(element);
				} else if (stageID == Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON) {
					daemonStagesMock->SendToDaemon(element);
				} else {
					assert(false);
				}
			}
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}
}/* namespace Lock */
} /* namespace Stages */
} /* namespace DTranx */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * idx_t is for integer quantities(real_t is for floating point quantities)
 */
#include "metis.h"
#include <iostream>

int main(void){
	/*
	 * nvtxs is the number of vertices
	 * ncon is the number of weights associated with each vertex
	 * nparts is the number of parts to partition the graph
	 */
	idx_t nvtxs = 7;
	idx_t ncon = 1;
	idx_t nparts = 3;
	/*
	 * xadj is the indices to the adjncy list for each vertex
	 * the size of xadj is the number of vertices
	 *
	 * vwgt can store the weights of the vertices, it contains n*ncon elements
	 *
	 * adjwgt can store the weights of the edges, i'th element corresponds to i'th element in adjncy
	 */
	idx_t xadj[8] = {0, 3, 6, 10, 14, 17, 20, 22};
	idx_t adjncy[22];
	adjncy[0] = 4; adjncy[1] = 2; adjncy[2] = 1;
	adjncy[3] = 0; adjncy[4] = 2; adjncy[5] = 3;
	adjncy[6] = 4; adjncy[7] = 3; adjncy[8] = 1; adjncy[9] = 0;
	adjncy[10] = 1; adjncy[11] = 2; adjncy[12] = 5; adjncy[13] = 6;
	adjncy[14] = 0; adjncy[15] = 2; adjncy[16] = 5;
	adjncy[17] = 4; adjncy[18] = 3; adjncy[19] = 6;
	adjncy[20] = 5; adjncy[21] = 3;		
	/*
	 * objval stores the edge-cut or the total communication volume of the partitioning solution upon successful completion
	 * part is a vector of size nvtxs that stores the partition vector of the graph upon successful completion
	 */
	idx_t objval;
	idx_t part[7];
	/*
	idx_t options[METIS_NOPTIONS];
	options[METIS_OPTION_PTYPE] = METIS_PTYPE_KWAY;
	options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT;
	options[METIS_OPTION_CTYPE] = METIS_CTYPE_SHEM;
	options[METIS_OPTI_RTYPE] = METIS_RTYPE_GREEDY;
	options[METIS_OPTION_IPTYPE] = METIS_IPTYPE_RANDOM;
	options[METIS_OPTION_DBGLVL] = 0;
	options[METIS_OPTION_UFACTOR] =  1.030;
	options[METIS_OPTION_NO2HOP] = 1;
	options[METIS_OPTION_MINCONN] = 0;
	options[METIS_OPTION_CONTIG] = 0;
	options[METIS_OPTION_SEED] = -1;
	options[METIS_OPTION_NITER] = 10;
	options[METIS_OPTION_NCUTS] = 1;
	options[METIS_OPTION_NUMBERING] = 0;
	*/
	if(METIS_OK == METIS_PartGraphKway(&nvtxs, &ncon, xadj, adjncy, NULL, NULL, NULL, &nparts, NULL, NULL, NULL, &objval, part)){
		printf("total edge-cut/communication cost is %d\nAssigment:\n", (int)objval);
		for(int i=0; i<7; i++){
			printf("%d ",(int)part[i]);
		}
		printf("\n");
		return 0;
	}
	return 0;
}


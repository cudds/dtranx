/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef _DTRANX_UTIL_TYPE_H_
#define _DTRANX_UTIL_TYPE_H_

#include <cstdint>

namespace DTranx{

typedef int8_t	int8;
typedef int16_t int16;
typedef int32_t int32;
typedef int64_t int64;
typedef uint8_t	uint8;
typedef uint16_t uint16;
typedef uint32_t uint32;
typedef uint64_t uint64;

} /* namespace DTranx */

#endif /* _DTRANX_UTIL_TYPE_H_ */

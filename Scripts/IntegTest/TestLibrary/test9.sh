#!/bin/bash
function test9(){
	RestartDtranxClean dtranx1
	RestartDtranxClean dtranx2
	RestartDtranxClean dtranx3
	sudo docker exec dtranx1 bash -c '/dtranx/SimpleTranx -p Workloads/1pc &> /dtranx/test9_forwarding'
	sudo docker cp dtranx1:/dtranx/test9_forwarding ./test9_shouldcontainforwarding
	sudo docker cp dtranx2:/dtranx/DTranx.out ./test9_newtransaction
	sudo docker exec dtranx1 bash -c 'rm /dtranx/test9_forwarding'
}
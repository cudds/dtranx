/*
 *  Author: Ning Gao(nigo9731@colorado.edu)
 *  Storage is a thread-safe local data store, providing read/write accesses with monotonically increasing version number.
 *  key: key#epoch
 *  value: version#value
 *  epoch data is stored at DB as key "epoch"
 */

#ifndef DTRANX_STAGES_STORAGE_H_
#define DTRANX_STAGES_STORAGE_H_

#include <unordered_map>
#include <unordered_set>
#include <mutex>
#include <vector>
#include <atomic>
#include <queue>
#include <set>
#include <algorithm>
#include <leveldb/db.h>
#include <leveldb/cache.h>
#include <leveldb/write_batch.h>
#include "DTranx/Util/types.h"
#include "Util/Exceptions/NotFoundException.h"
#include "Util/StaticConfig.h"
#include "DTranx/Util/Log.h"
#include "Util/StringUtil.h"
#include "MidStage.h"

namespace DTranx {
namespace Stages {

/*
 * IMPORTANT: do not use this DELIMITER in application keys
 * DELIMITER is used in Storage to separate key from epoch and version from value
 *
 * default value for currentThreadIndex is used by the recoverlog to automatically detect the database index
 */
const char DELIMITER = '#';
const std::string EPOCHKEY = "epoch";
const uint64 NO_PREVIOUS_EPOCH = std::stoull(Util::StaticConfig::GetInstance()->Read("FirstEpoch"));

class StringStorage: public MidStage {
public:
	StringStorage(SharedResources::TranxServerInfo *tranxServerInfo, int numOfStageThreads = 1);

	~StringStorage() noexcept(false) {
		for (int i = 0; i < numOfStageThreads; ++i) {
			if (db[i] != NULL) {
				delete db[i];
			}
			delete options[i].block_cache;
		}
	}

	std::string CombineKey(const std::string& key, int currentThreadIndex =
			TranxServiceQueueElement::INVALID_STAGE_THREADID) const {
		if (currentThreadIndex == TranxServiceQueueElement::INVALID_STAGE_THREADID) {
			currentThreadIndex = GetThreadByKey(key);
		}
		assert(currentThreadIndex < numOfStageThreads);
		std::string combinedStr;
		combinedStr.reserve(key.size() + 1 + 8 + 2);
		combinedStr += key;
		combinedStr += DELIMITER;
		combinedStr += std::to_string(currentEpoch[currentThreadIndex]);
		return combinedStr;
	}

	static void SeparateKey(const std::string& combinedKey, uint64 &epoch, std::string &key) {
		size_t pos = combinedKey.find_last_of(DELIMITER);
		if (pos == std::string::npos) {
			//VERBOSE("combinedKey is %s", combinedKey.c_str());
			assert(false);
		}
		assert(pos != 0);
		assert(pos != combinedKey.size() - 1);
		key = combinedKey.substr(0, pos);
		epoch = std::strtoull(combinedKey.substr(pos + 1).c_str(), NULL, 10);
	}

	std::string CombineVersion(uint64 version, std::string value) const {
		std::string combinedStr;
		combinedStr.reserve(value.size() + 8 + 2);
		combinedStr += std::to_string(version);
		combinedStr += DELIMITER;
		combinedStr += value;
		return combinedStr;
	}

	void SeparateVersion(const std::string& combinedValue,
			uint64 &version,
			std::string &value) const {
		size_t pos = combinedValue.find_first_of(DELIMITER);
		assert(pos != std::string::npos);
		assert(pos != 0);
		assert(pos != combinedValue.size() - 1);
		value = combinedValue.substr(pos + 1);
		version = std::strtoull(combinedValue.substr(0, pos).c_str(), NULL, 10);
	}

	/*
	 * return at most two
	 */
	static bool sortFunc(const std::pair<std::string, std::string> &a,
			const std::pair<std::string, std::string> &b) {
		if (a.first.size() < b.first.size()) {
			return true;
		}
		std::string key;
		uint64 aInt, bInt;
		SeparateKey(a.first, aInt, key);
		SeparateKey(b.first, bInt, key);
		return aInt < bInt;
	}

	std::vector<std::pair<std::string, std::string> > searchAll(const std::string& key,
			int currentThreadIndex = TranxServiceQueueElement::INVALID_STAGE_THREADID) {
		if (currentThreadIndex == TranxServiceQueueElement::INVALID_STAGE_THREADID) {
			currentThreadIndex = GetThreadByKey(key);
		}
		assert(currentThreadIndex < numOfStageThreads);
		std::vector<std::pair<std::string, std::string> > result;
		std::string searchKey = key + DELIMITER;
		leveldb::Iterator* db_it = db[currentThreadIndex]->NewIterator(leveldb::ReadOptions());
		bool doExist = false;
		for (db_it->Seek(searchKey); db_it->Valid() && db_it->key().starts_with(searchKey);
				db_it->Next()) {
			result.push_back(std::make_pair(db_it->key().ToString(), db_it->value().ToString()));
		}
		delete db_it;
		if (result.size() <= 2) {
			//VERBOSE("Storage: less than 2, returning");
			return result;
		}
		leveldb::WriteOptions writeOptions;
		writeOptions.sync = true;
		std::vector<std::pair<std::string, std::string> > resultTrim;
		std::sort(result.begin(), result.end(), sortFunc);
		size_t size = result.size();
		for (int i = 0; i < size; ++i) {
			if (i >= size - 2) {
				//VERBOSE("Storage: pushing back to result %s", result[i].first.c_str());
				resultTrim.push_back(result[i]);
			} else {
				//VERBOSE("Storage: deleting %s", result[i].first.c_str());
				db[currentThreadIndex]->Delete(writeOptions, result[i].first);
			}
		}
		return resultTrim;
	}

	bool Exist(const std::string& key, int currentThreadIndex =
			TranxServiceQueueElement::INVALID_STAGE_THREADID) {
		if (currentThreadIndex == TranxServiceQueueElement::INVALID_STAGE_THREADID) {
			currentThreadIndex = GetThreadByKey(key);
		}
		assert(currentThreadIndex < numOfStageThreads);
		std::string CombinedKey = CombineKey(key, currentThreadIndex);
		uint64 hashedKey = Util::StringUtil::SDBMHash(key.c_str());

		if (cache[currentThreadIndex].find(key) != cache[currentThreadIndex].end()) {
			return true;
		}
		std::vector<std::pair<std::string, std::string> > result = searchAll(key,
				currentThreadIndex);
		if (result.size() != 0) {
			uint64 version;
			std::string value;
			SeparateVersion(result.back().second, version, value);
			cache[currentThreadIndex][key] = std::make_pair(version, value);
			return true;
		}
		return false;
	}

	uint64 GetVersion(const std::string& key, int currentThreadIndex =
			TranxServiceQueueElement::INVALID_STAGE_THREADID) {
		if (currentThreadIndex == TranxServiceQueueElement::INVALID_STAGE_THREADID) {
			currentThreadIndex = GetThreadByKey(key);
		}
		assert(currentThreadIndex < numOfStageThreads);
		std::string CombinedKey = CombineKey(key, currentThreadIndex);
		uint64 hashedKey = Util::StringUtil::SDBMHash(key.c_str());

		if (cache[currentThreadIndex].find(key) != cache[currentThreadIndex].end()) {
			return cache[currentThreadIndex][key].first;
		}
		std::vector<std::pair<std::string, std::string> > result = searchAll(key,
				currentThreadIndex);

		if (result.size() != 0) {
			uint64 version;
			std::string value;
			SeparateVersion(result.back().second, version, value);
			cache[currentThreadIndex][key] = std::make_pair(version, value);
			return version;
		}
		throw Util::NotFoundException("key " + key + " not found in GetVersion");
	}

	bool ReadSnapshot(const std::string& key,
			uint64& epoch,
			std::string& value,
			int currentThreadIndex = TranxServiceQueueElement::INVALID_STAGE_THREADID) {
		if (currentThreadIndex == TranxServiceQueueElement::INVALID_STAGE_THREADID) {
			currentThreadIndex = GetThreadByKey(key);
		}
		assert(currentThreadIndex < numOfStageThreads);
		uint64 curEpochTmp = currentEpoch[currentThreadIndex];
		if (curEpochTmp == NO_PREVIOUS_EPOCH) {
			return false;
		}

		if (snapshotCache[currentThreadIndex].find(key)
				!= snapshotCache[currentThreadIndex].end()) {
			epoch = curEpochTmp - 1;
			value = snapshotCache[currentThreadIndex][key];
			return true;
		}
		std::vector<std::pair<std::string, std::string> > result = searchAll(key,
				currentThreadIndex);

		/*
		 * found 0: has never been read
		 * found 1: it could be old snapshot version or the current snapshot
		 * found 2 or more: the old snapshot is either the last one or the last but one
		 */

		//found 0
		if (result.size() == 0) {
			throw Util::NotFoundException("key " + key + " not found");
		}

		uint64 epochTmp;
		std::string keyTmp;
		SeparateKey(result.back().first, epochTmp, keyTmp);
		assert(keyTmp == key);
		//found more than 1 and last one is the old snapshot
		if (epochTmp < currentEpoch[currentThreadIndex]) {
			epoch = currentEpoch[currentThreadIndex] - 1;
			uint64 versionTmp;
			SeparateVersion(result.back().second, versionTmp, value);
			snapshotCache[currentThreadIndex][key] = value;
			return true;
		}

		//found 1 and no old snapshot
		if (result.size() == 1) {
			return false;
		}

		//found 2: not the last one
		epoch = currentEpoch[currentThreadIndex] - 1;
		uint64 versionTmp;
		std::string valueTmp;
		SeparateVersion(result.front().second, versionTmp, value);
		snapshotCache[currentThreadIndex][key] = value;
		return true;
	}

	std::string Read(const std::string& key, int currentThreadIndex =
			TranxServiceQueueElement::INVALID_STAGE_THREADID) {
		if (currentThreadIndex == TranxServiceQueueElement::INVALID_STAGE_THREADID) {
			currentThreadIndex = GetThreadByKey(key);
		}
		assert(currentThreadIndex < numOfStageThreads);
		std::string CombinedKey = CombineKey(key, currentThreadIndex);
		uint64 hashedKey = Util::StringUtil::SDBMHash(key.c_str());

		if (cache[currentThreadIndex].find(key) != cache[currentThreadIndex].end()) {
			return cache[currentThreadIndex][key].second;
		}
		std::vector<std::pair<std::string, std::string> > result = searchAll(key,
				currentThreadIndex);
		if (result.size() != 0) {
			uint64 version;
			std::string value;
			SeparateVersion(result.back().second, version, value);

			cache[currentThreadIndex][key] = std::make_pair(version, value);
			return value;
		}
		throw Util::NotFoundException("key " + key + " not found in Read");
	}

	std::pair<uint64, std::string> ReadWithVersion(const std::string& key, int currentThreadIndex =
			TranxServiceQueueElement::INVALID_STAGE_THREADID) {
		if (currentThreadIndex == TranxServiceQueueElement::INVALID_STAGE_THREADID) {
			currentThreadIndex = GetThreadByKey(key);
		}
		assert(currentThreadIndex < numOfStageThreads);
		std::string CombinedKey = CombineKey(key, currentThreadIndex);
		uint64 hashedKey = Util::StringUtil::SDBMHash(key.c_str());

		if (cache[currentThreadIndex].find(key) != cache[currentThreadIndex].end()) {
			return cache[currentThreadIndex][key];
		}

		std::vector<std::pair<std::string, std::string> > result = searchAll(key,
				currentThreadIndex);
		if (result.size() != 0) {
			uint64 version;
			std::string value;
			SeparateVersion(result.back().second, version, value);

			cache[currentThreadIndex][key] = std::make_pair(version, value);
			return cache[currentThreadIndex][key];
		}
		throw Util::NotFoundException("key " + key + " not found in ReadWithVersion");
	}

	uint64 Write(const std::string& key, const std::string& value, int currentThreadIndex =
			TranxServiceQueueElement::INVALID_STAGE_THREADID) {
		if (currentThreadIndex == TranxServiceQueueElement::INVALID_STAGE_THREADID) {
			currentThreadIndex = GetThreadByKey(key);
		}
		assert(currentThreadIndex < numOfStageThreads);
		std::string CombinedKey = CombineKey(key, currentThreadIndex);
		uint64 nextVersion;

		if (cache[currentThreadIndex].find(key) != cache[currentThreadIndex].end()) {
			nextVersion = cache[currentThreadIndex][key].first + 1;
		} else {
			std::vector<std::pair<std::string, std::string> > result = searchAll(key,
					currentThreadIndex);

			if (result.size() != 0) {
				std::string prevValue;
				SeparateVersion(result.back().second, nextVersion, prevValue);
				nextVersion++;
			} else {
				nextVersion = 1;
			}
		}
		cache[currentThreadIndex][key] = std::make_pair(nextVersion, value);

		leveldb::WriteOptions writeOptions;
		writeOptions.sync = true;
		db[currentThreadIndex]->Put(writeOptions, CombinedKey, CombineVersion(nextVersion, value));

		return nextVersion;
	}

	void WriteBatch(const std::unordered_map<std::string, std::string>& updateMap,
			int currentThreadIndex) {
		assert(currentThreadIndex != TranxServiceQueueElement::INVALID_STAGE_THREADID);
		assert(currentThreadIndex < numOfStageThreads);

		leveldb::WriteBatch updates;
		for (auto it = updateMap.begin(); it != updateMap.end(); ++it) {
			assert(currentThreadIndex == GetThreadByKey(it->first));
			std::string CombinedKey = CombineKey(it->first, currentThreadIndex);
			uint64 nextVersion;
			if (cache[currentThreadIndex].find(it->first) != cache[currentThreadIndex].end()) {
				nextVersion = cache[currentThreadIndex][it->first].first + 1;
			} else {
				std::vector<std::pair<std::string, std::string> > result = searchAll(it->first,
						currentThreadIndex);
				if (result.size() != 0) {
					std::string prevValue;
					SeparateVersion(result.back().second, nextVersion, prevValue);
					nextVersion++;
				} else {
					nextVersion = 1;
				}
			}
			cache[currentThreadIndex][it->first] = std::make_pair(nextVersion, it->second);
			updates.Put(CombinedKey, CombineVersion(nextVersion, it->second));
		}

		leveldb::WriteOptions writeOptions;
		writeOptions.sync = true;
		db[currentThreadIndex]->Write(writeOptions, &updates);
	}

	/*
	 * WritePlain writes the key without appending an epoch
	 * used currently for repartitioning
	 */
	void WritePlain(const std::string& key, std::string value, int currentThreadIndex =
			TranxServiceQueueElement::INVALID_STAGE_THREADID) {
		if (currentThreadIndex == TranxServiceQueueElement::INVALID_STAGE_THREADID) {
			currentThreadIndex = GetThreadByKey(key);
		}
		assert(currentThreadIndex < numOfStageThreads);
		uint64 hashedKey = Util::StringUtil::SDBMHash(key.c_str());
		size_t pos = key.find_last_of(DELIMITER);
		std::string originalKey = key.substr(0, pos);
		assert(cache[currentThreadIndex].find(originalKey) == cache[currentThreadIndex].end());
		//TODO: update the cache here
		leveldb::WriteOptions writeOptions;
		writeOptions.sync = true;
		db[currentThreadIndex]->Put(writeOptions, key, value);
	}

	/*
	 * delete all key related data items for repartitioning purpose
	 */
	void Delete(const std::string& key, int currentThreadIndex =
			TranxServiceQueueElement::INVALID_STAGE_THREADID) {
		if (currentThreadIndex == TranxServiceQueueElement::INVALID_STAGE_THREADID) {
			currentThreadIndex = GetThreadByKey(key);
		}
		/*
		 * delete db before cache because reading checks cache first.
		 * if cache is deleted before db, cache might be retrieved from the db when it's not deleted yet
		 */
		assert(currentThreadIndex < numOfStageThreads);
		std::vector<std::pair<std::string, std::string> > result = searchAll(key,
				currentThreadIndex);
		leveldb::WriteOptions writeOptions;
		writeOptions.sync = true;
		for (auto it = result.begin(); it != result.end(); ++it) {
			//VERBOSE("storage deleting %s:%s", it->first.c_str(), it->second.c_str());
			assert(db[currentThreadIndex]->Delete(writeOptions, it->first).ok());
			//VERBOSE("db deleting %s", it->first.c_str());
		}
		cache[currentThreadIndex].erase(key);
	}

	void CreateSnapshot(int currentThreadIndex = TranxServiceQueueElement::INVALID_STAGE_THREADID) {
		std::vector<int> allThreads;
		if (currentThreadIndex == TranxServiceQueueElement::INVALID_STAGE_THREADID) {
			for (int i = 0; i < numOfStageThreads; ++i) {
				allThreads.push_back(i);
			}
		} else {
			allThreads.push_back(currentThreadIndex);
		}
		for (auto threadIndex = allThreads.begin(); threadIndex != allThreads.end();
				++threadIndex) {
			assert(*threadIndex < numOfStageThreads);
			snapshotCache[*threadIndex].clear();
			currentEpoch[*threadIndex]++;

			leveldb::WriteOptions writeOptions;
			writeOptions.sync = true;
			db[*threadIndex]->Put(writeOptions, EPOCHKEY,
					std::to_string(currentEpoch[*threadIndex]));
		}
	}

	/*
	 * coreID for dispatch thread
	 * coreIDs for storage threads
	 */
	virtual void StartStage(std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) {
		bool enableCoreBinding = false;
		uint32_t coreIDIndex = 0;
		assert(numOfStageThreads >= 1);
		uint32_t dispatchCore;
		std::vector<uint32_t> otherCoreIDs;
		if (coreIDs.size() > 0) {
			enableCoreBinding = true;
			dispatchCore = coreIDs[0];
			if (coreIDs.size() > 1) {
				for (int i = 1; i < coreIDs.size(); ++i) {
					otherCoreIDs.push_back(coreIDs[i]);
				}
			} else {
				otherCoreIDs.push_back(coreIDs[0]);
			}
		}
		for (int i = 0; i < numOfStageThreads; ++i) {
			processQueues.push_back(moodycamel::ConcurrentQueue<TranxServiceQueueElement *>(1000));
		}
		for (int i = 0; i < numOfStageThreads; ++i) {
			stageThread.push_back(boost::thread(&StringStorage::StageThread, this, i));
			if (enableCoreBinding) {
				Util::ThreadHelper::PinToCPUCores(stageThread[i], otherCoreIDs);
			}
		}
		if (numOfStageThreads > 1) {
			dispatchThread = boost::thread(&StringStorage::DispatchThread, this);
			if (enableCoreBinding) {
				Util::ThreadHelper::PinToCPUCore(dispatchThread, dispatchCore);
			}
		}
	}

	/*
	 * GetThreadByKey get the thread that stores the key in its database
	 * 	this is the method to split the shared data(by hash)
	 */
	int GetThreadByKey(const std::string& key) const {
		uint64 hashedKey = Util::StringUtil::SDBMHash(key.c_str());
		return hashedKey % numOfStageThreads;
	}

	void SendBackToPreviousStage(TranxServiceQueueElement *element, int stageThreadIndex);

private:
	/*
	 * MaybeLater: LRU mechanism when memory space becomes an issue
	 */
	std::vector<std::unordered_map<std::string, std::pair<uint64, std::string>>>cache;
	std::vector<leveldb::DB*> db;
	std::vector<leveldb::Options> options;
	/*
	 * snapshot data
	 * snapshotCache stores real key without epoch and value without version
	 */
	std::vector<uint64> currentEpoch;
	std::vector<std::unordered_map<std::string, std::string>> snapshotCache;

	virtual void StageThread(int stageThreadIndex);
	virtual void DispatchThread();
};

}
/* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_STORAGE_H_ */

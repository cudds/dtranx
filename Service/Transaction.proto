syntax = "proto2";
package DTranx.Service;

import "Service/Service.proto";

/**
 * InquiryTranxStatus ise used for GTranxInquire request
 */
enum GInquiryTranxStatus {
	WAITING = 1;
	COMMITTED = 2;
	ABORTED = 3;
};

/**
 * TranxKeys stores the readset and writeset of a transaction
 * for repartitioning purposes
 */

message GTranxKeys{
	repeated string readset = 1;
	repeated string writeset = 2;
};

/**
 *
 */
message GTranxID{
	required int32 nodeid = 1;
	required uint64 tranxid = 2;
}

/*
 * Tranx RPC's:  GTranxPrep, GTranxCommit, GTranxAbort, GTranxInquire, GTranxPartition, GTranxMigration
 */

message GTranxPrep{
	message Request {
		required GTranxID tranxid = 1;
		repeated GItem read_set = 2;
		repeated GItem write_set = 3;
		repeated string ips = 4;
		repeated GMapItem newMappings = 5;
		required GTranxType tranxtype = 6;
	};
	message Response {
		required GStatus status = 1;
	};
	optional Request request = 1;
	optional Response response = 2;
};

message GTranxCommit{
	message Request {
		required GTranxID tranxid = 1;
		required GTranxType tranxtype = 2;
	};
	message Response {
		required GStatus status = 1;
	};
	optional Request request = 1;
	optional Response response = 2;
};

message GTranxAbort{
	message Request {
		required GTranxID tranxid = 1;
	};
	message Response {
		required GStatus status = 1;
	};
	optional Request request = 1;
	optional Response response = 2;
};

message GTranxInquire {
	message Request {
		required GTranxID tranxid = 1;
	};
	message Response {
		required GInquiryTranxStatus tranxstatus = 1;
		/**
		 * TODO: GStatus might not be needed here
		 */
		required GStatus status = 2;
	};
	optional Request request = 1;
	optional Response response = 2;
};

/*
 * tranx history broadcast message
 * No Response is needed for GTranxPartition because it's a one directional RPC call
 * where the correctness of the DTranx doesn't depend on the reliable delivery
 */
message GTranxPartition {
	repeated GTranxKeys tranxs = 1;
};

message GTranxMigration{
	message Request {
		required GTranxID tranxid = 1;
	};
	
	message Response {
		required GStatus status = 1;
	};
	optional Request request = 1;
	optional Response response = 2;
};

message GGCRequest{
	/**
	 * No Response is needed for GGCRequest because it's a one directional RPC call
	 * where the correctness of the DTranx doesn't depend on the reliable delivery of GGCRequest
	 */
	required int32 nodeID = 1;
	required uint64 tranxID = 2;
};


/**
 * service level RPC message
 */
message GTranxServiceRPC{
	optional GOpCode opcode = 1;
	optional GTranxPrep tranxprep = 2;
	optional GTranxCommit tranxcommit = 3;
	optional GTranxAbort tranxabort = 4;
	optional GTranxInquire tranxinquire = 5;
	optional GTranxPartition tranxpartition = 6;
	optional GTranxMigration tranxmigration = 7;
	optional GGCRequest gc = 8;
};

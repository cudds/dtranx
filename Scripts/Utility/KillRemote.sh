#
# arg1: app name
# arg2: remote ip
# arg3: remote user
if [ "$#" -ne 3 ];then
	echo "KillRemote wrong number of input"
	exit
fi
appName=$1
ip=$2
RemoteUser=$3
pid=`ssh $RemoteUser@$ip ps -ef|grep $appName|grep -v grep| awk '{print $2}'`
ssh $RemoteUser@$ip kill -9 $pid < /dev/null
#!/bin/bash

function PrepareTestProgram(){
	SCRIPT_PATH="${BASH_SOURCE[0]}"
	CurDir=`dirname "${SCRIPT_PATH}"`
	DTranxDir=$CurDir/../../../
	sudo docker cp $DTranxDir/Build/Example/Tranx/SimpleTranx dtranx1:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/Tranx/SimpleTranx dtranx2:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/Tranx/SimpleTranx dtranx3:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/Tranx/CreateSnapshot dtranx1:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/Tranx/CreateSnapshot dtranx2:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/Tranx/CreateSnapshot dtranx3:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/Tranx/SnapshotRead dtranx1:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/Tranx/SnapshotRead dtranx2:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/Tranx/SnapshotRead dtranx3:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/Tranx/ShowLock dtranx1:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/Tranx/ShowLock dtranx2:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/Tranx/ShowLock dtranx3:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/LevelDBScan dtranx1:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/LevelDBScan dtranx2:/dtranx/
	sudo docker cp $DTranxDir/Build/Example/LevelDBScan dtranx3:/dtranx/
	sudo docker cp $DTranxDir/Test/IntegTest/Workloads dtranx1:/dtranx/
	sudo docker cp $DTranxDir/Test/IntegTest/Workloads dtranx2:/dtranx/
	sudo docker cp $DTranxDir/Test/IntegTest/Workloads dtranx3:/dtranx/
}

# arg1: docker id
# arg2: failmode: fail or hang
# arg3..: fail point: prepare, commit, etc 
function RestartDtranxClean(){
	if [ $# -lt 1 ]; then
    	echo "Illegal number of parameters"
	fi
	sudo docker exec $1 bash -c 'rm -rf /dtranx/dtranx.db*'
	sudo docker exec $1 bash -c 'rm -rf /dtranx/dtranx.mapdb'
	sudo docker exec $1 bash -c 'rm -rf /dtranx/dtranx.log*'
	sudo docker exec $1 bash -c 'rm -rf /dtranx/gcdtranx.log'

	sudo docker exec $1 bash -c 'rm -rf /dtranx/DTranx.out'
	
	tmp=`sudo docker exec $1 bash -c 'ps -ef|grep DTranx|grep -v grep'`
	pid=`echo $tmp|awk '{print $2}'` 
	sudo docker exec $1 kill -9 $pid
	tmp=`sudo docker exec $1 bash -c 'ps -ef|grep DTranx|grep -v grep'`
	pid=`echo $tmp|awk '{print $2}'` 
	sudo docker exec $1 kill -9 $pid
	sleep 2
	faults=""
	for arg in ${@:3}; do 
		faults+="-f $arg "
	done
	if [ $# -gt 1 ]; then
		if [ $2 == "fail" ]; then
			sudo docker exec $1 bash -c '/dtranx/pin/pin.sh -injection child -t /dtranx/pin/source/tools/ManualExamples/obj-intel64/appfunc.so -m 1'" $faults"'-- /dtranx/DTranx -i /dtranx/ips -t /dtranx/test.data &> /dtranx/DTranx.out &'
			return
		fi
	fi
	sudo docker exec $1 bash -c '/dtranx/pin/pin.sh -injection child -t /dtranx/pin/source/tools/ManualExamples/obj-intel64/appfunc.so'" $faults"'-- /dtranx/DTranx -i /dtranx/ips -t /dtranx/test.data &> /dtranx/DTranx.out &'
}

# arg1: docker id
# arg2: failmode: fail or hang
# arg3..: fail point: prepare, commit, etc 
function RestartDtranx(){
	if [ $# -lt 1 ]; then
    	echo "Illegal number of parameters"
	fi

	sudo docker exec $1 bash -c 'rm -rf /dtranx/DTranx.out'
	tmp=`sudo docker exec $1 bash -c 'ps -ef|grep DTranx|grep -v grep'`
	pid=`echo $tmp|awk '{print $2}'` 
	sudo docker exec $1 kill -9 $pid
	tmp=`sudo docker exec $1 bash -c 'ps -ef|grep DTranx|grep -v grep'`
	pid=`echo $tmp|awk '{print $2}'` 
	sudo docker exec $1 kill -9 $pid
	sleep 2
	sudo docker exec $1 bash -c 'rm -rf /dtranx/dtranx.db0/LOCK /dtranx/dtranx.db1/LOCK /dtranx/dtranx.db2/LOCK /dtranx/dtranx.db3/LOCK /dtranx/dtranx.mapdb/LOCK'
	faults=""
	for arg in ${@:3}; do 
		faults+="-f $arg "
	done
	if [ $# -gt 1 ]; then
		if [ $2 == "fail" ]; then
			sudo docker exec $1 bash -c '/dtranx/pin/pin.sh -injection child -t /dtranx/pin/source/tools/ManualExamples/obj-intel64/appfunc.so -m 1'" $faults"'-- /dtranx/DTranx -c /dtranx/DTranx.conf -i /dtranx/ips &> /dtranx/DTranx.out &'
			return
		fi
	fi
	sudo docker exec $1 bash -c '/dtranx/pin/pin.sh -injection child -t /dtranx/pin/source/tools/ManualExamples/obj-intel64/appfunc.so'" $faults"'-- /dtranx/DTranx -c /dtranx/DTranx.conf -i /dtranx/ips &> /dtranx/DTranx.out &'
}

# arg1: docker id
function RestartDtranxRepartition(){
	if [ $# -lt 1 ]; then
    	echo "Illegal number of parameters"
	fi
	sudo docker exec $1 bash -c 'rm -rf /dtranx/dtranx.db*'
	sudo docker exec $1 bash -c 'rm -rf /dtranx/dtranx.mapdb'
	sudo docker exec $1 bash -c 'rm -rf /dtranx/dtranx.log*'
	sudo docker exec $1 bash -c 'rm -rf /dtranx/gcdtranx.log'

	sudo docker exec $1 bash -c 'rm -rf /dtranx/DTranx.out'
	
	tmp=`sudo docker exec $1 bash -c 'ps -ef|grep DTranx|grep -v grep'`
	pid=`echo $tmp|awk '{print $2}'` 
	sudo docker exec $1 kill -9 $pid
	tmp=`sudo docker exec $1 bash -c 'ps -ef|grep DTranx|grep -v grep'`
	pid=`echo $tmp|awk '{print $2}'` 
	sudo docker exec $1 kill -9 $pid
	sleep 2
	sudo docker exec $1 bash -c '/dtranx/DTranx -i /dtranx/ips -t /dtranx/test.data -c /dtranx/DTranx.conf.repartition &> /dtranx/DTranx.out &'
}

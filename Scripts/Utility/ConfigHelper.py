import os
import shutil

#CONSTANTS
'''
	Project properties, do not change
'''
DTranx = "DTranx"
ConfSuffix = ".conf"
LogSuffix = ".out"
LibPath = "/usr/local/lib/"

'''
	These are specific to Cloudlab cluster
'''
RemoteUser = "nigo9731"
RemoteProjDir = "/users/nigo9731/Test/"
RemoteHomeDir = "/users/nigo9731/"
RemoteDTranx = RemoteProjDir + DTranx
RemoteIps = RemoteProjDir + "ips"
RemoteDTranxOutput = RemoteProjDir + DTranx + LogSuffix
RemoteDTranxConf = RemoteProjDir + DTranx + ConfSuffix;

'''
	AdjustConfig automatically changes the configuration file for each server
'''
def AdjustConfig(name, value, filename):
	fin = open(filename, 'r')
	fout = open(filename+".tmp",'w')
	for line in fin:
		splittedStrs = line.strip().split('=')
		if len(splittedStrs)>0 and splittedStrs[0].strip() == name:
			fout.write(name+" = "+value+ "\n")
		else:
			fout.write(line)
	fin.close()
	fout.close()
	os.remove(filename)
	shutil.move(filename+".tmp",filename)
	os.chmod(filename, 0666)
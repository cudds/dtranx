#!/bin/bash
# tests for normal transactions and also storage service forwarding
# mainly about service class tests
function test1(){
	# test single client with no faults
	SCRIPT_PATH="${BASH_SOURCE[0]}"
	CurDir=`dirname "${SCRIPT_PATH}"`
	DTranxDir=$CurDir/../../../

	sudo docker exec dtranx1 bash -c '/dtranx/SimpleTranx -p Workloads/readall &> /dtranx/test1_read'
	sudo docker cp dtranx1:/dtranx/test1_read $DTranxDir/
	sudo docker exec dtranx1 bash -c 'rm /dtranx/test1_read'
	
	sudo docker exec dtranx1 bash -c '/dtranx/SimpleTranx -p Workloads/writedistributed &> /dtranx/test1_write'
	sudo docker cp dtranx1:/dtranx/test1_write $DTranxDir/
	sudo docker exec dtranx1 bash -c 'rm /dtranx/test1_write'
}

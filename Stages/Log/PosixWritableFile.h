/*
 * Copyright (c) 2011 The LevelDB Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file. See the AUTHORS file for names of contributors.
 *
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_STAGES_LOG_POSIXWRITABLEFILE_H_
#define DTRANX_STAGES_LOG_POSIXWRITABLEFILE_H_

#include <iostream>
#include <cerrno>
#include <fcntl.h>
#include <unistd.h>
#include "WritableFile.h"
#include "Util/LogStatus.h"

namespace DTranx {
namespace Stages {
namespace Log {

class PosixWritableFile: public WritableFile {
public:
	PosixWritableFile(const std::string& fname, FILE* f)
			: filename_(fname), file_(f) {
	}

	~PosixWritableFile() {
		if (file_ != NULL) {
			// Ignoring any potential errors
			fclose(file_);
		}
	}

	virtual LogStatus Append(const Slice& data);

	virtual LogStatus Close();

	virtual LogStatus Flush();

	virtual LogStatus Sync();

private:
	std::string filename_;
	FILE* file_;

	LogStatus SyncDirIfManifest() {
		const char* f = filename_.c_str();
		const char* sep = strrchr(f, '/');
		Slice basename;
		std::string dir;
		if (sep == NULL) {
			dir = ".";
			basename = f;
		} else {
			dir = std::string(f, sep - f);
			basename = sep + 1;
		}
		LogStatus s;
		if (basename.starts_with("MANIFEST")) {
			int fd = open(dir.c_str(), O_RDONLY);
			if (fd < 0) {
				s = LogStatus::IOError(dir, strerror(errno));
			} else {
				if (fsync(fd) < 0) {
					s = LogStatus::IOError(dir, strerror(errno));
				}
				close(fd);
			}
		}
		return s;
	}

};

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_LOG_POSIXWRITABLEFILE_H_ */

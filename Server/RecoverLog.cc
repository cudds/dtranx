/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <cstdlib>
#include <unordered_map>
#include <cassert>
#include <map>
#include "RecoverLog.h"
#include "DTranx/Util/Log.h"
#include "Util/FileUtil.h"

#include "Service/TranxService.h"
#include "DaemonStages/TranxAck.h"
#include "DaemonStages/Repartition.h"
#include "Stages/TranxStates.h"
#include "Stages/TranxRPCHelper.h"
#include "Stages/Log/TranxLog.h"
#include "Stages/Lock/WriteBlockLockTable.h"
#include "Stages/Storage.h"

namespace DTranx {
namespace Server {

RecoverLog::RecoverLog(SharedResources::TranxServerInfo *tranxServerInfo,
		SharedResources::TranxServiceSharedData *tranxServiceSharedData)
		: DaemonStages::DaemonStages(tranxServerInfo, tranxServiceSharedData),
				assignParticipantsStrategy(tranxServerInfo->GetSelfAddress(),
						tranxServiceSharedData) {

}

RecoverLog::~RecoverLog() {
}

void RecoverLog::Recover() {
	tranxServiceSharedData->SetNextTranxID(
			std::strtoull(Util::StaticConfig::GetInstance()->Read("FirstID").c_str(), NULL, 10));
	//first recover gc log
	Stages::Log::LogOption logOption =
			(tranxServerInfo->GetConfig().read("LogOption") == "Pmem") ?
					Stages::Log::PMEM : Stages::Log::DISK;

	std::string logFullName = Util::FileUtil::CombineName(
			tranxServerInfo->GetConfig().read("PersistLogDir"),
			tranxServerInfo->GetConfig().read("GCLogFileName"));
	Stages::Log::TranxLog gcLog(logFullName, tranxServerInfo);
	std::unordered_map<int, uint64> tranxIDs = gcLog.GetGCEntry();
	int nodeID = tranxServerInfo->GetNodeID();
	for (auto it = tranxIDs.begin(); it != tranxIDs.end(); ++it) {
		if (it->first == nodeID) {
			tranxStates->UpdateGCEarliest(it->second);
			VERBOSE("after recovery, the gc earliest for self is %lu", it->second);
		} else {
			tranxStates->UpdateGCEarliest(it->first, it->second);
			VERBOSE("after recovery, the gc earliest for %d is %lu", it->first, it->second);
		}
	}
	tranxServiceSharedData->SetNextTranxID(tranxStates->GetGCEarliest() + 1);

	//first redo complete transactions.
	VERBOSE(
			" PREP = 1; READY = 2; COMMIT = 3; COMMIT_1PC = 4; COORD_ABORT = 5; PART_ABORT = 6; ACK = 7");
	while (true) {
		try {
			Stages::Log::GTranxLogRecord entry = tranxLog->ReadNext();
			ProcessLog(entry);
		} catch (Util::NotFoundException &e) {
			break;
		}
	}

	//fill in the missing tranxs when it's lost when no logs are there and it's not in gc history, fill it with abort
	for (int i = tranxStates->GetGCEarliest() + 1; i < tranxServiceSharedData->GetNextTranxID();
			++i) {
		Util::TranxID tranxID(nodeID, i);
		Stages::State state = tranxStates->CheckState(tranxID);
		if (state == Stages::State::READY || state == Stages::State::ABORT
				|| state == Stages::State::COMMIT || state == Stages::State::GC
				|| state == Stages::State::MIGRATION) {
			continue;
		}
		if (tranxInDoubt.find(tranxID) != tranxInDoubt.end()) {
			continue;
		}
		tranxLog->CoordinatorAck(tranxID);
		tranxStates->SetToAck(tranxID, false);
		VERBOSE("tranxID %d:%lu is filled during recovery", tranxID.GetNodeID(),
				tranxID.GetTranxID());
	}
}

void RecoverLog::HoldLockFromLog(Stages::Log::GTranxLogRecord &entry) {
	bool isSnapshot = entry.has_tranxtype() && entry.tranxtype() == Service::GTranxType::SNAPSHOT;
	bool isRepartition = entry.has_tranxtype()
			&& entry.tranxtype() == Service::GTranxType::REPARTITION;
	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	Util::TranxID tranxID;
	tranxID.SetNodeID(entry.tranxid().nodeid());
	tranxID.SetTranxID(entry.tranxid().tranxid());
	VERBOSE("holding locks for tranx: %d#%lu", tranxID.GetNodeID(), tranxID.GetTranxID());
	if (isSnapshot) {
		VERBOSE("holding epoch locks for tranx: %d#%lu", tranxID.GetNodeID(), tranxID.GetTranxID());
		assert(lockTable->RequestEpochLock(tranxID));
	} else if (isRepartition) {
		std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
		for (auto hashkey = entry.newmapping().begin(); hashkey != entry.newmapping().end();
				++hashkey) {
			std::string key = hashkey->key();
			keysMode[key] = Stages::Lock::BasicLockTableLockMode::Exclusive;
		}
		assert(lockTable->RequestLocksBlocking(keysMode, tranxID));
	} else {
		std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
		if (entry.logtype() == Stages::Log::GLogType::PREP) {
			std::unordered_map<std::string,
					std::pair<std::vector<Service::GItem>, std::vector<Service::GItem>> > items =
					assignParticipantsStrategy.MapItem(entry.read_set(), entry.write_set());
			std::vector<Service::GItem> localReadSet = items[selfAddress].first;
			std::vector<Service::GItem> localWriteSet = items[selfAddress].second;
			for (auto it = localReadSet.begin(); it != localReadSet.end(); ++it) {
				keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Shared;
			}
			for (auto it = localWriteSet.begin(); it != localWriteSet.end(); ++it) {
				keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Exclusive;
			}
			assert(lockTable->RequestLocks(keysMode, tranxID));
		} else if (entry.logtype() == Stages::Log::GLogType::READY) {
			for (auto it = entry.read_set().begin(); it != entry.read_set().end(); ++it) {
				keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Shared;
			}
			for (auto it = entry.write_set().begin(); it != entry.write_set().end(); ++it) {
				keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Exclusive;
			}
			assert(lockTable->RequestLocks(keysMode, tranxID));
		} else {
			assert(false);
		}
	}
}

void RecoverLog::ReleaseLockFromLog(Stages::Log::GTranxLogRecord &entry) {
	bool isSnapshot = entry.has_tranxtype() && entry.tranxtype() == Service::GTranxType::SNAPSHOT;
	bool isRepartition = entry.has_tranxtype()
			&& entry.tranxtype() == Service::GTranxType::REPARTITION;
	std::string selfAddress = tranxServerInfo->GetSelfAddress();
	Util::TranxID tranxID;
	tranxID.SetNodeID(entry.tranxid().nodeid());
	tranxID.SetTranxID(entry.tranxid().tranxid());
	if (isSnapshot) {
		assert(lockTable->ReleaseEpochLock(tranxID));
	} else {
		if (entry.logtype() == Stages::Log::GLogType::COORD_ABORT) {
			if (tranxInDoubt.find(tranxID) != tranxInDoubt.end()) {
				VERBOSE("releasing locks for tranx: %d#%lu", tranxID.GetNodeID(),
						tranxID.GetTranxID());
				assert(tranxInDoubt[tranxID].size() == 1);
				assert(tranxInDoubt[tranxID][0].logtype() == Stages::Log::GLogType::PREP);
				Stages::Log::GTranxLogRecord &prepEntry = tranxInDoubt[tranxID][0];
				std::unordered_map<std::string,
						std::pair<std::vector<Service::GItem>, std::vector<Service::GItem>> > items =
						assignParticipantsStrategy.MapItem(prepEntry.read_set(),
								prepEntry.write_set());
				std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
				std::vector<Service::GItem> localReadSet = items[selfAddress].first;
				std::vector<Service::GItem> localWriteSet = items[selfAddress].second;
				for (auto it = localReadSet.begin(); it != localReadSet.end(); ++it) {
					keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Shared;
				}
				for (auto it = localWriteSet.begin(); it != localWriteSet.end(); ++it) {
					keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Exclusive;
				}
				assert(lockTable->ReleaseLocks(keysMode, tranxID));
			}
		} else if (entry.logtype() == Stages::Log::GLogType::COMMIT) {
			//commit is only called when it's in tranxInDoubt
			assert(tranxInDoubt[tranxID].size() == 1);
			if (tranxInDoubt[tranxID][0].logtype() == Stages::Log::GLogType::PREP) {
				VERBOSE("releasing locks for tranx: %d#%lu", tranxID.GetNodeID(),
						tranxID.GetTranxID());
				//if it is the coord
				Stages::Log::GTranxLogRecord &prepEntry = tranxInDoubt[tranxID][0];
				if (isRepartition) {
					std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
					for (auto hashkey = prepEntry.newmapping().begin();
							hashkey != prepEntry.newmapping().end(); ++hashkey) {
						std::string key = hashkey->key();
						keysMode[key] = Stages::Lock::BasicLockTableLockMode::Exclusive;
					}
					lockTable->ReleaseLocks(keysMode, tranxID);
				} else {
					std::unordered_map<std::string,
							std::pair<std::vector<Service::GItem>, std::vector<Service::GItem>> > items =
							assignParticipantsStrategy.MapItem(prepEntry.read_set(),
									prepEntry.write_set());
					std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
					std::vector<Service::GItem> localReadSet = items[selfAddress].first;
					std::vector<Service::GItem> localWriteSet = items[selfAddress].second;
					for (auto it = localReadSet.begin(); it != localReadSet.end(); ++it) {
						keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Shared;
					}
					for (auto it = localWriteSet.begin(); it != localWriteSet.end(); ++it) {
						keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Exclusive;
					}
					assert(lockTable->ReleaseLocks(keysMode, tranxID));
				}
			} else {
				VERBOSE("releasing locks for tranx: %d#%lu", tranxID.GetNodeID(),
						tranxID.GetTranxID());
				std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
				Stages::Log::GTranxLogRecord &readyEntry = tranxInDoubt[tranxID][0];
				if (isRepartition) {
					for (auto hashkey = readyEntry.newmapping().begin();
							hashkey != readyEntry.newmapping().end(); ++hashkey) {
						std::string key = hashkey->key();
						keysMode[key] = Stages::Lock::BasicLockTableLockMode::Exclusive;
					}
					lockTable->ReleaseLocks(keysMode, tranxID);
				} else {
					assert(readyEntry.logtype() == Stages::Log::GLogType::READY);
					for (auto it = readyEntry.read_set().begin(); it != readyEntry.read_set().end();
							++it) {
						keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Shared;
					}
					for (auto it = readyEntry.write_set().begin();
							it != readyEntry.write_set().end(); ++it) {
						keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Exclusive;
					}
					//lockTable->PrintLockStatus();
					assert(lockTable->ReleaseLocks(keysMode, tranxID));
				}
			}
		} else if (entry.logtype() == Stages::Log::GLogType::PART_ABORT) {
			if (tranxInDoubt.find(tranxID) != tranxInDoubt.end()) {
				VERBOSE("releasing locks for tranx: %d#%lu", tranxID.GetNodeID(),
						tranxID.GetTranxID());
				assert(tranxInDoubt[tranxID].size() == 1);
				assert(tranxInDoubt[tranxID][0].logtype() == Stages::Log::GLogType::READY);
				Stages::Log::GTranxLogRecord &readyEntry = tranxInDoubt[tranxID][0];
				std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
				for (auto it = readyEntry.read_set().begin(); it != readyEntry.read_set().end();
						++it) {
					keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Shared;
				}
				for (auto it = readyEntry.write_set().begin(); it != readyEntry.write_set().end();
						++it) {
					keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Exclusive;
				}
				assert(lockTable->ReleaseLocks(keysMode, tranxID));
			}
		} else if (entry.logtype() == Stages::Log::GLogType::PREP) {
			VERBOSE("releasing locks for tranx: %d#%lu", tranxID.GetNodeID(), tranxID.GetTranxID());
			if (isRepartition) {
				std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
				for (auto hashkey = entry.newmapping().begin(); hashkey != entry.newmapping().end();
						++hashkey) {
					std::string key = hashkey->key();
					keysMode[key] = Stages::Lock::BasicLockTableLockMode::Exclusive;
				}
				lockTable->ReleaseLocks(keysMode, tranxID);
			} else {
				std::unordered_map<std::string,
						std::pair<std::vector<Service::GItem>, std::vector<Service::GItem>> > items =
						assignParticipantsStrategy.MapItem(entry.read_set(), entry.write_set());
				std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
				std::vector<Service::GItem> localReadSet = items[selfAddress].first;
				std::vector<Service::GItem> localWriteSet = items[selfAddress].second;
				for (auto it = localReadSet.begin(); it != localReadSet.end(); ++it) {
					keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Shared;
				}
				for (auto it = localWriteSet.begin(); it != localWriteSet.end(); ++it) {
					keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Exclusive;
				}
				assert(lockTable->ReleaseLocks(keysMode, tranxID));
			}
		} else if (entry.logtype() == Stages::Log::GLogType::READY) {
			VERBOSE("releasing locks for tranx: %d#%lu", tranxID.GetNodeID(), tranxID.GetTranxID());
			if (isRepartition) {
				std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
				for (auto hashkey = entry.newmapping().begin(); hashkey != entry.newmapping().end();
						++hashkey) {
					std::string key = hashkey->key();
					keysMode[key] = Stages::Lock::BasicLockTableLockMode::Exclusive;
				}
				lockTable->ReleaseLocks(keysMode, tranxID);
			} else {
				std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
				for (auto it = entry.read_set().begin(); it != entry.read_set().end(); ++it) {
					keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Shared;
				}
				for (auto it = entry.write_set().begin(); it != entry.write_set().end(); ++it) {
					keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Exclusive;
				}
				//it might've already been released from tranxcommit request
				lockTable->ReleaseLocks(keysMode, tranxID);
			}
		} else {
			assert(false);
		}
	}
}

void RecoverLog::ProcessLog(Stages::Log::GTranxLogRecord entry) {
	VERBOSE("log entry tranxid %d#%lu, type %d", entry.tranxid().nodeid(),
			entry.tranxid().tranxid(), entry.logtype());
	Util::TranxID tranxID;
	tranxID.SetNodeID(entry.tranxid().nodeid());
	tranxID.SetTranxID(entry.tranxid().tranxid());
	int nodeID = tranxServerInfo->GetNodeID();
	Stages::State state = tranxStates->CheckState(tranxID);
	std::unordered_map<Util::TranxID, SharedResources::TranxServiceSharedData::TranxItems*,
			Util::KeyHasher>& curTranxs = tranxServiceSharedData->GetCurTranxReference();

	if (state == Stages::State::GC) {
		return;
	}
	SharedResources::TranxServiceSharedData::TranxItems* allItems;
	std::unordered_map<std::string, std::unordered_set<std::string> > newMappings;
	switch (entry.logtype()) {
		case Stages::Log::GLogType::PREP:
			VERBOSE("process log prep");
			assert(tranxInDoubt.find(tranxID) == tranxInDoubt.end());
			tranxInDoubt[tranxID] = std::vector<Stages::Log::GTranxLogRecord>();
			tranxInDoubt[tranxID].push_back(entry);
			{
				assert(tranxID.GetNodeID() == nodeID);
				if (tranxID.GetTranxID() >= tranxServiceSharedData->GetNextTranxID()) {
					tranxServiceSharedData->SetNextTranxID(tranxID.GetTranxID() + 1);
				}
			}
			if (entry.has_tranxtype() && entry.tranxtype() == Service::GTranxType::SNAPSHOT) {
				tranxServiceSharedData->SetSnapshotTranx(tranxID);
				toAckTranxType[tranxID] = Service::GTranxType::SNAPSHOT;
			} else if (entry.has_tranxtype()
					&& entry.tranxtype() == Service::GTranxType::REPARTITION) {
				repartition->SetRepartitionTranxIDRecover(tranxID);
			} else {
				toAckTranxType[tranxID] = Service::GTranxType::NORMAL;
			}
			HoldLockFromLog(entry);
			break;
		case Stages::Log::GLogType::COORD_ABORT:
			ReleaseLockFromLog(entry);
			if (tranxInDoubt.find(tranxID) != tranxInDoubt.end()) {
				std::vector<std::string> ips;
				assert(tranxInDoubt[tranxID].size() == 1);
				assert(tranxInDoubt[tranxID][0].logtype() == Stages::Log::GLogType::PREP);
				for (auto ip = tranxInDoubt[tranxID][0].ips().begin();
						ip != tranxInDoubt[tranxID][0].ips().end(); ++ip) {
					ips.push_back(*ip);
				}
				tranxStates->SetToAbortState(tranxID);
				toAckAbort[tranxID] = ips;
				tranxInDoubt.erase(tranxID);
			} else {
				//it might happen when the prepare log is reclaimed
				tranxStates->SetToAck(tranxID, false);
			}
			break;
		case Stages::Log::GLogType::COMMIT:
			VERBOSE("process log commit");
			if (tranxInDoubt.find(tranxID) == tranxInDoubt.end()) {
				/* it might happen when the prepare/ready log is garbage collected but the commit is not
				 * and it must've been ack'ed already
				 */
				break;
			} else {
				ReleaseLockFromLog(entry);
				ApplyLogs(tranxInDoubt[tranxID]);
				if (tranxInDoubt[tranxID][0].logtype() != Stages::Log::GLogType::PREP) {
					//if it is the participant, do nothing
					curTranxs.erase(tranxID);
					tranxService->ClearNewMappings();
					tranxStates->CheckAndSetCommitState(tranxID);
				} else {
					//if it is the coordinator, put it in toAck list
					std::vector<std::string> ips;
					for (auto ip = tranxInDoubt[tranxID][0].ips().begin();
							ip != tranxInDoubt[tranxID][0].ips().end(); ++ip) {
						ips.push_back(*ip);
					}
					toAckCommit[tranxID] = ips;
					tranxStates->SetToCommitState(tranxID);
				}
				tranxInDoubt.erase(tranxID);
			}
			break;
		case Stages::Log::GLogType::COMMIT_1PC:
			assert(tranxInDoubt.find(tranxID) == tranxInDoubt.end());
			ApplyLog(entry);
			tranxStates->SetToAck(tranxID, true);
			break;
		case Stages::Log::GLogType::PART_ABORT:
			ReleaseLockFromLog(entry);
			curTranxs.erase(tranxID);
			tranxInDoubt.erase(tranxID);
			tranxStates->CheckAndSetAbortState(tranxID);
			break;
		case Stages::Log::GLogType::READY:
			assert(tranxInDoubt.find(tranxID) == tranxInDoubt.end());
			{
				Stages::State state = tranxStates->CheckState(tranxID);
				if (state == Stages::State::ABORT) {
					/*
					 * abort log might come before ready log
					 */
					break;
				}
			}
			HoldLockFromLog(entry);
			tranxStates->CheckAndSetReadyState(tranxID);

			if (entry.has_tranxtype() && entry.tranxtype() == Service::GTranxType::REPARTITION) {
				for (auto it = entry.newmapping().begin(); it != entry.newmapping().end(); ++it) {
					for (auto ip = it->ips().begin(); ip != it->ips().end(); ++ip) {
						newMappings[it->key()].insert(*ip);
					}
				}
				tranxService->SetNewMappings(newMappings);
				tranxStates->CheckAndSetMigrateState(tranxID);
			} else {
				//add to the curTranxs
				allItems = tranxServiceSharedData->NewCurTranx();
				for (auto it_b = entry.read_set().begin(); it_b != entry.read_set().end(); ++it_b) {
					allItems->readItems.push_back(*it_b);
				}
				for (auto it_b = entry.write_set().begin(); it_b != entry.write_set().end();
						++it_b) {
					allItems->writeItems.push_back(*it_b);
				}
				curTranxs[tranxID] = allItems;
			}

			tranxInDoubt[tranxID] = std::vector<Stages::Log::GTranxLogRecord>();
			tranxInDoubt[tranxID].push_back(entry);
			break;
		case Stages::Log::GLogType::ACK:
			assert(tranxInDoubt.find(tranxID) == tranxInDoubt.end());
			if (toAckCommit.find(tranxID) != toAckCommit.end()) {
				toAckCommit.erase(tranxID);
			}
			if (toAckAbort.find(tranxID) != toAckAbort.end()) {
				toAckAbort.erase(tranxID);
			}
			{
				Stages::State state = tranxStates->CheckState(tranxID);
				/*
				 * it could be the case when the locks are rejected in the coordinator
				 * or when the last time it recovers and this tranxID is not in abort, commit or gc
				 */
				if (state != Stages::State::GC) {
					tranxStates->SetToAck(tranxID, state == Stages::State::COMMIT);
				}
			}
			break;
	}
}

void RecoverLog::ApplyLog(Stages::Log::GTranxLogRecord &logEntry) {
	std::unordered_map<std::string, std::unordered_set<std::string> > newMappings;
	SharedResources::MappingDS *mappingStorage = tranxServiceSharedData->getMappingStorage();
	assert(
			logEntry.logtype() == Stages::Log::GLogType::READY
					|| logEntry.logtype() == Stages::Log::GLogType::PREP
					|| logEntry.logtype() == Stages::Log::GLogType::COMMIT_1PC);
	if (logEntry.has_tranxtype() && logEntry.tranxtype() == Service::GTranxType::REPARTITION) {
		if (logEntry.logtype() == Stages::Log::GLogType::PREP) {
			for (auto it = logEntry.newmapping().begin(); it != logEntry.newmapping().end(); ++it) {
				for (auto ip = it->ips().begin(); ip != it->ips().end(); ++ip) {
					newMappings[it->key()].insert(*ip);
				}
			}
		}
		assert(!newMappings.empty());
		for (auto it = newMappings.begin(); it != newMappings.end(); ++it) {
			mappingStorage->UpdateLookupTable(it->first, it->second);
		}
	} else {
		for (auto it = logEntry.write_set().begin(); it != logEntry.write_set().end(); ++it) {
			std::unordered_set<std::string> nodeIPs;
			VERBOSE("%s", it->key().c_str());
			mappingStorage->GetMapping(it->key(), nodeIPs);
			assert(nodeIPs.size() != 0);
			if (nodeIPs.find(tranxServerInfo->GetSelfAddress()) == nodeIPs.end()) {
				continue;
			}
			localStorage->Write(it->key(), it->value());
		}
	}
}

void RecoverLog::ApplyLogs(std::vector<Stages::Log::GTranxLogRecord> &logVec) {
	assert(logVec.size() > 0);
	ApplyLog(logVec[0]);
}

void RecoverLog::DaemonThread() {
	NOTICE("PostRecovery started");
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	for (auto it = tranxInDoubt.begin(); it != tranxInDoubt.end(); ++it) {
		toRecover.insert(it->first);
	}
	for (auto it = toAckCommit.begin(); it != toAckCommit.end(); ++it) {
		toRecover.insert(it->first);
	}
	for (auto it = toAckAbort.begin(); it != toAckAbort.end(); ++it) {
		toRecover.insert(it->first);
	}
	while (true) {
		if (terminateThread.load()) {
			NOTICE("PostRecovery is reclaimed");
			break;
		}
		Stages::TranxServiceQueueElement *tmpRPCs[Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = daemonProcessQueue.try_dequeue_bulk(tmpRPCs,
				Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (rand() % queuePerfFreq == 1) {
				AddQueueLengthSample(0, daemonProcessQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			Stages::TranxServiceQueueElement *element = tmpRPCs[i];
			//element->goThroughStage("PostRecovery");
			PostRecoveryProcess(element);
		}
		if (toRecover.empty()) {
			terminateThread.store(true);
			continue;
		}
		if (!tranxInDoubt.empty()) {
			ProcessTranxInDoubt();
		}
		if (!toAckCommit.empty() || !toAckAbort.empty()) {
			ProcessToAck();
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void RecoverLog::PostRecoveryProcess(Stages::TranxServiceQueueElement *element) {
	int stageNum = element->getStageNum();
	if (stageNum == 1) {
		/*
		 * prepare returned
		 */
		Stages::TranxRPCHelperPollingResult peerResult = element->getTranxRpcResult();
		assert(peerResult == Stages::TranxRPCHelperPollingResult::AllOK);

		element->setStageNum(2);
		element->setLogTask(Stages::Log::TranxLogStageTask::_Commit);
		element->setLogType(Stages::Log::GLogType::COMMIT);
		tranxLog->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
	} else if (stageNum == 2) {
		/*
		 * Commit log written
		 */
		element->setStageNum(3);
		element->setStateTask(Stages::TranxStatesStageTask::_SetToCommitState);
		tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
				ptok_tranxStates);
	} else if (stageNum == 3) {
		/*
		 * create snapshot
		 */
		element->setStageNum(4);
		element->setStorageTask(Stages::StorageStageTask::_CreateSnapshot);
		localStorage->SendToStage(element,
				Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
	} else if (stageNum == 4) {
		element->setStageNum(5);
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASEEPOCHLOCK);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
	} else if (stageNum == 5) {
		/*
		 * tranxInDoubt get responses and need to ack
		 * this path follows sendack, write ack log and then change tranxstates
		 */
		element->setAckCommitAbort(true);
		element->setStageNum(1);
		element->setStageId(Stages::TranxServiceQueueElement::StageID::TRANXACK);
		tranxAck->SendToDaemon(element);
		assert(toRecover.find(element->GetTranxID()) != toRecover.end());
		toRecover.erase(element->GetTranxID());
	} else if (stageNum == 6) {
		/*
		 * normal transaction inquiry return
		 */
		Service::GInquiryTranxStatus status = element->getTranxRpcInquiryResult();
		if (status == Service::GInquiryTranxStatus::COMMITTED) {
			element->setStageNum(7);
			element->setLogTask(Stages::Log::TranxLogStageTask::_Commit);
			element->setLogType(Stages::Log::GLogType::COMMIT);
			tranxLog->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		} else if (status == Service::GInquiryTranxStatus::ABORTED) {
			element->setStageNum(10);
			element->setLogTask(Stages::Log::TranxLogStageTask::_CoordinatorAbort);
			tranxLog->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		} else {
			VERBOSE("tranxID %d#%lu: aborted during recovery", element->GetTranxID().GetNodeID(),
					element->GetTranxID().GetTranxID());
			element->setStageNum(10);
			element->setLogTask(Stages::Log::TranxLogStageTask::_CoordinatorAbort);
			tranxLog->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		}

	} else if (stageNum == 7) {
		element->setStageNum(8);
		element->setStateTask(Stages::TranxStatesStageTask::_SetToCommitState);
		tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
				ptok_tranxStates);
	} else if (stageNum == 8) {
		/*
		 * write to local storage here
		 */
		std::unordered_map<std::string, std::string> writeSet;
		std::unordered_map<std::string,
				std::pair<std::vector<Service::GItem>, std::vector<Service::GItem>> >& items =
				element->getItems();
		std::vector<Service::GItem> localWriteSet = items[tranxServerInfo->GetSelfAddress()].second;
		for (auto it = localWriteSet.begin(); it != localWriteSet.end(); ++it) {
			writeSet[it->key()] = it->value();
		}
		element->setStageNum(9);
		element->setWriteSet(writeSet);
		element->setStorageTask(Stages::StorageStageTask::_Write);
		localStorage->SendToStage(element,
				Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);

	} else if (stageNum == 9) {
		/*
		 * local storage are committed, next release log and send to ack
		 */
		element->setStageNum(5);
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSMAP);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
	} else if (stageNum == 10) {
		element->setStageNum(11);
		element->setStateTask(Stages::TranxStatesStageTask::_SetToAbortState);
		tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
				ptok_tranxStates);

	} else if (stageNum == 11) {
		/*
		 * local storage are aborted, next release log and send to ack
		 */
		element->setStageNum(12);
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSMAP);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
	} else if (stageNum == 12) {
		/*
		 * abort tranxs
		 * this path follows sendack, write ack log and then change tranxstates
		 */
		element->setAckCommitAbort(false);
		element->setStageNum(1);
		element->setStageId(Stages::TranxServiceQueueElement::StageID::TRANXACK);
		tranxAck->SendToDaemon(element);
		assert(toRecover.find(element->GetTranxID()) != toRecover.end());
		toRecover.erase(element->GetTranxID());

	} else {
		assert(false);
	}
}

void RecoverLog::ProcessTranxInDoubt() {
	/*
	 * Choose random one and then process it, delete it
	 */
	std::vector<std::string> allNodes = tranxServerInfo->GetAllNodes();
	assert(!tranxInDoubt.empty());
	auto chosenTranx = tranxInDoubt.begin();
	assert(chosenTranx->second.size() != 0);
	if (chosenTranx->second[0].logtype() == Stages::Log::GLogType::PREP) {
		/*
		 * follow client service processing
		 */
		std::vector<std::string> ips;
		Util::TranxID tranxIDStr = chosenTranx->first;
		bool isSnapshot = chosenTranx->second[0].has_tranxtype()
				&& chosenTranx->second[0].tranxtype() == Service::GTranxType::SNAPSHOT;
		bool isRepartition = chosenTranx->second[0].has_tranxtype()
				&& chosenTranx->second[0].tranxtype() == Service::GTranxType::REPARTITION;
		if (isSnapshot) {
			ips = allNodes;
		} else {
			for (auto ip = chosenTranx->second[0].ips().begin();
					ip != chosenTranx->second[0].ips().end(); ++ip) {
				ips.push_back(*ip);
			}
		}
		if (isSnapshot) {
			Stages::TranxServiceQueueElement *element = new Stages::TranxServiceQueueElement();
			//element->goThroughStage("PostRecovery");
			element->setStageId(Stages::TranxServiceQueueElement::StageID::RECOVERY);
			element->setStageNum(1);
			element->SetTranxID(tranxIDStr);
			element->setTranxType(Service::GTranxType::SNAPSHOT);
			element->setTranxRpcTask(Stages::TranxRPCHelperStageTask::_SENDPREPANDPOLL);
			element->setIps(ips);
			tranxRPCHelper->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		} else if (isRepartition) {
			std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
			std::unordered_map<std::string, std::unordered_set<std::string> > newMappings;
			for (auto mapItem = chosenTranx->second[0].newmapping().begin();
					mapItem != chosenTranx->second[0].newmapping().end(); ++mapItem) {
				for (auto ip = mapItem->ips().begin(); ip != mapItem->ips().end(); ++ip) {
					newMappings[mapItem->key()].insert(*ip);
					keysMode[mapItem->key()] = Stages::Lock::BasicLockTableLockMode::Exclusive;
				}
			}
			Stages::TranxServiceQueueElement *element = new Stages::TranxServiceQueueElement();
			//element->goThroughStage("PostRecovery");
			element->setTranxType(Service::GTranxType::REPARTITION);
			element->setNewMapping(newMappings);
			element->setIps(ips);
			element->setStageId(Stages::TranxServiceQueueElement::StageID::REPARTITION);

			element->setKeysMode(keysMode);
			element->setStageNum(3);
			element->SetTranxID(tranxIDStr);
			toRecover.erase(tranxIDStr);
		} else {
			/*
			 * normal transactions
			 */
			Stages::TranxServiceQueueElement *element = new Stages::TranxServiceQueueElement();
			//element->goThroughStage("PostRecovery");
			element->setStageId(Stages::TranxServiceQueueElement::StageID::RECOVERY);
			element->setStageNum(6);
			element->SetTranxID(tranxIDStr);
			element->setTranxType(Service::GTranxType::NORMAL);
			element->setTranxRpcTask(Stages::TranxRPCHelperStageTask::_SENDINQUIRYANDPOLL);
			element->setIps(ips);

			std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
			std::unordered_map<std::string,
					std::pair<std::vector<Service::GItem>, std::vector<Service::GItem>> > items =
					assignParticipantsStrategy.MapItem(chosenTranx->second[0].read_set(),
							chosenTranx->second[0].write_set());
			std::string selfAddress = tranxServerInfo->GetSelfAddress();
			std::vector<Service::GItem> localReadSet = items[selfAddress].first;
			std::vector<Service::GItem> localWriteSet = items[selfAddress].second;
			for (auto it = localReadSet.begin(); it != localReadSet.end(); ++it) {
				keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Shared;
			}
			for (auto it = localWriteSet.begin(); it != localWriteSet.end(); ++it) {
				keysMode[it->key()] = Stages::Lock::BasicLockTableLockMode::Exclusive;
			}
			element->setItems(items);
			element->setKeysMode(keysMode);
			tranxRPCHelper->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		}
	} else if (chosenTranx->second[0].logtype() == Stages::Log::GLogType::READY) {
		/*
		 * follow tranx service processing
		 *
		 * There's no need to initiate inquiry request because the coordinator
		 * will send commit/abort request to this node. If coordinator fails at that point,
		 * it will block
		 */
		toRecover.erase(chosenTranx->first);
	} else {
		assert(false);
	}
	tranxInDoubt.erase(chosenTranx->first);
}

void RecoverLog::ProcessToAck() {
	for (auto tranx = toAckCommit.begin(); tranx != toAckCommit.end(); ++tranx) {
		Stages::TranxServiceQueueElement *element = new Stages::TranxServiceQueueElement();
		//element->goThroughStage("PostRecovery");
		Util::TranxID tranxID = tranx->first;
		element->SetTranxID(tranxID);
		element->setStageId(Stages::TranxServiceQueueElement::StageID::TRANXACK);
		element->setStageNum(1);
		element->setAckCommitAbort(true);
		element->setIps(tranx->second);
		assert(toAckTranxType.find(tranxID) != toAckTranxType.end());
		element->setTranxType(toAckTranxType[tranxID]);
		tranxAck->SendToDaemon(element);
		assert(toRecover.find(element->GetTranxID()) != toRecover.end());
		toRecover.erase(element->GetTranxID());
	}
	for (auto tranx = toAckAbort.begin(); tranx != toAckAbort.end(); ++tranx) {
		Stages::TranxServiceQueueElement *element = new Stages::TranxServiceQueueElement();
		//element->goThroughStage("PostRecovery");
		Util::TranxID tranxID = tranx->first;
		element->SetTranxID(tranxID);
		element->setStageId(Stages::TranxServiceQueueElement::StageID::TRANXACK);
		element->setStageNum(1);
		element->setAckCommitAbort(false);
		element->setIps(tranx->second);
		assert(toAckTranxType.find(tranxID) != toAckTranxType.end());
		element->setTranxType(toAckTranxType[tranxID]);
		tranxAck->SendToDaemon(element);
		assert(toRecover.find(element->GetTranxID()) != toRecover.end());
		toRecover.erase(element->GetTranxID());
	}
	toAckCommit.clear();
	toAckAbort.clear();
	toAckTranxType.clear();
}

} /* namespace Server */
} /* namespace DTranx */

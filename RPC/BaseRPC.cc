/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "BaseRPC.h"

namespace DTranx {
namespace RPC {

BaseRPC::BaseRPC()
		: responseMessage(NULL) {
	requestMessage = new GRPCRequestMessage();
	//responseMessage = new GRPCResponseMessage();
}

BaseRPC::BaseRPC(Buffer reqBuffer, Buffer repBuffer) {
	requestMessage = new GRPCRequestMessage();
	responseMessage = new GRPCResponseMessage();
	if (reqBuffer.GetData() != NULL) {
		assert(parse(reqBuffer, *requestMessage, 0));
	}
	if (repBuffer.GetData() != NULL) {
		assert(parse(repBuffer, *responseMessage, 0));
	}
}

BaseRPC::BaseRPC(GRPCRequestMessage *requestMessage, GRPCResponseMessage *responseMessage)
		: requestMessage(requestMessage), responseMessage(responseMessage) {
}

BaseRPC::BaseRPC(BaseRPC&& other)
		: requestMessage(other.requestMessage), responseMessage(other.responseMessage) {
	other.requestMessage = NULL;
	other.responseMessage = NULL;
}

BaseRPC& BaseRPC::operator=(BaseRPC&& other) {
	requestMessage = other.requestMessage;
	responseMessage = other.responseMessage;
	other.requestMessage = NULL;
	other.responseMessage = NULL;
	return *this;
}

/*
 * request ip/port access methods
 */

std::string BaseRPC::GetReqIP() {
	if (requestMessage == NULL) {
		assert(false);
		return 0;
	}
	std::string ip = std::to_string(requestMessage->ipint().fieldone()) + '.'
			+ std::to_string(requestMessage->ipint().fieldtwo()) + '.'
			+ std::to_string(requestMessage->ipint().fieldthree()) + '.'
			+ std::to_string(requestMessage->ipint().fieldfour());
	return ip;
}

void BaseRPC::SetReqIP(uint32_t* ip) {
	if (requestMessage == NULL) {
		return;
	}
	requestMessage->mutable_ipint()->set_fieldone(ip[0]);
	requestMessage->mutable_ipint()->set_fieldtwo(ip[1]);
	requestMessage->mutable_ipint()->set_fieldthree(ip[2]);
	requestMessage->mutable_ipint()->set_fieldfour(ip[3]);
	return;
}

void BaseRPC::SetReqIP(std::string ipstr) {
	/*
	 if (requestMessage == NULL) {
	 return;
	 }
	 requestMessage->set_ipstr(ipstr);
	 return;
	 */
}

void BaseRPC::GetReqCombinedEndpoint(char *endpoint, char *ip) {
	sprintf(endpoint, "%d.%d.%d.%d%d\0", requestMessage->ipint().fieldone(),
			requestMessage->ipint().fieldtwo(), requestMessage->ipint().fieldthree(),
			requestMessage->ipint().fieldfour(), requestMessage->port());
	sprintf(ip, "%d.%d.%d.%d\0", requestMessage->ipint().fieldone(),
			requestMessage->ipint().fieldtwo(), requestMessage->ipint().fieldthree(),
			requestMessage->ipint().fieldfour());
}

} /* namespace RPC */
} /* namespace DTranx */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * A file abstraction for sequential writing.  The implementation
 * must provide buffering since callers may append small fragments
 * at a time to the file.
 */
#ifndef DTRANX_STAGES_LOG_WRITABLEFILE_H_
#define DTRANX_STAGES_LOG_WRITABLEFILE_H_

#include "Util/LogStatus.h"

namespace DTranx {
namespace Stages {
namespace Log {

class WritableFile {
public:
	WritableFile() {
	}
	virtual ~WritableFile(){

	}

	virtual LogStatus Append(const Slice& data) = 0;
	virtual LogStatus Close() = 0;
	virtual LogStatus Flush() = 0;
	virtual LogStatus Sync() = 0;

private:
	//non-copyable
	WritableFile(const WritableFile&) = delete;
	WritableFile& operator=(const WritableFile&) = delete;
};

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_LOG_WRITABLEFILE_H_ */

In this directory, examples help to test different transaction types, help for debug/perf tuning usage

Files 
	Tranx Directory: contains all transaction client programs
		CreateSnapshot: create snapshot
		RunMetis: initiate repartitioning process
		ShowLock: show locks status in every server
		SnapshotRead: snapshot read transactions
		SimpleTranx: normal transactions
		ShowMap: show the mapping data for a key
	LevelDB: example to use leveldb
	LevelDBScan: scan local leveldb database 
	Metis: example to use metis library
	RunMetisHelper: read from tranxhistory and run metis algorithms
	RunWorkload: run transaction with multiple threads based on a directory of workload data.
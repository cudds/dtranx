#
# Clean.sh helps to clean runtime programs
#	1. kills logcabin and dtranx
#	2. remove program output
#	3. delete logs and possibly databases
#
source Scripts/Utility/ConfigHelper.sh
while getopts ":i:dh" opt; do
	case $opt in
  		i)
   			ipsFile=$OPTARG
    		;;
    	d)
			cleanDB="true"
			;;
   		h)
			echo "This script is to clean the remote DDSBrick program"
			echo "-i: ips file path"
			echo "-d: whether to clean the database or not"
			exit
	esac
done
if [[ -z $ipsFile ]];then
	ipsFile="/home/neal/Documents/dev/DTranx/ips"
fi

let i=0
while read line || [[ -n $line ]];do
	ips[i]=$line
	((++i))
done < $ipsFile

if [[ ${#ips[@]} == 0 ]];then
	echo "ips file is empty"		
	exit
fi

for ip in ${ips[*]}; do
	echo "cleaning $ip server"
	pid=`ssh $RemoteUser@$ip ps -ef|grep DTranx|grep -v grep| awk '{print $2}'`
	ssh $RemoteUser@$ip kill -9 $pid < /dev/null
	pid=`ssh $RemoteUser@$ip ps -ef|grep LogCabin|grep -v grep| awk '{print $2}'`
	ssh $RemoteUser@$ip kill -9 $pid  < /dev/null
	ssh $RemoteUser@$ip rm -rf $RemoteProjDir/DTranx.out $RemoteProjDir/LogCabin.out < /dev/null
	ssh $RemoteUser@$ip rm -rf $RemoteProjDir/Logcabinlog  < /dev/null
	if [[ -n $cleanDB ]] && [[ $cleanDB == "true" ]];then
		ssh $RemoteUser@$ip 'rm -rf `ls -d /users/nigo9731/Test/dtranx.db*`'  < /dev/null
		ssh $RemoteUser@$ip rm -rf $RemoteProjDir/dtranx.mapdb  < /dev/null
	fi
	ssh $RemoteUser@$ip 'rm `ls /users/nigo9731/Test/dtranx.log*`'  < /dev/null
	ssh $RemoteUser@$ip rm $RemoteProjDir/gcdtranx.log  < /dev/null
	ssh $RemoteUser@$ip 'rm `ls /mnt/mem/dtranx.log*`'  < /dev/null
	ssh $RemoteUser@$ip 'rm `ls /mnt/mem/gcdtranx.log*`'  < /dev/null
done
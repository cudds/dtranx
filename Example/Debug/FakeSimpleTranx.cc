/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * FakeSimpleTranx send requests to the server without waiting for replies
 * it's a open loop pressure test
 */

#include <getopt.h>
#include <fstream>
#include "DTranx/Client/ClientTranx.h"
#include "DTranx/Util/ConfigHelper.h"
#include "DTranx/Util/Log.h"
#include "Util/StringUtil.h"
using namespace DTranx;

/**
 * Parses argv for the main function.
 */
class OptionParser {
public:
	OptionParser(int& argc, char**& argv)
			: argc(argc), argv(argv), rep(10000000) {
		while (true) {
			static struct option longOptions[] = { { "cluster",
			required_argument, NULL, 'c' }, { "key", required_argument, NULL, 'k' }, { "repetition",
			required_argument, NULL, 'r' }, { "help",
			no_argument, NULL, 'h' }, { 0, 0, 0, 0 } };
			int c = getopt_long(argc, argv, "c:k:h", longOptions, NULL);

			// Detect the end of the options.
			if (c == -1)
				break;

			switch (c) {
			case 'c':
				serverIP = optarg;
				break;
			case 'k':
				key = optarg;
				break;
			case 'r':
				rep = strtoul(optarg, NULL, 10);
				break;
			case 'h':
				usage();
				exit(0);
			case '?':
			default:
				// getopt_long already printed an error message.
				usage();
				exit(1);
			}
		}
	}

	void usage() {
		std::cout << "Usage: " << argv[0] << " [options] <servers>" << std::endl;
		std::cout << "Options: " << std::endl;
		std::cout << "  -c, serverip" << std::endl;
		std::cout << "  -k, key string" << std::endl;
		std::cout << "  -r, repetition, default 10,000,000" << std::endl;
		std::cout << "  -h, --help              " << "Print this usage information" << std::endl;
	}

	int& argc;
	char**& argv;
	std::string serverIP;
	std::string key;
	uint64_t rep;
};

int main(int argc, char** argv) {
	OptionParser options(argc, argv);
	Util::ConfigHelper configHelper;
	Util::Log::setLogPolicy( { { "Client", "VERBOSE" }, { "RPC", "VERBOSE" } });
	configHelper.readFile("DTranx.conf");
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(1);
	std::string serverIP = options.serverIP;
	std::string key = options.key;
	uint64_t rep = options.rep;
	std::vector<std::string> ips;
	ips.push_back(serverIP);

	Client::ClientTranx *clientTranx = new DTranx::Client::ClientTranx(
			std::stoul(configHelper.read("routerFrontPort").c_str(), nullptr, 10), context, ips,
			configHelper.read("SelfAddress"), 30030, false);

	for (int i = 0; i < rep; ++i) {
		clientTranx->ReadDebug(key);
	}
	sleep(10);
	delete clientTranx;
	return 0;
}

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * Server runs a loop to receive RPC calls and dispatch to the thread pool
 * It starts server main thread.
 * server main thread will launch all the server threads that connect to the routers.
 *
 * there are three mode: unittest, integ test, normal
 * unittest: running locally, not configurable but determined by if RSMHelper is null
 * integ test: running in docker machines, affect dispatch service if waiting is necessary in work thread
 * 	configured in DTranx.conf
 * normal: normal running
 */

#ifndef DTRANX_SERVER_SERVER_H_
#define DTRANX_SERVER_SERVER_H_

#include <unordered_map>
#include "RSMHelper.h"
#include "RPC/ZeromqProxy.h"
#include "DTranx/Util/ConfigHelper.h"
#include "Service/ClientService.h"
#include "Service/TranxService.h"
#include "Service/StorageService.h"
#include "DaemonStages/TranxAck.h"
#include "DaemonStages/GarbageCollector.h"
#include "DaemonStages/Repartition.h"
#include "RecoverLog.h"
#include "Stages/TranxRPCHelper.h"
#include "Stages/TranxStates.h"
#include "Stages/Storage.h"
#include "Stages/Log/TranxLog.h"
#include "Stages/Lock/WriteBlockLockTable.h"
#include "Stages/ServerReply.h"
#include "SharedResources/TranxServerInfo.h"
#include "SharedResources/TranxServiceSharedData.h"

namespace DTranx {
namespace Server {

class Server {
public:
	explicit Server(Util::ConfigHelper& config, RSMHelper* rsmHelper = NULL);
	virtual ~Server() noexcept(false) {
		ShutDown();
		delete clientService;
		delete tranxService;
		delete storageService;

		delete tranxAck;
		delete gc;
		delete repartition;
		delete recoverLog;

		delete tranxRPCHelper;
		delete tranxStates;
		delete localStorage;
		delete tranxLog;
		delete lockTable;
		delete replyThread;
		delete tranxServerInfo;
		delete tranxServiceSharedData;
	}
	/*
	 * Start/ShutDown: functions that launch/shut the ServerMainThread.
	 * ServerMainThread: launches many server threads
	 */
	void Start();
	void ShutDown();
	void ServerMainThread();

	/*
	 * all the InitForTest is used in unit test and integration test where the mapping and storage are initialized instead of hashing
	 *  the first three are basically the same because they all fall into the third InitForTest
	 *  the fourth one is loading a file with data and mapping
	 * InitDBForPerfTest is used to initialize the database with hash partitioning
	 * 	args:
	 * 		isRead: whether to read the data into the database or insert data into the database.
	 * 		initializedDB: the size of the database.
	 */
	void InitForTest(std::string fileName);
	void InitDBForPerfTest(bool isRead, uint64 intializedDB);

	RSMHelper *rsmHelper;

	/*
	 * DumpPerfLog tells every stages to output its perf log
	 *
	 * TODO: memory leak when try_enqueue fails
	 */
	void DumpPerfLog();

	void AddToFreeList(RPC::ServerRPC *element) {
		element->Clear();
		freeList.try_enqueue(element);
	}

private:
	/*
	 * cluster/Server Basic Info
	 * config: system configuration
	 * allNodes: used throughout the system: ServerReply MappingDS, TranxService, Server itself to initialize data
	 *
	 */
	Util::ConfigHelper config;
	std::vector<std::string> allNodes;
	/*
	 * all stages and data
	 */

	Service::ClientService *clientService;
	Service::TranxService *tranxService;
	Service::StorageService *storageService;
	DaemonStages::TranxAck *tranxAck;
	DaemonStages::GarbageCollector *gc;
	DaemonStages::Repartition *repartition;
	RecoverLog *recoverLog;
	Stages::TranxRPCHelper *tranxRPCHelper;
	Stages::TranxStates *tranxStates;
	Stages::StringStorage *localStorage;
	Stages::Log::TranxLog *tranxLog;
	Stages::Lock::WriteBlockLockTable *lockTable;
	Stages::ServerReply *replyThread;
	SharedResources::TranxServerInfo *tranxServerInfo;
	SharedResources::TranxServiceSharedData *tranxServiceSharedData;

	/*
	 * serverMainThread is a thread that launches all the other server threads
	 * server threads related info
	 */
	std::vector<boost::thread> serverThreads;
	void ServerThread(std::string port);
	std::atomic_bool terminate;
	boost::thread serverMainThread;

	/*
	 * Init is called in the server thread to initialize storage service instances if not already
	 */
	void Init();

	/*
	 * messagesFromClients is to avoid re-exec client transactions
	 */
	Service::MessageIDSet messagesFromClients;
	Service::MessageIDSet messagesFromPeers;

	/*
	 * freeList is used to reuse the ServerRPC to avoid new/delete
	 */
	moodycamel::ConcurrentQueue<RPC::ServerRPC *> freeList;

	//Server is non copyable
	Server(const Server&) = delete;
	Server& operator=(const Server&) = delete;
};

} /* namespace Server */
} /* namespace DTranx */

#endif /* DTRANX_SERVER_SERVER_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_STAGES_PERFSTAGE_H_
#define DTRANX_STAGES_PERFSTAGE_H_

#include <cassert>
#include <string>
#include <unordered_set>
#include <vector>

namespace DTranx {
namespace Stages {

/*
 * In order to log performance metrics: response time/queue length/long latency
 * 	First, it should be enabled.
 * 	Second, profile frequency should be adjusted.
 * 	Finally, index number should be passed to AddXXSample function to isolate thread accesses.
 * 		Comment below explains the regular index numbers.
 * 		e.g. 5 is used for dispatch thread
 *	 	 	 6 is used for external thread, like ClientService
 *  Above all, the stage classes should be responsible for how to allocate
 *  	the sample arrays to difference threads in its own stage.
 */

class PerfStage {
public:
	PerfStage(std::string stageName);
	virtual ~PerfStage() noexcept(false) {

	}

	enum PerfKind {
		QUEUE_LENGTH, RESPONSE_TIME,
	};

	void Enable(PerfKind perfKind) {
		if (perfKind == QUEUE_LENGTH) {
			queueEnabled = true;
		} else if (perfKind == RESPONSE_TIME) {
			responseEnabled = true;
		}
	}

	bool IsEnabled(PerfKind perfKind) {
		if (perfKind == QUEUE_LENGTH) {
			return queueEnabled;
		} else if (perfKind == RESPONSE_TIME) {
			return responseEnabled;
		}
		return false;
	}

	void SetQueuePerfFreq(uint32_t newQueuePerfFreq) {
		queuePerfFreq = newQueuePerfFreq;
	}

	void SetResponsePerfFreq(uint32_t newResponsePerfFreq) {
		responsePerfFreq = newResponsePerfFreq;
	}

	void AddResponseTimeSample(uint32_t index, uint32_t responseTime) {
		assert(index < 10);
		responseTimeSamples[index].push_back(responseTime);
	}

	std::vector<uint32_t> GetResponseTimeSamplesAndClear(uint32_t index) {
		assert(index < 10);
		std::vector<uint32_t> responseTimeSamplesCopy = responseTimeSamples[index];
		responseTimeSamples[index].clear();
		return responseTimeSamplesCopy;
	}

	void AddQueueLengthSample(uint32_t index, uint32_t queueLength);

	std::vector<uint32_t> GetQueueLengthSamplesAndClear(uint32_t index);

	void AddLongLatency(uint32_t index, int task) {
		taskLongLatency[index].push_back(task);
	}

	std::vector<int> GetLongLatency(uint32_t index) {
		return taskLongLatency[index];
	}

	/*
	 * for performance monitoring
	 *
	 * responseEnabled/queueEnabled: enabled perf metrics
	 * queuePerfFreq means how many requests are pushed to the queue before the queue length is recorded
	 * responsePerfFreq means how many requests are pushed to the queue before the response time is recorded
	 *
	 */
	std::string stageName;
	bool responseEnabled;
	bool queueEnabled;
	uint32_t queuePerfFreq;
	uint32_t responsePerfFreq;

	/*
	 * the following members *Samples are used to temporarily store the performance samples
	 * right now 10 is more than sufficient for the threads in each stage
	 *
	 * rules:
	 * 0-4: splitted thread
	 * 5: dispatch thread
	 * 6: external thread, like ClientService
	 * 7: internal thread, like ClientServiceInternal, TranxRPCInternal
	 * 8-9: special usage for customization
	 *
	 * taskLongLatency records tasks for the stage that takes too long
	 */
	std::vector<uint32_t> responseTimeSamples[10];
	std::vector<uint32_t> queueLengthSamples[10];
	std::vector<int> taskLongLatency[10];
};

} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_PERFSTAGE_H_ */

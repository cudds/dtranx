/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * ClientCache manages local cache of data : mapping of key to nodes
 * It's thread safe
 * temporary client side mapping caching, will be updated when reading goes to the wrong node
 */

#ifndef DTRANX_CLIENT_CLIENTCACHE_H_
#define DTRANX_CLIENT_CLIENTCACHE_H_
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <boost/thread.hpp>
#include "DTranx/Util/types.h"

namespace DTranx {
namespace Client {

using DTranx::uint64;

class ClientCache {
public:
	ClientCache();
	virtual ~ClientCache() noexcept(false);

	/*
	 * mapping
	 * 	GetMapping: get one server address from the mapping ip list
	 * 	GetAllMapping: get all server addresses of the mapping ip list
	 */
	std::string GetMapping(const std::string& key);
	std::unordered_set<std::string> GetAllMapping(const std::string& key);
	void UpdateMapping(const std::string& key, const std::unordered_set<std::string>& newips);

	/*
	 * data
	 */
	std::string ReadLocal(const std::string& key, uint64& version);
	void UpdateLocal(const std::string& key, const std::string& value, const uint64& version);
	void Invalidate(const std::string& key);

	/*
	 * threadsafety
	 */
	void SetThreadSafety(bool threadSafe) {
		this->threadSafe = threadSafe;
	}

	bool GetThreadSafety() {
		return threadSafe;
	}

	static const uint64 NO_SUCH_DATA;
	static const std::string NO_SUCH_MAP;

private:
	std::unordered_map<std::string, std::unordered_set<std::string> > mapping;
	std::unordered_map<std::string, std::pair<uint64, std::string> > data;
	uint64_t randSeed;
	boost::mutex mutex;
	bool threadSafe;
};

} /* namespace Client */
} /* namespace DTranx */

#endif /* CLIENT_CLIENTCACHE_H_ */

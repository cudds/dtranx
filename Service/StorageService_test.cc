/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "StorageService.h"
#include "Util/StaticConfig.h"
#include "Server/Server.h"
#include "DTranx/Util/types.h"
#include "Util/FileUtil.h"
#include "Util/StaticConfig.h"
#include "DTranx/Client/ClientTranx.h"
#include "RPC/ClientRPC.h"
#include "DTranx/Util/Log.h"

using DTranx::uint32;
/*
class StorageServiceTest_DTranxServer: public ::testing::Test {
public:
	StorageServiceTest_DTranxServer() {
		DTranx::Util::Log::setLogPolicy( { { "Storage", "PROFILE" }, { "Server", "PROFILE" } });
		port = "60010";
		localhost = "127.0.0.1";
		configHelper.readFile("DTranx.conf");
		server = new DTranx::Server::Server(configHelper);

		std::shared_ptr<DTranx::Stages::StringStorage> storage = std::make_shared<
				DTranx::Stages::StringStorage>("dtranx.db");
		server->InitForTest(storage);
		server->Start();
	}
	~StorageServiceTest_DTranxServer() {
		delete server;
		DTranx::Util::FileUtil::RemoveDir("dtranx.db");
		DTranx::Util::FileUtil::RemoveDir("dtranx.mapdb");
	}

	DTranx::Server::Server *server;
	DTranx::Util::ConfigHelper configHelper;
	std::string port;
	std::string localhost;

	StorageServiceTest_DTranxServer(const StorageServiceTest_DTranxServer&) = delete;
	StorageServiceTest_DTranxServer& operator=(const StorageServiceTest_DTranxServer&) = delete;
};

TEST_F(StorageServiceTest_DTranxServer, WriteData) {
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(
			configHelper.read<uint32>("maxIOThreads"));
	DTranx::Client::Client *client = new DTranx::Client::Client(configHelper.read("SelfAddress"),
			configHelper.read("routerFrontPort"), context);
	assert(client->Bind(localhost, port));
	DTranx::RPC::ClientRPC* clientRPC = new DTranx::RPC::ClientRPC(
			DTranx::RPC::GServiceID::StorageService, client);
	clientRPC->SetStorageOpcode(DTranx::Service::GOpCode::WRITE_DATA);
	clientRPC->SetReqIP(localhost);
	clientRPC->SetReqPort(port);

	DTranx::Service::GWriteData::Request *request =
			clientRPC->GetStorageRequest()->mutable_writedata()->mutable_request();
	DTranx::Service::GItem *item = request->add_items();
	item->set_key("key1#1");
	item->set_value("1#key1value1");

	client->CallRPC(clientRPC);
	assert(client->BlockPoll(clientRPC));
	DTranx::Service::GWriteData::Response *response =
			clientRPC->GetStorageResponse()->mutable_writedata()->mutable_response();
	assert(response->status() == DTranx::Service::GStatus::OK);
	delete clientRPC;
	delete client;

	std::vector<std::string> cluster;
	cluster.push_back(localhost);
	DTranx::Client::ClientTranx *ClientTranx = new DTranx::Client::ClientTranx(
			configHelper.read("routerFrontPort"), context, cluster, localhost, port);
	std::string key, value;
	key = "key1";
	ClientTranx->Read(key, value);
	EXPECT_EQ("key1value1", value);
	delete ClientTranx;
}
*/


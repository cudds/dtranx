/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * A file abstraction for reading sequentially through a file
 */

#ifndef DTRANX_STAGES_LOG_SEQUENTIALFILE_H_
#define DTRANX_STAGES_LOG_SEQUENTIALFILE_H_

#include "Util/LogStatus.h"

namespace DTranx {
namespace Stages {
namespace Log {

class SequentialFile {
public:
	SequentialFile() {
	}
	virtual ~SequentialFile(){

	}

	// Read up to "n" bytes from the file.  "scratch[0..n-1]" may be
	// written by this routine.  Sets "*result" to the data that was
	// read (including if fewer than "n" bytes were successfully read).
	// May set "*result" to point at data in "scratch[0..n-1]", so
	// "scratch[0..n-1]" must be live when "*result" is used.
	// If an error was encountered, returns a non-OK status.
	//
	// REQUIRES: External synchronization
	virtual LogStatus Read(size_t n, Slice* result, char* scratch) = 0;

	// Skip "n" bytes from the file. This is guaranteed to be no
	// slower that reading the same data, but may be faster.
	//
	// If end of file is reached, skipping will stop at the end of the
	// file, and Skip will return OK.
	//
	// REQUIRES: External synchronization
	virtual LogStatus Skip(uint64_t n) = 0;

private:
	// Noncopyable
	SequentialFile(const SequentialFile&) = delete;
	SequentialFile& operator=(const SequentialFile&) = delete;
};

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_LOG_SEQUENTIALFILE_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "GCFactory.h"

using namespace DTranx;

TEST(GCFactory, basicmethods) {
	Stages::GCFactory gcFactory(1, 100, 10000);
	gcFactory.AddTranxID(3);
	gcFactory.AddTranxID(2);
	EXPECT_EQ(0, gcFactory.GetEarliest());
	gcFactory.AddTranxID(1);
	EXPECT_EQ(3, gcFactory.GetEarliest());
	EXPECT_EQ(0, gcFactory.discreteSet.size());
	EXPECT_EQ(0, gcFactory.prevBroadcast);
	gcFactory.UpdateEarliest(15);
	EXPECT_EQ(15, gcFactory.GetEarliest());
	EXPECT_EQ(0, gcFactory.discreteSet.size());
	EXPECT_EQ(15, gcFactory.prevBroadcast);
	for (int i = 4; i <= 1000; i++) {
		gcFactory.AddTranxID(i);
	}
	EXPECT_EQ(1000, gcFactory.GetEarliest());
	EXPECT_TRUE(gcFactory.IsOutdated(555));
	EXPECT_EQ(15, gcFactory.prevBroadcast);
	gcFactory.UpdateEarliest(2, 10);
	EXPECT_FALSE(gcFactory.IsOutdated(2, 15));
	EXPECT_TRUE(gcFactory.IsOutdated(2, 8));
	EXPECT_EQ(10, gcFactory.GetEarliest(2));

	std::unordered_map<int, uint64> allNodeGC = gcFactory.GetAllNodeGC();
	EXPECT_EQ(2, allNodeGC.size());
	EXPECT_TRUE(allNodeGC.find(2) != allNodeGC.end());
	EXPECT_EQ(10, allNodeGC[2]);
}

TEST(GCFactory, broadcast) {
	Stages::GCFactory gcFactory(1, 100, 100);
	for (int i = 1; i <= 99; i++) {
		gcFactory.AddTranxID(i);
	}
	EXPECT_FALSE(gcFactory.ToBroadcast());
	sleep(1);
	EXPECT_TRUE(gcFactory.ToBroadcast());

	gcFactory = Stages::GCFactory(1, 100, 100000);
	for (int i = 1; i <= 100; i++) {
		gcFactory.AddTranxID(i);
	}
	EXPECT_TRUE(gcFactory.ToBroadcast());
	EXPECT_EQ(100, gcFactory.prevBroadcast);
}

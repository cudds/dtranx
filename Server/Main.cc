/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * 	Main function is the entry point of the whole project
 * 	it launches the replicated state machine instance and the server instance
 *
 * Arguments:
 * 	config: configuration file
 * 	ips: ips file that contains all the ip addresses of the cluster
 * 	test: test file that contains initial database and mapping data
 * 		there are two kinds of test formats that are supported
 * 		(1) three columns: key, value, ip
 * 		(2) one column: key
 * 		in the second case, value will be initialized as "value_init"
 */

#include <iostream>
#include <fstream>
#include <getopt.h>
#include "Server.h"
#include "RSMHelper.h"
#include "DTranx/Util/Log.h"
#include <gperftools/profiler.h>
using namespace DTranx;

Server::Server *server;
Server::RSMHelper* rsmHelper;

/*
 * SIGINT handler
 */
void ExitHandler(int param) {
	//::ProfilerStop();
	VERBOSE("sigint is received");
	rsmHelper->Shutdown();
	VERBOSE("rsmHelper shut");
	delete rsmHelper;
	server->ShutDown();
	VERBOSE("sigint is processed");
}

namespace {
class OptionParser {
public:
	OptionParser(int& argc, char**& argv)
			: argc(argc), argv(argv), configFilename("DTranx.conf"), ipFilename(""), testFile() {
		while (true) {
			static struct option longOptions[] = { { "config",
			required_argument, NULL, 'c' }, { "ips",
			required_argument, NULL, 'i' }, { "test",
			required_argument, NULL, 't' }, { "help", no_argument, NULL, 'h' }, { 0, 0, 0, 0 } };
			int c = getopt_long(argc, argv, "c:i:t:h", longOptions, NULL);

			// Detect the end of the options.
			if (c == -1)
				break;

			switch (c) {
				case 'h':
					usage();
					exit(0);
				case 'c':
					configFilename = optarg;
					break;
				case 'i':
					ipFilename = optarg;
					break;
				case 't':
					testFile = optarg;
					break;
				default:
					usage();
					exit(1);
			}
		}
	}

	void usage() {
		std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
		std::cout << "Options: " << std::endl;
		std::cout << "  -h, --help          " << "Print this usage information" << std::endl;
		std::cout << "  -c, --config <file> " << "Specify the configuration file "
				<< "(default: DTranx.conf)" << std::endl;
		std::cout << "  -i, --ips <file> " << "Specify the ips file " << "(default: empty)"
				<< std::endl;
		std::cout << "  -t, --test <file> " << "Specify the test initialization file "
				<< "(default: empty)" << std::endl;
	}

	int& argc;
	char**& argv;
	std::string configFilename;
	std::string ipFilename;
	std::string testFile;
};

}  // anonymous namespace

/*
 * Main Entry
 */
int main(int argc, char** argv) {
	OptionParser options(argc, argv);
	Util::ConfigHelper configHelper;
	Util::Log::setLogPolicy( { { "Client", "PROFILE" }, { "Server", "PROFILE" }, { "Service",
			"PROFILE" }, { "DaemonStages", "PROFILE" }, { "SharedResources", "PROFILE" }, { "RPC", "PROFILE" }, { "Util", "PROFILE" }, {
			"Stages", "PROFILE" } });

	configHelper.readFile(options.configFilename);

	/*
	 * RSMHelper is storing the cluster membership
	 */
	rsmHelper = new Server::RSMHelper(configHelper);
	if (!options.ipFilename.empty()) {
		std::vector<std::string> ips;
		std::ifstream ipFile(options.ipFilename);
		if (!ipFile.is_open()) {
			VERBOSE("cannot open %s", options.ipFilename.c_str());
			exit(0);
		}
		std::string ip;
		while (std::getline(ipFile, ip)) {
			ips.push_back(ip);
		}
		VERBOSE("creating rsm");
		rsmHelper->Init(ips);
	}
	NOTICE("DTranx starting!!!");
	server = new Server::Server(configHelper, rsmHelper);
	/*
	 * initialize database and mapping data from a local file
	 */
	if (!options.testFile.empty()) {
		server->InitForTest(options.testFile);
	}
	signal(SIGINT, ExitHandler);
	//::ProfilerStart("gprofile");
	server->ServerMainThread();
	VERBOSE("deleting server");
	delete server;
	Util::StaticConfig::ReleaseInstance();
	NOTICE("DTranx exiting!!!");
}

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <boost/thread.hpp>
#include "DTranx/RPC/ZeromqReceiver.h"
#include "DTranx/RPC/ZeromqSender.h"
#include "gtest/gtest.h"

namespace DTranx {
namespace RPC {

class ZeromqTest: public ::testing::Test {
public:
	ZeromqTest() {
		port = "30000";
		localhost = "127.0.0.1";
		context = std::make_shared<zmq::context_t>(10);
		sender = new ZeromqSender(context);
		receiver = new ZeromqReceiver(context);
		assert(receiver->Bind(localhost, port));
	}
	~ZeromqTest() {
		if (receiverThread.joinable()) {
			receiverThread.join();
		}
		delete sender;
		delete receiver;
		context->close();
	}

	void ReceiverThread(std::vector<std::string> expectedStrings) {
		int index = 0;
		while (true) {
			int size;
			void *message = NULL;
			zmsg::ustring clientID;
			while (message == NULL) {
				if (index == expectedStrings.size()) {
					return;
				}
				message = receiver->Wait(size, clientID);
				usleep(100);
			}
			std::string actualString;
			for (int i = 0; i < size; i++) {
				actualString += *((char *) message + i);
			}
			assert(actualString == expectedStrings[index++]);
			delete[] static_cast<uint8*>(message);
		}
	}

	void StartReceiver(std::vector<std::string> expectedStrings) {
		receiverThread = boost::thread(&ZeromqTest::ReceiverThread, this, expectedStrings);
	}

	std::shared_ptr<zmq::context_t> context;
	boost::thread receiverThread;
	ZeromqSender* sender;
	ZeromqReceiver *receiver;
	std::string port;
	std::string localhost;

	ZeromqTest(const ZeromqTest&) = delete;
	ZeromqTest& operator=(const ZeromqTest&) = delete;
};

TEST_F(ZeromqTest, receiver) {
	std::vector<std::string> expectedStrings;
	expectedStrings.push_back("hello world");
	expectedStrings.push_back("string2");
	StartReceiver(expectedStrings);
	assert(sender->ConnectTcp(localhost, port));
	char str[20];
	strcpy(str, "hello world");
	sender->Send(str, 11);
	strcpy(str, "string2");
	sender->Send(str, 7);
}

} /* namespace RPC */
} /* namespace DTranx */

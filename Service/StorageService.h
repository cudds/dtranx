/*
 *  Author: Ning Gao(nigo9731@colorado.edu)
 *
 *  Class Summary:
 *  StorageService is the service handler for Storage access from clients.
 *  concurrent access to StorageService is OK for RPC readData calls
 */

#ifndef DTRANX_SERVICE_STORAGESERVICE_H_
#define DTRANX_SERVICE_STORAGESERVICE_H_

#include <memory>
#include "RPC/ServerRPC.h"
#include "DTranx/RPC/ZeromqSender.h"
#include "Service.h"
#include "DTranx/Util/ConfigHelper.h"
#include "SharedResources/MappingDS.h"

namespace DTranx {
namespace Service {

class StorageService: public Service {
public:
	StorageService(SharedResources::TranxServerInfo *tranxServerInfo,
			SharedResources::TranxServiceSharedData *tranxServiceSharedData);
	virtual void HandleRPC(RPC::ServerRPC *serverRPC);

	virtual void StartService(int numOfServiceThreads,
			std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) {
		bool enableCoreBinding = false;
		uint32_t coreIDIndex = 0;
		assert(numOfServiceThreads >= 1);
		enableCoreBinding = (coreIDs.size() == numOfServiceThreads + 1);
		for (int i = 0; i < numOfServiceThreads; ++i) {
			serviceThread.push_back(boost::thread(&StorageService::ServiceThread, this, i));
			if (enableCoreBinding) {
				Util::ThreadHelper::PinToCPUCore(serviceThread[i], coreIDs[coreIDIndex++]);
			}
		}
		serviceInternalThread.push_back(
				boost::thread(&StorageService::ServiceInternalThread, this, 0));
		if (enableCoreBinding) {
			Util::ThreadHelper::PinToCPUCore(serviceInternalThread[0], coreIDs[coreIDIndex++]);
		}
	}

private:

	std::string selfAddress;
	virtual void ServiceThread(int index);
	virtual void ServiceInternalThread(int index);

	void ReadData(RPC::ServerRPC *serverRPC);
	void WriteData(RPC::ServerRPC *serverRPC);
	void Process(Stages::TranxServiceQueueElement *element);

	// StorageService is non-copyable.
	StorageService(const StorageService&) = delete;
	StorageService& operator=(const StorageService&) = delete;
};

} /* namespace Service */
} /* namespace DTranx */

#endif /* DTRANX_SERVICE_STORAGESERVICE_H_ */

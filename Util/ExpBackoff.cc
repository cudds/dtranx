/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <boost/thread.hpp>
#include "ExpBackoff.h"
#include "Util/StaticConfig.h"

namespace DTranx {
namespace Util {

ExpBackoff::ExpBackoff() {
	initialBackoff = std::stoul(Util::StaticConfig::GetInstance()->Read("epochInitalBackoff"));
	maxBackoff = std::stoul(Util::StaticConfig::GetInstance()->Read("epochMaxBackoff"));
	maxRetries = std::stoul(Util::StaticConfig::GetInstance()->Read("epochMaxRetries"));
	retries = 1;
	backoff = initialBackoff;
}

ExpBackoff::ExpBackoff(uint32 initialBackoff, uint32 maxBackoff, uint maxRetries)
		: initialBackoff(initialBackoff), maxBackoff(maxBackoff), maxRetries(maxRetries) {
	retries = 1;
	backoff = initialBackoff;
}

ExpBackoff::~ExpBackoff() noexcept(false) {
}

bool ExpBackoff::wait() {
	if (retries > maxRetries || backoff > maxBackoff) {
		return false;
	}
	boost::this_thread::sleep_for(boost::chrono::milliseconds(backoff));
	backoff *= 2;
	retries++;
	return true;
}

} /* namespace Util */
} /* namespace DTranx */

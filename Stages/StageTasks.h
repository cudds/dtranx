/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_STAGES_STAGETASKS_H_
#define DTRANX_STAGES_STAGETASKS_H_

namespace DTranx {
namespace Stages {

enum TranxStatesStageTask {
	_CheckState = 1,
	_CheckAndSetReadyState = 2,
	_CheckAndSetCommitState = 3,
	_SetToCommitState = 4,
	_CheckAndSetAbortState = 5,
	_SetToAbortState = 6,
	_CheckAndSetMigrateState = 7,
	_SetToAck = 8,
	_UpdateGC = 9,
	_GarbageCollectAck = 10,
};

enum State {
	NOTEXIST, READY, COMMIT, ABORT, GC, MIGRATION
};

namespace Lock {
enum WriteBlockLockTableStageTask {
	_REQUESTEPOCHLOCK = 1,
	_REQUESTLOCKSMAP = 2,
	_REQUESTLOCKSBLOCKING = 3,
	_RELEASEEPOCHLOCK = 4,
	_RELEASELOCKSVECTOR = 5,
	_RELEASELOCKSMAP = 6,
	_PRINTLOCKSTATUS = 7,
	_RELEASELOCKSWHENFAIL = 8,
	_CLEANBLACKLIST = 9,
};

enum BasicLockTableLockMode {
	Shared = 1, Exclusive = 2
};
}

enum StorageStageTask {
	_CheckReadSet = 1,
	_ReadSnapshot = 2,
	_ReadWithVersion = 3,
	_Write = 4,
	_WritePlain = 5,
	_Delete = 6,
	_CreateSnapshot = 7,
	_SearchAll = 8,
};

namespace Log {

enum TranxLogStageTask {
	_Prepare = 1,
	_PrepareRepartition = 2,
	_ParticipantAbort = 3,
	_CoordinatorAbort = 4,
	_Ready = 5,
	_ReadyRepartition = 6,
	_Commit = 7,
	_Commit1PC = 8,
	_CoordinatorAck = 9,
};
}

enum TranxRPCHelperPollingResult {
	AllOK = 1, AnyFail = 2, Timeout = 3,
};

/*
 * _SENDNOW in TranxRPCHelperStageTask/TranxPeerStageTask means send to the TranxPeer/peer servers right away
 * 	this request comes from TranxRPCHelper Internal/TranxPeer Recv after the rpcInternalProcessQueue/responses
 * 	are pushed and waited on
 */
enum TranxRPCHelperStageTask {
	_SENDPREPANDPOLL = 1,
	_SENDABORTANDPOLL = 2,
	_SENDCOMMITANDPOLL = 3,
	_SENDINQUIRYANDPOLL = 4,
	_SENDMIGRATEANDPOLL = 5,
	_SENDPREPANDPOLLREPARTITION = 6,
	_SENDNOWTRANXRPCHELPER = 7,
};

enum TranxPeerStageTask{
	_INFORMPEERRECV = 1,
	_SENDNOWTRANXPEER = 2,
	_OUTPUTPERFPEER = 3,
};

} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_STAGETASKS_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "TranxServiceQueueElement.h"
#include "Storage.h"

namespace DTranx {
namespace Stages {

int TranxServiceQueueElement::INVALID_STAGE_THREADID = -1;
int TranxServiceQueueElement::DISPATCH_STAGE_THREADID = -2;
boost::chrono::steady_clock::time_point TranxServiceQueueElement::EPOCH_TIMEPOINT(
		boost::chrono::duration<int>(0));
int TranxServiceQueueElement::DEQUEUE_BULK_SIZE = 20;

TranxServiceQueueElement::TranxServiceQueueElement()
		: serverRPC_(), opcode_(), tranxID_(), tranxType_(), stageID_(StageID::INVALID), stageNum_(
				0), oldStageID_(StageID::INVALID), oldStageNum_(0), terminateInThisStage_(false), items_(), ips_(), newMapping_(), state_(), stateAckCommitAbort_(
		true), stateGCFactory_(0, 1, 1), stateToBroadcast_(false), stateTask_(
				Stages::TranxStatesStageTask::_CheckAndSetReadyState), keysMode_(), keys_(), lockObtained_(
		true), lockStatus_(), lockTask_(
				Stages::Lock::WriteBlockLockTableStageTask::_RELEASEEPOCHLOCK), lockStartTimePoint_(
				EPOCH_TIMEPOINT), lockAlreadyLockedKeys_(), lockEpochAlreadyLocked_(false), writeSet_(), readSet_(), storageKeys_(), storageCheckSucc_(
		true), storageSnapshotExist_(true), storageKeyExist_(true), storageEpoch_(0), storageValue_(), storageVersion_(), storageSearchAllResults_(), splittedInvolvedStageThreads_(), storageTask_(
				StorageStageTask::_CheckReadSet), logType_(), logTask_(
				Stages::Log::TranxLogStageTask::_Ready), tranxRPCResult_(), tranxRPCInquiryResult_(), tranxRPCStartTime_(), tranxRPCNumOfPeerStillWaiting_(
				0), tranxRPCs_(), tranxRPCTask_(TranxRPCHelperStageTask::_SENDPREPANDPOLL), ackCommitAbort_(
		true), isPostRecovery_(false), inquiryStatus_(), tranxLogRecord_(), passedStages_(), stageEnteringTime_(
				EPOCH_TIMEPOINT), isPerfOutPutTask_(false) {
	keysMode_.reserve(10);
	readSet_.reserve(10);
}

TranxServiceQueueElement::~TranxServiceQueueElement() {
	//PrintElement();
}

void TranxServiceQueueElement::Clear() {
	serverRPC_ = NULL;
	tranxID_.Clear();
	stageID_ = StageID::INVALID;
	stageNum_ = 0;
	oldStageID_ = StageID::INVALID;
	oldStageNum_ = 0;
	terminateInThisStage_ = false;
	items_.clear();
	writeItemMapping.clear();
	ips_.clear();
	newMapping_.clear();
	stateAckCommitAbort_ = true;
	stateToBroadcast_ = false;
	stateTask_ = Stages::TranxStatesStageTask::_CheckAndSetReadyState;
	keysMode_.clear();
	keys_.clear();
	lockObtained_ = true;
	lockStatus_.clear();
	lockTask_ = Stages::Lock::WriteBlockLockTableStageTask::_RELEASEEPOCHLOCK;
	lockStartTimePoint_ = EPOCH_TIMEPOINT;
	lockAlreadyLockedKeys_.clear();
	lockEpochAlreadyLocked_ = false;
	writeSet_.clear();
	readSet_.clear();
	storageKeys_.clear();
	storageCheckSucc_ = true;
	storageSnapshotExist_ = true;
	storageKeyExist_ = true;
	storageEpoch_ = 0;
	storageValue_.clear();
	storageVersion_ = 0;
	storageSearchAllResults_.clear();
	splittedInvolvedStageThreads_.clear();
	storageTask_ = StorageStageTask::_CheckReadSet;
	logTask_ = Stages::Log::TranxLogStageTask::_Ready;
	tranxRPCNumOfPeerStillWaiting_ = 0;
	tranxRPCs_.clear();
	tranxRPCTask_ = TranxRPCHelperStageTask::_SENDPREPANDPOLL;
	ackCommitAbort_ = true;
	isPostRecovery_ = false;
	tranxLogRecord_.clear();
	passedStages_.clear();
	stageEnteringTime_ = EPOCH_TIMEPOINT;
	isPerfOutPutTask_ = false;
}

void TranxServiceQueueElement::PrintElement() {
	std::string message;
	message += "Print element(tranxID:" + std::to_string(tranxID_.GetNodeID()) + "#"
			+ std::to_string(tranxID_.GetTranxID()) + ", tranxtype: " + std::to_string(tranxType_)
			+ ", passedstages: ";
	for (auto stage = passedStages_.begin(); stage != passedStages_.end(); ++stage) {
		message += *stage + "->";
	}
	message += "End, stageID: " + std::to_string(stageID_) + ", stageNum: "
			+ std::to_string(stageNum_);
	VERBOSE("%s, memory address: %p", message.c_str(), this);
}

} /* namespace Stages */
} /* namespace DTranx */

import os
import subprocess

#
# Commons provides basic functions like directory manipulation and run bash commands
#

def GetScriptDir(file):
	return os.path.dirname(os.path.realpath(file))

def GetWorkingDir():
	return os.getcwd() + "/"

# convert relative/absolute path to absolute path
def GetAbsolutePath(path):
	if len(path) == 0:
		#if empty path, return itself
		return path
	if path[0] == '/':
		#already absolute path, no change
		return path
	# path is relative, append working directory
	if path[-1] == '/':
		return GetWorkingDir() + path
	else:
		return GetWorkingDir() + path + '/'

def runBash(command):
	out, err = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True).communicate()
	return out
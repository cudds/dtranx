/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <boost/thread.hpp>
#include "gtest/gtest.h"
#include "DTranx/Client/Client.h"
#include "DTranx/RPC/ZeromqReceiver.h"
#include "RPC/ServerRPC.h"
#include "RPC/ClientRPC.h"

using namespace DTranx;

class ClientTest_MockServer: public ::testing::Test {
public:
	ClientTest_MockServer() {
		context = std::make_shared<zmq::context_t>(10);
		selfAddressStr = "127.0.0.1";
		selfAddress[0] = 127;
		selfAddress[1] = 0;
		selfAddress[2] = 0;
		selfAddress[3] = 1;
		listenPort = 30000;
		clientPort = 30030;
		receiver = new RPC::ZeromqReceiver(context);
		assert(receiver->Bind(selfAddressStr, std::to_string(listenPort)));
		replyer = new RPC::ZeromqSender(context);
		replyer->ConnectTcp(selfAddressStr, std::to_string(clientPort));
	}
	~ClientTest_MockServer() {
		if (ServerThread.joinable()) {
			ServerThread.join();
		}
		delete receiver;
		delete replyer;
		context->close();
	}
	void ReceiverThread(int expectedMessageNum) {
		int index = 0;
		while (true) {
			int size;
			void *message = NULL;
			while (message == NULL) {
				if (index == expectedMessageNum) {
					return;
				}
				message = receiver->Wait(size);
			}
			Util::Buffer buffer(message, size);
			RPC::ServerRPC *serverRPC = new RPC::ServerRPC();
			assert(RPC::BaseRPC::parse(buffer, *serverRPC->GetRequest(), 0));

			assert(serverRPC->GetReqIP() == selfAddressStr);
			int messageID = serverRPC->GetReqMessageID();
			assert(messageID <= expectedMessageNum);
			serverRPC->SetRepStatus(RPC::GStatus::OK);
			serverRPC->SetRepMessageID(messageID);
			Util::Buffer localBuffer;
			RPC::BaseRPC::serialize(*serverRPC->GetResponse(), localBuffer, 0);
			replyer->Send(localBuffer.GetData(), localBuffer.GetLength());
			delete serverRPC;
			index++;
		}
	}

	void StartReceiver(int expectedMessageNum) {
		ServerThread = boost::thread(&ClientTest_MockServer::ReceiverThread, this,
				expectedMessageNum);
	}

	RPC::ZeromqReceiver *receiver;
	RPC::ZeromqSender *replyer;
	boost::thread ServerThread;
	/*
	 * selfAddress, selfPort are used for clients to bind
	 */

	std::string selfAddressStr;
	uint32_t selfAddress[4];
	uint32_t listenPort;
	uint32_t clientPort;
	std::shared_ptr<zmq::context_t> context;

	ClientTest_MockServer(const ClientTest_MockServer&) = delete;
	ClientTest_MockServer& operator=(const ClientTest_MockServer&) = delete;
};

TEST_F(ClientTest_MockServer, BlockPoll) {
	Client::Client client(selfAddressStr, listenPort, context);
	assert(client.Bind(selfAddressStr, clientPort));
	StartReceiver(2);
	RPC::ClientRPC* clientRPC = new RPC::ClientRPC(RPC::GServiceID::StorageService, &client);
	clientRPC->SetReqIP(selfAddress);
	clientRPC->SetReqPort(clientPort);
	client.CallRPC(clientRPC);
	EXPECT_TRUE(client.BlockPoll(clientRPC));
	EXPECT_EQ(RPC::GStatus::OK, clientRPC->GetRepStatus());
	delete clientRPC;
	clientRPC = new RPC::ClientRPC(RPC::GServiceID::ClientService, &client);
	clientRPC->SetReqIP(selfAddress);
	clientRPC->SetReqPort(clientPort);
	client.CallRPC(clientRPC);
	EXPECT_TRUE(client.BlockPoll(clientRPC));
	EXPECT_EQ(RPC::GStatus::OK, clientRPC->GetRepStatus());
	delete clientRPC;
}

TEST_F(ClientTest_MockServer, Retry) {
	Client::Client client(selfAddressStr, listenPort, context);
	assert(client.Bind(selfAddressStr, clientPort));
	StartReceiver(2);
	RPC::ClientRPC* clientRPC = new RPC::ClientRPC(RPC::GServiceID::StorageService, &client);
	clientRPC->SetReqIP(selfAddress);
	clientRPC->SetReqPort(clientPort);
	client.CallRPC(clientRPC);
	client.Retry(clientRPC);
	EXPECT_TRUE(client.BlockPoll(clientRPC));
	EXPECT_EQ(RPC::GStatus::OK, clientRPC->GetRepStatus());
	delete clientRPC;
}


/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_DAEMONSTAGES_TRANXACKMOCK_H_
#define DTRANX_DAEMONSTAGES_TRANXACKMOCK_H_

#include "TranxAck.h"

namespace DTranx {
namespace DaemonStages {

class TranxAckMock: public TranxAck {
public:
	TranxAckMock(SharedResources::TranxServerInfo *tranxServerInfo,
			SharedResources::TranxServiceSharedData *tranxServiceSharedData)
			: TranxAck(tranxServerInfo, tranxServiceSharedData), ProcessFunc() {
	}
	~TranxAckMock() {

	}

	void SetProcessFunc(void (*f)(Stages::TranxServiceQueueElement *)) {
		ProcessFunc = f;
	}

	virtual void SendToDaemon(Stages::TranxServiceQueueElement *element) {
		/*
		 * process the request
		 */
		if (ProcessFunc != NULL) {
			ProcessFunc(element);
		}
	}

	void (*ProcessFunc)(Stages::TranxServiceQueueElement *);
};
}
}

#endif /* DTRANX_DAEMONSTAGES_TRANXACKMOCK_H_ */

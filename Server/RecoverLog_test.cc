/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 *  * Note: set correct mappingds so that storage will be updated after logs are processed
 */
#include "gtest/gtest.h"
#include "Util/FileUtil.h"
#include "RecoverLog.h"

#include "Service/TranxService.h"
#include "DaemonStages/TranxAckMock.h"
#include "DaemonStages/Repartition.h"
#include "Stages/TranxStates.h"
#include "Stages/TranxRPCHelper.h"
#include "Stages/Log/TranxLog.h"
#include "Stages/Lock/WriteBlockLockTable.h"
#include "Stages/Storage.h"

using namespace DTranx;

class RecoverLogTest_BasicInstance: public ::testing::Test {
public:
	RecoverLogTest_BasicInstance() {
		Util::Log::setLogPolicy( { { "Server", "VERBOSE" } });
		std::string selfAddress = "192.168.0.1";

		std::vector<std::string> allNodes;
		allNodes.push_back("192.168.0.1");
		allNodes.push_back("192.168.0.2");
		allNodes.push_back("192.168.0.3");
		allNodes.push_back("192.168.0.4");
		allNodes.push_back("192.168.0.5");
		Util::ConfigHelper configHelper;
		configHelper.readFile("DTranx.conf");
		tranxServerInfo = new SharedResources::TranxServerInfo();
		tranxServerInfo->SetAllNodes(allNodes);
		tranxServerInfo->SetConfig(configHelper);
		tranxServerInfo->selfAddress_ = selfAddress;
		tranxServerInfo->SetNodeID(1);
		tranxServerInfo->SetMode(SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER);
		tranxServiceSharedData = new SharedResources::TranxServiceSharedData(1, tranxServerInfo);

		recoverLog = new Server::RecoverLog(tranxServerInfo, tranxServiceSharedData);
		Util::Log::setLogPolicy( { { "Server", "NOTICE" } });
	}
	~RecoverLogTest_BasicInstance() {
		delete recoverLog;
		delete tranxServerInfo;
		delete tranxServiceSharedData;
		Util::FileUtil::RemoveDir("dtranx.db");
		Util::FileUtil::RemoveDir("dtranx.mapdb");
		Util::FileUtil::RemovePrefix("./", "dtranx.log");
		Util::FileUtil::RemovePrefix("./", "gcdtranx.log");
	}

	SharedResources::TranxServerInfo* tranxServerInfo;
	SharedResources::TranxServiceSharedData* tranxServiceSharedData;
	Server::RecoverLog *recoverLog;

	RecoverLogTest_BasicInstance(const RecoverLogTest_BasicInstance&) = delete;
	RecoverLogTest_BasicInstance& operator=(const RecoverLogTest_BasicInstance&) = delete;
};

TEST_F(RecoverLogTest_BasicInstance, RecoverLock) {
	/*
	 * prepare lockTable Stage
	 */
	recoverLog->lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo);
	std::unordered_set<std::string> mapping;
	mapping.insert("192.168.0.1");
	SharedResources::MappingDS *mappingDS = tranxServiceSharedData->getMappingStorage();
	EXPECT_TRUE(mappingDS != NULL);
	mappingDS->UpdateLookupTable("key1", mapping);
	mappingDS->UpdateLookupTable("key2", mapping);

	Stages::Log::GTranxLogRecord logRecord;
	/*
	 * prepare log
	 * 	tranxID: 1#1
	 * 	readset: key1
	 * 	writeset: key2
	 */
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	Service::GItem *item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_1");
	recoverLog->HoldLockFromLog(logRecord);
	Util::TranxID tranxID(1, 2);
	EXPECT_FALSE(
			recoverLog->lockTable->RequestLock("key1", tranxID,
					Stages::Lock::BasicLockTableLockMode::Exclusive));
	EXPECT_TRUE(recoverLog->lockTable->_CancelRequest("key1", tranxID));
	recoverLog->ReleaseLockFromLog(logRecord);
	/*
	 * ready log
	 * 	tranxID: 1#2
	 * 	readset: key1
	 * 	writeset: key2
	 */
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(2);
	logRecord.set_logtype(Stages::Log::GLogType::READY);
	item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_1");
	recoverLog->HoldLockFromLog(logRecord);
	int threadID = recoverLog->lockTable->GetThreadByKey("key2");
	EXPECT_EQ(Stages::Lock::BasicLockTableLockMode::Exclusive,
			recoverLog->lockTable->lockTable[threadID]["key2"]->mode);
	delete recoverLog->lockTable;
}

TEST_F(RecoverLogTest_BasicInstance, RecoverLock_Snapshot) {
	/*
	 * prepare lockTable Stage
	 */
	recoverLog->lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo);
	Stages::Log::GTranxLogRecord logRecord;
	/*
	 * prepare log
	 * 	tranxID: 1#1
	 */
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_tranxtype(Service::GTranxType::SNAPSHOT);
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	recoverLog->HoldLockFromLog(logRecord);
	EXPECT_EQ(Util::TranxID(1, 1), recoverLog->lockTable->epochLock->writeTranx);
	recoverLog->ReleaseLockFromLog(logRecord);
	/*
	 * ready log
	 * 	tranxID: 1#2
	 * 	readset: key1
	 * 	writeset: key2
	 */
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(2);
	logRecord.set_tranxtype(Service::GTranxType::NORMAL);
	logRecord.set_logtype(Stages::Log::GLogType::READY);
	Service::GItem *item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_1");
	recoverLog->HoldLockFromLog(logRecord);
	EXPECT_EQ(Stages::Lock::BasicLockTableLockMode::Shared, recoverLog->lockTable->epochLock->mode);
	delete recoverLog->lockTable;
}

TEST_F(RecoverLogTest_BasicInstance, RecoverLock_Repartition) {
	/*
	 * prepare lockTable Stage
	 */
	recoverLog->lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo);
	Stages::Log::GTranxLogRecord logRecord;
	/*
	 * prepare log
	 * 	tranxID: 1#1
	 */
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_tranxtype(Service::GTranxType::REPARTITION);
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	Service::GMapItem *newMappings = logRecord.add_newmapping();
	newMappings->set_key("key1");
	newMappings->add_ips("192.168.0.1");
	newMappings->add_ips("192.168.0.3");

	recoverLog->HoldLockFromLog(logRecord);
	int threadID = recoverLog->lockTable->GetThreadByKey("key1");
	EXPECT_EQ(Stages::Lock::BasicLockTableLockMode::Exclusive,
			recoverLog->lockTable->lockTable[threadID]["key1"]->mode);
	recoverLog->ReleaseLockFromLog(logRecord);
	/*
	 * ready log
	 * 	tranxID: 1#2
	 * 	readset: key1
	 * 	writeset: key2
	 */
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(2);
	logRecord.set_tranxtype(Service::GTranxType::NORMAL);
	logRecord.set_logtype(Stages::Log::GLogType::READY);
	Service::GItem *item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_1");
	recoverLog->HoldLockFromLog(logRecord);
	EXPECT_EQ(Stages::Lock::BasicLockTableLockMode::Shared,
			recoverLog->lockTable->lockTable[threadID]["key1"]->mode);
	delete recoverLog->lockTable;
}

TEST_F(RecoverLogTest_BasicInstance, ProcessLog_Prep) {
	/*
	 * prepare tranxStates
	 */
	recoverLog->tranxStates = new Stages::TranxStates(tranxServerInfo);
	recoverLog->repartition = new DaemonStages::Repartition(tranxServerInfo,
			tranxServiceSharedData);
	recoverLog->lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo);
	std::unordered_set<std::string> mapping;
	mapping.insert("192.168.0.1");
	SharedResources::MappingDS *mappingDS = tranxServiceSharedData->getMappingStorage();
	EXPECT_TRUE(mappingDS != NULL);
	mappingDS->UpdateLookupTable("key1", mapping);
	mappingDS->UpdateLookupTable("key2", mapping);
	recoverLog->tranxService = new Service::TranxService(tranxServerInfo, tranxServiceSharedData);
	recoverLog->localStorage = new Stages::StringStorage(tranxServerInfo);
	/*
	 * prepare log
	 * 	tranxID: 1#1
	 * 	readset: key1
	 * 	writeset: key2
	 */
	Stages::Log::GTranxLogRecord logRecord;
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_tranxtype(Service::GTranxType::NORMAL);
	Service::GItem *item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_1");
	recoverLog->ProcessLog(logRecord);
	EXPECT_EQ(Stages::Lock::BasicLockTableLockMode::Shared, recoverLog->lockTable->epochLock->mode);
	int threadID = recoverLog->lockTable->GetThreadByKey("key2");
	EXPECT_EQ(Stages::Lock::BasicLockTableLockMode::Exclusive,
			recoverLog->lockTable->lockTable[threadID]["key2"]->mode);
	Util::TranxID tranxID(1, 1);
	EXPECT_TRUE(recoverLog->tranxInDoubt.find(tranxID) != recoverLog->tranxInDoubt.end());
	delete recoverLog->tranxStates;
	delete recoverLog->repartition;
	delete recoverLog->lockTable;
	delete recoverLog->tranxService;
	delete recoverLog->localStorage;
}

TEST_F(RecoverLogTest_BasicInstance, ProcessLog_Coordabort) {
	/*
	 * prepare tranxStates
	 */
	recoverLog->tranxStates = new Stages::TranxStates(tranxServerInfo);
	recoverLog->repartition = new DaemonStages::Repartition(tranxServerInfo,
			tranxServiceSharedData);
	recoverLog->lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo);
	std::unordered_set<std::string> mapping;
	mapping.insert("192.168.0.1");
	SharedResources::MappingDS *mappingDS = tranxServiceSharedData->getMappingStorage();
	EXPECT_TRUE(mappingDS != NULL);
	mappingDS->UpdateLookupTable("key1", mapping);
	mappingDS->UpdateLookupTable("key2", mapping);
	recoverLog->tranxService = new Service::TranxService(tranxServerInfo, tranxServiceSharedData);
	recoverLog->localStorage = new Stages::StringStorage(tranxServerInfo);
	/*
	 * prepare log
	 * 	tranxID: 1#1
	 * 	readset: key1
	 * 	writeset: key2
	 */
	Stages::Log::GTranxLogRecord logRecord;
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_tranxtype(Service::GTranxType::NORMAL);
	Service::GItem *item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_1");
	recoverLog->ProcessLog(logRecord);
	/*
	 * coord abort log
	 */
	logRecord.set_logtype(Stages::Log::GLogType::COORD_ABORT);
	recoverLog->ProcessLog(logRecord);
	Util::TranxID tranxID(1, 1);
	EXPECT_EQ(Stages::State::ABORT, recoverLog->tranxStates->CheckState(tranxID));
	EXPECT_TRUE(recoverLog->tranxInDoubt.find(tranxID) == recoverLog->tranxInDoubt.end());
	EXPECT_TRUE(recoverLog->toAckAbort.find(tranxID) != recoverLog->toAckAbort.end());
	EXPECT_TRUE(recoverLog->toAckTranxType.find(tranxID) != recoverLog->toAckTranxType.end());
	EXPECT_TRUE(recoverLog->tranxInDoubt.find(tranxID) == recoverLog->tranxInDoubt.end());

	delete recoverLog->tranxStates;
	delete recoverLog->repartition;
	delete recoverLog->lockTable;
	delete recoverLog->tranxService;
	delete recoverLog->localStorage;
}

TEST_F(RecoverLogTest_BasicInstance, ProcessLog_Commit) {
	/*
	 * prepare tranxStates
	 */
	recoverLog->tranxStates = new Stages::TranxStates(tranxServerInfo);
	recoverLog->repartition = new DaemonStages::Repartition(tranxServerInfo,
			tranxServiceSharedData);
	recoverLog->lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo);
	std::unordered_set<std::string> mapping;
	mapping.insert("192.168.0.1");
	SharedResources::MappingDS *mappingDS = tranxServiceSharedData->getMappingStorage();
	EXPECT_TRUE(mappingDS != NULL);
	mappingDS->UpdateLookupTable("key1", mapping);
	mappingDS->UpdateLookupTable("key2", mapping);
	mappingDS->UpdateLookupTable("key3", mapping);
	recoverLog->tranxService = new Service::TranxService(tranxServerInfo, tranxServiceSharedData);
	recoverLog->localStorage = new Stages::StringStorage(tranxServerInfo);
	/*
	 * prepare log
	 * 	tranxID: 1#1
	 * 	readset: key1
	 * 	writeset: key2
	 */
	Stages::Log::GTranxLogRecord logRecord;
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_tranxtype(Service::GTranxType::NORMAL);
	Service::GItem *item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_1");
	recoverLog->ProcessLog(logRecord);
	/*
	 * commit log
	 */
	logRecord.set_logtype(Stages::Log::GLogType::COMMIT);
	recoverLog->ProcessLog(logRecord);
	Util::TranxID tranxID(1, 1);
	EXPECT_EQ(Stages::State::COMMIT, recoverLog->tranxStates->CheckState(tranxID));
	int threadID = recoverLog->lockTable->GetThreadByKey("key2");
	EXPECT_EQ(0, recoverLog->lockTable->lockTable[threadID]["key2"]->tranx.size());
	EXPECT_EQ("value2_1", recoverLog->localStorage->Read("key2"));
	EXPECT_TRUE(recoverLog->tranxInDoubt.find(tranxID) == recoverLog->tranxInDoubt.end());
	EXPECT_TRUE(recoverLog->toAckCommit.find(tranxID) != recoverLog->toAckCommit.end());
	/*
	 * ready log
	 * 	tranxID: 1#2
	 * 	readset: key1
	 * 	writeset: key2
	 */
	logRecord.set_logtype(Stages::Log::GLogType::READY);
	logRecord.clear_write_set();
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_2");
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(2);
	recoverLog->ProcessLog(logRecord);
	/*
	 * commit log
	 */
	logRecord.set_logtype(Stages::Log::GLogType::COMMIT);
	recoverLog->ProcessLog(logRecord);
	tranxID.SetTranxID(2);
	EXPECT_EQ(Stages::State::COMMIT, recoverLog->tranxStates->CheckState(tranxID));
	EXPECT_EQ(0, recoverLog->lockTable->lockTable[threadID]["key2"]->tranx.size());
	EXPECT_EQ("value2_2", recoverLog->localStorage->Read("key2"));
	/*
	 * 1PC commit log
	 */
	logRecord.set_logtype(Stages::Log::GLogType::COMMIT_1PC);
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(3);
	logRecord.set_tranxtype(Service::GTranxType::NORMAL);
	logRecord.clear_read_set();
	item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	logRecord.clear_write_set();
	item = logRecord.add_write_set();
	item->set_key("key3");
	item->set_value("value3_1");
	recoverLog->ProcessLog(logRecord);
	tranxID.SetTranxID(3);
	EXPECT_EQ(Stages::State::GC, recoverLog->tranxStates->CheckState(tranxID));
	EXPECT_EQ("value3_1", recoverLog->localStorage->Read("key3"));
	delete recoverLog->tranxStates;
	delete recoverLog->repartition;
	delete recoverLog->lockTable;
	delete recoverLog->tranxService;
	delete recoverLog->localStorage;
}

TEST_F(RecoverLogTest_BasicInstance, ProcessLog_partabort) {
	/*
	 * prepare tranxStates
	 */
	recoverLog->tranxStates = new Stages::TranxStates(tranxServerInfo);
	recoverLog->repartition = new DaemonStages::Repartition(tranxServerInfo,
			tranxServiceSharedData);
	recoverLog->lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo);
	std::unordered_set<std::string> mapping;
	mapping.insert("192.168.0.1");
	SharedResources::MappingDS *mappingDS = tranxServiceSharedData->getMappingStorage();
	EXPECT_TRUE(mappingDS != NULL);
	mappingDS->UpdateLookupTable("key1", mapping);
	mappingDS->UpdateLookupTable("key2", mapping);
	mappingDS->UpdateLookupTable("key3", mapping);
	recoverLog->tranxService = new Service::TranxService(tranxServerInfo, tranxServiceSharedData);
	recoverLog->localStorage = new Stages::StringStorage(tranxServerInfo);
	/*
	 * prepare log
	 * 	tranxID: 1#1
	 * 	readset: key1
	 * 	writeset: key2
	 */
	Stages::Log::GTranxLogRecord logRecord;
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_tranxtype(Service::GTranxType::NORMAL);
	Service::GItem *item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_1");
	recoverLog->ProcessLog(logRecord);
	/*
	 * commit log
	 */
	logRecord.set_logtype(Stages::Log::GLogType::COMMIT);
	recoverLog->ProcessLog(logRecord);
	Util::TranxID tranxID(1, 1);
	EXPECT_EQ(Stages::State::COMMIT, recoverLog->tranxStates->CheckState(tranxID));
	int threadID = recoverLog->lockTable->GetThreadByKey("key2");
	EXPECT_EQ(0, recoverLog->lockTable->lockTable[threadID]["key2"]->tranx.size());
	EXPECT_EQ("value2_1", recoverLog->localStorage->Read("key2"));
	/*
	 * ready log
	 * 	tranxID: 1#2
	 * 	readset: key1
	 * 	writeset: key2
	 */
	logRecord.set_logtype(Stages::Log::GLogType::READY);
	logRecord.clear_write_set();
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_2");
	logRecord.mutable_tranxid()->set_nodeid(2);
	logRecord.mutable_tranxid()->set_tranxid(3);
	recoverLog->ProcessLog(logRecord);
	/*
	 * abort log
	 */
	logRecord.set_logtype(Stages::Log::GLogType::PART_ABORT);
	recoverLog->ProcessLog(logRecord);
	tranxID.SetNodeID(2);
	tranxID.SetTranxID(3);
	EXPECT_EQ(Stages::State::ABORT, recoverLog->tranxStates->CheckState(tranxID));
	EXPECT_EQ(0, recoverLog->lockTable->lockTable[threadID]["key2"]->tranx.size());
	EXPECT_EQ("value2_1", recoverLog->localStorage->Read("key2"));
	delete recoverLog->tranxStates;
	delete recoverLog->repartition;
	delete recoverLog->lockTable;
	delete recoverLog->tranxService;
	delete recoverLog->localStorage;
}

TEST_F(RecoverLogTest_BasicInstance, ProcessLog_ack) {
	/*
	 * prepare tranxStates
	 */
	recoverLog->tranxStates = new Stages::TranxStates(tranxServerInfo);
	recoverLog->repartition = new DaemonStages::Repartition(tranxServerInfo,
			tranxServiceSharedData);
	recoverLog->lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo);
	std::unordered_set<std::string> mapping;
	mapping.insert("192.168.0.1");
	SharedResources::MappingDS *mappingDS = tranxServiceSharedData->getMappingStorage();
	EXPECT_TRUE(mappingDS != NULL);
	mappingDS->UpdateLookupTable("key1", mapping);
	mappingDS->UpdateLookupTable("key2", mapping);
	recoverLog->tranxService = new Service::TranxService(tranxServerInfo, tranxServiceSharedData);
	recoverLog->localStorage = new Stages::StringStorage(tranxServerInfo);
	/*
	 * prepare log
	 * 	tranxID: 1#1
	 * 	readset: key1
	 * 	writeset: key2
	 */
	Stages::Log::GTranxLogRecord logRecord;
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_tranxtype(Service::GTranxType::NORMAL);
	Service::GItem *item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_1");
	recoverLog->ProcessLog(logRecord);
	/*
	 * commit log
	 */
	logRecord.set_logtype(Stages::Log::GLogType::COMMIT);
	recoverLog->ProcessLog(logRecord);
	Util::TranxID tranxID(1, 1);
	EXPECT_EQ(Stages::State::COMMIT, recoverLog->tranxStates->CheckState(tranxID));
	int threadID = recoverLog->lockTable->GetThreadByKey("key2");
	EXPECT_EQ(0, recoverLog->lockTable->lockTable[threadID]["key2"]->tranx.size());
	EXPECT_EQ("value2_1", recoverLog->localStorage->Read("key2"));
	/*
	 * ack log
	 */
	logRecord.set_logtype(Stages::Log::GLogType::ACK);
	recoverLog->ProcessLog(logRecord);
	EXPECT_EQ(Stages::State::GC, recoverLog->tranxStates->CheckState(tranxID));
	delete recoverLog->tranxStates;
	delete recoverLog->repartition;
	delete recoverLog->lockTable;
	delete recoverLog->tranxService;
	delete recoverLog->localStorage;
}

TEST_F(RecoverLogTest_BasicInstance, Recover_completetranx) {
	/*
	 * prepare tranxStates
	 */
	recoverLog->tranxStates = new Stages::TranxStates(tranxServerInfo);
	recoverLog->repartition = new DaemonStages::Repartition(tranxServerInfo,
			tranxServiceSharedData);
	recoverLog->lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo);
	std::unordered_set<std::string> mapping;
	mapping.insert("192.168.0.1");
	SharedResources::MappingDS *mappingDS = tranxServiceSharedData->getMappingStorage();
	EXPECT_TRUE(mappingDS != NULL);
	mappingDS->UpdateLookupTable("key1", mapping);
	mappingDS->UpdateLookupTable("key2", mapping);
	recoverLog->tranxService = new Service::TranxService(tranxServerInfo, tranxServiceSharedData);
	recoverLog->localStorage = new Stages::StringStorage(tranxServerInfo);
	std::string logFullName = Util::FileUtil::CombineName(
			tranxServerInfo->GetConfig().read("PersistLogDir"),
			tranxServerInfo->GetConfig().read("LogFileName"));
	recoverLog->tranxLog = new Stages::Log::TranxLog(logFullName, tranxServerInfo);

	Stages::Log::TranxLog *tmpLog = new Stages::Log::TranxLog(logFullName, tranxServerInfo);
	/*
	 * tranxLog
	 */
	Stages::Log::GTranxLogRecord logRecord;
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	Service::GItem *item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_1");
	std::vector<std::string> allNodes;
	allNodes.push_back("192.168.0.1");
	Util::TranxID tranxID(1, 1);
	tmpLog->Prepare(tranxID, logRecord.read_set(), logRecord.write_set(), allNodes);
	tmpLog->Commit(tranxID);
	tmpLog->CoordinatorAck(tranxID);
	tmpLog->CloseWrite();
	delete tmpLog;
	recoverLog->Recover();
	EXPECT_EQ("value2_1", recoverLog->localStorage->Read("key2"));
	delete recoverLog->tranxStates;
	delete recoverLog->repartition;
	delete recoverLog->lockTable;
	delete recoverLog->tranxService;
	delete recoverLog->localStorage;
	delete recoverLog->tranxLog;
}

TEST_F(RecoverLogTest_BasicInstance, Recover_partialtranx) {
	/*
	 * prepare tranxStates
	 */
	recoverLog->tranxStates = new Stages::TranxStates(tranxServerInfo);
	recoverLog->repartition = new DaemonStages::Repartition(tranxServerInfo,
			tranxServiceSharedData);
	recoverLog->lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo);
	std::unordered_set<std::string> mapping;
	mapping.insert("192.168.0.1");
	SharedResources::MappingDS *mappingDS = tranxServiceSharedData->getMappingStorage();
	EXPECT_TRUE(mappingDS != NULL);
	mappingDS->UpdateLookupTable("key1", mapping);
	mappingDS->UpdateLookupTable("key2", mapping);
	recoverLog->tranxService = new Service::TranxService(tranxServerInfo, tranxServiceSharedData);
	recoverLog->localStorage = new Stages::StringStorage(tranxServerInfo);
	std::string logFullName = Util::FileUtil::CombineName(
			tranxServerInfo->GetConfig().read("PersistLogDir"),
			tranxServerInfo->GetConfig().read("LogFileName"));
	recoverLog->tranxLog = new Stages::Log::TranxLog(logFullName, tranxServerInfo);

	Stages::Log::TranxLog *tmpLog = new Stages::Log::TranxLog(logFullName, tranxServerInfo);
	/*
	 * tranxLog
	 */
	Stages::Log::GTranxLogRecord logRecord;
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_logtype(Stages::Log::GLogType::READY);
	Service::GItem *item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_1");
	Util::TranxID tranxID(1, 1);
	tmpLog->Ready(tranxID, logRecord.read_set(), logRecord.write_set(), logRecord.ips());
	tmpLog->CloseWrite();
	delete tmpLog;
	recoverLog->Recover();
	tranxID.SetNodeID(2);
	EXPECT_FALSE(
			recoverLog->lockTable->RequestLock("key2", tranxID,
					Stages::Lock::BasicLockTableLockMode::Shared));
	std::unordered_map<Util::TranxID, SharedResources::TranxServiceSharedData::TranxItems*,
			Util::KeyHasher>& curTranx = tranxServiceSharedData->GetCurTranxReference();
	tranxID.SetNodeID(1);
	EXPECT_TRUE(curTranx.find(tranxID) != curTranx.end());
	EXPECT_EQ(1, curTranx[tranxID]->GetWriteSize());
	EXPECT_EQ("key2", curTranx[tranxID]->writeItems[0].key());
	delete recoverLog->tranxStates;
	delete recoverLog->repartition;
	delete recoverLog->lockTable;
	delete recoverLog->tranxService;
	delete recoverLog->localStorage;
	delete recoverLog->tranxLog;
}

TEST_F(RecoverLogTest_BasicInstance, PostRecovery) {
	/*
	 * prepare tranxStates
	 */
	recoverLog->tranxStates = new Stages::TranxStates(tranxServerInfo);
	recoverLog->repartition = new DaemonStages::Repartition(tranxServerInfo,
			tranxServiceSharedData);
	recoverLog->lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo);
	std::unordered_set<std::string> mapping;
	mapping.insert("192.168.0.1");
	SharedResources::MappingDS *mappingDS = tranxServiceSharedData->getMappingStorage();
	EXPECT_TRUE(mappingDS != NULL);
	mappingDS->UpdateLookupTable("key1", mapping);
	mappingDS->UpdateLookupTable("key2", mapping);
	recoverLog->tranxService = new Service::TranxService(tranxServerInfo, tranxServiceSharedData);
	recoverLog->localStorage = new Stages::StringStorage(tranxServerInfo);

	recoverLog->tranxAck = new DaemonStages::TranxAckMock(tranxServerInfo, tranxServiceSharedData);
	/*
	 * prepare log
	 * 	tranxID: 1#1
	 * 	readset: key1
	 * 	writeset: key2
	 */
	Stages::Log::GTranxLogRecord logRecord;
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_tranxtype(Service::GTranxType::NORMAL);
	Service::GItem *item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2_1");
	recoverLog->ProcessLog(logRecord);
	/*
	 * coord abort log
	 */
	logRecord.set_logtype(Stages::Log::GLogType::COORD_ABORT);
	recoverLog->ProcessLog(logRecord);
	Util::TranxID tranxID(1, 1);
	EXPECT_EQ(Stages::State::ABORT, recoverLog->tranxStates->CheckState(tranxID));
	EXPECT_TRUE(recoverLog->tranxInDoubt.empty());
	EXPECT_TRUE(recoverLog->toAckAbort.find(tranxID) != recoverLog->toAckAbort.end());
	EXPECT_TRUE(recoverLog->toAckTranxType.find(tranxID) != recoverLog->toAckTranxType.end());
	EXPECT_TRUE(recoverLog->tranxInDoubt.find(tranxID) == recoverLog->tranxInDoubt.end());

	recoverLog->StartService();
	if (recoverLog->daemonThread.joinable()) {
		recoverLog->daemonThread.join();
	}
	EXPECT_TRUE(recoverLog->toAckAbort.find(tranxID) == recoverLog->toAckAbort.end());

	delete recoverLog->tranxStates;
	delete recoverLog->repartition;
	delete recoverLog->lockTable;
	delete recoverLog->tranxService;
	delete recoverLog->localStorage;
}


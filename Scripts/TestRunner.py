#!/usr/bin/env python
#
# Running ycsb test in clients
from Utility.SSHHelper import *
from Utility.ConfigHelper import *
from Utility.BashHelper import *
import argparse
import threading
import time

NUMTHREAD_PER_CLIENT = 10
def WaitForThreads(threads):
	while True:
		deleted_threads = []
		for thread in threads:
			if thread.isAlive() == False:
				thread.join()
				deleted_threads.append(thread)
		for thread in deleted_threads:
			threads.remove(thread)
		if len(threads) == 0:
			break;
		time.sleep(5)

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()
runtime_parser = subparsers.add_parser('run', help='run perf test clients')
runtime_parser.add_argument("-c","--client",dest="client",help="client ip list", required = True)
runtime_parser.add_argument("-s","--server",dest="servernum",help="number of servers", required = True)
runtime_parser.add_argument("-i","--clientindex",dest="clientIndex",help="client index, used to generate client LAN self address", required = True)
runtime_parser.add_argument("-w","--workload",dest="workload",help="workload file path", required = True)
runtime_parser.add_argument("-db","--database",dest="db",help="database: dtranx, hyperdex etc", required = True)
runtime_parser.add_argument("-t", "--thread",dest="thread", help="thread num for each client", required = True)
runtime_parser.add_argument("-o", "--operation",dest="operationnum", help="tranx num for each client", required = True)
runtime_parser.add_argument("-s1", dest="stablestart", help="stable start num", required = False)
runtime_parser.add_argument("-s2", dest="stableend", help="stable end num", required = False)
runtime_parser.set_defaults(subcommand = 'runtime')

args = parser.parse_args()

NUMTHREAD_PER_CLIENT = args.thread

# initialize clientips
clientips = []
f = open(args.client,'r')
for line in f:
	clientips.append(line.strip())

# prepare clients with ips and workload
# handle relative/absolute path
baseIP = "192.168.0."
index = 0
localipsFile = open('localips', 'w')
try:
	clusterSize = int(args.servernum)
except:
	print "input cluster size as the -s option to generate ips for benchmark tests"
	exit()
for i in range(clusterSize):
	index = index + 1
	localipsFile.write(baseIP + str(index) + '\n')
localipsFile.close();

if args.workload.startswith('/'):
	workloadRealPath = args.workload
else:
	workingPath = GetWorkingDir()
	workloadRealPath = workingPath + args.workload
print workloadRealPath

AdjustConfig('operationcount',args.operationnum, workloadRealPath)

for ip in clientips:
	scp(True, './localips', RemoteUser, ip, RemoteHomeDir + "ips")
	scp(True, workloadRealPath, RemoteUser, ip, RemoteHomeDir + "workload.spec")
runBash("rm localips");

threads = []
baseIP = "192.168.0."
index = int(args.clientIndex)
for ip in clientips:
	print ip
	if args.stablestart is not None and args.stableend is not None:
		clientBash = "\
		./ycsbc -threads " + str(NUMTHREAD_PER_CLIENT) + " -db " + args.db + " -i 0 -P workload.spec -C ips -s " + baseIP + str(index) + " -s1 " + args.stablestart +" -s2 " + args.stableend
	else:
		clientBash = "\
		./ycsbc -threads " + str(NUMTHREAD_PER_CLIENT) + " -db " + args.db + " -i 0 -P workload.spec -C ips -s " + baseIP + str(index)
	index = index + 1
	thread = threading.Thread(target = runSSHCommandWithOutput, args = (ip, RemoteUser, clientBash, RemoteHomeDir + "test.output"))
	thread.start()
	threads.append(thread)
	time.sleep(1/100.0)
WaitForThreads(threads)

for ip in clientips:
	 scp(False, workingPath + "test.output." + ip, RemoteUser, ip, RemoteHomeDir + "test.output")

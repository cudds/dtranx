/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "GarbageCollector.h"
#include "Util/FileUtil.h"

#include "Service/TranxService.h"
#include "Service/StorageService.h"
#include "Service/ClientService.h"
#include "TranxAck.h"
#include "Server/RecoverLog.h"
#include "Repartition.h"
#include "Stages/TranxStateMock.h"
#include "Stages/Log/TranxLog.h"

using namespace DTranx;

class GarbageCollector_Stage: public ::testing::Test {
public:
	GarbageCollector_Stage() {
		Util::ConfigHelper configHelper;
		configHelper.readFile("DTranx.conf");
		tranxServerInfo = new SharedResources::TranxServerInfo();
		tranxServerInfo->SetConfig(configHelper);
		std::vector<std::string> ips;
		ips.push_back("192.168.0.1");
		ips.push_back("192.168.0.2");
		ips.push_back("192.168.0.3");
		tranxServerInfo->SetAllNodes(ips);
		tranxServerInfo->SetNodeID(2);
		tranxServerInfo->SetMode(SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER);
		tranxServiceSharedData = new SharedResources::TranxServiceSharedData(2, tranxServerInfo);
		gc = new DaemonStages::GarbageCollector(tranxServerInfo, tranxServiceSharedData);
	}
	~GarbageCollector_Stage() {
		delete tranxServerInfo;
		delete tranxServiceSharedData;
		delete gc;
		Util::FileUtil::RemoveDir("dtranx.mapdb");
		Util::FileUtil::RemovePrefix("./", "dtranx.log");
		Util::FileUtil::RemovePrefix("./", "gcdtranx.log");
		Util::FileUtil::RemovePrefix("./", ".dtranx.log");
	}

	SharedResources::TranxServerInfo *tranxServerInfo;
	SharedResources::TranxServiceSharedData *tranxServiceSharedData;
	DaemonStages::GarbageCollector *gc;
private:
	GarbageCollector_Stage(const GarbageCollector_Stage&) = delete;
	GarbageCollector_Stage& operator=(const GarbageCollector_Stage&) = delete;
};

void ProcessStates(Stages::TranxServiceQueueElement *element) {
	EXPECT_EQ(Stages::TranxStatesStageTask::_GarbageCollectAck, element->getStateTask());
	element->setStateToBroadcast(false);
	Stages::GCFactory gcFactory(2, 100, 100);
	gcFactory.UpdateEarliest(1, 15);
	gcFactory.UpdateEarliest(2, 16);
	gcFactory.UpdateEarliest(3, 17);
	element->setStateGcFactory(gcFactory);
}

TEST_F(GarbageCollector_Stage, gcLog) {
	DTranx::Util::Log::setLogPolicy( { { "DaemonStages", "VERBOSE" } ,{ "Stages", "VERBOSE" } });
	Stages::TranxStateMock tranxStatesMock(tranxServerInfo);
	tranxStatesMock.SetProcessFunc(ProcessStates);
	DaemonStages::Repartition repartition(tranxServerInfo, tranxServiceSharedData);
	gc->repartition = &repartition;
	gc->tranxStates = &tranxStatesMock;
	tranxStatesMock.gc = gc;

	/*
	 * prepare existing logs
	 */
	Stages::Log::TranxLog *tranxLog = new Stages::Log::TranxLog("./dtranx.log", tranxServerInfo);
	Stages::Log::GTranxLogRecord logRecord;
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	Service::GItem *item = logRecord.add_read_set();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	item = logRecord.add_write_set();
	item->set_key("key2");
	item->set_value("value2");
	std::vector<std::string> ips;
	ips.push_back("192.168.0.1");
	ips.push_back("192.168.0.2");
	Util::TranxID tranxID(1, 1);
	tranxLog->Prepare(tranxID, logRecord.read_set(), logRecord.write_set(), ips);
	tranxLog->CloseWrite();
	tranxID.SetNodeID(2);
	tranxLog->Commit(tranxID, Service::GTranxType::NORMAL, Stages::Log::GLogType::COMMIT_1PC);
	tranxLog->CloseWrite();
	delete tranxLog;

	gc->StartService();
	sleep(5);
	/*
	 * check if there is a .dtranx.log*
	 */
	std::set<std::string> files = DTranx::Util::FileUtil::LsPrefix(
			tranxServerInfo->GetConfig().read("PersistLogDir"),
			"." + tranxServerInfo->GetConfig().read("LogFileName"));
	EXPECT_EQ(1, files.size());
	gc->ShutService();
	DTranx::Util::Log::setLogPolicy( { { "DaemonStages", "NOTICE" } ,{ "Stages", "NOTICE" }  });
}


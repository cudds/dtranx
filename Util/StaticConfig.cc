/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "StaticConfig.h"

namespace DTranx {
namespace Util {

StaticConfig* StaticConfig::instance = NULL;
std::mutex StaticConfig::mutex;

void StaticConfig::init() {
	/*
	 * RPC related
	 */
	keyValues["backendReceiver"] = "dtranx_req";

	/*
	 * OCC backoff related
	 * in milliseconds
	 */
	keyValues["epochMaxRetries"] = "10";
	keyValues["epochMaxBackoff"] = "2000";
	keyValues["epochInitalBackoff"] = "10";

	/*
	 * Timing
	 * AckPoll is used in ack thread waiting(in microseconds)
	 * ClientPollPeriod is used in client thread waiting to poll, snapshot check timeout(in milliseconds)
	 * RPCTimeout is used to resend RPC again in both tranxservice and clientRPC because
	 * 		even zeromq guanrantees reliability, we still need to handle node failure(in milliseconds)
	 * LockTimeout is used to abort lock request when write lock is not granted(in nanoseconds)
	 * SnapshotPeriod is used to wait before the next snapshot is created.(in seconds)
	 *
	 */
	keyValues["AckPoll"] = "10";
	keyValues["ClientPollPeriod"] = "1000";
	keyValues["RPCTimeout"] = "5000";
	keyValues["LockTimeout"] = "500000";
	keyValues["SnapshotPeriod"] = "1000000";

	/*
	 * Initial values
	 * FirstID is first messageID for clients and first tranxID for tranx services
	 * FirstEpoch is first epoch number for the snapshot reading transactions
	 */
	keyValues["FirstID"] = "1";
	keyValues["FirstEpoch"] = "1";
}

} /* namespace Util */
} /* namespace DTranx */


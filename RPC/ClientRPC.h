/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * ClientRPC is the client side of the RPC call.
 * It constructs the request header with serviceID and opcode.
 * It mainly takes care of client service and storage service.
 *
 * Note: use the response pointer after poll success because it will be freed and reassigned
 */

#ifndef DTRANX_RPC_CLIENTRPC_H_
#define DTRANX_RPC_CLIENTRPC_H_

#include "BaseRPC.h"
#include "DTranx/RPC/ZeromqSender.h"

namespace DTranx {
namespace Client {
class Client;
}
;
namespace RPC {

class ClientRPC: public BaseRPC {
public:
	explicit ClientRPC(GServiceID serviceType, Client::Client *client);
	ClientRPC(ClientRPC&& other);
	ClientRPC& operator=(ClientRPC&& other);
	virtual ~ClientRPC() noexcept(false);

	/*
	 * opcode access methods
	 */

	void SetClientOpcode(Service::GOpCode opcode) {
		assert(requestMessage != NULL);
		return requestMessage->mutable_clientservicerpc()->set_opcode(opcode);
	}

	void SetStorageOpcode(Service::GOpCode opcode) {
		assert(requestMessage != NULL);
		requestMessage->mutable_storageservicerpc()->set_opcode(opcode);
	}

	/*
	 * specific service request/response access methods
	 */
	Service::GClientServiceRPC* GetClientRequest() {
		assert(requestMessage != NULL);
		return requestMessage->mutable_clientservicerpc();
	}

	Service::GStorageServiceRPC* GetStorageRequest() {
		assert(requestMessage != NULL);
		return requestMessage->mutable_storageservicerpc();
	}

	Service::GClientServiceRPC* GetClientResponse() {
		assert(responseMessage != NULL);
		return responseMessage->mutable_clientservicerpc();
	}

	Service::GStorageServiceRPC* GetStorageResponse() {
		assert(responseMessage != NULL);
		return responseMessage->mutable_storageservicerpc();
	}

private:
	Client::Client *client;
	/*
	 * ClientRPC is non-copyable.
	 */
	ClientRPC(const ClientRPC&) = delete;
	ClientRPC& operator=(const ClientRPC&) = delete;
};

} /* namespace RPC */
} /* namespace DTranx */

#endif /* DTRANX_RPC_CLIENTRPC_H_ */

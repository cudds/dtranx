/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "TranxServiceSharedData.h"

namespace DTranx {
namespace SharedResources {

TranxServiceSharedData::TranxServiceSharedData(int nodeID, TranxServerInfo *tranxServerInfo)
		: nodeID(nodeID), mutexCurTranx(), curTranxs(), snapshotTranx(), mutexSnapshotTranx(),
				nextTranxID(
						std::strtoull(Util::StaticConfig::GetInstance()->Read("FirstID").c_str(),
						NULL, 10)), threadSafeCurTranx(false) {
	mappingStorage = new SharedResources::MappingDS(tranxServerInfo->GetAllNodes(),
			tranxServerInfo->GetConfig().read("MapdbFileName"), MappingDS::StorageOption::STRING);
	tranxHistory = new SharedResources::TranxHistory(
			tranxServerInfo->GetConfig().read<uint64>("PartitionBroadcastThreshold"),
			tranxServerInfo->GetConfig().read<uint64>("RePartitionThreshold"),
			tranxServerInfo->GetConfig().read<uint64>("TranxHistoryLimit"));
	for (int i = 0; i < 5000; i++) {
		TranxItems *curTranx = new TranxItems();
		assert(freeList.enqueue(curTranx));
	}
	curTranxs.reserve(5000);
}
TranxServiceSharedData::~TranxServiceSharedData() {
	delete mappingStorage;
	delete tranxHistory;
}

} /* namespace SharedResources */
} /* namespace DTranx */

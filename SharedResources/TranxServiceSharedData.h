/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * TranxServiceSharedData wraps all the data that are shared across stages
 * data are protected by mutex/locks.
 *
 * Exception: only RecoverLog::Recover can access the reference without any locks
 * And these shared data will be initialized in RecoverLog::Recover
 *
 * ThreadSafe
 */

#ifndef DTRANX_SHAREDRESOURCES_TRANXSERVICESHAREDDATA_H_
#define DTRANX_SHAREDRESOURCES_TRANXSERVICESHAREDDATA_H_

#include <unordered_map>
#include <unordered_set>
#include <vector>
#include "DTranx/Service/Transaction.pb.h"
#include "DTranx/Util/Log.h"
#include "TranxServerInfo.h"
#include "MappingDS.h"
#include "TranxHistory.h"
#include "RPC/TranxRPC.h"
#include "DTranx/Util/ConcurrentQueue.h"
#include "Util/TranxID.h"

namespace DTranx {

namespace SharedResources {

class TranxServiceSharedData {
public:
	TranxServiceSharedData(int nodeID, TranxServerInfo *tranxServerInfo);
	virtual ~TranxServiceSharedData();

	struct TranxItems {
		std::vector<Service::GItem> readItems;
		std::vector<Service::GItem> writeItems;
		void Clear() {
			readItems.clear();
			writeItems.clear();
		}
		uint32_t GetReadSize() {
			return readItems.size();
		}
		uint32_t GetWriteSize() {
			return writeItems.size();
		}
		TranxItems() {
			readItems.reserve(20);
			writeItems.reserve(20);
		}

	};

	TranxItems* NewCurTranx() {
		TranxItems *curTranx;
		if (freeList.try_dequeue(curTranx)) {
			curTranx->Clear();
		} else {
			curTranx = new TranxItems();
		}
		return curTranx;
	}

	void Reclaim(TranxItems* curTranx) {
		assert(curTranx != NULL);
		assert(freeList.enqueue(curTranx));
	}

	void AddCurTranx(const Util::TranxID& tranxID, TranxItems* tranx) {
		if (!isThreadSafeCurTranx()) {
			mutexCurTranx.lock();
		}
		curTranxs[tranxID] = tranx;
		if (!isThreadSafeCurTranx()) {
			mutexCurTranx.unlock();
		}
	}
	void DeleteCurTranx(const Util::TranxID& tranxID) {
		if (!isThreadSafeCurTranx()) {
			mutexCurTranx.lock();
		}
		if ((curTranxs.find(tranxID) != curTranxs.end())) {
			assert(freeList.enqueue(curTranxs[tranxID]));
		}
		curTranxs.erase(tranxID);
		if (!isThreadSafeCurTranx()) {
			mutexCurTranx.unlock();
		}
	}
	TranxItems* GetAndDeleteCurTranx(const Util::TranxID& tranxID) {
		if (!isThreadSafeCurTranx()) {
			mutexCurTranx.lock();
		}
		TranxItems* tmpCurTranx = curTranxs[tranxID];
		curTranxs.erase(tranxID);
		if (!isThreadSafeCurTranx()) {
			mutexCurTranx.unlock();
		}
		return tmpCurTranx;
	}
	TranxItems* GetCurTranx(const Util::TranxID& tranxID) {
		if (!isThreadSafeCurTranx()) {
			mutexCurTranx.lock();
		}
		TranxItems* tmpCurTranx = curTranxs[tranxID];
		if (tmpCurTranx == NULL) {
			/*
			 * avoid adding an empty entry
			 */
			curTranxs.erase(tranxID);
		}
		if (!isThreadSafeCurTranx()) {
			mutexCurTranx.unlock();
		}
		return tmpCurTranx;
	}

	std::unordered_map<Util::TranxID, TranxItems*, Util::KeyHasher>&GetCurTranxReference() {
		return curTranxs;
	}

	SharedResources::MappingDS *getMappingStorage() {
		return mappingStorage;
	}

	void setMappingStorage(SharedResources::MappingDS* mappingStorage) {
		this->mappingStorage = mappingStorage;
	}

	Util::TranxID GetSnapshotTranx() {
		boost::unique_lock<boost::mutex> lockGuard(mutexSnapshotTranx);
		return snapshotTranx;
	}
	void SetSnapshotTranx(const Util::TranxID& snapshotTranx) {
		boost::unique_lock<boost::mutex> lockGuard(mutexSnapshotTranx);
		this->snapshotTranx = snapshotTranx;
	}

	/*
	 * Used by Tranx Service broadcast request
	 */
	void AddTranxHistory(Service::GTranxPartition &request) {
		tranxHistory->ExtractFromMessage(&request);
	}

	/*
	 * Used by Repartition Thread
	 */
	void AddTranxHistory(std::unordered_set<std::string>& readset,
			std::unordered_set<std::string>& writeset) {
		tranxHistory->AddTranx(readset, writeset);
	}

	RPC::TranxRPC* TranxHistoryBroadcast() {
		RPC::TranxRPC* tranxRPC = new RPC::TranxRPC(Service::GOpCode::TRANX_PARTITION,
				RPC::GServiceID::TranxService, 0, false);
		if (tranxHistory->ToBroadcast(tranxRPC->GetTranxRequest()->mutable_tranxpartition())) {
			return tranxRPC;
		}
		delete tranxRPC;
		return NULL;
	}

	std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>>TranxHistoryRepartition() {
		return tranxHistory->ToRePartition();
	}

	std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>>TranxHistoryForceRepartition() {
		return tranxHistory->ForceRePartition();
	}

	void SetNextTranxID(uint64 nextTranxID) {
		this->nextTranxID.store(nextTranxID);
	}

	uint64 GetNextTranxID() {
		return nextTranxID.load(std::memory_order_relaxed);
	}

	Util::TranxID GenerateTranxID() {
		return Util::TranxID(nodeID, nextTranxID++);
	}

	bool isThreadSafeCurTranx() const
	{
		return threadSafeCurTranx;
	}

	void setThreadSafeCurTranx(bool threadSafeCurTranx)
	{
		this->threadSafeCurTranx = threadSafeCurTranx;
	}

private:
	/*
	 * curTranxs: current committing transactions for participant
	 * 	participant use this to write logs and commit
	 *
	 * 	only RecoverLog::Recover can access curTranx without locks
	 *
	 * 	threadSafeCurTranx is used to reduce mutex overhead when only one thread is accessing curTranx
	 * 		which means there's only one tranxservice internal thread
	 */
	std::unordered_map<Util::TranxID, TranxItems*, Util::KeyHasher>curTranxs;
	boost::mutex mutexCurTranx;
	bool threadSafeCurTranx;
	moodycamel::ConcurrentQueue<TranxItems*> freeList;
	/*
	 * mappingStorage are thread safe
	 */
	MappingDS *mappingStorage;
	/*
	 * snapshotTranx is used to avoid concurrent snapshot creation
	 */
	Util::TranxID snapshotTranx;
	boost::mutex mutexSnapshotTranx;

	/*
	 * tranxHistory is used by Transaction partition broadcast service request and partitionthread
	 *  lock is not needed in slave because slave nodes only add and broadcast in partitionthread and
	 *  master node might get tranxhistory from slaves in broadcast service request and clear in PartitionThread
	 *  note: tranxHistory only store transactions in coordinators.
	 *
	 *  lock is embedded in tranxHistory
	 */
	TranxHistory *tranxHistory;

	/*
	 * nextTranxID is used to generate transaction ID's for new transactions
	 * nodeID is used by the new tranxID generation
	 */
	std::atomic_ulong nextTranxID;
	int nodeID;
};

}
/* namespace SharedResources */
} /* namespace DTranx */

#endif /* DTRANX_SHAREDRESOURCES_TRANXSERVICESHAREDDATA_H_ */

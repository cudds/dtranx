/*
 * Copyright (c) 2011 The LevelDB Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file. See the AUTHORS file for names of contributors.
 *
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_STAGES_LOG_POSIXSEQUENTIALFILE_H_
#define DTRANX_STAGES_LOG_POSIXSEQUENTIALFILE_H_

#include <cerrno>
#include "Util/Slice.h"
#include "SequentialFile.h"

namespace DTranx {
namespace Stages {
namespace Log {

class PosixSequentialFile: public SequentialFile{
public:
	PosixSequentialFile(const std::string& fname, FILE* f)
			: filename_(fname), file_(f) {
	}
	virtual ~PosixSequentialFile() {
		if (file_ != NULL) {
			// Ignoring any potential errors
			fclose(file_);
		}
	}

	virtual LogStatus Read(size_t n, Slice* result, char* scratch);
	virtual LogStatus Skip(uint64_t n);

private:
	std::string filename_;
	FILE* file_;
};

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_LOG_POSIXSEQUENTIALFILE_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * idx_t is for integer quantities(real_t is for floating point quantities)
 */
#include "libpmemlog.h"
#include <iostream>

/* size of the pmemlog pool -- 1GB MB */
#define	POOL_SIZE ((off_t)(1 << 29))

int Print(const void *buf, size_t len, void *arg) {
	std::cout << "address in Print " << const_cast<void*>(buf) << std::endl;
	std::string str((char *) buf, len);
	std::cout << str << std::endl;
	return 1;
}

int main(int argc, char **argv) {
	if (argc < 3) {
		std::cout << "arg1: filename; arg2: read/write" << std::endl;
		return 0;
	}
	/* create the pmemlog pool or open it if it already exists */
	std::string fname = argv[1];
	std::string readOrWrite = argv[2];

	if (readOrWrite == "read") {
		PMEMlogpool *plp = pmemlog_open(fname.c_str());
		if (plp == NULL) {
			std::cout << "error in open log file" << std::endl;
			return 0;
		}

		size_t actual_read = 0;
		while (true) {
			char *hello;
			actual_read = pmemlog_read(plp, 10, &hello);
			Print(hello, actual_read, NULL);
			if (actual_read < 10) {
				break;
			}
		}
		if (plp) {
			pmemlog_close(plp);
		}
	} else if (readOrWrite == "write") {
		PMEMlogpool *plp = pmemlog_create(fname.c_str(), POOL_SIZE, 0666);

		if (plp == NULL)
			plp = pmemlog_open(fname.c_str());

		if (plp == NULL) {
			std::cout << "error in open/create log file" << std::endl;
			return 0;
		}

		std::string newLog = "new log entry1";
		if (pmemlog_append(plp, newLog.c_str(), newLog.size()) < 0) {
			std::cout << "error in log append" << std::endl;
		}
		newLog = "new log entry2";
		if (pmemlog_append(plp, newLog.c_str(), newLog.size()) < 0) {
			std::cout << "error in log append" << std::endl;
		}
		if (plp) {
			pmemlog_close(plp);
		}
	}
	return 0;
}

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * TranxPeer handles asynchronous RPC calls and provides the user with cancel, poll methods
 * Besides, it's running as a separate thread for parallelism, mainly used by TranxRPCHelper
 */

#ifndef DTRANX_STAGES_TRANXPEER_H_
#define DTRANX_STAGES_TRANXPEER_H_

#include <mutex>
#include <memory>
#include <boost/thread.hpp>
#include <queue>
#include <atomic>
#include <unordered_map>
#include <unordered_set>
#include <condition_variable>
#include "DTranx/RPC/ZeromqSender.h"
#include "DTranx/RPC/ZeromqReceiver.h"
#include "RPC/TranxRPC.h"
#include "DTranx/Service/Transaction.pb.h"
#include "DTranx/Util/ConfigHelper.h"
#include "DTranx/Util/ConcurrentQueue.h"
#include "SharedResources/TranxServerInfo.h"
#include "Build/Util/Profile.pb.h"
#include "PerfStage.h"

namespace DTranx {
namespace Stages {
class TranxRPCHelper;
class TranxPeer: public PerfStage {
public:
	/*
	 * context: create senders/receivers
	 * RemoteIP:remotePorts: peer server ip/port
	 * listenPorts: a list of ports listening for this peer
	 * selfAddress: peer receiver binding address
	 * remoteRouterPort: remote peer port
	 */
	TranxPeer(TranxRPCHelper* tranxRPCHelper,
			SharedResources::TranxServerInfo *tranxServerInfo,
			std::vector<uint32_t> coreIDs = std::vector<uint32_t>());
	virtual ~TranxPeer() noexcept(false);

	RPC::GStatus Poll(RPC::TranxRPC* rpc,
			std::chrono::system_clock::time_point until);
	void StartPeerThread(std::vector<uint32_t> coreIDs =
			std::vector<uint32_t>());

	/*
	 * interfaces to add to queue
	 */
	void SendToPeerSend(RPC::TranxRPC *rpc, bool messageIDAlreadySet = false) {
		if (rpc->getStageTask() != TranxPeerStageTask::_OUTPUTPERFPEER) {
			std::string remoteIp = rpc->getRemoteIp();
			if (!messageIDAlreadySet) {
				uint64 messageID = nextMessageId[remoteIp].fetch_add(1);
				messageID++;
				rpc->SetReqMessageID(messageID);
			}
		}
		//assert(SendProcessQueue.enqueue(rpc));
		while (!SendProcessQueue.try_enqueue(rpc)) {
			if (SendProcessQueue.enqueue(rpc)) {
				break;
			}
		}
	}
	void SendToPeerRecv(RPC::TranxRPC *rpc) {
		//assert(RecvProcessQueue.enqueue(rpc));
		while (!RecvProcessQueue.try_enqueue(rpc)) {
			if (RecvProcessQueue.enqueue(rpc)) {
				break;
			}
		}
	}

private:
	void PeerSenderThreadMain();
	void PeerReceiverThreadMain();
	void KillPeerThread();

	/*
	 * nextMessageId assign message id's
	 */
	std::unordered_map<std::string, std::atomic<uint64>> nextMessageId;

	/*
	 * sockets
	 */
	std::unordered_map<std::string, RPC::ZeromqSender*> senders;
	std::unordered_map<std::string, RPC::ZeromqReceiver*> receivers;

	/*
	 * ip, port information
	 * listenPortMap: remote ip -> local port that listens to that peer
	 */
	std::unordered_map<std::string, uint32_t> listenPortMap;
	std::string selfAddressStr;
	uint32_t *selfAddress;
	TranxRPCHelper* tranxRPCHelper;
	SharedResources::TranxServerInfo *tranxServerInfo;
	SharedResources::TranxServerInfo::Mode mode;

	/*
	 * stage pipeline thread
	 */
	void PeerSendThread();
	boost::thread peerSendThread;
	std::atomic_bool terminateThread;
	moodycamel::ConcurrentQueue<RPC::TranxRPC *> SendProcessQueue;
	void PeerRecvThread();
	boost::thread peerRecvThread;
	moodycamel::ConcurrentQueue<RPC::TranxRPC *> RecvProcessQueue;
	std::unordered_map<std::string, std::unordered_map<uint64, RPC::TranxRPC*> > responses;

	/*
	 * buffer space to reduce memory allocation/deletion
	 */
	Util::Buffer localBuffer;
	uint32_t bufferSize;

	/*
	 * TranxPeer is non copyable
	 */
	TranxPeer(const TranxPeer&) = delete;
	TranxPeer& operator=(const TranxPeer&) = delete;
};

} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_TRANXPEER_H_ */

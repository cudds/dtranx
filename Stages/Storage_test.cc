/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "Storage.h"
#include "gtest/gtest.h"
#include "Util/FileUtil.h"
#include "DaemonStages/DaemonStagesMock.h"

using namespace DTranx;

class StorageTest: public ::testing::Test {
public:
	StorageTest() {
		Util::ConfigHelper configHelper;
		configHelper.readFile("DTranx.conf");
		tranxServerInfo = new SharedResources::TranxServerInfo();
		tranxServerInfo->SetConfig(configHelper);
		stringStorageInstance = new Stages::StringStorage(tranxServerInfo);
	}
	~StorageTest() {
		delete stringStorageInstance;
		delete tranxServerInfo;
		DTranx::Util::FileUtil::RemoveDir("dtranx.db*");
	}
private:
	Stages::StringStorage *stringStorageInstance;
	SharedResources::TranxServerInfo *tranxServerInfo;
};

TEST_F(StorageTest, readwriteversion) {
	stringStorageInstance->Write("key1", "value1_version0");
	EXPECT_EQ("value1_version0", stringStorageInstance->Read("key1"));
	EXPECT_EQ(1, stringStorageInstance->GetVersion("key1"));
	stringStorageInstance->Write("key1", "value1_version1");
	EXPECT_EQ(2, stringStorageInstance->GetVersion("key1"));
	EXPECT_EQ("value1_version1", stringStorageInstance->Read("key1"));
	std::pair<uint64, std::string> result = stringStorageInstance->ReadWithVersion("key1");
	EXPECT_EQ(2, result.first);
	EXPECT_EQ("value1_version1", result.second);
}

TEST_F(StorageTest, utilFunction) {
	EXPECT_EQ("key1#1", stringStorageInstance->CombineKey("key1"));
	uint64 epoch;
	std::string key;
	stringStorageInstance->SeparateKey("key1#2", epoch, key);
	EXPECT_EQ(2, epoch);
	EXPECT_EQ("key1", key);
}

TEST_F(StorageTest, snapshot) {
	//current epoch 1, no snapshot
	stringStorageInstance->Write("key1", "value1_version0");
	stringStorageInstance->Write("key2", "value2_version0");
	stringStorageInstance->Write("key3", "value3_version0");

	uint64 epoch;
	std::string value;
	EXPECT_FALSE(stringStorageInstance->ReadSnapshot("key1", epoch, value));
	EXPECT_FALSE(stringStorageInstance->ReadSnapshot("key4", epoch, value));

	//old snapshot 1, current epoch 2
	stringStorageInstance->CreateSnapshot();
	EXPECT_TRUE(stringStorageInstance->ReadSnapshot("key1", epoch, value));
	EXPECT_EQ("value1_version0", stringStorageInstance->Read("key1"));
	EXPECT_EQ("value1_version0", value);
	EXPECT_EQ(1, epoch);
	try {
		stringStorageInstance->ReadSnapshot("key4", epoch, value);
	} catch (Util::NotFoundException &e) {
		stringStorageInstance->Write("key1", "value1_version1");
		stringStorageInstance->Write("key4", "value4_version0");
		EXPECT_FALSE(stringStorageInstance->ReadSnapshot("key4", epoch, value));
		EXPECT_TRUE(stringStorageInstance->ReadSnapshot("key1", epoch, value));
		EXPECT_EQ("value1_version0", value);

		//old snapshot 2, current epoch 3
		stringStorageInstance->CreateSnapshot();
		EXPECT_TRUE(stringStorageInstance->ReadSnapshot("key1", epoch, value));
		EXPECT_EQ("value1_version1", value);
		EXPECT_EQ(2, epoch);
		stringStorageInstance->Write("key1", "value1_version2");
		EXPECT_TRUE(stringStorageInstance->ReadSnapshot("key2", epoch, value));
		EXPECT_EQ("value2_version0", value);
		EXPECT_EQ(2, epoch);

		//old snapshot 3, current epoch 4
		//enabled manually to test if snapshot is cleaned, add a print at ReadSnapshot function
		stringStorageInstance->CreateSnapshot();
		EXPECT_TRUE(stringStorageInstance->ReadSnapshot("key1", epoch, value));
		EXPECT_EQ("value1_version2", value);
		EXPECT_EQ(3, epoch);
		return;
	}
	EXPECT_TRUE(false);
}

TEST_F(StorageTest, SortFunc) {
	EXPECT_TRUE(
			stringStorageInstance->sortFunc(std::make_pair("1#3", ""), std::make_pair("1#4", "")));
	EXPECT_TRUE(
			stringStorageInstance->sortFunc(std::make_pair("1#3", ""), std::make_pair("1#23", "")));
}

TEST_F(StorageTest, PerfTestSnapshot) {
	int numKeys = 500;
	boost::chrono::steady_clock::time_point start = boost::chrono::steady_clock::now();
	for (int i = 1; i < numKeys; i++) {
		std::string key = "key" + std::to_string(i);
		std::string value = "value" + std::to_string(i);
		stringStorageInstance->Write(key, value);
	}
	boost::chrono::steady_clock::time_point end = boost::chrono::steady_clock::now();
	int timeElapsed =
			boost::chrono::duration_cast<boost::chrono::milliseconds>(end - start).count();
	std::cout << "storage write test: " << timeElapsed << " ms, for " << numKeys << " writes"
			<< std::endl;
	std::cout << "average latency: " << 1.0 * timeElapsed / numKeys << " ms/req" << std::endl;

	int repReads = 10;
	start = boost::chrono::steady_clock::now();
	for (int rep = 0; rep < repReads; ++rep) {
		for (int i = 1; i < numKeys; i++) {
			std::string key = "key" + std::to_string(i);
			stringStorageInstance->Read(key);
		}
	}
	end = boost::chrono::steady_clock::now();
	timeElapsed = boost::chrono::duration_cast<boost::chrono::milliseconds>(end - start).count();
	std::cout << "storage read test: " << timeElapsed << " ms, for " << repReads * numKeys
			<< " reads" << std::endl;
	std::cout << "average latency: " << 1.0 * timeElapsed / (numKeys * repReads) << " ms/req"
			<< std::endl;
}

TEST_F(StorageTest, PerfTestLevelDB) {
	int numWrites = 500;
	leveldb::DB* db;
	leveldb::Options options;
	options.create_if_missing = true;
	leveldb::Status status = leveldb::DB::Open(options, "dtranx.db2", &db);

	if (false == status.ok()) {
		ERROR("Unable to open/create database dtranx.db: %s", status.ToString().c_str());
		assert(false);
	}
	boost::chrono::steady_clock::time_point start = boost::chrono::steady_clock::now();
	for (int i = 1; i < numWrites; i++) {

		std::string key = "key" + std::to_string(i);
		std::string value = "value" + std::to_string(i);

		leveldb::WriteOptions writeOptions;
		writeOptions.sync = true;
		db->Put(writeOptions, key, value);
	}
	boost::chrono::steady_clock::time_point end = boost::chrono::steady_clock::now();
	int timeElapsed =
			boost::chrono::duration_cast<boost::chrono::milliseconds>(end - start).count();
	std::cout << "storage write test: " << timeElapsed << " ms, for " << numWrites << " writes"
			<< std::endl;
	std::cout << "average latency: " << 1.0 * timeElapsed / numWrites << " ms/req" << std::endl;
	if (db != NULL) {
		delete db;
	}
	DTranx::Util::FileUtil::RemoveDir("dtranx.db2");
}

TEST_F(StorageTest, Delete) {
	//current epoch 1, no snapshot
	stringStorageInstance->Write("key1", "value1_version0");
	stringStorageInstance->Write("key2", "value2_version0");
	stringStorageInstance->Write("key3", "value3_version0");

	uint64 epoch;
	std::string value;

	//old snapshot 1, current epoch 2
	stringStorageInstance->CreateSnapshot();
	stringStorageInstance->Write("key1", "value1_version1");
	stringStorageInstance->Delete("key1");
	try {
		stringStorageInstance->Read("key1");
	} catch (Util::NotFoundException &e) {
		return;
	}
	EXPECT_TRUE(false);
}

TEST_F(StorageTest, WritePlain) {
	stringStorageInstance->WritePlain("key1#1", "1#value1_version0");
	EXPECT_EQ("value1_version0", stringStorageInstance->Read("key1"));
}

class StorageTest_Stage: public ::testing::Test {
public:
	StorageTest_Stage() {
		Util::ConfigHelper configHelper;
		configHelper.readFile("DTranx.conf");
		tranxServerInfo = new SharedResources::TranxServerInfo();
		tranxServerInfo->SetConfig(configHelper);
		std::vector<std::string> ips;
		ips.push_back("192.168.0.1");
		ips.push_back("192.168.0.2");
		ips.push_back("192.168.0.3");
		tranxServerInfo->SetAllNodes(ips);
		tranxServerInfo->SetNodeID(2);
		tranxServerInfo->SetMode(SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER);
		tranxServiceSharedData = new DTranx::SharedResources::TranxServiceSharedData(2,
				tranxServerInfo);
		stringStorageInstance = new Stages::StringStorage(tranxServerInfo);
		daemonStages = new DaemonStages::DaemonStagesMock(tranxServerInfo, tranxServiceSharedData);
		stringStorageInstance->daemonStagesMock = daemonStages;
		stringStorageInstance->StartStage();
	}
	~StorageTest_Stage() {
		stringStorageInstance->ShutStage();
		delete stringStorageInstance;
		delete daemonStages;
		delete tranxServerInfo;
		delete tranxServiceSharedData;
		DTranx::Util::FileUtil::RemoveDir("dtranx.db");
		Util::FileUtil::RemoveDir("dtranx.mapdb");
	}

	Stages::StringStorage *stringStorageInstance;
	DaemonStages::DaemonStagesMock *daemonStages;
	SharedResources::TranxServerInfo *tranxServerInfo;
	DTranx::SharedResources::TranxServiceSharedData *tranxServiceSharedData;

private:
	StorageTest_Stage(const StorageTest_Stage&) = delete;
	StorageTest_Stage& operator=(const StorageTest_Stage&) = delete;
};

TEST_F(StorageTest_Stage, _Write) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setStorageTask(Stages::StorageStageTask::_Write);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	std::unordered_map<std::string, std::string> writeset;
	writeset["key1"] = "value1";
	writeset["key2"] = "value2";
	element.setWriteSet(writeset);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());

	RPC::ServerRPC serverRPC;
	serverRPC.requestMessage->mutable_storageservicerpc()->mutable_readdata()->mutable_request()->set_key(
			"key2");
	serverRPC.requestMessage->mutable_storageservicerpc()->mutable_readdata()->mutable_request()->set_snapshot(
	false);
	element.SetServerRPC(&serverRPC);
	element.setStorageTask(Stages::StorageStageTask::_ReadWithVersion);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_TRUE(element.isStorageKeyExist());
	EXPECT_EQ("value2", element.getStorageValue());
}

TEST_F(StorageTest_Stage, _CreateSnapshot) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);

	RPC::ServerRPC serverRPC;
	serverRPC.requestMessage->mutable_storageservicerpc()->mutable_readdata()->mutable_request()->set_key(
			"key2");
	serverRPC.requestMessage->mutable_storageservicerpc()->mutable_readdata()->mutable_request()->set_snapshot(
	true);
	element.SetServerRPC(&serverRPC);
	element.setStorageTask(Stages::StorageStageTask::_ReadSnapshot);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_FALSE(element.isStorageSnapshotExist());

	element.setStorageTask(Stages::StorageStageTask::_Write);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	std::unordered_map<std::string, std::string> writeset;
	writeset["key1"] = "value1";
	writeset["key2"] = "value2";
	element.setWriteSet(writeset);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());

	element.setStorageTask(Stages::StorageStageTask::_CreateSnapshot);
	element.SetTranxID(tranxID);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());

	element.setStorageTask(Stages::StorageStageTask::_ReadSnapshot);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_TRUE(element.isStorageKeyExist());
	EXPECT_TRUE(element.isStorageSnapshotExist());
	EXPECT_EQ("value2", element.getStorageValue());
	EXPECT_EQ(1, element.getStorageEpoch());
}

TEST_F(StorageTest_Stage, _CheckReadSet) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);

	element.setStorageTask(Stages::StorageStageTask::_WritePlain);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	std::unordered_map<std::string, std::string> writeset;
	writeset["key1#1"] = "5#value1";
	writeset["key2#1"] = "6#value2";
	element.setWriteSet(writeset);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());

	element.setStorageTask(Stages::StorageStageTask::_CheckReadSet);
	std::unordered_map<std::string, uint64> readset;
	readset["key1"] = 4;
	element.setReadSet(readset);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_FALSE(element.isStorageCheckSucc());

	element.setStorageTask(Stages::StorageStageTask::_CheckReadSet);
	readset["key1"] = 5;
	element.setReadSet(readset);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_TRUE(element.isStorageCheckSucc());
}

TEST_F(StorageTest_Stage, _SearchAll) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	Util::TranxID tranxID(1, 1);
	element.setStorageTask(Stages::StorageStageTask::_WritePlain);
	element.SetTranxID(tranxID);
	std::unordered_map<std::string, std::string> writeset;
	writeset["key1#1"] = "5#value1_5";
	writeset["key1#2"] = "9#value1_9";
	writeset["key2#1"] = "6#value2";
	element.setWriteSet(writeset);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());

	element.setStorageTask(Stages::StorageStageTask::_SearchAll);
	std::vector<std::string> keys;
	keys.push_back("key1");
	keys.push_back("key2");
	element.setStorageKeys(keys);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	std::vector<std::vector<std::pair<std::string, std::string>>>allResults = element.getStorageSearchAllResults();
	EXPECT_EQ(2, allResults.size());
	EXPECT_EQ(2, allResults[0].size());
	EXPECT_EQ("key1#1", allResults[0][0].first);
	EXPECT_EQ("5#value1_5", allResults[0][0].second);
}

TEST_F(StorageTest_Stage, _WritePlain) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	Util::TranxID tranxID(1, 1);
	element.setStorageTask(Stages::StorageStageTask::_WritePlain);
	element.SetTranxID(tranxID);
	std::unordered_map<std::string, std::string> writeset;
	writeset["key1#1"] = "5#value1_5";
	writeset["key1#2"] = "9#value1_9";
	writeset["key2#1"] = "6#value2";
	element.setWriteSet(writeset);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());

	RPC::ServerRPC serverRPC;
	serverRPC.requestMessage->mutable_storageservicerpc()->mutable_readdata()->mutable_request()->set_key(
			"key1");
	serverRPC.requestMessage->mutable_storageservicerpc()->mutable_readdata()->mutable_request()->set_snapshot(
	false);
	element.SetServerRPC(&serverRPC);
	element.setStorageTask(Stages::StorageStageTask::_ReadWithVersion);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_TRUE(element.isStorageKeyExist());
	EXPECT_EQ("value1_9", element.getStorageValue());
	EXPECT_EQ(9, element.getStorageVersion());
}

TEST_F(StorageTest_Stage, _Delete) {
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	Util::TranxID tranxID(1, 1);
	element.setStorageTask(Stages::StorageStageTask::_WritePlain);
	element.SetTranxID(tranxID);
	std::unordered_map<std::string, std::string> writeset;
	writeset["key1#1"] = "5#value1_5";
	writeset["key1#2"] = "9#value1_9";
	writeset["key2#1"] = "6#value2";
	element.setWriteSet(writeset);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());

	element.setStorageTask(Stages::StorageStageTask::_Delete);
	std::vector<std::string> keys;
	keys.push_back("key1");
	element.setStorageKeys(keys);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());

	RPC::ServerRPC serverRPC;
	serverRPC.requestMessage->mutable_storageservicerpc()->mutable_readdata()->mutable_request()->set_key(
			"key1");
	serverRPC.requestMessage->mutable_storageservicerpc()->mutable_readdata()->mutable_request()->set_snapshot(
	false);
	element.SetServerRPC(&serverRPC);
	element.setStorageTask(Stages::StorageStageTask::_ReadWithVersion);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_FALSE(element.isStorageKeyExist());
}

class StorageTest_Stage_MultiThreads: public ::testing::Test {
public:
	StorageTest_Stage_MultiThreads() {
		Util::ConfigHelper configHelper;
		configHelper.readFile("DTranx.conf");
		tranxServerInfo = new SharedResources::TranxServerInfo();
		tranxServerInfo->SetConfig(configHelper);
		std::vector<std::string> ips;
		ips.push_back("192.168.0.1");
		ips.push_back("192.168.0.2");
		ips.push_back("192.168.0.3");
		tranxServerInfo->SetAllNodes(ips);
		tranxServerInfo->SetNodeID(2);
		tranxServerInfo->SetMode(SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER);
		tranxServiceSharedData = new DTranx::SharedResources::TranxServiceSharedData(2,
				tranxServerInfo);
		stringStorageInstance = new Stages::StringStorage(tranxServerInfo, 4);
		daemonStages = new DaemonStages::DaemonStagesMock(tranxServerInfo, tranxServiceSharedData);
		stringStorageInstance->daemonStagesMock = daemonStages;
		stringStorageInstance->StartStage();
	}
	~StorageTest_Stage_MultiThreads() {
		stringStorageInstance->ShutStage();
		delete stringStorageInstance;
		delete daemonStages;
		delete tranxServerInfo;
		delete tranxServiceSharedData;
		DTranx::Util::FileUtil::RemoveDir("dtranx.db*");
		Util::FileUtil::RemoveDir("dtranx.mapdb");
	}

	Stages::StringStorage *stringStorageInstance;
	DaemonStages::DaemonStagesMock *daemonStages;
	SharedResources::TranxServerInfo *tranxServerInfo;
	DTranx::SharedResources::TranxServiceSharedData *tranxServiceSharedData;

private:
	StorageTest_Stage_MultiThreads(const StorageTest_Stage_MultiThreads&) = delete;
	StorageTest_Stage_MultiThreads& operator=(const StorageTest_Stage_MultiThreads&) = delete;
};

TEST_F(StorageTest_Stage_MultiThreads, _Write) {
	/*
	 * if there are two threads, key1 is in storage0, key2 is in storage1
	 */
	//Util::Log::setLogPolicy( { { "Stages", "PROFILE" } });
	Stages::TranxServiceQueueElement element;
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setStorageTask(Stages::StorageStageTask::_Write);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	std::unordered_map<std::string, std::string> writeset;
	writeset["key1"] = "value1";
	writeset["key2"] = "value2";
	element.setWriteSet(writeset);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());

	RPC::ServerRPC serverRPC;
	serverRPC.requestMessage->mutable_storageservicerpc()->mutable_readdata()->mutable_request()->set_key(
			"key2");
	serverRPC.requestMessage->mutable_storageservicerpc()->mutable_readdata()->mutable_request()->set_snapshot(
	false);
	element.SetServerRPC(&serverRPC);
	element.setStorageTask(Stages::StorageStageTask::_ReadWithVersion);
	stringStorageInstance->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_TRUE(element.isStorageKeyExist());
	EXPECT_EQ("value2", element.getStorageValue());
	EXPECT_EQ(4, Util::FileUtil::LsPrefixDir("./", "dtranx.db").size());
}

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 * ZeromqConnection is the basic class for ZeromqReceiver and ZeromqSender and manages connections.
 */

#ifndef DTRANX_RPC_ZEROMQCONNECTION_H_
#define DTRANX_RPC_ZEROMQCONNECTION_H_

#include <memory>
#include "zmsg.hpp"

namespace DTranx {
namespace RPC {

class ZeromqConnection {
public:
	explicit ZeromqConnection(std::shared_ptr<zmq::context_t> context, int socketType);
	virtual ~ZeromqConnection() noexcept(false);
	bool ConnectInproc(const std::string &address);
	bool ConnectTcp(const std::string &address, const std::string &port);
	bool Bind(const std::string &address, const std::string &port);

protected:
	zmq::socket_t socket;
	// ZeromqConnection has to keep a copy of context, or else when the context is destructed, the socket cannot be destructed and the program hangs
	std::shared_ptr<zmq::context_t> context;

private:
	bool Connect(const std::string& endpoint);
};

} /* namespace RPC */
} /* namespace DTranx */

#endif /* DTRANX_RPC_ZEROMQCONNECTION_H_ */

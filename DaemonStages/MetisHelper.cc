/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <cassert>
#include "MetisHelper.h"
#include "DTranx/Util/Log.h"
#include "Util/StringUtil.h"

namespace DTranx {
namespace DaemonStages {

const int MetisHelper::NO_PARTITION_FOUND = -1;
const int MetisHelper::INVALID_VERTEXID = -1;
const int MetisHelper::VOID_VERTEXID = 0;

MetisHelper::MetisHelper(bool replicationEnabled)
		: replicationEnabled(replicationEnabled) {
	nextVertexID = 0;
}

MetisHelper::~MetisHelper() noexcept(false) {
}

int MetisHelper::ConvertKeyToVertexID(std::string key) {
	if (keyToVertexID.find(key) == keyToVertexID.end()) {
		keyToVertexID[key] = nextVertexID++;
		vertexIDToKey.push_back(key);
		//std::cout << "key: " << key << " maps to " << nextVertexID - 1 << std::endl;
		assert(nextVertexID == vertexIDToKey.size());
	}
	return keyToVertexID[key];
}

std::string MetisHelper::ConvertVertexIDToKey(int vertexID) {
	assert(vertexIDToKey.size() > vertexID);
	return vertexIDToKey[vertexID];
}

void MetisHelper::AddTranx(std::unordered_set<std::string> readSet,
		std::unordered_set<std::string> writeSet) {
	if (replicationEnabled) {
		return _AddTranxR(readSet, writeSet);
	}
	std::unordered_set<std::string> allKeys;
	allKeys.insert(readSet.begin(), readSet.end());
	allKeys.insert(writeSet.begin(), writeSet.end());

	for (auto it = allKeys.begin(); it != allKeys.end(); ++it) {
		int vertexID = ConvertKeyToVertexID(*it);
		if (vertexID >= graph.size()) {
			assert(vertexID == graph.size());
			graph.push_back(std::unordered_map<int, int>());
		}
	}

	for (auto it = allKeys.begin(); it != allKeys.end(); ++it) {
		int vertexID1 = ConvertKeyToVertexID(*it);
		std::unordered_map<int, int>& adjNodes = graph[vertexID1];

		for (auto it_b = allKeys.begin(); it_b != allKeys.end(); ++it_b) {
			if ((*it_b) == (*it)) {
				continue;
			}
			int vertexID2 = ConvertKeyToVertexID(*it_b);
			if (adjNodes.find(vertexID2) == adjNodes.end()) {
				adjNodes[vertexID2] = 1;
			} else {
				adjNodes[vertexID2]++;
			}
		}
	}
}

bool MetisHelper::RunMetis(int numOfPartition) {
	if (replicationEnabled) {
		return _RunMetisR(numOfPartition);
	}
	int edgeNum = 0;
	for (auto it = graph.begin(); it != graph.end(); ++it) {
		edgeNum += (*it).size();
	}
	VERBOSE("vertex num: %lu", graph.size());
	VERBOSE("edge num: %d", edgeNum);

	idx_t nvtxs = vertexIDToKey.size();
	idx_t ncon = 1;
	idx_t nparts = numOfPartition;
	idx_t *xadj = new idx_t[nvtxs + 1];
	idx_t *adjncy = new idx_t[edgeNum];
	idx_t *adjwgt = new idx_t[edgeNum];
	int adjncyIndex = 0;
	int adjIndex = 0;
	xadj[adjIndex++] = adjncyIndex;
	for (auto it = graph.begin(); it != graph.end(); ++it) {
		for (auto it_b = it->begin(); it_b != it->end(); ++it_b) {
			assert(adjncyIndex < edgeNum);
			adjncy[adjncyIndex] = it_b->first;
			adjwgt[adjncyIndex++] = it_b->second;
		}
		assert(adjIndex <= nvtxs);
		xadj[adjIndex++] = adjncyIndex;
	}
	idx_t objval;
	idx_t *part = new idx_t[nvtxs];

	int metis_ok = METIS_PartGraphKway(&nvtxs, &ncon, xadj, adjncy, NULL, NULL, adjwgt, &nparts,
	NULL,
	NULL, NULL, &objval, part);
	if (METIS_OK == metis_ok) {
		VERBOSE("total edge-cut/communication cost is %d\nAssigment:\n", (int )objval);
		partitionResult.assign(part, part + nvtxs);
	}
	delete[] xadj;
	delete[] adjncy;
	delete[] part;
	delete[] adjwgt;
	return metis_ok == METIS_OK;
}

int MetisHelper::GetPartitions(std::string key) {
	int vertexID = ConvertKeyToVertexID(key);
	if (vertexID >= partitionResult.size()) {
		return NO_PARTITION_FOUND;
	}
	return partitionResult[vertexID];
}

std::unordered_set<int> MetisHelper::GetPartitionsR(std::string key) {
	int vertexID = ConvertKeyToVertexID(key);
	if (vertexID >= partitionResultR.size()) {
		return std::unordered_set<int>(NO_PARTITION_FOUND);
	}
	return partitionResultR[vertexID];
}

void MetisHelper::GetNewMappingsR(std::unordered_map<std::string, std::unordered_set<int> > &newMapping) {
	for (size_t i = 0; i < partitionResultR.size(); ++i) {
		std::string key = ConvertVertexIDToKey(i);
		newMapping[key] = partitionResultR[i];
	}
	return;
}

void MetisHelper::_AddTranxR(std::unordered_set<std::string> readSet,
		std::unordered_set<std::string> writeSet) {
	std::set<int> allKeysID;
	/*
	 * remove tranx with only one element
	 */
	std::unordered_set<std::string> allKeys;
	allKeys.insert(readSet.begin(), readSet.end());
	allKeys.insert(writeSet.begin(), writeSet.end());
	if (allKeys.size() == 1) {
		return;
	}

	/*
	 * Adjust update number for write set
	 */
	for (auto it = writeSet.begin(); it != writeSet.end(); ++it) {
		int vertexID = ConvertKeyToVertexID(*it);
		allKeysID.insert(vertexID);
		if (vertexID >= graphR_S1.size()) {
			assert(vertexID == graphR_S1.size());
			graphR_S1.push_back(Vertex());
		}
		graphR_S1[vertexID].numOfUpdate++;
	}

	/*
	 * insert readset
	 */
	for (auto it = readSet.begin(); it != readSet.end(); ++it) {
		int vertexID = ConvertKeyToVertexID(*it);
		allKeysID.insert(vertexID);
		if (vertexID >= graphR_S1.size()) {
			assert(vertexID == graphR_S1.size());
			graphR_S1.push_back(Vertex());
		}
	}
	/*
	 * for each data item/vertex, update the tranx list
	 */
	for (auto it = allKeysID.begin(); it != allKeysID.end(); ++it) {
		int vertexID1 = *it;
		std::set<int> tranxSet = allKeysID;

		std::map<std::set<int>, std::pair<int, int>>::iterator obj =
				graphR_S1[vertexID1].tranxs.find(tranxSet);

		if (obj == graphR_S1[vertexID1].tranxs.end()) {
			graphR_S1[vertexID1].tranxs[tranxSet] = std::make_pair(1, 0);
		} else {
			obj->second.first++;
		}
	}
}

void MetisHelper::_ConvertToStage2() {
	/*
	 * algorithm
	 * 	nextReplicatedVertex: current used replicated vertexID for each vertexID, it should be either INVALID_VERTEXID
	 * 		or some value between start and end for that particular vertexID
	 * 		it's updated as we add the replicated vertex for the particular vertex
	 * 	vertexMappingR: the replicated vertex ID ranges of each original vertex ID
	 */
	std::vector<int> nextReplicatedVertex;
	/*
	 * initialize vertexMappingR
	 */
	int startVertexID = 0;
	for (auto it = graphR_S1.begin(); it != graphR_S1.end(); ++it) {
		int numRep = (it->tranxs.size() == 1) ? 1 : (1 + it->tranxs.size());
		vertexMappingR.push_back(std::make_pair(startVertexID, startVertexID + numRep - 1));
		nextReplicatedVertex.push_back((numRep == 1) ? startVertexID : startVertexID + 1);
		startVertexID += numRep;
		int index = 0;
		for (auto it_b = it->tranxs.begin(); it_b != it->tranxs.end(); ++it_b) {
			it_b->second.second = index++;
		}
	}
	/*
	 * DEBUG: show the stage1 graph
	 *
	 std::cout << "stage1" << std::endl;
	 for (auto it = graphR_S1.begin(); it != graphR_S1.end(); ++it) {
	 for (auto it_b = it->tranxs.begin(); it_b != it->tranxs.end(); ++it_b) {
	 for (auto it_c = it_b->first.begin(); it_c != it_b->first.end(); ++it_c) {
	 std::cout << *it_c << " ";
	 }
	 std::cout << std::endl;
	 }
	 std::cout << std::endl;
	 }
	 */
	/*
	 * generating stage2 graph data
	 */
	int currentVertexID = 0;
	for (auto it = graphR_S1.begin(); it != graphR_S1.end(); ++it) {
		/*
		 * now dealing with currentVertexID
		 * corresponding replicated vertexID are between vertexMappingR[currentVertexID].first and vertexMappingR[currentVertexID].second
		 * corresponding current replicatedVertex for others to connect to is nextReplicatedVertex[currentVertexID]
		 */

		/*
		 * Add the center node to the stage2 graph
		 *
		 * store the adjacent nodes for replicated vertices at adjncy
		 */
		if (it->tranxs.size() > 1) {
			//std::cout << "add center node for vertex: " << currentVertexID << " -> ";
			std::list<int> tranxList;
			for (int i = vertexMappingR[currentVertexID].first + 1;
					i <= vertexMappingR[currentVertexID].second; ++i) {
				tranxList.push_back(i);
			}
			graphR_S2.push_back(std::make_pair(tranxList, 1));
		}
		/*
		 * Add all the other replicated nodes
		 */
		int centerNodeRepVertexID = vertexMappingR[currentVertexID].first;
		for (auto it_b = it->tranxs.begin(); it_b != it->tranxs.end(); ++it_b) {
			/*
			 * now dealing with tranx it_b(iterator)
			 *
			 * isOldTranx means if tranx already exist for a previous vertex
			 * in which case the corresponding replicated vertexID for the whole transaction should
			 * come from the old tranx instead of nextReplicatedVertex
			 *
			 */
			bool isOldTranx = false;
			std::unordered_map<int, int> tmpCorrespondingRepVertex;

			/*
			 * tranxList records all the neighbor replicated vertex ID's for this new replicated vertex
			 */
			std::list<int> tranxList;
			if (it->tranxs.size() > 1) {
				tranxList.push_back(centerNodeRepVertexID);
			}
			for (auto it_c = it_b->first.begin(); it_c != it_b->first.end(); ++it_c) {
				if (*it_c == currentVertexID) {
					continue;
				}
				if (*it_c < currentVertexID) {
					if (!isOldTranx) {
						isOldTranx = true;
						tmpCorrespondingRepVertex = FillOldTranxRepVertex(it_b, *it_c);
					}
					tranxList.push_back(tmpCorrespondingRepVertex[*it_c]);
				} else {
					if (isOldTranx) {
						assert(
								tmpCorrespondingRepVertex.find(*it_c)
										!= tmpCorrespondingRepVertex.end());
						tranxList.push_back(tmpCorrespondingRepVertex[*it_c]);
					} else {
						int tmpNextRepVertex = nextReplicatedVertex[*it_c];
						assert(tmpNextRepVertex <= vertexMappingR[*it_c].second);
						tranxList.push_back(tmpNextRepVertex);
						nextReplicatedVertex[*it_c] =
								(tmpNextRepVertex < vertexMappingR[*it_c].second) ?
										tmpNextRepVertex + 1 : INVALID_VERTEXID;
					}
				}
			}
			graphR_S2.push_back(std::make_pair(tranxList, it_b->second.first));

		}
		currentVertexID++;
	}
	/*
	 * DEBUG: show the stage2 graph
	 *
	 *
	 std::cout << "stage2" << std::endl;
	 for (auto it = graphR_S2.begin(); it != graphR_S2.end(); ++it) {
	 for (auto it_b = it->first.begin(); it_b != it->first.end(); ++it_b) {
	 std::cout << *it_b << " ";
	 }
	 std::cout << std::endl;
	 }
	 */

}

std::unordered_map<int, int> MetisHelper::FillOldTranxRepVertex(const std::map<std::set<int>,
		std::pair<int, int>>::iterator& it,
		int prevVertexID) {

	std::set<int> matchSet = it->first;

	std::unordered_map<int, int> result;
	int index = -1;
	std::map<std::set<int>, std::pair<int, int>>::iterator it_prev =
			graphR_S1[prevVertexID].tranxs.find(matchSet);
	assert(it_prev != graphR_S1[prevVertexID].tranxs.end());
	index = it_prev->second.second;
	assert(index != -1);
	assert(it_prev->second.first == it->second.first);
	/*
	 * replicatedVertexID is the corresponding replicated vertexID for prevVertexID that stores the vertices assignment
	 */
	int replicatedVertexID;
	if (vertexMappingR[prevVertexID].first == vertexMappingR[prevVertexID].second) {
		replicatedVertexID = vertexMappingR[prevVertexID].first;
	} else {
		replicatedVertexID = vertexMappingR[prevVertexID].first + 1 + index;
	}
	assert(replicatedVertexID <= vertexMappingR[prevVertexID].second);

	/*
	 * matching vertex ID with their replicated vertex ID's
	 * graphR_S2[replicatedVertexID]
	 */
	result[prevVertexID] = replicatedVertexID;
	for (auto vertexToResolve = it->first.begin(); vertexToResolve != it->first.end();
			++vertexToResolve) {
		if (*vertexToResolve == prevVertexID) {
			continue;
		}
		for (auto vertexToMatch = graphR_S2[replicatedVertexID].first.begin();
				vertexToMatch != graphR_S2[replicatedVertexID].first.end(); ++vertexToMatch) {
			if (*vertexToMatch >= vertexMappingR[*vertexToResolve].first
					&& *vertexToMatch <= vertexMappingR[*vertexToResolve].second) {
				result[*vertexToResolve] = *vertexToMatch;
				break;
			}
		}
	}
	return result;
}

void MetisHelper::_PostProcessResult() {
	int totalVertexNum = vertexIDToKey.size();
	assert(vertexIDToKey.size() == vertexMappingR.size());
	for (int i = 0; i < totalVertexNum; ++i) {
		partitionResultR.push_back(std::unordered_set<int>());
		std::unordered_set<int> &partitionList = partitionResultR.back();
		for (int repVertexID = vertexMappingR[i].first; repVertexID <= vertexMappingR[i].second;
				++repVertexID) {
			partitionList.insert(partitionResult[repVertexID]);
		}
	}
}

bool MetisHelper::_RunMetisR(int numOfPartition) {
	_ConvertToStage2();
	int edgeNum = 0;
	for (auto it = graphR_S2.begin(); it != graphR_S2.end(); ++it) {
		edgeNum += it->first.size();
	}
	VERBOSE("total replicated vertex num: %lu", graphR_S2.size());
	VERBOSE("edge num: %d", edgeNum);

	idx_t nvtxs = graphR_S2.size();
	idx_t ncon = 1;
	idx_t nparts = numOfPartition;
	idx_t *xadj = new idx_t[nvtxs + 1];
	idx_t *adjncy = new idx_t[edgeNum];
	idx_t *adjwgt = new idx_t[edgeNum];
	int adjncyIndex = 0;
	/*
	 * an index to xadj that stores the start index of the next vertex to its adjacent vertices
	 */
	int adjIndex = 0;
	xadj[adjIndex++] = adjncyIndex;

	int currentOriginalVertex = 0;
	bool replicateFirst = true;
	bool isReplicated = vertexMappingR[currentOriginalVertex].first
			!= vertexMappingR[currentOriginalVertex].second;
	for (auto it = graphR_S2.begin(); it != graphR_S2.end(); ++it) {
		assert(adjIndex >= vertexMappingR[currentOriginalVertex].first);
		if (adjIndex - 1 == vertexMappingR[currentOriginalVertex].first) {
			replicateFirst = true;
		} else if (adjIndex - 1 <= vertexMappingR[currentOriginalVertex].second) {
			replicateFirst = false;
		} else if (adjIndex - 1 > vertexMappingR[currentOriginalVertex].second) {
			currentOriginalVertex++;
			isReplicated = vertexMappingR[currentOriginalVertex].first
					!= vertexMappingR[currentOriginalVertex].second;
			assert(adjIndex - 1 == vertexMappingR[currentOriginalVertex].first);
			replicateFirst = true;
		}
		for (auto it_b = it->first.begin(); it_b != it->first.end(); ++it_b) {
			assert(adjncyIndex < edgeNum);
			adjncy[adjncyIndex] = *it_b;
			/*
			 * the weights for center node to replicated node and replicated node to center node
			 */
			if ((isReplicated && replicateFirst)
					|| *it_b == vertexMappingR[currentOriginalVertex].first) {
				adjwgt[adjncyIndex] = graphR_S1[currentOriginalVertex].numOfUpdate;
			} else {
				adjwgt[adjncyIndex] = it->second;
			}
			adjncyIndex++;
		}

		assert(adjIndex <= nvtxs);
		xadj[adjIndex++] = adjncyIndex;
	}
	idx_t objval;
	idx_t *part = new idx_t[nvtxs];
	/*
	 * DEBUG:
	 std::cout << "vertex list:" << std::endl;
	 for (int i = 0; i < nvtxs; i++) {
	 int start = xadj[i];
	 int end = xadj[i + 1];
	 std::cout << "vertex " << i << std::endl;
	 std::cout << "\t";
	 for (int index = xadj[i]; index < xadj[i + 1]; ++index) {
	 std::cout << adjncy[index] << " ";
	 }
	 std::cout << std::endl;
	 std::cout << "\t";
	 for (int index = xadj[i]; index < xadj[i + 1]; ++index) {
	 std::cout << adjwgt[index] << " ";
	 }
	 std::cout << std::endl;
	 }
	 */
	if (!_CheckGraph(nvtxs, xadj, adjncy, adjwgt)) {
		ERROR("checkgraph failed in RunMetis");
		return false;
	}

	VERBOSE("Calling METIS library");
	int metis_ok = METIS_PartGraphKway(&nvtxs, &ncon, xadj, adjncy, NULL, NULL, adjwgt, &nparts,
	NULL,
	NULL, NULL, &objval, part);
	if (METIS_OK == metis_ok) {
		VERBOSE("total edge-cut/communication cost is %d\nAssigment:\n", (int )objval);
		partitionResult.assign(part, part + nvtxs);
	}
	_PostProcessResult();
	delete[] xadj;
	delete[] adjncy;
	delete[] part;
	delete[] adjwgt;
	return metis_ok == METIS_OK;
}

bool MetisHelper::_CheckGraph(idx_t nvtxs, idx_t *xadj, idx_t *adjncy, idx_t *adjwgt) {
	for (int i = 0; i < nvtxs; i++) {
		int start = xadj[i];
		int end = xadj[i + 1] - 1;
		for (int index = start; index <= end; ++index) {
			int neighbor = adjncy[index];
			int neighS = xadj[neighbor];
			int neighE = xadj[neighbor + 1] - 1;
			int exist = false;
			for (int indexn = neighS; indexn <= neighE; ++indexn) {
				if (adjncy[indexn] == i && adjwgt[indexn] == adjwgt[index]) {
					assert(adjwgt[index] != 0);
					exist = true;
					break;
				}
			}
			if (!exist) {
				std::cout << "checking vertex " << i << std::endl;
				std::cout << "for neighbor " << neighbor << std::endl;
				for (int j = neighS; j <= neighE; ++j) {
					std::cout << adjncy[j] << " ";
				}
				std::cout << std::endl;
				return false;
			}
		}
	}
	return true;
}

} /* namespace DaemonStages */
} /* namespace DTranx */

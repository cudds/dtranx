/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * 	RSMHelper interacts with the logcabin service to launch replicated state machines,
 * 	reconfigure it and shut it down. It also gives server ID assignment based on
 * 	the sequence in group membership
 */

#ifndef DTRANX_SERVER_RSMHELPER_H_
#define DTRANX_SERVER_RSMHELPER_H_

#include <vector>
#include <logcabin/Client/Client.h>
#include "DTranx/Util/ConfigHelper.h"
#include "DTranx/Util/types.h"

namespace DTranx {
namespace Server {

class RSMHelper {
public:
	RSMHelper(Util::ConfigHelper& config);
	virtual ~RSMHelper() noexcept(false);
	/*
	 * launch a new RSM, reconfigure it and store serverID mappings for the cluster
	 * it's called by only one server.
	 */
	void Init(std::vector<std::string> ips);
	void Shutdown();
	uint64_t GetServerID();
	LogCabin::Client::Configuration GetAllServers();

private:
	void PrintConfiguration(
			const std::pair<uint64_t, LogCabin::Client::Configuration>& configuration);
	bool Reconfig(std::vector<std::string> servers, std::string leader, std::string smID);
	bool InitEpoch();
	std::string smID;
	std::string RSMPort;
	std::string RSMDaemonPort;
	std::string selfAddr;
	std::vector<std::string> ips;
};

} /* namespace Server */
} /* namespace DTranx */

#endif /* DTRANX_SERVER_RSMHELPER_H_ */

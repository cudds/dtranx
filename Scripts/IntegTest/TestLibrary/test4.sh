#!/bin/bash
function test4(){
	# there are contentions and failure, esepcially for readall
	sudo docker exec dtranx1 bash -c '/dtranx/SimpleTranx -p Workloads/writedistributed -r 400 &> /dtranx/test4_write' &
	sudo docker exec dtranx2 bash -c '/dtranx/SimpleTranx -p Workloads/readall -r 400 &> /dtranx/test4_read' &
	wait
	sudo docker cp dtranx1:/dtranx/test4_write ./
	sudo docker exec dtranx1 bash -c 'rm /dtranx/test4_write'
	sudo docker cp dtranx2:/dtranx/test4_read ./
	sudo docker exec dtranx2 bash -c 'rm /dtranx/test4_read'
}
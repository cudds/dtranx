/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef _DTRANX_UTIL_NULLPOINTEREXCEPTION_H_
#define _DTRANX_UTIL_NULLPOINTEREXCEPTION_H_

#include <exception>
#include <string>
namespace DTranx {
namespace Util {

class NullPointerException: public std::exception {
public:
	NullPointerException(std::string m)
			: msg(m) {
	}
	~NullPointerException() throw () {
	}
	const char* what() const throw () {
		return msg.c_str();
	}

private:
	std::string msg;
};

} /* namespace Util */
} /* namespace DTranx */

#endif /* _DTRANX_UTIL_NULLPOINTEREXCEPTION_H_ */

#!/usr/bin/env python
#
# TestHelper
#	database
#
import argparse
from Utility.SSHHelper import *
from Utility.BashHelper import *

# TODO: could be moved to Utility directory, together with GetIpsFromFile in Deploy.py 
def GetIpsFromFile(filename):
	# handle relative/absolute path
	if len(filename) > 0 and filename.startswith('/'):
		ipsRealPath = filename
	else:
		workingPath = GetWorkingDir()
		ipsRealPath = workingPath + filename
	print "ip path is: " + ipsRealPath
	f = open(ipsRealPath,'r')
	ips = []
	for line in f:
		ips.append(line.strip())
	return ips, ipsRealPath

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()

db_parser = subparsers.add_parser('db', help='database test helper')
db_parser.add_argument("-s","--server",dest="serverfile",help="server ips file", required = True)
db_parser.add_argument("-d","--ds", dest="datastructure",help="data structure, e.g. rtree, btree", required = True)
db_parser.add_argument("-m","--mode", dest="mode",help="mode, 0: copy current db to data structure db, 1: copy data structure db to current db, 2: copy remote data structure db to local, 3: copy local backup of data structure db to remote", required = True)
db_parser.set_defaults(subcommand = 'db')

clean_parser = subparsers.add_parser('clean', help='clean test helper')
clean_parser.add_argument("-c","--client",dest="clientfile",help="client ips file", required = True)
clean_parser.set_defaults(subcommand = 'clean')

log_parser = subparsers.add_parser('log', help="retrieve logs")
log_parser.add_argument("-s","--server",dest="serverfile",help="server ips file", required = True)
log_parser.set_defaults(subcommand = 'log')

testout_parser = subparsers.add_parser('testout', help='retrieve test output')
testout_parser.add_argument("-c","--client",dest="clientfile",help="client ips file", required = True)
testout_parser.set_defaults(subcommand = 'testout')

args = parser.parse_args()

if args.subcommand == 'db':
	ips, ipsRealPath = GetIpsFromFile(args.serverfile)
	datastructure = args.datastructure
	assert datastructure == 'btree' or datastructure == 'rtree'
	index=1
	for ip in ips:
		if args.mode == '0':
			print runSSHCommand(ip, RemoteUser, "rm -rf ~/Test/dtranx."+datastructure+".db0;rm -rf ~/Test/dtranx."+datastructure+".db1;\
			rm -rf ~/Test/dtranx."+datastructure+".db2;rm -rf ~/Test/dtranx."+datastructure+".db3;rm -rf ~/Test/dtranx."+datastructure+".mapdb")
			print runSSHCommand(ip, RemoteUser, "cp -R ~/Test/dtranx.db0 ~/Test/dtranx."+datastructure+".db0;cp -R ~/Test/dtranx.db1 ~/Test/dtranx."+datastructure+".db1;\
			cp -R ~/Test/dtranx.db2 ~/Test/dtranx."+datastructure+".db2;cp -R ~/Test/dtranx.db3 ~/Test/dtranx."+datastructure+".db3;cp -R ~/Test/dtranx.mapdb ~/Test/dtranx."+datastructure+".mapdb")
		elif args.mode == "1":
			print runSSHCommand(ip, RemoteUser, "rm -rf ~/Test/dtranx.db0;rm -rf ~/Test/dtranx.db1;\
			rm -rf ~/Test/dtranx.db2;rm -rf ~/Test/dtranx.db3;rm -rf ~/Test/dtranx.mapdb")
			print runSSHCommand(ip, RemoteUser, "cp -R ~/Test/dtranx."+datastructure+".db0 ~/Test/dtranx.db0;cp -R ~/Test/dtranx."+datastructure+".db1 ~/Test/dtranx.db1;\
			cp -R ~/Test/dtranx."+datastructure+".db2 ~/Test/dtranx.db2;cp -R ~/Test/dtranx."+datastructure+".db3 ~/Test/dtranx.db3;cp -R ~/Test/dtranx."+datastructure+".mapdb ~/Test/dtranx.mapdb")
		elif args.mode == "2":
			dirname  = datastructure+"/server"+str(index)
			runBash("mkdir -p "+dirname)
			scp(False,dirname+"/",RemoteUser,ip, "~/Test/dtranx."+datastructure+".db0")
			scp(False,dirname+"/",RemoteUser,ip, "~/Test/dtranx."+datastructure+".db1")
			scp(False,dirname+"/",RemoteUser,ip, "~/Test/dtranx."+datastructure+".db2")
			scp(False,dirname+"/",RemoteUser,ip, "~/Test/dtranx."+datastructure+".db3")
			scp(False,dirname+"/",RemoteUser,ip, "~/Test/dtranx."+datastructure+".mapdb")
			index=index+1
		elif args.mode == "3":
			dirname  = datastructure+"/server"+str(index)
			scp(True,dirname+"/dtranx."+datastructure+".db0",RemoteUser,ip, "~/Test/")
			scp(True,dirname+"/dtranx."+datastructure+".db1",RemoteUser,ip, "~/Test/")
			scp(True,dirname+"/dtranx."+datastructure+".db2",RemoteUser,ip, "~/Test/")
			scp(True,dirname+"/dtranx."+datastructure+".db3",RemoteUser,ip, "~/Test/")
			scp(True,dirname+"/dtranx."+datastructure+".mapdb",RemoteUser,ip, "~/Test/")
			index=index+1
elif args.subcommand == 'clean':
	ips, ipsRealPath = GetIpsFromFile(args.clientfile)
	for ip in ips:
		print ip
		KillRemote(ip, RemoteUser, 'ycsbc')
elif args.subcommand == 'log':
	ips, ipsRealPath = GetIpsFromFile(args.serverfile)
	for ip in ips:
		scp(False,"./dtranx.out."+ip,RemoteUser,ip, "~/Test/DTranx.out")
elif args.subcommand == 'testout':
	ips, ipsRealPath = GetIpsFromFile(args.clientfile)
	for ip in ips:
		scp(False,"./test.output."+ip,RemoteUser,ip, "~/test.output")
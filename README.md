Copyright (c) 2015 University of Colorado Boulder. All rights reserved.
Developed by: 
Ning Gao(nigo9731@colorado.edu)

------------------------------------------------------------------
Introduction
DTranx is a distributed transactional solution that uses optimistic concurrency control
and two phase commit.
------------------------------------------------------------------
Files(TODO: update after it's been converted to SEDA design)
Client/: client library used by client program and server side peer, TranxService etc. 
Example/: important examples for different libraries like leveldb, metis, clients
gtest/: gtest for unit testing
IntegTest/: files used for integration test, like pin program
RPC/: rpc library
Server/: main, server classes
site_scons/: used for scons build system
Storage/: storage code
Test/: unit test, integration test and performance test
Tla/: tla verification model
Tranx/: transaction related services
Util/: utility functions
Scripts/" scripts to automate deployment, log analyzing etc.
DTranx.conf.sample: sampleconfigration file

Deployments
	Build: in local machine, refer to my tiddly wiki page
	Google Unit Test: in local machine
	Integration Test: use docker machines in local machines
	Deployment: openstack virtual machines run

Library Dependency
	Static: Metis
	Zeromq: 4.0.5
	Protobuf: commit(f473bb9)
	Boost: 1.53.0
	LevelDB
	Logcabin
	libcrypto++-dev
	gperftools
	Note: anything changes with logcabin, run deploy build so that dtranx project is always linked with the latest logcabin library
Service Dependency
	Logcabin
	
Compiler
	Scons: 2.1.0
	G++: 4.9
		
------------------------------------------------------------------
Performance
	1. gperftools(CPU profiling)
		Intro: 
			Downloaded from https://github.com/gperftools/gperftools
			commit version: https://github.com/gperftools/gperftools
		How to Use:
			First, call ::ProfilerStart("gprofile") to start profiling and
			call ::ProfilerStop() to stop profiling; then linked the project 
			with -lprofiler option.
			Now, run the program with  "env CPUPROFILE=dtranx.prof program_path program_arguments"
			generate callgrind with "pprof --callgrind program_path dtranx.prof &> dtranx.callgrind"
			check out call graph by "kcachegrind dtranx.callgrind"

------------------------------------------------------------------

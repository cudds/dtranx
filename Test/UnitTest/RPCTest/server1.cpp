#include <boost/thread.hpp>
#include <mutex>
#include <memory>
#include <queue>

#include "zmsg.hpp"
class ZeromqProxy {

private:
	boost::thread zeromqProxyThread1;
	boost::thread zeromqProxyThread2;
	std::shared_ptr<bool> terminate_proxy;
	std::shared_ptr<zmq::context_t> context;
	void StartProxyThread1(std::string port, std::shared_ptr<bool> terminate_proxy,
							std::shared_ptr<zmq::context_t> context) {
		zmq::socket_t frontend(*context, ZMQ_ROUTER);

		std::string localAddress = "tcp://128.104.222.74";
		std::string frontAddress = localAddress + ":" + port;
		try {
			frontend.bind(frontAddress.c_str());
		} catch (zmq::error_t &e) {
			std::cout << e.what() << std::endl;
			assert(false);
		}

		int lingtime = 0;
		frontend.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
		int hwm = 0;
		size_t len = sizeof(hwm);
		frontend.setsockopt(ZMQ_SNDHWM, &hwm, len);
		frontend.setsockopt(ZMQ_RCVHWM, &hwm, len);
		int mandatory = 1;
		len = sizeof(mandatory);
		frontend.setsockopt(ZMQ_ROUTER_MANDATORY, &mandatory, len);
		int count = 0;
		std::chrono::system_clock::time_point start, end;
		while (true) {
			zmsg zm(frontend);

			 count++;
			 if (count % 100000 == 0) {
			 std::cout << "1receiver " << count << std::endl;
			 }
			 if (count == 1) {
			 start = std::chrono::system_clock::now();
			 } else if (count == 5000000) {
			 end = std::chrono::system_clock::now();
			 int timePassed =
			 std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
			 std::cout << "1throughout: " << 1.0 * 5000000 * 1000 / timePassed << std::endl;
			 }

		}

		frontend.close();
	}
	void StartProxyThread2(std::string port, std::shared_ptr<bool> terminate_proxy,
							std::shared_ptr<zmq::context_t> context) {
		zmq::socket_t frontend(*context, ZMQ_ROUTER);

		std::string localAddress = "tcp://128.104.222.74";
		std::string frontAddress = localAddress + ":" + port;
		try {
			frontend.bind(frontAddress.c_str());
		} catch (zmq::error_t &e) {
			std::cout << e.what() << std::endl;
			assert(false);
		}

		int lingtime = 0;
		frontend.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
		int hwm = 0;
		size_t len = sizeof(hwm);
		frontend.setsockopt(ZMQ_SNDHWM, &hwm, len);
		frontend.setsockopt(ZMQ_RCVHWM, &hwm, len);
		int mandatory = 1;
		len = sizeof(mandatory);
		frontend.setsockopt(ZMQ_ROUTER_MANDATORY, &mandatory, len);
		int count = 0;
		std::chrono::system_clock::time_point start, end;
		while (true) {
			zmsg zm(frontend);

			 count++;
			 if (count % 100000 == 0) {
			 std::cout << "2receiver " << count << std::endl;
			 }
			 if (count == 1) {
			 start = std::chrono::system_clock::now();
			 } else if (count == 5000000) {
			 end = std::chrono::system_clock::now();
			 int timePassed =
			 std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
			 std::cout << "2throughout: " << 1.0 * 5000000 * 1000 / timePassed << std::endl;
			 }

		}

		frontend.close();
	}
public:
	explicit ZeromqProxy(std::shared_ptr<zmq::context_t> context)
			: context(context){
		terminate_proxy = std::make_shared<bool>(false);
	}

	~ZeromqProxy() {
		ShutProxy();
	}

	void StartProxy() {
		zeromqProxyThread1 = boost::thread(&ZeromqProxy::StartProxyThread1, this, "2222",
				terminate_proxy, context);
		zeromqProxyThread2 = boost::thread(&ZeromqProxy::StartProxyThread2, this, "2223",
						terminate_proxy, context);
	}

	void ShutProxy() {
		if (terminate_proxy) {
			*terminate_proxy = true;
		}
		if (zeromqProxyThread1.joinable()) {
			zeromqProxyThread1.join();
		}
	}

};

int main(void) {
	s_version();
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(50);
	std::unique_ptr<ZeromqProxy> proxy = std::unique_ptr<ZeromqProxy>(
			new ZeromqProxy(context));
	proxy->StartProxy();

	while (true) {
		sleep(5);
	}

}


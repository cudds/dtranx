/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <utility>
#include <memory>
#include <chrono>
#include "TranxService.h"
#include "RPC/ClientRPC.h"
#include "Util/StaticConfig.h"
#include "Util/StringUtil.h"
#include "Util/FileUtil.h"
#include "DTranx/Client/Client.h"
#include "DTranx/Util/Log.h"
#include "Util/ThreadHelper.h"
#include "SharedResources/MappingDS.h"
#include "Build/Util/Profile.pb.h"

#include "Stages/Storage.h"
#include "Stages/Log/TranxLog.h"
#include "Stages/Lock/WriteBlockLockTable.h"
#include "Stages/TranxStates.h"
#include "Stages/TranxRPCHelper.h"
#include "SharedResources/TranxHistory.h"
#include "DaemonStages/TranxAck.h"
#include "DaemonStages/Repartition.h"
#include "Server/Server.h"

namespace DTranx {
namespace Service {

TranxService::TranxService(SharedResources::TranxServerInfo *tranxServerInfo,
		SharedResources::TranxServiceSharedData *tranxServiceSharedData)
		: Service(tranxServerInfo, tranxServiceSharedData, "tranxservice") {
	keysModeBuffer.reserve(10);
	readSetBuffer.reserve(10);
}

TranxService::~TranxService() noexcept(false) {
}

void TranxService::HandleRPC(RPC::ServerRPC *serverRPC) {
	GOpCode opcode = GOpCode(serverRPC->GetTranxOpcode());
	switch (opcode) {
	case GOpCode::TRANX_ABORT:
		TranxAbort(serverRPC);
		break;
	case GOpCode::TRANX_COMMIT:
		TranxCommit(serverRPC);
		break;
	case GOpCode::TRANX_PREP:
		TranxPrepare(serverRPC);
		break;
	case GOpCode::TRANX_INQUIRE:
		TranxInquire(serverRPC);
		break;
	case GOpCode::TRANX_GC:
		TranxGC(serverRPC);
		break;
	case GOpCode::TRANX_PARTITION:
		TranxPartition(serverRPC);
		break;
	case GOpCode::TRANX_MIGRATION:
		TranxMigrate(serverRPC);
		break;
	default:
		break;
	}
}

void TranxService::SnapshotThread() {
	NOTICE("SnapshotThread started");
	Util::ThreadHelper::SetThreadID("DTranx::SnapshotThread");
	uint64_t snapshotPeriod = std::stoull(
			Util::StaticConfig::GetInstance()->Read("SnapshotPeriod"));
	Util::ConfigHelper config = tranxServerInfo->GetConfig();
	std::shared_ptr<zmq::context_t> context = tranxServerInfo->GetContext();
	std::string selfAddressStr = tranxServerInfo->GetSelfAddress();
	uint32_t *selfAddress = new uint32_t[4];
	std::vector<std::string> tmpFields = Util::StringUtil::Split(selfAddressStr, '.');
	assert(tmpFields.size() == 4);
	for (int i = 0; i < 4; ++i) {
		selfAddress[i] = std::stoul(tmpFields[i], nullptr, 10);
	}
	while (true) {
		if (terminateSnapshotThread.load()) {
			break;
		}
		sleep(snapshotPeriod);
		//if the previous snapshot creation is not complete, wait to avoid concurrent snapshot creation
		while (true) {
			if (terminateSnapshotThread.load()) {
				NOTICE("SnapshotThread reclaimed");
				return;
			}
			if (tranxServiceSharedData->GetSnapshotTranx().empty()) {
				break;
			}
			Util::TranxID tranxID = tranxServiceSharedData->GetSnapshotTranx();
			Stages::State state = tranxStates->CheckState(tranxID);
			if (state == Stages::State::GC) {
				break;
			}
			sleep(snapshotPeriod);
		}

		Client::Client* client = new Client::Client(selfAddressStr,
				std::stoul(config.read<std::string>("routerFrontPort").c_str(), nullptr, 10),
				context);
		std::vector<std::string> clientPortsStr = Util::StringUtil::Split(
				config.read<std::string>("clientReceivePort"), ',');
		std::vector<uint32_t> clientPorts;
		for (auto port = clientPortsStr.begin(); port != clientPortsStr.end(); ++port) {
			clientPorts.push_back(std::strtoul(port->c_str(), nullptr, 10));
		}
		int clientPortsNum = clientPorts.size();
		int currentClientPort = 0;
		assert(currentClientPort < clientPortsNum);
		while (!client->Bind(selfAddressStr, clientPorts[currentClientPort])) {
			currentClientPort = (currentClientPort + 1) % clientPortsNum;
		}
		VERBOSE("sending snapshot creation request");
		RPC::ClientRPC *clientRPC = new RPC::ClientRPC(RPC::GServiceID::ClientService, client);
		GClientCommit::Request* request =
				clientRPC->GetClientRequest()->mutable_clientcommit()->mutable_request();
		request->set_tranxtype(GTranxType::SNAPSHOT);
		clientRPC->SetClientOpcode(GOpCode::CLIENT_COMMIT);
		clientRPC->SetReqIP(selfAddress);
		clientRPC->SetReqPort(clientPorts[currentClientPort]);
		client->CallRPC(clientRPC);
		assert(client->BlockPoll(clientRPC));
		GClientCommit::Response* response =
				clientRPC->GetClientResponse()->mutable_clientcommit()->mutable_response();
		VERBOSE("snapshot creation: %s",
				(response->status() == GStatus::OK) ? "success" : "failure");
		delete client;
		delete clientRPC;
	}
	NOTICE("SnapshotThread reclaimed");
}

void TranxService::ServiceThread(int index) {
	NOTICE("TranxServiceThread started");
	uint64_t randSeed = 0;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateThread.load()) {
			NOTICE("TranxServiceThread is reclaimed");
			break;
		}
		RPC::ServerRPC *tmpRPCs[Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = serviceProcessQueue.try_dequeue_bulk(tmpRPCs,
				Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1 && index == 0) {
				/*
				 * use index here to avoid race condition on queue length samples
				 */
				AddQueueLengthSample(6, serviceProcessQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			RPC::ServerRPC *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(6);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(6);
				std::vector<int> taskLongLatencySamples = GetLongLatency(6);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName);
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				server->AddToFreeList(element);
				Stages::TranxServiceQueueElement *tmpElement =
						new Stages::TranxServiceQueueElement();
				tmpElement->setIsPerfOutPutTask(true);
				SendToServiceInternal(tmpElement);
				continue;
			}
			HandleRPC(element);
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void TranxService::ServiceInternalThread(int index) {
	NOTICE("TranxServiceInternalThread started");
	uint64_t randSeed = 0;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateInternalThread.load()) {
			NOTICE("TranxServiceInternalThread is reclaimed");
			break;
		}
		Stages::TranxServiceQueueElement *tmpRPCs[Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = serviceInternalProcessQueue.try_dequeue_bulk(tmpRPCs,
				Stages::TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1 && index == 0) {
				/*
				 * use index here to avoid race condition on queue length samples
				 */
				AddQueueLengthSample(7, serviceInternalProcessQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			Stages::TranxServiceQueueElement *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(7);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(7);
				std::vector<int> taskLongLatencySamples = GetLongLatency(7);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_Internal");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				AddToFreeList(element);
				continue;
			}
			//element->goThroughStage("TranxService");
			GOpCode opcode = element->GetOpcode();
			if (opcode == GOpCode::TRANX_PREP) {
				TranxPrepare(element);
			} else if (opcode == GOpCode::TRANX_COMMIT) {
				TranxCommit(element);
			} else if (opcode == GOpCode::TRANX_ABORT) {
				TranxAbort(element);
			} else if (opcode == GOpCode::TRANX_INQUIRE) {
				TranxInquire(element);
			} else if (opcode == GOpCode::TRANX_MIGRATION) {
				TranxMigrate(element);
			} else if (opcode == GOpCode::TRANX_GC) {
				TranxGC(element);
			} else {
				assert(false);
			}
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void TranxService::TranxPrepare(RPC::ServerRPC *serverRPC) {
	Stages::TranxServiceQueueElement *element;
	if (!freeList.try_dequeue(element)) {
		element = new Stages::TranxServiceQueueElement();
	}
	//element->goThroughStage("TranxService");
	element->SetServerRPC(serverRPC);
	element->SetOpcode(GOpCode::TRANX_PREP);
	element->setStageId(Stages::TranxServiceQueueElement::TRANXSERVICE);
	/*
	 * initialize request/response
	 */
	GTranxPrep::Request *request =
			serverRPC->GetTranxRequest()->mutable_tranxprep()->mutable_request();
	GTranxPrep::Response *response =
			serverRPC->GetTranxResponse()->mutable_tranxprep()->mutable_response();
	response->set_status(GStatus::FAIL);

	Util::TranxID tranxID;
	tranxID.SetNodeID(request->tranxid().nodeid());
	tranxID.SetTranxID(request->tranxid().tranxid());
	element->SetTranxID(tranxID);
	element->setTranxType(request->tranxtype());

	//VERBOSE("tranxservice prepare request for tranxid %d#%lu with messageID %lu",
	//		tranxID.GetNodeID(), tranxID.GetTranxID(), serverRPC->GetReqMessageID());
	element->setStateTask(Stages::TranxStatesStageTask::_CheckAndSetReadyState);
	element->setStageNum(1);
	tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
			ptok_tranxStates_external);
}

void TranxService::TranxPrepare(Stages::TranxServiceQueueElement *element) {
	int stageNum = element->getStageNum();
	if (stageNum == 1) {
		Stages::State state = element->getState();
		if (state == Stages::State::COMMIT || state == Stages::State::ABORT
				|| state == Stages::State::GC || state == Stages::State::MIGRATION) {
			server->AddToFreeList(element->GetServerRPC());
			AddToFreeList(element);
			return;
		}
		/*
		 * it need to reply in this case because this server might've failed and
		 * the transaction coordinator is still waiting
		 *
		 * however it should not reply when it didn't fail because volatile
		 * state READY does not mean it's processed. Duplication of prepare messages
		 * if not failure is taken care of by messagesFromPeers
		 */

		if (state == Stages::State::READY) {
			element->GetServerRPC()->GetTranxResponse()->mutable_tranxprep()->mutable_response()->set_status(
					GStatus::OK);
			replyThread->SendReplyPeer(element->GetServerRPC());
			AddToFreeList(element);
			return;
		}
		GTranxPrep::Request *request =
				element->GetServerRPC()->GetTranxRequest()->mutable_tranxprep()->mutable_request();
		keysModeBuffer.clear();

		if (element->getTranxType() == GTranxType::REPARTITION) {
			const google::protobuf::RepeatedPtrField<GMapItem>& mappingItems =
					request->newmappings();
			assert(!mappingItems.empty());
			for (auto it = mappingItems.begin(); it != mappingItems.end(); ++it) {
				keysModeBuffer[it->key()] = Stages::Lock::BasicLockTableLockMode::Exclusive;
				for (auto ip = it->ips().begin(); ip != it->ips().end(); ++ip) {
					newMappings[it->key()].insert(*ip);
				}
			}
			element->setKeysMode(keysModeBuffer);
			element->setStageNum(2);
			element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSBLOCKING);
			lockTable->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		}

		const google::protobuf::RepeatedPtrField<GItem>& readItems = request->read_set();
		const google::protobuf::RepeatedPtrField<GItem>& writeItems = request->write_set();
		SharedResources::TranxServiceSharedData::TranxItems* curTranxItem =
				tranxServiceSharedData->NewCurTranx();
		for (auto it = readItems.begin(); it != readItems.end(); ++it) {
			keysModeBuffer[it->key()] = Stages::Lock::BasicLockTableLockMode::Shared;
			curTranxItem->readItems.push_back(*it);
		}
		for (auto it = writeItems.begin(); it != writeItems.end(); ++it) {
			keysModeBuffer[it->key()] = Stages::Lock::BasicLockTableLockMode::Exclusive;
			curTranxItem->writeItems.push_back(*it);
		}
		element->setStageNum(3);
		const Util::TranxID &tranxID = element->GetTranxID();
		tranxServiceSharedData->AddCurTranx(tranxID, curTranxItem);
		if (element->getTranxType() == GTranxType::SNAPSHOT) {
			element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTEPOCHLOCK);
			lockTable->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		} else {
			element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSMAP);
			element->setKeysMode(keysModeBuffer);
			lockTable->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		}
		return;
	} else if (stageNum == 2) {
		assert(element->isLockObtained());
		element->setStageNum(4);
		element->setLogTask(Stages::Log::TranxLogStageTask::_ReadyRepartition);
		tranxLog->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else if (stageNum == 3) {
		if (element->isLockObtained()) {
			element->setStageNum(5);
			element->setStorageTask(Stages::StorageStageTask::_CheckReadSet);
			readSetBuffer.clear();

			const google::protobuf::RepeatedPtrField<GItem>& readItems =
					element->GetServerRPC()->GetTranxRequest()->tranxprep().request().read_set();
			for (auto it = readItems.begin(); it != readItems.end(); ++it) {
				readSetBuffer[it->key()] = it->version();
			}
			element->setReadSet(readSetBuffer);
			localStorage->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		} else {
			//VERBOSE("Tranx %d#%lu lock not obtained", element->GetTranxID().GetNodeID(),
			//		element->GetTranxID().GetTranxID());
			assert(element->getTranxType() != GTranxType::SNAPSHOT);
			element->setStageNum(6);
			element->setStateTask(Stages::TranxStatesStageTask::_CheckAndSetAbortState);
			tranxStates->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID, ptok_tranxStates);
			return;
		}

	} else if (stageNum == 4) {
		GTranxPrep::Response *response =
				element->GetServerRPC()->GetTranxResponse()->mutable_tranxprep()->mutable_response();
		response->set_status(GStatus::OK);
		replyThread->SendReplyPeer(element->GetServerRPC());
		AddToFreeList(element);
		return;
	} else if (stageNum == 5) {
		/*
		 * Storage Check returned
		 */
		if (element->isStorageCheckSucc()) {
			/*
			 * CheckState here to avoid unnecessary processing
			 * removing the following stage doesn't affect correctness though
			 */
			element->setStageNum(7);
			element->setStateTask(Stages::TranxStatesStageTask::_CheckState);
			tranxStates->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID, ptok_tranxStates);
			return;
		} else {
			//VERBOSE("Tranx %d#%lu storage check failed", element->GetTranxID().GetNodeID(),
			//		element->GetTranxID().GetTranxID());
			element->setStageNum(8);
			element->setStateTask(Stages::TranxStatesStageTask::_CheckAndSetAbortState);
			tranxStates->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID, ptok_tranxStates);
			return;
		}

	} else if (stageNum == 6) {
		/*
		 * Lock not obtained
		 * CheckAndSetAbortState returned
		 */
		Stages::State state = element->getState();
		if (state != Stages::State::ABORT && state != Stages::State::GC) {
			element->setStageNum(9);
			element->setLogTask(Stages::Log::TranxLogStageTask::_ParticipantAbort);
			tranxLog->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		}
		replyThread->SendReplyPeer(element->GetServerRPC());
		const Util::TranxID &tranxID = element->GetTranxID();
		tranxServiceSharedData->DeleteCurTranx(tranxID);
		AddToFreeList(element);
		return;

	} else if (stageNum == 7) {
		/*
		 * Storage check succ
		 * CheckState returned
		 */
		Stages::State state = element->getState();
		if (state == Stages::State::ABORT || state == Stages::State::GC) {
			/*
			 * Lock needs to be released because there's a chance that
			 * abort request comes after TranxPrepare check state but complete
			 * before this TranxPrepare locks, so that will leave the locks still locked
			 */
			element->setStageNum(10);
			element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSMAP);
			lockTable->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		} else {
			element->setStageNum(4);
			/*
			 * abort log might happen before ready log
			 */
			element->setLogTask(Stages::Log::TranxLogStageTask::_Ready);
			tranxLog->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		}

	} else if (stageNum == 8) {
		/*
		 * storage check fail
		 * CheckAndSetAbortState returned
		 */
		Stages::State state = element->getState();
		if (state != Stages::State::ABORT && state != Stages::State::GC) {
			element->setStageNum(11);
			element->setLogTask(Stages::Log::TranxLogStageTask::_ParticipantAbort);
			tranxLog->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		}
		replyThread->SendReplyPeer(element->GetServerRPC());
		const Util::TranxID &tranxID = element->GetTranxID();
		AddToFreeList(element);
		return;
	} else if (stageNum == 9) {
		/*
		 * Lock not obtained/released
		 * no previous abort/gc
		 * TranxLog returned
		 */
		replyThread->SendReplyPeer(element->GetServerRPC());
		const Util::TranxID &tranxID = element->GetTranxID();
		tranxServiceSharedData->DeleteCurTranx(tranxID);
		AddToFreeList(element);
		return;

	} else if (stageNum == 10) {
		/*
		 * storage check succ
		 * state shows already abort/gc
		 * TranxLock released returned
		 */
		const Util::TranxID &tranxID = element->GetTranxID();
		tranxServiceSharedData->DeleteCurTranx(tranxID);
		server->AddToFreeList(element->GetServerRPC());
		AddToFreeList(element);
		return;

	} else if (stageNum == 11) {
		/*
		 * abort log written
		 */
		element->setStageNum(9);
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSMAP);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else {
		assert(false);
	}
}
void TranxService::TranxCommit(RPC::ServerRPC *serverRPC) {
	Stages::TranxServiceQueueElement *element;
	if (!freeList.try_dequeue(element)) {
		element = new Stages::TranxServiceQueueElement();
	}
	//element->goThroughStage("TranxService");
	element->SetServerRPC(serverRPC);
	element->SetOpcode(GOpCode::TRANX_COMMIT);
	element->setStageId(Stages::TranxServiceQueueElement::TRANXSERVICE);
	/*
	 * initialize request/response
	 */
	GTranxCommit::Request *request =
			serverRPC->GetTranxRequest()->mutable_tranxcommit()->mutable_request();
	GTranxCommit::Response *response =
			serverRPC->GetTranxResponse()->mutable_tranxcommit()->mutable_response();
	response->set_status(GStatus::OK);
	Util::TranxID& tranxID = element->GetMutableTranxID();
	tranxID.SetNodeID(request->tranxid().nodeid());
	tranxID.SetTranxID(request->tranxid().tranxid());
	element->setTranxType(request->tranxtype());
	//VERBOSE("tranxservice commit request for tranxid %d#%lu with messageID %lu with %p",
	//		tranxID.GetNodeID(), tranxID.GetTranxID(), serverRPC->GetReqMessageID(), element);
	element->setStageNum(1);
	element->setStateTask(Stages::TranxStatesStageTask::_CheckAndSetCommitState);
	tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
			ptok_tranxStates_external);
}

void TranxService::TranxCommit(Stages::TranxServiceQueueElement *element) {
	int stageNum = element->getStageNum();
	if (stageNum == 1) {
		Stages::State state = element->getState();
		if (state == Stages::State::GC) {
			server->AddToFreeList(element->GetServerRPC());
			AddToFreeList(element);
			return;
		}
		if (state == Stages::State::COMMIT) {
			replyThread->SendReplyPeer(element->GetServerRPC());
			AddToFreeList(element);
			return;
		}
		element->setStageNum(2);
		element->setLogTask(Stages::Log::TranxLogStageTask::_Commit);
		element->setLogType(Stages::Log::GLogType::COMMIT);
		tranxLog->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else if (stageNum == 2) {
		if (element->getTranxType() == GTranxType::REPARTITION) {
			assert(!newMappings.empty());
			std::vector<std::string> keys;
			for (auto it = newMappings.begin(); it != newMappings.end(); ++it) {
				tranxServiceSharedData->getMappingStorage()->UpdateLookupTable(it->first,
						it->second);
				keys.push_back(it->first);
			}
			element->setKeys(keys);
			element->setStageNum(3);
			element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSVECTOR);
			lockTable->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		} else {
			replyThread->SendReplyPeer(element->GetServerRPC());
			element->SetServerRPC(NULL);
			if (element->getTranxType() == GTranxType::SNAPSHOT) {
				element->setStageNum(4);
				element->setStorageTask(Stages::StorageStageTask::_CreateSnapshot);
				localStorage->SendToStage(element,
						Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
				return;
			} else {
				element->setStageNum(5);
				std::unordered_map<std::string, std::string> writeset;
				const Util::TranxID &tranxID = element->GetTranxID();
				SharedResources::TranxServiceSharedData::TranxItems* tranxDetail =
						tranxServiceSharedData->GetCurTranx(tranxID);
				uint32_t writeSize = tranxDetail->GetWriteSize();
				for (int i = 0; i < writeSize; ++i) {
					writeset[tranxDetail->writeItems[i].key()] = tranxDetail->writeItems[i].value();
				}
				element->setWriteSet(writeset);
				element->setStorageTask(Stages::StorageStageTask::_Write);
				localStorage->SendToStage(element,
						Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
				return;
			}
		}
	} else if (stageNum == 3) {
		/*
		 * lock are released
		 */
		replyThread->SendReplyPeer(element->GetServerRPC());
		newMappings.clear();
		AddToFreeList(element);
		return;
	} else if (stageNum == 4) {
		element->setStageNum(6);
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASEEPOCHLOCK);
		element->setTerminateInThisStage(true);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else if (stageNum == 5) {
		//VERBOSE("Tranx %d#%lu releasing locks", element->GetTranxID().GetNodeID(), element->GetTranxID().GetTranxID());
		element->setStageNum(6);
		std::vector<std::string> keys;
		SharedResources::TranxServiceSharedData::TranxItems* tranxDetail =
				tranxServiceSharedData->GetAndDeleteCurTranx(element->GetTranxID());
		uint32_t readSize = tranxDetail->GetReadSize();
		uint32_t writeSize = tranxDetail->GetWriteSize();
		for (int i = 0; i < readSize; ++i) {
			keys.push_back(tranxDetail->readItems[i].key());
		}
		for (int i = 0; i < writeSize; ++i) {
			keys.push_back(tranxDetail->writeItems[i].key());
		}
		tranxServiceSharedData->Reclaim(tranxDetail);
		const Util::TranxID &tranxID = element->GetTranxID();
		element->setKeys(keys);
		element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSVECTOR);
		element->setTerminateInThisStage(true);
		lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else {
		assert(false);
	}

}

void TranxService::TranxAbort(RPC::ServerRPC *serverRPC) {
	Stages::TranxServiceQueueElement *element;
	if (!freeList.try_dequeue(element)) {
		element = new Stages::TranxServiceQueueElement();
	}
	//element->goThroughStage("TranxService");
	element->SetServerRPC(serverRPC);
	element->SetOpcode(GOpCode::TRANX_ABORT);
	element->setStageId(Stages::TranxServiceQueueElement::TRANXSERVICE);
	/*
	 * initialize request/response
	 */
	GTranxAbort::Request *request =
			serverRPC->GetTranxRequest()->mutable_tranxabort()->mutable_request();
	GTranxAbort::Response *response =
			serverRPC->GetTranxResponse()->mutable_tranxabort()->mutable_response();
	response->set_status(GStatus::OK);
	Util::TranxID& tranxID = element->GetMutableTranxID();
	tranxID.SetNodeID(request->tranxid().nodeid());
	tranxID.SetTranxID(request->tranxid().tranxid());
	//VERBOSE("tranxservice abort request for tranxid %d#%lu with messageID %lu", tranxID.GetNodeID(),
	//		tranxID.GetTranxID(), serverRPC->GetReqMessageID());
	element->setStageNum(1);
	element->setStateTask(Stages::TranxStatesStageTask::_CheckAndSetAbortState);
	tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
			ptok_tranxStates_external);
}

void TranxService::TranxAbort(Stages::TranxServiceQueueElement *element) {
	int stageNum = element->getStageNum();
	if (stageNum == 1) {
		Stages::State state = element->getState();
		if (state == Stages::State::GC) {
			server->AddToFreeList(element->GetServerRPC());
			AddToFreeList(element);
			return;
		}
		if (state == Stages::State::ABORT) {
			replyThread->SendReplyPeer(element->GetServerRPC());
			AddToFreeList(element);
			return;
		}
		const Util::TranxID& tranxID = element->GetTranxID();
		assert(tranxServiceSharedData->GetCurTranx(tranxID) != NULL);
		element->setStageNum(2);

		element->setLogTask(Stages::Log::TranxLogStageTask::_ParticipantAbort);
		tranxLog->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;
	} else if (stageNum == 2) {
		std::vector<std::string> keys;
		const Util::TranxID &tranxID = element->GetTranxID();
		SharedResources::TranxServiceSharedData::TranxItems* curTranxItem =
				tranxServiceSharedData->GetAndDeleteCurTranx(tranxID);
		if (curTranxItem != NULL) {
			uint32_t readSize = curTranxItem->GetReadSize();
			uint32_t writeSize = curTranxItem->GetWriteSize();
			for (int i = 0; i < readSize; ++i) {
				keys.push_back(curTranxItem->readItems[i].key());
			}
			for (int i = 0; i < writeSize; ++i) {
				keys.push_back(curTranxItem->writeItems[i].key());
			}
			tranxServiceSharedData->Reclaim(curTranxItem);
			element->setStageNum(3);
			element->setKeys(keys);
			element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSVECTOR);
			lockTable->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
			return;
		} else {
			/*
			 * This could happen when two abort requests come together.
			 */
			replyThread->SendReplyPeer(element->GetServerRPC());
			AddToFreeList(element);
			return;
		}

	} else if (stageNum == 3) {
		replyThread->SendReplyPeer(element->GetServerRPC());
		AddToFreeList(element);
		return;
	} else {
		assert(false);
	}
}

/*
 * if the local has decided ,then return commit/abort
 * if the local has not decided but knows the tranx
 * 		if it hasn't processed yet, return abort
 * 		if itself has processed but waiting for others, return waiting
 * if the local has not decided and doesn't know the tranx, return abort
 */
void TranxService::TranxInquire(RPC::ServerRPC *serverRPC) {
	Stages::TranxServiceQueueElement *element;
	if (!freeList.try_dequeue(element)) {
		element = new Stages::TranxServiceQueueElement();
	}
	//element->goThroughStage("TranxService");
	element->SetServerRPC(serverRPC);
	element->SetOpcode(GOpCode::TRANX_INQUIRE);
	element->setStageId(Stages::TranxServiceQueueElement::TRANXSERVICE);
	/*
	 * initialize request/response
	 */
	GTranxInquire::Request *request =
			serverRPC->GetTranxRequest()->mutable_tranxinquire()->mutable_request();
	GTranxInquire::Response *response =
			serverRPC->GetTranxResponse()->mutable_tranxinquire()->mutable_response();
	//in inquire message, status is not used but tranxstatus is
	response->set_status(GStatus::OK);
	Util::TranxID& tranxID = element->GetMutableTranxID();
	tranxID.SetNodeID(request->tranxid().nodeid());
	tranxID.SetTranxID(request->tranxid().tranxid());
	//VERBOSE("tranxservice inquire request for tranxid %s", tranxID.c_str());
	element->setStageNum(1);
	element->setStateTask(Stages::TranxStatesStageTask::_CheckState);
	tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
			ptok_tranxStates_external);
}

void TranxService::TranxInquire(Stages::TranxServiceQueueElement *element) {
	int stageNum = element->getStageNum();
	if (stageNum == 1) {
		Stages::State state = element->getState();
		if (state == Stages::State::GC) {
			server->AddToFreeList(element->GetServerRPC());
			AddToFreeList(element);
			return;
		}
		GTranxInquire::Response *response =
				element->GetServerRPC()->GetTranxResponse()->mutable_tranxinquire()->mutable_response();

		if (state == Stages::State::ABORT) {
			response->set_tranxstatus(GInquiryTranxStatus::ABORTED);
		} else if (state == Stages::State::COMMIT) {
			response->set_tranxstatus(GInquiryTranxStatus::COMMITTED);
		} else if (state == Stages::State::READY) {
			response->set_tranxstatus(GInquiryTranxStatus::WAITING);
		} else {
			/*
			 * it only happens in participants so ack is not checked
			 */
			element->setStageNum(2);
			element->setStateTask(Stages::TranxStatesStageTask::_CheckAndSetAbortState);
			tranxStates->SendToStage(element,
					Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID, ptok_tranxStates);
			return;
		}
		replyThread->SendReplyPeer(element->GetServerRPC());
		AddToFreeList(element);
		return;
	} else if (stageNum == 2) {
		element->setStageNum(3);
		element->setLogTask(Stages::Log::TranxLogStageTask::_ParticipantAbort);
		tranxLog->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
		return;

	} else if (stageNum == 3) {
		element->GetServerRPC()->GetTranxResponse()->mutable_tranxinquire()->mutable_response()->set_tranxstatus(
				GInquiryTranxStatus::ABORTED);
		replyThread->SendReplyPeer(element->GetServerRPC());
		AddToFreeList(element);
		return;
	} else {
		assert(false);
	}
}
void TranxService::TranxGC(RPC::ServerRPC *serverRPC) {
	Stages::TranxServiceQueueElement *element;
	if (!freeList.try_dequeue(element)) {
		element = new Stages::TranxServiceQueueElement();
	}
	//element->goThroughStage("TranxService");
	element->SetServerRPC(serverRPC);
	element->SetOpcode(GOpCode::TRANX_GC);
	element->setStageId(Stages::TranxServiceQueueElement::TRANXSERVICE);
	//VERBOSE("request of gc broadcast");
	element->setStateTask(Stages::TranxStatesStageTask::_UpdateGC);
	element->setTerminateInThisStage(false);
	tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
			ptok_tranxStates_external);
}

void TranxService::TranxGC(Stages::TranxServiceQueueElement *element) {
	element->setTerminateInThisStage(true);
	element->setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_CLEANBLACKLIST);
	lockTable->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID);
}

void TranxService::TranxPartition(RPC::ServerRPC *serverRPC) {
	GTranxPartition *request = serverRPC->GetTranxRequest()->mutable_tranxpartition();
	VERBOSE("request of partitioning broadcast");
	assert(tranxServerInfo->GetNodeID() == 1);
	/*
	 * TranxHistory is a non-blocking call
	 */
	tranxServiceSharedData->AddTranxHistory(*request);
	server->AddToFreeList(serverRPC);
}

void TranxService::TranxMigrate(RPC::ServerRPC *serverRPC) {
	Stages::TranxServiceQueueElement *element;
	if (!freeList.try_dequeue(element)) {
		element = new Stages::TranxServiceQueueElement();
	}
	//element->goThroughStage("TranxService");
	element->SetServerRPC(serverRPC);
	element->SetOpcode(GOpCode::TRANX_MIGRATION);
	element->setStageId(Stages::TranxServiceQueueElement::TRANXSERVICE);
	/*
	 * initialize request/response
	 */
	GTranxMigration::Request *request =
			serverRPC->GetTranxRequest()->mutable_tranxmigration()->mutable_request();
	GTranxMigration::Response *response =
			serverRPC->GetTranxResponse()->mutable_tranxmigration()->mutable_response();
	response->set_status(GStatus::OK);
	VERBOSE("request of data migration for repartitioning");
	/*
	 * migrate from the first server in the old mapping to all new servers
	 * delete the local data if exists
	 */
	Util::TranxID& tranxID = element->GetMutableTranxID();
	tranxID.SetNodeID(request->tranxid().nodeid());
	tranxID.SetTranxID(request->tranxid().tranxid());
	element->setStageNum(1);
	element->setStateTask(Stages::TranxStatesStageTask::_CheckAndSetMigrateState);
	tranxStates->SendToStage(element, Stages::TranxServiceQueueElement::DISPATCH_STAGE_THREADID,
			ptok_tranxStates_external);
}

void TranxService::TranxMigrate(Stages::TranxServiceQueueElement *element) {
	int stageNum = element->getStageNum();
	if (stageNum == 1) {
		Stages::State state = element->getState();
		if (state == Stages::State::GC || state == Stages::State::COMMIT
				|| state == Stages::State::MIGRATION) {
			server->AddToFreeList(element->GetServerRPC());
			AddToFreeList(element);
			return;
		} else {
			element->setOldStageId(element->getStageId());
			element->setStageId(Stages::TranxServiceQueueElement::StageID::REPARTITION);
			element->setOldStageNum(2);
			element->setStageNum(11);
			element->setNewMapping(newMappings);
			repartition->SendToDaemon(element);
			return;
		}
	} else if (stageNum == 2) {
		replyThread->SendReplyPeer(element->GetServerRPC());
		AddToFreeList(element);
		return;
	}
}

} /* namespace Service */
} /* namespace DTranx */

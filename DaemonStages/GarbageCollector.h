/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * GarbageCollector polled tranxStates for ack'ed transactions, update GC log, broadcast GC info,
 * then garbage collect the WAL, finally send to Repartition thread to use the log for repartitioning
 * purposes.
 */

#ifndef DTRANX_DAEMONSTAGES_GARBAGECOLLECTOR_H_
#define DTRANX_DAEMONSTAGES_GARBAGECOLLECTOR_H_

#include <cassert>
#include <queue>
#include "DaemonStages.h"
namespace DTranx {
namespace DaemonStages {
class Repartition;

class GarbageCollector: public DaemonStages {
public:
	GarbageCollector(SharedResources::TranxServerInfo *tranxServerInfo,
			SharedResources::TranxServiceSharedData *tranxServiceSharedData);
	virtual ~GarbageCollector();

	void InitStages(Stages::TranxRPCHelper *tranxRPCHelper,
			Stages::TranxStates *tranxStates,
			Stages::StringStorage *localStorage,
			Stages::Log::TranxLog *tranxLog,
			Stages::Lock::WriteBlockLockTable *lockTable,
			Repartition *repartition) {
		DaemonStages::InitStages(tranxRPCHelper, tranxStates, localStorage, tranxLog, lockTable);
		this->repartition = repartition;
	}
	virtual void DaemonThread();

	virtual void StartService(std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) {
		bool enableCoreBinding = false;
		uint32_t coreIDIndex = 0;
		enableCoreBinding = (coreIDs.size() == 1);
		daemonThread = boost::thread(&GarbageCollector::DaemonThread, this);
		if (enableCoreBinding) {
			Util::ThreadHelper::PinToCPUCore(daemonThread, coreIDs[coreIDIndex++]);
		}
	}

	void AddToFreeList(Stages::TranxServiceQueueElement *element) {
		element->Clear();
		freeList.push(element);
	}

private:
	Repartition *repartition;

	/*
	 * non shared data
	 */
	std::unique_ptr<Stages::Log::TranxLog> gcLog;
	/*
	 * freeList is used to reuse the TranxServiceQueueElement to avoid new/delete
	 * 	since GC only allocates/deletes in the same thread, no lock needed
	 */
	std::queue<Stages::TranxServiceQueueElement *> freeList;

};

} /* namespace DaemonStages */
} /* namespace DTranx */

#endif /* DTRANX_DAEMONSTAGES_GARBAGECOLLECTOR_H_ */

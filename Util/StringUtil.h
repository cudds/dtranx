/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_UTIL_STRINGUTIL_H_
#define DTRANX_UTIL_STRINGUTIL_H_
#include <iostream>
#include <vector>
#include <set>
#include "zmsg.hpp"
#include "DTranx/Util/types.h"

namespace DTranx {
using DTranx::uint64;
namespace Util {

class StringUtil {
public:
	StringUtil();
	virtual ~StringUtil() noexcept(false);
	/*
	 * sub string check prefix/suffix
	 */
	static bool StartsWith(const std::string& haystack, const std::string& needle);
	static bool EndsWith(const std::string& haystack, const std::string& needle);

	static std::string format(const char* format, ...);
	/*
	 * Split/Concatenation functions
	 */
	static std::vector<std::string> Split(std::string str, char delimiter);
	static void Split(std::string str, char delimiter, std::vector<std::string>& result);
	static std::string Concat(std::vector<std::string> strs, char delimiter);

	/*
	 * special Ustring conversion
	 */
	static std::string UstringToString(zmsg::ustring str);

	/*
	 * hash functions from YCSB_C project to generate keys and initialize database
	 */
	static uint64 SDBMHash(const char *cstr);
	static std::string BuildKeyName(uint64 key);

	/*
	 * tranxID transformation
	 * DELIMITER here is used to separate nodeID and integer tranxID
	 * other DELIMITER are in Stages::Storage and SharedResources::MappingDS
	 *
	 * After Util::TranxID class is used to replace std::string
	 * these two functions are seldomly used.
	 */
	static const char DELIMITER;
	static std::string GetNodeID(std::string tranxID);
	static uint64 GetTranxID(std::string tranxID);

	static const uint64 kFNVOffsetBasis64;
	static const uint64 kFNVPrime64;
};

} /* namespace Util */
} /* namespace DTranx */

#endif /* DTRANX_UTIL_STRINGUTIL_H_ */

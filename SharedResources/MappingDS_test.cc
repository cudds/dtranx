/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "MappingDS.h"
#include "DTranx/Util/types.h"
#include "Util/FileUtil.h"

using namespace DTranx;

SharedResources::MappingDS *InitializeMapping() {
	std::vector<std::string> cluster;
	cluster.push_back("192.12.1.1");
	cluster.push_back("192.12.1.2");
	cluster.push_back("192.12.1.3");
	cluster.push_back("192.12.1.4");
	cluster.push_back("192.12.1.5");
	SharedResources::MappingDS *mappingDS = new SharedResources::MappingDS(cluster, "dtranx.mapdb");
	return mappingDS;
}

SharedResources::MappingDS *InitializeMappingString() {
	std::vector<std::string> cluster;
	cluster.push_back("192.12.1.1");
	cluster.push_back("192.12.1.2");
	cluster.push_back("192.12.1.3");
	cluster.push_back("192.12.1.4");
	cluster.push_back("192.12.1.5");
	SharedResources::MappingDS *mappingDS = new SharedResources::MappingDS(cluster, "dtranx.mapdb", SharedResources::MappingDS::StorageOption::STRING);
	return mappingDS;
}

SharedResources::MappingDS *InitializeMapping2() {
	std::vector<std::string> cluster;
	cluster.push_back("192.12.1.4");
	cluster.push_back("192.12.1.3");
	cluster.push_back("192.12.1.2");
	cluster.push_back("192.12.1.1");
	cluster.push_back("192.12.1.5");
	SharedResources::MappingDS *mappingDS = new SharedResources::MappingDS(cluster, "dtranx.mapdb");
	return mappingDS;
}

TEST(MappingDS, GetMappingFromBitArray) {
	SharedResources::MappingDS *mappingDS = InitializeMapping();
	//bit representation: 01101
	std::unordered_set<std::string> result;
	mappingDS->GetMappingFromBitArray(13, result);
	EXPECT_EQ(3, result.size());
	EXPECT_TRUE(result.find("192.12.1.1") != result.end());
	EXPECT_TRUE(result.find("192.12.1.2") == result.end());
	EXPECT_TRUE(result.find("192.12.1.3") != result.end());
	EXPECT_TRUE(result.find("192.12.1.4") != result.end());
	EXPECT_TRUE(result.find("192.12.1.5") == result.end());

	delete mappingDS;
	Util::FileUtil::RemoveDir("dtranx.mapdb");
}

TEST(MappingDS, GetBitArrayFromMapping) {
	SharedResources::MappingDS *mappingDS = InitializeMapping();
	//bit representation: 011010
	std::unordered_set<std::string> ips;
	ips.insert("192.12.1.2");
	ips.insert("192.12.1.4");
	ips.insert("192.12.1.5");
	uint64 result = mappingDS->GetBitArrayFromMapping(ips);
	EXPECT_EQ(26, result);

	delete mappingDS;
	Util::FileUtil::RemoveDir("dtranx.mapdb");
}

TEST(MappingDS, UpdateAndGet) {
	SharedResources::MappingDS *mappingDS = InitializeMapping();
	std::unordered_set<std::string> mapping;
	mapping.insert("192.12.1.2");
	mapping.insert("192.12.1.3");
	mappingDS->UpdateLookupTable("key1", mapping);
	mapping.clear();
	mappingDS->GetMapping("key1", mapping);
	EXPECT_EQ(2, mapping.size());
	EXPECT_TRUE(mapping.find("192.12.1.3") != mapping.end());
	mapping.clear();
	mapping.insert("192.12.1.5");
	mappingDS->UpdateLookupTable("key1", mapping);
	mapping.clear();
	mappingDS->GetMapping("key1", mapping);
	EXPECT_EQ(1, mapping.size());
	EXPECT_TRUE(mapping.find("192.12.1.5") != mapping.end());

	delete mappingDS;
	Util::FileUtil::RemoveDir("dtranx.mapdb");
}

TEST(MappingDS, STRING_STORAGE_OPTION) {
	SharedResources::MappingDS *mappingDS = InitializeMappingString();
	std::unordered_set<std::string> mapping;
	mapping.insert("192.12.1.2");
	mapping.insert("192.12.1.3");
	mappingDS->UpdateLookupTable("key1", mapping);
	mapping.clear();
	mappingDS->GetMapping("key1", mapping);
	EXPECT_EQ(2, mapping.size());
	EXPECT_TRUE(mapping.find("192.12.1.3") != mapping.end());
	mapping.clear();
	mapping.insert("192.12.1.5");
	mappingDS->UpdateLookupTable("key1", mapping);
	mapping.clear();
	mappingDS->GetMapping("key1", mapping);
	EXPECT_EQ(1, mapping.size());
	EXPECT_TRUE(mapping.find("192.12.1.5") != mapping.end());

	delete mappingDS;
	Util::FileUtil::RemoveDir("dtranx.mapdb");
}

TEST(MappingDS, DeleteTable) {
	SharedResources::MappingDS *mappingDS = InitializeMapping();
	std::unordered_set<std::string> mapping;
	mapping.insert("192.12.1.2");
	mapping.insert("192.12.1.3");
	mappingDS->UpdateLookupTable("key1", mapping);
	EXPECT_TRUE(mappingDS->DeleteFromLookupTable("key1"));
	mapping.clear();
	mappingDS->GetMapping("key1", mapping);
	EXPECT_EQ(1, mapping.size());
	EXPECT_FALSE(mappingDS->DeleteFromLookupTable("key1"));

	delete mappingDS;
	Util::FileUtil::RemoveDir("dtranx.mapdb");

}

TEST(MappingDS, FailRecovery) {
	SharedResources::MappingDS *mappingDS = InitializeMapping();
	std::unordered_set<std::string> mapping;
	mapping.insert("192.12.1.2");
	mapping.insert("192.12.1.3");
	mappingDS->UpdateLookupTable("key1", mapping);
	delete mappingDS;
	mappingDS = InitializeMapping2();
	mapping.clear();
	mappingDS->GetMapping("key1", mapping);
	EXPECT_EQ(2, mapping.size());
	EXPECT_TRUE(mapping.find("192.12.1.2") != mapping.end());
	EXPECT_TRUE(mapping.find("192.12.1.3") != mapping.end());

	delete mappingDS;
	Util::FileUtil::RemoveDir("dtranx.mapdb");
}

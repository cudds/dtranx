#include "pin.H"
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <map>
#include <cassert>
#include "unistd.h"

#define DTRANX "DTranx"
#define TRANX_SERVICE "TranxService"
#define RPC "RPC"
#define SERVER_RPC "ServerRPC"
#define ZEROMQ_SENDER "ZeromqSender"

#define TRANX_PREPARE "TranxPrepare"
#define TRANX_COMMIT "TranxCommit"
#define TRANX_ABORT "TranxAbort"
#define TRANX_INQUIRE "TranxInquire"
#define TRANX_CLIENTCOMMIT "ClientCommit"
#define POST_RECOVERY "PostRecovery"

//in microseconds
#define HANG_TIME	7000000

/*
 * vector of strings to help locate the function
 */
std::vector<std::string> _prepare;
std::vector<std::string> _commit;
std::vector<std::string> _abort;
std::vector<std::string> _inquire;
std::vector<std::string> _client_commit;
std::vector<std::string> _post_recovery;

void PrepareDataStructure() {
	_prepare.push_back(TRANX_PREPARE);

	_prepare.push_back(DTRANX);
	_prepare.push_back(TRANX_SERVICE);
	_prepare.push_back(RPC);
	_prepare.push_back(SERVER_RPC);

	_commit.push_back(TRANX_COMMIT);

	_commit.push_back(DTRANX);
	_commit.push_back(TRANX_SERVICE);
	_commit.push_back(RPC);
	_commit.push_back(SERVER_RPC);

	_abort.push_back(TRANX_ABORT);

	_abort.push_back(DTRANX);
	_abort.push_back(TRANX_SERVICE);
	_abort.push_back(RPC);
	_abort.push_back(SERVER_RPC);

	_inquire.push_back(TRANX_INQUIRE);

	_inquire.push_back(DTRANX);
	_inquire.push_back(TRANX_SERVICE);
	_inquire.push_back(RPC);
	_inquire.push_back(SERVER_RPC);

	_client_commit.push_back(TRANX_CLIENTCOMMIT);

	_client_commit.push_back(DTRANX);
	_client_commit.push_back(TRANX_SERVICE);
	_client_commit.push_back(RPC);
	_client_commit.push_back(SERVER_RPC);

	_post_recovery.push_back(POST_RECOVERY);

	_post_recovery.push_back(DTRANX);
	_post_recovery.push_back(TRANX_SERVICE);
}

/*
 * xxEnabled means whether the fault injection is enabled
 * failTest is used to decide whether to fail a program or hang
 */
bool prepareEnabled = false;
bool commitEnabled = false;
bool abortEnabled = false;
bool inquireEnabled = false;
bool clientcommitEnabled = false;
bool postrecoveryEnabled = false;
bool failOrHang = false;

/*
 * define the arguments
 */
std::ofstream OutFile;
KNOB<string> OutFileName(KNOB_MODE_WRITEONCE, "pintool", "o", "apptrace.txt",
		"app function trace file name");

KNOB<string> FailPoint(KNOB_MODE_APPEND, "pintool", "f", "",
		"where to fail(prepare, commit, abort, clientcommit, inquire, postrecovery)");

KNOB<BOOL> FailOrHang(KNOB_MODE_WRITEONCE, "pintool", "m", "0", "whether to exit or hang");

VOID ProcessArgs() {
	UINT32 num_failpoint = FailPoint.NumberOfValues();
	UINT32 i = 0;
	for (i = 0; i < num_failpoint; ++i) {
		std::string failpoint = FailPoint.Value(i);
		if (failpoint == "prepare") {
			cerr << "prepare is enabled" << endl;
			prepareEnabled = true;
		} else if (failpoint == "commit") {
			cerr << "commit is enabled" << endl;
			commitEnabled = true;
		} else if (failpoint == "abort") {
			cerr << "abort is enabled" << endl;
			abortEnabled = true;
		} else if (failpoint == "inquiry") {
			cerr << "inquiry is enabled" << endl;
			inquireEnabled = true;
		} else if (failpoint == "clientcommit") {
			cerr << "clientcommit is enabled" << endl;
			clientcommitEnabled = true;
		} else if (failpoint == "postrecovery") {
			cerr << "postrecovery is enabled" << endl;
			postrecoveryEnabled = true;
		} else {
			cerr << "invalid fail point" << endl;
		}
	}
	failOrHang = FailOrHang.Value();
	if(failOrHang){
		cerr<< "fail exit is enabled" << endl;
	}
}

/*
 * function hookers
 */
VOID RecordPrepare() {
	cerr << "prepare is called" << endl;
	if (prepareEnabled) {
		if (failOrHang) {
			exit(0);
		}
		usleep(HANG_TIME);
	}
}

VOID RecordCommit() {
	cerr << "commit is called" << endl;
	if (commitEnabled) {
		if (failOrHang) {
			exit(0);
		}
		usleep(HANG_TIME);
	}
}

VOID RecordAbort() {
	cerr << "abort is called" << endl;
	if (abortEnabled) {
		if (failOrHang) {
			exit(0);
		}
		usleep(HANG_TIME);
	}
}

VOID RecordInquire() {
	cerr << "inquire is called" << endl;
	if (inquireEnabled) {
		if (failOrHang) {
			exit(0);
		}
		usleep(HANG_TIME);
	}
}

VOID RecordClientCommit() {
	cerr << "client_commit is called" << endl;
	if (clientcommitEnabled) {
		if (failOrHang) {
			exit(0);
		}
		usleep(HANG_TIME);
	}
}

VOID RecordPostRecovery() {
	cerr << "post recovery is called" << endl;
	if (postrecoveryEnabled) {
		if (failOrHang) {
			exit(0);
		}
		usleep(HANG_TIME);
	}
}

bool CheckForFunc(SYM& sym, std::vector<std::string>& funcName) {
	bool success = true;
	std::vector<std::string>::iterator it;
	for (it = funcName.begin(); it != funcName.end(); ++it) {
		if (SYM_Name(sym).find(*it) == string::npos) {
			success = false;
			break;
		}
	}
	return success;
}

VOID Image(IMG img, VOID *v) {
	cerr << "image " << IMG_Name(img) << " is loaded" << endl;
	SYM sym = IMG_RegsymHead(img);
	int allSuccess = 0;
	while (SYM_Valid(sym)) {
		if (CheckForFunc(sym, _prepare)) {
			RTN rtn = RTN_FindByName(img, SYM_Name(sym).c_str());
			if (rtn.is_valid()) {
				cerr << "prepare is found" << endl;
				RTN_Open(rtn);
				RTN_InsertCall(rtn, IPOINT_BEFORE, (AFUNPTR) RecordPrepare, IARG_END);
				RTN_Close(rtn);
				allSuccess++;
			}
		} else if (CheckForFunc(sym, _abort)) {
			RTN rtn = RTN_FindByName(img, SYM_Name(sym).c_str());
			if (rtn.is_valid()) {
				cerr << "abort is found" << endl;
				RTN_Open(rtn);
				RTN_InsertCall(rtn, IPOINT_BEFORE, (AFUNPTR) RecordAbort, IARG_END);
				RTN_Close(rtn);
				allSuccess++;
			}
		} else if (CheckForFunc(sym, _commit)) {
			RTN rtn = RTN_FindByName(img, SYM_Name(sym).c_str());
			if (rtn.is_valid()) {
				cerr << "commit is found" << endl;
				RTN_Open(rtn);
				RTN_InsertCall(rtn, IPOINT_BEFORE, (AFUNPTR) RecordCommit, IARG_END);
				RTN_Close(rtn);
				allSuccess++;
			}
		} else if (CheckForFunc(sym, _inquire)) {
			RTN rtn = RTN_FindByName(img, SYM_Name(sym).c_str());
			if (rtn.is_valid()) {
				cerr << "inquire is found" << endl;
				RTN_Open(rtn);
				RTN_InsertCall(rtn, IPOINT_BEFORE, (AFUNPTR) RecordInquire, IARG_END);
				RTN_Close(rtn);
				allSuccess++;
			}
		} else if (CheckForFunc(sym, _client_commit)) {
			RTN rtn = RTN_FindByName(img, SYM_Name(sym).c_str());
			if (rtn.is_valid()) {
				cerr << "client_commit is found" << endl;
				RTN_Open(rtn);
				RTN_InsertCall(rtn, IPOINT_BEFORE, (AFUNPTR) RecordClientCommit, IARG_END);
				RTN_Close(rtn);
				allSuccess++;
			}
		} else if (CheckForFunc(sym, _post_recovery)) {
			RTN rtn = RTN_FindByName(img, SYM_Name(sym).c_str());
			if (rtn.is_valid()) {
				cerr << "post recovery is found" << endl;
				RTN_Open(rtn);
				RTN_InsertCall(rtn, IPOINT_BEFORE, (AFUNPTR) RecordPostRecovery, IARG_END);
				RTN_Close(rtn);
				allSuccess++;
			}
		}
		if (allSuccess == 6) {
			return;
		}
		sym = SYM_Next(sym);
	}

}

INT32 Usage() {
	PIN_ERROR("This tool intercept dtranx events\n" + KNOB_BASE::StringKnobSummary() + "\n");
	return -1;
}

int main(int argc, char *argv[]) {
	PrepareDataStructure();
	PIN_InitSymbols();
	if (PIN_Init(argc, argv)) {
		return Usage();
	}
	ProcessArgs();
	OutFile.open(OutFileName.Value().c_str());
	IMG_AddInstrumentFunction(Image, NULL);

	PIN_StartProgram();
	return 0;
}

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 * ZeromqReceiver is a wrapper around zeromq interfaces so that Server class doesn't have to deal with context, socket mode etc.
 * it's the receiver end of RPC calls
 */

#ifndef DTRANX_RPC_ZEROMQRECEIVER_H_
#define DTRANX_RPC_ZEROMQRECEIVER_H_

#include "DTranx/RPC/ZeromqConnection.h"

namespace DTranx {
namespace RPC {

class ZeromqReceiver: public ZeromqConnection {
public:
	/*
	 * call Bind before use
	 */
	explicit ZeromqReceiver(std::shared_ptr<zmq::context_t> context);
	virtual ~ZeromqReceiver() noexcept(false);
	void *Wait(int &size);
	void *Wait(int &size, int pollInMilliSecond);
	void *Wait(int &size, zmsg::ustring& clientID);
	/*
	 * the following wait is used by server to reuse local buffer
	 *
	 * if requestBufferSize is enough, message will be stored in requestBuffer
	 * if not, memory will be created and returned
	 * if there's no message, inBuffer is false and returned pointer is NULL
	 */
	void *Wait(int &size, zmsg::ustring& clientID, void *requestBuffer, uint32_t requestBufferSize, bool& inBuffer);

private:
	zmsg zm;
};

} /* namespace RPC */
} /* namespace DTranx */

#endif /* DTRANX_RPC_ZEROMQRECEIVER_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */
#include <iostream>
#include <cassert>
#include <memory>
#include <unordered_map>
#include "gtest/gtest.h"
#include "Stages/Storage.h"
#include "Server/Server.h"
#include "DTranx/Util/Log.h"
#include "DTranx/Util/ConfigHelper.h"
#include "Util/FileUtil.h"
#include "Service/TranxService.h"
#include "DTranx/Client/ClientTranx.h"
#include "RPC/ClientRPC.h"

using DTranx::uint32;
using DTranx::uint64;
std::string LocalAddress = "127.0.0.1";
std::string ClientPort = "60010";
/*
 * if tests are running with a new nodeID, initialize the tranxStates with allNodes including this new nodeID
 */
/*
void InitStorage(std::shared_ptr<DTranx::Stages::StringStorage> storage) {
	for (int i = 0; i < 10; i++) {
		storage->Write("key" + std::to_string(i), "value" + std::to_string(i));
	}
}

void InitMapping(DTranx::Util::ConfigHelper& configHelper,
					std::shared_ptr<DTranx::SharedResources::MappingDS> mapping) {
	std::string selfAddress = configHelper.read<std::string>("SelfAddress");
	std::unordered_set<std::string> nodeIPs;
	nodeIPs.insert(selfAddress);
	for (int i = 0; i < 10; i++) {
		mapping->UpdateLookupTable("key" + std::to_string(i), nodeIPs);
	}
}

class TranxServiceTest_BasicServer: public ::testing::Test {
public:
	TranxServiceTest_BasicServer() {
		DTranx::Util::Log::setLogPolicy( { { "Tranx", "VERBOSE" }, { "Server", "VERBOSE" } });
		configHelper.readFile("DTranx.conf");
		server = new DTranx::Server::Server(configHelper);

		std::shared_ptr<DTranx::Stages::StringStorage> storage = std::make_shared<
				DTranx::Stages::StringStorage>("dtranx.db");
		InitStorage(storage);
		std::vector<std::string> allNodes;
		allNodes.push_back(configHelper.read<std::string>("SelfAddress"));
		std::shared_ptr<DTranx::SharedResources::MappingDS> mapping = std::make_shared<
				DTranx::SharedResources::MappingDS>(allNodes, "dtranx.mapdb");
		InitMapping(configHelper, mapping);
		server->InitForTest(storage, mapping);
		server->Start();
	}
	~TranxServiceTest_BasicServer() {
		if (server != NULL) {
			delete server;
		}
		DTranx::Util::FileUtil::RemoveDir("dtranx.db");
		DTranx::Util::FileUtil::RemoveDir("dtranx.mapdb");
		DTranx::Util::FileUtil::RemovePrefix("./", "dtranx.log");
		DTranx::Util::FileUtil::RemovePrefix("./", "gcdtranx.log");
	}

	DTranx::Server::Server *server;
	DTranx::Util::ConfigHelper configHelper;

	TranxServiceTest_BasicServer(const TranxServiceTest_BasicServer&) = delete;
	TranxServiceTest_BasicServer& operator=(const TranxServiceTest_BasicServer&) = delete;
};

TEST_F(TranxServiceTest_BasicServer, LocalTranx) {
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(
			configHelper.read<uint32>("maxIOThreads"));
	std::vector<std::string> cluster;
	cluster.push_back(LocalAddress);
	DTranx::Client::ClientTranx *clientTranx = new DTranx::Client::ClientTranx(
			configHelper.read("routerFrontPort"), context, cluster, LocalAddress, ClientPort);
	std::string key("key1"), value;
	clientTranx->Read(key, value);
	EXPECT_EQ("value1", value);
	key = "key1";
	value = "value1_1";
	clientTranx->Write(key, value);
	EXPECT_TRUE(clientTranx->Commit());
	clientTranx->Clear();
	clientTranx->Read(key, value);
	if (clientTranx->Commit()) {
		//it may fail because the locks are released after the server replies to the client
		EXPECT_EQ("value1_1", value);
	}
	delete clientTranx;

	if (server != NULL) {
		delete server;
		server = NULL;
	}
	DTranx::Stages::Log::TranxLog *tranxLog = new DTranx::Stages::Log::TranxLog("./dtranx.log",
			configHelper.read<uint64>("FileTruncate"));
	DTranx::Stages::Log::GTranxLogRecord returnedLog = tranxLog->ReadNext();
	EXPECT_EQ(DTranx::Stages::Log::GLogType::COMMIT_1PC, returnedLog.logtype());
	delete tranxLog;
}

TEST_F(TranxServiceTest_BasicServer, SnapshotTranx) {
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(
			configHelper.read<uint32>("maxIOThreads"));

	DTranx::Client::Client* client = new DTranx::Client::Client(LocalAddress,
			configHelper.read<std::string>("routerFrontPort"), context);
	assert(client->Bind(LocalAddress, ClientPort));

	DTranx::RPC::ClientRPC *clientRPC = new DTranx::RPC::ClientRPC(
			DTranx::RPC::GServiceID::ClientService, client);
	clientRPC->SetClientOpcode(DTranx::Service::GOpCode::CLIENT_COMMIT);
	clientRPC->SetReqIP(LocalAddress);
	clientRPC->SetReqPort(ClientPort);
	clientRPC->GetClientRequest()->mutable_clientcommit()->mutable_request()->set_tranxtype(
			DTranx::Service::GTranxType::SNAPSHOT);
	client->CallRPC(clientRPC);
	assert(client->BlockPoll(clientRPC));
	EXPECT_EQ(DTranx::Service::GStatus::OK,
			clientRPC->GetClientRequest()->clientcommit().response().status());

	delete client;
	delete clientRPC;
}

class TranxServiceTest_BasicServer_clienttranx: public ::testing::Test {
public:
	TranxServiceTest_BasicServer_clienttranx() {
		DTranx::Util::Log::setLogPolicy( { { "Client", "VERBOSE" }, { "Tranx", "VERBOSE" }, { "RPC",
				"VERBOSE" }, { "Server", "VERBOSE" } });
		configHelper.readFile("DTranx.conf");
		server = new DTranx::Server::Server(configHelper);
		/*
		 * server side allNodes is used to initialize peer/tranxstates
		 *
		 * allNodes variable here is used for mapping data structure for both server and tranxservice
		 * and is used for peer/tranxstates at tranx service
		 */
/*
		server->allNodes.push_back(LocalAddress);
		std::vector<std::string> allNodes;
		allNodes.push_back(configHelper.read<std::string>("SelfAddress"));
		allNodes.push_back(LocalAddress);

		std::shared_ptr<DTranx::Stages::StringStorage> serviceStorage = std::make_shared<
				DTranx::Stages::StringStorage>("dtranx1.db");
		std::shared_ptr<DTranx::SharedResources::MappingDS> serviceMapping = std::make_shared<
				DTranx::SharedResources::MappingDS>(allNodes, "dtranx1.mapdb");
		InitStorage(serviceStorage);
		InitMapping(configHelper, serviceMapping);
		server->InitForTest(serviceStorage, serviceMapping);
		server->Start();

		std::unordered_map<int, std::shared_ptr<DTranx::Stages::StringStorage>> tranxStorage;
		tranxStorage[0] = std::make_shared<DTranx::Stages::StringStorage>("dtranx2.db");
		std::shared_ptr<DTranx::SharedResources::MappingDS> tranxMapping = std::make_shared<
				DTranx::SharedResources::MappingDS>(allNodes, "dtranx2.mapdb");
		InitStorage(tranxStorage[0]);
		InitMapping(configHelper, tranxMapping);
		tranxService = new DTranx::Service::TranxService("1", configHelper, tranxStorage,
				tranxMapping, allNodes, NULL,
				DTranx::Tranx::TranxServerInfo::Mode::UNITTEST_CLIENT);

	}
	~TranxServiceTest_BasicServer_clienttranx() {
		delete server;
		delete tranxService;
		DTranx::Util::FileUtil::RemoveDir("dtranx1.db");
		DTranx::Util::FileUtil::RemoveDir("dtranx2.db");
		DTranx::Util::FileUtil::RemoveDir("dtranx1.mapdb");
		DTranx::Util::FileUtil::RemoveDir("dtranx2.mapdb");
		DTranx::Util::FileUtil::RemovePrefix("./", "dtranx.log");
		DTranx::Util::FileUtil::RemovePrefix("./", "gcdtranx.log");
	}

	DTranx::Server::Server *server;
	DTranx::Util::ConfigHelper configHelper;
	DTranx::Service::TranxService *tranxService;

	TranxServiceTest_BasicServer_clienttranx(const TranxServiceTest_BasicServer_clienttranx&) = delete;
	TranxServiceTest_BasicServer_clienttranx& operator=(
			const TranxServiceTest_BasicServer_clienttranx&) = delete;
};

TEST_F(TranxServiceTest_BasicServer_clienttranx, ParticipantPrepare) {
	std::pair<std::vector<DTranx::Service::GItem>, std::vector<DTranx::Service::GItem> > items =
			std::make_pair(std::vector<DTranx::Service::GItem>(), std::vector<DTranx::Service::GItem>());
	DTranx::Service::GItem *item = new DTranx::Service::GItem();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	items.first.push_back(*item);
	item = new DTranx::Service::GItem();
	item->set_key("key2");
	item->set_value("value2_1");
	items.second.push_back(*item);

	std::unordered_map<std::string,
			std::pair<std::vector<DTranx::Service::GItem>, std::vector<DTranx::Service::GItem>> > tmpItems;
	tmpItems["127.0.0.1"] = items;
	EXPECT_TRUE(
			DTranx::Stages::TranxRPCHelperPollingResult::AllOK
					== tranxService->tranxRPCHelper->SendPrepAndPoll("1#1", tmpItems));
}

TEST_F(TranxServiceTest_BasicServer_clienttranx, ParticipantCommit) {
	std::pair<std::vector<DTranx::Service::GItem>, std::vector<DTranx::Service::GItem> > items =
			std::make_pair(std::vector<DTranx::Service::GItem>(), std::vector<DTranx::Service::GItem>());
	DTranx::Service::GItem *item = new DTranx::Service::GItem();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	items.first.push_back(*item);
	item = new DTranx::Service::GItem();
	item->set_key("key2");
	item->set_value("value2_1");
	items.second.push_back(*item);

	std::unordered_map<std::string,
			std::pair<std::vector<DTranx::Service::GItem>, std::vector<DTranx::Service::GItem>> > tmpItems;
	tmpItems["127.0.0.1"] = items;
	std::vector<std::string> nodes;
	for (auto it = tmpItems.begin(); it != tmpItems.end(); ++it) {
		nodes.push_back(it->first);
	}
	EXPECT_TRUE(
			DTranx::Stages::TranxRPCHelperPollingResult::AllOK
					== tranxService->tranxRPCHelper->SendPrepAndPoll("2#1", tmpItems));
	EXPECT_TRUE(
			DTranx::Stages::TranxRPCHelperPollingResult::AllOK
					== tranxService->tranxRPCHelper->SendCommitAndPoll("2#1", nodes));
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(
			configHelper.read<uint32>("maxIOThreads"));
	std::vector<std::string> cluster;
	cluster.push_back("127.0.0.1");
	DTranx::Client::ClientTranx *clientTranx = new DTranx::Client::ClientTranx(
			configHelper.read("routerFrontPort"), context, cluster, LocalAddress, ClientPort);
	std::string key, value;
	key = "key2";
	clientTranx->Read(key, value);
	EXPECT_EQ("value2_1", value);
	delete clientTranx;
}

TEST_F(TranxServiceTest_BasicServer_clienttranx, ParticipantAbort) {
	std::pair<std::vector<DTranx::Service::GItem>, std::vector<DTranx::Service::GItem> > items =
			std::make_pair(std::vector<DTranx::Service::GItem>(), std::vector<DTranx::Service::GItem>());
	DTranx::Service::GItem *item = new DTranx::Service::GItem();
	item->set_key("key1");
	item->set_value("value1");
	item->set_version(1);
	items.first.push_back(*item);
	item = new DTranx::Service::GItem();
	item->set_key("key2");
	item->set_value("value2_1");
	items.second.push_back(*item);

	std::unordered_map<std::string,
			std::pair<std::vector<DTranx::Service::GItem>, std::vector<DTranx::Service::GItem>> > tmpItems;
	tmpItems["127.0.0.1"] = items;
	std::vector<std::string> nodes;
	for (auto it = tmpItems.begin(); it != tmpItems.end(); ++it) {
		nodes.push_back(it->first);
	}
	EXPECT_TRUE(
			DTranx::Stages::TranxRPCHelperPollingResult::AllOK
					== tranxService->tranxRPCHelper->SendAbortAndPoll("2#1", nodes));

	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(
			configHelper.read<uint32>("maxIOThreads"));
	std::vector<std::string> cluster;
	cluster.push_back("127.0.0.1");
	DTranx::Client::ClientTranx *ClientTranx = new DTranx::Client::ClientTranx(
			configHelper.read("routerFrontPort"), context, cluster, LocalAddress, ClientPort);
	std::string key, value;
	key = "key2";
	ClientTranx->Read(key, value);
	EXPECT_EQ("value2", value);
}
*/

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * TranxID is implemented to save memory allocation time and avoid dynamically allocated std::string
 */

#ifndef DTRANX_UTIL_TRANXID_H_
#define DTRANX_UTIL_TRANXID_H_

#include <functional>
#include "DTranx/Util/types.h"

namespace DTranx {
namespace Util {

class TranxID {
public:
	explicit TranxID(int nodeID, uint64_t tranxID);
	TranxID();
	virtual ~TranxID();

	/*
	 * constructors
	 */
	TranxID(const TranxID& other);

	/*
	 * operators overload
	 */
	void operator=(const TranxID& other);
	bool operator==(const TranxID& other) const;
	bool operator!=(const TranxID& other) const;
	bool operator<(const TranxID& other) const;

	/*
	 * access methods
	 */
	int GetNodeID() const {
		return nodeID;
	}

	void SetNodeID(int nodeID) {
		this->nodeID = nodeID;
	}

	uint64_t GetTranxID() const {
		return tranxID;
	}

	void SetTranxID(uint64_t tranxID) {
		this->tranxID = tranxID;
	}

	bool empty() {
		if (nodeID == -1) {
			return true;
		}
		return false;
	}

	void Clear() {
		nodeID = -1;
		tranxID = 0;
	}

private:
	int nodeID;
	uint64_t tranxID;
};

struct KeyHasher {
	std::size_t operator()(const TranxID& k) const {
		using std::size_t;
		using std::hash;

		return ((hash<int>()(k.GetNodeID()) ^ (hash<uint64_t>()(k.GetTranxID()) << 1)) >> 1);
	}
};

} /* namespace Util */
} /* namespace DTranx */

#endif /* DTRANX_UTIL_TRANXID_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "TranxServiceQueueElement.h"
#include "gtest/gtest.h"

using namespace DTranx;
TEST(tranxservicequeuelement, timepoint){
	Stages::TranxServiceQueueElement element;
	EXPECT_EQ(0, element.getLockStartTimePoint().time_since_epoch().rep_);
}

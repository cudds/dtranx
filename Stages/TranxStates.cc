/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "TranxStates.h"
#include "DaemonStages/GarbageCollector.h"
#include "Service/TranxService.h"
#include "Service/StorageService.h"
#include "Service/ClientService.h"
#include "DaemonStages/Repartition.h"
#include "DaemonStages/TranxAck.h"
#include "DaemonStages/DaemonStagesMock.h"
#include "Server/RecoverLog.h"

namespace DTranx {
namespace Stages {

TranxStates::TranxStates(SharedResources::TranxServerInfo *tranxServerInfo, int numOfStageThreads)
		: MidStage(tranxServerInfo, numOfStageThreads, "TranxStates"),
				configHelper(tranxServerInfo->GetConfig()),
				gcFactory(tranxServerInfo->GetNodeID(),
						configHelper.read<uint64>("GCBroadcastTranxThreshold"),
						configHelper.read<uint64>("GCBroadcastTimeThreshold")) {
	selfNodeID = tranxServerInfo->GetNodeID();
	allStates.push_back(std::unordered_map<Util::TranxID, State, Util::KeyHasher>());
	for (int _nodeID = 1; _nodeID <= tranxServerInfo->GetAllNodes().size(); ++_nodeID) {
		VERBOSE("initializing tranxstate for nodeid %d", _nodeID);
		allStates.push_back(std::unordered_map<Util::TranxID, State, Util::KeyHasher>());
		allStates.back().reserve(1000);
	}
	if (tranxServerInfo->GetAllNodes().empty()) {
		assert(tranxServerInfo->GetNodeID() == 1);
	}
	if (numOfStageThreads > allStates.size()) {
		numOfStageThreads = allStates.size();
	}
}

TranxStates::~TranxStates() noexcept(false) {
}

State TranxStates::CheckState(const Util::TranxID& tranxID) {
	/*
	 * might be in both gc and commit/abort/ack, return gc in this case
	 */
	int nodeID = tranxID.GetNodeID();

	if (gcFactory.IsOutdated(nodeID, tranxID.GetTranxID())) {
		return State::GC;
	}
	if (allStates[nodeID].find(tranxID) == allStates[nodeID].end()) {
		return State::NOTEXIST;
	}
	return allStates[nodeID][tranxID];
}

State TranxStates::CheckAndSetReadyState(const Util::TranxID& tranxID) {
	//VERBOSE("CheckAndSetReadyState for tranx %s", tranxID.c_str());
	/*
	 * if it's not in ready/commit/abort/ack/gc/migration states, then set ready
	 */
	int nodeID = tranxID.GetNodeID();
	if (gcFactory.IsOutdated(nodeID, tranxID.GetTranxID())) {
		return State::GC;
	}

	if (allStates[nodeID].find(tranxID) != allStates[nodeID].end()) {
		return allStates[nodeID][tranxID];
	}
	allStates[nodeID][tranxID] = State::READY;
	return State::NOTEXIST;
}

State TranxStates::CheckAndSetCommitState(const Util::TranxID& tranxID) {
	//VERBOSE("CheckAndSetCommitState for tranx %s", tranxID.c_str());
	/*
	 * if it's not in commit/gc, then set commit
	 * then previous state should be ready
	 * only used in participants, so not in ack
	 */
	int nodeID = tranxID.GetNodeID();
	if (gcFactory.IsOutdated(nodeID, tranxID.GetTranxID())) {
		return State::GC;
	}
	assert(allStates[nodeID].find(tranxID) != allStates[nodeID].end());
	if (allStates[nodeID][tranxID] == State::COMMIT) {
		return State::COMMIT;
	}
	if (allStates[nodeID][tranxID] != State::READY
			&& allStates[nodeID][tranxID] != State::MIGRATION) {
		VERBOSE("CheckAndSetCommitState error, prev state is %d", allStates[nodeID][tranxID]);
		assert(false);
	}
	allStates[nodeID][tranxID] = State::COMMIT;
	return State::READY;
}

void TranxStates::SetToCommitState(const Util::TranxID& tranxID) {
	//VERBOSE("SetToCommitStateBeforeLog for tranx %s", tranxID.c_str());
	int nodeID = tranxID.GetNodeID();
	assert(allStates[nodeID].find(tranxID) == allStates[nodeID].end());
	allStates[nodeID][tranxID] = State::COMMIT;
}

State TranxStates::CheckAndSetAbortState(const Util::TranxID& tranxID) {
	//VERBOSE("CheckAndSetAbortStateBeforeLog for tranx %s", tranxID.c_str());
	int nodeID = tranxID.GetNodeID();
	if (gcFactory.IsOutdated(nodeID, tranxID.GetTranxID())) {
		return State::GC;
	}
	if (allStates[nodeID].find(tranxID) == allStates[nodeID].end()) {
		allStates[nodeID][tranxID] = State::ABORT;
		return State::NOTEXIST;
	}
	State previousState = allStates[nodeID][tranxID];
	if (previousState == State::READY) {
		allStates[nodeID][tranxID] = State::ABORT;
	} else if (previousState == State::ABORT) {

	} else {
		assert(false);
	}
	return previousState;
}

void TranxStates::SetToAbortState(const Util::TranxID& tranxID) {
	//VERBOSE("SetToAbortStateBeforeLog for tranx %s", tranxID.c_str());
	int nodeID = tranxID.GetNodeID();
	assert(allStates[nodeID].find(tranxID) == allStates[nodeID].end());
	allStates[nodeID][tranxID] = State::ABORT;
}

State TranxStates::CheckAndSetMigrateState(const Util::TranxID& tranxID) {
	//if it's not in gc/commit/migrate, then set migrate
	int nodeID = tranxID.GetNodeID();
	if (gcFactory.IsOutdated(nodeID, tranxID.GetTranxID())) {
		return State::GC;
	}
	if (allStates[nodeID].find(tranxID) != allStates[nodeID].end()) {
		if (allStates[nodeID][tranxID] == State::COMMIT) {
			return State::COMMIT;
		} else if (allStates[nodeID][tranxID] == State::MIGRATION) {
			return State::MIGRATION;
		} else if (allStates[nodeID][tranxID] == State::READY) {
			allStates[nodeID][tranxID] = State::MIGRATION;
			return State::READY;
		} else {
			assert(false);
		}
	}
	allStates[nodeID][tranxID] = State::MIGRATION;
	return State::NOTEXIST;
}

void TranxStates::SetToAck(const Util::TranxID& tranxID, bool isCommit) {
	int nodeID = tranxID.GetNodeID();
	assert(selfNodeID == nodeID);
	gcFactory.AddTranxID(tranxID.GetTranxID());
	allStates[selfNodeID].erase(tranxID);
	if (isCommit && repartition != NULL) {
		repartition->SendToCommitHistory(tranxID);
	}
}

void TranxStates::UpdateGC(int nodeID, uint64_t tranxID) {
	gcFactory.UpdateEarliest(nodeID, tranxID);
}

void TranxStates::CleanStateFromGCForOtherNodes(int nodeID) {
	std::unordered_set<Util::TranxID, Util::KeyHasher> toRemove;
	for (auto it = allStates[nodeID].begin(); it != allStates[nodeID].end(); ++it) {
		if (gcFactory.IsOutdated(nodeID, it->first.GetTranxID())) {
			toRemove.insert(it->first);
		}
	}
	for (auto it = toRemove.begin(); it != toRemove.end(); ++it) {
		allStates[nodeID].erase(*it);
	}
	if (!toRemove.empty()) {
		VERBOSE("TranxStates clean state for nodeID %d", nodeID);
	}
}

GCFactory TranxStates::GetGCFactory() {
	GCFactory gcFactoryTmp(selfNodeID, configHelper.read<uint64>("GCBroadcastTranxThreshold"),
			configHelper.read<uint64>("GCBroadcastTimeThreshold"));
	for (int i = 1; i < allStates.size(); ++i) {
		gcFactoryTmp.UpdateEarliest(i, gcFactory.GetEarliest(i));
	}
	return gcFactoryTmp;
}

bool TranxStates::GCToBroadcast() {
	return gcFactory.ToBroadcast();
}

void TranxStates::DispatchThread() {
	NOTICE("TranxStateThread Dispatch started");
	uint64_t randSeed = 0;
	boost::chrono::steady_clock::time_point time1, time2;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateThread.load()) {
			NOTICE("TranxStateThread Dispatch is reclaimed");
			break;
		}
		TranxServiceQueueElement *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = dispatchQueue.try_dequeue_bulk(tmpRPCs,
				TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1) {
				AddQueueLengthSample(5, dispatchQueue.size_approx());
			}
		}

		for (int i = 0; i < count; ++i) {
			TranxServiceQueueElement *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(5);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(5);
				std::vector<int> taskLongLatencySamples = GetLongLatency(5);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_dispatch");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				std::set<int> involvedStageThreads;
				for (int i = 1; i < numOfStageThreads; ++i) {
					involvedStageThreads.insert(i);
				}
				element->setSplittedInvolvedStageThreads(involvedStageThreads);
				element->setTerminateInThisStage(true);
				SendToStage(element, 0);
				continue;
			}
			//element->goThroughStage("tranxstates dispatch");
			TranxStatesStageTask task = element->getStateTask();
			const Util::TranxID& tranxID = element->GetTranxID();
			LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
			if (task == TranxStatesStageTask::_CheckState) {
				SendToStage(element, GetThreadByNodeID(tranxID.GetNodeID()));
			} else if (task == TranxStatesStageTask::_CheckAndSetReadyState) {
				SendToStage(element, GetThreadByNodeID(tranxID.GetNodeID()));
			} else if (task == TranxStatesStageTask::_CheckAndSetCommitState) {
				SendToStage(element, GetThreadByNodeID(tranxID.GetNodeID()));
			} else if (task == TranxStatesStageTask::_CheckAndSetAbortState) {
				SendToStage(element, GetThreadByNodeID(tranxID.GetNodeID()));
			} else if (task == TranxStatesStageTask::_CheckAndSetMigrateState) {
				SendToStage(element, GetThreadByNodeID(tranxID.GetNodeID()));
			} else if (task == TranxStatesStageTask::_UpdateGC) {
				assert(element->GetServerRPC() != NULL);
				int nodeID = element->GetServerRPC()->GetTranxRequest()->mutable_gc()->nodeid();
				SendToStage(element, GetThreadByNodeID(nodeID));
			} else if (task == TranxStatesStageTask::_SetToCommitState) {
				SendToStage(element, GetThreadByNodeID(tranxID.GetNodeID()));
			} else if (task == TranxStatesStageTask::_SetToAbortState) {
				SendToStage(element, GetThreadByNodeID(tranxID.GetNodeID()));
			} else if (task == TranxStatesStageTask::_SetToAck) {
				SendToStage(element, GetThreadByNodeID(tranxID.GetNodeID()));
			} else if (task == TranxStatesStageTask::_GarbageCollectAck) {
				SendToStage(element, GetThreadByNodeID(selfNodeID));
			} else {
				assert(false);
			}
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void TranxStates::StageThread(int stageThreadIndex) {
	NOTICE("TranxStateThread %d started", stageThreadIndex);
	uint64_t randSeed = 0;
	uint32_t prevNoTask = 0;
	boost::chrono::steady_clock::time_point potentialFreeStartTime;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateThread.load()) {
			NOTICE("TranxStateThread %d is reclaimed", stageThreadIndex);
			break;
		}
		TranxServiceQueueElement *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = 0;

		if (numOfStageThreads > 1) {
			count = processQueues[stageThreadIndex].try_dequeue_bulk(tmpRPCs,
					TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
			if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
				if (randSeed++ % queuePerfFreq == 1) {
					AddQueueLengthSample(stageThreadIndex,
							processQueues[stageThreadIndex].size_approx());
				}
			}
		} else {
			count = processQueue.try_dequeue_bulk(tmpRPCs,
					TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
			if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
				if (randSeed++ % queuePerfFreq == 1) {
					AddQueueLengthSample(0, processQueue.size_approx());
				}
			}
		}

		if (count < 2) {
			/*
			 * the reason for 2 because gc stage will periodically polling for broadcast
			 */
			prevNoTask++;
			if (prevNoTask == 1000) {
				potentialFreeStartTime = boost::chrono::steady_clock::now();
			} else if (prevNoTask > 1000 && prevNoTask % 1000 == 1) {
				boost::chrono::steady_clock::time_point now = boost::chrono::steady_clock::now();
				uint64 timeElapsed = boost::chrono::duration_cast<boost::chrono::seconds>(
						now - potentialFreeStartTime).count();
				if (timeElapsed >= 5) {
					for (int i = 1; i < allStates.size(); ++i) {
						if (i % numOfStageThreads == stageThreadIndex && i != selfNodeID) {
							CleanStateFromGCForOtherNodes(i);
						}
					}
					prevNoTask = 0;
				}
			}

		} else {
			prevNoTask = 0;
		}

		for (int i = 0; i < count; ++i) {
			TranxServiceQueueElement *element = tmpRPCs[i];
			//element->goThroughStage("tranxstates" + std::to_string(stageThreadIndex));
			TranxStatesStageTask task = element->getStateTask();
			const Util::TranxID& tranxID = element->GetTranxID();
			//VERBOSE("%d#%lu leaving TranxStateStage0", tranxID.GetNodeID(), tranxID.GetTranxID());
			if (element->isIsPerfOutPutTask()) {
				/*
				 * the setTerminate is necessary because it might've skipped the dispatch because the numOfStageThreads is 1
				 */
				element->setTerminateInThisStage(true);
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(
						stageThreadIndex);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(
						stageThreadIndex);
				std::vector<int> taskLongLatencySamples = GetLongLatency(stageThreadIndex);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(
						stageName + std::to_string(stageThreadIndex));
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
			} else {
				if (task == TranxStatesStageTask::_CheckState) {
					State state = CheckState(tranxID);
					element->setState(state);
				} else if (task == TranxStatesStageTask::_CheckAndSetReadyState) {
					State state = CheckAndSetReadyState(tranxID);
					element->setState(state);
				} else if (task == TranxStatesStageTask::_CheckAndSetCommitState) {
					State state = CheckAndSetCommitState(tranxID);
					element->setState(state);
				} else if (task == TranxStatesStageTask::_CheckAndSetAbortState) {
					State state = CheckAndSetAbortState(tranxID);
					element->setState(state);
				} else if (task == TranxStatesStageTask::_CheckAndSetMigrateState) {
					State state = CheckAndSetMigrateState(tranxID);
					element->setState(state);
				} else if (task == TranxStatesStageTask::_UpdateGC) {
					assert(element->GetServerRPC() != NULL);
					UpdateGC(element->GetServerRPC()->GetTranxRequest()->mutable_gc()->nodeid(),
							element->GetServerRPC()->GetTranxRequest()->mutable_gc()->tranxid());
				} else if (task == TranxStatesStageTask::_SetToCommitState) {
					SetToCommitState(tranxID);
				} else if (task == TranxStatesStageTask::_SetToAbortState) {
					SetToAbortState(tranxID);
				} else if (task == TranxStatesStageTask::_SetToAck) {
					SetToAck(tranxID, element->isStateAckCommitAbort());
				} else if (task == _GarbageCollectAck) {
					element->setStateGcFactory(GetGCFactory());
					element->setStateToBroadcast(GCToBroadcast());
				} else {
					assert(false);
				}
			}

			/*
			 * first check if there are still some stage threads that this element should go through
			 * 	if complete, send back to the daemonstage/service
			 */
			int nextStageThread = element->popSplittedNextInvolvedStageThreads();
			if (nextStageThread != TranxServiceQueueElement::INVALID_STAGE_THREADID) {
				//VERBOSE("%d#%lu leaving TranxStateStage1", tranxID.GetNodeID(), tranxID.GetTranxID());
				LeaveStage(element, stageThreadIndex, task);
				SendToStage(element, nextStageThread);
			} else {
				//VERBOSE("%d#%lu leaving TranxStateStage2", tranxID.GetNodeID(), tranxID.GetTranxID());
				TranxServiceQueueElement::StageID stageID = element->getStageId();
				if (element->isTerminateInThisStage()) {
					//VERBOSE("Deleting element/stoping stages in TranxStates for tranx %s", element->GetTranxID().c_str());
					if (stageID == Stages::TranxServiceQueueElement::StageID::TRANXSERVICE) {
						tranxService->AddToFreeList(element);
					} else if (stageID
							== Stages::TranxServiceQueueElement::StageID::CLIENTSERVICE) {
						clientService->AddToFreeList(element);
					} else if (stageID
							== Stages::TranxServiceQueueElement::StageID::STORAGESERVICE) {
						storageService->AddToFreeList(element);
					} else if (stageID == Stages::TranxServiceQueueElement::StageID::TRANXACK) {
						/*
						 * tranxAck elements were originally created in ClientService
						 */
						clientService->AddToFreeList(element);
					} else {
						delete element;
					}
					continue;
				}
				LeaveStage(element, stageThreadIndex, task);
				if (stageID == Stages::TranxServiceQueueElement::StageID::TRANXSERVICE) {
					tranxService->SendToServiceInternal(element);
				} else if (stageID == Stages::TranxServiceQueueElement::StageID::CLIENTSERVICE) {
					clientService->SendToServiceInternal(element);
				} else if (stageID == Stages::TranxServiceQueueElement::StageID::STORAGESERVICE) {
					storageService->SendToServiceInternal(element);
				} else if (stageID == Stages::TranxServiceQueueElement::StageID::TRANXACK) {
					tranxAck->SendToDaemon(element);
				} else if (stageID == Stages::TranxServiceQueueElement::StageID::GCTHREAD) {
					gc->SendToDaemon(element);
				} else if (stageID == Stages::TranxServiceQueueElement::StageID::REPARTITION) {
					repartition->SendToDaemon(element);
				} else if (stageID == Stages::TranxServiceQueueElement::StageID::RECOVERY) {
					postRecovery->SendToDaemon(element);
				} else if (stageID == Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON) {
					daemonStagesMock->SendToDaemon(element);
				} else {
					assert(false);
				}
			}
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

} /* namespace Stages */
} /* namespace DTranx */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef _DTRANX_UTIL_NOTFOUNDEXCEPTION_H_
#define _DTRANX_UTIL_NOTFOUNDEXCEPTION_H_

#include <exception>
#include <string>
namespace DTranx {
namespace Util {

class NotFoundException: public std::exception {
public:
	NotFoundException(std::string m)
			: msg(m) {
	}
	~NotFoundException() throw () {
	}
	const char* what() const throw () {
		return msg.c_str();
	}

private:
	std::string msg;
};

} /* namespace Util */
} /* namespace DTranx */

#endif /* _DTRANX_UTIL_NOTFOUNDEXCEPTION_H_ */

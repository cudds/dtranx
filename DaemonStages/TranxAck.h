/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * TranxAck reads committed/aborted tranx's in the coordinator and send commit/abort request to
 * 	the participants after it's committed at the coordinator.
 * It also integrates a threadpool design to increase concurrency
 *
 * It's used by TranxService and TranxRepartition classes
 */

#ifndef DTRANX_DAEMONSTAGES_TRANXACK_H_
#define DTRANX_DAEMONSTAGES_TRANXACK_H_

#include <unordered_set>
#include <condition_variable>
#include "Stages/TranxServiceQueueElement.h"
#include "DaemonStages.h"

namespace DTranx {
namespace DaemonStages {

class TranxAck: public DaemonStages {
public:
	TranxAck(SharedResources::TranxServerInfo *tranxServerInfo,
			SharedResources::TranxServiceSharedData *tranxServiceSharedData);
	virtual ~TranxAck() noexcept(false);

	virtual void DaemonThread();

	virtual void StartService(std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) {
		bool enableCoreBinding = false;
		uint32_t coreIDIndex = 0;
		enableCoreBinding = (coreIDs.size() == 1);
		daemonThread = boost::thread(&TranxAck::DaemonThread, this);
		if (enableCoreBinding) {
			Util::ThreadHelper::PinToCPUCore(daemonThread, coreIDs[coreIDIndex++]);
		}
	}

private:
	/* class members
	 *
	 * snapshotTranx/repartitionTranx are used only to avoid duplicate commit/abort sending
	 */
	std::string nodeID;

	void ProcessAck(Stages::TranxServiceQueueElement *element);
};

} /* namespace DaemonStages */
} /* namespace DTranx */

#endif /* DTRANX_DAEMONSTAGES_TRANXACK_H_ */

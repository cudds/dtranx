/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "WriteBlockLockTable.h"
#include "gtest/gtest.h"
#include "Util/FileUtil.h"
#include "DaemonStages/DaemonStagesMock.h"

using namespace DTranx;

class WriteBlockLockTest: public ::testing::Test {
public:
	WriteBlockLockTest() {
		Util::ConfigHelper configHelper;
		configHelper.readFile("DTranx.conf");
		tranxServerInfo = new SharedResources::TranxServerInfo();
		tranxServerInfo->SetConfig(configHelper);
		lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo);
	}
	~WriteBlockLockTest() {
		delete lockTable;
		delete tranxServerInfo;
	}
private:
	Stages::Lock::WriteBlockLockTable *lockTable;
	DTranx::SharedResources::TranxServerInfo *tranxServerInfo;
};

/*
 * Basic tests are already done in BasicLockTable,
 * here batch request will be tested
 */
TEST_F(WriteBlockLockTest, lockmap) {
	std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key3"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	Util::TranxID tranxID1(1, 1);
	Util::TranxID tranxID2(2, 1);
	Util::TranxID tranxID3(3, 1);
	EXPECT_TRUE(lockTable->RequestLocks(keysMode, tranxID1));
	/*
	 * key1: 1#1 shared
	 * key2: 1#1 shared
	 * key3: 1#1 ex
	 */
	keysMode.clear();
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key5"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	EXPECT_TRUE(lockTable->RequestLocks(keysMode, tranxID2));
	/*
	 * key1: 1#1 shared, 2#1 shared
	 * key2: 1#1 shared, 2#1 shared
	 * key3: 1#1 ex
	 * key5: 2#1 ex
	 */
	keysMode.clear();
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key5"] = Stages::Lock::BasicLockTableLockMode::Shared;
	EXPECT_FALSE(lockTable->RequestLocks(keysMode, tranxID3));
	/*
	 * key1: 1#1 shared, 2#1 shared
	 * key2: 1#1 shared, 2#1 shared, 3#1 shared
	 * key3: 1#1 ex
	 * key5: 2#1 ex
	 */
	keysMode.clear();
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key3"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	EXPECT_TRUE(lockTable->ReleaseLocks(keysMode, tranxID1));
	/*
	 * key1: 2#1 shared
	 * key2: 2#1 shared, 3#1 shared
	 * key5: 2#1 ex
	 */
	keysMode.clear();
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key5"] = Stages::Lock::BasicLockTableLockMode::Shared;
	EXPECT_FALSE(lockTable->RequestLocks(keysMode, tranxID3));
	/*
	 * key1: 2#1 shared
	 * key2: 2#1 shared, 3#1 shared
	 * key5: 2#1 ex
	 */
	keysMode.clear();
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key5"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	EXPECT_TRUE(lockTable->ReleaseLocks(keysMode, tranxID2));
	/*
	 * key2: 3#1 shared
	 * key5: 3#1 shared
	 */
	keysMode.clear();
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key5"] = Stages::Lock::BasicLockTableLockMode::Shared;
	EXPECT_TRUE(lockTable->RequestLocks(keysMode, tranxID3));
}

class WriteBlockLockTest_Stage: public ::testing::Test {
public:
	WriteBlockLockTest_Stage() {
		Util::ConfigHelper configHelper;
		configHelper.readFile("DTranx.conf");
		tranxServerInfo = new SharedResources::TranxServerInfo();
		tranxServerInfo->SetConfig(configHelper);
		std::vector<std::string> ips;
		ips.push_back("192.168.0.1");
		ips.push_back("192.168.0.2");
		ips.push_back("192.168.0.3");
		tranxServerInfo->SetAllNodes(ips);
		tranxServerInfo->SetNodeID(2);
		tranxServerInfo->SetMode(SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER);
		tranxServiceSharedData = new SharedResources::TranxServiceSharedData(2, tranxServerInfo);
		lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo);
		daemonStages = new DaemonStages::DaemonStagesMock(tranxServerInfo, tranxServiceSharedData);
		lockTable->daemonStagesMock = daemonStages;
		lockTable->StartStage();
	}
	~WriteBlockLockTest_Stage() {
		lockTable->ShutStage();
		delete lockTable;
		delete tranxServerInfo;
		delete tranxServiceSharedData;
		delete daemonStages;
		Util::FileUtil::RemoveDir("dtranx.mapdb");
	}

	Stages::Lock::WriteBlockLockTable *lockTable;
	DaemonStages::DaemonStagesMock *daemonStages;
	SharedResources::TranxServiceSharedData *tranxServiceSharedData;
	SharedResources::TranxServerInfo *tranxServerInfo;
private:
	WriteBlockLockTest_Stage(const WriteBlockLockTest_Stage&) = delete;
	WriteBlockLockTest_Stage& operator=(const WriteBlockLockTest_Stage&) = delete;
};

TEST_F(WriteBlockLockTest_Stage, _REQUESTLOCKSMAP) {
	std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	Stages::TranxServiceQueueElement element;
	element.setKeysMode(keysMode);
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSMAP);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_TRUE(element.isLockObtained());

	keysMode.clear();
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Shared;
	element.setKeysMode(keysMode);
	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSMAP);
	Util::TranxID tranxID2(1, 2);
	element.SetTranxID(tranxID2);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_FALSE(element.isLockObtained());
}

TEST_F(WriteBlockLockTest_Stage, _PRINTLOGSTATUS) {
	std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	Stages::TranxServiceQueueElement element;
	element.setKeysMode(keysMode);
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSMAP);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_TRUE(element.isLockObtained());

	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_PRINTLOCKSTATUS);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	std::cout << element.getLockStatus() << std::endl;
}

TEST_F(WriteBlockLockTest_Stage, _RELEASELOCKSVECTOR) {
	std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	Stages::TranxServiceQueueElement element;
	element.setKeysMode(keysMode);
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSMAP);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_TRUE(element.isLockObtained());

	std::vector<std::string> keys;
	keys.push_back("key1");
	keys.push_back("key2");
	element.setKeys(keys);
	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSVECTOR);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());

	keysMode.clear();
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Shared;
	element.setKeysMode(keysMode);
	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSMAP);
	Util::TranxID tranxID2(1, 2);
	element.SetTranxID(tranxID2);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_TRUE(element.isLockObtained());
}

TEST_F(WriteBlockLockTest_Stage, _REQUESTLOCKSBLOCKING) {
	std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	Stages::TranxServiceQueueElement element;
	element.setKeysMode(keysMode);
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSMAP);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_TRUE(element.isLockObtained());

	Stages::TranxServiceQueueElement element2;
	keysMode.clear();
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	element2.setKeysMode(keysMode);
	element2.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element2.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSBLOCKING);
	Util::TranxID tranxID2(1, 2);
	element2.SetTranxID(tranxID2);
	lockTable->SendToStage(&element2);

	sleep(1);

	std::vector<std::string> keys;
	keys.push_back("key1");
	keys.push_back("key2");
	element.setKeys(keys);
	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSVECTOR);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());

	EXPECT_EQ(&element2, daemonStages->WaitForResult());
	EXPECT_TRUE(element2.isLockObtained());
}

class WriteBlockLockTest_Stage_MultiThreads: public ::testing::Test {
public:
	WriteBlockLockTest_Stage_MultiThreads() {
		Util::Log::setLogPolicy( { { "Stages", "PROFILE" } });
		Util::ConfigHelper configHelper;
		configHelper.readFile("DTranx.conf");
		tranxServerInfo = new SharedResources::TranxServerInfo();
		tranxServerInfo->SetConfig(configHelper);
		std::vector<std::string> ips;
		ips.push_back("192.168.0.1");
		ips.push_back("192.168.0.2");
		ips.push_back("192.168.0.3");
		tranxServerInfo->SetAllNodes(ips);
		tranxServerInfo->SetNodeID(2);
		tranxServerInfo->SetMode(SharedResources::TranxServerInfo::Mode::UNITTEST_SERVER);
		tranxServiceSharedData = new SharedResources::TranxServiceSharedData(2, tranxServerInfo);
		lockTable = new Stages::Lock::WriteBlockLockTable(tranxServerInfo, 2);
		daemonStages = new DaemonStages::DaemonStagesMock(tranxServerInfo, tranxServiceSharedData);
		lockTable->daemonStagesMock = daemonStages;
		lockTable->StartStage();
	}
	~WriteBlockLockTest_Stage_MultiThreads() {
		lockTable->ShutStage();
		delete lockTable;
		delete tranxServerInfo;
		delete tranxServiceSharedData;
		delete daemonStages;
		Util::FileUtil::RemoveDir("dtranx.mapdb");
		Util::Log::setLogPolicy( { { "Stages", "NOTICE" } });
	}

	Stages::Lock::WriteBlockLockTable *lockTable;
	DaemonStages::DaemonStagesMock *daemonStages;
	SharedResources::TranxServiceSharedData *tranxServiceSharedData;
	SharedResources::TranxServerInfo *tranxServerInfo;
private:
	WriteBlockLockTest_Stage_MultiThreads(const WriteBlockLockTest_Stage_MultiThreads&) = delete;
	WriteBlockLockTest_Stage_MultiThreads& operator=(const WriteBlockLockTest_Stage_MultiThreads&) = delete;
};

TEST_F(WriteBlockLockTest_Stage_MultiThreads, _REQUESTLOCKSMAP) {
	std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	Stages::TranxServiceQueueElement element;
	element.setKeysMode(keysMode);
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSMAP);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_TRUE(element.isLockObtained());

	keysMode.clear();
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Shared;
	element.setKeysMode(keysMode);
	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSMAP);
	Util::TranxID tranxID2(1, 2);
	element.SetTranxID(tranxID2);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_FALSE(element.isLockObtained());
}

TEST_F(WriteBlockLockTest_Stage_MultiThreads, _RELEASELOCKSVECTOR) {
	std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysMode;
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Shared;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	Stages::TranxServiceQueueElement element;
	element.setKeysMode(keysMode);
	element.setStageId(Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON);
	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSMAP);
	Util::TranxID tranxID(1, 1);
	element.SetTranxID(tranxID);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_TRUE(element.isLockObtained());

	std::vector<std::string> keys;
	keys.push_back("key1");
	keys.push_back("key2");
	element.setKeys(keys);
	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_RELEASELOCKSVECTOR);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());

	keysMode.clear();
	keysMode["key1"] = Stages::Lock::BasicLockTableLockMode::Exclusive;
	keysMode["key2"] = Stages::Lock::BasicLockTableLockMode::Shared;
	element.setKeysMode(keysMode);
	element.setLockTask(Stages::Lock::WriteBlockLockTableStageTask::_REQUESTLOCKSMAP);
	Util::TranxID tranxID2(1, 2);
	element.SetTranxID(tranxID2);
	lockTable->SendToStage(&element);
	EXPECT_EQ(&element, daemonStages->WaitForResult());
	EXPECT_TRUE(element.isLockObtained());
}

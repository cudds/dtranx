/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * run clients using workloads in the directory
 * each file in the directory is a separate workload for one thread
 * each file is of format: read/write key1 key2 after key3 value3 key4 value4...
 */

#include <getopt.h>
#include <fstream>
#include <boost/thread.hpp>
#include "DTranx/Client/ClientTranx.h"
#include "DTranx/Util/ConfigHelper.h"
#include "DTranx/Util/Log.h"
#include "Util/StringUtil.h"
#include "Util/FileUtil.h"
using namespace DTranx;

/**
 * Parses argv for the main function.
 */
class OptionParser {
public:
	OptionParser(int& argc, char**& argv)
			: argc(argc), argv(argv), clusterFile() {
		workload = "";
		while (true) {
			static struct option longOptions[] = { { "workload",
			required_argument, NULL, 'w' }, { "cluster",
			required_argument, NULL, 'c' }, { "self", required_argument, NULL, 's' }, { "help",
			no_argument, NULL, 'h' }, { 0, 0, 0, 0 } };
			int c = getopt_long(argc, argv, "w:c:h", longOptions, NULL);

			// Detect the end of the options.
			if (c == -1)
				break;

			switch (c) {
				case 'w':
					workload = optarg;
					break;
				case 'c':
					clusterFile = optarg;
					break;
				case 's':
					selfAddress = optarg;
					break;
				case 'h':
					usage();
					exit(0);
				case '?':
				default:
					// getopt_long already printed an error message.
					usage();
					exit(1);
			}
		}
	}

	void usage() {
		std::cout << "Usage: " << argv[0] << " [options]" << std::endl;
		std::cout << "Options: " << std::endl;
		std::cout << "  -w, --workload (dirname) "
				<< "dtranx workload directory, line format read/update key1 key2 after key3 value3 key4 value4"
				<< std::endl;
		std::cout << "  -c, --cluster (filename) " << "dtranx cluster, line format ip"
				<< "(optional argument, default localhost)" << std::endl;
		std::cout << "  -h, --help              " << "Print this usage information" << std::endl;
	}

	int& argc;
	char**& argv;
	std::string workload;
	std::string clusterFile;
	std::string selfAddress;
};

struct Tranx {
	bool isRead;
	std::vector<std::string> reads;
	std::unordered_map<std::string, std::string> writes;
};

struct Workload {
	std::vector<struct Tranx> tranxs;
};

void workThread(Workload workload,
		std::vector<DTranx::Client::Client*> clients,
		std::vector<std::pair<std::string, int> > endpoints,
		int * succNum,
		std::unordered_map<std::string, int> *key_dist,
		OptionParser options) {
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(10);
	std::vector<std::string> ips;
	for (auto endpoint = endpoints.begin(); endpoint != endpoints.end(); ++endpoint) {
		ips.push_back(endpoint->first);
	}
	Client::ClientTranx *clientTranx = new DTranx::Client::ClientTranx(30000, context, ips,
			options.selfAddress, 30030);
	for (size_t i = 0; i < clients.size(); ++i) {
		clientTranx->InitClients(endpoints[i].first, clients[i]);
	}
	//tranx execution
	for (auto it = workload.tranxs.begin(); it != workload.tranxs.end(); ++it) {
		//read
		for (auto it_b = it->reads.begin(); it_b != it->reads.end(); ++it_b) {
			std::string value;
			if (key_dist->find(*it_b) != key_dist->end()) {
				(*key_dist)[*it_b] += 1;
			} else {
				(*key_dist)[*it_b] = 1;
			}
			clientTranx->Read(*it_b, value);
		}
		for (auto it_b = it->writes.begin(); it_b != it->writes.end(); ++it_b) {
			std::string key = it_b->first;
			if (key_dist->find(key) != key_dist->end()) {
				(*key_dist)[key] += 1;
			} else {
				(*key_dist)[key] = 1;
			}
			clientTranx->Write(key, it_b->second);
		}

		bool succ = clientTranx->Commit();
		if (succ) {
			(*succNum) = (*succNum) + 1;
		}
		clientTranx->Clear();

	}
	delete clientTranx;
}

int main(int argc, char** argv) {
	OptionParser options(argc, argv);
	Util::Log::setLogPolicy( { { "Client", "VERBOSE" } });
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(10);
	std::vector<std::string> ips;
	std::vector<std::pair<std::string, int> > endpoints;
	std::vector<DTranx::Client::Client*> clients;
	if (options.clusterFile.empty()) {
		ips.push_back(options.selfAddress);
	} else {
		std::ifstream ipFile(options.clusterFile);
		if (!ipFile.is_open()) {
			VERBOSE("cannot open %s", argv[1]);
			exit(0);
		}
		std::string ip;
		while (std::getline(ipFile, ip)) {
			ips.push_back(ip);
		}
		//prepare clients
		int startClientPort = 30030;
		for (auto ip = ips.begin(); ip != ips.end(); ++ip) {
			clients.push_back(new DTranx::Client::Client(*ip, 30000, context));
			while (!clients.back()->Bind(options.selfAddress, startClientPort++)) {
				assert(false);
			}
			endpoints.push_back(std::make_pair(*ip, startClientPort));
		}
	}
	//read workload directory
	std::unordered_map<std::string, struct Workload> workloads;
	std::set<std::string> fileNames = DTranx::Util::FileUtil::LsFiles(options.workload);
	for (auto fileIter = fileNames.begin(); fileIter != fileNames.end(); ++fileIter) {
		std::ifstream file(*fileIter);
		if (!file.is_open()) {
			VERBOSE("cannot open %s", fileIter->c_str());
			exit(0);
		}
		workloads[*fileIter] = Workload();
		std::string line;
		while (std::getline(file, line)) {
			std::vector<std::string> fields = DTranx::Util::StringUtil::Split(line, ' ');
			assert(fields.size() > 0);
			if (fields[0] == "read") {
				struct Tranx readTranx;
				readTranx.isRead = true;
				for (int j = 1; j < fields.size(); ++j) {
					assert(fields[j].find("user") != std::string::npos);
					readTranx.reads.push_back(fields[j]);
				}
				workloads[*fileIter].tranxs.push_back(readTranx);
			} else if (fields[0] == "update") {
				struct Tranx writeTranx;
				writeTranx.isRead = false;
				bool readfields = true;
				for (int j = 1; j < fields.size(); ++j) {
					if (fields[j] == "after") {
						readfields = false;
						continue;
					}
					assert(fields[j].find("user") != std::string::npos);
					if (readfields) {
						writeTranx.reads.push_back(fields[j]);
					} else {
						assert(fields.size() > j + 1);
						writeTranx.writes[fields[j]] = fields[j + 1];
						j++;
					}
				}
				workloads[*fileIter].tranxs.push_back(writeTranx);
			} else {
				assert(false);
			}
		}
		file.close();
	}

	boost::chrono::system_clock::time_point start = boost::chrono::system_clock::now();
	// start thread pool
	std::vector<boost::thread> threads;
	std::vector<int *> succ;
	std::vector<std::unordered_map<std::string, int>*> key_dist;
	int index = 0;
	for (auto workload = workloads.begin(); workload != workloads.end(); ++workload) {
		succ.push_back(new int(0));
		key_dist.push_back(new std::unordered_map<std::string, int>());
		threads.emplace_back(
				boost::thread(workThread, workload->second, clients, endpoints, succ[index],
						key_dist[index], options));
		index++;
	}
	for (auto it = threads.begin(); it != threads.end(); ++it) {
		it->join();
	}

	boost::chrono::system_clock::time_point end = boost::chrono::system_clock::now();

	int allTranxNum = 0;
	for (auto it = workloads.begin(); it != workloads.end(); ++it) {
		allTranxNum += it->second.tranxs.size();
	}
	int succTranxNum = 0;
	for (auto it = succ.begin(); it != succ.end(); ++it) {
		succTranxNum += *(*it);
		delete (*it);
	}
	std::unordered_map<std::string, int> agg_key_dist;
	for (auto it = key_dist.begin(); it != key_dist.end(); ++it) {
		for (auto it_b = (*it)->begin(); it_b != (*it)->end(); ++it_b) {
			if (agg_key_dist.find(it_b->first) != agg_key_dist.end()) {
				agg_key_dist[it_b->first] += it_b->second;
			} else {
				agg_key_dist[it_b->first] = 1;
			}
		}
	}

	int totalKeyAccessed = 0;
	for (auto it = agg_key_dist.begin(); it != agg_key_dist.end(); ++it) {
		std::cout << "key: " << it->first << ", times: " << it->second << std::endl;
		totalKeyAccessed += it->second;
	}
	std::cout << "number of difference keys: " << agg_key_dist.size()
			<< ", and the average number of access is "
			<< totalKeyAccessed * 1.0 / agg_key_dist.size() << std::endl;
	std::cout << "success percentage: " << std::to_string(1.0 * succTranxNum / allTranxNum)
			<< std::endl;
	int timeSpentSeconds =
			boost::chrono::duration_cast<boost::chrono::milliseconds>(end - start).count();
	std::cout << "throughput: " << std::to_string(1.0 * 1000 * succTranxNum / timeSpentSeconds)
			<< std::endl;
	std::cout << "there are in total: " << workloads.size() << " threads and " << allTranxNum
			<< " transactions" << std::endl;
	return 0;
}

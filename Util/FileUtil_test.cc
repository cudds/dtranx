/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "FileUtil.h"

TEST(FileUtil, rmprefix) {
	system("touch dtranx.log1234");
	system("touch dtranx.log1235");
	system("touch dtranx.log1236");
	std::set<std::string> files = DTranx::Util::FileUtil::LsPrefix("./", "dtranx.log");
	EXPECT_EQ(3, files.size());
	DTranx::Util::FileUtil::RemovePrefix("./", "dtranx.log");
	files = DTranx::Util::FileUtil::LsPrefix("./", "dtranx.log");
	EXPECT_EQ(0, files.size());
}

TEST(FileUtil, rmfile) {
	system("touch dtranx.log1234");
	std::set<std::string> files = DTranx::Util::FileUtil::LsPrefix("./", "dtranx.log");
	EXPECT_EQ(1, files.size());
	DTranx::Util::FileUtil::RemoveFile("dtranx.log1234");
	files = DTranx::Util::FileUtil::LsPrefix("./", "dtranx.log");
	EXPECT_EQ(0, files.size());
}

TEST(FileUtil, rmdir) {
	system("mkdir ./fileutiltest");
	system("touch ./fileutiltest/dtranx.log1234");
	system("touch ./fileutiltest/dtranx.log1235");
	DTranx::Util::FileUtil::RemoveDir("./fileutiltest");
	std::set<std::string> files = DTranx::Util::FileUtil::LsFiles("./fileutiltest");
	EXPECT_EQ(0, files.size());
}

TEST(FileUtil, lsfiles) {
	system("mkdir ./fileutiltest");
	system("touch ./fileutiltest/dtranx.log1234");
	system("touch ./fileutiltest/dtranx.log1235");
	std::set<std::string> files = DTranx::Util::FileUtil::LsFiles("./fileutiltest/");
	EXPECT_EQ(2, files.size());
	system("rm -rf ./fileutiltest");
}

TEST(FileUtil, lsfullprefix) {
	system("touch dtranx.log1234");
	system("touch dtranx.log1235");
	system("touch dtranx.log1236");
	std::set<std::string> files = DTranx::Util::FileUtil::LsPrefix(
			"/home/neal/Documents/dev/DTranx/dtranx.log");
	EXPECT_EQ(3, files.size());
	files = DTranx::Util::FileUtil::LsPrefix("dtranx.log");
	EXPECT_EQ(3, files.size());
	remove("dtranx.log1234");
	remove("dtranx.log1235");
	remove("dtranx.log1236");
}

TEST(FileUtil, lsprefix) {
	system("touch dtranx.log1234");
	system("touch dtranx.log1235");
	system("touch dtranx.log1236");
	std::set<std::string> files = DTranx::Util::FileUtil::LsPrefix("./", "dtranx.log");
	EXPECT_EQ(3, files.size());
	remove("dtranx.log1234");
	remove("dtranx.log1235");
	remove("dtranx.log1236");
}

TEST(FileUtil, lsdir) {
	system("mkdir ./fileutiltest");
	system("mkdir ./fileutiltest/dir1");
	system("mkdir ./fileutiltest/dir2");
	system("mkdir ./fileutiltest/dir3");
	std::set<std::string> alldirs = DTranx::Util::FileUtil::LsDirectory("./fileutiltest");
	EXPECT_TRUE(alldirs.find("./fileutiltest/dir1") != alldirs.end());
	system("rm -rf ./fileutiltest");
}

TEST(FileUtil, lsprefixdir) {
	system("mkdir ./fileutiltest");
	system("mkdir ./fileutiltest/dtranx.db0");
	system("mkdir ./fileutiltest/dtranx.db1");
	system("mkdir ./fileutiltest/dtranx.db2");
	std::set<std::string> alldirs = DTranx::Util::FileUtil::LsPrefixDir("./fileutiltest",
			"dtranx.db");
	EXPECT_EQ(3, alldirs.size());
	system("rm -rf ./fileutiltest");
}

TEST(FileUtil, rename) {
	system("touch dtranx.log1234");
	EXPECT_TRUE(DTranx::Util::FileUtil::RenameFile("dtranx.log1234", ".dtranx.log1234"));
	std::set<std::string> files = DTranx::Util::FileUtil::LsPrefix("./", ".dtranx.log1234");
	EXPECT_EQ(1, files.size());
	DTranx::Util::FileUtil::RemoveFile(".dtranx.log1234");
}

TEST(FileUtil, combinename) {
	EXPECT_EQ("/mnt/mem/dtranx.log1234",
			DTranx::Util::FileUtil::CombineName("/mnt/mem", "dtranx.log1234"));
	EXPECT_EQ("/mnt/mem/dtranx.log1234",
			DTranx::Util::FileUtil::CombineName("/mnt/mem/", "dtranx.log1234"));
}

TEST(FileUtil, splitname) {
	std::string dirName, fileName;
	DTranx::Util::FileUtil::SplitName("/mnt/mem/dtranx.log", dirName, fileName);
	EXPECT_EQ("/mnt/mem", dirName);
	EXPECT_EQ("dtranx.log", fileName);
}


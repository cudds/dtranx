#!/usr/bin/env python
import argparse
import sys
import shutil
import time
import threading
from Utility.SSHHelper import *
from Utility.BashHelper import *

#
# EnvSetup has three modes
#	check mode: check remote environment
#	install mode: install DDSBrick to build client programs later
#	run mode: set up the remote environment for DDSBrick to run
#

def RunFormat(ip, RemoteUser, diskRef):
	#SSD format and mount
	bash = "\
		echo -e \"n\np\n1\n\n\nw\n\" | sudo fdisk /dev/" + diskRef
	runSSHCommand(ip, RemoteUser, bash)
	runSSHCommand(ip, RemoteUser, "sudo mkfs.ext4 /dev/"+diskRef+"1; mkdir ~/Test; sudo mount /dev/"+diskRef+"1 ~/Test; sudo chmod -R a+rwx ~/Test;")

def scpLibrary(server, username):
	scp(True, LibPath + "libproto*", username, server, RemoteHomeDir)
	scp(True, LibPath + "libzmq*", username, server, RemoteHomeDir)
	scp(True, LibPath + "libprofiler*", username, server, RemoteHomeDir)
	scp(True, LibPath + "libunwind*", username, server, RemoteHomeDir)
	scp(True, "/usr/lib/libcrypto*", username, server, RemoteHomeDir)
	scp(True, LibPath + "libpmem*", username, server, RemoteHomeDir)
	scp(True, LibPath + "libboost*", username, server, RemoteHomeDir)
	scp(True, LibPath + "libmetis*", username, server, RemoteHomeDir)
	scp(True, LibPath + "liblogcabin*", username, server, RemoteHomeDir)
	scp(True, LibPath + "libleveldb*", username, server, RemoteHomeDir)
	scp(True, LibPath + "libtcmalloc*", username, server, RemoteHomeDir)
	scp(True, "/usr/lib/x86_64-linux-gnu/libstdc++.so.6.0.21", username, server, RemoteHomeDir)
	runSSHCommand(server, username, "sudo mv "+RemoteHomeDir+ "libproto* /usr/local/lib/")
	runSSHCommand(server, username, "sudo mv "+RemoteHomeDir+ "libzmq* /usr/local/lib/")
	runSSHCommand(server, username, "sudo mv "+RemoteHomeDir+ "libprofiler* /usr/local/lib/")
	runSSHCommand(server, username, "sudo mv "+RemoteHomeDir+ "libunwind* /usr/local/lib/")
	runSSHCommand(server, username, "sudo mv "+RemoteHomeDir+ "libcrypto* /usr/lib/")
	runSSHCommand(server, username, "sudo mv "+RemoteHomeDir+ "libpmem* /usr/local/lib/")
	runSSHCommand(server, username, "sudo mv "+RemoteHomeDir+ "libboost* /usr/local/lib/")
	runSSHCommand(server, username, "sudo mv "+RemoteHomeDir+ "libmetis* /usr/local/lib/")
	runSSHCommand(server, username, "sudo mv "+RemoteHomeDir+ "liblogcabin* /usr/local/lib/")
	runSSHCommand(server, username, "sudo mv "+RemoteHomeDir+ "libleveldb* /usr/local/lib/")
	runSSHCommand(server, username, "sudo mv "+RemoteHomeDir+ "libtcmalloc* /usr/local/lib/")
	runSSHCommand(server, username, "sudo mv "+RemoteHomeDir+ "libstdc++.so.6.0.21 /usr/lib/x86_64-linux-gnu/")
	runSSHCommand(server, username, "sudo rm /usr/lib/x86_64-linux-gnu/libstdc++.so.6; sudo ln -s /usr/lib/x86_64-linux-gnu/libstdc++.so.6.0.21 /usr/lib/x86_64-linux-gnu/libstdc++.so.6")

def RunPmemKernelBuild(ip):
	scp(True, "/home/neal/NingDev/home/neal/Documents/dev/Library/linux-4.4.tar.gz", RemoteUser, ip, RemoteProjDir)
	#scp(True, "/home/neal/Documents/dev/Library/linux-4.4.tar.gz", RemoteUser, ip, RemoteProjDir)
	runSSHCommand(ip, RemoteUser, "cd "+ RemoteProjDir + "; tar -zxf linux-4.4.tar.gz; cd linux-4.4; sudo make modules_install install")
	runSSHCommand(ip, RemoteUser, "sudo sed -i 's/GRUB_CMDLINE_LINUX=\"/GRUB_CMDLINE_LINUX=\"memmap=40G!80G /' /etc/default/grub")
	runSSHCommand(ip, RemoteUser, "sudo update-grub2")

def WaitForThreads(threads):
	while True:
		deleted_threads = []
		for thread in threads:
			if thread.isAlive() == False:
				thread.join()
				deleted_threads.append(thread)
		for thread in deleted_threads:
			threads.remove(thread)
		if len(threads) == 0:
			break;
		time.sleep(5)

'''
Procedures of run subcommand
	including format, lib, pmemkernel, openfile
'''
def FormatProcedure(args, ips):
	threads = []
	if args.other == None:
		diskRef = "sdc"
	else:
		diskRef = args.other
	for ip in ips:
		print "server: " + ip
		thread = threading.Thread(target = RunFormat, args = (ip,RemoteUser,diskRef,))
		thread.start()
		threads.append(thread)
	WaitForThreads(threads)

def LibProcedure(args, ips):
	#install dynamic library
	threads = []
	for ip in ips:
		print ip
		thread = threading.Thread(target = scpLibrary, args = (ip,RemoteUser,))
		thread.start()
		threads.append(thread)
	WaitForThreads(threads)

def PmemkernelProcedure(args, ips):
	#install kernel that supports persistent memory
	threads = []
	for ip in ips:
		print ip
		thread = threading.Thread(target = RunPmemKernelBuild, args = (ip,))
		thread.start()
		threads.append(thread)
	while True:
		deleted_threads = []
		for thread in threads:
			if thread.isAlive() == False:
				thread.join()
				deleted_threads.append(thread)
		for thread in deleted_threads:
			threads.remove(thread)
		if len(threads) == 0:
			break;
		time.sleep(5)

def OpenfileProcedure(args, ips):
	#change open file limit
	#remember to mount sdc1 after restart
	for ip in ips:
		print ip
		bash ="\
		cp /etc/security/limits.conf ./limits.conf;\
		echo \* soft nofile 500000 >> ./limits.conf;\
		echo \* hard nofile 500000 >> ./limits.conf;"
		runBash(bash) 
		scp(True, "./limits.conf", RemoteUser, ip, RemoteProjDir)
		runSSHCommand(ip, RemoteUser,"sudo mv " + RemoteProjDir + "limits.conf /etc/security/; sudo reboot now")
	runBash("rm ./limits.conf")

def RemountProcedure(args, ips):
	if args.other == None:
		diskRef = "sdc"
	else:
		diskRef = args.other
	for ip in ips:
		print ip
		if diskRef == "mem":
			runSSHCommand(ip, RemoteUser, "sudo mount -t tmpfs -o size=10G tmpfs ~/Test; sudo chmod -R a+rwx ~/Test;")
		else:
			runSSHCommand(ip, RemoteUser, "sudo mount /dev/"+diskRef+"1 ~/Test; sudo chmod -R a+rwx ~/Test;")
		runSSHCommand(ip, RemoteUser, "sudo mkdir /mnt/mem; sudo mkfs.ext4 /dev/pmem0; sudo mount -o dax /dev/pmem0 /mnt/mem; sudo chmod -R a+rwx /mnt")

def SoftIRQProcedure(args, ips):
	# in cloudlab, the second ethernet in ifconfig output is always the LAN network
	for ip in ips:
		print ip
		stdout,stderr = runSSHCommand(ip, RemoteUser,"ifconfig|sed -n '11,11p'|awk '{print $1}'")
		eth = stdout[0].strip()
		print eth
		print runSSHCommand(ip, RemoteUser, "cd " + RemoteHomeDir + ";sudo ./process.sh softirq " + eth + " 40 1 17")

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()
runtime_parser = subparsers.add_parser('run', help='run in all nodes for deployment preparation')
runtime_parser.add_argument("-c","--cluster",dest="cluster",help="cluster config file required for vm mode: ip list", required = True)
runtime_parser.add_argument("-j","--job",dest="job",help=
	"job type: "
	"(1)format(format ssd at sd* and mount;"
	"(2)lib(install libraries);"
	"(3)pmemkernel(prepare pmem supported kernel);"
	"(4)openfile(change openfile limit and restart;"
	"(5)stageone(including format, lib, pmemkernel, openfile)"
	"(6)remount(remount the ssd after restart, plus pmem device);"
	"(7)softirq(setting irq binding for 32 core scenario, make sure process.sh is copied to the nodes, this command works only for cloudlab);"
	"(8)stagetwo(including remount, softirq)"
	"(9)iobind(bind io to cores 1,21, if needed change the script)"
 	"(10)helper(run a command in each node)", required = True)
runtime_parser.add_argument("-o","--other",dest="other", help=
	"for ssd/format/remount, specify the disk, e.g. sdc(sdc is by default)"
	"for remount, a special argument using mem means mount a tmpfs")
runtime_parser.set_defaults(subcommand = 'runtime')
check_parser = subparsers.add_parser('check', help='check deployment environment')
check_parser.add_argument("-c","--cluster",dest="cluster",help="cluster config file required for vm mode: ip list", required = True)
check_parser.add_argument("-j","--job",dest="job",help=
	"job type: "
	"(1)ssd(check sd* for ssd);" 
	"(2)cpu(check frequency and core number\n", required = True)
check_parser.add_argument("-o","--other",dest="other", help=
	"for ssd, specify the disk, e.g. sdc(sdc is by default)")
check_parser.set_defaults(subcommand = 'check')
install_parser = subparsers.add_parser('install', help='install dtranx for client build')
install_parser.set_defaults(subcommand = 'install')

args = parser.parse_args()

if args.subcommand == 'install':
	try:
		shutil.rmtree("/usr/local/include/DTranx")
	except OSError:
		print "first time installing DTranx on this computer"
	shutil.copytree("include/DTranx", "/usr/local/include/DTranx")
	shutil.copyfile("Build/libdtranx.a","/usr/local/lib/libdtranx.a")
	shutil.copyfile("Build/libdtranx.so","/usr/local/lib/libdtranx.so")
elif args.subcommand == 'check':
	ips = []
	f = open(args.cluster,'r')
	for line in f:
		ips.append(line.strip())
	print ips
	if args.job == "ssd":
		if args.other == None:
			diskRef = "sdc"
		else:
			diskRef = args.other
		#find the disk that contains SSD
		for ip in ips:
			print ip
			print runSSHCommand(ip, RemoteUser, "sudo smartctl -i /dev/"+diskRef)
	elif args.job == "cpu":
		for ip in ips:
			print ip
			print runSSHCommand(ip, RemoteUser, "lscpu|sed -n '4,4p'")
			print runSSHCommand(ip, RemoteUser, "cat /proc/cpuinfo | grep \"GHz\"|head -n 1")
elif args.subcommand == 'runtime':
	ips = []
	f = open(args.cluster,'r')
	for line in f:
		ips.append(line.strip())
	print ips

	if args.job == "format":
		FormatProcedure(args, ips)
	elif args.job == "lib":
		LibProcedure(args, ips)
	elif args.job == "pmemkernel":
		PmemkernelProcedure(args, ips)
	elif args.job == "openfile":
		OpenfileProcedure(args, ips)
	elif args.job == "stageone":
		FormatProcedure(args, ips)
		LibProcedure(args, ips)
		PmemkernelProcedure(args, ips)
		OpenfileProcedure(args, ips)
	elif args.job == "remount":
		RemountProcedure(args, ips)
	elif args.job == "softirq":
		processCopied = raw_input("Is process.sh copied to the nodes('yes' or 'no'):")
		if processCopied == "yes":
			SoftIRQProcedure(args, ips)
		else:
			exit()
	elif args.job == "stagetwo":
		RemountProcedure(args, ips)
		SoftIRQProcedure(args, ips)
	elif args.job == "iobind":
		#zeromq thread binding
		for ip in ips:
			print ip
			print runSSHCommand(ip, RemoteUser, "cd " + RemoteHomeDir + ";./process.sh bind 1,21")
	elif args.job == "helper":
		index = 1
		#print runSSHCommand("128.104.222.217", RemoteUser, "cd ~/Test;hyperdex coordinator -f -l 192.168.0.1 -p 7777 &> hyperdex.coord &")
		for ip in ips:
			print ip
			#print runSSHCommand(ip, RemoteUser, "sudo ldconfig")
			#print runSSHCommand(ip, RemoteUser, "cd ~/Test;hyperdex daemon -f --listen=192.168.0."+str(index)+" --listen-port=7776 --coordinator=192.168.0.1 --coordinator-port=7777 --data=./hyperdex.db &> hyperdex.daemon &")
			#print runSSHCommand(ip, RemoteUser, "mount")
			print runSSHCommand(ip, RemoteUser, "ps -ef|grep DTranx")
			#print runSSHCommand(ip, RemoteUser, "ps -ef|grep hyperdex")
			#print runSSHCommand(ip, RemoteUser, "wc -l ~/Test/DTranx.out")
			#print runSSHCommand(ip, RemoteUser, "rm -rf ~/Test/dtranx.db; rm -rf ~/Test/dtranx.mapdb; cp -R ~/Test/dtranx.db_ ~/Test/dtranx.db;cp -R ~/Test/dtranx.mapdb_ ~/Test/dtranx.mapdb")
			index += 1
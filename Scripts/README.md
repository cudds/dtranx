In this directory, scripts helps to deploy, test
The scripts with '*' are usually used, the others are helper scripts that are indirectly called.

Directory(* files are ones user might want to run directly)

Deploy:
	*Deploy.py: check runtime environment, install DTranx locally, deploy in docker and machines
	*EnvSetup.py: used to set up environment of bare Ubuntu to run DTranx, e.g in Cloudlab
	Clean.sh: clean runtime services like Logcabin/DTranx, called by Deploy.py

Test:
	*TestHelper.sh: used for integ test, perf test to fetch, clean stuff, etc.
	*TestRunner.py: run ycsb tests, supporting multiple clients 
	*TestAnalyze.py: analyze results from tests, especially for multi clients

	Helper for processing outputs from multiple clients:
		*AggregateFromMultiClients.sh: aggregate throughput, latency, success rate 
		*InstantThroughputFromMultiClients.sh: aggregate throughput but generate a list of instant throughput

Prepare:
	proto.sh: run right after project are downloaded, to compile some proto files

SubDirectory
PerfUtil:
	PerfAnalyzer.py: analyze performance metrics in the log
	BasicLogHelper.py: log helper to extract information from the log, used by PerfAnalyzer.py
	PerfPlot.py: plotting metrics, used by PerfAnalyzer.py

Utility:
	SSHHelper.py: provides ssh access to the remote openstack machines
	ConfigHelper.py: Common variables, helper configure DTranx.conf
	ConfigHelper.sh: bash version of ConfigHelper.py
	BashHelper.py: common functionalities, like get current directory
	KillRemote.sh: provides ssh kill remote processes, wrapped in python function in SSHHelper.py

IntegTest:(all bash scripts)
	*IntegTest.sh used to run integ test
	TestLibrary: all test libraries called by IntegTest.sh
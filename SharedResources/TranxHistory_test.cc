/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "TranxHistory.h"

using namespace DTranx;

TEST(TranxHistory, EncodeIntoMessage) {
	SharedResources::TranxHistory tranxHistory(100, 100000000);
	std::unordered_set<std::string> readset, writeset;
	readset.insert("key1");
	readset.insert("key2");
	writeset.insert("key3");
	writeset.insert("key4");
	writeset.insert("key5");
	tranxHistory.AddTranx(readset, writeset);
	readset.clear();
	readset.insert("key6");
	tranxHistory.AddTranx(readset, writeset);

	Service::GTranxPartition tranxPartition;
	tranxHistory.EncodeIntoMessage(&tranxPartition);
	EXPECT_EQ(2, tranxPartition.tranxs().size());
	EXPECT_EQ(3, tranxPartition.tranxs(0).writeset().size());
	EXPECT_EQ(1, tranxPartition.tranxs(1).readset().size());
	EXPECT_EQ("key6", tranxPartition.tranxs(1).readset(0));
}

TEST(TranxHistory, ExtractFromMessage) {
	SharedResources::TranxHistory tranxHistory(100, 100000000);
	Service::GTranxPartition tranxPartition;
	Service::GTranxKeys *tranxKeys = tranxPartition.add_tranxs();
	tranxKeys->add_readset("key1");
	tranxKeys->add_readset("key2");
	tranxKeys->add_writeset("key3");
	tranxKeys->add_writeset("key4");
	tranxKeys->add_writeset("key5");
	tranxKeys = tranxPartition.add_tranxs();
	tranxKeys->add_readset("key6");
	tranxKeys->add_writeset("key7");
	tranxKeys->add_writeset("key8");
	tranxHistory.ExtractFromMessage(&tranxPartition);
	EXPECT_EQ(2, tranxHistory.tranxList.size());
	EXPECT_EQ(2, tranxHistory.tranxList[0].first.size());
}

TEST(TranxHistory, Repartition) {
	SharedResources::TranxHistory tranxHistory(1000000, 5);
	Service::GTranxPartition tranxPartition;
	for (int i = 0; i < 5; ++i) {
		Service::GTranxKeys *tranxKeys = tranxPartition.add_tranxs();
		tranxKeys->add_readset("key1");
		tranxKeys->add_readset("key2");
		tranxKeys->add_writeset("key3");
		tranxKeys->add_writeset("key4");
		tranxKeys->add_writeset("key5");
	}
	tranxHistory.ExtractFromMessage(&tranxPartition);
	std::vector<std::pair<std::unordered_set<std::string>, std::unordered_set<std::string>>>tranxList = tranxHistory.ToRePartition();
	EXPECT_EQ(5, tranxList.size());
	EXPECT_EQ(0, tranxHistory.tranxList.size());
	tranxPartition.clear_tranxs();
	for (int i = 0; i < 4; ++i) {
		Service::GTranxKeys *tranxKeys = tranxPartition.add_tranxs();
		tranxKeys->add_readset("key1");
		tranxKeys->add_readset("key2");
		tranxKeys->add_writeset("key3");
		tranxKeys->add_writeset("key4");
		tranxKeys->add_writeset("key5");
	}
	tranxHistory.ExtractFromMessage(&tranxPartition);
	tranxList = tranxHistory.ToRePartition();
	EXPECT_EQ(0, tranxList.size());
	EXPECT_EQ(4, tranxHistory.tranxList.size());
}

TEST(TranxHistory, Tobroadcast) {
	SharedResources::TranxHistory tranxHistory(5, 1000000);
	Service::GTranxPartition tranxPartition;
	std::unordered_set<std::string> readset, writeset;
	readset.insert("key1");
	readset.insert("key2");
	writeset.insert("key3");
	writeset.insert("key4");
	writeset.insert("key5");
	for (int i = 0; i < 5; ++i) {
		tranxHistory.AddTranx(readset, writeset);
	}
	Service::GTranxPartition partitionRequest;
	EXPECT_TRUE(tranxHistory.ToBroadcast(&partitionRequest));
	EXPECT_EQ(5, partitionRequest.tranxs_size());
	EXPECT_EQ(0, tranxHistory.tranxList.size());
	partitionRequest.clear_tranxs();
	for (int i = 0; i < 4; ++i) {
		tranxHistory.AddTranx(readset, writeset);
	}
	EXPECT_FALSE(tranxHistory.ToBroadcast(&partitionRequest));
	EXPECT_EQ(0, partitionRequest.tranxs_size());
	EXPECT_EQ(4, tranxHistory.tranxList.size());
}


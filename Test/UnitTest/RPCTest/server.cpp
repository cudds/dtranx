#include <boost/thread.hpp>
#include <mutex>
#include <memory>
#include <queue>

#include "zmsg.hpp"
class ZeromqProxy {

private:
	std::thread zeromqProxyThread1, zeromqProxyThread2;
	std::thread zeromqProxyThread3, zeromqProxyThread4;
	std::thread zeromqProxyThread5, zeromqProxyThread6;
	boost::thread zeromqProxyThread7, zeromqProxyThread8;
	boost::thread zeromqProxyThread9, zeromqProxyThread10;
	std::shared_ptr<bool> terminate_proxy;
	std::shared_ptr<zmq::context_t> context;
	std::string selfAddress;
	void StartProxyThread(std::string port, std::shared_ptr<bool> terminate_proxy,
							std::shared_ptr<zmq::context_t> context, int proxyIndex) {
		zmq::socket_t frontend(*context, ZMQ_ROUTER);
		zmq::socket_t backendReceiver(*context, ZMQ_DEALER);

		std::string localAddress = "tcp://" + selfAddress;
		std::string frontAddress = localAddress + ":" + port;
		try {
			frontend.bind(frontAddress.c_str());
			backendReceiver.bind(
					(std::string("inproc://backendReceiver") + std::to_string(proxyIndex)).c_str());
		} catch (zmq::error_t &e) {
			std::cout << e.what() << std::endl;
			assert(false);
		}

		int lingtime = 0;
		frontend.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
		int hwm = 0;
		size_t len = sizeof(hwm);
		frontend.setsockopt(ZMQ_SNDHWM, &hwm, len);
		backendReceiver.setsockopt(ZMQ_SNDHWM, &hwm, len);
		frontend.setsockopt(ZMQ_RCVHWM, &hwm, len);
		backendReceiver.setsockopt(ZMQ_RCVHWM, &hwm, len);
		int mandatory = 1;
		len = sizeof(mandatory);
		frontend.setsockopt(ZMQ_ROUTER_MANDATORY, &mandatory, len);
		try {
			zmq::proxy(frontend, backendReceiver, NULL);
		} catch (std::exception &e) {

		}
	}
public:
	explicit ZeromqProxy(std::shared_ptr<zmq::context_t> context, std::string selfAddress)
			: context(context), selfAddress(selfAddress) {
		terminate_proxy = std::make_shared<bool>(false);
	}

	~ZeromqProxy() {
		ShutProxy();
	}

	void StartProxy() {
		zeromqProxyThread1 = std::thread(&ZeromqProxy::StartProxyThread, this, "2222",
				terminate_proxy, context, 1);
		zeromqProxyThread2 = std::thread(&ZeromqProxy::StartProxyThread, this, "2223",
				terminate_proxy, context, 2);
		zeromqProxyThread3 = std::thread(&ZeromqProxy::StartProxyThread, this, "2224",
				terminate_proxy, context, 3);
		zeromqProxyThread4 = std::thread(&ZeromqProxy::StartProxyThread, this, "2225",
				terminate_proxy, context, 4);
		zeromqProxyThread5 = std::thread(&ZeromqProxy::StartProxyThread, this, "2226",
				terminate_proxy, context, 5);
		zeromqProxyThread6 = std::thread(&ZeromqProxy::StartProxyThread, this, "2227",
				terminate_proxy, context, 6);
		zeromqProxyThread7 = std::thread(&ZeromqProxy::StartProxyThread, this, "2228",
				terminate_proxy, context, 7);
		zeromqProxyThread8 = std::thread(&ZeromqProxy::StartProxyThread, this, "2229",
				terminate_proxy, context, 8);
		zeromqProxyThread9 = std::thread(&ZeromqProxy::StartProxyThread, this, "2230",
				terminate_proxy, context, 9);
		zeromqProxyThread10 = std::thread(&ZeromqProxy::StartProxyThread, this, "2231",
				terminate_proxy, context, 10);
	}

	void ShutProxy() {
		if (terminate_proxy) {
			*terminate_proxy = true;
		}
		if (zeromqProxyThread1.joinable()) {
			zeromqProxyThread1.join();
		}
	}

};

struct ServerThreadInfo {
	int numFreeWorkers;
	int maxThreads;
	std::vector<std::thread> threadPool;
	std::mutex mutex_queue;
	std::condition_variable conditionVariable;
	std::queue<std::pair<zmsg::ustring, zmsg::ustring>> rpcQueue;
	ServerThreadInfo()
			: threadPool(), mutex_queue(), conditionVariable(), rpcQueue() {
		numFreeWorkers = 0;
		maxThreads = 1000;
	}
};
ServerThreadInfo serverInfo[10];

void WorkerThread(int serverID, zmq::socket_t *socket, std::mutex *mutex) {
	int lingtime = 0;
	std::cout << "a newthread created" << std::endl;

	while (true) {
		zmsg::ustring client_id;
		zmsg::ustring request;
		{
			std::unique_lock<std::mutex> lockGuard(serverInfo[serverID].mutex_queue);
			++serverInfo[serverID].numFreeWorkers;
			while (serverInfo[serverID].rpcQueue.empty()) {
				serverInfo[serverID].conditionVariable.wait(lockGuard);
			}
			--serverInfo[serverID].numFreeWorkers;
			client_id = serverInfo[serverID].rpcQueue.front().first;
			request = serverInfo[serverID].rpcQueue.front().second;
			serverInfo[serverID].rpcQueue.pop();
		}
		usleep(450);
		zmsg zm;
		zm.push_ustring(client_id);
		zm.push_back("");
		zm.push_ustring(request);
		mutex->lock();
		zm.send(*socket);
		mutex->unlock();
	}
}

void ServerThread(std::shared_ptr<zmq::context_t> context, int serverID, std::string backend) {
	zmq::socket_t socket(*context, ZMQ_DEALER);
	int lingtime = 0;
	socket.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
	socket.connect(backend.c_str());
	int count = 0;
	std::chrono::system_clock::time_point start, end;
	std::mutex mutex_socket;

	while (true) {
		zmsg zm;
		while (true) {
			mutex_socket.lock();
			if (zm.recv_non_block(socket)) {
				mutex_socket.unlock();
				break;
			}
			mutex_socket.unlock();
		}
		count++;
		if (count % 10000 == 0) {
			std::cout << serverID << " receiver " << count << std::endl;
		}
		zmsg::ustring client_id = zm.get_part(0);
		zmsg::ustring request = zm.get_part(1);
		std::unique_lock<std::mutex> lockGuard(serverInfo[serverID].mutex_queue);
		serverInfo[serverID].rpcQueue.push(std::make_pair(client_id,request));
		if (serverInfo[serverID].numFreeWorkers == 0
				&& serverInfo[serverID].threadPool.size() < serverInfo[serverID].maxThreads) {
			serverInfo[serverID].threadPool.emplace_back(&WorkerThread, serverID, &socket,
					&mutex_socket);
		}
		lockGuard.unlock();
		serverInfo[serverID].conditionVariable.notify_one();
	}
}

int main(int argc, char **argv) {
	s_version();
	if(argc != 2){
		std::cout<<"provide the ip address you want to bind"<<std::endl;
		return 0;
	}
	std::string selfAddress(argv[1]);
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(200);
	std::unique_ptr<ZeromqProxy> proxy = std::unique_ptr<ZeromqProxy>(new ZeromqProxy(context, selfAddress));
	proxy->StartProxy();
	std::thread serverThread1 = std::thread(&ServerThread, context, 0, "inproc://backendReceiver1");
	std::thread serverThread2 = std::thread(&ServerThread, context, 1, "inproc://backendReceiver2");
	std::thread serverThread3 = std::thread(&ServerThread, context, 2, "inproc://backendReceiver3");
	std::thread serverThread4 = std::thread(&ServerThread, context, 3, "inproc://backendReceiver4");
	std::thread serverThread5 = std::thread(&ServerThread, context, 4, "inproc://backendReceiver5");
	std::thread serverThread6 = std::thread(&ServerThread, context, 5, "inproc://backendReceiver6");
	std::thread serverThread7 = std::thread(&ServerThread, context, 6, "inproc://backendReceiver7");
	std::thread serverThread8 = std::thread(&ServerThread, context, 7, "inproc://backendReceiver8");
	std::thread serverThread9 = std::thread(&ServerThread, context, 8, "inproc://backendReceiver9");
	std::thread serverThread10 = std::thread(&ServerThread, context, 9,
			"inproc://backendReceiver10");
	while (true) {
		sleep(5);
	}
}


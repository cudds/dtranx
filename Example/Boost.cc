/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <boost/chrono.hpp>
#include <boost/atomic.hpp>
#include <boost/thread.hpp>

/*
 * Test if boost time measurement function might affect response time monitoring
 */
using namespace std;

void TestThread(int numOfOperation) {
	boost::chrono::steady_clock::time_point time1, time2;
	std::vector<uint64_t> timeLatencies;
	for (int i = 0; i < numOfOperation; ++i) {
		time1 = boost::chrono::steady_clock::now();
		time2 = boost::chrono::steady_clock::now();

		uint64_t timeElapsed = boost::chrono::duration_cast<boost::chrono::nanoseconds>(
				time2 - time1).count();
		if (timeElapsed > 1000000) {
			timeLatencies.push_back(timeElapsed);
		}
	}
	std::string str;
	std::cout << "total number of high latency: " << timeLatencies.size() << std::endl;
	for (auto it = timeLatencies.begin(); it != timeLatencies.end(); ++it) {
		str += std::to_string(*it) + ";";
	}
	std::cout << str << std::endl;
}

int main(int argc, char **argv) {
	if (argc < 3) {
		std::cout << "arg1: # of operations" << std::endl;
		std::cout << "arg2: # of threads" << std::endl;
		return 0;
	}
	int numOfOperation = stoi(argv[1], 0, 10);
	int numOfThreads = stoi(argv[2], 0, 10);
	std::vector<boost::thread> threads;
	for (int i = 0; i < numOfThreads; ++i) {
		threads.push_back(boost::thread(TestThread, numOfOperation / numOfThreads));
	}

	for (int i = 0; i < numOfThreads; ++i) {
		if (threads[i].joinable()) {
			threads[i].join();
		}
	}

}

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "Build/Util/Profile.pb.h"
#include "DTranx/Util/Log.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace DTranx;
int main(void) {
	Util::ProfileLog profileLog;
	Util::Log::setLogPolicy( { { "Example", "PROFILE" } });
	profileLog.set_type(Util::ProfileType::PERF);
	profileLog.mutable_perflog()->set_stagename("storage dispatch");
	profileLog.mutable_perflog()->add_queuelength(10);
	VERBOSE("%s", profileLog.DebugString().c_str());
	PROFILE(profileLog.SerializeAsString());
	/*
	 ifstream myfile("hello");
	 string line;
	 if (myfile.is_open()) {
	 while (getline(myfile, line)) {
	 std::cout<<line<<std::endl;
	 bool result = profileLog.ParseFromString(line);
	 if(result)
	 cout<< profileLog.perflog().preparetime() <<endl;
	 }
	 myfile.close();
	 }

	 else
	 cout << "Unable to open file";
	 */
}

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_SHAREDRESOURCES_TRANXSERVERINFO_H_
#define DTRANX_SHAREDRESOURCES_TRANXSERVERINFO_H_

#include <atomic>
#include <vector>
#include <memory>
#include "DTranx/Util/ConfigHelper.h"
#include "DTranx/Util/types.h"
#include "Util/StringUtil.h"

namespace DTranx {
namespace SharedResources {

class TranxServerInfo {
public:
	enum Mode {
		NORMAL, UNITTEST_SERVER, UNITTEST_CLIENT, INTEGTEST,
	};

	TranxServerInfo()
			: nodeID_(), selfAddress_(), allNodes_(), mode_(), enablePerfLog_(false) {
	}

	void SetNodeID(int nodeID) {
		nodeID_ = nodeID;
	}

	int GetNodeID() {
		return nodeID_;
	}

	std::string GetSelfAddress() {
		return selfAddress_;
	}

	void SetAllNodes(std::vector<std::string> allNodes) {
		allNodes_ = allNodes;
	}

	std::vector<std::string> GetAllNodes() {
		return allNodes_;
	}

	void SetConfig(Util::ConfigHelper config) {
		config_ = config;
		enablePerfLog_ = config.read<uint64>("PerfLog") == 1;
		selfAddress_ = config.read<std::string>("SelfAddress");
		context_ = std::make_shared<zmq::context_t>(config.read<uint32>("maxIOThreads"),
				config.read<uint32>("maxSockets"));
	}

	Util::ConfigHelper GetConfig() {
		return config_;
	}

	void SetMode(Mode mode) {
		mode_ = mode;
	}

	Mode GetMode() {
		return mode_;
	}

	std::shared_ptr<zmq::context_t> GetContext() {
		return context_;
	}

	bool isEnablePerfLog() const {
		return enablePerfLog_;
	}

private:
	int nodeID_;
	std::vector<std::string> allNodes_;
	Util::ConfigHelper config_;
	Mode mode_;

	/*
	 * data that can be generated from config_
	 */
	bool enablePerfLog_;
	std::string selfAddress_;
	std::shared_ptr<zmq::context_t> context_;
};

} /* namespace SharedResources */
} /* namespace DTranx */

#endif /* DTRANX_SHAREDRESOURCES_TRANXSERVERINFO_H_ */

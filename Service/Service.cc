/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "Service.h"
#include "Stages/Lock/WriteBlockLockTable.h"
#include "Stages/Log/TranxLog.h"
#include "Stages/TranxStates.h"
#include "Stages/TranxRPCHelper.h"
#include "DaemonStages/TranxAck.h"
#include "DaemonStages/Repartition.h"
#include "Stages/Storage.h"
#include "Server/Server.h"

namespace DTranx {
namespace Service {

void Service::InitStages(Stages::TranxRPCHelper *tranxRPCHelper,
		Stages::TranxStates *tranxStates,
		Stages::StringStorage *localStorage,
		DaemonStages::TranxAck *tranxAck,
		DaemonStages::Repartition *repartition,
		Stages::Log::TranxLog *tranxLog,
		Stages::Lock::WriteBlockLockTable *lockTable,
		Stages::ServerReply *replyThread) {
	this->tranxRPCHelper = tranxRPCHelper;
	this->tranxStates = tranxStates;
	this->localStorage = localStorage;
	this->tranxAck = tranxAck;
	this->repartition = repartition;
	this->tranxLog = tranxLog;
	this->lockTable = lockTable;
	this->replyThread = replyThread;
	for (int i = 0; i < 10; i++) {
		metrics[i].store(0);
	}

	//ptok_tranxStates = this->tranxStates->CreateDispatchQueueToken();
	//ptok_tranxStates_external = this->tranxStates->CreateDispatchQueueToken();
}

void Service::SetServer(Server::Server *server) {
	this->server = server;
}

}
}

#!/bin/bash
function testall(){
	echo "test1 starts"
	test1
	echo "test2 starts"
	test2
	echo "test3 starts"
	test3
	echo "test4 starts"
	test4
	echo "test5 starts"
	test5
	echo "test6 starts"
	test6
	echo "test7 starts"
	test7
	echo "test8 starts"
	test8
	echo "test9 starts"
	test9
}

SCRIPT_PATH="${BASH_SOURCE[0]}"
CurDir=`dirname "${SCRIPT_PATH}"`
. $CurDir/TestLibrary/incl.sh
echo -e "input test program"
echo -e "\t1: single client, normal test"
echo -e "\t2: single client, retry test(prepare will fail)"
echo -e "\t3: single client, fail then recovery test"
echo -e "\t4: multi client, contention test"
echo -e "\t5: recover log test: check manually for test5_recoverylogscheck"
echo -e "\t6: snapshot read test"
echo -e "\t7: locking with replication test"
echo -e "\t8: repartitioning test"
echo -e "\t\t set RepartitionThreshold in DTranx.conf to 130 for this test, FileTruncate to 10, PartitionBroadcastThreshold to 50"
echo -e "\t\t check the log data items (2,3,4),(5,6,7),(8,9,1) should be together"
echo -e "\t9: 1pc test"
echo -e "\tall: run all tests"
read test
test=test$test
PrepareTestProgram
$test

#TODO: calling deploy and testhelper to automate whole integ test process
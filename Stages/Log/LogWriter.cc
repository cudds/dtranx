/*
 * Copyright (c) 2011 The LevelDB Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file. See the AUTHORS file for names of contributors.
 *
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * wrap the LogWriter around WritableFile so that the interface to the outside is only LogWriter with filename as the input
 * modified to adjust the block_offset_ during initialization
 */

#include <sys/stat.h>
#include "LogWriter.h"
#include "Util/Crc32c.h"
#include "DTranx/Util/Log.h"

namespace DTranx {
namespace Stages {
namespace Log {

LogWriter::LogWriter(std::string& fname, bool append, LogOption logOption,
		uint64 truncateThreshold) :
		block_offset_(0), totalAvailBytes(0) {
	LogStatus s;

	if (logOption == DISK) {
		struct stat st;
		if (!stat(fname.c_str(), &st)) {
			block_offset_ = st.st_size % kBlockSize;
		}
		FILE* f;
		if (append) {
			f = fopen(fname.c_str(), "a");
		} else {
			f = fopen(fname.c_str(), "w");
		}
		assert(f != NULL);
		dest_ = new PosixWritableFile(fname, f);
	}
	if (logOption == PMEM) {
		/*
		 * instead of open old ones, here it creates new one because the DTranx system
		 * will only open new ones after recovery
		 */
		PMEMlogpool *plp = pmemlog_create(fname.c_str(), truncateThreshold,
				0666);
		//VERBOSE("LogWriter opening %s", fname.c_str());
		if (plp == NULL) {
			std::cout << pmemlog_errormsg() << std::endl << std::flush;
		}
		assert(plp != NULL);
		totalAvailBytes = pmemlog_nbyte(plp);
		dest_ = new PmemWritableFile(fname, plp);
	}
	for (int i = 0; i <= kMaxRecordType; i++) {
		char t = static_cast<char>(i);
		type_crc_[i] = Value(&t, 1);
	}
}

LogWriter::~LogWriter() {
	assert(dest_->Close().ok());
	delete dest_;
}

LogStatus LogWriter::AddRecord(const Slice& slice) {
	const char* ptr = slice.data();
	size_t left = slice.size();

	// Fragment the record if necessary and emit it.  Note that if slice
	// is empty, we still want to iterate once to emit a single
	// zero-length record
	LogStatus s;
	bool begin = true;
	do {
		const int leftover = kBlockSize - block_offset_;
		assert(leftover >= 0);
		if (leftover < kHeaderSize) {
			// Switch to a new block
			if (leftover > 0) {
				// Fill the trailer (literal below relies on kHeaderSize being 7)
				assert(kHeaderSize == 7);
				dest_->Append(Slice("\x00\x00\x00\x00\x00\x00", leftover));
			}
			block_offset_ = 0;
		}

		// Invariant: we never leave < kHeaderSize bytes in a block.
		assert(kBlockSize - block_offset_ - kHeaderSize >= 0);

		const size_t avail = kBlockSize - block_offset_ - kHeaderSize;
		const size_t fragment_length = (left < avail) ? left : avail;

		RecordType type;
		const bool end = (left == fragment_length);
		if (begin && end) {
			type = kFullType;
		} else if (begin) {
			type = kFirstType;
		} else if (end) {
			type = kLastType;
		} else {
			type = kMiddleType;
		}

		s = EmitPhysicalRecord(type, ptr, fragment_length);
		ptr += fragment_length;
		left -= fragment_length;
		begin = false;
	} while (s.ok() && left > 0);
	return s;
}

LogStatus LogWriter::EmitPhysicalRecord(RecordType t, const char* ptr,
		size_t n) {
	assert(n <= 0xffff);  // Must fit in two bytes
	assert(block_offset_ + kHeaderSize + n <= kBlockSize);

	// Format the header
	char buf[kHeaderSize];
	buf[4] = static_cast<char>(n & 0xff);
	buf[5] = static_cast<char>(n >> 8);
	buf[6] = static_cast<char>(t);

	// Compute the crc of the record type and the payload.
	uint32_t crc = Extend(type_crc_[t], ptr, n);
	crc = Mask(crc);  // Adjust for storage
	EncodeFixed32(buf, crc);

	// Write the header and the payload
	LogStatus s = dest_->Append(Slice(buf, kHeaderSize));
	if (s.ok()) {
		s = dest_->Append(Slice(ptr, n));
		if (s.ok()) {
			s = dest_->Flush();
		}
	}
	block_offset_ += kHeaderSize + n;
	return s;
}

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

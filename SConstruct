import os

opts = Variables()
opts.AddVariables(
    ("CC", "C Compiler"),
    ("CPPPATH", "The list of directories that the C preprocessor "
                "will search for include directories", []),
    ("CXX", "C++ Compiler"),
    ("CXXFLAGS", "Options that are passed to the C++ compiler", [])
)

env = Environment(options = opts,
				tools = ['default', 'protoc'],
				ENV = os.environ,
				CXX = 'g++-4.9')
				
# -Wl,--no-as-needed and pthread are used to solve pthread "operation not permitted" problem
env.Prepend(CXXFLAGS = [
    "-std=c++11",
    "-g",
    "-rdynamic",
    "-fno-omit-frame-pointer",
    ])
    
env.Prepend(LINKFLAGS = [
    "-rdynamic",
    "-g",
    ])

env.Append(CPPPATH = ['#', '#/include', '/usr/local/include/logcabin'])

# Define protocol buffers builder to simplify SConstruct files
def Protobuf(env, source):
    # First build the proto file
    cc = env.Protoc(os.path.splitext(source)[0] + '.pb.cc',
                    source,
                    PROTOCPROTOPATH = ["."],
                    PROTOCPYTHONOUTDIR = None,
                    PROTOCOUTDIR = ".")[1]
    # Then build the resulting C++ file with no warnings
    return env.SharedObject(cc,
                            CXXFLAGS = "-std=c++11")
env.AddMethod(Protobuf)

object_files = {}
Export('object_files')

# print env.Dump()

Export('env')
SConscript('Service/SConscript', variant_dir='Build/Service')
SConscript('Client/SConscript', variant_dir='Build/Client')
SConscript('DaemonStages/SConscript', variant_dir='Build/DaemonStages')
SConscript('RPC/SConscript', variant_dir='Build/RPC')
SConscript('Server/SConscript', variant_dir='Build/Server')
SConscript('SharedResources/SConscript', variant_dir='Build/SharedResources')
SConscript('Stages/SConscript', variant_dir='Build/Stages')
SConscript('Util/SConscript', variant_dir='Build/Util')
SConscript('Test/SConscript', variant_dir='Build/Test')
SConscript('Example/SConscript', variant_dir='Build/Example')

staticLib = env.StaticLibrary("Build/dtranx",
                  (object_files['Service']+
                   object_files['Client'] +
                   object_files['RPC'] +
                   object_files['Util'] +
                   object_files['DaemonStages']))
                   
dynamicLib = env.SharedLibrary("Build/dtranx",
                  (object_files['Service']+
                   object_files['Client'] +
                   object_files['RPC'] +
                   object_files['Util'] +
                   object_files['Stages'] +
                   object_files['SharedResources'] +
                   object_files['Server'] +
                   object_files['DaemonStages']))
                   
LibsForLogCabin = ["pthread", "cryptopp", "protobuf" , "zmq"]
 
 # metis are linked as static because there's only static library available
 # in /usr/local/lib/ 
env.Program("Build/DTranx",
            (["Build/Server/Main.cc"]
             + object_files['Service']
             + object_files['Client']
             + object_files['DaemonStages']
             + object_files['RPC']
             + object_files['Server']
             + object_files['SharedResources']
             + object_files['Stages']
             + object_files['Util']),
            LIBS = ["logcabin" , "leveldb", "metis", "pmemlog", "boost_thread", "boost_system", "boost_chrono", "profiler"] + LibsForLogCabin, LIBPATH = ["/usr/lib", "/usr/local/lib", "/usr/lib/gcc/x86_64-linux-gnu/4.9"]) 
            
env.Alias('install-library', env.Install('/usr/local/lib', [staticLib, dynamicLib]))
env.Alias('install-header', env.Install('/usr/local/include', ['include/DTranx']))
env.Alias('install', ['install-library', 'install-header'])
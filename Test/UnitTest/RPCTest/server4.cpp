#include <boost/thread.hpp>
#include <mutex>
#include <memory>
#include <queue>

#include "zmsg.hpp"
class ZeromqProxy {

private:
	boost::thread zeromqProxyThread1, zeromqProxyThread2;
	boost::thread zeromqProxyThread3, zeromqProxyThread4;
	boost::thread zeromqProxyThread5, zeromqProxyThread6;
	boost::thread zeromqProxyThread7, zeromqProxyThread8;
	boost::thread zeromqProxyThread9, zeromqProxyThread10;
	std::shared_ptr<bool> terminate_proxy;
	std::shared_ptr<zmq::context_t> context;
	void StartProxyThread(std::string port, std::shared_ptr<bool> terminate_proxy,
							std::shared_ptr<zmq::context_t> context, int proxyIndex) {
		zmq::socket_t frontend(*context, ZMQ_ROUTER);
		zmq::socket_t backendReceiver(*context, ZMQ_DEALER);

		std::string localAddress = "tcp://128.104.222.74";
		std::string frontAddress = localAddress + ":" + port;
		try {
			frontend.bind(frontAddress.c_str());
			backendReceiver.bind(
					(std::string("inproc://backendReceiver") + std::to_string(proxyIndex)).c_str());
		} catch (zmq::error_t &e) {
			std::cout << e.what() << std::endl;
			assert(false);
		}

		int lingtime = 0;
		frontend.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
		int hwm = 0;
		size_t len = sizeof(hwm);
		frontend.setsockopt(ZMQ_SNDHWM, &hwm, len);
		backendReceiver.setsockopt(ZMQ_SNDHWM, &hwm, len);
		frontend.setsockopt(ZMQ_RCVHWM, &hwm, len);
		backendReceiver.setsockopt(ZMQ_RCVHWM, &hwm, len);
		int mandatory = 1;
		len = sizeof(mandatory);
		frontend.setsockopt(ZMQ_ROUTER_MANDATORY, &mandatory, len);
		try {
			zmq::proxy(frontend, backendReceiver, NULL);
		} catch (std::exception &e) {

		}
	}
public:
	explicit ZeromqProxy(std::shared_ptr<zmq::context_t> context)
			: context(context) {
		terminate_proxy = std::make_shared<bool>(false);
	}

	~ZeromqProxy() {
		ShutProxy();
	}

	void StartProxy() {
		zeromqProxyThread1 = boost::thread(&ZeromqProxy::StartProxyThread, this, "2222",
				terminate_proxy, context, 1);
		zeromqProxyThread2 = boost::thread(&ZeromqProxy::StartProxyThread, this, "2223",
				terminate_proxy, context, 2);
		zeromqProxyThread3 = boost::thread(&ZeromqProxy::StartProxyThread, this, "2224",
				terminate_proxy, context, 3);
		zeromqProxyThread4 = boost::thread(&ZeromqProxy::StartProxyThread, this, "2225",
				terminate_proxy, context, 4);
		zeromqProxyThread5 = boost::thread(&ZeromqProxy::StartProxyThread, this, "2226",
				terminate_proxy, context, 5);
		zeromqProxyThread6 = boost::thread(&ZeromqProxy::StartProxyThread, this, "2227",
				terminate_proxy, context, 6);
		zeromqProxyThread7 = boost::thread(&ZeromqProxy::StartProxyThread, this, "2228",
				terminate_proxy, context, 7);
		zeromqProxyThread8 = boost::thread(&ZeromqProxy::StartProxyThread, this, "2229",
				terminate_proxy, context, 8);
		zeromqProxyThread9 = boost::thread(&ZeromqProxy::StartProxyThread, this, "2230",
				terminate_proxy, context, 9);
		zeromqProxyThread10 = boost::thread(&ZeromqProxy::StartProxyThread, this, "2231",
				terminate_proxy, context, 10);
	}

	void ShutProxy() {
		if (terminate_proxy) {
			*terminate_proxy = true;
		}
		if (zeromqProxyThread1.joinable()) {
			zeromqProxyThread1.join();
		}
	}

};

struct ServerThreadInfo {
	int numFreeWorkers;
	int maxThreads;
	std::vector<boost::thread> threadPool;
	std::mutex mutex_queue;
	std::condition_variable conditionVariable;
	std::queue<zmsg::ustring> rpcQueue;
	ServerThreadInfo()
			: threadPool(), mutex_queue(), conditionVariable(), rpcQueue() {
		numFreeWorkers = 0;
		maxThreads = 1000;
	}
};
ServerThreadInfo serverInfo[10];

void WorkerThread(int serverID, zmq::socket_t *socket, std::mutex *mutex) {
	int lingtime = 0;
	std::cout << "a newthread created" << std::endl;

	while (true) {
		zmsg::ustring client_id;
		{
			std::unique_lock<std::mutex> lockGuard(serverInfo[serverID].mutex_queue);
			++serverInfo[serverID].numFreeWorkers;
			while (serverInfo[serverID].rpcQueue.empty()) {
				serverInfo[serverID].conditionVariable.wait(lockGuard);
			}
			--serverInfo[serverID].numFreeWorkers;
			client_id = serverInfo[serverID].rpcQueue.front();
			serverInfo[serverID].rpcQueue.pop();
		}
		zmsg zm;
		zm.push_ustring(client_id);
		zm.push_back("");
		zm.push_back("data");
		mutex->lock();
		zm.send(*socket);
		mutex->unlock();
	}
}

void ServerThread(std::shared_ptr<zmq::context_t> context, int serverID, std::string backend) {
	zmq::socket_t socket(*context, ZMQ_DEALER);
	int lingtime = 0;
	socket.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
	socket.connect(backend.c_str());
	zmq::pollitem_t items[] = { { socket, 0, ZMQ_POLLIN, 0 } };
	int count = 0;
	std::chrono::system_clock::time_point start, end;
	std::mutex mutex_socket;

	while (true) {
		zmsg zm;
		while (true) {
			mutex_socket.lock();
			if (zm.recv_non_block(socket)) {
				mutex_socket.unlock();
				break;
			}
			mutex_socket.unlock();
		}
		count++;
		if (count % 10000 == 0) {
			std::cout << serverID << " receiver " << count << std::endl;
		}

		zmsg::ustring client_id = zm.get_part(0);
		std::unique_lock<std::mutex> lockGuard(serverInfo[serverID].mutex_queue);
		serverInfo[serverID].rpcQueue.push(client_id);
		if (serverInfo[serverID].numFreeWorkers == 0
				&& serverInfo[serverID].threadPool.size() < serverInfo[serverID].maxThreads) {
			serverInfo[serverID].threadPool.emplace_back(&WorkerThread, serverID, &socket,
					&mutex_socket);
		}
		serverInfo[serverID].conditionVariable.notify_one();
		lockGuard.unlock();

	}
}

int main(void) {
	s_version();
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(200);
	std::unique_ptr<ZeromqProxy> proxy = std::unique_ptr<ZeromqProxy>(new ZeromqProxy(context));
	proxy->StartProxy();
	boost::thread serverThread1 = boost::thread(&ServerThread, context, 0, "inproc://backendReceiver1");
	boost::thread serverThread2 = boost::thread(&ServerThread, context, 1, "inproc://backendReceiver2");
	boost::thread serverThread3 = boost::thread(&ServerThread, context, 2, "inproc://backendReceiver3");
	boost::thread serverThread4 = boost::thread(&ServerThread, context, 3, "inproc://backendReceiver4");
	boost::thread serverThread5 = boost::thread(&ServerThread, context, 4, "inproc://backendReceiver5");
	boost::thread serverThread6 = boost::thread(&ServerThread, context, 5, "inproc://backendReceiver6");
	boost::thread serverThread7 = boost::thread(&ServerThread, context, 6, "inproc://backendReceiver7");
	boost::thread serverThread8 = boost::thread(&ServerThread, context, 7, "inproc://backendReceiver8");
	boost::thread serverThread9 = boost::thread(&ServerThread, context, 8, "inproc://backendReceiver9");
	boost::thread serverThread10 = boost::thread(&ServerThread, context, 9,
			"inproc://backendReceiver10");
	while (true) {
		sleep(5);
	}
}


/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <chrono>
#include "ClientRPC.h"
#include "Util/StaticConfig.h"
#include "DTranx/Util/Log.h"
#include "DTranx/Client/Client.h"

namespace DTranx {
namespace RPC {

ClientRPC::ClientRPC(GServiceID serviceType, Client::Client *client)
		: BaseRPC(), client(client) {
	assert(client != NULL);
	SetReqService(serviceType);
	SetReqMessageID(client->GetNextMessageId());
}

ClientRPC::ClientRPC(ClientRPC&& other)
		: BaseRPC(std::forward<BaseRPC>(other)), client(std::move(other.client)) {
	other.client = NULL;
}

ClientRPC& ClientRPC::operator=(ClientRPC&& other) {
	BaseRPC(std::forward<BaseRPC>(other));
	client = std::move(other.client);
	other.client = NULL;
	return *this;
}

ClientRPC::~ClientRPC() noexcept(false) {
}

} /* namespace RPC */
} /* namespace DTranx */

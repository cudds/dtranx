#!/usr/bin/python
import argparse
import glob
import matplotlib.pyplot as plt
import numpy as np
from Utility.BashHelper import *

parser = argparse.ArgumentParser()
subparsers = parser.add_subparsers()
agg_parser = subparsers.add_parser('agg', help='aggregate throughput, latency, success rate from multiple clients or server requests numbers')
agg_parser.add_argument("-d","--dir",dest="directory",help="directory where output files are, default test files are test.output.$ip", required = True)
agg_parser.add_argument("-m","--mode",dest="mode",help="whether aggregate server info or client info, (s or c)")
agg_parser.set_defaults(subcommand = 'agg')

instant_parser = subparsers.add_parser('instant', help='plot instant throughput from multiple clients')
instant_parser.add_argument("-d","--dir",dest="directory",help="directory where output files are, default test files are test.output.$ip", required = True)
instant_parser.add_argument("-o","--output",dest="output",help="output file name", required = True)
instant_parser.add_argument("-s","--start",dest="start",help="client throughput re-aggregate, when to start")
instant_parser.add_argument("-e","--end",dest="end",help="client throughput re-aggregate, when to end")
instant_parser.set_defaults(subcommand = 'instant')

args = parser.parse_args()

if args.subcommand == 'agg':
	dirname = GetAbsolutePath(args.directory)
	if args.mode == "c":
		totalThroughput = 0.0
		totalLatency = 0.0
		totalNum = 0
		for testfile in glob.glob(dirname + 'test.output.*'):
			totalNum = totalNum + 1
			index = testfile[-3:]
			print "processing ", index
			fileHandle = open(testfile, 'r')
			for line in fileHandle:
				# found throughput line
				if line.find('workload') != -1:
					fields = line.strip().split('\t')
					if len(fields) == 4:
						try:
							totalThroughput = totalThroughput + float(fields[3].strip())
						except:
							# value error
							print "error in file ",testfile
							continue
				# found latency line
				elif line.find('avg_latency') != -1:
					fields = line.strip().split(' ')
					if len(fields) == 3:
						try:
							totalLatency = totalLatency + float(fields[1].strip())
						except:
							# value error
							print "error in file ",testfile
							continue
		print "throughput: ",totalThroughput
		print "latency: ",totalLatency/totalNum
	# aggregate server side info
	elif args.mode == "s":
		tranxService = []
		clientService = []
		storageService = []
		onepc = []
		print "tranx, client, storage, 1pc"
		for testfile in glob.glob(dirname + 'dtranx.out.*'):
			curTranxService = 0
			curClientService = 0
			curStorageService = 0
			cur1pc = 0
			index = testfile[-3:]
			print "processing ", index
			fileHandle = open(testfile, 'r')
			for line in fileHandle:
				# found tranxservice line
				if line.find('TranxService') != -1 and line.find('messages') != -1:
					fields = line.strip().split(' ')
					assert len(fields) == 9
					curTranxService = int(fields[7])
				# found clientservice line
				elif line.find('ClientService') != -1 and line.find('messages') != -1:
					fields = line.strip().split(' ')
					assert len(fields) == 9
					curClientService = int(fields[7])
				# found clientservice line
				elif line.find('StorageService') != -1 and line.find('messages') != -1:
					fields = line.strip().split(' ')
					assert len(fields) == 9
					curStorageService = int(fields[7])
				# found 1pc line	
				elif line.find('1pc') != -1 and line.find('metric') != -1:
					fields = line.strip().split(' ')
					assert len(fields) == 8
					cur1pc = int(fields[7])
			print curTranxService, curClientService, curStorageService, cur1pc
			tranxService.append(curTranxService)
			clientService.append(curClientService)
			storageService.append(curStorageService)
			onepc.append(cur1pc)
		print "tranxservice: ", np.sum(tranxService)
		print "clientservice: ", np.sum(clientService)
		print "storageservice: ", np.sum(storageService)
		print "onepc: ", np.sum(onepc)
elif args.subcommand == "instant":
	dirname = GetAbsolutePath(args.directory)
	instantThroughput = {}
	# startNum, endNum: when to start aggregating the client throughput(stable area)
	startNum = int(args.start)
	endNum = int(args.end)
	aggThroughput = 0.0
	# scan each test output file
	for testfile in glob.glob(dirname + 'test.output.*'):
		index = testfile[-3:]
		print "processing ", index
		instantThroughput[index] = []
		fileHandle = open(testfile, 'r')
		for line in fileHandle:
			# found instant throughput line
			if line.find('diff') != -1:
				fields = line.strip().split(':')
				if len(fields) == 3:
					try:
						instantThroughput[index].append(int(fields[2].strip()))
					except:
						# value error
						continue
		aggThroughput = aggThroughput + np.average(instantThroughput[index][startNum:endNum])
	print aggThroughput
	instantAggThroughput = []
	minNum = 1000000
	for index in instantThroughput.keys():
		if minNum > len(instantThroughput[index]):
			minNum = len(instantThroughput[index])
	for index in instantThroughput.keys():
		if len(instantAggThroughput) == 0:
			instantAggThroughput = instantThroughput[index][0:minNum-1]
		else:
			instantAggThroughput = [x + y for x, y in zip(instantAggThroughput, instantThroughput[index][0:minNum-1])]
	#for instantAggthroughput in instantAggThroughput:
	#	print instantAggthroughput
	#plot instant throughput
	plt.plot(instantAggThroughput)
	for index in instantThroughput.keys():
		plt.plot(instantThroughput[index])
	plt.savefig(args.output)

#include <boost/thread.hpp>
#include <mutex>
#include <memory>
#include <queue>
#include <unordered_map>

#include "zmsg.hpp"

#define reqPerThread 5000000
#define numOfThreads 10

std::unordered_map<int, std::vector<struct timespec>> latency;
std::mutex latency_mutex;
void sender(zmq::socket_t* socket, std::mutex *mutex) {
	std::unique_lock<std::mutex> lockGuard(*mutex);
	std::vector<std::string> allData;
	std::string data;
	for (int i = 0; i < 40; i++) {
		data += 'a';
	}
	allData.push_back(data);
	data.clear();
	for (int i = 0; i < 100; i++) {
		data += 'a';
	}
	allData.push_back(data);
	data.clear();
	for (int i = 0; i < 300; i++) {
		data += 'a';
	}
	allData.push_back(data);
	data.clear();

	int count = 0;
	while (count < reqPerThread) {
		count++;
		zmsg zm;
		zm.push_back(allData[rand() % 3].c_str());
		zm.send(*socket);
		if (count % 10000 == 0) {
			std::cout << "sender " << count << std::endl;
		}
	}
}

void sender_1(std::vector<zmq::socket_t*> sockets, std::vector<std::mutex *> mutex) {
	std::vector<std::string> allData;
	std::string data;
	for (int i = 0; i < 40; i++) {
		data += 'a';
	}
	allData.push_back(data);
	data.clear();
	for (int i = 0; i < 100; i++) {
		data += 'a';
	}
	allData.push_back(data);
	data.clear();
	for (int i = 0; i < 300; i++) {
		data += 'a';
	}
	allData.push_back(data);
	data.clear();

	std::cout << data.size() << " bytes per message" << std::endl;
	int socketIndex = -1;
	int numOfSockets = sockets.size();
	int count = 0;
	while (count < reqPerThread) {
		count++;
		zmsg zm;
		zm.push_back((allData[rand() % 3] + std::to_string(count)).c_str());
		if (count % 10000 == 0) {
			struct timespec now;
			clock_gettime(CLOCK_MONOTONIC, &now);
			std::cout << "sending message count " << count << " at " << now.tv_sec << "."
					<< now.tv_nsec << std::endl;
			latency_mutex.lock();
			assert(latency.find(count) == latency.end());
			latency[count].push_back(now);
			latency_mutex.unlock();
		}
		socketIndex = (socketIndex + 1) % numOfSockets;
		mutex[socketIndex]->lock();
		zm.send(*sockets[socketIndex]);
		mutex[socketIndex]->unlock();
		if (count % 10000 == 0) {
			std::cout << "sender " << count << std::endl;
		}
	}
}

void receiver(zmq::socket_t* socket, std::mutex *mutex) {
	std::unique_lock<std::mutex> lockGuard(*mutex);
	int count = 0;
	while (count < reqPerThread) {
		zmsg zm;
		if (zm.recv_non_block(*socket)) {
			count++;
		}
		if (count % 10000 == 1) {
			std::cout << "receiver " << count << std::endl;
		}
		lockGuard.unlock();
		usleep(50);
		lockGuard.lock();
	}
}

void receiver_1(std::vector<zmq::socket_t*> sockets, std::vector<std::mutex *> mutex) {
	int count = 0;
	int numOfSockets = sockets.size();
	while (count < reqPerThread) {
		for (int i = 0; i < numOfSockets; ++i) {
			while (true) {
				zmsg zm;
				mutex[i]->lock();
				if (zm.recv_non_block(*sockets[i])) {
					mutex[i]->unlock();
					count++;
					zmsg::ustring request = zm.get_part(1);
					int size = request.size();
					uint8_t *requestArray = new uint8_t[size];
					memcpy((void*) requestArray, request.data(), size);
					int count_recv;
					if (size < 60) {
						count_recv = stoi(std::string((char *) requestArray + 40, size - 40), NULL,
								10);
					} else if (size < 120) {
						count_recv = stoi(std::string((char *) requestArray + 100, size - 100),
						NULL, 10);
					} else if (size < 320) {
						count_recv = stoi(std::string((char *) requestArray + 300, size - 300),
						NULL, 10);
					}
					if (count_recv % 10000 == 0) {
						struct timespec now;
						clock_gettime(CLOCK_MONOTONIC, &now);
						std::cout << "receiving message count " << count_recv << " at "
								<< now.tv_sec << "." << now.tv_nsec << std::endl;
						latency_mutex.lock();
						assert(latency.find(count_recv) != latency.end());
						latency[count_recv].push_back(now);
						latency_mutex.unlock();
					}
				} else {
					mutex[i]->unlock();
					break;
				}
			}
			if (count % 1000 == 1) {
				std::cout << "receiver " << count << std::endl;
			}
		}
		usleep(500);
	}
}

int diff_ms(timespec t1, timespec t2) {
	return (t1.tv_sec - t2.tv_sec) * 1000000 + (t1.tv_nsec - t2.tv_nsec) / 1000;
}

int main(int argc, char **argv) {
	s_version();
	if (argc != 2) {
		std::cout << "provide the ip address you want to bind" << std::endl;
		return 0;
	}
	std::string selfAddress(argv[1]);
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(200);
	std::vector<zmq::socket_t *> sockets;
	std::vector<std::mutex *> mutexs;

	for (int i = 0; i < numOfThreads; ++i) {
		sockets.push_back(new zmq::socket_t(*context, ZMQ_DEALER));
		int lingtime = 0;
		sockets.back()->setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
		int hwm = 0;
		size_t len = sizeof(hwm);
		sockets.back()->setsockopt(ZMQ_SNDHWM, &hwm, len);
		sockets.back()->setsockopt(ZMQ_RCVHWM, &hwm, len);
		if (i % 10 == 0) {
			sockets.back()->connect(std::string("tcp://" + selfAddress+ ":2222").c_str());
		} else if (i % 10 == 1) {
			sockets.back()->connect(std::string("tcp://" + selfAddress+ ":2223").c_str());
		} else if (i % 10 == 2) {
			sockets.back()->connect(std::string("tcp://" + selfAddress+ ":2224").c_str());
		} else if (i % 10 == 3) {
			sockets.back()->connect(std::string("tcp://" + selfAddress+ ":2225").c_str());
		} else if (i % 10 == 4) {
			sockets.back()->connect(std::string("tcp://" + selfAddress+ ":2226").c_str());
		} else if (i % 10 == 5) {
			sockets.back()->connect(std::string("tcp://" + selfAddress+ ":2227").c_str());
		} else if (i % 10 == 6) {
			sockets.back()->connect(std::string("tcp://" + selfAddress+ ":2228").c_str());
		} else if (i % 10 == 7) {
			sockets.back()->connect(std::string("tcp://" + selfAddress+ ":2229").c_str());
		} else if (i % 10 == 8) {
			sockets.back()->connect(std::string("tcp://" + selfAddress+ ":2230").c_str());
		} else if (i % 10 == 9) {
			sockets.back()->connect(std::string("tcp://" + selfAddress+ ":2231").c_str());
		}

	}

	std::vector<boost::thread> sendingThreads;
	std::vector<boost::thread> receivingThreads;

	std::chrono::system_clock::time_point start = std::chrono::system_clock::now();

	mutexs.push_back(new std::mutex());

	for (int i = 0; i < numOfThreads; ++i) {
		mutexs.push_back(new std::mutex());
	}

	sendingThreads.push_back(boost::thread(sender_1, sockets, mutexs));
	receivingThreads.push_back(boost::thread(receiver_1, sockets, mutexs));

	sendingThreads.back().join();
	std::cout << "sender joined" << std::endl;
	receivingThreads.back().join();
	std::cout << "receiver joined" << std::endl;

	for (int i = 0; i < numOfThreads; ++i) {
		sockets[i]->close();
	}
	context->close();

	std::chrono::system_clock::time_point end = std::chrono::system_clock::now();

	int timePassed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

	std::cout << "throughout: " << 1.0 * reqPerThread * 1000 / timePassed << std::endl;

	int total;
	int max, min;
	total = 0;
	max = -1;
	int maxCount = 0;
	min = 0x7fffffff;
	for (auto it = latency.begin(); it != latency.end(); ++it) {
		assert(it->second.size() == 2);
		int curLatency = diff_ms(it->second[1], it->second[0]);
		total += curLatency;
		if (max < curLatency) {
			max = curLatency;
			maxCount = it->first;
		}
		if (min > curLatency) {
			min = curLatency;
		}
	}
	std::cout << "avg: " << total * 1.0 / latency.size() << "microseconds" << std::endl;
	std::cout << "max: " << max << "microseconds at " << maxCount << std::endl;
	std::cout << "min: " << min << "microseconds" << std::endl;
	return 0;
}

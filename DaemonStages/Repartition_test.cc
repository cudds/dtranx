/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "Repartition.h"
#include "Util/FileUtil.h"
#include "Stages/Log/TranxLog.h"

using namespace DTranx;

TEST(Repartition, OptimizeForDataMigration) {
	std::vector<std::string> allNodes;
	allNodes.push_back("192.168.0.1");
	allNodes.push_back("192.168.0.2");
	allNodes.push_back("192.168.0.3");
	allNodes.push_back("192.168.0.4");
	allNodes.push_back("192.168.0.5");
	DTranx::Util::ConfigHelper configHelper;
	configHelper.readFile("DTranx.conf");
	DTranx::SharedResources::TranxServerInfo *tranxServerInfo =
			new DTranx::SharedResources::TranxServerInfo();
	tranxServerInfo->SetAllNodes(allNodes);
	tranxServerInfo->SetConfig(configHelper);
	DTranx::SharedResources::TranxServiceSharedData *tranxServiceSharedData =
			new DTranx::SharedResources::TranxServiceSharedData(1, tranxServerInfo);

	DaemonStages::Repartition repartition(tranxServerInfo, tranxServiceSharedData);
	std::unordered_map<uint64, std::unordered_set<int> > oldMappings, newMappings;
	/*
	 * old
	 * 1: 0,1
	 * 2: 1,2,3
	 * 3: 3,4
	 * 4: 0,2,4
	 *
	 * new
	 * 1: 1,2
	 * 2: 1,3,4
	 * 3: 0,3
	 * 4: 0,2,4
	 *
	 * good mapping
	 * new-> old
	 * 0->4
	 * 1->1
	 * 2->0
	 * 3->3
	 * 4->2
	 */
	oldMappings[1].insert(0);
	oldMappings[1].insert(1);
	oldMappings[2].insert(1);
	oldMappings[2].insert(2);
	oldMappings[2].insert(3);
	oldMappings[3].insert(3);
	oldMappings[3].insert(4);
	oldMappings[4].insert(0);
	oldMappings[4].insert(2);
	oldMappings[4].insert(4);

	newMappings[1].insert(1);
	newMappings[1].insert(2);
	newMappings[2].insert(1);
	newMappings[2].insert(3);
	newMappings[2].insert(4);
	newMappings[3].insert(0);
	newMappings[3].insert(3);
	newMappings[4].insert(0);
	newMappings[4].insert(2);
	newMappings[4].insert(4);
	std::unordered_map<int, int> partitionMapping = repartition.OptimizeForDataMigration(
			oldMappings, newMappings);
	EXPECT_EQ(4, partitionMapping[0]);
	EXPECT_EQ(1, partitionMapping[1]);
	EXPECT_EQ(0, partitionMapping[2]);
	EXPECT_EQ(3, partitionMapping[3]);
	EXPECT_EQ(2, partitionMapping[4]);
	delete tranxServerInfo;
	delete tranxServiceSharedData;
	Util::FileUtil::RemoveDir("dtranx.mapdb");
}

TEST(Repartition, CollectTranxHistoryFromGCLog) {
	/*
	 * When getting a logfile request in the queue, repartition
	 * will collect the committed transactions based on commitHistory from the reclaimed log file
	 */
	std::vector<std::string> allNodes;
	allNodes.push_back("192.168.0.1");
	allNodes.push_back("192.168.0.2");
	allNodes.push_back("192.168.0.3");
	allNodes.push_back("192.168.0.4");
	allNodes.push_back("192.168.0.5");
	Util::ConfigHelper configHelper;
	configHelper.readFile("DTranx.conf");
	DTranx::SharedResources::TranxServerInfo *tranxServerInfo =
			new DTranx::SharedResources::TranxServerInfo();
	tranxServerInfo->SetAllNodes(allNodes);
	tranxServerInfo->SetConfig(configHelper);
	DTranx::SharedResources::TranxServiceSharedData *tranxServiceSharedData =
			new DTranx::SharedResources::TranxServiceSharedData(1, tranxServerInfo);
	Stages::Log::LogOption logOption =
			(configHelper.read("LogOption") == "Pmem") ? Stages::Log::PMEM : Stages::Log::DISK;
	Stages::Log::TranxLog *tranxLog = new Stages::Log::TranxLog("./dtranx.log", tranxServerInfo);
	Stages::Log::GTranxLogRecord logRecord;
	logRecord.mutable_tranxid()->set_nodeid(1);
	logRecord.mutable_tranxid()->set_tranxid(1);
	logRecord.set_logtype(Stages::Log::GLogType::PREP);
	Service::GItem *item = logRecord.add_write_set();
	item->set_key("key1");
	item->set_value("value1_1");
	item = logRecord.add_read_set();
	item->set_key("key2");
	item->set_value("value2_1");
	std::vector<std::string> ips;
	ips.push_back(configHelper.read<std::string>("SelfAddress"));
	Util::TranxID tranxID(1, 1);
	tranxLog->Prepare(tranxID, logRecord.read_set(), logRecord.write_set(), ips, false);
	delete tranxLog;
	DaemonStages::Repartition repartition(tranxServerInfo, tranxServiceSharedData);
	repartition.localCommitHistorySet.insert(tranxID);
	repartition.StartService();
	std::set<std::string> files = DTranx::Util::FileUtil::LsPrefix("./", "dtranx.log");
	EXPECT_EQ(1, files.size());
	repartition.SendToLogRepartition(*files.begin());
	sleep(5);
	EXPECT_EQ(1, repartition.tranxServiceSharedData->tranxHistory->tranxList.size());
	delete tranxServerInfo;
	delete tranxServiceSharedData;
	Util::FileUtil::RemoveDir("dtranx.mapdb");
	repartition.ShutService();
}

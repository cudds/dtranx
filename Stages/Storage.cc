/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "Storage.h"
#include "Service/TranxService.h"
#include "Service/StorageService.h"
#include "Service/ClientService.h"
#include "DaemonStages/Repartition.h"
#include "DaemonStages/TranxAck.h"
#include "DaemonStages/DaemonStagesMock.h"
#include "Server/RecoverLog.h"

namespace DTranx {
namespace Stages {

StringStorage::StringStorage(SharedResources::TranxServerInfo *tranxServerInfo,
		int numOfStageThreads)
		: MidStage(tranxServerInfo, numOfStageThreads, "Storage"), cache() {
	std::string dbName = tranxServerInfo->GetConfig().read("DBFileName");

	for (int i = 0; i < numOfStageThreads; ++i) {
		currentEpoch.push_back(std::stoull(Util::StaticConfig::GetInstance()->Read("FirstEpoch")));
		leveldb::DB *tmpDB;
		options.push_back(leveldb::Options());
		options[i].create_if_missing = true;
		options[i].block_cache = leveldb::NewLRUCache(100 * 1024 * 1024);
		leveldb::Status status = leveldb::DB::Open(options[i], dbName + std::to_string(i), &tmpDB);

		if (false == status.ok()) {
			ERROR("Unable to open/create database %s: %s", (dbName + std::to_string(i)).c_str(),
					status.ToString().c_str());
			assert(false);
		}

		db.push_back(tmpDB);
		leveldb::ReadOptions readOptions;
		std::string epochValue;
		status = db[i]->Get(readOptions, EPOCHKEY, &epochValue);
		if (status.IsNotFound()) {
			leveldb::WriteOptions writeOptions;
			writeOptions.sync = true;
			db[i]->Put(writeOptions, EPOCHKEY, std::to_string(currentEpoch[i]));
		} else {
			currentEpoch[i] = std::stoull(epochValue);
		}
		VERBOSE("storage %d initialized and epoch is %lu", i, currentEpoch[i]);

		cache.push_back(std::unordered_map<std::string, std::pair<uint64, std::string>>());
		snapshotCache.push_back(std::unordered_map<std::string, std::string>());
	}
}

void StringStorage::SendBackToPreviousStage(TranxServiceQueueElement *element,
		int stageThreadIndex) {
	/*
	 * first check if there are still some stage threads that this element should go through
	 * 	if complete, send back to the daemonstage/service
	 */
	StorageStageTask task = element->getStorageTask();
	int nextStageThread = element->popSplittedNextInvolvedStageThreads();
	if (nextStageThread != TranxServiceQueueElement::INVALID_STAGE_THREADID) {
		LeaveStage(element, stageThreadIndex, task);
		SendToStage(element, nextStageThread);
	} else {
		TranxServiceQueueElement::StageID stageID = element->getStageId();
		if (element->isTerminateInThisStage()) {
			if (stageID == Stages::TranxServiceQueueElement::StageID::TRANXSERVICE) {
				tranxService->AddToFreeList(element);
			} else if (stageID == Stages::TranxServiceQueueElement::StageID::CLIENTSERVICE) {
				clientService->AddToFreeList(element);
			} else if (stageID == Stages::TranxServiceQueueElement::StageID::STORAGESERVICE) {
				storageService->AddToFreeList(element);
			} else if (stageID == Stages::TranxServiceQueueElement::StageID::TRANXACK) {
				/*
				 * tranxAck elements were originally created in ClientService
				 */
				clientService->AddToFreeList(element);
			} else {
				delete element;
			}
			return;
		}
		LeaveStage(element, stageThreadIndex, task);
		if (stageID == TranxServiceQueueElement::StageID::TRANXSERVICE) {
			tranxService->SendToServiceInternal(element);
		} else if (stageID == Stages::TranxServiceQueueElement::StageID::CLIENTSERVICE) {
			clientService->SendToServiceInternal(element);
		} else if (stageID == Stages::TranxServiceQueueElement::StageID::STORAGESERVICE) {
			storageService->SendToServiceInternal(element);
		} else if (stageID == Stages::TranxServiceQueueElement::StageID::TRANXACK) {
			tranxAck->SendToDaemon(element);
		} else if (stageID == Stages::TranxServiceQueueElement::StageID::REPARTITION) {
			repartition->SendToDaemon(element);
		} else if (stageID == Stages::TranxServiceQueueElement::StageID::RECOVERY) {
			postRecovery->SendToDaemon(element);
		} else if (stageID == Stages::TranxServiceQueueElement::StageID::UNITTESTDAEMON) {
			daemonStagesMock->SendToDaemon(element);
		} else {
			assert(false);
		}
	}
}

/*
 * DispatchThread assigns the next stage threads in the queue element
 * 	however each thread still needs to call GetThreadByKey to get the corresponding keys that the
 * 	current thread stores
 */
void StringStorage::DispatchThread() {
	NOTICE("StorageThread Dispatch started");
	uint64_t randSeed = 0;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateThread.load()) {
			NOTICE("StorageThread Dispatch is reclaimed");
			break;
		}
		TranxServiceQueueElement *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = dispatchQueue.try_dequeue_bulk(tmpRPCs,
				TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1) {
				AddQueueLengthSample(5, dispatchQueue.size_approx());
			}
		}

		for (int i = 0; i < count; ++i) {
			TranxServiceQueueElement *element = tmpRPCs[i];
			if (element->isIsPerfOutPutTask()) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(5);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(5);
				std::vector<int> taskLongLatencySamples = GetLongLatency(5);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_dispatch");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				std::set<int> involvedStageThreads;
				for (int i = 1; i < numOfStageThreads; ++i) {
					involvedStageThreads.insert(i);
				}
				element->setSplittedInvolvedStageThreads(involvedStageThreads);
				element->setTerminateInThisStage(true);
				SendToStage(element, 0);
				continue;
			}
			//element->goThroughStage("storage dispatch");
			StorageStageTask task = element->getStorageTask();
			if (task == StorageStageTask::_CheckReadSet) {
				std::unordered_map<std::string, uint64> readSet = element->getReadSet();
				std::set<int> involvedStageThreads;
				for (auto readItem = readSet.begin(); readItem != readSet.end(); ++readItem) {
					involvedStageThreads.insert(GetThreadByKey(readItem->first));
				}
				if (involvedStageThreads.empty()) {
					LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
					SendToStage(element, randSeed++ % numOfStageThreads);
				} else {
					int nextStageThread = *involvedStageThreads.begin();
					involvedStageThreads.erase(involvedStageThreads.begin());
					element->setSplittedInvolvedStageThreads(involvedStageThreads);
					LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
					SendToStage(element, nextStageThread);
				}
			} else if (task == StorageStageTask::_CreateSnapshot) {
				std::set<int> involvedStageThreads;
				for (int i = 1; i < numOfStageThreads; ++i) {
					involvedStageThreads.insert(i);
				}
				element->setSplittedInvolvedStageThreads(involvedStageThreads);
				LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
				SendToStage(element, 0);
			} else if (task == StorageStageTask::_Write) {
				std::unordered_map<std::string, std::string> writeSet = element->getWriteSet();
				std::set<int> involvedStageThreads;
				for (auto writeItem = writeSet.begin(); writeItem != writeSet.end(); ++writeItem) {
					involvedStageThreads.insert(GetThreadByKey(writeItem->first));
				}
				if (involvedStageThreads.empty()) {
					LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
					SendToStage(element, randSeed++ % numOfStageThreads);
				} else {
					int nextStageThread = *involvedStageThreads.begin();
					involvedStageThreads.erase(involvedStageThreads.begin());
					element->setSplittedInvolvedStageThreads(involvedStageThreads);
					LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
					SendToStage(element, nextStageThread);
				}
			} else if (task == StorageStageTask::_WritePlain) {
				/*
				 * Bug fix here: writekey needs to be processed to get the hash
				 * because the plain key stored in the database is key#epoch
				 */
				std::unordered_map<std::string, std::string> writeSet = element->getWriteSet();
				std::set<int> involvedStageThreads;
				for (auto writeItem = writeSet.begin(); writeItem != writeSet.end(); ++writeItem) {
					uint64_t epoch;
					std::string realKey;
					SeparateKey(writeItem->first, epoch, realKey);
					involvedStageThreads.insert(GetThreadByKey(realKey));
				}
				if (involvedStageThreads.empty()) {
					LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
					SendToStage(element, randSeed++ % numOfStageThreads);
				} else {
					int nextStageThread = *involvedStageThreads.begin();
					involvedStageThreads.erase(involvedStageThreads.begin());
					element->setSplittedInvolvedStageThreads(involvedStageThreads);
					LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
					SendToStage(element, nextStageThread);
				}
			} else if (task == StorageStageTask::_ReadSnapshot) {
				std::string key =
						element->GetServerRPC()->GetStorageRequest()->readdata().request().key();
				LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
				SendToStage(element, GetThreadByKey(key));
			} else if (task == StorageStageTask::_ReadWithVersion) {
				std::string key =
						element->GetServerRPC()->GetStorageRequest()->readdata().request().key();
				LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
				SendToStage(element, GetThreadByKey(key));
			} else if (task == StorageStageTask::_SearchAll) {
				std::vector<std::string> searchKeys = element->getStorageKeys();
				std::set<int> involvedStageThreads;
				for (auto key = searchKeys.begin(); key != searchKeys.end(); ++key) {
					involvedStageThreads.insert(GetThreadByKey(*key));
				}
				if (involvedStageThreads.empty()) {
					LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
					SendToStage(element, randSeed++ % numOfStageThreads);
				} else {
					int nextStageThread = *involvedStageThreads.begin();
					involvedStageThreads.erase(involvedStageThreads.begin());
					element->setSplittedInvolvedStageThreads(involvedStageThreads);
					LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
					SendToStage(element, nextStageThread);
				}
			} else if (task == StorageStageTask::_Delete) {
				std::vector<std::string> deleteKeys = element->getStorageKeys();
				std::set<int> involvedStageThreads;

				for (auto key = deleteKeys.begin(); key != deleteKeys.end(); ++key) {
					involvedStageThreads.insert(GetThreadByKey(*key));
				}
				if (involvedStageThreads.empty()) {
					LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
					SendToStage(element, randSeed++ % numOfStageThreads);
				} else {
					int nextStageThread = *involvedStageThreads.begin();
					involvedStageThreads.erase(involvedStageThreads.begin());
					element->setSplittedInvolvedStageThreads(involvedStageThreads);
					LeaveStage(element, TranxServiceQueueElement::DISPATCH_STAGE_THREADID, task);
					SendToStage(element, nextStageThread);
				}
			} else {
				assert(false);
			}
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void StringStorage::StageThread(int stageThreadIndex) {
	NOTICE("StorageThread %d started", stageThreadIndex);
	uint64_t randSeed = 0;
	SharedResources::TranxServerInfo::Mode mode = tranxServerInfo->GetMode();
	while (true) {
		if (terminateThread.load()) {
			NOTICE("StorageThread %d is reclaimed", stageThreadIndex);
			break;
		}
		TranxServiceQueueElement *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = 0;

		if (numOfStageThreads > 1) {
			count = processQueues[stageThreadIndex].try_dequeue_bulk(tmpRPCs,
					TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
			if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
				if (randSeed++ % queuePerfFreq == 1) {
					AddQueueLengthSample(stageThreadIndex,
							processQueues[stageThreadIndex].size_approx());
				}
			}
		} else {
			count = processQueue.try_dequeue_bulk(tmpRPCs,
					TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
			if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
				if (randSeed++ % queuePerfFreq == 1) {
					AddQueueLengthSample(0, processQueue.size_approx());
				}
			}
		}

		/*
		 * aggregate all write in a batch
		 */
		std::unordered_set<TranxServiceQueueElement *> writeRequests;
		writeRequests.reserve(20);
		std::unordered_map<std::string, std::string> writeBatch;
		writeBatch.reserve(20);
		for (int i = 0; i < count; ++i) {
			TranxServiceQueueElement *element = tmpRPCs[i];
			//element->goThroughStage("storage" + std::to_string(stageThreadIndex));
			StorageStageTask task = element->getStorageTask();
			if (!element->isIsPerfOutPutTask() && task == StorageStageTask::_Write) {
				writeRequests.insert(element);
			}
		}
		for (auto it = writeRequests.begin(); it != writeRequests.end(); ++it) {
			const std::unordered_map<std::string, std::string>& writeSet = (*it)->getWriteSet();
			for (auto it_b = writeSet.begin(); it_b != writeSet.end(); ++it_b) {
				if (GetThreadByKey(it_b->first) == stageThreadIndex) {
					//VERBOSE("storage write %s", it_b->first.c_str());
					writeBatch[it_b->first] = it_b->second;
				}
			}
		}
		if (writeBatch.size() != 0) {
			WriteBatch(writeBatch, stageThreadIndex);
		}
		for (auto it = writeRequests.begin(); it != writeRequests.end(); ++it) {
			SendBackToPreviousStage(*it, stageThreadIndex);
		}

		for (int i = 0; i < count; ++i) {
			TranxServiceQueueElement *element = tmpRPCs[i];
			if (writeRequests.find(element) != writeRequests.end()) {
				continue;
			}
			//element->goThroughStage("storage" + std::to_string(stageThreadIndex));
			StorageStageTask task = element->getStorageTask();

			if (element->isIsPerfOutPutTask()) {
				/*
				 * the setTerminate is necessary because it might've skipped the dispatch because the numOfStageThreads is 1
				 */
				element->setTerminateInThisStage(true);
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(
						stageThreadIndex);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(
						stageThreadIndex);
				std::vector<int> taskLongLatencySamples = GetLongLatency(stageThreadIndex);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(
						stageName + std::to_string(stageThreadIndex));
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
			} else {
				if (task == StorageStageTask::_CheckReadSet) {
					try {
						const std::unordered_map<std::string, uint64>& readSet =
								element->getReadSet();
						bool checkSucc = true;
						for (auto it_b = readSet.begin(); it_b != readSet.end(); ++it_b) {
							if (GetThreadByKey(it_b->first) == stageThreadIndex) {
								if (GetVersion(it_b->first, stageThreadIndex) != it_b->second) {
									checkSucc = false;
									break;
								}
							}
						}
						element->setStorageCheckSucc(checkSucc);
						if (!checkSucc) {
							element->clearSplittedInvolvedStageThreads();
						}
					} catch (Util::NotFoundException &e) {
						element->setStorageCheckSucc(false);
					}
				} else if (task == StorageStageTask::_CreateSnapshot) {
					CreateSnapshot(stageThreadIndex);
				} else if (task == StorageStageTask::_Write) {
					assert(false);
				} else if (task == StorageStageTask::_WritePlain) {
					std::unordered_map<std::string, std::string> writeSet = element->getWriteSet();
					for (auto it_b = writeSet.begin(); it_b != writeSet.end(); ++it_b) {
						uint64_t epoch;
						std::string realKey;
						SeparateKey(it_b->first, epoch, realKey);
						if (GetThreadByKey(realKey) == stageThreadIndex) {
							WritePlain(it_b->first, it_b->second, stageThreadIndex);
						}
					}
				} else if (task == StorageStageTask::_ReadSnapshot) {
					uint64 epoch;
					std::string value;
					try {
						std::string key =
								element->GetServerRPC()->GetStorageRequest()->readdata().request().key();
						assert(GetThreadByKey(key) == stageThreadIndex);
						bool snapshotExist = ReadSnapshot(key, epoch, value, stageThreadIndex);
						element->setStorageKeyExist(true);
						element->setStorageSnapshotExist(snapshotExist);
						if (snapshotExist) {
							element->setStorageEpoch(epoch);
							element->setStorageValue(value);
						}
					} catch (Util::NotFoundException &e) {
						element->setStorageKeyExist(false);
					}
				} else if (task == StorageStageTask::_ReadWithVersion) {
					try {
						const std::string& key =
								element->GetServerRPC()->GetStorageRequest()->readdata().request().key();
						assert(GetThreadByKey(key) == stageThreadIndex);
						std::pair<int64, std::string> data = ReadWithVersion(key, stageThreadIndex);
						element->setStorageKeyExist(true);
						element->setStorageVersion(data.first);
						element->setStorageValue(data.second);
					} catch (Util::NotFoundException &e) {
						element->setStorageKeyExist(false);
					}
				} else if (task == StorageStageTask::_SearchAll) {
					std::vector<std::string> searchKeys = element->getStorageKeys();
					std::vector<std::vector<std::pair<std::string, std::string>>>searchAllResults;
					for (auto key = searchKeys.begin(); key != searchKeys.end(); ++key) {
						if (GetThreadByKey(*key) == stageThreadIndex) {
							searchAllResults.push_back(searchAll(*key, stageThreadIndex));
						}
					}
					element->addStorageSearchAllResults(searchAllResults);
				} else if (task == StorageStageTask::_Delete) {
					std::vector<std::string> deleteKeys = element->getStorageKeys();
					for (auto key = deleteKeys.begin(); key != deleteKeys.end(); ++key) {
						if (GetThreadByKey(*key) == stageThreadIndex) {
							Delete(*key, stageThreadIndex);
						}
					}
				} else {
					assert(false);
				}
			}
			SendBackToPreviousStage(element, stageThreadIndex);
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}
} /* namespace Stages */
} /* namespace DTranx */


/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <chrono>
#include "TranxPeer.h"
#include "DTranx/Util/Log.h"
#include "Util/StaticConfig.h"
#include "Util/ThreadHelper.h"
#include "Util/StringUtil.h"
#include "TranxRPCHelper.h"

namespace DTranx {
namespace Stages {

TranxPeer::TranxPeer(TranxRPCHelper* tranxRPCHelper,
		SharedResources::TranxServerInfo *tranxServerInfo,
		std::vector<uint32_t> coreIDs)
		: PerfStage("tranxpeer"), terminateThread(false), tranxRPCHelper(tranxRPCHelper),
				SendProcessQueue(1000), RecvProcessQueue(1000), bufferSize(300), localBuffer() {
	mode = tranxServerInfo->GetMode();
	selfAddressStr = tranxServerInfo->GetSelfAddress();
	localBuffer.SetData(new uint8[bufferSize], bufferSize);
	selfAddress = new uint32_t[4];
	std::vector<std::string> tmpFields = Util::StringUtil::Split(selfAddressStr, '.');
	assert(tmpFields.size() == 4);
	for (int i = 0; i < 4; ++i) {
		selfAddress[i] = std::stoul(tmpFields[i], nullptr, 10);
	}
	if (mode == SharedResources::TranxServerInfo::Mode::NORMAL
			|| mode == SharedResources::TranxServerInfo::Mode::INTEGTEST) {
		std::vector<std::string> peerPortsStr = Util::StringUtil::Split(
				tranxServerInfo->GetConfig().read("peerReceiverPort"), ',');
		std::vector<uint32_t> peerPorts;
		for (auto port = peerPortsStr.begin(); port != peerPortsStr.end(); ++port) {
			peerPorts.push_back(std::strtoul(port->c_str(), nullptr, 10));
		}
		std::vector<std::string> allNodes = tranxServerInfo->GetAllNodes();
		std::shared_ptr<zmq::context_t> context = tranxServerInfo->GetContext();
		std::string routerPort = Util::StringUtil::Split(
				tranxServerInfo->GetConfig().read("routerFrontPort"), ',')[0];
		assert(allNodes.size() - 1 == peerPorts.size());
		int curPeerPort = 0;

		for (auto it = allNodes.begin(); it != allNodes.end(); ++it) {
			if (*it == selfAddressStr) {
				continue;
			}
			NOTICE("peer %s connect port %s", it->c_str(), routerPort.c_str());
			senders[*it] = new RPC::ZeromqSender(context);
			senders[*it]->ConnectTcp(*it, routerPort);
			NOTICE("peer %s bind port %u", it->c_str(), peerPorts[curPeerPort]);
			receivers[*it] = new RPC::ZeromqReceiver(context);
			receivers[*it]->Bind(selfAddressStr, peerPortsStr[curPeerPort]);
			listenPortMap[*it] = peerPorts[curPeerPort];
			nextMessageId[*it] = std::strtoull(
					Util::StaticConfig::GetInstance()->Read("FirstID").c_str(), NULL, 10);
			responses[*it] = std::unordered_map<uint64, RPC::TranxRPC*>();
			curPeerPort++;
		}
		StartPeerThread(coreIDs);
	}
}

TranxPeer::~TranxPeer() noexcept(false) {
	KillPeerThread();
	for (auto it = senders.begin(); it != senders.end(); ++it) {
		delete it->second;
	}
	for (auto it = receivers.begin(); it != receivers.end(); ++it) {
		delete it->second;
	}
	delete[] selfAddress;
}

void TranxPeer::StartPeerThread(std::vector<uint32_t> coreIDs) {
	bool enableCoreBinding = false;
	uint32_t coreIDIndex = 0;
	enableCoreBinding = (coreIDs.size() == 2);
	peerSendThread = boost::thread(&TranxPeer::PeerSendThread, this);
	if (enableCoreBinding) {
		Util::ThreadHelper::PinToCPUCore(peerSendThread, coreIDs[coreIDIndex++]);
	}
	peerRecvThread = boost::thread(&TranxPeer::PeerRecvThread, this);
	if (enableCoreBinding) {
		Util::ThreadHelper::PinToCPUCore(peerRecvThread, coreIDs[coreIDIndex++]);
	}
}

void TranxPeer::KillPeerThread() {
	terminateThread.store(true);
	if (peerSendThread.joinable()) {
		peerSendThread.join();
	}
	if (peerRecvThread.joinable()) {
		peerRecvThread.join();
	}
}

void TranxPeer::PeerSendThread() {
	NOTICE("PeerSendThread started");
	uint64_t randSeed = 0;
	while (true) {
		if (terminateThread.load()) {
			NOTICE("PeerSendThread is reclaimed");
			break;
		}
		RPC::TranxRPC *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = SendProcessQueue.try_dequeue_bulk(tmpRPCs,
				TranxServiceQueueElement::DEQUEUE_BULK_SIZE);

		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1) {
				AddQueueLengthSample(8, SendProcessQueue.size_approx());
			}
		}
		for (int i = 0; i < count; ++i) {
			RPC::TranxRPC *element = tmpRPCs[i];
			if (element->getStageTask() == TranxPeerStageTask::_OUTPUTPERFPEER) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(8);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(8);
				std::vector<int> taskLongLatencySamples = GetLongLatency(8);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_send");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				SendToPeerRecv(element);
				continue;
			}
			if (element->getStageTask() == TranxPeerStageTask::_INFORMPEERRECV
					&& element->IsResponsesNeeded()) {
				SendToPeerRecv(element);
			} else {
				const std::string& remoteIP = element->getRemoteIp();
				assert(!remoteIP.empty());

				RPC::GRPCRequestMessage *message = element->GetRequest();
				message->mutable_ipint()->set_fieldone(selfAddress[0]);
				message->mutable_ipint()->set_fieldtwo(selfAddress[1]);
				message->mutable_ipint()->set_fieldthree(selfAddress[2]);
				message->mutable_ipint()->set_fieldfour(selfAddress[3]);

				message->set_port(listenPortMap[remoteIP]);
				/*
				 * smartly share local buffer, when it's less than bufferSize, reuse
				 * the local buffer memory, or else, alocate new memory
				 */
				if (message->ByteSize() <= bufferSize) {
					RPC::BaseRPC::serializeWithMem(*message, localBuffer, bufferSize);
					senders[remoteIP]->Send(localBuffer.GetData(), localBuffer.GetLength());
				} else {
					Util::Buffer localBufferTmp;
					RPC::BaseRPC::serialize(*message, localBufferTmp, 0);
					senders[remoteIP]->Send(localBufferTmp.GetData(), localBufferTmp.GetLength());
				}
				if (!element->IsResponsesNeeded()) {
					delete element;
				}
			}
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

void TranxPeer::PeerRecvThread() {
	NOTICE("PeerRecvThread started");
	uint64_t randSeed = 0;
	std::vector<std::pair<void *, int> > curResponses;
	curResponses.reserve(100);
	while (true) {
		if (terminateThread.load()) {
			NOTICE("PeerRecvThread is reclaimed");
			break;
		}
		RPC::TranxRPC *tmpRPCs[TranxServiceQueueElement::DEQUEUE_BULK_SIZE];
		size_t count = RecvProcessQueue.try_dequeue_bulk(tmpRPCs,
				TranxServiceQueueElement::DEQUEUE_BULK_SIZE);
		if (IsEnabled(Stages::PerfStage::PerfKind::QUEUE_LENGTH)) {
			if (randSeed++ % queuePerfFreq == 1) {
				AddQueueLengthSample(9, RecvProcessQueue.size_approx());
			}
		}

		for (int i = 0; i < count; ++i) {
			RPC::TranxRPC *element = tmpRPCs[i];

			if (element->getStageTask() == TranxPeerStageTask::_OUTPUTPERFPEER) {
				std::vector<uint32_t> responseTimeSamples = GetResponseTimeSamplesAndClear(9);
				std::vector<uint32_t> queueLengthSamples = GetQueueLengthSamplesAndClear(9);
				std::vector<int> taskLongLatencySamples = GetLongLatency(9);
				Util::ProfileLog profileLog;
				profileLog.set_type(Util::ProfileType::PERF);
				profileLog.mutable_perflog()->set_stagename(stageName + "_recv");
				for (auto responseTimeSample = responseTimeSamples.begin();
						responseTimeSample != responseTimeSamples.end(); ++responseTimeSample) {
					profileLog.mutable_perflog()->add_timensec(*responseTimeSample);
				}
				for (auto queueLengthSample = queueLengthSamples.begin();
						queueLengthSample != queueLengthSamples.end(); ++queueLengthSample) {
					profileLog.mutable_perflog()->add_queuelength(*queueLengthSample);
				}
				for (auto taskLongLatencySample = taskLongLatencySamples.begin();
						taskLongLatencySample != taskLongLatencySamples.end();
						++taskLongLatencySample) {
					profileLog.mutable_perflog()->add_longlatencytask(*taskLongLatencySample);
				}
				PROFILE(profileLog.SerializeAsString());
				delete element;
				continue;
			}

			responses[element->getRemoteIp()][element->GetReqMessageID()] = element;
			element->setStageTask(TranxPeerStageTask::_SENDNOWTRANXPEER);
			SendToPeerSend(element, true);
		}

		uint64 messageID;
		int size;
		void *responseArray;

		for (auto receiver = receivers.begin(); receiver != receivers.end(); ++receiver) {
			while (true) {
				responseArray = receiver->second->Wait(size);
				if (responseArray != NULL) {
					curResponses.push_back(std::make_pair(responseArray, size));
				} else {
					break;
				}
			}
			std::vector<RPC::GRPCResponseMessage*> parsedResponses;
			for (auto it = curResponses.begin(); it != curResponses.end(); ++it) {
				RPC::GRPCResponseMessage *message = RPC::BaseRPC::GetMessage(it->first, it->second);
				parsedResponses.push_back(message);
			}
			for (auto it = parsedResponses.begin(); it != parsedResponses.end(); ++it) {
				messageID = (*it)->messageid();
				//VERBOSE("PeerRecv getting a message %lu from %s", messageID, receiver->first.c_str());
				if (responses[receiver->first].find(messageID)
						== responses[receiver->first].end()) {
					//VERBOSE("PeerRecv getting a message that's not being waited");
					continue;
				}
				responses[receiver->first][messageID]->SetResponse(*it);
				responses[receiver->first][messageID]->waitingStatus =
						RPC::TranxRPC::RPCWaitingStatus::HAS_REPLY;
				assert(responses[receiver->first][messageID]->GetRepStatus() == RPC::GStatus::OK);
				tranxRPCHelper->SendToTranxRPCInternal(responses[receiver->first][messageID]);
				responses[receiver->first].erase(messageID);
			}
			curResponses.clear();
		}
		if (mode != SharedResources::TranxServerInfo::Mode::NORMAL) {
			usleep(10000);
		}
	}
}

} /* namespace Stages */
} /* namespace DTranx */

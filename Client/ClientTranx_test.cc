/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <memory>
#include <iostream>
#include "gtest/gtest.h"
#include "DTranx/Client/ClientTranx.h"
#include "zmsg.hpp"
#include "DTranx/Util/types.h"
#include "DTranx/Util/Log.h"
#include "Util/FileUtil.h"
#include "Util/StaticConfig.h"
#include "Server/Server.h"

using namespace DTranx;

TEST(ClientTranx, internalutility) {
	Util::ConfigHelper configHelper;
	configHelper.readFile("DTranx.conf");
	std::string selfAddress = configHelper.read("SelfAddress");
	uint32_t selfPort = std::stoul(configHelper.read("clientReceivePort").c_str(), nullptr, 10);
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(
			configHelper.read<uint32>("maxIOThreads"));
	std::vector<std::string> cluster;
	cluster.push_back("192.168.0.1");
	cluster.push_back("192.168.0.2");
	cluster.push_back("192.168.0.3");
	std::unordered_set<std::string> clusterSet(cluster.begin(), cluster.end());
	Client::ClientTranx clientTranx(
			std::stoul(configHelper.read("routerFrontPort").c_str(), nullptr, 10), context, cluster,
			selfAddress, selfPort);
	std::string mapIP = clientTranx.GetMapping("key1");
	EXPECT_TRUE(clusterSet.find(mapIP) != clusterSet.end());
	Service::GReadData::Response response;
	response.set_value("value1");
	response.set_version(12);
	response.add_newips("192.168.0.2");
	response.add_newips("192.168.0.3");
	std::string key = "key1";
	clientTranx.UpdateMappingCacheFromStorageService(key, response);
	clientTranx.UpdateDataCacheFromStorageService(key, response);
	std::string value;
	clientTranx.Read(key, value, false);
	EXPECT_EQ("value1", value);
	EXPECT_EQ(2, clientTranx.cache->mapping[key].size());
}

TEST(ClientTranx, GetOverlappedIPs) {
	Util::ConfigHelper configHelper;
	configHelper.readFile("DTranx.conf");
	std::string selfAddress = configHelper.read("SelfAddress");
	uint32_t selfPort = std::stoul(configHelper.read("clientReceivePort").c_str(), nullptr, 10);
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(
			configHelper.read<uint32>("maxIOThreads"));
	std::vector<std::string> cluster;
	cluster.push_back("192.168.0.1");
	cluster.push_back("192.168.0.2");
	cluster.push_back("192.168.0.3");
	std::unordered_set<std::string> clusterSet(cluster.begin(), cluster.end());
	Client::ClientTranx clientTranx(
			std::stoul(configHelper.read("routerFrontPort").c_str(), nullptr, 10), context, cluster,
			selfAddress, selfPort);
	std::unordered_set<std::string> v1,v2;
	v1.insert("192.168.0.1");
	v1.insert("192.168.0.2");
	v1.insert("192.168.0.3");
	v2.insert("192.168.0.2");
	v2.insert("192.168.0.3");
	v2.insert("192.168.0.4");
	std::unordered_set<std::string> v3 = clientTranx.GetOverlappedIPs(v1, v2);
	EXPECT_EQ(2, v3.size());
	EXPECT_TRUE(v3.find("192.168.0.3") != v3.end());
	EXPECT_TRUE(v3.find("192.168.0.1") == v3.end());
}

class ClientTranxTest_MockServer: public ::testing::Test {
public:
	ClientTranxTest_MockServer() {
		configHelper.readFile("DTranx.conf");
		selfAddress = configHelper.read("SelfAddress");
		selfPort = std::stoul(configHelper.read("clientReceivePort").c_str(), nullptr, 10);
		server = new Server::Server(configHelper);
		InitStorage(server->localStorage);
		server->Start();
	}
	~ClientTranxTest_MockServer() {
		server->ShutDown();
		delete server;
		Util::FileUtil::RemoveDir("dtranx.db*");
		Util::FileUtil::RemoveDir("dtranx.mapdb");
		Util::FileUtil::RemovePrefix("./", "dtranx.log");
	}

	Server::Server *server;
	Util::ConfigHelper configHelper;
	/*
	 * selfAddress, selfPort are used for clients to bind
	 */
	std::string selfAddress;
	uint32_t selfPort;

	void InitStorage(Stages::StringStorage *storage);

	ClientTranxTest_MockServer(const ClientTranxTest_MockServer&) = delete;
	ClientTranxTest_MockServer& operator=(const ClientTranxTest_MockServer&) = delete;
};

void ClientTranxTest_MockServer::InitStorage(Stages::StringStorage *storage) {
	for (int i = 0; i < 10; i++) {
		storage->Write("key" + std::to_string(i), "value" + std::to_string(i));
	}
}

TEST_F(ClientTranxTest_MockServer, ReadData) {
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(1);
	std::vector<std::string> cluster;
	cluster.push_back("localhost");
	Client::ClientTranx *ClientTranx = new Client::ClientTranx(
			configHelper.read<uint32_t>("routerFrontPort"), context, cluster, selfAddress,
			selfPort);
	std::string key, value;
	key = "key1";
	ClientTranx->Read(key, value);
	EXPECT_EQ("value1", value);
	delete ClientTranx;
}

TEST_F(ClientTranxTest_MockServer, ClientCache) {
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(1);
	std::vector<std::string> cluster;
	std::shared_ptr<Client::ClientCache> cache = std::make_shared<Client::ClientCache>();
	cluster.push_back("localhost");
	Client::ClientTranx *ClientTranx = new Client::ClientTranx(
			configHelper.read<uint32_t>("routerFrontPort"), context, cluster, cache, selfAddress,
			selfPort);
	std::string key, value;
	key = "key1";
	ClientTranx->Read(key, value);
	EXPECT_EQ("value1", value);
	key = "key2";
	value = "value2_2";
	ClientTranx->Write(key, value);

	EXPECT_TRUE(cache->data.find("key1") != cache->data.end());
	EXPECT_TRUE(cache->data.find("key2") == cache->data.end());
	EXPECT_TRUE(ClientTranx->Commit());
	key = "key1";
	ClientTranx->Read(key, value);
	delete ClientTranx;
}

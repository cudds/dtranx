/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "MessageIDSet.h"

using namespace DTranx;

TEST(MessageIDSet, IsProcessed) {
	std::string clientID1 = "1";
	std::string clientID2 = "2";
	Service::MessageIDSet * clientMessages = new Service::MessageIDSet();
	EXPECT_FALSE(clientMessages->IsProcessed(clientID1, 1));
	EXPECT_TRUE(clientMessages->IsProcessed(clientID1, 1));
	EXPECT_FALSE(clientMessages->IsProcessed(clientID1, 2));
	EXPECT_TRUE(clientMessages->IsProcessed(clientID1, 1));
	EXPECT_FALSE(clientMessages->IsProcessed(clientID1, 18));
	EXPECT_TRUE(clientMessages->IsProcessed(clientID1, 18));
	EXPECT_FALSE(clientMessages->IsProcessed(clientID2, 5));
	EXPECT_TRUE(clientMessages->IsProcessed(clientID2, 5));
	EXPECT_FALSE(clientMessages->IsProcessed(clientID2, 1));
	EXPECT_TRUE(clientMessages->IsProcessed(clientID2, 1));
	delete clientMessages;
}


TEST(MessageIDSet, AggregateAllMessage) {
	std::string clientID1 = "1";
	std::string clientID2 = "2";
	Service::MessageIDSet * clientMessages = new Service::MessageIDSet();
	EXPECT_FALSE(clientMessages->IsProcessed(clientID1, 1));
	EXPECT_FALSE(clientMessages->IsProcessed(clientID1, 2));
	EXPECT_FALSE(clientMessages->IsProcessed(clientID1, 18));
	EXPECT_FALSE(clientMessages->IsProcessed(clientID2, 5));
	EXPECT_FALSE(clientMessages->IsProcessed(clientID2, 1));
	EXPECT_EQ(5, clientMessages->AggregateAllMessage());
	delete clientMessages;
}

protoc -I. --cpp_out=./include/DTranx Service/Service.proto
protoc -I. --cpp_out=./include/DTranx Service/Storage.proto
protoc -I. --cpp_out=./include/DTranx Service/Transaction.proto
protoc -I. --cpp_out=./include/DTranx Service/Client.proto
protoc -I. --cpp_out=./include/DTranx RPC/RPC.proto
protoc -I. --cpp_out=./include/DTranx Stages/Log/Log.proto

mv ./include/DTranx/Service/Service.pb.cc Service/
sed -i -e 's/#include \"Service\/Service.pb.h\"/#include \"DTranx\/Service\/Service.pb.h\"/g' Service/Service.pb.cc
mv ./include/DTranx/Service/Storage.pb.cc Service/
sed -i -e 's/#include \"Service\/Storage.pb.h\"/#include \"DTranx\/Service\/Storage.pb.h\"/g' Service/Storage.pb.cc
sed -i -e 's/#include \"Service\/Service.pb.h\"/#include \"DTranx\/Service\/Service.pb.h\"/g' include/DTranx/Service/Storage.pb.h
mv ./include/DTranx/Service/Transaction.pb.cc Service/
sed -i -e 's/#include \"Service\/Transaction.pb.h\"/#include \"DTranx\/Service\/Transaction.pb.h\"/g' Service/Transaction.pb.cc
sed -i -e 's/#include \"Service\/Service.pb.h\"/#include \"DTranx\/Service\/Service.pb.h\"/g' include/DTranx/Service/Transaction.pb.h
mv ./include/DTranx/Service/Client.pb.cc Service/
sed -i -e 's/#include \"Service\/Client.pb.h\"/#include \"DTranx\/Service\/Client.pb.h\"/g' Service/Client.pb.cc
sed -i -e 's/#include \"Service\/Service.pb.h\"/#include \"DTranx\/Service\/Service.pb.h\"/g' include/DTranx/Service/Client.pb.h

mv ./include/DTranx/RPC/RPC.pb.cc RPC/
sed -i -e 's/#include \"Service\/Transaction.pb.h\"/#include \"DTranx\/Service\/Transaction.pb.h\"/g' include/DTranx/RPC/RPC.pb.h
sed -i -e 's/#include \"Service\/Storage.pb.h\"/#include \"DTranx\/Service\/Storage.pb.h\"/g' include/DTranx/RPC/RPC.pb.h
sed -i -e 's/#include \"Service\/Client.pb.h\"/#include \"DTranx\/Service\/Client.pb.h\"/g' include/DTranx/RPC/RPC.pb.h
sed -i -e 's/#include \"RPC\/RPC.pb.h\"/#include \"DTranx\/RPC\/RPC.pb.h\"/g' RPC/RPC.pb.cc
	
mv ./include/DTranx/Stages/Log/Log.pb.cc Stages/Log/
sed -i -e 's/#include \"Service\/Transaction.pb.h\"/#include \"DTranx\/Service\/Transaction.pb.h\"/g' include/DTranx/Stages/Log/Log.pb.h
sed -i -e 's/#include \"Service\/Service.pb.h\"/#include \"DTranx\/Service\/Service.pb.h\"/g' include/DTranx/Stages/Log/Log.pb.h
sed -i -e 's/#include \"Stages\/Log\/Log.pb.h\"/#include \"DTranx\/Stages\/Log\/Log.pb.h\"/g' Stages/Log/Log.pb.cc
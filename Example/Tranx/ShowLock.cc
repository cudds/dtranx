/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * show the remote lock status
 */

#include <memory>
#include <fstream>
#include "DTranx/Client/Client.h"
#include "DTranx/Util/ConfigHelper.h"
#include "RPC/ClientRPC.h"
#include "DTranx/Service/Transaction.pb.h"
#include "Util/StringUtil.h"
using namespace DTranx;

int main(int argc, char **argv) {
	Util::Log::setLogPolicy( { { "Client", "VERBOSE" }, { "RPC", "VERBOSE" } });
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(10);
	if (argc < 3) {
		std::cout << "arg1: ips file path, arg2: self address" << std::endl;
		return 0;
	}
	std::vector<std::string> ips;
	std::ifstream ipFile(argv[1]);
	if (!ipFile.is_open()) {
		VERBOSE("cannot open %s", argv[1]);
		exit(0);
	}
	std::string ip;
	while (std::getline(ipFile, ip)) {
		ips.push_back(ip);
	}
	std::string selfAddressStr(argv[2]);
	uint32_t selfAddress[4];
	std::vector<std::string> tmpFields = Util::StringUtil::Split(selfAddressStr, '.');
	assert(tmpFields.size() == 4);
	for (int i = 0; i < 4; ++i) {
		selfAddress[i] = std::stoul(tmpFields[i], nullptr, 10);
	}

	for (auto it = ips.begin(); it != ips.end(); ++it) {
		std::cout << "fetching locks for server " << *it << std::endl;
		Client::Client *client = new Client::Client(*it, 30000, context);
		while(!client->Bind(selfAddressStr, 30030));
		RPC::ClientRPC* clientRPC;
		try {
			clientRPC = new RPC::ClientRPC(RPC::GServiceID::ClientService, client);
			clientRPC->SetClientOpcode(Service::GOpCode::CLIENT_COMMIT);
			clientRPC->SetReqIP(selfAddress);
			clientRPC->SetReqPort(30030);
		} catch (std::exception &e) {
			std::cout << "ning bad alloc 4" << std::endl;
		}
		DTranx::Service::GClientCommit::Request *request =
				clientRPC->GetClientRequest()->mutable_clientcommit()->mutable_request();
		request->set_tranxtype(Service::GTranxType::SHOWLOCK);
		client->CallRPC(clientRPC);
		assert(client->BlockPoll(clientRPC));
		DTranx::Service::GClientCommit::Response *response =
				clientRPC->GetClientResponse()->mutable_clientcommit()->mutable_response();
		if (response->status() == Service::GStatus::OK) {
			std::cout << "lock status for " << *it << std::endl;
			std::cout << response->lock_status() << std::endl;
			std::cout << "\n\n\n\n\n\n\n" << std::endl;
		} else {
			assert(false);
		}
		delete clientRPC;
		delete client;
	}
	return 0;
}


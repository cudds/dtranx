/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <cassert>
#include "BasicLockTable.h"
#include "DTranx/Util/Log.h"

namespace DTranx {
namespace Stages {
namespace Lock {

BasicLockTable::BasicLockTable(SharedResources::TranxServerInfo *tranxServerInfo,
		int numOfStageThreads,
		std::string stageName)
		: MidStage(tranxServerInfo, numOfStageThreads, stageName) {
	epochLock = std::unique_ptr<EpochLockElement>(new EpochLockElement());
	for (int i = 0; i < numOfStageThreads; ++i) {
		lockTable.push_back(std::unordered_map<std::string, LockElement*>());
	}
}

BasicLockTable::~BasicLockTable() noexcept(false) {
	for (auto lockTableOne = lockTable.begin(); lockTableOne != lockTable.end(); ++lockTableOne) {
		for (auto lockEntry = lockTableOne->begin(); lockEntry != lockTableOne->end();
				++lockEntry) {
			for (auto tranxInfo = lockEntry->second->tranx.begin();
					tranxInfo != lockEntry->second->tranx.end(); ++tranxInfo) {
				delete *tranxInfo;
			}
			delete lockEntry->second;
		}
	}
}

bool BasicLockTable::_RequestLockPerKey(LockElement *currentLock,
		const Util::TranxID& tranxID,
		BasicLockTableLockMode lockMode,
		bool waitIfDenied) {
	if (currentLock->tranx.size() == 0) {
		currentLock->mode = lockMode;
		currentLock->wait = false;
		TranxInfo *tranxInfo = new TranxInfo(tranxID, lockMode);
		currentLock->tranx.push_back(tranxInfo);
		return true;
	}

	//first, when it's locked by itself and it's exclusive, grant the req
	if (currentLock->mode == BasicLockTableLockMode::Exclusive
			&& currentLock->tranx.front()->tranxID == tranxID) {
		return true;
	}

	//when the lockMode is shared
	if (lockMode == BasicLockTableLockMode::Shared) {
		//when it's already locked by itself or can be locked
		//when it's in the list of the shared lock, or can be placed in the list
		if (currentLock->mode == BasicLockTableLockMode::Shared) {
			bool allSharedList = true;
			for (auto it = currentLock->tranx.begin(); it != currentLock->tranx.end(); ++it) {
				if ((*it)->tranxID == tranxID) {
					return true;
				}
				if ((*it)->lockMode == BasicLockTableLockMode::Exclusive) {
					//this lock req cannot be from the same tranx, because the tranx will first cancel
					//the waiting req before requesting for a new lock when the lock type is different
					allSharedList = false;
					break;
				}
			}
			if (allSharedList) {
				currentLock->tranx.push_back(new TranxInfo(tranxID, lockMode));
				return true;
			}
		}
		if (!waitIfDenied) {
			return false;
		}
		//put it in the list only if it's not there
		for (auto it = currentLock->tranx.begin(); it != currentLock->tranx.end(); ++it) {
			if ((*it)->tranxID == tranxID) {
				assert((*it)->lockMode == lockMode);
				return false;
			}
		}

		currentLock->wait = true;
		currentLock->tranx.push_back(new TranxInfo(tranxID, lockMode));
		return false;
	}
	//when the lockMode is exclusive
	else {
		//when it's already shared locked by itself, and the list size is 1, upgrade it to exclusive
		if (currentLock->mode == BasicLockTableLockMode::Shared
				&& currentLock->tranx.front()->tranxID == tranxID
				&& currentLock->tranx.size() == 1) {
			currentLock->mode = BasicLockTableLockMode::Exclusive;
			return true;
		}
		if (!waitIfDenied) {
			return false;
		}

		//put it in the list only if it's not there
		for (auto it = currentLock->tranx.begin(); it != currentLock->tranx.end(); ++it) {
			if ((*it)->tranxID == tranxID) {
				assert((*it)->lockMode == lockMode);
				return false;
			}
		}

		currentLock->wait = true;
		currentLock->tranx.push_back(new TranxInfo(tranxID, lockMode));
		return false;
	}
}

bool BasicLockTable::_RequestLock(const std::string& key,
		const Util::TranxID& tranxID,
		BasicLockTableLockMode lockMode,
		bool waitIfDenied) {
	// if create new data(exclusive lock), grant locks too
	// remove the checking because if data not exists, it won't go through during the reading phase of the transaction
	/*
	 if(lockMode == BasicLockTableLockMode::Shared && !localStorage->Exist(key)) {
	 return false;
	 }
	 */
	int threadID = GetThreadByKey(key);
	if (lockTable[threadID].find(key) == lockTable[threadID].end()) {
		LockElement *lockElement = new LockElement(lockMode, false);
		TranxInfo *tranxInfo = new TranxInfo(tranxID, lockMode);
		lockElement->tranx.push_back(tranxInfo);
		lockTable[threadID][key] = lockElement;
		return true;
	}
	return _RequestLockPerKey(lockTable[threadID][key], tranxID, lockMode, waitIfDenied);
}

bool BasicLockTable::_RequestEpochLock(const Util::TranxID& tranxID, BasicLockTableLockMode lockMode) {
	if (lockMode == BasicLockTableLockMode::Shared) {
		if (epochLock->wait) {
			if (epochLock->tranxID.find(tranxID) != epochLock->tranxID.end()) {
				//already hold lock.
				return true;
			}
			assert(!epochLock->writeTranx.empty());
			return false;
		} else {
			if (epochLock->mode == BasicLockTableLockMode::Shared) {
				epochLock->tranxID.insert(tranxID);
				return true;
			} else {
				return false;
			}
		}
	}
	if (epochLock->wait) {
		return false;
	} else {
		//not waiting
		if (epochLock->tranxID.empty()) {
			if (!epochLock->writeTranx.empty()) {
				assert(epochLock->mode == BasicLockTableLockMode::Exclusive);
				//already hold lock.
				return epochLock->writeTranx == tranxID;
			} else {
				epochLock->writeTranx = tranxID;
				epochLock->mode = BasicLockTableLockMode::Exclusive;
				return true;
			}
		} else {
			assert(epochLock->mode == BasicLockTableLockMode::Shared);
			epochLock->wait = true;
			epochLock->writeTranx = tranxID;
			return false;
		}
	}
}

bool BasicLockTable::_CancelRequest(const std::string& key, const Util::TranxID& tranxID) {
	//There can be only one record for each key, each tranx
	//do not call cancel when the lock is granted, but you can call cancel when not request before
	int threadID = GetThreadByKey(key);
	if (lockTable[threadID].find(key) == lockTable[threadID].end()) {
		return false;
	}
	LockElement *currentLock = lockTable[threadID][key];

	if (currentLock->tranx.size() == 0) {
		return false;
	}

	std::list<TranxInfo*>::iterator target;
	bool result = false;
	bool otherWriteLockExist = false;

	for (auto it = currentLock->tranx.begin(); it != currentLock->tranx.end(); ++it) {
		if ((*it)->tranxID == tranxID) {
			target = it;
			result = true;
		} else if ((*it)->lockMode == BasicLockTableLockMode::Exclusive) {
			otherWriteLockExist = true;
		}

	}
	if (result) {
		delete (*target);
		currentLock->tranx.erase(target);
	}
	//adjust wait
	if (!otherWriteLockExist) {
		currentLock->wait = false;
	} else if (currentLock->tranx.size() == 1) {
		currentLock->wait = false;
	}
	return result;
}

bool BasicLockTable::_ReleaseEpochLock(const Util::TranxID& tranxID) {
	bool releasedOK = false;
	if (epochLock->tranxID.find(tranxID) != epochLock->tranxID.end()) {
		epochLock->tranxID.erase(tranxID);
		releasedOK = true;
	}
	if (epochLock->writeTranx == tranxID && epochLock->wait == false) {
		epochLock->writeTranx.Clear();
		epochLock->mode = BasicLockTableLockMode::Shared;
		releasedOK = true;
	}

	//grant a previous write request when this release leads to empty read set.
	if (epochLock->wait && epochLock->tranxID.empty()) {
		assert(!epochLock->writeTranx.empty());
		assert(epochLock->mode == BasicLockTableLockMode::Shared);
		epochLock->mode = BasicLockTableLockMode::Exclusive;
		epochLock->wait = false;
	}

	return releasedOK;
}

bool BasicLockTable::_ReleaseLockPerKey(LockElement *currentLock, const Util::TranxID& tranxID) {
	if (currentLock->tranx.size() == 0) {
		return false;
	}

	//if it's not locked by itself, return false
	if (currentLock->mode == BasicLockTableLockMode::Exclusive
			&& currentLock->tranx.front()->tranxID != tranxID) {
		return false;
	}
	assert(currentLock->tranx.size() >= 1);
	if (currentLock->mode == BasicLockTableLockMode::Exclusive) {
		assert(currentLock->tranx.front()->tranxID == tranxID);
		delete currentLock->tranx.front();
		currentLock->tranx.pop_front();
	}

	//when the lock mode is shared
	if (currentLock->mode == BasicLockTableLockMode::Shared) {
		//find its entry in the tranx list
		std::list<TranxInfo*>::iterator cur;
		for (cur = currentLock->tranx.begin(); cur != currentLock->tranx.end(); ++cur) {
			if ((*cur)->lockMode == BasicLockTableLockMode::Exclusive) {
				return false;
			}
			if ((*cur)->tranxID == tranxID) {
				break;
			}
		}
		if (cur == currentLock->tranx.end()) {
			return false;
		}
		delete (*cur);
		currentLock->tranx.erase(cur);

		//if no one is waiting, delete and continue, nothing to do
		if (currentLock->wait == true) {
			//if someone is waiting, check the list and grant if no shared locks are held, adjust the wait value
			assert(currentLock->tranx.size() >= 1);
			//if the next one is shared lock, do nothing.
			//if the next one is exclusive lock req, adjust wait and mode
			if (currentLock->tranx.front()->lockMode == BasicLockTableLockMode::Exclusive) {
				currentLock->mode = BasicLockTableLockMode::Exclusive;
				currentLock->wait = (currentLock->tranx.size() > 1);
			}
		}
	}
	//when the lock mode is exclusive
	else {
		//if no one is waiting, delete and continue, do nothing
		//if someone is waiting, grant lock(exclusive or shared), adjust wait value
		if (currentLock->wait == true) {
			assert(currentLock->tranx.size() >= 1);
			currentLock->mode = currentLock->tranx.front()->lockMode;
			//adjust wait value, if it's shared lock, check the tranx list whether there are any exclusive lock req waiting
			//if it's exclusive lock, as long as there's some tranx in the list except the current one, it's waiting
			if (currentLock->mode == BasicLockTableLockMode::Shared) {
				bool allSharedList = true;
				for (auto it = currentLock->tranx.begin(); it != currentLock->tranx.end(); ++it) {
					if ((*it)->lockMode == BasicLockTableLockMode::Exclusive) {
						allSharedList = false;
						break;
					}
				}
				if (allSharedList) {
					currentLock->wait = false;
				}
			} else {
				currentLock->wait = currentLock->tranx.size() > 1;
			}
		}
	}
	return true;
}

bool BasicLockTable::_ReleaseLock(const std::string& key, const Util::TranxID& tranxID) {
	/* new data releases when it's written, it happens when request fails in the middle or the tranx failed somehow
	 if(!localStorage->Exist(key)) {
	 PrintLockStatus();
	 throw Util::NotFoundException(key + "key not found");
	 }
	 */
	int threadID = GetThreadByKey(key);
	if (lockTable[threadID].find(key) == lockTable[threadID].end()) {
		return false;
		//it happens when requests failed and releasing locks happen on the ones that's not obtained previously.
		//throw Util::NotFoundException("lock for "+tranxID+" not found");
	}
	return _ReleaseLockPerKey(lockTable[threadID][key], tranxID);
}

std::string BasicLockTable::PrintLockStatus(int stageThreadIndex) {
	std::string result;
	std::vector<std::string> keys;
	result += "LockTable status:(lockmode:shared->" + std::to_string(BasicLockTableLockMode::Shared)
			+ ", exclusive->" + std::to_string(BasicLockTableLockMode::Exclusive) + ")\n";

	for (auto it = lockTable[stageThreadIndex].begin(); it != lockTable[stageThreadIndex].end();
			++it) {
		result += "key " + it->first + ": lockmode("
				+ std::string(
						(it->second->mode == BasicLockTableLockMode::Shared) ?
								"share" : "exclusive") + "), wait("
				+ std::string(it->second->wait ? "true" : "false") + ")\n";
		result += "\ttransaction list:\n";
		for (auto it_b = it->second->tranx.begin(); it_b != it->second->tranx.end(); ++it_b) {
			result += "\ttranxID(" + std::to_string((*it_b)->tranxID.GetNodeID()) + "#"
					+ std::to_string((*it_b)->tranxID.GetTranxID()) + "), lockmode("
					+ std::string(
							((*it_b)->lockMode == BasicLockTableLockMode::Shared) ?
									"share" : "exclusive") + ")\n";
		}
	}
	if (numOfStageThreads == 1 || stageThreadIndex == numOfStageThreads) {
		result += "epoch lock: lockmode("
				+ std::string(
						(epochLock->mode == BasicLockTableLockMode::Shared) ? "share" : "exclusive")
				+ "), wait(" + std::string(epochLock->wait ? "true" : "false") + ")\n";
		result += "\tread transaction list:\n";
		for (auto it_b = epochLock->tranxID.begin(); it_b != epochLock->tranxID.end(); ++it_b) {
			result += "\ttranxID(" + std::to_string(it_b->GetNodeID()) + "#"
					+ std::to_string(it_b->GetTranxID()) + ")\n";
		}
		result += "\twriteTranx: " + std::to_string(epochLock->writeTranx.GetNodeID()) + "#"
				+ std::to_string(epochLock->writeTranx.GetTranxID()) + "\n";
	}
	return result;
}

} /* namespace Lock */
} /* namespace Stages */
} /* namespace DTranx */

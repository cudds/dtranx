#include <boost/thread.hpp>
#include <mutex>
#include <memory>
#include <queue>

#include "zmsg.hpp"
class ZeromqProxy {

private:
	boost::thread zeromqProxyThread1, zeromqProxyThread2;
	std::shared_ptr<bool> terminate_proxy;
	std::shared_ptr<zmq::context_t> context;
	void StartProxyThread(std::string port, std::shared_ptr<bool> terminate_proxy,
							std::shared_ptr<zmq::context_t> context, int proxyIndex) {
		zmq::socket_t frontend(*context, ZMQ_ROUTER);
		zmq::socket_t backendReceiver(*context, ZMQ_DEALER);

		std::string localAddress = "tcp://128.104.222.74";
		std::string frontAddress = localAddress + ":" + port;
		try {
			frontend.bind(frontAddress.c_str());
			backendReceiver.bind(
					(std::string("inproc://backendReceiver") + std::to_string(proxyIndex)).c_str());
		} catch (zmq::error_t &e) {
			std::cout << e.what() << std::endl;
			assert(false);
		}

		int lingtime = 0;
		frontend.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
		int hwm = 0;
		size_t len = sizeof(hwm);
		frontend.setsockopt(ZMQ_SNDHWM, &hwm, len);
		backendReceiver.setsockopt(ZMQ_SNDHWM, &hwm, len);
		frontend.setsockopt(ZMQ_RCVHWM, &hwm, len);
		backendReceiver.setsockopt(ZMQ_RCVHWM, &hwm, len);
		int mandatory = 1;
		len = sizeof(mandatory);
		frontend.setsockopt(ZMQ_ROUTER_MANDATORY, &mandatory, len);
		try {
			zmq::proxy(frontend, backendReceiver, NULL);
		} catch (std::exception &e) {

		}
		/*
		 while (true) {
		 zmsg zm(frontend);
		 zm.send(backendReceiver);
		 }
		 frontend.close();
		 */
	}
public:
	explicit ZeromqProxy(std::shared_ptr<zmq::context_t> context)
			: context(context) {
		terminate_proxy = std::make_shared<bool>(false);
	}

	~ZeromqProxy() {
		ShutProxy();
	}

	void StartProxy() {
		zeromqProxyThread1 = boost::thread(&ZeromqProxy::StartProxyThread, this, "2222",
				terminate_proxy, context, 1);
		zeromqProxyThread2 = boost::thread(&ZeromqProxy::StartProxyThread, this, "2223",
				terminate_proxy, context, 2);
	}

	void ShutProxy() {
		if (terminate_proxy) {
			*terminate_proxy = true;
		}
		if (zeromqProxyThread1.joinable()) {
			zeromqProxyThread1.join();
		}
	}

};

void ServerThread(std::shared_ptr<zmq::context_t> context, int serverID, std::string backend) {
	zmq::socket_t socket(*context, ZMQ_DEALER);
	int lingtime = 0;
	socket.setsockopt(ZMQ_LINGER, &lingtime, sizeof(int));
	socket.connect(backend.c_str());
	zmq::pollitem_t items[] = { { socket, 0, ZMQ_POLLIN, 0 } };
	int count = 0;
	std::chrono::system_clock::time_point start, end;
	while (true) {
		while (true) {
			zmq::poll(items, 1, 100);
			if (items[0].revents & ZMQ_POLLIN) {
				break;
			}
		}
		count++;
		if (count % 100000 == 0) {
			std::cout << serverID << " receiver " << count << std::endl;
		}
		if (count == 1) {
			start = std::chrono::system_clock::now();
		} else if (count == 5000000) {
			end = std::chrono::system_clock::now();
			int timePassed =
					std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
			std::cout << serverID << "throughout: " << 1.0 * 5000000 * 1000 / timePassed
					<< std::endl;
		}

		zmsg zm;
		zm.recv(socket);
	}
}

int main(void) {
	s_version();
	std::shared_ptr<zmq::context_t> context = std::make_shared<zmq::context_t>(50);
	std::unique_ptr<ZeromqProxy> proxy = std::unique_ptr<ZeromqProxy>(new ZeromqProxy(context));
	proxy->StartProxy();

	boost::thread serverThread1 = boost::thread(&ServerThread, context, 1, "inproc://backendReceiver1");
	boost::thread serverThread2 = boost::thread(&ServerThread, context, 2, "inproc://backendReceiver2");
	while (true) {
		sleep(5);
	}
}


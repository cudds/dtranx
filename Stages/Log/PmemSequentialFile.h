/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_STAGES_LOG_PMEMSEQUENTIALFILE_H_
#define DTRANX_STAGES_LOG_PMEMSEQUENTIALFILE_H_

#include <libpmemlog.h>
#include "SequentialFile.h"
#include "DTranx/Util/Log.h"

namespace DTranx {
namespace Stages {
namespace Log {

class PmemSequentialFile: public SequentialFile {
public:
	PmemSequentialFile(const std::string& fname, PMEMlogpool *plp)
		: filename_(fname), plp(plp) {

	}
	virtual ~PmemSequentialFile(){
		if (plp != NULL) {
			//VERBOSE("pmem closing %s", filename_.c_str());
			pmemlog_close(plp);
		}
	}

	virtual LogStatus Read(size_t n, Slice* result, char* scratch);
	virtual LogStatus Skip(uint64_t n);

private:
	std::string filename_;
	PMEMlogpool *plp;
};

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_LOG_PMEMSEQUENTIALFILE_H_ */

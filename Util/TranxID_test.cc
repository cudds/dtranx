/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <unordered_map>
#include "gtest/gtest.h"
#include "TranxID.h"

using namespace DTranx;
TEST(TranxID, operators) {
	Util::TranxID tranxID1(1, 10);
	Util::TranxID tranxID2(2, 30);
	tranxID2 = tranxID1;
	EXPECT_EQ(1, tranxID2.nodeID);
	EXPECT_EQ(10, tranxID2.tranxID);
	EXPECT_TRUE(tranxID1 == tranxID2);
	Util::TranxID tranxID3(3, 20);
	EXPECT_FALSE(tranxID2 == tranxID3);
}

TEST(TranxID, constructor) {
	Util::TranxID tranxID1(1, 10);
	Util::TranxID tranxID2(tranxID1);
	EXPECT_EQ(1, tranxID2.nodeID);
	EXPECT_EQ(10, tranxID2.tranxID);
}

TEST(TranxID, hash) {
	std::unordered_map<Util::TranxID, int, Util::KeyHasher> tranxIDs;
	Util::TranxID tranxID1(1, 10);
	Util::TranxID tranxID2(2, 30);
	tranxIDs[tranxID1] = 5;
	tranxIDs[tranxID2] = 6;
	EXPECT_TRUE(tranxIDs.find(tranxID1) != tranxIDs.end());
}

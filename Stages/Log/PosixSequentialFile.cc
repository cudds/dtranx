/*
 * Copyright (c) 2011 The LevelDB Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file. See the AUTHORS file for names of contributors.
 *
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "PosixSequentialFile.h"

namespace DTranx {
namespace Stages {
namespace Log {

LogStatus PosixSequentialFile::Read(size_t n, Slice* result, char* scratch) {
	LogStatus s;
	size_t r = fread_unlocked(scratch, 1, n, file_);
	*result = Slice(scratch, r);
	if (r < n) {
		if (feof(file_)) {
			// We leave status as ok if we hit the end of the file
		} else {
			// A partial read with an error: return a non-ok status
			s = LogStatus::IOError(filename_, strerror(errno));
		}
	}
	return s;
}

LogStatus PosixSequentialFile::Skip(uint64_t n) {
	if (fseek(file_, n, SEEK_CUR)) {
		return LogStatus::IOError(filename_, strerror(errno));
	}
	return LogStatus::OK();
}

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "gtest/gtest.h"
#include "LogWriter.h"
#include "LogReader.h"
#include "DTranx/Util/ConfigHelper.h"

using namespace DTranx;
TEST(LogReaderWriter, write) {
	std::string fname = "logTest";
	Util::ConfigHelper configHelper;
	configHelper.readFile("DTranx.conf");
	Stages::Log::LogOption logOption =
			(configHelper.read("LogOption") == "Pmem") ? Stages::Log::PMEM : Stages::Log::DISK;
	Stages::Log::LogWriter *logWriter = new Stages::Log::LogWriter(fname, true, logOption,
			configHelper.read<uint64>("FileTruncate"));
	logWriter->AddRecord(Stages::Log::Slice("hello", 5));
	logWriter->AddRecord(Stages::Log::Slice("world", 5));
	delete logWriter;

	Stages::Log::LogReader *logReader = new Stages::Log::LogReader(fname, true, 0, logOption);
	std::string scratch;
	Stages::Log::Slice record;
	EXPECT_TRUE(logReader->ReadRecord(&record, &scratch));
	EXPECT_EQ("hello", record.ToString());
	EXPECT_TRUE(logReader->ReadRecord(&record, &scratch));
	EXPECT_EQ("world", record.ToString());
	EXPECT_FALSE(logReader->ReadRecord(&record, &scratch));
	EXPECT_EQ(0, record.size());
	delete logReader;
	if (remove(fname.c_str()) != 0) {
		FAIL();
	}
}

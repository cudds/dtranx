/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include <cassert>
#include "GCFactory.h"
#include "Util/StaticConfig.h"

namespace DTranx {
namespace Stages {

GCFactory::GCFactory(int nodeID,
		uint64 tranxBroadcastThreshold,
		uint64 timeBroadcastThreshold)
		: discreteSet(), otherNodeGC(), nodeID(nodeID) {
	earliest = std::strtoull(Util::StaticConfig::GetInstance()->Read("FirstID").c_str(), NULL, 10)
			- 1;
	prevBroadcast = earliest;
	lastBroadcast = boost::chrono::steady_clock::now();
	GCTranxThreshold = tranxBroadcastThreshold;
	GCTimeThreshold = timeBroadcastThreshold;
}

GCFactory::GCFactory(const GCFactory &other) {
	earliest = other.earliest;
	discreteSet = other.discreteSet;
	prevBroadcast = other.prevBroadcast;
	otherNodeGC = other.otherNodeGC;
	nodeID = other.nodeID;
	lastBroadcast = other.lastBroadcast;
	GCTranxThreshold = other.GCTranxThreshold;
	GCTimeThreshold = other.GCTimeThreshold;
}

GCFactory::~GCFactory() noexcept(false) {
}

void GCFactory::AddTranxID(uint64 tranxID) {
	if (tranxID <= earliest) {
		return;
	}
	discreteSet.insert(tranxID);
	uint64 tmpEarliest = earliest;
	for (auto it = discreteSet.begin(); it != discreteSet.end(); ++it) {
		if (*it - 1 == earliest) {
			earliest++;
		} else {
			break;
		}
	}
	for (int i = tmpEarliest + 1; i <= earliest; ++i) {
		discreteSet.erase(i);
	}
}

bool GCFactory::ToBroadcast() {
	assert(earliest >= prevBroadcast);
	if (earliest == prevBroadcast) {
		return false;
	}
	if (earliest - prevBroadcast >= GCTranxThreshold) {
		prevBroadcast = earliest;
		lastBroadcast = boost::chrono::steady_clock::now();
		return true;
	}
	boost::chrono::steady_clock::time_point now = boost::chrono::steady_clock::now();
	uint64 timeElapsed = boost::chrono::duration_cast<boost::chrono::milliseconds>(
			now - lastBroadcast).count();
	if (timeElapsed >= GCTimeThreshold) {
		prevBroadcast = earliest;
		lastBroadcast = boost::chrono::steady_clock::now();
		return true;
	}
	return false;
}

uint64 GCFactory::GetEarliest() {
	return earliest;
}

uint64 GCFactory::GetEarliest(int nodeID_) {
	if (nodeID_ == nodeID) {
		return GetEarliest();
	}
	if (otherNodeGC.find(nodeID_) == otherNodeGC.end()) {
		return std::strtoull(Util::StaticConfig::GetInstance()->Read("FirstID").c_str(), NULL, 10)
				- 1;
	}
	return otherNodeGC[nodeID_];
}

bool GCFactory::IsOutdated(uint64 tranxID) {
	if (tranxID <= earliest) {
		return true;
	}
	if (discreteSet.find(tranxID) != discreteSet.end()) {
		return true;
	}
	return false;
}

bool GCFactory::IsOutdated(int _nodeID, uint64 tranxID) {
	if (_nodeID == nodeID) {
		return IsOutdated(tranxID);
	}
	if (otherNodeGC.find(_nodeID) == otherNodeGC.end()) {
		return false;
	}
	if (otherNodeGC[_nodeID] >= tranxID) {
		return true;
	}
	return false;
}

void GCFactory::UpdateEarliest(uint64 tranxID) {
	/*
	 * only called during recoverlog, the local is usually updated by AddTranxID
	 */
	assert(earliest <= tranxID);
	assert(discreteSet.empty());
	earliest = tranxID;
	prevBroadcast = earliest;
}

void GCFactory::UpdateEarliest(int nodeID, uint64 tranxID) {
	if (nodeID == this->nodeID) {
		return UpdateEarliest(tranxID);
	}
	if (otherNodeGC.find(nodeID) != otherNodeGC.end() && otherNodeGC[nodeID] >= tranxID) {
		return;
	}
	otherNodeGC[nodeID] = tranxID;
}

std::unordered_map<int, uint64> GCFactory::GetAllNodeGC() {
	std::unordered_map<int, uint64> allNodeGC = otherNodeGC;
	assert(allNodeGC.find(nodeID) == allNodeGC.end());
	allNodeGC[nodeID] = earliest;
	return allNodeGC;
}

} /* namespace Stages */
} /* namespace DTranx */

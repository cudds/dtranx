/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_STAGES_MIDSTAGE_H_
#define DTRANX_STAGES_MIDSTAGE_H_

#include <boost/thread.hpp>
#include "TranxServiceQueueElement.h"
#include "SharedResources/TranxServerInfo.h"
#include "Build/Util/Profile.pb.h"
#include "PerfStage.h"
#include "Util/ThreadHelper.h"
#include "DTranx/Util/ConcurrentQueue.h"

namespace DTranx {
namespace Service {
class ClientService;
class TranxService;
class StorageService;
}
namespace DaemonStages {
class TranxAck;
class Repartition;
class DaemonStagesMock;
}
namespace Server {
class RecoverLog;
}
namespace Stages {

class MidStage: public Stages::PerfStage {
public:
	MidStage(SharedResources::TranxServerInfo *tranxServerInfo,
			int numOfStageThreads = 1,
			std::string stageName = "MidStageDefault")
			: Stages::PerfStage(stageName), tranxServerInfo(tranxServerInfo),
					terminateThread(false), numOfStageThreads(numOfStageThreads),
					processQueue(1000), dispatchQueue(1000), tranxService(NULL),
					clientService(NULL), storageService(NULL), tranxAck(NULL), repartition(NULL),
					postRecovery(NULL) {
	}

	virtual ~MidStage() noexcept(false) {

	}

	void InitStage(Service::TranxService *tranxService,
			Service::ClientService *clientService,
			Service::StorageService *storageService,
			DaemonStages::TranxAck *tranxAck,
			DaemonStages::Repartition *repartition,
			Server::RecoverLog *postRecovery) {
		this->tranxService = tranxService;
		this->clientService = clientService;
		this->storageService = storageService;
		this->tranxAck = tranxAck;
		this->repartition = repartition;
		this->postRecovery = postRecovery;
	}

	virtual void StartStage(std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) =0;

	void ShutStage() {
		terminateThread.store(true);
		for (auto thread = stageThread.begin(); thread != stageThread.end(); ++thread) {
			if (thread->joinable()) {
				thread->join();
			}
		}
		if (dispatchThread.joinable()) {
			dispatchThread.join();
		}
	}

	/*
	 * interfaces to add to queue
	 *
	 * randSeed is used to decide whether to profile this request for response time or not.
	 * 	while IsEnabled function decides for all requests. So is IsEnable is false, randSeed is not
	 * 	used at all.
	 */
	virtual void SendToStage(TranxServiceQueueElement *element, int stageThread =
			TranxServiceQueueElement::DISPATCH_STAGE_THREADID, moodycamel::ProducerToken *ptok =
	NULL, uint64_t randSeed = 1);

	/*
	 * post processing after the current stage is finished
	 *
	 */
	void LeaveStage(TranxServiceQueueElement *element, int stageThread, int task);

	moodycamel::ProducerToken* CreateDispatchQueueToken() {
		return new moodycamel::ProducerToken(dispatchQueue);
	}

protected:
	/*
	 * stage pipeline thread
	 *
	 * if there's only one thread, processQueue is used
	 * if there're multiple threads, processQueues is used and DispatchQueue is used for the dispatch thread
	 */
	std::vector<boost::thread> stageThread;
	boost::thread dispatchThread;
	std::atomic_bool terminateThread;
	moodycamel::ConcurrentQueue<TranxServiceQueueElement *> processQueue;
	moodycamel::ConcurrentQueue<TranxServiceQueueElement *> dispatchQueue;
	std::vector<moodycamel::ConcurrentQueue<TranxServiceQueueElement *>> processQueues;

	/*
	 * initiator stages: Service and DaemonStages
	 */
	Service::TranxService *tranxService;
	Service::ClientService *clientService;
	Service::StorageService *storageService;
	DaemonStages::TranxAck *tranxAck;
	DaemonStages::Repartition *repartition;
	Server::RecoverLog *postRecovery;

	/*
	 * shared info
	 */
	SharedResources::TranxServerInfo *tranxServerInfo;

	/*
	 * for unit test
	 */
	DaemonStages::DaemonStagesMock *daemonStagesMock;

	/*
	 * whether to enable dispatch or not, used for multiple stage threads
	 * if enabled, inherited class should implement Dispatch thread and use processQueues in its stageThread
	 * else, ignore enableDispatch and use processQueue in its stageThread
	 */
	int numOfStageThreads;
};

} /* namespace Stages */
} /* namespace DTranx */

#endif /* DTRANX_STAGES_MIDSTAGE_H_ */

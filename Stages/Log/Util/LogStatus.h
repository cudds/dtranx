/*
 * Copyright (c) 2011 The LevelDB Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file. See the AUTHORS file for names of contributors.
 *
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_STAGES_LOG_DISKSTATUS_H_
#define DTRANX_STAGES_LOG_DISKSTATUS_H_

#include <iostream>
#include "Slice.h"
namespace DTranx {
namespace Stages {
namespace Log {

enum LogOption{
	DISK, PMEM
};

enum RecordType {
  // Zero is reserved for preallocated files
  kZeroType = 0,

  kFullType = 1,

  // For fragments
  kFirstType = 2,
  kMiddleType = 3,
  kLastType = 4
};
static const int kMaxRecordType = kLastType;

static const int kBlockSize = 32768;

// Header is checksum (4 bytes), length (2 bytes), type (1 byte).
static const int kHeaderSize = 4 + 2 + 1;

class LogStatus {
public:
	// Create a success status.
	LogStatus()
			: state_(NULL) {
	}
	~LogStatus() {
		delete[] state_;
	}

	// Copy the specified status.
	LogStatus(const LogStatus& s);
	void operator=(const LogStatus& s);

	// Return a success status.
	static LogStatus OK() {
		return LogStatus();
	}

	// Return error status of an appropriate type.
	static LogStatus NotFound(const Slice& msg, const Slice& msg2 = Slice()) {
		return LogStatus(kNotFound, msg, msg2);
	}
	static LogStatus Corruption(const Slice& msg, const Slice& msg2 = Slice()) {
		return LogStatus(kCorruption, msg, msg2);
	}
	static LogStatus NotSupported(const Slice& msg, const Slice& msg2 = Slice()) {
		return LogStatus(kNotSupported, msg, msg2);
	}
	static LogStatus InvalidArgument(const Slice& msg, const Slice& msg2 = Slice()) {
		return LogStatus(kInvalidArgument, msg, msg2);
	}
	static LogStatus IOError(const Slice& msg, const Slice& msg2 = Slice()) {
		return LogStatus(kIOError, msg, msg2);
	}

	// Returns true iff the status indicates success.
	bool ok() const {
		return (state_ == NULL);
	}

	// Returns true iff the status indicates a NotFound error.
	bool IsNotFound() const {
		return code() == kNotFound;
	}

	// Returns true iff the status indicates a Corruption error.
	bool IsCorruption() const {
		return code() == kCorruption;
	}

	// Returns true iff the status indicates an IOError.
	bool IsIOError() const {
		return code() == kIOError;
	}

	// Return a string representation of this status suitable for printing.
	// Returns the string "OK" for success.
	std::string ToString() const;

private:
	// OK status has a NULL state_.  Otherwise, state_ is a new[] array
	// of the following form:
	//    state_[0..3] == length of message
	//    state_[4]    == code
	//    state_[5..]  == message
	const char* state_;

	enum Code {
		kOk = 0,
		kNotFound = 1,
		kCorruption = 2,
		kNotSupported = 3,
		kInvalidArgument = 4,
		kIOError = 5
	};

	Code code() const {
		return (state_ == NULL) ? kOk : static_cast<Code>(state_[4]);
	}

	LogStatus(Code code, const Slice& msg, const Slice& msg2);
	static const char* CopyState(const char* s);
};

inline LogStatus::LogStatus(const LogStatus& s) {
	state_ = (s.state_ == NULL) ? NULL : CopyState(s.state_);
}
inline void LogStatus::operator=(const LogStatus& s) {
	// The following condition catches both aliasing (when this == &s),
	// and the common case where both s and *this are ok.
	if (state_ != s.state_) {
		delete[] state_;
		state_ = (s.state_ == NULL) ? NULL : CopyState(s.state_);
	}
}

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */
#endif /* DTRANX_STAGES_LOG_DISKSTATUS_H_ */

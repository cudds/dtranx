#!/bin/bash
function test8(){
	RestartDtranxRepartition dtranx1
	RestartDtranxRepartition dtranx2
	RestartDtranxRepartition dtranx3

	sudo docker exec dtranx1 bash -c '/dtranx/SimpleTranx -p Workloads/accesstogether1 -r 60'
	sudo docker exec dtranx1 bash -c '/dtranx/SimpleTranx -p Workloads/accesstogether2 -r 60'
	sudo docker exec dtranx1 bash -c '/dtranx/SimpleTranx -p Workloads/accesstogether3 -r 100'
	sleep 2
	sudo docker cp dtranx1:/dtranx/DTranx.out ./test8_repartition
	
	RestartDtranxClean dtranx1
	RestartDtranxClean dtranx2
	RestartDtranxClean dtranx3
}
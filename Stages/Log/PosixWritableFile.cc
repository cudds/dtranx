/*
 * Copyright (c) 2011 The LevelDB Authors. All rights reserved.
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file. See the AUTHORS file for names of contributors.
 *
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#include "PosixWritableFile.h"

namespace DTranx {
namespace Stages {
namespace Log {

LogStatus PosixWritableFile::Append(const Slice& data) {
	size_t r = fwrite_unlocked(data.data(), 1, data.size(), file_);
	if (r != data.size()) {
		return LogStatus::IOError(filename_, strerror(errno));
	}
	return LogStatus::OK();
}

LogStatus PosixWritableFile::Close() {
	LogStatus result;
	if (fclose(file_) != 0) {
		result = LogStatus::IOError(filename_, strerror(errno));
	}
	file_ = NULL;
	return result;
}

LogStatus PosixWritableFile::Flush() {
	if (fflush_unlocked(file_) != 0) {
		return LogStatus::IOError(filename_, strerror(errno));
	}
	fdatasync(fileno(file_));
	return LogStatus::OK();
}

LogStatus PosixWritableFile::Sync() {
	// Ensure new files referred to by the manifest are in the filesystem.
	LogStatus s = SyncDirIfManifest();
	if (!s.ok()) {
		return s;
	}
	if (fflush_unlocked(file_) != 0 || fdatasync(fileno(file_)) != 0) {
		s = LogStatus::IOError(filename_, strerror(errno));
	}
	return s;
}

} /* namespace Log */
} /* namespace Stages */
} /* namespace DTranx */

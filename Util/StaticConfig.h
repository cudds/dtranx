/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 */

#ifndef DTRANX_UTIL_STATICCONFIG_H_
#define DTRANX_UTIL_STATICCONFIG_H_

#include <unordered_map>
#include <mutex>
#include "Exceptions/NotFoundException.h"

namespace DTranx {
namespace Util {

class StaticConfig {
public:
	static StaticConfig *GetInstance() {
		std::unique_lock<std::mutex> lockGuard(mutex);
		if (!instance) {
			instance = new StaticConfig();
			instance->init();
		}
		return instance;
	}
	static void ReleaseInstance() {
		std::unique_lock<std::mutex> lockGuard(mutex);
		if (instance) {
			delete instance;
		}
	}
	std::string Read(const std::string& key) const {
		auto get = keyValues.find(key);
		if (get == keyValues.end()) {
			throw NotFoundException(key + ": No such key found in StaticConfig");
		}
		return get->second;
	}
private:
	void init();
	static StaticConfig *instance;
	std::unordered_map<std::string, std::string> keyValues;
	static std::mutex mutex;
};

} /* namespace Util */
} /* namespace DTranx */

#endif /* DTRANX_UTIL_STATICCONFIG_H_ */

/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * DaemonStageMock is used in unit test to mock
 * the behavior of a daemon stage class and interact with mid stages
 */

#ifndef DTRANX_DAEMONSTAGESMOCK_H_
#define DTRANX_DAEMONSTAGESMOCK_H_

#include "DaemonStages.h"

namespace DTranx {
namespace DaemonStages {

class DaemonStagesMock: public DaemonStages {
public:
	DaemonStagesMock(SharedResources::TranxServerInfo *tranxServerInfo,
			SharedResources::TranxServiceSharedData *tranxServiceSharedData)
			: DaemonStages(tranxServerInfo, tranxServiceSharedData) {

	}
	virtual ~DaemonStagesMock() {

	}

	virtual void DaemonThread() {
	}

	virtual void StartService(std::vector<uint32_t> coreIDs = std::vector<uint32_t>()) {

	}

	Stages::TranxServiceQueueElement * WaitForResult() {
		Stages::TranxServiceQueueElement *element;
		while (!daemonProcessQueue.try_dequeue(element)) {
			usleep(10000);
		}
		return element;
	}

};

} /* namespace DaemonStages */
} /* namespace DTranx */

#endif /* DTRANX_DAEMONSTAGESMOCK_H_ */

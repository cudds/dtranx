/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 * Tranx is the service handler for Distributed Transactions both from Client and other transaction managers.
 *
 * It consists of transaction service handlers, log recovery, snapshot/gc threads functions
 */

#ifndef DTRANX_SERVICE_TRANXSERVICE_H_
#define DTRANX_SERVICE_TRANXSERVICE_H_

#include <mutex>
#include "Service.h"
#include "RPC/ServerRPC.h"
#include "MessageIDSet.h"
#include "Stages/GCFactory.h"
#include "DTranx/Stages/Log/Log.pb.h"

namespace DTranx {
namespace DaemonStages {
class Repartition;
}
namespace Service {

class TranxService: public Service {
public:
	/*
	 * createPeers/createPeerToSelf will be used by the unit test to disable peer creation and enable create peers that connect to itself
	 */
	TranxService(SharedResources::TranxServerInfo *tranxServerInfo,
			SharedResources::TranxServiceSharedData *tranxServiceSharedData);
	virtual ~TranxService() noexcept(false);
	virtual void HandleRPC(RPC::ServerRPC *serverRPC);

	/*
	 * Access methods to new mappings are only for RecoverLog::Recover
	 */
	void SetNewMappings(const std::unordered_map<std::string, std::unordered_set<std::string> >& newMappings) {
		this->newMappings = newMappings;
	}

	void ClearNewMappings() {
		newMappings.clear();
	}

	virtual void StartService(int numOfInServiceThreads, std::vector<uint32_t> coreIDs =
			std::vector<uint32_t>()) {
		/*
		 * If multiple internal threads are needed, adjust the keysModeBuffer/readSetBuffer
		 * to avoid contention bugs.
		 * assert is put here to remind the user.
		 */
		assert(numOfInServiceThreads == 1);
		/*
		 * internal service threads will share physical cores while there's only one external thread
		 */
		bool enableCoreBinding = false;
		uint32_t coreIDIndex = 0;
		assert(numOfInServiceThreads >= 1);
		enableCoreBinding = (coreIDs.size() - 1 == numOfInServiceThreads);

		if (numOfInServiceThreads == 1) {
			tranxServiceSharedData->setThreadSafeCurTranx(true);
		}

		for (int i = 0; i < numOfInServiceThreads; ++i) {
			serviceInternalThread.push_back(
					boost::thread(&TranxService::ServiceInternalThread, this, i));
			if (enableCoreBinding) {
				Util::ThreadHelper::PinToCPUCore(serviceInternalThread[i],
						coreIDs[coreIDIndex++]);
			}
		}
		serviceThread.push_back(boost::thread(&TranxService::ServiceThread, this, 0));
		if (enableCoreBinding) {
			Util::ThreadHelper::PinToCPUCore(serviceThread[0], coreIDs[coreIDIndex++]);
		}
	}

private:
	/*************************************************************************************************************
	 * in memory states
	 *
	 * newMapping is used by participants to store temporary new mappings for repartitioning purpose
	 * no locks has been used to prevent concurrent access because no two threads will be accessing it concurrently
	 * RecoverLog::recover will be accessing the newMappings without locks
	 *
	 * for coordinators, newMappings are stored in TranxServiceQueueElement
	 */
	std::unordered_map<std::string, std::unordered_set<std::string> > newMappings;

	/**************************************************************************************************************
	 * thread to send snapshot creation request
	 *
	 * thread to garbage collect ack, broadcast gc, and then reclaim WAL
	 *
	 * thread to commit changes to localStorage
	 *
	 * toGC is changed by the OneReadNext
	 *
	 * MaybeLater: enable later, and it might be better to start the thread in Server
	 */
	std::atomic_bool terminateSnapshotThread;
	void SnapshotThread();
	boost::thread snapshotThread;

	virtual void ServiceThread(int index);
	virtual void ServiceInternalThread(int index);

	/**************************************************************************************************************
	 * transaction service handlers from Servers
	 */
	void TranxPrepare(RPC::ServerRPC *serverRPC);
	void TranxCommit(RPC::ServerRPC *serverRPC);
	void TranxAbort(RPC::ServerRPC *serverRPC);
	void TranxInquire(RPC::ServerRPC *serverRPC);
	void TranxGC(RPC::ServerRPC *serverRPC);
	void TranxPartition(RPC::ServerRPC *serverRPC);
	void TranxMigrate(RPC::ServerRPC *serverRPC);

	/**************************************************************************************************************
	 * transaction service handlers from other stages
	 */
	void TranxPrepare(Stages::TranxServiceQueueElement *element);
	void TranxCommit(Stages::TranxServiceQueueElement *element);
	void TranxAbort(Stages::TranxServiceQueueElement *element);
	void TranxInquire(Stages::TranxServiceQueueElement *element);
	void TranxMigrate(Stages::TranxServiceQueueElement *element);
	void TranxGC(Stages::TranxServiceQueueElement *element);

	/*
	 * avoid too much memory reallocation, currently it's only working
	 * for one internal service thread.
	 */
	std::unordered_map<std::string, Stages::Lock::BasicLockTableLockMode> keysModeBuffer;
	std::unordered_map<std::string, uint64> readSetBuffer;

	/*
	 * TranxService is non-copyable.
	 */
	TranxService(const TranxService&) = delete;
	TranxService& operator=(const TranxService&) = delete;
};

}
/* namespace Service */
} /* namespace DTranx */
#endif /* DTRANX_SERVICE_TRANXSERVICE_H_ */

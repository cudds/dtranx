/*
 * Author: Ning Gao(nigo9731@colorado.edu)
 *
 * Class Summary:
 * ServerRPC wraps around the Buffer to provide google message to service classes
 * at the same time, ServerRPC provides service header for Server class to dispatch to different thread pool according to service type
 */

#ifndef DTRANX_RPC_SERVERRPC_H_
#define DTRANX_RPC_SERVERRPC_H_

#include "DTranx/Service/Storage.pb.h"
#include "BaseRPC.h"
#include "Util/StringUtil.h"

namespace DTranx {
namespace RPC {

class ServerRPC: public BaseRPC {
public:
	ServerRPC();
	ServerRPC(ServerRPC&& other);
	ServerRPC& operator=(ServerRPC&& other);
	virtual ~ServerRPC() noexcept(false);

	void Clear();

	/*
	 * opcode access methods
	 */
	Service::GOpCode GetClientOpcode() {
		assert(requestMessage != NULL);
		assert(requestMessage->has_clientservicerpc());
		return requestMessage->clientservicerpc().opcode();
	}

	Service::GOpCode GetTranxOpcode() {
		assert(requestMessage != NULL);
		assert(requestMessage->has_tranxservicerpc());
		return requestMessage->tranxservicerpc().opcode();
	}

	Service::GOpCode GetStorageOpcode() {
		assert(requestMessage != NULL);
		assert(requestMessage->has_storageservicerpc());
		return requestMessage->storageservicerpc().opcode();
	}

	/*
	 * specific request/response methods
	 */

	Service::GClientServiceRPC* GetClientRequest() {
		assert(requestMessage->has_clientservicerpc());
		return requestMessage->mutable_clientservicerpc();
	}

	Service::GClientServiceRPC* GetClientResponse() {
		return responseMessage->mutable_clientservicerpc();
	}

	Service::GTranxServiceRPC* GetTranxRequest() {
		assert(requestMessage->has_tranxservicerpc());
		return requestMessage->mutable_tranxservicerpc();
	}

	Service::GTranxServiceRPC* GetTranxResponse() {
		return responseMessage->mutable_tranxservicerpc();
	}

	Service::GStorageServiceRPC* GetStorageRequest() {
		assert(requestMessage->has_storageservicerpc());
		return requestMessage->mutable_storageservicerpc();
	}

	Service::GStorageServiceRPC* GetStorageResponse() {
		return responseMessage->mutable_storageservicerpc();
	}

	void SetStartTime(struct timespec time) {
		startTime = time;
	}

	void SetClientID(zmsg::ustring clientID_) {
		clientID = clientID_;
	}

	std::string GetClientID() {
		std::string s_clientID = Util::StringUtil::UstringToString(clientID);
		return s_clientID;
	}

	void PrintTime() {
		if (toMonitor) {
			clock_gettime(CLOCK_MONOTONIC_RAW, &endTime);
			double timePeriod = (endTime.tv_sec - startTime.tv_sec) * 1000000
					+ (endTime.tv_nsec - startTime.tv_nsec) / 1000;
			VERBOSE("serverRPC handle time %f for messageID %lu", timePeriod, GetReqMessageID());
		}
	}

	void SetMonitor() {
		toMonitor = true;
	}

	bool isIsPerfOutPutTask() const {
		return isPerfOutPutTask;
	}

	void setIsPerfOutPutTask(bool isPerfOutPutTask) {
		this->isPerfOutPutTask = isPerfOutPutTask;
	}

private:
	/*
	 * used for metric system
	 */
	struct timespec startTime;
	struct timespec endTime;
	bool toMonitor;

	/*
	 * clientID used for distinguishing connections
	 */
	zmsg::ustring clientID;

	/*
	 * perf use only
	 */
	bool isPerfOutPutTask;

	/*
	 * ServerRPC is non-copyable.
	 */
	ServerRPC(const ServerRPC&) = delete;
	ServerRPC& operator=(const ServerRPC&) = delete;
};

} /* namespace RPC */
} /* namespace DTranx */

#endif /* DTRANX_RPC_SERVERRPC_H_ */
